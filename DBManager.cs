﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SATS
{
    public class DBManager
    {
        private MySqlConnection connection;
        private Object thisLock;

        string EncryptOrDecrypt(string text, string key)
        {
            var result = new StringBuilder();

            for (int c = 0; c < text.Length; c++)
                result.Append((char)((uint)text[c] ^ (uint)key[c % key.Length]));

            return result.ToString();
        }

        public DBManager(string connectionString1)
        {
            //string connectionStringWeb = "SERVER=" + "sagamedicao.com" + ";" + "DATABASE=" +
            //    "sagamedi_sats_be" + ";" + "UID=" + "sagamedi_be_slv" + ";" + "PASSWORD=" + "be*245" + ";";

            thisLock = new Object();
            string connectionString = EncryptOrDecrypt(connectionString1, "saga*245");
            try
            {
                connection = new MySqlConnection(connectionString);
            }
            catch (MySqlException)
            {
                // MessageBox.Show("MySQL Server connection failure: " + e);
            }
        }


        public bool OpenConnection()
        {
            lock (thisLock)
            {
                bool isOpen = true;
                try
                {
                    if (LookForExistingConnections() == false)
                    {
                        connection.Open();
                    }
                }

                catch (TimeoutException ex)
                {
                    MessageBox.Show(ex.ToString());

                }

                catch (MySqlException)
                {
                    //MessageBox.Show(e.Number.ToString() + " " + e.Message);
                    connection.Close();
                    isOpen = false;
                }
                return isOpen;
            }
        }

        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }

            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        public bool LookForExistingConnections()
        {
            if (connection.State == System.Data.ConnectionState.Open)
                return true;
            else
                return false;
        }

        public bool Insert(string table, string columns, string values)
        {
            string query = "INSERT INTO " + table + "(" + columns + ")"
                   + "VALUES(" + values + " );";
            if (this.OpenConnection() == true)
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                    MessageBox.Show("Dados inseridos com sucesso.", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return true;
                }
                catch (MySqlException e)
                {
                    this.CloseConnection();
                    if (e.Number == 1062)
                        MessageBox.Show("Dados já cadastrados no Banco de Dados!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else MessageBox.Show(e.ToString(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return false;
                }
            }
            return false;
        }

        public bool Insert2(string table, string columns, string values)
        {
            string query = "INSERT INTO " + table + "(" + columns + ")"
                   + "VALUES(" + values + " );";
            if (this.OpenConnection() == true)
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                    return true;

                    //MessageBox.Show("Dados inseridos com sucesso.", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (MySqlException)
                {
                    this.CloseConnection();
                    return false;
                    //if (e.Number == 1062)
                    //    MessageBox.Show("Usuário ja Cadastrado.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //else MessageBox.Show(e.ToString(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return false;
        }

        public bool Update(string table, string clmnValues, string where)
        {
            string query = "UPDATE " + table + " SET " + clmnValues + " WHERE " +
                where + ";";

            if (this.OpenConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                this.CloseConnection();

                return true;
            }
            return false;
        }

        public bool Delete(string table, string where)
        {
            string query = "DELETE FROM " + table + " WHERE " + where + ";";
            if (this.OpenConnection() == true)
            {
                try
                {

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                    return true;

                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        /* Exemplo utilizacao (a partir de outra classe)
            List<string []> res = manager.SelectAllFromTable("sigt_user");
            foreach(string [] item in res)
            {
               for (int i = 0; i < item.Length; ++i )
                MessageBox.Show(item[i]);
            }
        */
        // Esses quatro metodos de baixo sao usados pra realizar um select. Eles estao
        // separados pra dividir as tarefas, facilitar debug e poder reutilizar
        // no select de duas tabelas.
        private List<string[]> ReturnDataReaderAsStringList(MySqlDataReader dataReader)
        {
            List<string[]> list = new List<string[]>();

            lock (thisLock)
            {
                while (dataReader.Read())
                {
                    int colsNumber = dataReader.FieldCount;
                    list.Add(new string[colsNumber]);

                    //list[list.count - 1] pra acessar o ultimo elemento da lista
                    for (var i = 0; i < colsNumber; ++i)
                        list[list.Count - 1][i] = dataReader[i].ToString();
                }
            }
            return list;
        }

        // Onde o comando select e realmente executado.
        public List<string[]> DeFactoSelect(string query)
        {

            List<string[]> list = new List<string[]>();
            // string[] vai ser o tamanho do numero
            // de colunas de determinada tabela.

            // Foi necessario implementar serializacao aqui porque se duas threads
            // tentarem abrir conexoes paralelamente o programa da pau.
            lock (thisLock)
            {
                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    try
                    {
                        cmd.CommandTimeout = 600;
                        MySqlDataReader dataReader = cmd.ExecuteReader();
                        list = ReturnDataReaderAsStringList(dataReader);
                        dataReader.Close();
                    }
                    catch (MySqlException ex)
                    {
                        //MessageBox.Show("Erro na consulta ao Banco de Dados.\nDetalhes técnicos: " + e.GetType());
                    }

                    catch (NullReferenceException)
                    {
                        //MessageBox.Show("Erro na consulta ao Banco de Dados.\nDetalhes técnicos: " + e.GetType());
                    }

                    this.CloseConnection();
                }
            }
            return list;
        }

        public List<string[]> Select(string table, string columns, string where, string orderBy,
                                    string limit)
        {
            string query = "SELECT " + columns + " FROM " + table;
            if (where != String.Empty)
                query += " WHERE " + where;
            if (orderBy != String.Empty)
                query += " ORDER BY " + orderBy + " DESC ";
            if (limit != String.Empty)
                query += " LIMIT " + limit;
            query += ";";

            Console.WriteLine("Select "+query.ToString());

            return DeFactoSelect(query);
        }

        public List<string[]> Select2(string table, string columns, string where, string orderBy,
                                    string limit)
        {
            string query = "SELECT " + columns + " FROM " + table;
            if (where != String.Empty)
                query += " WHERE " + where;
            if (orderBy != String.Empty)
                query += " ORDER BY " + orderBy + " ASC ";
            if (limit != String.Empty)
                query += " LIMIT " + limit;
            query += ";";

            return DeFactoSelect(query);
        }

        public List<string[]> Select3(string table, string columns, string where, string orderBy,
                               string limit)
        {
            string query = "SELECT " + columns + " FROM " + table;
            if (where != String.Empty)
                query += " WHERE " + where + "UNION ALL";
            if (orderBy != String.Empty)
                query += " ORDER BY " + orderBy + " DESC ";
            if (limit != String.Empty)
                query += " LIMIT " + limit;
            query += ";";

            return DeFactoSelect(query);
        }


        public List<string[]> Select(string table, string columns, string where)
        {
            return Select(table, columns, where, String.Empty, String.Empty);
        }

        public List<string[]> Select(string table, string columns, string where, string orderBy)
        {
            return Select(table, columns, where, orderBy, String.Empty);
        }

        public List<string[]> Select2(string table, string columns, string where)
        {
            return Select2(table, columns, where, String.Empty, String.Empty);
        }

        public List<string[]> Select2(string table, string columns, string where, string orderBy)
        {
            return Select2(table, columns, where, orderBy, String.Empty);
        }

        public List<string[]> SelectUnionTwoTables(string table1, string columns1, string where1,
            string table2, string columns2, string where2, string orderBy)
        {
            string query = "SELECT " + columns1 + " FROM " + table1;
            if (where1 != String.Empty)
                query += " WHERE " + where1;

            query += " UNION ";
            query += "SELECT " + columns2 + " FROM " + table2;
            if (where2 != String.Empty)
                query += " WHERE " + where2;

            if (orderBy != String.Empty)
                query += " ORDER BY " + orderBy + " ASC";
            query += ";";

            return DeFactoSelect(query);
        }

        public int Count(string table)
        {
            string query = "SELECT Count(*) FROM " + table;
            int count = -1;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                count = int.Parse(cmd.ExecuteScalar().ToString());

                this.CloseConnection();
            }

            return count;
        }

        public List<string[]> SelectInnerJoin(string table1, string columns,
            string table2, string similar, string where, string orderBy, string sentido)
        {
            string query = "SELECT " + columns + " FROM " + table1;

            query += " INNER JOIN " + table2;
            query += " ON " + similar;
            if (where != String.Empty)
                query += " WHERE " + where;

            if (orderBy != String.Empty)
                query += " ORDER BY " + orderBy + sentido;
            query += ";";

            //Console.WriteLine(query);

            return DeFactoSelect(query);
        }

        public List<string[]> SelectInnerJoin2(string table1, string columns,
            string table2, string similar,string where)
        {
            string query = "SELECT " + columns + " FROM " + table1 + " INNER JOIN " + table2 + " ON " + similar + " Where "+where+";";

            //Console.WriteLine(query);

            return DeFactoSelect(query);
        }


        public List<string[]> Obterporhora(string dataini, string datafinal, string id)
        {
            try
            {
                string query = "";
                if (id != "processo" && id != "producao")
                {
                    query = "SELECT nome_ponto, Round(volume/1000, 0),Round(avg(vazao)/1000,2), data_hora, hour(data_hora) FROM  ponto_leitura" +
    " inner join ponto on ponto_leitura.id_ponto = ponto.id_ponto" +
     " where (data_hora between '" + dataini + "' and '" + datafinal + "' ) and ponto_leitura.id_ponto = " + id +
    " GROUP BY ponto_leitura.id_ponto, month(data_hora), day(data_hora), hour(data_hora);";
                }
                else
                {
                    query = "SELECT nome_ponto, Round(volume/1000, 0) ,Round(avg(vazao)/1000,2), data_hora, hour(data_hora) FROM  ponto_leitura" +
    " inner join ponto on ponto_leitura.id_ponto = ponto.id_ponto" +
     " where ponto.ponto_tipo='" + id + "' and (data_hora between '" + dataini + "' and '" + datafinal + "' )" +
    " GROUP BY ponto_leitura.id_ponto, month(data_hora), day(data_hora), hour(data_hora);";
                }

                return DeFactoSelect(query);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public int Obterporquatidade(string dataini, string datafinal, string id)
        {
            try
            {
                string query = "";
                if (id != "processo" && id != "producao")
                {

                    query = "SELECT Round(avg(vazao)/1000,2) FROM ponto_leitura where (data_hora between '" + dataini + "' and '" + datafinal + "' ) and id_ponto = " + id.ToString() + " GROUP BY id_ponto, day(data_hora) ;";

                }
                else
                {
                    query = "SELECT Round(avg(vazao)/1000,2) FROM ponto_leitura" +
                        " inner join ponto on ponto_leitura.id_ponto = ponto.id_ponto" +
                        " where ponto.ponto_tipo='" + id + "' and (data_hora between '" + dataini + "' and '" + datafinal + "' ) GROUP BY id_ponto, day(data_hora) ;";
                }
                int a = DeFactoSelect(query).Count();
                return a;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public List<string[]> Obterporhora2(string dataini, string datafinal, int dia, string id)
        {
            try
            {
                string query = "SELECT Round(volume/1000, 0) ,Round(avg(vazao)/1000,2), hour(data_hora), data_hora FROM ponto_leitura where (data_hora between '" + dataini + "' and '" + datafinal + "' ) and dayofweek(data_hora)= " + dia.ToString() + " and id_ponto= " + id.ToString() + " GROUP BY month(data_hora), day(data_hora), hour(data_hora) ;";
                return DeFactoSelect(query);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public int Obterporquatidade2(string dataini, string datafinal, int dia, string id)
        {
            try
            {
                string query = "SELECT Round(avg(vazao)/1000,2) FROM ponto_leitura where (data_hora between '" + dataini + "' and '" + datafinal + "' ) and dayofweek(data_hora)= " + dia.ToString() + " and id_ponto= " + id.ToString() + " GROUP BY  day(data_hora) ;";
                int a = DeFactoSelect(query).Count();
                return a;
            }
            catch (Exception)
            {
                return 0;
            }
        }


        public List<string[]> obtermicro(string dataini, string datafinal)
        {
            string query = "SELECT nome_ponto, MAX(volume) - MIN(volume) AS 'Tax Rate Difference' "
            + "FROM ponto_leitura inner join ponto on ponto_leitura.id_ponto = ponto.id_ponto "
            + " WHERE data_hora between '" + dataini + "' and '" + datafinal + "' "
            + " GROUP BY ponto_leitura.id_ponto;";
            return DeFactoSelect(query);
        }

        public List<string[]> obterconsumo(string dataini, string datafinal, string Tipoponto)
        {
            string query = "select ponto.nome_ponto, Round((Max(volume)-MIN(volume))/1000,0) as consumo, min(data_hora) as mindatahora, max(data_hora) as maxdatahora " +
            "from ponto_leitura inner join ponto ON ponto.id_ponto = ponto_leitura.id_ponto " +
            "where data_hora >='" + dataini + "' and data_hora<='" + datafinal + "' and ponto.ponto_tipo = '" + Tipoponto + "' " +
            "group by ponto_leitura.id_ponto asc;";
            return DeFactoSelect(query);
        }

        public decimal SelectVolume(int id, string dataini, string datafinal)
        {

            string query = "select max(volume)- min(volume) from ponto_leitura " +
                         "where id_ponto='" + id + "' and data_hora>'" + dataini + "' and data_hora<'" + datafinal + " 23:59:59'";

            List<string[]> result = DeFactoSelect(query);
            decimal volume = Convert.ToDecimal(result[0][0]) / 1000;
            return Math.Round(volume, 2);
        }

    }
}
