﻿using DevComponents.DotNetBar;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;


namespace SATS
{
    public partial class FrmInfoPonto : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        protected DBManager dB;
        public List<string> indexClientes { get; set; }
        private int index { get; set; }
        private string status {get;set;}
        private string texto { get; set; }
        List<string[]> result;
        Thread novaThread;


        public FrmInfoPonto(int index)
        {
            string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
            dB = new DBManager(stcon);
            this.index = index;
            InitializeComponent();
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
          
                }
                catch (Exception)
                {
                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
           
            foreach (string[] item in result)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                listConcat.Add(concat);
            }

            foreach (string[] item in result)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                //listBomba.Add(concat);
            }
            return listConcat;
        }


        private void carregar()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    result = dB.Select("ponto_informacao", "*", "id_ponto='" + index + "'");

                    texto = result[0][2];

                    if (result.Count == 0)
                    {
                        //MessageBox.Show("Dados não cadastrados!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Invoke((MethodInvoker)(() => textBox1.Enabled = true));
                        Invoke((MethodInvoker)(() => btnEdit.Enabled = false));
                        status = "salvar";
                    }
                    else
                    {
                        Invoke((MethodInvoker)(() => textBox1.Enabled = false));
                        Invoke((MethodInvoker)(() => btSalvar.Enabled = false));
                        Invoke((MethodInvoker)(() => textBox1.Text = texto));
                        status = "atualizar";
                    }
                }

                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();

        }


        private void btSalvar_Click(object sender, EventArgs e)
        {
            getValores();
            btSalvar.Enabled = false;
            textBox1.Enabled = false;
            btnLimpar.Enabled = false;

            if(textBox1.Text != "")
                MessageBox.Show("Informações adicionadas! ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);


            //if (btnEdit.Enabled)
            //    atualizar();
            //else
            //    salvar();
        }

        private void FrmInfoPonto_Load(object sender, EventArgs e)
        {
           
            novaThread = new Thread(new ThreadStart(carregar));
            novaThread.Start();
        }

        //void salvar()
        //{
        //    btnEdit.Enabled = false;
        //    if (textBox1.Text.Length <= 1000)
        //    {
        //        try
        //        {

        //            dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
        //            bool resultbol = dB.Insert("ponto_informacao", "id_ponto, informacao, data_hora",
        //                        "'" + index + "', '" + textBox1.Text + "', now()");
        //            if (resultbol)
        //                this.Dispose();

        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }

                
        //    }
        //    else
        //    {
        //        MessageBox.Show("Quantidade de caracteres execedida ! ", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        return;
        //    }
            
        //    Application.UseWaitCursor = false;
        //    Application.DoEvents();
        //}

        //void atualizar()
        //{
        //    btnEdit.Enabled = true;

        //    bool resultbol2 = false;
        //    if (textBox1.Text.Length <= 1000)
        //    {

        //        try
        //        {

        //            resultbol2 = dB.Update("ponto_informacao", "informacao = '" + textBox1.Text + "', data_hora = now()", "id_ponto = " + index);
        //            dB.Insert2("ponto_aciona", "id_ponto, acionar, porta, data_hora", "'" + id_ponto[0][0] + "', '0', '" + portabomba + "', now()");
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }

        //        if (resultbol2)
        //        {
        //            MessageBox.Show("Infomação de Ponto Atualizada!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            this.Dispose();
        //        }
        //        else
        //        {
        //            MessageBox.Show("Não atualizado", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Não atualizado Quantidade de caracteres Execedida ! ", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    Application.UseWaitCursor = false;
        //    Application.DoEvents();

        //}

        private void btnEdit_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            btSalvar.Enabled = true;
            btnLimpar.Enabled = true;
        }

       public string getValores()
        {
            return textBox1.Text;
        }

     public string getTestStatus()
        {
            return status;
        }
    }
}
