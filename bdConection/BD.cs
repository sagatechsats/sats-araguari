﻿using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SATS
{
    //Classe que faz apenas comunicação (abrindo e fechando) com o banco de dados
    class BD
    {
        private MySqlConnection Connection;
        // Encapsulamento que retorna o ponteiro aberto de conexão e/ou seu status atual
        public MySqlConnection _Connection
        {
            get { return Connection; }
        }

        string EncryptOrDecrypt(string text, string key)
        {
            var result = new StringBuilder();

            for (int c = 0; c < text.Length; c++)
                result.Append((char)((uint)text[c] ^ (uint)key[c % key.Length]));

            return result.ToString();
        }
        //Método construtor vazio que através de uma string de conexão realiza a comunicação com o banco de dados padrão
        public BD()
        {
            //string connectionString = "Server=sagamedicao.com.br;port=3306;Database=sagamedi_sgit;Uid=sagamedi_users;Pwd=saga*2456;";

            string stcon = EncryptOrDecrypt(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"), "saga*245");

            string connectionString = stcon;
            //string connectionString = "SERVER=" + serverAddress + ";" + "DATABASE=" +
            //    database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            try
            {
                Connection = new MySqlConnection(connectionString);
            }
            catch (MySqlException)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //Método que abre a conexão usando os dados do construtor e devolve um booleano para confirmação
        public bool OpenConnection()
        {
            try
            {
                Connection.Open();
                return true;
            }

            catch (MySqlException e)
            {
                //MessageBox.Show(e.ToString());
                MessageBox.Show(e.ToString(),"ERRO DE CONEXÃO COM O SERVIDOR", 
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Connection.Close();
                return false;
            }
        }

        public bool CloseConnection()
        {
            try
            {
                Connection.Close();
                return true;
            }

            catch (MySqlException)
            {
                //MessageBox.Show(e.Message);
                return false;
            }

        }
        /*
        public void Insert(string table, string columns, string values)
        {
            string query = "INSERT INTO " + table + "(" + columns + ")"
                   + "VALUES(" + values + " );";
           / if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                //this.CloseConnection();
            }
        }

        public void Update(string table, string clmnValues, string where)
        {
            string query = "UPDATE " + table + " SET " + clmnValues + " WHERE " +
                where + ";";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                //this.CloseConnection();
            }
        }

        public void Delete(string table, string where)
        {
            string query = "DELETE FROM " + table + " WHERE " + where + ";";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                //this.CloseConnection();
            }
        }

        /*Exemplo utilizacao (a partir de outra classe)
            List<string []> res = manager.SelectAllFromTable("sigt_user");
            foreach(string [] item in res)
            {
               for (int i = 0; i < item.Length; ++i )
                MessageBox.Show(item[i]);
            }
        
        public List<string[]> Select(string table, string colums, string where)
        {
            string query = "SELECT " + colums + " FROM " + table;
            if (where != String.Empty)
                query += " WHERE " + where;
            query += ";";
            List<string[]> list = new List<string[]>();//string[] vai ser o tamanho do numero
            //de colunas de determinada tabela
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    int colsNumber = dataReader.FieldCount;
                    list.Add(new string[colsNumber]);
                    //list[list.count - 1] pra acessar o ultimo elemento da lista
                    for (int i = 0; i < colsNumber; ++i)
                        list[list.Count - 1][i] = dataReader[i].ToString();
                }

                dataReader.Close();
                //this.CloseConnection();
                return list;
            }
            return list;
        }

        public int Count(string table)
        {
            string query = "SELECT Count(*) FROM " + table;
            int count = -1;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                count = int.Parse(cmd.ExecuteScalar().ToString());

                //this.CloseConnection();
            }

            return count;
        }

     */
    }
}
