﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using DevComponents.DotNetBar;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Linq.Expressions;
using NAudio.Wave;
using System.Media;
using System.Linq;

namespace SATS
{
    public partial class Main : Office2007Form
    {
        // Esse listNames eu tive que fazer porque eu vou ter que fazer a iteracao por
        // todos os pontos, e pelo id pode nao dar certo porque nem sempre eles sao
        // sequenciais.

        // listNames e listPontos devem ser correspondentes na ordem das informacoes dos
        // pontos. Se nao, o programa vai quebrar. Faria mais sentido por listaNamesBD em
        // DBApplication, mas visto que o acoplamento entre as duas classes e alto, deixei
        // aqui perto de listPontos pra ver mais facil a correspondencia entre posicoes dos dois
        //  private int _alertaSom;
        private List<string[]> ponto;
        protected List<Ponto> listPontos;
        protected List<string> listNamesDB;
        protected DBApplication DatabaseAppManager;
        protected InternetConnectionVerifier NetVerifier;
        protected bool TheresNetAbnormality;
        protected Thread ThreadPontos;
        protected Thread ThreadEmail;
        protected Thread ThreadInternet;

        //protected Thread ThreadAlertas;
        //protected Thread ThreadVazao;
        List<double> XRatioList, YRatioList;
        private string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
        string path = @"" + System.AppDomain.CurrentDomain.BaseDirectory + "Version.txt";

        public void AppendlistPontos(Ponto ponto)
        {
            //if (listPontos.Count > -1)
            //{
            //    for (int x = 0; x < listPontos.Count; x++)
            //    {
            //        int p = listPontos.FindIndex(y => y.name == ponto.name);
            //        if (p >= 0)
            //        {
            //            listPontos[p] = ponto;
            //        }
            //        else
            //        {
            //            listPontos.Add(ponto);
            //        }
            //    }
            //}

            if (File.Exists(path))
            {
                string text = File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "Version.txt", Encoding.UTF8);
                toolStripVersion.Text = "Versão:" + text;
            }
            else
            {
                toolStripVersion.Text = "";
            }

        }

        public Main(String User, String permission = "Supervisor")
        {
            InitializeComponent();
            DatabaseAppManager = new DBApplication();
            listPontos = new List<Ponto>();
            listNamesDB = new List<string>();
            NetVerifier = new InternetConnectionVerifier();
            TheresNetAbnormality = false;

            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.AllScreens[1].WorkingArea;
            }

            //if (System.IO.File.Exists(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "map.png"))
            //    pictureBox1.ImageLocation = @"" + System.AppDomain.CurrentDomain.BaseDirectory + "map.png";

            InitializePontos();
            this.WindowState = FormWindowState.Maximized;
            CalculatePontosRatio();
            sincronizaçãoToolStripMenuItem.Enabled = false;
            sincronizaçãoToolStripMenuItem.Visible = false;

            if (permission == "Operador")
            {

                cadastroDeClienteToolStripMenuItem.Enabled = false;
                cadastroDeClienteToolStripMenuItem.Visible = false;
                cadastroDeAçõesToolStripMenuItem.Enabled = false;

                cadastroDeAçõesToolStripMenuItem.Visible = false;

                gastosToolStripMenuItem.Enabled = false;
                gastosToolStripMenuItem.Visible = false;
                importaçãoToolStripMenuItem.Enabled = false;

                importaçãoToolStripMenuItem.Visible = false;
                importarGastosToolStripMenuItem.Enabled = false;
                importarGastosToolStripMenuItem.Visible = false;
                AutomaticoToolStripMenuItem.Enabled = false;
                AutomaticoToolStripMenuItem.Visible = false;
                ativarEdiçãoToolStripMenuItem.Enabled = false;

                ativarEdiçãoToolStripMenuItem.Visible = false;
                pontoToolStripMenuItem.Visible = false;
                pontoToolStripMenuItem.Enabled = false;

                configuraçãoDeAlarmeToolStripMenuItem.Visible = false;
                configuraçãoDeAlarmeToolStripMenuItem.Enabled = false;

                configuraçãoSetPointToolStripMenuItem.Visible = false;
                configuraçãoSetPointToolStripMenuItem.Enabled = false;
            }
            toolStripUser.Text = "Usuário: " + User;
        }

        // Eu queria colocar isso no Designer, mas da problema
        public void InitializePontos()
        {
            ponto = DatabaseAppManager.AllPontos();
            listPontos.Clear();

            for (int i = 0; i < ponto.Count; i++)
            {
                Ponto pt = new Ponto(); //User Control Ponto
                Point p = new Point(Convert.ToInt32(ponto[i][3].ToString()), Convert.ToInt32(ponto[i][4].ToString()));
                pt.idPonto = ponto[i][0];
                pt.tipo_ponto = ponto[i][2];
                pt.Location = p;
                pt.BringToFront();
                pt.Visible = true;
                pt.Enabled = true;
                pt.Parent = pbAcionamentos;
                pt.BackColor = Color.Transparent;
                pt.Name = replace(ponto[i][1].ToString().Trim().Replace(" ", ""));
                pt.Start(ponto[i][1].ToString().Trim(), DatabaseAppManager);
                listNamesDB.Add(ponto[i][1].ToString().Trim());
                pt.SetName(ponto[i][1].ToString());
                if (ponto[i][6] == "1")
                {
                    pt.Parent = pbPocoSolteiro;
                }
                listPontos.Add(pt);
            }
            DatabaseAppManager.SetAllIDs(listPontos);
        }


        private void CarregaPontoUpdate()
        {
            ponto.Clear();
            ponto = DatabaseAppManager.AllPontos();

            if (listPontos.Count != ponto.Count)
            {
                foreach (var pt in listPontos)
                {
                    deletePonto(pt.name, pbAcionamentos);
                    deletePonto(pt.name, pbPocoSolteiro);
                }
                InitializePontos();
            }
            //Console.WriteLine(listPontos.Count.ToString());
        }

        //private void CarregaPontoNivel()
        //{

        //    ponto = DatabaseAppManager.AllPontos();

        //    for (int i = 0; i < ponto.Count; i++)
        //    {
        //        Ponto pt = new Ponto(); //User Control Ponto
        //        Point p = new Point(Convert.ToInt32(ponto[i][3].ToString()), Convert.ToInt32(ponto[i][4].ToString()));

        //        switch (ponto[i][2])
        //        {
        //            case "0":
        //                pt.idPonto = ponto[i][0];
        //                pt.Location = p;
        //                pt.BringToFront();
        //                pt.Visible = true;
        //                pt.Enabled = true;
        //                pt.Parent = pBNivel;
        //                pt.BackColor = Color.Transparent;
        //                pt.Name = replace(ponto[i][1].ToString().Replace(" ", ""));
        //                pt.Start(ponto[i][1].ToString(), DatabaseAppManager);
        //                listNamesDB.Add(ponto[i][1].ToString());
        //                pt.SetName(ponto[i][1].ToString());
        //                break;

        //            case "1":
        //                pt.Location = p;
        //                //pt.BringToFront();
        //                //pt.Visible = true;
        //                //pt.Enabled = true;
        //                //pt.Parent = pBomba;
        //                //pt.BackColor = Color.Transparent;
        //                pt.Name = replace(ponto[i][1].ToString().Replace(" ", ""));
        //                pt.Start(ponto[i][1].ToString(), DatabaseAppManager);
        //                listNamesDB.Add(ponto[i][1].ToString());
        //                pt.SetName(ponto[i][1].ToString());
        //                break;

        //            case "7":

        //                //pt.BringToFront();
        //                //pt.Visible = true;
        //                //pt.Enabled = true;
        //                //pt.Parent = pBPressoes;
        //                //pt.BackColor = Color.Transparent;
        //                pt.Name = replace(ponto[i][1].ToString().Replace(" ", ""));
        //                pt.Start(ponto[i][1].ToString(), DatabaseAppManager);
        //                listNamesDB.Add(ponto[i][1].ToString());
        //                pt.SetName(ponto[i][1].ToString());
        //                break;
        //        }

        //        listPontos.Add(pt);
        //    }


        //    DatabaseAppManager.SetAllIDs(listPontos);
        //}


        // Calcula proporcoes para redimensionar de maneira correta, 
        // coisa que o Windows Forms nao prove.

        public void CalculatePontosRatio()
        {
            XRatioList = new List<double>();
            YRatioList = new List<double>();

            for (var i = 0; i < listPontos.Count; ++i)
            {
                XRatioList.Add(Convert.ToDouble(listPontos[i].Left) / pbAcionamentos.Width);
                YRatioList.Add(Convert.ToDouble(listPontos[i].Top) / pbAcionamentos.Height);
            }
        }

        private int SearchPontoByDBName(string name)
        {
            for (int i = 0; i < listPontos.Count; ++i)
            {
                if (listPontos[i]._nameBD == name)
                    return i;
            }
            return -1;
        }

        // Desligar pontos: true para desligar, false para ligar
        private bool TurnPontos(bool OnOff)
        {
            if (OnOff)
            {
                this.UseWaitCursor = true;
                foreach (var ponto in listPontos)
                    Invoke((MethodInvoker)(() => ponto.Enabled = false));
                return true;
            }
            else
            {
                foreach (var ponto in listPontos)
                    Invoke((MethodInvoker)(() => ponto.Enabled = true));
                this.UseWaitCursor = false;
                return false;
            }
        }


        private void UpdateValorPontos()
        {
            DBManager DatabaseManager = new DBManager(stcon);
            while (true)
            {
                if (!this.IsHandleCreated || TheresNetAbnormality)
                    continue;

                Invoke((MethodInvoker)(() => toolStripLastUpdate.Text = "ATUALIZANDO..."));
                Invoke((MethodInvoker)(() => CarregaPontoUpdate()));
                Invoke((MethodInvoker)(() => Application.UseWaitCursor = true));

                //TurnPontos(true);

                ////Listar ponto de PRESSÃO
                //List<string[]> rs6 = DatabaseAppManager.UpdatePressaoAll(listPontos, listNamesDB);
                //for (var i = 0; i < rs6.Count; ++i)
                //{
                //    for (var j = 0; j < listPontos.Count; ++j)
                //    {
                //        if (rs6[i][1] == listPontos[j]._nameBD)
                //        {
                //            List<string[]> ArAlerta = DatabaseManager.Select("alarme_ar", "*", "id_ponto=" + Convert.ToDouble(rs6[i][3]));

                //            Invoke((MethodInvoker)(() => listPontos[j].SetValor("Pressão:" + " " + rs6[i][0] + " (m.c.a)", rs6[i][2])));

                //            if (ArAlerta.Count != 0)
                //            {
                //                Invoke((MethodInvoker)(() => listPontos[j].AlertaAr(Convert.ToInt32(ArAlerta[0][5]))));
                //                if (ArAlerta[0][5] == "1")
                //                    Invoke((MethodInvoker)(() => listPontos[j].SetValor("Pressão:" + " " + rs6[i][0] + " (m.c.a) - AR", rs6[i][2])));
                //            }
                //        }

                //    }
                //}


                //Listar VAZÃO
                List<string[]> rs = DatabaseAppManager.UpdateVazaoAll(listPontos, listNamesDB);
                for (var i = 0; i < rs.Count; ++i)
                {
                    for (var j = 0; j < listPontos.Count; ++j)
                    {
                        if (rs[i][1] == listPontos[j]._nameBD)
                            //listPontos[j].SetValor(rs[i][0]);
                            Invoke((MethodInvoker)(() => listPontos[j].SetValor("Vazão: " + " " + Math.Round(Convert.ToDecimal(rs[i][0].Replace(".", ",")), 2) + " (l/s)", rs[i][2])));
                        Invoke((MethodInvoker)(() => SetValueVazao(Math.Round(Convert.ToDecimal(rs[i][0].Replace(".", ",")), 2), listPontos[j]._nameBD)));
                    }
                }

                //Listar pontos NÍVEL
                List<string[]> rs1 = DatabaseAppManager.UpdateNivelAll(listPontos, listNamesDB);
                for (var i = 0; i < rs1.Count; ++i)
                {
                    for (var j = 0; j < listPontos.Count; ++j)
                    {
                        if (rs1[i][1] == listPontos[j]._nameBD)
                        {
                            List<string[]> NivelAlerta = DatabaseManager.Select("alarme_nivel", "*", "id_ponto=" + Convert.ToDouble(rs1[i][3]));
                            //listPontos[j].SetValor(rs[i][0]);
                            Invoke((MethodInvoker)(() => listPontos[j].SetValor("Nível: " + " " + rs1[i][0] + " (m.c.a)", rs1[i][2])));
                            Invoke((MethodInvoker)(() => SetValueNivel(Convert.ToDouble(rs1[i][0]), listPontos[j]._nameBD, Convert.ToInt32(rs1[i][3]))));

                            if (NivelAlerta.Count != 0)
                            {
                                Invoke((MethodInvoker)(() => listPontos[j].NivelCritico(Convert.ToInt32(NivelAlerta[0][4]))));
                                if (NivelAlerta[0][4] == "0")
                                {
                                    Invoke((MethodInvoker)(() => listPontos[j].SetValor("Nível: " + " " + rs1[i][0] + " (m.c.a) - NÍVEL BAIXO", rs1[i][2])));
                                }
                                else if (NivelAlerta[0][4] == "1")
                                {
                                    Invoke((MethodInvoker)(() => listPontos[j].SetValor("Nível: " + " " + rs1[i][0] + " (m.c.a) - NÍVEL ALTO", rs1[i][2])));
                                }
                            }
                        }
                    }
                }



                //Listar pontos BOMBAS
                List<string[]> rs2 = DatabaseAppManager.SelectAllBomba();
                string chave = "";
                DateTime timePontaIni = DateTime.Today;
                for (var i = 0; i < rs2.Count; ++i)
                {


                    for (var j = 0; j < listPontos.Count; ++j)
                    {
                        if (rs2[i][1] == listPontos[j]._nameBD)
                        {
                            //listPontos[j].SetValor(rs[i][0]);
                            if (rs2[i][4] == "0" && Convert.ToDateTime(rs2[i][5]).AddMinutes(10) > Convert.ToDateTime(rs2[i][2]))
                            {
                                chave = "/Manual";
                            }
                            else if (rs2[i][4] == "1" && Convert.ToDateTime(rs2[i][5]).AddMinutes(10) > Convert.ToDateTime(rs2[i][2]))
                            {
                                chave = "/Automático";
                            }
                            else
                            {
                                chave = "";
                            }
                            if (rs2[i][0] == "0")
                            {
                                Invoke((MethodInvoker)(() => listPontos[j].SetValor("Bomba: Desligada" + chave, rs2[i][2])));
                                Invoke((MethodInvoker)(() => SetValueBomba(false, listPontos[j]._nameBD)));
                            }

                            if (rs2[i][0] == "1")
                            {
                                Invoke((MethodInvoker)(() => listPontos[j].SetValor("Bomba: Ligada" + chave, rs2[i][2])));
                                Invoke((MethodInvoker)(() => SetValueBomba(true, listPontos[j]._nameBD)));
                            }
                        }

                    }
                }

                ////Listar pontos pH
                //List<string[]> rs3 = DatabaseAppManager.UpdatePhAll(listPontos, listNamesDB);
                //for (var i = 0; i < rs3.Count; ++i)
                //{
                //    for (var j = 0; j < listPontos.Count; ++j)
                //    {
                //        if (rs3[i][1] == listPontos[j]._nameBD)
                //            //listPontos[j].SetValor(rs[i][0]);
                //            Invoke((MethodInvoker)(() => listPontos[j].SetValor("pH:" + " " + rs3[i][0], rs3[i][2])));
                //    }
                //}

                //Listar ponto de ENERGIA
                List<string[]> rs4 = DatabaseAppManager.UpdateEnergiaAll(listPontos, listNamesDB);
                for (var i = 0; i < rs4.Count; ++i)
                {
                    for (var j = 0; j < listPontos.Count; ++j)
                    {
                        if (rs4[i][1] == listPontos[j]._nameBD)
                        {
                            //listPontos[j].SetValor(rs[i][0]);
                            Invoke((MethodInvoker)(() => listPontos[j].SetValor("Corrente:" + " " + rs4[i][0] + " (A)", rs4[i][2])));

                        }
                    }
                }

                ////Listar ponto de TURBIDEZ
                //List<string[]> rs5 = DatabaseAppManager.UpdateTurbidezAll(listPontos, listNamesDB);
                //for (var i = 0; i < rs5.Count; ++i)
                //{
                //    for (var j = 0; j < listPontos.Count; ++j)
                //    {
                //        if (rs5[i][1] == listPontos[j]._nameBD)
                //            Invoke((MethodInvoker)(() => listPontos[j].SetValor("Turbidez:" + " " + rs5[i][0] + " (NTU)", rs5[i][2])));
                //    }
                //}

                //Listar ponto de PRESSÃO
                List<string[]> rs6 = DatabaseAppManager.UpdatePressaoAll(listPontos, listNamesDB);
                for (var i = 0; i < rs6.Count; ++i)
                {
                    for (var j = 0; j < listPontos.Count; ++j)
                    {
                        if (rs6[i][1] == listPontos[j]._nameBD)
                        {
                            List<string[]> ArAlerta = DatabaseManager.Select("alarme_ar", "*", "id_ponto=" + Convert.ToDouble(rs6[i][3]));

                            Invoke((MethodInvoker)(() => listPontos[j].SetValor("Pressão:" + " " + rs6[i][0] + " (m.c.a)", rs6[i][2])));

                            if (ArAlerta.Count != 0)
                            {
                                Invoke((MethodInvoker)(() => listPontos[j].AlertaAr(Convert.ToInt32(ArAlerta[0][5]))));
                                if (ArAlerta[0][5] == "1")
                                {
                                    Invoke((MethodInvoker)(() => listPontos[j].SetValor("Pressão:" + " " + rs6[i][0] + " (m.c.a) - AR", rs6[i][2])));
                                }
                            }
                        }

                    }
                }

                //Listar ponto de INVERSOR
                List<string[]> rs7 = DatabaseAppManager.UpdateInversorAll(listPontos, listNamesDB);
                for (var i = 0; i < rs7.Count; ++i)
                {
                    for (var j = 0; j < listPontos.Count; ++j)
                    {
                        if (rs7[i][1] == listPontos[j]._nameBD)
                        {
                            Invoke((MethodInvoker)(() => listPontos[j].SetValor("Frequência:" + " " + rs7[i][0] + " (Hz)", rs7[i][2])));
                        }
                    }
                }


                //Desativa os menus do Relatorio dos compontes que não tem no banco de dados
                if (rs.Count == 0)
                {
                    vazaoToolStripMenuItem.Enabled = true;
                    vazaoToolStripMenuItem.Visible = true;
                }

                if (rs1.Count == 0)
                {
                    nívelToolStripMenuItem.Enabled = true;
                    nívelToolStripMenuItem.Visible = true;
                }
                if (rs2.Count == 0)
                {
                    bombaToolStripMenuItem.Enabled = true;
                    bombaToolStripMenuItem.Visible = true;
                }
                //if (rs3.Count == 0)
                //{
                //    pHToolStripMenuItem.Enabled = false;
                //    pHToolStripMenuItem.Visible = false;
                //}

                if (rs4.Count == 0)
                {
                    Invoke((MethodInvoker)(() => energiaToolStripMenuItem.Enabled = false));
                    Invoke((MethodInvoker)(() => energiaToolStripMenuItem.Visible = false));
                }

                //if (rs5.Count == 0)
                //{
                //    turbidezToolStripMenuItem.Enabled = false;
                //    turbidezToolStripMenuItem.Visible = false;
                //}
                if (rs6.Count == 0)
                {
                    pressãoToolStripMenuItem.Enabled = true;
                    pressãoToolStripMenuItem.Visible = true;
                }

                //if (rs7.Count == 0)
                //{
                //    inversorToolStripMenuItem.Enabled = true;
                //    inversorToolStripMenuItem.Visible = true;
                //}

                RefreshLastUpdateTime();
                Invoke((MethodInvoker)(() => Application.UseWaitCursor = false));

                Thread.Sleep(1000 * 1000);// Diminui de 65 para 35 
            }
        }



        private void RefreshLastUpdateTime()
        {
            string last = DatabaseAppManager.GetServerTime();
            Invoke((MethodInvoker)(() => toolStripLastUpdate.Text = "Última Atualização: " + last));
            //if (_alertaSom > 0)
            //{
            //    playSimpleSound();
            //}
        }

        private void FilltoolStripVazao(string rs)
        {
            try
            {
                string text = "Vazão total de produção: ";
                text += Math.Round(Convert.ToDecimal(rs.ToString()), 2) / 1000 + " m³/H";
                toolStripAlarmeEvento.Text = text;
            }
            catch
            {

            }
        }


        private void send_email()
        {
            while (true)
            {
                bool envia = DatabaseAppManager.returnquant();
                if (envia == true)
                {
                    long soma = DatabaseAppManager.Volumeproduzido();

                    Thread novaThread = new Thread(new ParameterizedThreadStart(Send));
                    novaThread.Start(soma);
                }
                List<string[]> result = DatabaseAppManager.manutencao();
                if (result.Count > 0)
                {
                    Invoke((MethodInvoker)(() => variaveis(result)));
                }
                Thread.Sleep(1000 * 60 * 60 * 2);
            }
        }

        private void variaveis(List<string[]> result)
        {
            if (result.Count > 0)
            {
                AlertaPreventiva newform1 = new AlertaPreventiva(result);
                newform1.ShowDialog();
            }
        }

        void SetValueNivel(double nivel, string ponto, int id)
        {
            //CHAMCIA
            DBManager DatabaseManager = new DBManager(stcon);
            List<string[]> NivelMax = DatabaseManager.Select("ponto", "max_reservatorio", "id_ponto=" + id);
            List<string[]> NivelAlerta = DatabaseManager.Select("alarme_nivel", "*", "id_ponto=" + id);

            ReservatorioLiquidos.Tanque Tanque;
            // ReservatorioLiquidos.Tanque Tanque1;
            Tanque = chamciaTanque1;

            if (ponto == "chamciaTanque2" || ponto == "CHANCIA NIVEL 2" || ponto == "CHANCIA R. METAL")
            {
                chamciaTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                chamciaTanque1.LabelValue.Text = nivel.ToString();

                chamciaTanque2.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                chamciaTanque2.LabelValue.Text = nivel.ToString();

                chamciaTanque3.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                chamciaTanque3.LabelValue.Text = nivel.ToString();

                RChanciaN2.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RChanciaN2.Tanque1.LabelValue.Text = nivel.ToString();


                Tanque = chamciaTanque1;
                Tanque = RChanciaN2.Tanque1;


            }

            if (ponto == "chamciaTanque1" || ponto == "CHANCIA NIVEL 1" || ponto == "CHAMCIA R. ALVENARIA")
            {
                chamciaTanque4.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                chamciaTanque4.LabelValue.Text = nivel.ToString();

                RChanciaNivel1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RChanciaNivel1.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = chamciaTanque4;

                Tanque = RChanciaNivel1.Tanque1;

            }

            //SÃO BENEDITO
            if (ponto == "beneditoTanque1" || ponto == "S.BENEDITO NIVEL 1" || ponto == "S.BENEDITO R.ALVENARIA")
            {
                beneditoTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                beneditoTanque1.LabelValue.Text = nivel.ToString();
                RsBeneditoN1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RsBeneditoN1.Tanque1.LabelValue.Text = nivel.ToString();
                Tanque = beneditoTanque1;
                Tanque = RsBeneditoN1.Tanque1;
            }
            if (ponto == "beneditoTanque2" || ponto == "S.BENEDITO NIVEL 2" || ponto == "S.BENEDITO R.METAL")
            {
                beneditoTanque2.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                beneditoTanque2.LabelValue.Text = nivel.ToString();
                RsBeneditoN2.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RsBeneditoN2.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = beneditoTanque2;
                Tanque = RsBeneditoN2.Tanque1;
            }

            //OURO VERDE
            if (ponto == "ouroTanque1" || ponto == "OURO V. NIVEL 1")
            {
                ouroTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                ouroTanque1.LabelValue.Text = nivel.ToString();
                RouroVerdeN1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RouroVerdeN1.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = ouroTanque1;
                Tanque = RouroVerdeN1.Tanque1;
            }
            if (ponto == "ouroTanque2" || ponto == "OURO V. NIVEL 2")
            {
                ouroTanque2.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                ouroTanque2.LabelValue.Text = nivel.ToString();

                ouroTanque3.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                ouroTanque3.LabelValue.Text = nivel.ToString();
                RouroVN2.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RouroVN2.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = ouroTanque2;
                Tanque = RouroVN2.Tanque1;
            }

            //SÃO SEBASTIÃO
            if (ponto == "sebastiaoTanque1" || ponto == "S.SEBASTIAO NIVEL 1")
            {
                sebastiaoTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                sebastiaoTanque1.LabelValue.Text = nivel.ToString();

                sebastiaoTanque2.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                sebastiaoTanque2.LabelValue.Text = nivel.ToString();

                RSSebastiaoN1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RSSebastiaoN1.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = sebastiaoTanque1;
                Tanque = RSSebastiaoN1.Tanque1;
            }

            //INDEPENDENCIA
            if (ponto == "idependenciaTanque1" || ponto == "INDEPENDENCIA NIVEL 1" || ponto == "INDEPENDEN. NIVEL 1" || ponto == "INDEPENDEN. R.METAL")
            {
                independenciaTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                independenciaTanque1.LabelValue.Text = nivel.ToString();

                independenciaTanque2.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                independenciaTanque2.LabelValue.Text = nivel.ToString();

                independenciaTanque3.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                independenciaTanque3.LabelValue.Text = nivel.ToString();

                RIndependenciaN1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RIndependenciaN1.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = independenciaTanque1;
                Tanque = RIndependenciaN1.Tanque1;
            }

            if (ponto == "idependenciaTanque4" || ponto == "INDEPENDENCIA NIVEL 2" || ponto == "INDEPENDEN. NIVEL 2" || ponto == "INDEPENDEN. R.ALVENARIA")
            {
                independenciaTanque4.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                independenciaTanque4.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                independenciaTanque4.LabelValue.Text = nivel.ToString();

                RIndependenciaN2.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RIndependenciaN2.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = independenciaTanque4;
                Tanque = RIndependenciaN2.Tanque1;
            }

            //ESTADUAL
            if (ponto == "estadualTanque1" || ponto == "ESTADUAL NIVEL 1")
            {
                estadualTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                estadualTanque1.LabelValue.Text = nivel.ToString();
                REstadualN1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                REstadualN1.Tanque1.LabelValue.Text = nivel.ToString();
                Tanque = estadualTanque1;
                Tanque = REstadualN1.Tanque1;
            }

            if (ponto == "estadualTanque2" || ponto == "ESTADUAL NIVEL 2")
            {
                estadualTanque2.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                estadualTanque2.LabelValue.Text = nivel.ToString();
                REstadualN2.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                REstadualN2.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = estadualTanque2;
                Tanque = REstadualN2.Tanque1;
            }


            //GUTIERREZ
            if (ponto == "gutierrezTanque1" || ponto == "GUTIERREZ NIVEL 1")
            {
                gutierrezTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                gutierrezTanque1.LabelValue.Text = nivel.ToString();
                RGuitierrezN1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RGuitierrezN1.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = RGuitierrezN1.Tanque1;
                Tanque = gutierrezTanque1;
            }

            //JARDIM BOTANICO
            if (ponto == "jardimTanque1" || ponto == "J.BOTANICO NIVEL 1")
            {
                jardimTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                jardimTanque1.LabelValue.Text = nivel.ToString();
                RJBN1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RJBN1.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = jardimTanque1;
                Tanque = RJBN1.Tanque1;
            }

            //FÁTIMA
            if (ponto == "fatimaTanque1" || ponto == "FATIMA NIVEL 1")
            {
                fatimaTanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                fatimaTanque1.LabelValue.Text = nivel.ToString();

                fatimaTanque2.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                fatimaTanque2.LabelValue.Text = nivel.ToString();

                RFatimaN1.Tanque1.Value = (nivel / float.Parse(NivelMax[0][0])) * 100;
                RFatimaN1.Tanque1.LabelValue.Text = nivel.ToString();

                Tanque = fatimaTanque1;
                Tanque = RFatimaN1.Tanque1;
            }
            int Cont1 = 0;
            int Cont2 = 0;
            if (NivelAlerta.Count != 0)
            {
                int status = Convert.ToInt32(NivelAlerta[0][4]);

                if (NivelAlerta[0][4] == "1")
                {
                    //Thread novaThread = new Thread(new ParameterizedThreadStart(NivelAlto));
                    //novaThread.Start(Tanque);
                    NivelAlto(Tanque);
                    //  _alertaSom++;

                }
                else if (NivelAlerta[0][4] == "0")
                {
                    //Thread novaThread = new Thread(new ParameterizedThreadStart(NivelBaixo));
                    //novaThread.Start(Tanque);
                    NivelBaixo(Tanque);
                    //_alertaSom++;
                }
                else
                {
                    Tanque.CorVidro = Color.Transparent;
                }


            }
            // _alertaSom = Cont1 + Cont2;


        }

        void SetValueBomba(bool status, string ponto)
        {
            //CHAMCIA BOMBA
            if (ponto == "chamciaBomba1" || ponto == "CHANCIA BOMBA 1")
            {
                chamciaBomba1.LigarBomba = status;
                if (status)
                {
                    chamciaCano1Bomba1.fluxo = -1;
                    chamciaCano2Bomba1.fluxo = -1;
                    chamciaCano3Bomba1.fluxo = 1;
                }
                else
                {
                    chamciaCano1Bomba1.fluxo = 0;
                    chamciaCano2Bomba1.fluxo = 0;
                    chamciaCano3Bomba1.fluxo = 0;
                }
            }
            if (ponto == "chamciaBomba2" || ponto == "CHANCIA BOMBA 2")
            {
                chamciaBomba2.LigarBomba = status;
                if (status)
                {
                    chamciaCano1Bomba2.fluxo = -1;
                    chamciaCano2Bomba2.fluxo = -1;
                    chamciaCano3Bomba2.fluxo = 1;
                }
                else
                {
                    chamciaCano1Bomba2.fluxo = 0;
                    chamciaCano2Bomba2.fluxo = 0;
                    chamciaCano3Bomba2.fluxo = 0;
                }
            }
            if (ponto == "chamciaBomba3" || ponto == "CHANCIA BOMBA 3")
            {
                chamciaBomba3.LigarBomba = status;
                if (status)
                {
                    chamciaCano1Bomba3.fluxo = -1;
                    chamciaCano2Bomba3.fluxo = -1;
                    chamciaCano3Bomba3.fluxo = 1;
                }
                else
                {
                    chamciaCano1Bomba3.fluxo = 0;
                    chamciaCano2Bomba3.fluxo = 0;
                    chamciaCano3Bomba3.fluxo = 0;
                }
            }
            if (ponto == "chamciaBomba4" || ponto == "CHANCIA BOMBA 4")
            {
                chamciaBomba4.LigarBomba = status;
                beneditoBombaChamcia.LigarBomba = status;
                estadualBombaChamcia.LigarBomba = status;
                if (status)
                {
                    chamciaCano1Bomba4.fluxo = -1;
                    chamciaCano2Bomba4.fluxo = -1;
                    chamciaCano3Bomba4.fluxo = -1;
                    beneditoCanoChamcia.fluxo = -1;
                    beneditoTanque2.CanoCheio = true;
                    estadualCanoChamcia.fluxo = -1;
                    estadualTanque1.CanoCheio = true;
                }
                else
                {
                    chamciaCano1Bomba4.fluxo = 0;
                    chamciaCano2Bomba4.fluxo = 0;
                    chamciaCano3Bomba4.fluxo = 0;
                    beneditoCanoChamcia.fluxo = 0;
                    beneditoTanque2.CanoCheio = false;
                    estadualCanoChamcia.fluxo = 0;
                    estadualTanque1.CanoCheio = false;
                }
            }

            //CHAMCIA POÇO
            if (ponto == "chamciaPocoSolt1" || ponto == "CHANCIA POCO SOLT 1")
            {
                chamciaPocoSolt1.LigarBomba = status;
                if (status)
                {
                    chamciaCano1PocoSolt1.fluxo = 1;
                    chamciaCano2PocoSolt1.fluxo = 1;
                    chamciaCano3PocoSolt1.fluxo = 1;
                    chamciaTanque4.CanoCheio = true;
                }
                else
                {
                    chamciaCano1PocoSolt1.fluxo = 0;
                    chamciaCano2PocoSolt1.fluxo = 0;
                    chamciaCano3PocoSolt1.fluxo = 0;
                    chamciaTanque4.CanoCheio = false;
                }
            }
            if (ponto == "chamciaPocoSolt2" || ponto == "CHANCIA POCO SOLT 2")
            {
                chamciaPocoSolt2.LigarBomba = status;
                if (status)
                {
                    chamciaCano1PocoSolt1.fluxo = 1;
                    chamciaCano2PocoSolt1.fluxo = 1;
                    chamciaCano3PocoSolt1.fluxo = 1;
                    chamciaTanque4.CanoCheio = true;
                    chamciaCanoPocoSolt2.fluxo = 1;
                }
                else
                {
                    chamciaCanoPocoSolt2.fluxo = 0;
                }
            }

            if (ponto == "chamciaPoco1" || ponto == "CHANCIA POCO 1")
            {
                chamciaPoco1.LigarBomba = status;
                if (status)
                {
                    chamciaCanoPoco1.fluxo = -1;
                    chamciaTanque1.CanoCheio = true;
                }
                else
                {
                    chamciaCanoPoco1.fluxo = 0;
                    chamciaTanque1.CanoCheio = false;
                }
            }
            if (ponto == "chamciaPoco2" || ponto == "CHANCIA POCO 2")
            {
                chamciaPoco2.LigarBomba = status;
                if (status)
                {
                    chamciaCanoPoco1.fluxo = -1;
                    chamciaCanoPoco2.fluxo = -1;
                    chamciaTanque1.CanoCheio = true;
                }
                else
                {
                    chamciaCanoPoco2.fluxo = 0;
                }
            }
            if (ponto == "chamciaPoco3" || ponto == "CHANCIA POCO 3")
            {
                chamciaPoco3.LigarBomba = status;
                if (status)
                {
                    chamciaCanoPoco1.fluxo = -1;
                    chamciaCanoPoco2.fluxo = -1;
                    chamciaCanoPoco3.fluxo = -1;
                    chamciaTanque1.CanoCheio = true;
                }
                else
                {
                    chamciaCanoPoco3.fluxo = 0;
                }
            }
            if (ponto == "chamciaPoco4" || ponto == "CHANCIA POCO 4")
            {
                chamciaPoco4.LigarBomba = status;
                if (status)
                {
                    chamciaCanoPoco1.fluxo = -1;
                    chamciaCanoPoco2.fluxo = -1;
                    chamciaCanoPoco3.fluxo = -1;
                    chamciaCanoPoco4.fluxo = -1;
                    chamciaTanque1.CanoCheio = true;
                }
                else
                {
                    chamciaCanoPoco4.fluxo = 0;
                }
            }
            if (ponto == "chamciaPoco5" || ponto == "CHANCIA POCO 5")
            {
                chamciaPoco5.LigarBomba = status;
                if (status)
                {
                    chamciaCanoPoco1.fluxo = -1;
                    chamciaCanoPoco2.fluxo = -1;
                    chamciaCanoPoco3.fluxo = -1;
                    chamciaCanoPoco4.fluxo = -1;
                    chamciaCanoPoco5.fluxo = -1;
                    chamciaTanque1.CanoCheio = true;
                }
                else
                {
                    chamciaCanoPoco5.fluxo = 0;
                }
            }
            if (ponto == "chamciaPoco6" || ponto == "CHANCIA POCO 6")
            {
                chamciaPoco6.LigarBomba = status;
                if (status)
                {
                    chamciaCanoPoco1.fluxo = -1;
                    chamciaCanoPoco2.fluxo = -1;
                    chamciaCanoPoco3.fluxo = -1;
                    chamciaCanoPoco4.fluxo = -1;
                    chamciaCanoPoco5.fluxo = -1;
                    chamciaCanoPoco6.fluxo = -1;
                    chamciaTanque1.CanoCheio = true;
                }
                else
                {
                    chamciaCanoPoco6.fluxo = 0;
                }
            }
            if (ponto == "chamciaPoco7" || ponto == "CHANCIA POCO 7")
            {
                chamciaPoco7.LigarBomba = status;
                if (status)
                {
                    chamciaCanoPoco1.fluxo = -1;
                    chamciaCanoPoco2.fluxo = -1;
                    chamciaCanoPoco3.fluxo = -1;
                    chamciaCanoPoco4.fluxo = -1;
                    chamciaCanoPoco5.fluxo = -1;
                    chamciaCanoPoco6.fluxo = -1;
                    chamciaCanoPoco7.fluxo = -1;
                    chamciaTanque1.CanoCheio = true;
                }
                else
                {
                    chamciaCanoPoco7.fluxo = 0;
                }
            }

            //SÃO BENEDITO BOMBAS

            if (ponto == "beneditoBomba1" || ponto == "S.BENEDITO BOMBA 1")
            {
                beneditoBomba1.LigarBomba = status;
                if (status)
                {
                    beneditoCano1Bomba1.fluxo = -1;
                    beneditoCano2Bomba1.fluxo = -1;
                    beneditoCano3Bomba1.fluxo = -1;
                }
                else
                {
                    beneditoCano1Bomba1.fluxo = 0;
                    beneditoCano2Bomba1.fluxo = 0;
                    beneditoCano3Bomba1.fluxo = 0;
                }
            }
            if (ponto == "beneditoBomba2" || ponto == "S.BENEDITO BOMBA 2")
            {
                beneditoBomba2.LigarBomba = status;
                if (status)
                {
                    beneditoCano1Bomba2.fluxo = -1;
                    beneditoCano2Bomba2.fluxo = -1;
                    beneditoCano3Bomba2.fluxo = -1;
                }
                else
                {
                    beneditoCano1Bomba2.fluxo = 0;
                    beneditoCano2Bomba2.fluxo = 0;
                    beneditoCano3Bomba2.fluxo = 0;
                }
            }
            if (ponto == "beneditoBomba3" || ponto == "S.BENEDITO BOMBA 3")
            {
                beneditoBomba3.LigarBomba = status;
                if (status)
                {
                    beneditoCano1Bomba3.fluxo = -1;
                    beneditoCano2Bomba3.fluxo = -1;
                    beneditoCano3Bomba3.fluxo = 1;
                }
                else
                {
                    beneditoCano1Bomba3.fluxo = 0;
                    beneditoCano2Bomba3.fluxo = 0;
                    beneditoCano3Bomba3.fluxo = 0;
                }
            }

            //SÃO BENEDITO POÇOS
            if (ponto == "beneditoPocoSolt1" || ponto == "S.BENEDITO POCO SOLT 1")
            {
                beneditoPocoSolt1.LigarBomba = status;
                if (status)
                {
                    beneditoCano1PocoSolt1.fluxo = 1;
                    beneditoCano2PocoSolt1.fluxo = 1;
                    beneditoCano3PocoSolt1.fluxo = 1;
                    beneditoTanque1.CanoCheio = true;
                }
                else
                {
                    beneditoCano1PocoSolt1.fluxo = 0;
                    beneditoCano2PocoSolt1.fluxo = 0;
                    beneditoCano3PocoSolt1.fluxo = 0;
                    beneditoTanque1.CanoCheio = false;
                }
            }
            if (ponto == "beneditoPocoSolt2" || ponto == "S.BENEDITO POCO SOLT 2")
            {
                beneditoPocoSolt2.LigarBomba = status;
                if (status)
                {
                    beneditoCano1PocoSolt1.fluxo = 1;
                    beneditoCano2PocoSolt1.fluxo = 1;
                    beneditoCano3PocoSolt1.fluxo = 1;
                    beneditoTanque1.CanoCheio = true;
                    beneditoCanoPocoSolt2.fluxo = 1;
                }
                else
                {
                    beneditoCanoPocoSolt2.fluxo = 0;
                }
            }
            if (ponto == "beneditoPoco1" || ponto == "S.BENEDITO POCO 1")
            {
                beneditoPoco1.LigarBomba = status;
                if (status)
                {
                    beneditoCanoPoco1.fluxo = -1;
                    beneditoTanque2.CanoCheio = true;
                }
                else
                {
                    beneditoCanoPoco1.fluxo = 0;
                }
            }

            //OURO VERDE
            if (ponto == "ouroPoco1" || ponto == "OURO V. POCO 1")
            {
                ouroPoco1.LigarBomba = status;
                if (status)
                {
                    ouroCanoPoco1.fluxo = -1;
                    ouroTanque1.CanoCheio = true;
                }
                else
                {
                    ouroCanoPoco1.fluxo = 0;
                    ouroTanque1.CanoCheio = false;
                }
            }
            if (ponto == "ouroPoco2" || ponto == "OURO V. POCO 2")
            {
                ouroPoco2.LigarBomba = status;
                if (status)
                {
                    ouroCanoPoco1.fluxo = -1;
                    ouroCanoPoco2.fluxo = -1;
                    ouroTanque1.CanoCheio = true;
                }
                else
                {
                    ouroCanoPoco2.fluxo = 0;
                }
            }
            if (ponto == "ouroPoco3" || ponto == "OURO V. POCO 3")
            {
                ouroPoco3.LigarBomba = status;
                if (status)
                {
                    ouroCanoPoco1.fluxo = -1;
                    ouroCanoPoco2.fluxo = -1;
                    ouroCanoPoco3.fluxo = -1;
                    ouroTanque1.CanoCheio = true;
                }
                else
                {
                    ouroCanoPoco3.fluxo = 0;
                }
            }
            if (ponto == "ouroPoco4" || ponto == "OURO V. POCO 4")
            {
                ouroPoco4.LigarBomba = status;
                if (status)
                {
                    ouroCanoPoco1.fluxo = -1;
                    ouroCanoPoco2.fluxo = -1;
                    ouroCanoPoco3.fluxo = -1;
                    ouroCanoPoco4.fluxo = -1;
                    ouroTanque1.CanoCheio = true;
                }
                else
                {
                    ouroCanoPoco4.fluxo = 0;
                }
            }
            if (ponto == "ouroPoco5" || ponto == "OURO V. POCO 5")
            {
                ouroPoco5.LigarBomba = status;
                if (status)
                {
                    ouroCanoPoco1.fluxo = -1;
                    ouroCanoPoco2.fluxo = -1;
                    ouroCanoPoco3.fluxo = -1;
                    ouroCanoPoco4.fluxo = -1;
                    ouroCanoPoco5.fluxo = -1;
                    ouroTanque1.CanoCheio = true;
                }
                else
                {
                    ouroCanoPoco5.fluxo = 0;
                }
            }
            if (ponto == "ouroPoco6" || ponto == "OURO V. POCO 6")
            {
                ouroPoco6.LigarBomba = status;
                if (status)
                {
                    ouroCanoPoco1.fluxo = -1;
                    ouroCanoPoco2.fluxo = -1;
                    ouroCanoPoco3.fluxo = -1;
                    ouroCanoPoco4.fluxo = -1;
                    ouroCanoPoco5.fluxo = -1;
                    ouroCanoPoco6.fluxo = -1;
                    ouroTanque1.CanoCheio = true;
                }
                else
                {
                    ouroCanoPoco6.fluxo = 0;
                }
            }
            if (ponto == "ouroPoco7" || ponto == "OURO V. POCO 7")
            {
                ouroPoco7.LigarBomba = status;
                if (status)
                {
                    ouroCanoPoco7.fluxo = 1;
                    ouroTanque3.CanoCheio = true;
                }
                else
                {
                    ouroCanoPoco7.fluxo = 0;
                    ouroTanque3.CanoCheio = false;
                }
            }
            //SÃO SEBASTIAO BOMBAS

            if (ponto == "sebastiaoBomba1" || ponto == "S.SEBASTIAO BOMBA 1")
            {
                sebastiaoBomba1.LigarBomba = status;
                if (status)
                {
                    sebastiaoCano1Bomba1.fluxo = -1;
                }
                else
                {
                    sebastiaoCano1Bomba1.fluxo = 0;
                }
            }
            if (ponto == "sebastiaoBomba2" || ponto == "S.SEBASTIAO BOMBA 2")
            {
                sebastiaoBomba2.LigarBomba = status;
                if (status)
                {
                    sebastiaoCano1Bomba2.fluxo = -1;
                }
                else
                {
                    sebastiaoCano1Bomba2.fluxo = 0;
                }
            }

            //SÃO SEBASTIAO POCOS
            if (ponto == "sebastiaoPoco1" || ponto == "S.SEBASTIAO POCO 1")
            {
                sebastiaoPoco1.LigarBomba = status;
                if (status)
                {
                    sebastiaoCanoPoco1.fluxo = -1;
                    sebastiaoCano2Poco1.fluxo = -1;
                    sebastiaoTanque1.CanoCheio = true;
                }
                else
                {
                    sebastiaoCanoPoco1.fluxo = 0;
                    sebastiaoCano2Poco1.fluxo = 0;
                    sebastiaoTanque1.CanoCheio = false;
                }
            }
            if (ponto == "sebastiaoPoco2" || ponto == "S.SEBASTIAO POCO 2")
            {
                sebastiaoPoco2.LigarBomba = status;
                if (status)
                {
                    sebastiaoCanoPoco1.fluxo = -1;
                    sebastiaoCanoPoco2.fluxo = -1;
                    sebastiaoCano2Poco1.fluxo = -1;
                    sebastiaoTanque1.CanoCheio = true;
                }
                else
                {
                    sebastiaoCanoPoco2.fluxo = 0;
                }
            }
            if (ponto == "sebastiaoPoco3" || ponto == "S.SEBASTIAO POCO 3")
            {
                sebastiaoPoco3.LigarBomba = status;
                if (status)
                {
                    sebastiaoCanoPoco1.fluxo = -1;
                    sebastiaoCanoPoco2.fluxo = -1;
                    sebastiaoCanoPoco3.fluxo = -1;
                    sebastiaoCano2Poco1.fluxo = -1;
                    sebastiaoTanque1.CanoCheio = true;
                }
                else
                {
                    sebastiaoCanoPoco3.fluxo = 0;
                }
            }

            if (ponto == "sebastiaoPoco4" || ponto == "S.SEBASTIAO POCO 4")
            {
                sebastiaoPoco4.LigarBomba = status;
                if (status)
                {
                    sebastiaoCanoPoco4.fluxo = -1;
                }
                else
                {
                    sebastiaoCanoPoco4.fluxo = 0;
                }
            }

            if (ponto == "sebastiaoPoco5" || ponto == "S.SEBASTIAO POCO 5")
            {
                sebastiaoPoco5.LigarBomba = status;
                if (status)
                {
                    sebastiaoCanoPoco5.fluxo = -1;
                    sebastiaoCano2Poco5.fluxo = -1;
                    sebastiaoCano3Poco5.fluxo = -1;
                    sebastiaoTanque1.CanoCheio = true;
                }
                else
                {
                    sebastiaoCanoPoco5.fluxo = 0;
                    sebastiaoCano2Poco5.fluxo = 0;
                    sebastiaoCano3Poco5.fluxo = 0;
                    //sebastiaoTanque1.CanoCheio = false;
                }
            }
            if (ponto == "sebastiaoPoco6" || ponto == "S.SEBASTIAO POCO 6")
            {
                sebastiaoPoco6.LigarBomba = status;
                if (status)
                {
                    sebastiaoCanoPoco6.fluxo = -1;
                    sebastiaoCanoPoco5.fluxo = -1;
                    sebastiaoCano2Poco5.fluxo = -1;
                    sebastiaoCano3Poco5.fluxo = -1;
                    sebastiaoTanque1.CanoCheio = true;
                }
                else
                {
                    sebastiaoCanoPoco6.fluxo = 0;
                }
            }
            if (ponto == "sebastiaoPoco7" || ponto == "S.SEBASTIAO POCO 7")
            {
                sebastiaoPoco7.LigarBomba = status;
                if (status)
                {
                    sebastiaoCanoPoco7.fluxo = -1;
                    sebastiaoCanoPoco6.fluxo = -1;
                    sebastiaoCanoPoco5.fluxo = -1;
                    sebastiaoCano2Poco5.fluxo = -1;
                    sebastiaoCano3Poco5.fluxo = -1;
                    sebastiaoTanque1.CanoCheio = true;
                }
                else
                {
                    sebastiaoCanoPoco7.fluxo = 0;
                }
            }
            //SÃO INDEPENDÊNCIA BOMBAS

            if (ponto == "independenciaBomba1" || ponto == "INDEPENDENCIA BOMBA 1" || ponto == "INDEPENDEN. BOMBA 1")
            {
                independenciaBomba1.LigarBomba = status;
                if (status)
                {
                    independenciaCano1Bomba1.fluxo = -1;
                    independenciaCano2Bomba1.fluxo = -1;
                    independenciaCano3Bomba1.fluxo = -1;
                }
                else
                {
                    independenciaCano1Bomba1.fluxo = 0;
                    independenciaCano2Bomba1.fluxo = 0;
                    independenciaCano3Bomba1.fluxo = 0;
                }
            }

            //INDEPENDENCIA POÇO
            if (ponto == "independenciaPocoSolt1" || ponto == "INDEPENDENCIA POCO SOLT 1")
            {
                independenciaPocoSolt1.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPocoSolt1.fluxo = 1;
                    independenciaTanque1.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPocoSolt1.fluxo = 0;
                    independenciaTanque1.CanoCheio = false;
                }
            }
            if (ponto == "independenciaPocoSolt2" || ponto == "INDEPENDENCIA POCO SOLT 2")
            {
                independenciaPocoSolt2.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPocoSolt1.fluxo = 1;
                    independenciaCanoPocoSolt2.fluxo = 1;
                    independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPocoSolt2.fluxo = 0;
                }
            }

            if (ponto == "independenciaPoco1" || ponto == "INDEPENDENCIA POCO 1")
            {
                independenciaPoco1.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco1.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco1.fluxo = 0;
                    //independenciaTanque4.CanoCheio = false;
                }
            }
            if (ponto == "independenciaPoco2" || ponto == "INDEPENDENCIA POCO 2")
            {
                independenciaPoco2.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco1.fluxo = 1;
                    independenciaCanoPoco2.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco2.fluxo = 0;
                }
            }
            if (ponto == "independenciaPoco3" || ponto == "INDEPENDENCIA POCO 3")
            {
                independenciaPoco3.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco1.fluxo = 1;
                    independenciaCanoPoco2.fluxo = 1;
                    independenciaCanoPoco3.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco3.fluxo = 0;
                }
            }
            if (ponto == "independenciaPoco4" || ponto == "INDEPENDENCIA POCO 4")
            {
                independenciaPoco4.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco1.fluxo = 1;
                    independenciaCanoPoco2.fluxo = 1;
                    independenciaCanoPoco3.fluxo = 1;
                    independenciaCanoPoco4.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco4.fluxo = 0;
                }
            }
            if (ponto == "independenciaPoco5" || ponto == "INDEPENDENCIA POCO 5")
            {
                independenciaPoco5.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco1.fluxo = 1;
                    independenciaCanoPoco2.fluxo = 1;
                    independenciaCanoPoco3.fluxo = 1;
                    independenciaCanoPoco4.fluxo = 1;
                    independenciaCanoPoco5.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco5.fluxo = 0;
                }
            }
            if (ponto == "independenciaPoco6" || ponto == "INDEPENDENCIA POCO 6")
            {
                independenciaPoco6.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco1.fluxo = 1;
                    independenciaCanoPoco2.fluxo = 1;
                    independenciaCanoPoco3.fluxo = 1;
                    independenciaCanoPoco4.fluxo = 1;
                    independenciaCanoPoco5.fluxo = 1;
                    independenciaCanoPoco6.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco6.fluxo = 0;
                }
            }

            //BLOCO POCOS INDEPENDENCIA 7 AO 11

            if (ponto == "independenciaPoco7" || ponto == "INDEPENDENCIA POCO 7")
            {
                independenciaPoco7.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco7.fluxo = 1;
                    independenciaCano1Poco7.fluxo = 1;
                    independenciaCano2Poco7.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco7.fluxo = 0;
                    independenciaCano1Poco7.fluxo = 0;
                    independenciaCano2Poco7.fluxo = 0;
                    //independenciaTanque4.CanoCheio = false;
                }
            }
            if (ponto == "independenciaPoco8" || ponto == "INDEPENDENCIA POCO 8")
            {
                independenciaPoco8.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco7.fluxo = 1;
                    independenciaCano1Poco7.fluxo = 1;
                    independenciaCano2Poco7.fluxo = 1;
                    independenciaCanoPoco8.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco8.fluxo = 0;
                }
            }
            if (ponto == "independenciaPoco9" || ponto == "INDEPENDENCIA POCO 9")
            {
                independenciaPoco9.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco7.fluxo = 1;
                    independenciaCano1Poco7.fluxo = 1;
                    independenciaCano2Poco7.fluxo = 1;
                    independenciaCanoPoco8.fluxo = 1;
                    independenciaCanoPoco9.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco9.fluxo = 0;
                }
            }
            if (ponto == "independenciaPoco10" || ponto == "INDEPENDENCIA POCO 10")
            {
                independenciaPoco10.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco7.fluxo = 1;
                    independenciaCano1Poco7.fluxo = 1;
                    independenciaCano2Poco7.fluxo = 1;
                    independenciaCanoPoco8.fluxo = 1;
                    independenciaCanoPoco9.fluxo = 1;
                    independenciaCanoPoco10.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco10.fluxo = 0;
                }
            }
            if (ponto == "independenciaPoco11" || ponto == "INDEPENDENCIA POCO 11")
            {
                independenciaPoco11.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco7.fluxo = 1;
                    independenciaCano1Poco7.fluxo = 1;
                    independenciaCano2Poco7.fluxo = 1;
                    independenciaCanoPoco8.fluxo = 1;
                    independenciaCanoPoco9.fluxo = 1;
                    independenciaCanoPoco10.fluxo = 1;
                    independenciaCanoPoco11.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco11.fluxo = 0;
                }
            }
            if (ponto == "independenciaPoco12" || ponto == "INDEPENDENCIA POCO 12")
            {
                independenciaPoco12.LigarBomba = status;
                if (status)
                {
                    independenciaCanoPoco7.fluxo = 1;
                    independenciaCano1Poco7.fluxo = 1;
                    independenciaCano2Poco7.fluxo = 1;
                    independenciaCanoPoco8.fluxo = 1;
                    independenciaCanoPoco9.fluxo = 1;
                    independenciaCanoPoco10.fluxo = 1;
                    independenciaCanoPoco11.fluxo = 1;
                    independenciaCanoPoco12.fluxo = 1;
                    //independenciaTanque4.CanoCheio = true;
                }
                else
                {
                    independenciaCanoPoco12.fluxo = 0;
                }
            }

            //ESTADUAL BOMBAS
            if (ponto == "estadualBomba1" || ponto == "ESTADUAL BOMBA 1")
            {
                estadualBomba1.LigarBomba = status;
                if (status)
                {
                    estadualCano1Bomba1.fluxo = -1;
                    estadualCano2Bomba1.fluxo = -1;
                    estadualCano3Bomba1.fluxo = -1;
                }
                else
                {
                    estadualCano1Bomba1.fluxo = 0;
                    estadualCano2Bomba1.fluxo = 0;
                    estadualCano3Bomba1.fluxo = 0;
                }
            }
            if (ponto == "estadualBomba2" || ponto == "ESTADUAL BOMBA 2")
            {
                estadualBomba2.LigarBomba = status;
                if (status)
                {
                    estadualCano1Bomba2.fluxo = -1;
                    estadualCano2Bomba2.fluxo = -1;
                    estadualCano3Bomba2.fluxo = -1;
                }
                else
                {
                    estadualCano1Bomba2.fluxo = 0;
                    estadualCano2Bomba2.fluxo = 0;
                    estadualCano3Bomba2.fluxo = 0;
                }
            }

            //ESTADUAL POÇOS
            if (ponto == "estadualPoco1" || ponto == "ESTADUAL POCO 1")
            {
                estadualPoco1.LigarBomba = status;
                if (status)
                {
                    estadualCanoPoco1.fluxo = -1;
                    estadualTanque1.CanoCheio = true;
                }
                else
                {
                    estadualCanoPoco1.fluxo = 0;
                    estadualTanque1.CanoCheio = false;
                }
            }
            if (ponto == "estadualPoco2" || ponto == "ESTADUAL POCO 2")
            {
                estadualPoco2.LigarBomba = status;
                if (status)
                {
                    estadualCanoPoco1.fluxo = -1;
                    estadualCanoPoco2.fluxo = -1;
                    estadualTanque1.CanoCheio = true;
                }
                else
                {
                    estadualCanoPoco2.fluxo = 0;
                }
            }
            if (ponto == "estadualPoco3" || ponto == "ESTADUAL POCO 3")
            {
                estadualPoco3.LigarBomba = status;
                if (status)
                {
                    estadualCanoPoco1.fluxo = -1;
                    estadualCanoPoco2.fluxo = -1;
                    estadualCanoPoco3.fluxo = -1;
                    estadualTanque1.CanoCheio = true;
                }
                else
                {
                    estadualCanoPoco3.fluxo = 0;
                }
            }
            if (ponto == "estadualPoco4" || ponto == "ESTADUAL POCO 4")
            {
                estadualPoco4.LigarBomba = status;
                if (status)
                {
                    estadualCanoPoco1.fluxo = -1;
                    estadualCanoPoco2.fluxo = -1;
                    estadualCanoPoco3.fluxo = -1;
                    estadualCanoPoco4.fluxo = -1;
                    estadualTanque1.CanoCheio = true;
                }
                else
                {
                    estadualCanoPoco4.fluxo = 0;
                }
            }
            if (ponto == "estadualPoco5" || ponto == "ESTADUAL POCO 5")
            {
                estadualPoco5.LigarBomba = status;
                if (status)
                {
                    estadualCanoPoco1.fluxo = -1;
                    estadualCanoPoco2.fluxo = -1;
                    estadualCanoPoco3.fluxo = -1;
                    estadualCanoPoco4.fluxo = -1;
                    estadualCanoPoco5.fluxo = -1;
                    estadualTanque1.CanoCheio = true;
                }
                else
                {
                    estadualCanoPoco5.fluxo = 0;
                }
            }
            if (ponto == "estadualPoco6" || ponto == "ESTADUAL POCO 6")
            {
                estadualPoco6.LigarBomba = status;
                if (status)
                {
                    estadualCanoPoco1.fluxo = -1;
                    estadualCanoPoco2.fluxo = -1;
                    estadualCanoPoco3.fluxo = -1;
                    estadualCanoPoco4.fluxo = -1;
                    estadualCanoPoco5.fluxo = -1;
                    estadualCanoPoco6.fluxo = -1;
                    estadualTanque1.CanoCheio = true;
                }
                else
                {
                    estadualCanoPoco6.fluxo = 0;
                }
            }
            if (ponto == "estadualPoco7" || ponto == "ESTADUAL POCO 7")
            {
                estadualPoco7.LigarBomba = status;
                if (status)
                {
                    estadualCanoPoco1.fluxo = -1;
                    estadualCanoPoco2.fluxo = -1;
                    estadualCanoPoco3.fluxo = -1;
                    estadualCanoPoco4.fluxo = -1;
                    estadualCanoPoco5.fluxo = -1;
                    estadualCanoPoco6.fluxo = -1;
                    estadualCanoPoco7.fluxo = -1;
                    estadualTanque1.CanoCheio = true;
                }
                else
                {
                    estadualCanoPoco7.fluxo = 0;
                }
            }
            //GUTIERREZ BOMBAS
            if (ponto == "gutierrezBomba1" || ponto == "GUTIERREZ BOMBA 1")
            {
                gutierrezBomba1.LigarBomba = status;
                if (status)
                {
                    gutierrezCano1Bomba1.fluxo = -1;
                    gutierrezCano2Bomba1.fluxo = -1;
                    gutierrezCano3Bomba1.fluxo = -1;
                }
                else
                {
                    gutierrezCano1Bomba1.fluxo = 0;
                    gutierrezCano2Bomba1.fluxo = 0;
                    gutierrezCano3Bomba1.fluxo = 0;
                }
            }
            if (ponto == "gutierrezBomba2" || ponto == "GUTIERREZ BOMBA 2")
            {
                gutierrezBomba2.LigarBomba = status;
                if (status)
                {
                    gutierrezCano1Bomba2.fluxo = -1;
                    gutierrezCano2Bomba2.fluxo = -1;
                    gutierrezCano3Bomba2.fluxo = -1;
                }
                else
                {
                    gutierrezCano1Bomba2.fluxo = 0;
                    gutierrezCano2Bomba2.fluxo = 0;
                    gutierrezCano3Bomba2.fluxo = 0;
                }
            }

            //GUTIERREZ POÇO
            if (ponto == "gutierrezPoco1" || ponto == "GUTIERREZ POCO 1")
            {
                gutierrezPoco1.LigarBomba = status;
                if (status)
                {
                    gutierrezCanoPoco1.fluxo = -1;
                    gutierrezTanque1.CanoCheio = true;
                }
                else
                {
                    gutierrezCanoPoco1.fluxo = 0;
                    gutierrezTanque1.CanoCheio = false;
                }
            }
            if (ponto == "gutierrezPoco2" || ponto == "GUTIERREZ POCO 2")
            {
                gutierrezPoco2.LigarBomba = status;
                if (status)
                {
                    gutierrezCanoPoco1.fluxo = -1;
                    gutierrezCanoPoco2.fluxo = -1;
                    gutierrezTanque1.CanoCheio = true;
                }
                else
                {
                    gutierrezCanoPoco2.fluxo = 0;
                }
            }
            if (ponto == "gutierrezPoco3" || ponto == "GUTIERREZ POCO 3")
            {
                gutierrezPoco3.LigarBomba = status;
                if (status)
                {
                    gutierrezCanoPoco1.fluxo = -1;
                    gutierrezCanoPoco2.fluxo = -1;
                    gutierrezCanoPoco3.fluxo = -1;
                    gutierrezTanque1.CanoCheio = true;
                }
                else
                {
                    gutierrezCanoPoco3.fluxo = 0;
                }
            }
            if (ponto == "gutierrezPoco4" || ponto == "GUTIERREZ POCO 4")
            {
                gutierrezPoco4.LigarBomba = status;
                if (status)
                {
                    gutierrezCanoPoco1.fluxo = -1;
                    gutierrezCanoPoco2.fluxo = -1;
                    gutierrezCanoPoco3.fluxo = -1;
                    gutierrezCanoPoco4.fluxo = -1;
                    gutierrezTanque1.CanoCheio = true;
                }
                else
                {
                    gutierrezCanoPoco4.fluxo = 0;
                }
            }
            if (ponto == "gutierrezPoco5" || ponto == "GUTIERREZ POCO 5")
            {
                gutierrezPoco5.LigarBomba = status;
                if (status)
                {
                    gutierrezCanoPoco1.fluxo = -1;
                    gutierrezCanoPoco2.fluxo = -1;
                    gutierrezCanoPoco3.fluxo = -1;
                    gutierrezCanoPoco4.fluxo = -1;
                    gutierrezCanoPoco5.fluxo = -1;
                    gutierrezTanque1.CanoCheio = true;
                }
                else
                {
                    gutierrezCanoPoco5.fluxo = 0;
                }
            }

            //GUTIERREZ POÇOS 6 AO 8
            if (ponto == "gutierrezPoco6" || ponto == "GUTIERREZ POCO 6")
            {
                gutierrezPoco6.LigarBomba = status;
                if (status)
                {
                    gutierrezCanoPoco6.fluxo = 1;
                    gutierrezTanque1.CanoCheio = true;
                }
                else
                {
                    gutierrezCanoPoco6.fluxo = 0;
                    gutierrezTanque1.CanoCheio = false;
                }
            }
            if (ponto == "gutierrezPoco7" || ponto == "GUTIERREZ POCO 7")
            {
                gutierrezPoco7.LigarBomba = status;
                if (status)
                {
                    gutierrezCanoPoco6.fluxo = 1;
                    gutierrezCanoPoco7.fluxo = 1;
                    gutierrezTanque1.CanoCheio = true;
                }
                else
                {
                    gutierrezCanoPoco7.fluxo = 0;
                }
            }
            if (ponto == "gutierrezPoco8" || ponto == "GUTIERREZ POCO 8")
            {
                gutierrezPoco8.LigarBomba = status;
                if (status)
                {
                    gutierrezCanoPoco6.fluxo = 1;
                    gutierrezCanoPoco7.fluxo = 1;
                    gutierrezCanoPoco8.fluxo = 1;
                    gutierrezTanque1.CanoCheio = true;
                }
                else
                {
                    gutierrezCanoPoco8.fluxo = 0;
                }
            }

            //SÃO FATIMA BOMBAS
            if (ponto == "fatimaBomba1" || ponto == "FATIMA BOMBA 1")
            {
                fatimaBomba1.LigarBomba = status;
                if (status)
                {
                    fatimaCano1Bomba1.fluxo = -1;
                    fatimaCano2Bomba1.fluxo = -1;
                    fatimaCano3Bomba1.fluxo = -1;
                }
                else
                {
                    fatimaCano1Bomba1.fluxo = 0;
                    fatimaCano2Bomba1.fluxo = 0;
                    fatimaCano3Bomba1.fluxo = 0;
                }
            }
            if (ponto == "fatimaBomba2" || ponto == "FATIMA BOMBA 2")
            {
                fatimaBomba2.LigarBomba = status;
                if (status)
                {
                    fatimaCano1Bomba2.fluxo = -1;
                    fatimaCano2Bomba2.fluxo = -1;
                    fatimaCano3Bomba2.fluxo = -1;
                }
                else
                {
                    fatimaCano1Bomba2.fluxo = 0;
                    fatimaCano2Bomba2.fluxo = 0;
                    fatimaCano3Bomba2.fluxo = 0;
                }
            }
            if (ponto == "fatimaBomba3" || ponto == "FATIMA BOMBA 3")
            {
                fatimaBomba3.LigarBomba = status;
                if (status)
                {
                    fatimaCano1Bomba3.fluxo = -1;
                    fatimaTanque2.CanoCheio = status;
                }
                else
                {
                    fatimaCano1Bomba3.fluxo = 0;
                    fatimaTanque2.CanoCheio = status;
                }
            }
            //FÁTIMA POÇOS 1 AO 10

            if (ponto == "fatimaPoco1" || ponto == "FATIMA POCO 1")
            {
                fatimaPoco1.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco1.fluxo = 0;
                    fatimaCanoPocos.fluxo = 0;
                    fatimaTanque1.CanoCheio = false;
                }
            }
            if (ponto == "fatimaPoco2" || ponto == "FATIMA POCO 2")
            {
                fatimaPoco2.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco2.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco3" || ponto == "FATIMA POCO 3")
            {
                fatimaPoco3.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPoco3.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco3.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco4" || ponto == "FATIMA POCO 4")
            {
                fatimaPoco4.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPoco3.fluxo = -1;
                    fatimaCanoPoco4.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco4.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco5" || ponto == "FATIMA POCO 5")
            {
                fatimaPoco5.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPoco3.fluxo = -1;
                    fatimaCanoPoco4.fluxo = -1;
                    fatimaCanoPoco5.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco5.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco6" || ponto == "FATIMA POCO 6")
            {
                fatimaPoco6.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPoco3.fluxo = -1;
                    fatimaCanoPoco4.fluxo = -1;
                    fatimaCanoPoco5.fluxo = -1;
                    fatimaCanoPoco6.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco6.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco7" || ponto == "FATIMA POCO 7")
            {
                fatimaPoco7.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPoco3.fluxo = -1;
                    fatimaCanoPoco4.fluxo = -1;
                    fatimaCanoPoco5.fluxo = -1;
                    fatimaCanoPoco6.fluxo = -1;
                    fatimaCanoPoco7.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco7.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco8" || ponto == "FATIMA POCO 8")
            {
                fatimaPoco8.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPoco3.fluxo = -1;
                    fatimaCanoPoco4.fluxo = -1;
                    fatimaCanoPoco5.fluxo = -1;
                    fatimaCanoPoco6.fluxo = -1;
                    fatimaCanoPoco7.fluxo = -1;
                    fatimaCanoPoco8.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco8.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco9" || ponto == "FATIMA POCO 9")
            {
                fatimaPoco9.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPoco3.fluxo = -1;
                    fatimaCanoPoco4.fluxo = -1;
                    fatimaCanoPoco5.fluxo = -1;
                    fatimaCanoPoco6.fluxo = -1;
                    fatimaCanoPoco7.fluxo = -1;
                    fatimaCanoPoco8.fluxo = -1;
                    fatimaCanoPoco9.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco9.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco10" || ponto == "FATIMA POCO 10")
            {
                fatimaPoco10.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco1.fluxo = -1;
                    fatimaCanoPoco2.fluxo = -1;
                    fatimaCanoPoco3.fluxo = -1;
                    fatimaCanoPoco4.fluxo = -1;
                    fatimaCanoPoco5.fluxo = -1;
                    fatimaCanoPoco6.fluxo = -1;
                    fatimaCanoPoco7.fluxo = -1;
                    fatimaCanoPoco8.fluxo = -1;
                    fatimaCanoPoco9.fluxo = -1;
                    fatimaCanoPoco10.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco10.fluxo = 0;
                }
            }
            //FÁTIMA POÇOS 11 AO 18

            if (ponto == "fatimaPoco11" || ponto == "FATIMA POCO 11")
            {
                fatimaPoco11.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco11.fluxo = -1;
                    fatimaCano1Poco11.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco11.fluxo = 0;
                    fatimaCano1Poco11.fluxo = 0;
                    fatimaCanoPocos.fluxo = 0;
                    fatimaTanque1.CanoCheio = false;
                }
            }
            if (ponto == "fatimaPoco12" || ponto == "FATIMA POCO 12")
            {
                fatimaPoco12.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco11.fluxo = -1;
                    fatimaCano1Poco11.fluxo = -1;
                    fatimaCanoPoco12.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco12.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco13" || ponto == "FATIMA POCO 13")
            {
                fatimaPoco13.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco11.fluxo = -1;
                    fatimaCano1Poco11.fluxo = -1;
                    fatimaCanoPoco12.fluxo = -1;
                    fatimaCanoPoco13.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco13.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco14" || ponto == "FATIMA POCO 14")
            {
                fatimaPoco14.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco11.fluxo = -1;
                    fatimaCano1Poco11.fluxo = -1;
                    fatimaCanoPoco12.fluxo = -1;
                    fatimaCanoPoco13.fluxo = -1;
                    fatimaCanoPoco14.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco14.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco15" || ponto == "FATIMA POCO 15")
            {
                fatimaPoco5.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco11.fluxo = -1;
                    fatimaCano1Poco11.fluxo = -1;
                    fatimaCanoPoco12.fluxo = -1;
                    fatimaCanoPoco13.fluxo = -1;
                    fatimaCanoPoco14.fluxo = -1;
                    fatimaCanoPoco15.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco15.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco16" || ponto == "FATIMA POCO 16")
            {
                fatimaPoco16.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco11.fluxo = -1;
                    fatimaCano1Poco11.fluxo = -1;
                    fatimaCanoPoco12.fluxo = -1;
                    fatimaCanoPoco13.fluxo = -1;
                    fatimaCanoPoco14.fluxo = -1;
                    fatimaCanoPoco15.fluxo = -1;
                    fatimaCanoPoco16.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco16.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco17" || ponto == "FATIMA POCO 17")
            {
                fatimaPoco17.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco11.fluxo = -1;
                    fatimaCano1Poco11.fluxo = -1;
                    fatimaCanoPoco12.fluxo = -1;
                    fatimaCanoPoco13.fluxo = -1;
                    fatimaCanoPoco14.fluxo = -1;
                    fatimaCanoPoco15.fluxo = -1;
                    fatimaCanoPoco16.fluxo = -1;
                    fatimaCanoPoco17.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco17.fluxo = 0;
                }
            }
            if (ponto == "fatimaPoco18" || ponto == "FATIMA POCO 18")
            {
                fatimaPoco18.LigarBomba = status;
                if (status)
                {
                    fatimaCanoPoco11.fluxo = -1;
                    fatimaCano1Poco11.fluxo = -1;
                    fatimaCanoPoco12.fluxo = -1;
                    fatimaCanoPoco13.fluxo = -1;
                    fatimaCanoPoco14.fluxo = -1;
                    fatimaCanoPoco15.fluxo = -1;
                    fatimaCanoPoco16.fluxo = -1;
                    fatimaCanoPoco17.fluxo = -1;
                    fatimaCanoPoco18.fluxo = -1;
                    fatimaCanoPocos.fluxo = -1;
                    fatimaTanque1.CanoCheio = true;
                }
                else
                {
                    fatimaCanoPoco18.fluxo = 0;
                }
            }

            //JARDIM BOTANICO POÇO
            if (ponto == "jardimPoco1" || ponto == "J.BOTANICO POCO 1")
            {
                jardimPoco1.LigarBomba = status;
                if (status)
                {
                    jardimCanoPoco.fluxo = -1;
                    jardimCanoPoco2.fluxo = -1;
                    jardimTanque1.CanoCheio = true;
                    jardimCanoPoco1.fluxo = -1;
                }
                else
                {
                    jardimCanoPoco.fluxo = 0;
                    jardimCanoPoco2.fluxo = 0;
                    jardimTanque1.CanoCheio = false;
                    jardimCanoPoco1.fluxo = 0;
                }
            }
            if (ponto == "jardimPoco2" || ponto == "J.BOTANICO POCO 2")
            {
                jardimPoco2.LigarBomba = status;
                if (status)
                {
                    jardimCanoPoco.fluxo = -1;
                    jardimCanoPoco2.fluxo = -1;
                    jardimTanque1.CanoCheio = true;
                    jardimCano1Poco2.fluxo = -1;
                    jardimCano2Poco2.fluxo = -1;
                }
                else
                {
                    jardimCano1Poco2.fluxo = 0;
                    jardimCano2Poco2.fluxo = 0;
                }
            }
            if (ponto == "jardimPoco3" || ponto == "J.BOTANICO POCO 3")
            {
                jardimPoco3.LigarBomba = status;
                if (status)
                {
                    jardimCanoPoco.fluxo = -1;
                    jardimCanoPoco2.fluxo = -1;
                    jardimTanque1.CanoCheio = true;
                    jardimCano1Poco3.fluxo = -1;
                    jardimCano2Poco3.fluxo = -1;
                }
                else
                {
                    jardimCano1Poco3.fluxo = 0;
                    jardimCano2Poco3.fluxo = 0;
                }
            }

        }

        void SetValueVazao(decimal vazao, string ponto)
        {
            //VAZAO CHAMCIA
            if (ponto == "chamciaVazaoBomba1" || ponto == "CHAMCIA VAZAO B1")
            {
                chamciaVazaoBomba1.Text = vazao.ToString() + " (m³/h)";
            }
            if (ponto == "chamciaVazaoBomba2" || ponto == "CHAMCIA VAZAO B2")
            {
                chamciaVazaoBomba2.Text = vazao.ToString() + " (m³/h)";
            }
            if (ponto == "chamciaVazaoBomba3" || ponto == "CHAMCIA VAZAO B3")
            {
                chamciaVazaoBomba3.Text = vazao.ToString() + " (m³/h)";
            }
            if (ponto == "chamciaVazaoBomba4" || ponto == "CHAMCIA VAZAO B4")
            {
                chamciaVazaoBomba4.Text = vazao.ToString() + " (m³/h)";
            }

            //VAZAO SÃO BENEDITO
            if (ponto == "beneditoVazaoBomba1" || ponto == "S.BENEDITO VAZAO B1")
            {
                beneditoVazaoBomba1.Text = vazao.ToString() + " (m³/h)";
            }
            if (ponto == "beneditoVazaoBomba2" || ponto == "S.BENEDITO VAZAO B2")
            {
                beneditoVazaoBomba2.Text = vazao.ToString() + " (m³/h)";
            }
            if (ponto == "beneditoVazaoBomba3" || ponto == "S.BENEDITO VAZAO B3")
            {
                beneditoVazaoBomba3.Text = vazao.ToString() + " (m³/h)";
            }

            //VAZAO SÃO SEBASTIAO
            if (ponto == "sebastiaoVazaoBomba1" || ponto == "S.SEBASTIAO VAZAO B1")
            {
                sebastiaoVazaoBomba1.Text = vazao.ToString() + " (m³/h)";
            }
            if (ponto == "sebastiaoVazaoBomba2" || ponto == "S.SEBASTIAO VAZAO B2")
            {
                sebastiaoVazaoBomba2.Text = vazao.ToString() + " (m³/h)";
            }

            //VAZAO ESTADUAL
            if (ponto == "estadualVazaoBomba1" || ponto == "ESTADUAL VAZAO B1")
            {
                estadualVazaoBomba1.Text = vazao.ToString() + " (m³/h)";
            }
            if (ponto == "estadualVazaoBomba2" || ponto == "ESTADUAL VAZAO B2")
            {
                estadualVazaoBomba2.Text = vazao.ToString() + " (m³/h)";
            }

            //VAZAO FÁTIMA
            if (ponto == "fatimaVazaoBomba1" || ponto == "FATIMA VAZAO B1")
            {
                fatimaVazaoBomba1.Text = vazao.ToString() + " (m³/h)";
            }
            if (ponto == "fatimaVazaoBomba2" || ponto == "FATIMA VAZAO B2")
            {
                fatimaVazaoBomba2.Text = vazao.ToString() + " (m³/h)";
            }
        }

        private void NivelAlto(object parametro)
        {
            ReservatorioLiquidos.Tanque Tanque;
            Tanque = (ReservatorioLiquidos.Tanque)(parametro);

            //DateTime now = DateTime.Now;
            //while (now.Second < 30)
            //{
            //    Tanque.CorVidro = Color.Yellow;
            //    Thread.Sleep(1000);
            //    Tanque.BackColor = Color.Transparent;
            //    Thread.Sleep(1000);
            //}
            Tanque.CorVidro = Color.Red;
            // _alertaSom++;

        }

        private void NivelBaixo(object parametro)
        {
            ReservatorioLiquidos.Tanque Tanque;
            Tanque = (ReservatorioLiquidos.Tanque)(parametro);

            //DateTime now = DateTime.Now;
            //while(now.Second < 30)
            //{
            //    Tanque.CorVidro = Color.Red;
            //    Thread.Sleep(1000);
            //    Tanque.CorVidro = Color.Transparent;
            //    Thread.Sleep(1000);
            //}
            Tanque.CorVidro = Color.Red;
            // _alertaSom++;

        }

        //private void playSimpleSound()
        //{
        //    //int n = 0;
        //    //while (n <= _alertaSom)
        //    //{
        //        //IWavePlayer waveOutDevice = new WaveOut();
        //      //  AudioFileReader audioFileReader = new AudioFileReader(@"C:\SATS\AlarmeSATS.mp3");

        //        //waveOutDevice.Init(audioFileReader);
        //        //Thread.Sleep(6000);

        //        //waveOutDevice.Play();
        //        //Thread.Sleep(6000);

        //        SoundPlayer musica = new   SoundPlayer(@"C:\SATS\Alarm.wav");
        //        musica.Play();
        //        //n++;
        //    //01  }
        //}

        private void CheckInternet()
        {
            string message = "";
            string messag2 = "VERIFIQUE O SERVIDOR";
            string mysqlOff = "VERIFIQUE SE O BANCO DE DADOS MYSQL ESTÁ OPERANDO NORMALMENTE";
            string apacheOff = "VERIFIQUE SE O APACHE ESTÁ OPERANDO NORMALMENTE";
            bool firstIteration = true;
            while (true)
            {
                if (firstIteration)
                    firstIteration = false;
                else
                    Thread.Sleep(1000 * 30);

                if (!NetVerifier.CheckInternet())
                {
                    //TurnPontos(true);
                    Invoke((MethodInvoker)(() => toolStripAlarmeEvento.Text = message));
                    TheresNetAbnormality = true;
                    Thread.Sleep(1000 * 2);
                }

                //if (!CheckProcess("mysqld"))
                //    {
                //        Invoke((MethodInvoker)(() => toolStripAlarmeEvento.Text = mysqlOff));
                //        TheresNetAbnormality = true;
                //        continue;
                //    }

                //    if (!CheckProcess("httpd"))
                //    {
                //        Invoke((MethodInvoker)(() => toolStripAlarmeEvento.Text = apacheOff));
                //        TheresNetAbnormality = true;
                //        continue;
                //    }

                if (!NetVerifier.CheckSaga())
                {
                    //TurnPontos(true);
                    Invoke((MethodInvoker)(() => toolStripAlarmeEvento.Text = messag2));
                    TheresNetAbnormality = true;
                }
                TheresNetAbnormality = false;

                Invoke((MethodInvoker)(() => toolStripAlarmeEvento.Text = message));

                TheresNetAbnormality = false;
                //TurnPontos(false);
            }
        }
        private void CheckAlertas()
        {
            while (true)
            {
                try
                {
                    bool checkAlert = false;
                    checkAlert = DatabaseAppManager.CheckAlarmsEvents();
                    if (checkAlert)
                    {
                        AlertasGerais alert = new AlertasGerais();
                        alert.ShowDialog();
                    }
                }
                catch
                {
                }
                Thread.Sleep(1000 * 60 * 60);
            }
        }

        private void salvaPosition()
        {
            int newX;
            int newY;
            List<string[]> PontoList = new List<string[]>();
            for (int i = 0; i < listPontos.Count; i++)
            {
                string[] subitems = new string[4];
                subitems[0] = listPontos[i]._nameBD.ToString();
                subitems[1] = listPontos[i].name.ToString();
                CalculatePontosRatio();
                newX = Convert.ToInt32((1351 * XRatioList[i]));
                newY = Convert.ToInt32((611 * YRatioList[i]));
                listPontos[i].Location = new Point(newX, newY);
                subitems[2] = newX.ToString();
                subitems[3] = newY.ToString();
                PontoList.Add(subitems);
            }
            //PontoList.ForEach(i => Console.WriteLine(i[2].ToString()));
            //DatabaseAppManager.SetAllIDs(listPontos);
            //updatePonto(PontoList);
            Invoke((MethodInvoker)(() => updatePonto(PontoList)));
            //PontoList.ForEach(p =>  dB.Update("ponto_posicao","",""));
            //mXml.GeraXML(PontoList);

        }

        public void updatePonto(List<string[]> PontoList)
        {

            DBManager dB = new DBManager(stcon);

            List<string[]> allPontos = DatabaseAppManager.AllPontos();

            for (int i = 0; i < PontoList.Count; i++)
            {

                try
                {
                    dB.Update("ponto_posicao", "eixoX ='" + PontoList[i][2] + "', eixoY= '" + PontoList[i][3] + "'", "id_ponto  = '" + allPontos[i][0] + "'");

                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);                    
                }
            }


        }

        public void deletePonto(string ponto, PictureBox pb)
        {
            foreach (Control item in pb.Controls)
            {
                if (item.Name == replace(ponto.Replace(" ", "")))
                {
                    this.Controls.Remove(item);
                    item.Dispose();
                    break;
                }
            }

            //foreach (Ponto item in listPontos)
            //{
            //    if (item._nameBD == ponto)
            //    {
            //        listPontos.Remove(item);
            //        break;
            //    }
            //}
        }

        //public Ponto criaPonto(string NomeDB, string NomePonto, int x, int y)
        //{
        //    Ponto pt = new Ponto();
        //    this.Controls.Add(pt);
        //    Point p = new Point(x, y);
        //    pt.Location = p;
        //    pt.BringToFront();
        //    pt.BringToFront();
        //    pt.Visible = true;
        //    pt.Name = replace(NomeDB.Replace(" ", ""));
        //    pt.Enabled = true;
        //    pt.Parent = pictureBox1;
        //    pt.BackColor = Color.Transparent;

        //    pt.Start(NomeDB, DatabaseAppManager);
        //    listNamesDB.Add(NomeDB);
        //    pt.SetName(NomePonto);

        //    AppendlistPontos(pt);

        //    DatabaseAppManager.SetAllIDs(listPontos);
        //    //CalculatePontosRatio();
        //    return pt;
        //}

        private void Form1_Load(object sender, EventArgs e)
        {
            //InitializeComponent();
            //this.MinimumSize = this.MaximumSize = this.Size;
            //resize = true;
            ThreadEmail = new Thread(new ThreadStart(send_email));
            ThreadPontos = new Thread(new ThreadStart(UpdateValorPontos));
            ThreadInternet = new Thread(new ThreadStart(CheckInternet));
            //ThreadAlertas = new Thread(new ThreadStart(CheckAlertas));
            //ThreadVazao = new Thread(new ThreadStart(UpdateTotalvazao));
            //ThreadVazao.Start();
            ThreadPontos.Start();
            ThreadEmail.Start();
            ThreadInternet.Start();
            //ThreadAlertas.Start();
            while (!ThreadEmail.IsAlive) ;
            //while (!ThreadVazao.IsAlive) ;
            while (!ThreadPontos.IsAlive) ;
            while (!ThreadInternet.IsAlive) ;
            //while (!ThreadAlertas.IsAlive) ;
            //Thread.Sleep(1000);


            energiaToolStripMenuItem.Visible = false;
            // vazaoToolStripMenuItem.Visible = false;
            RChanciaNivel1.Tanque1.CorFluido = Color.DodgerBlue;
            RChanciaN2.Tanque1.CorFluido = Color.DodgerBlue;
            RouroVerdeN1.Tanque1.CorFluido = Color.DodgerBlue;
            RouroVN2.Tanque1.CorFluido = Color.DodgerBlue;
            REstadualN1.Tanque1.CorFluido = Color.DodgerBlue;
            REstadualN2.Tanque1.CorFluido = Color.DodgerBlue;
            RFatimaN1.Tanque1.CorFluido = Color.DodgerBlue;
            RGuitierrezN1.Tanque1.CorFluido = Color.DodgerBlue;
            RIndependenciaN1.Tanque1.CorFluido = Color.DodgerBlue;
            RIndependenciaN2.Tanque1.CorFluido = Color.DodgerBlue;
            RsBeneditoN1.Tanque1.CorFluido = Color.DodgerBlue;
            RsBeneditoN2.Tanque1.CorFluido = Color.DodgerBlue;
            RSSebastiaoN1.Tanque1.CorFluido = Color.DodgerBlue;
            RJBN1.Tanque1.CorFluido = Color.DodgerBlue;

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Tem que finalizar as threads ao sair, senao o SATS continua rodando residente
            //ThreadAlarmeEvento.Abort();
            ThreadPontos.Abort();
            //ThreadVazao.Abort();
            ThreadEmail.Abort();
            ThreadInternet.Abort();
            //ThreadAlertas.Abort();
            fecha();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            int newX;
            int newY;

            //for (var i = 0; i < listPontos.Count; ++i)
            //{
            //    newX = Convert.ToInt32(pBNivel.Width * XRatioList[i]);
            //    newY = Convert.ToInt32(pBNivel.Height * YRatioList[i]);

            //    newX = Convert.ToInt32(pBomba.Width * XRatioList[i]);
            //    newY = Convert.ToInt32(pBomba.Height * YRatioList[i]);

            //    newX = Convert.ToInt32(pBPressoes.Width * XRatioList[i]);
            //    newY = Convert.ToInt32(pBPressoes.Height * YRatioList[i]);

            //    newX = Convert.ToInt32(pBVazao.Width * XRatioList[i]);
            //    newY = Convert.ToInt32(pBVazao.Height * YRatioList[i]);

            //    listPontos[i].Location = new Point(newX, newY);
            //}
        }

        private void cadastroDeClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroUsuario newform = new CadastroUsuario();
            newform.ShowDialog();
        }

        private void cadastroDeAçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroAcao newform = new CadastroAcao();
            newform.ShowDialog();
        }

        private void importarGastosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportGastos newform = new ImportGastos();
            newform.ShowDialog();
        }

        private void importarMicromediçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportarMicromedicao newform = new ImportarMicromedicao();
            newform.ShowDialog();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void alarmesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarAlarmes newform = new ConsultarAlarmes();
            newform.ShowDialog();
        }

        private void dadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarDados newform = new ConsultarDados();
            newform.ShowDialog();
        }

        private void eventosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarEventos newform = new ConsultarEventos();
            newform.ShowDialog();
        }

        private void relatórioDescritivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioDescritivo newform = new RelatorioDescritivo();
            try
            {
                newform.ShowDialog();
            }
            catch
            {

                //   MessageBox.Show(es.ToString());
            }
        }

        private void relatórioComparativoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioComparativo newform = new RelatorioComparativo();
            newform.ShowDialog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 newform = new AboutBox1();
            newform.ShowDialog();
        }

        private void fecha()
        {
            Properties.Settings.Default.Save();
            foreach (var process in Process.GetProcessesByName("SATS"))
            {
                process.Kill();
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
            ThreadPontos.Abort();
            //ThreadVazao.Abort();
            ThreadEmail.Abort();
            //ThreadAlertas.Abort();
            ThreadInternet.Abort();
            fecha();
        }



        private void habilitarEdiçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // this.MouseDown += new MouseEventHandler(Ponto_MouseDown);
            // this.MouseMove += new MouseEventHandler(Ponto_MouseMove);
        }


        bool ativa = false;

        private void ativarEdiçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ativa == false)
            {
                ativarEdiçãoToolStripMenuItem.Text = "Salvar Edição";
                ativa = true;
                Ponto.permi = "Supervisor";
                //button1.Visible = true;
            }
            else
            {
                MessageBox.Show("Salvando mudanças!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //button1.Visible = false;
                Invoke((MethodInvoker)(() => this.salvaPosition()));
                ativarEdiçãoToolStripMenuItem.Text = "Ativar Edição de Ponto";
                ativa = false;
                Ponto.permi = "Operador";

            }

        }


        OpenFileDialog Trocaimage = new OpenFileDialog();

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    Trocaimage.Filter = "PNG|*.png";
        //    if (Trocaimage.ShowDialog() == DialogResult.OK)
        //    {
        //        pictureBox1.Image = Image.FromFile(Trocaimage.FileName);
        //    }
        //    else return;

        //    if (!System.IO.Directory.Exists(@"" + System.AppDomain.CurrentDomain.BaseDirectory + ""))
        //    {
        //        System.IO.Directory.CreateDirectory(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "");
        //    }
        //    System.IO.File.Copy(Trocaimage.FileName, @"" + System.AppDomain.CurrentDomain.BaseDirectory + "map.png", true);
        //}


        private void balançoHidricoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BalancoHidrico newform = new BalancoHidrico();
            newform.ShowDialog();
        }

        public string replace(string texto)
        {
            texto = texto.Replace("-", "");
            texto = texto.Replace("_", "");
            texto = texto.Replace("+", "");
            texto = texto.Replace("=", "");
            texto = texto.Replace("/", "");
            texto = texto.Replace(",", "");
            texto = texto.Replace("[", "");
            texto = texto.Replace("]", "");
            texto = texto.Replace("{", "");
            texto = texto.Replace("}", "");
            texto = texto.Replace("(", "");
            texto = texto.Replace(")", "");
            texto = texto.Replace("*", "");
            texto = texto.Replace("&", "");
            texto = texto.Replace("$", "");
            texto = texto.Replace("@", "");
            texto = texto.Replace("!", "");
            texto = texto.Replace("%", "");
            texto = texto.Replace("-", "");
            texto = texto.Replace(">", "");
            texto = texto.Replace("<", "");
            texto = texto.Replace(":", "");
            texto = texto.Replace(";", "");
            texto = texto.Replace("?", "");

            return texto;
        }

        public void Send(object parametro)
        {
            try
            {
                long soma = Convert.ToInt64(parametro);
                soma = soma / 1000;
                bool inserido = DatabaseAppManager.insertvolume(soma);

                if (inserido == true)
                {
                    MailMessage mailMessage = new MailMessage();
                    //Endereço que irá aparecer no e-mail do usuário
                    mailMessage.From = new MailAddress("helio@sagamedicao.com.br", "Saga Sats");
                    //destinatarios do e-mail, para incluir mais de um basta separar por ponto e virgula 
                    //mailMessage.To.Add("diego@sagamedicao.com.br");
                    mailMessage.To.Add(new MailAddress("helio@sagamedicao.com.br", "Helio"));
                    //mailMessage.To.Add(new MailAddress("anderson@sagamedicao.com.br", "Anderson"));
                    //mailMessage.To.Add(new MailAddress("saaebocaiuva@gmail.com", "Saae"));

                    DateTime HORA = DateTime.Today.AddDays(-1);

                    String CorpoEmail = "\nFoi registrada um volume de: " + soma.ToString() + " m³, no periodo de " + HORA.ToString() + " até " + DateTime.Today.AddMinutes(-1) +
                                        "\nVazão máxima total de água produzida no dia anterior." +
                                        ".\n\nAtenciosamente,\nSistema de automação e telemetria no saneamento.";

                    mailMessage.Subject = "Relatório dos pontos de Vazão Sats Araguari";

                    mailMessage.IsBodyHtml = false;
                    //conteudo do corpo do e-mail
                    mailMessage.Body = CorpoEmail.ToString();
                    mailMessage.Priority = MailPriority.High;
                    //smtp do e-mail que irá enviar
                    SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
                    smtpClient.EnableSsl = true;
                    //credenciais da conta que utilizará para enviar o e-mail
                    smtpClient.Credentials = new NetworkCredential("saga.contas", "saga*245");
                    smtpClient.Send(mailMessage);
                    //MessageBox.Show("Enviado");
                    // return true;
                }
            }
            catch
            {
            }
        }

        private void preventivaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            manunteção newform1 = new manunteção();
            newform1.ShowDialog();
        }

        private void pontoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarPonto newform1 = new CadastrarPonto(this);
            newform1.ShowDialog();
        }

        private void relatórioDeGastosToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            RelatoriodeGastos newform = new RelatoriodeGastos();
            newform.ShowDialog();
        }

        private void dadosDoPoçoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroDados newform = new CadastroDados();
            newform.ShowDialog();
        }

        private void acionamentoAutomáticoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProgramaAcionamentoAutomatico newform = new ProgramaAcionamentoAutomatico();
            newform.ShowDialog();
        }

        protected void alarmesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AlertasGerais newform = new AlertasGerais();
            newform.ShowDialog();
        }

        private void nívelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioNivel newform = new RelatorioNivel();
            newform.ShowDialog();
        }

        private void vazãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioVazao newform = new RelatorioVazao();
            newform.ShowDialog();
        }


        private void energiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioEnergia newform = new RelatorioEnergia();
            newform.ShowDialog();
        }


        private void pressãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioPressao newform = new RelatorioPressao();
            newform.ShowDialog();
        }

        private bool CheckProcess(string name)
        {
            int count = 0;
            foreach (Process services in Process.GetProcesses())
            {
                if (services.ProcessName == name)
                {
                    count++;
                }
            }
            if (count > 1)
                return true;
            else
                return false;
        }

        private void chaveSeletoraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioChave newform = new RelatorioChave();
            newform.ShowDialog();
        }

        private void inversorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioFrequencia newform = new RelatorioFrequencia();
            newform.ShowDialog();
        }

        private void configuraçãoDeAlarmeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfiguracaoAlarmeNivel newform = new ConfiguracaoAlarmeNivel();
            newform.ShowDialog();
        }

        private void configuraçãoSetPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProgramacaoSetPoint newform = new ProgramacaoSetPoint();
            newform.ShowDialog();
        }

        private void consumoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioConsumo newform = new RelatorioConsumo();
            newform.ShowDialog();
        }

        private void pressãoEFrequênciaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarPressaoFrequencia newform = new ConsultarPressaoFrequencia();
            newform.ShowDialog();
        }

        private void PSolteiroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioPocoSolteiro newform = new RelatorioPocoSolteiro();
            newform.ShowDialog();
        }
        private void VazaoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioVazao newform = new RelatorioVazao();
            newform.ShowDialog();
        }

        private void bombaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelatorioBomba newform = new RelatorioBomba();
            newform.ShowDialog();
        }
    }
}