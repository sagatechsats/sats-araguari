﻿namespace SATS
{
    //Alarme Nivel
    partial class ConfiguracaoAlarmeNivel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfiguracaoAlarmeNivel));
            this.label2 = new System.Windows.Forms.Label();
            this.txtNivelMin = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNivelMax = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.bttnCancelar = new System.Windows.Forms.Button();
            this.bttnSalvar = new System.Windows.Forms.Button();
            this.bttnNovo = new System.Windows.Forms.Button();
            this.bttnEditar = new System.Windows.Forms.Button();
            this.cBPonto = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btAcionamento = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(376, 139);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 69;
            this.label2.Text = "m.c.a";
            // 
            // txtNivelMin
            // 
            this.txtNivelMin.Enabled = false;
            this.txtNivelMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNivelMin.Location = new System.Drawing.Point(219, 136);
            this.txtNivelMin.MaxLength = 12;
            this.txtNivelMin.Name = "txtNivelMin";
            this.txtNivelMin.Size = new System.Drawing.Size(150, 22);
            this.txtNivelMin.TabIndex = 5;
            this.txtNivelMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNivelMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(86, 139);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 16);
            this.label6.TabIndex = 68;
            this.label6.Text = "Nivel mínimo Alerta:";
            // 
            // checkBox
            // 
            this.checkBox.AutoSize = true;
            this.checkBox.Enabled = false;
            this.checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox.Location = new System.Drawing.Point(118, 98);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(94, 22);
            this.checkBox.TabIndex = 4;
            this.checkBox.Text = "Habilitar ";
            this.checkBox.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(376, 172);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 16);
            this.label3.TabIndex = 72;
            this.label3.Text = "m.c.a";
            // 
            // txtNivelMax
            // 
            this.txtNivelMax.Enabled = false;
            this.txtNivelMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNivelMax.Location = new System.Drawing.Point(219, 169);
            this.txtNivelMax.MaxLength = 12;
            this.txtNivelMax.Name = "txtNivelMax";
            this.txtNivelMax.Size = new System.Drawing.Size(150, 22);
            this.txtNivelMax.TabIndex = 7;
            this.txtNivelMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNivelMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(82, 172);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 16);
            this.label4.TabIndex = 71;
            this.label4.Text = "Nível máximo Alerta:";
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerColorTint = System.Drawing.Color.CornflowerBlue;
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            // 
            // bttnCancelar
            // 
            this.bttnCancelar.Enabled = false;
            this.bttnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("bttnCancelar.Image")));
            this.bttnCancelar.Location = new System.Drawing.Point(322, 226);
            this.bttnCancelar.Name = "bttnCancelar";
            this.bttnCancelar.Size = new System.Drawing.Size(97, 35);
            this.bttnCancelar.TabIndex = 11;
            this.bttnCancelar.Text = "Cancelar";
            this.bttnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnCancelar.UseVisualStyleBackColor = true;
            this.bttnCancelar.Click += new System.EventHandler(this.bttnCancelar_Click);
            // 
            // bttnSalvar
            // 
            this.bttnSalvar.Enabled = false;
            this.bttnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnSalvar.Image = ((System.Drawing.Image)(resources.GetObject("bttnSalvar.Image")));
            this.bttnSalvar.Location = new System.Drawing.Point(219, 226);
            this.bttnSalvar.Name = "bttnSalvar";
            this.bttnSalvar.Size = new System.Drawing.Size(97, 35);
            this.bttnSalvar.TabIndex = 10;
            this.bttnSalvar.Text = "Salvar";
            this.bttnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnSalvar.UseVisualStyleBackColor = true;
            this.bttnSalvar.Click += new System.EventHandler(this.bttnSalvar_Click);
            // 
            // bttnNovo
            // 
            this.bttnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnNovo.Image = global::SATS.Properties.Resources.Crystal_Clear_action_edit_add;
            this.bttnNovo.Location = new System.Drawing.Point(116, 226);
            this.bttnNovo.Name = "bttnNovo";
            this.bttnNovo.Size = new System.Drawing.Size(97, 35);
            this.bttnNovo.TabIndex = 13;
            this.bttnNovo.Text = "Novo";
            this.bttnNovo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnNovo.UseVisualStyleBackColor = true;
            this.bttnNovo.Click += new System.EventHandler(this.bttnNovo_Click);
            // 
            // bttnEditar
            // 
            this.bttnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnEditar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_editpaste;
            this.bttnEditar.Location = new System.Drawing.Point(416, 51);
            this.bttnEditar.Name = "bttnEditar";
            this.bttnEditar.Size = new System.Drawing.Size(107, 39);
            this.bttnEditar.TabIndex = 2;
            this.bttnEditar.Text = "Editar";
            this.bttnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnEditar.UseVisualStyleBackColor = true;
            this.bttnEditar.Click += new System.EventHandler(this.bttnEditar_Click);
            // 
            // cBPonto
            // 
            this.cBPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBPonto.FormattingEnabled = true;
            this.cBPonto.Location = new System.Drawing.Point(118, 59);
            this.cBPonto.Name = "cBPonto";
            this.cBPonto.Size = new System.Drawing.Size(292, 24);
            this.cBPonto.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 67);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 16);
            this.label8.TabIndex = 59;
            this.label8.Text = "Reservatório:";
            // 
            // btAcionamento
            // 
            this.btAcionamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAcionamento.Image = global::SATS.Properties.Resources.crystal_clear_action_playlist__2_;
            this.btAcionamento.Location = new System.Drawing.Point(151, 12);
            this.btAcionamento.Name = "btAcionamento";
            this.btAcionamento.Size = new System.Drawing.Size(233, 35);
            this.btAcionamento.TabIndex = 92;
            this.btAcionamento.Text = "Visualizar Programações";
            this.btAcionamento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAcionamento.UseVisualStyleBackColor = true;
            this.btAcionamento.Visible = false;
            this.btAcionamento.Click += new System.EventHandler(this.btAcionamento_Click);
            // 
            // ConfiguracaoAlarmeNivel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(552, 294);
            this.Controls.Add(this.btAcionamento);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNivelMax);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNivelMin);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bttnCancelar);
            this.Controls.Add(this.bttnSalvar);
            this.Controls.Add(this.bttnNovo);
            this.Controls.Add(this.checkBox);
            this.Controls.Add(this.bttnEditar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cBPonto);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(568, 333);
            this.MinimumSize = new System.Drawing.Size(568, 333);
            this.Name = "ConfiguracaoAlarmeNivel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuração Nível Crítico";
            this.Load += new System.EventHandler(this.programaligamento_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNivelMin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bttnCancelar;
        private System.Windows.Forms.Button bttnSalvar;
        private System.Windows.Forms.Button bttnNovo;
        private System.Windows.Forms.CheckBox checkBox;
        private System.Windows.Forms.Button bttnEditar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNivelMax;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private System.Windows.Forms.ComboBox cBPonto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btAcionamento;
    }
}