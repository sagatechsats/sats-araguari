﻿using OfficeOpenXml;
using System;
using System.IO;
using System.Windows.Forms;

// Para XLSX utilizamos a biblioteca EPPlus(using OfficeOpenXml). Para XLS, 
// Excel Library.(Lembrar de adicionar refencias)

namespace SATS
{
    class SpreadsheetCreator4
    {
        String entrada1 = "";
        String cmfaturado1 = "";
        String cefaturado1 = "";
        String cmnfaturado1 = "";
        String cmestimado1 = "";
        String cnautorizado1 = "";
        String edemedicao1 = "";
        String vreservatori1 = "";
        String vemredes1 = "";

        public SpreadsheetCreator4(String entrada, String cmfaturado, String cefaturado, String cmnfaturado, String cmestimado, String cnautorizado, String edemedicao, String vreservatori, String vemredes)
        {  //

            entrada1 = entrada;
            cmfaturado1 = cmfaturado;
            cefaturado1 = cefaturado;
            cmnfaturado1 = cmnfaturado;
            cmestimado1 = cmestimado;
            cnautorizado1 = cnautorizado;
            edemedicao1 = edemedicao;
            vreservatori1 = vreservatori;
            vemredes1 = vemredes;

        }

        // Estou retornando o tipo SaveFileDialog porque eu preciso do path e do
        // filterIndex escolhido.
        private SaveFileDialog SaveDialog()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Excel 2007/2010/2013 (*.xlsx)|*.xlsx";
            //"Todos os arquivos (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true; ;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                return saveFileDialog1;
            else
                return null;
        }

        public void CreateFile(string nameSpread, string dataInicial, string dataFinal)
        {

            SaveFileDialog saveFileStruct = SaveDialog();
            if (saveFileStruct == null)
                return;


            CreateXLSFile(nameSpread, dataInicial, dataFinal, saveFileStruct.FileName, entrada1, cmfaturado1, cefaturado1, cmnfaturado1, cmestimado1, cnautorizado1, edemedicao1, vreservatori1, vemredes1);

        }


        private void CreateXLSFile(string nameSpread, string dataInicial, string dataFinal, string path, String entrada, String cmfaturado, String cefaturado, String cmnfaturado, String cmestimado, String cnautorizado, String edemedicao, String vreservatori, String vemredes)
        {
            // string cliente, string dataInicial,            string dataFinal, string path, String entrada, String cmfaturado, String cefaturado, String cmnfaturado, String cmestimado, String cnautorizado, String edemedicao, String vreservatori, String vemredes)

            FileInfo file = new FileInfo(path);
            //FileInfo file = new FileInfo(@"C:\Users\amador\Desktop\test.xlsx");
            using (ExcelPackage pck = new ExcelPackage())
            {
                var ws = pck.Workbook.Worksheets.Add(nameSpread);
                ws.Name = nameSpread;
                ws.Cells.Style.Font.Size = 11;
                ws.Cells["A1:E1"].Merge = true;
                ws.Cells[1, 1].Value = "Balanço Hídrico - Início: " + dataInicial + "    Término: " + dataFinal;
                ws.Cells[1, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells[1, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);

                // for (var i = 0; i < listView.Columns.Count; ++i)
                //   ws.Cells[2, i + 1].Value = listView.Columns[i].Text;

                ws.Cells["A1:E1"].Style.Font.Italic = true;
                ws.Cells["A1:E1"].Style.Font.Bold = true;

                ws.Cells["A1:E1"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(1).Width = 22;
                ws.Column(2).Width = 32;
                ws.Column(3).Width = 45;
                ws.Column(4).Width = 45;
                ws.Column(5).Width = 30;

                ws.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Column(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Column(4).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Column(5).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Column(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ws.Column(2).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ws.Column(3).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ws.Column(4).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ws.Column(5).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;


                ws.Cells["A2:A9"].Merge = true;
                ws.Cells["A2:A9"].Value = "Entrada (m3): \r\n" + entrada + " - 100%";
                ws.Cells["A2:A9"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Cells["A2:A9"].Style.Font.Bold = true;

                decimal value1 = Convert.ToInt32(cmfaturado) + Convert.ToInt32(cefaturado) + Convert.ToInt32(cmnfaturado) + Convert.ToInt32(cmestimado);
                ws.Cells["B2:B5"].Merge = true;
                ws.Cells["B2:B5"].Value = "Consumo autorizado (m3): \r\n" + value1.ToString() + " - " + string.Format("{0:n2}", ((value1 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["B2:B5"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value2 = Convert.ToInt32(cmfaturado) + Convert.ToInt32(cefaturado);
                ws.Cells["C2:C3"].Merge = true;
                ws.Cells["C2:C3"].Value = "Consumo autorizado Faturado (m3): \r\n" + value2.ToString() + " - " + string.Format("{0:n2}", ((value2 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["C2:C3"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value3 = Convert.ToInt32(cmfaturado);
                ws.Cells["D2:D2"].Value = "Consumo medido faturado (m3): \r\n" + value3.ToString() + " - " + string.Format("{0:n2}", ((value3 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["D2:D2"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value4 = Convert.ToInt32(cefaturado);
                ws.Cells["D3:D3"].Value = "Consumo estimado faturado (m3): \r\n" + value4.ToString() + " - " + string.Format("{0:n2}", ((value4 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["D3:D3"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value5 = Convert.ToInt32(cmnfaturado) + Convert.ToInt32(cmestimado);
                ws.Cells["C4:C5"].Merge = true;
                ws.Cells["C4:C5"].Value = "Consumo autorizado Não Faturado (m3): \r\n" + value5.ToString() + " - " + string.Format("{0:n2}", ((value5 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["C4:C5"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value6 = Convert.ToInt32(cmnfaturado);
                ws.Cells["D4:D4"].Value = "Consumo medido não faturado (m3): \r\n" + value6.ToString() + " - " + string.Format("{0:n2}", ((value6 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["D4:D4"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value7 = Convert.ToInt32(cmestimado);
                ws.Cells["D5:D5"].Value = "Consumo estimando não faturado (m3): \r\n" + value7.ToString() + " - " + string.Format("{0:n2}", ((value7 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["D5:D5"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value8 = Convert.ToInt32(cnautorizado) + Convert.ToInt32(edemedicao) + Convert.ToInt32(vreservatori) + Convert.ToInt32(vemredes);
                ws.Cells["B6:B9"].Merge = true;
                ws.Cells["B6:B9"].Value = "Perdas de água (m3): \r\n" + value8.ToString() + " - " + string.Format("{0:n2}", ((value8 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["B6:B9"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value9 = Convert.ToInt32(cnautorizado) + Convert.ToInt32(edemedicao);
                ws.Cells["C6:C7"].Merge = true;
                ws.Cells["C6:C7"].Value = "Perdas aparentes (m3): \r\n" + value9.ToString() + " - " + string.Format("{0:n2}", ((value9 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["C6:C7"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value10 = Convert.ToInt32(cnautorizado);
                ws.Cells["D6:D6"].Value = "Consumo não autorizado (m3): \r\n" + value10.ToString() + " - " + string.Format("{0:n2}", ((value10 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["D6:D6"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value11 = Convert.ToInt32(edemedicao);
                ws.Cells["D7:D7"].Value = "Erro de Medição (m3): \r\n" + value11.ToString() + " - " + string.Format("{0:n2}", ((value11 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["D7:D7"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value12 = Convert.ToInt32(vreservatori) + Convert.ToInt32(vemredes);
                ws.Cells["C8:C9"].Merge = true;
                ws.Cells["C8:C9"].Value = "Perdas Físicas (m3): \r\n" + value12.ToString() + " - " + string.Format("{0:n2}", ((value12 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["C8:C9"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value13 = Convert.ToInt32(vreservatori);
                ws.Cells["D8:D8"].Value = "Vazamento em reservatórios (m3): \r\n" + value13.ToString() + " - " + string.Format("{0:n2}", ((value13 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["D8:D8"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value14 = Convert.ToInt32(vemredes);
                ws.Cells["D9:D9"].Value = "Vazamento em redes (m3): \r\n" + value14.ToString() + " - " + string.Format("{0:n2}", ((value14 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["D9:D9"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                decimal value15 = Convert.ToInt32(cmfaturado) + Convert.ToInt32(cefaturado);
                ws.Cells["E2:E3"].Merge = true;
                ws.Cells["E2:E3"].Value = "Água Faturada (m3): \r\n" + value15.ToString() + " - " + string.Format("{0:n2}", ((value15 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["E2:E3"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Cells["E2:E3"].Style.Font.Bold = true;
                ws.Cells["E2:E3"].Style.Font.Italic = true;

                decimal value16 = Convert.ToInt32(cmnfaturado) + Convert.ToInt32(cmestimado) + Convert.ToInt32(cnautorizado) + Convert.ToInt32(edemedicao) + Convert.ToInt32(vreservatori) + Convert.ToInt32(vemredes);
                ws.Cells["E4:E9"].Merge = true;
                ws.Cells["E4:E9"].Value = "Água Não Faturada (m3): \r\n" + value16.ToString() + " - " + string.Format("{0:n2}", ((value16 * 100) / Convert.ToDecimal(entrada))) + " %";
                ws.Cells["E4:E9"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Cells["E4:E9"].Style.Font.Bold = true;
                ws.Cells["E4:E9"].Style.Font.Italic = true;

                /* for (var i = 0; i < listView.Items.Count; ++i)
                 {
                     for (var j = 0; j < listView.Items[i].SubItems.Count; ++j)
                     {
                         ws.Cells[i + 3, j + 1].Value = listView.Items[i].SubItems[j].Text;
                     }
                 }*/
                // Salva o arquivo.
                Byte[] bin = pck.GetAsByteArray();
                try
                {
                    File.WriteAllBytes(path, bin);
                    MessageBox.Show("Arquivo salvo com sucesso.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Aviso: O arquivo esta aberto.", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
