﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Media;

namespace SATS
{
    public partial class AlertasGerais : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private List<string[]> alertas;
        private DBManager dB;
        private DataTable dt = new DataTable();
        private DBManager dbManager;
        SoundPlayer simpleSound = new SoundPlayer("Alarm.wav");
        public AlertasGerais()
        {
            string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
            dB = new DBManager(stcon);
            InitializeComponent();
            while (dgvAlertas.Columns.Count > 0)
            {
                dgvAlertas.Columns.RemoveAt(0);
            }
            dt.Columns.Clear();
            dt.Columns.Add("ID_ALARME", typeof(string));
            dt.Columns.Add("PONTO", typeof(string)).ReadOnly = true;
            dt.Columns.Add("DATA/HORA", typeof(string)).ReadOnly = true;
            dt.Columns.Add("TIPO", typeof(string)).ReadOnly = true;
            dt.Columns.Add("DESCRIÇÃO", typeof(string)).ReadOnly = true;
            dgvAlertas.DataSource = dt;
            dgvAlertas.Columns["ID_ALARME"].Visible = false;
            dgvAlertas.Columns["DESCRIÇÃO"].Width = 135;
            //dgvAlertas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void AlertasGerais_Load(object sender, EventArgs e)
        {
            string msg = "";
            string query = "SELECT p.nome_ponto, pa.data_hora, pa.tipo_alarme, pa.descricao, pa.id_alarme" +
                           " FROM ponto_alarme pa INNER JOIN ponto p on p.id_ponto = pa.id_ponto" +
                           " WHERE status_alarme = 0 ORDER BY data_hora ASC";

            alertas = dB.DeFactoSelect(query);
            if (alertas.Count() > 0)
            {
                foreach (var item in alertas)
                {
                    DataRow dr = dt.NewRow();
                    dr["ID_ALARME"] = item[4];
                    dr["PONTO"] = item[0];
                    dr["DATA/HORA"] = item[1];
                    dr["TIPO"] = item[2];
                    dr["DESCRIÇÃO"] = item[3];
                    dt.Rows.Add(dr);

                    dgvAlertas.DataSource = dt;
                    dgvAlertas.Refresh();
                }
                simpleSound.Play();
            }
            else
            {
                msg += "Não foram encontrados Alertas Novos!";
                lblAvisos.Text = msg;
                lblAvisos.Visible = true;
                dgvAlertas.Visible = false;
                simpleSound.Stop();
            }
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                /**
                 * Mudança Necessaria pois com grandes volumes de erros 
                 * no Alerta os updates eram muito demorados porque eram rodados um por um. 
                 * Dessa forma eles são rodados simultaneamente deixando a carga de trabalho por conta do proprio SGBD
                 **/                
                string updateIds = "";
                int rowsCount = dgvAlertas.Rows.Count;//conta quantos registros tem no DataGridView
                int x = 1;//Inicializa o Contador que uso para evitar erros na string.
                foreach (DataGridViewRow row in dgvAlertas.Rows)
                {
                    if( x < rowsCount)
                        updateIds += "id_alarme=" + row.Cells["ID_ALARME"].Value.ToString()+" OR ";
                    else
                        updateIds += "id_alarme=" + row.Cells["ID_ALARME"].Value.ToString() + " ";

                    x++; //contador para evitar inserir um "OR" errado na ultima string.
                }
                dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                dbManager.Update("ponto_alarme", "status_alarme=1", updateIds);
                dbManager.CloseConnection();
            }
            catch (Exception)
            {
                dbManager.CloseConnection();
            }

            

            simpleSound.Stop();
            this.Close();
        }
    }
}
