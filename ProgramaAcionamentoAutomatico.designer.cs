﻿namespace SATS
{
    partial class ProgramaAcionamentoAutomatico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgramaAcionamentoAutomatico));
            this.label2 = new System.Windows.Forms.Label();
            this.txtNivelMin = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cBPonto = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNivelMax = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cBReservatorio = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.dTLiga = new System.Windows.Forms.DateTimePicker();
            this.dTDesliga = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtminFponta = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cBres2 = new System.Windows.Forms.ComboBox();
            this.txtMinRes2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.gbResDependente = new System.Windows.Forms.GroupBox();
            this.ckResDependente = new System.Windows.Forms.CheckBox();
            this.ckTemporizacao = new System.Windows.Forms.CheckBox();
            this.dgvTemporizacao = new System.Windows.Forms.DataGridView();
            this.btAcionamento = new System.Windows.Forms.Button();
            this.bttnCancelar = new System.Windows.Forms.Button();
            this.bttnSalvar = new System.Windows.Forms.Button();
            this.bttnNovo = new System.Windows.Forms.Button();
            this.bttnEditar = new System.Windows.Forms.Button();
            this.txtmaxFponta = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ckBombaPoco = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAtualizarH = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCadHorario = new System.Windows.Forms.Button();
            this.gbResDependente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemporizacao)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(366, 136);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 69;
            this.label2.Text = "m.c.a";
            // 
            // txtNivelMin
            // 
            this.txtNivelMin.Enabled = false;
            this.txtNivelMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNivelMin.Location = new System.Drawing.Point(209, 133);
            this.txtNivelMin.MaxLength = 12;
            this.txtNivelMin.Name = "txtNivelMin";
            this.txtNivelMin.Size = new System.Drawing.Size(150, 22);
            this.txtNivelMin.TabIndex = 5;
            this.txtNivelMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNivelMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(76, 136);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 16);
            this.label6.TabIndex = 68;
            this.label6.Text = "Nivel mínimo Ponta:";
            // 
            // checkBox
            // 
            this.checkBox.AutoSize = true;
            this.checkBox.Enabled = false;
            this.checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox.Location = new System.Drawing.Point(118, 104);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(94, 22);
            this.checkBox.TabIndex = 4;
            this.checkBox.Text = "Habilitar ";
            this.checkBox.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 23);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 16);
            this.label8.TabIndex = 59;
            this.label8.Text = "Bomba Ponto:";
            // 
            // cBPonto
            // 
            this.cBPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBPonto.FormattingEnabled = true;
            this.cBPonto.Location = new System.Drawing.Point(118, 20);
            this.cBPonto.Name = "cBPonto";
            this.cBPonto.Size = new System.Drawing.Size(292, 24);
            this.cBPonto.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(366, 162);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 16);
            this.label3.TabIndex = 72;
            this.label3.Text = "m.c.a";
            // 
            // txtNivelMax
            // 
            this.txtNivelMax.Enabled = false;
            this.txtNivelMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNivelMax.Location = new System.Drawing.Point(209, 159);
            this.txtNivelMax.MaxLength = 12;
            this.txtNivelMax.Name = "txtNivelMax";
            this.txtNivelMax.Size = new System.Drawing.Size(150, 22);
            this.txtNivelMax.TabIndex = 7;
            this.txtNivelMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNivelMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(72, 162);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 16);
            this.label4.TabIndex = 71;
            this.label4.Text = "Nível máximo Ponta:";
            // 
            // cBReservatorio
            // 
            this.cBReservatorio.Enabled = false;
            this.cBReservatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBReservatorio.FormattingEnabled = true;
            this.cBReservatorio.Location = new System.Drawing.Point(118, 50);
            this.cBReservatorio.Name = "cBReservatorio";
            this.cBReservatorio.Size = new System.Drawing.Size(292, 24);
            this.cBReservatorio.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 54);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 74;
            this.label1.Text = "Reservatório:";
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerColorTint = System.Drawing.Color.CornflowerBlue;
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            // 
            // dTLiga
            // 
            this.dTLiga.CustomFormat = "HH:mm:ss";
            this.dTLiga.Enabled = false;
            this.dTLiga.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTLiga.Location = new System.Drawing.Point(209, 240);
            this.dTLiga.Name = "dTLiga";
            this.dTLiga.Size = new System.Drawing.Size(150, 22);
            this.dTLiga.TabIndex = 8;
            this.dTLiga.Value = new System.DateTime(2016, 10, 28, 0, 0, 0, 0);
            // 
            // dTDesliga
            // 
            this.dTDesliga.CustomFormat = "HH:mm:ss";
            this.dTDesliga.Enabled = false;
            this.dTDesliga.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTDesliga.Location = new System.Drawing.Point(209, 268);
            this.dTDesliga.Name = "dTDesliga";
            this.dTDesliga.Size = new System.Drawing.Size(150, 22);
            this.dTDesliga.TabIndex = 9;
            this.dTDesliga.Value = new System.DateTime(2016, 10, 28, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(74, 242);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 16);
            this.label5.TabIndex = 77;
            this.label5.Text = "Horário Ponta Início:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(83, 270);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 16);
            this.label7.TabIndex = 78;
            this.label7.Text = "Horário Ponta Fim:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(366, 187);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 16);
            this.label9.TabIndex = 81;
            this.label9.Text = "m.c.a";
            // 
            // txtminFponta
            // 
            this.txtminFponta.Enabled = false;
            this.txtminFponta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtminFponta.Location = new System.Drawing.Point(209, 184);
            this.txtminFponta.MaxLength = 12;
            this.txtminFponta.Name = "txtminFponta";
            this.txtminFponta.Size = new System.Drawing.Size(150, 22);
            this.txtminFponta.TabIndex = 6;
            this.txtminFponta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtminFponta.TextChanged += new System.EventHandler(this.txtminFponta_TextChanged);
            this.txtminFponta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(26, 187);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(176, 16);
            this.label10.TabIndex = 80;
            this.label10.Text = "Nivel mínimo Fora de Ponta:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 23);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 83;
            this.label11.Text = "Reservatório dependente:";
            // 
            // cBres2
            // 
            this.cBres2.Enabled = false;
            this.cBres2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBres2.FormattingEnabled = true;
            this.cBres2.Location = new System.Drawing.Point(180, 20);
            this.cBres2.Name = "cBres2";
            this.cBres2.Size = new System.Drawing.Size(228, 24);
            this.cBres2.TabIndex = 82;
            // 
            // txtMinRes2
            // 
            this.txtMinRes2.Enabled = false;
            this.txtMinRes2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinRes2.Location = new System.Drawing.Point(180, 49);
            this.txtMinRes2.MaxLength = 12;
            this.txtMinRes2.Name = "txtMinRes2";
            this.txtMinRes2.Size = new System.Drawing.Size(145, 22);
            this.txtMinRes2.TabIndex = 84;
            this.txtMinRes2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMinRes2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(85, 50);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 16);
            this.label12.TabIndex = 85;
            this.label12.Text = "Nível mínimo:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(332, 52);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 16);
            this.label13.TabIndex = 86;
            this.label13.Text = "m.c.a";
            // 
            // gbResDependente
            // 
            this.gbResDependente.Controls.Add(this.cBres2);
            this.gbResDependente.Controls.Add(this.label12);
            this.gbResDependente.Controls.Add(this.label13);
            this.gbResDependente.Controls.Add(this.label11);
            this.gbResDependente.Controls.Add(this.txtMinRes2);
            this.gbResDependente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbResDependente.Location = new System.Drawing.Point(122, 414);
            this.gbResDependente.Name = "gbResDependente";
            this.gbResDependente.Size = new System.Drawing.Size(414, 78);
            this.gbResDependente.TabIndex = 87;
            this.gbResDependente.TabStop = false;
            this.gbResDependente.Text = "Reservatório de onde a Bomba puxa a água";
            this.gbResDependente.Visible = false;
            // 
            // ckResDependente
            // 
            this.ckResDependente.AutoSize = true;
            this.ckResDependente.Enabled = false;
            this.ckResDependente.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckResDependente.Location = new System.Drawing.Point(118, 359);
            this.ckResDependente.Name = "ckResDependente";
            this.ckResDependente.Size = new System.Drawing.Size(245, 22);
            this.ckResDependente.TabIndex = 88;
            this.ckResDependente.Text = "Possui Reservatório Dependente";
            this.ckResDependente.UseVisualStyleBackColor = true;
            this.ckResDependente.CheckedChanged += new System.EventHandler(this.cbResDependente_CheckedChanged);
            // 
            // ckTemporizacao
            // 
            this.ckTemporizacao.AutoSize = true;
            this.ckTemporizacao.Enabled = false;
            this.ckTemporizacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckTemporizacao.Location = new System.Drawing.Point(256, 55);
            this.ckTemporizacao.Name = "ckTemporizacao";
            this.ckTemporizacao.Size = new System.Drawing.Size(140, 22);
            this.ckTemporizacao.TabIndex = 90;
            this.ckTemporizacao.Text = "Ativar Horários";
            this.ckTemporizacao.UseVisualStyleBackColor = true;
            this.ckTemporizacao.CheckedChanged += new System.EventHandler(this.ckTemporizacao_CheckedChanged);
            // 
            // dgvTemporizacao
            // 
            this.dgvTemporizacao.AllowUserToAddRows = false;
            this.dgvTemporizacao.AllowUserToResizeColumns = false;
            this.dgvTemporizacao.AllowUserToResizeRows = false;
            this.dgvTemporizacao.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTemporizacao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvTemporizacao.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvTemporizacao.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTemporizacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTemporizacao.Location = new System.Drawing.Point(20, 23);
            this.dgvTemporizacao.Name = "dgvTemporizacao";
            this.dgvTemporizacao.RowHeadersVisible = false;
            this.dgvTemporizacao.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvTemporizacao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTemporizacao.Size = new System.Drawing.Size(217, 186);
            this.dgvTemporizacao.TabIndex = 91;
            // 
            // btAcionamento
            // 
            this.btAcionamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAcionamento.Image = global::SATS.Properties.Resources.crystal_clear_action_playlist__2_;
            this.btAcionamento.Location = new System.Drawing.Point(440, 23);
            this.btAcionamento.Name = "btAcionamento";
            this.btAcionamento.Size = new System.Drawing.Size(233, 35);
            this.btAcionamento.TabIndex = 92;
            this.btAcionamento.Text = "Visualizar Programações";
            this.btAcionamento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAcionamento.UseVisualStyleBackColor = true;
            this.btAcionamento.Click += new System.EventHandler(this.btAcionamento_Click);
            // 
            // bttnCancelar
            // 
            this.bttnCancelar.Enabled = false;
            this.bttnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("bttnCancelar.Image")));
            this.bttnCancelar.Location = new System.Drawing.Point(515, 536);
            this.bttnCancelar.Name = "bttnCancelar";
            this.bttnCancelar.Size = new System.Drawing.Size(97, 35);
            this.bttnCancelar.TabIndex = 11;
            this.bttnCancelar.Text = "Cancelar";
            this.bttnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnCancelar.UseVisualStyleBackColor = true;
            this.bttnCancelar.Click += new System.EventHandler(this.bttnCancelar_Click);
            // 
            // bttnSalvar
            // 
            this.bttnSalvar.Enabled = false;
            this.bttnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnSalvar.Image = ((System.Drawing.Image)(resources.GetObject("bttnSalvar.Image")));
            this.bttnSalvar.Location = new System.Drawing.Point(397, 536);
            this.bttnSalvar.Name = "bttnSalvar";
            this.bttnSalvar.Size = new System.Drawing.Size(97, 35);
            this.bttnSalvar.TabIndex = 10;
            this.bttnSalvar.Text = "Salvar";
            this.bttnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnSalvar.UseVisualStyleBackColor = true;
            this.bttnSalvar.Click += new System.EventHandler(this.bttnSalvar_Click);
            // 
            // bttnNovo
            // 
            this.bttnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnNovo.Image = global::SATS.Properties.Resources.Crystal_Clear_action_edit_add;
            this.bttnNovo.Location = new System.Drawing.Point(266, 536);
            this.bttnNovo.Name = "bttnNovo";
            this.bttnNovo.Size = new System.Drawing.Size(97, 35);
            this.bttnNovo.TabIndex = 13;
            this.bttnNovo.Text = "Novo";
            this.bttnNovo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnNovo.UseVisualStyleBackColor = true;
            this.bttnNovo.Click += new System.EventHandler(this.bttnNovo_Click);
            // 
            // bttnEditar
            // 
            this.bttnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnEditar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_editpaste;
            this.bttnEditar.Location = new System.Drawing.Point(703, 23);
            this.bttnEditar.Name = "bttnEditar";
            this.bttnEditar.Size = new System.Drawing.Size(107, 35);
            this.bttnEditar.TabIndex = 2;
            this.bttnEditar.Text = "Editar";
            this.bttnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnEditar.UseVisualStyleBackColor = true;
            this.bttnEditar.Click += new System.EventHandler(this.bttnEditar_Click);
            // 
            // txtmaxFponta
            // 
            this.txtmaxFponta.Enabled = false;
            this.txtmaxFponta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmaxFponta.Location = new System.Drawing.Point(209, 209);
            this.txtmaxFponta.MaxLength = 12;
            this.txtmaxFponta.Name = "txtmaxFponta";
            this.txtmaxFponta.Size = new System.Drawing.Size(150, 22);
            this.txtmaxFponta.TabIndex = 93;
            this.txtmaxFponta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(23, 212);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(180, 16);
            this.label14.TabIndex = 94;
            this.label14.Text = "Nível máximo Fora de Ponta:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(366, 212);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 16);
            this.label15.TabIndex = 95;
            this.label15.Text = "m.c.a";
            // 
            // ckBombaPoco
            // 
            this.ckBombaPoco.AutoSize = true;
            this.ckBombaPoco.Enabled = false;
            this.ckBombaPoco.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBombaPoco.Location = new System.Drawing.Point(440, 359);
            this.ckBombaPoco.Name = "ckBombaPoco";
            this.ckBombaPoco.Size = new System.Drawing.Size(181, 22);
            this.ckBombaPoco.TabIndex = 96;
            this.ckBombaPoco.Text = "BOMBA RECALQUE";
            this.ckBombaPoco.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAtualizarH);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.btnCadHorario);
            this.groupBox1.Controls.Add(this.dgvTemporizacao);
            this.groupBox1.Controls.Add(this.ckTemporizacao);
            this.groupBox1.Location = new System.Drawing.Point(436, 104);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 215);
            this.groupBox1.TabIndex = 97;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Horários:";
            // 
            // btnAtualizarH
            // 
            this.btnAtualizarH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizarH.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizarH.Image")));
            this.btnAtualizarH.Location = new System.Drawing.Point(256, 174);
            this.btnAtualizarH.Name = "btnAtualizarH";
            this.btnAtualizarH.Size = new System.Drawing.Size(118, 35);
            this.btnAtualizarH.TabIndex = 122;
            this.btnAtualizarH.Text = "Atualizar";
            this.btnAtualizarH.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAtualizarH.UseVisualStyleBackColor = true;
            this.btnAtualizarH.Click += new System.EventHandler(this.btnAtualizarH_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = global::SATS.Properties.Resources.Action_delete_icon;
            this.btnDelete.Location = new System.Drawing.Point(257, 136);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(117, 33);
            this.btnDelete.TabIndex = 121;
            this.btnDelete.Text = "Deletar";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCadHorario
            // 
            this.btnCadHorario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadHorario.Image = global::SATS.Properties.Resources.Crystal_Clear_action_edit_add;
            this.btnCadHorario.Location = new System.Drawing.Point(256, 92);
            this.btnCadHorario.Name = "btnCadHorario";
            this.btnCadHorario.Size = new System.Drawing.Size(118, 35);
            this.btnCadHorario.TabIndex = 119;
            this.btnCadHorario.Text = "Novo Horário";
            this.btnCadHorario.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCadHorario.UseVisualStyleBackColor = true;
            this.btnCadHorario.Click += new System.EventHandler(this.btnCadHorario_Click);
            // 
            // ProgramaAcionamentoAutomatico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(846, 597);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ckBombaPoco);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtmaxFponta);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btAcionamento);
            this.Controls.Add(this.ckResDependente);
            this.Controls.Add(this.gbResDependente);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtminFponta);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dTDesliga);
            this.Controls.Add(this.dTLiga);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBReservatorio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNivelMax);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNivelMin);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bttnCancelar);
            this.Controls.Add(this.bttnSalvar);
            this.Controls.Add(this.bttnNovo);
            this.Controls.Add(this.checkBox);
            this.Controls.Add(this.bttnEditar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cBPonto);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(16, 477);
            this.Name = "ProgramaAcionamentoAutomatico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Acionamento Automático";
            this.Load += new System.EventHandler(this.programaligamento_Load);
            this.gbResDependente.ResumeLayout(false);
            this.gbResDependente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemporizacao)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNivelMin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bttnCancelar;
        private System.Windows.Forms.Button bttnSalvar;
        private System.Windows.Forms.Button bttnNovo;
        private System.Windows.Forms.CheckBox checkBox;
        private System.Windows.Forms.Button bttnEditar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cBPonto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNivelMax;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cBReservatorio;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private System.Windows.Forms.DateTimePicker dTLiga;
        private System.Windows.Forms.DateTimePicker dTDesliga;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtminFponta;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cBres2;
        private System.Windows.Forms.TextBox txtMinRes2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox gbResDependente;
        private System.Windows.Forms.CheckBox ckResDependente;
        private System.Windows.Forms.CheckBox ckTemporizacao;
        private System.Windows.Forms.DataGridView dgvTemporizacao;
        private System.Windows.Forms.Button btAcionamento;
        private System.Windows.Forms.TextBox txtmaxFponta;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox ckBombaPoco;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCadHorario;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAtualizarH;
    }
}