﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using DevComponents.DotNetBar;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;


namespace SATS
{
    public partial class FrmGraficoPressao : Office2007Form
    {
        private int data1 = 0;
        private int type = 1;
        private int grafico = 1;


        List<string[]> paraGrafico;



        public FrmGraficoPressao(List<string[]> resultGrafics)
        {
            this.paraGrafico = resultGrafics;

            InitializeComponent();

        }

        private void FrmGraficoPressao_Load(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(() => this.WindowState = FormWindowState.Maximized));
            // gerarGraficoDay();
        }


        public void gerarGraficoDay()
        {
            try
            {


                Invoke(new MethodInvoker(() => cartesianChart1.Series.Clear()));
                Invoke(new MethodInvoker(() => cartesianChart1.AxisX.Clear()));
                Invoke(new MethodInvoker(() => cartesianChart1.AxisY.Clear()));
                var datas = new List<string>();

                var chart = new ChartValues<double>();

                foreach (var l in paraGrafico)
                {

                    chart.Add(Convert.ToDouble(l[1]));
                    datas.Add(l[2]);

                }


                if (grafico == 1)
                {
                    Invoke(new MethodInvoker(() => cartesianChart1.Series = new LiveCharts.SeriesCollection
                                {
                                    new LineSeries
                                    {
                                        Title = "Leitura de pressão",
                                        Values = chart,
                                        PointGeometry = DefaultGeometries.Circle,
                                        PointGeometrySize = 10,
                                        LineSmoothness = 0.3
                                    },
                                    //new LineSeries
                                    //{
                                    //    Title = "Despesa",
                                    //    Values = chart1,
                                    //    PointGeometry = DefaultGeometries.Circle,
                                    //    PointGeometrySize = 10,
                                    //    LineSmoothness = 0.2
                                    //}
                                }));
                }
                else if (grafico == 2)
                {
                    Invoke(new MethodInvoker(() => cartesianChart1.Series = new LiveCharts.SeriesCollection
                                {
                                    new ColumnSeries
                                    {
                                        Title = "Leitura de pressão",
                                        Values = chart,
                                    },

                                }));
                }

                Invoke(new MethodInvoker(() => cartesianChart1.AxisX.Add(new LiveCharts.Wpf.Axis
                {
                    Title = "Data e Hora",
                    Labels = datas
                })));
                Invoke(new MethodInvoker(() => cartesianChart1.AxisY.Clear()));
                Invoke(new MethodInvoker(() => cartesianChart1.AxisY.Add(new LiveCharts.Wpf.Axis
                {
                    Title = "Pressão",
                    //  LabelFormatter = value => value.ToString("C")
                })));
                Invoke(new MethodInvoker(() => cartesianChart1.LegendLocation = LegendLocation.Right));
                Invoke(new MethodInvoker(() => this.Cursor = Cursors.Default));
            }
            catch
            {

            }
        }
    }
}
