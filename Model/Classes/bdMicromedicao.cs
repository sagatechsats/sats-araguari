﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public class bdMicromedicao
    {
        private micromedicao Micromedicao;
        
        public bdMicromedicao()
        {
        }
        
        public bdMicromedicao(micromedicao aux)
        {
            this.Micromedicao = aux;
        }
        
        public micromedicao getMicromedicao()
        {
            return this.Micromedicao;
        }
        
        public void setMicromedicao(micromedicao aux)
        {
            this.Micromedicao = aux;
        }

        public void inserir()
        {
            DBManager bd = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            bd.Insert("micromedicao", "data_inicial, data_final, volume_micro", "'" + Micromedicao.getDataInicial().ToString("yyyy-MM-dd HH:mm:ss") + "','" + Micromedicao.getDataFinal().ToString("yyyy-MM-dd HH:mm:ss") + "'," + Micromedicao.getVolume());
 
           
        }

        
    }
}
