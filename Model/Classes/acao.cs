﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATS
{
    public class acao
    {
        private String nomeAcao;
        private DateTime dataAcao;
        private String descricaoAcao;

        public acao(String nome, DateTime data, String descricao)
        {
            this.nomeAcao = nome;
            this.dataAcao = data;
            this.descricaoAcao = descricao;
        }
        
        public String getNome()
        {
            return nomeAcao;
        }
        public DateTime getData()
        {
            return dataAcao;
        }
        public String getDescricao()
        {
            return descricaoAcao;
        }
        public void setNome(String nome)
        {
            this.nomeAcao = nome;
        }
        public void setData(DateTime data)
        {
            this.dataAcao = data;
        }
        public void setDescricao(String descricao)
        {
            this.descricaoAcao = descricao;
        }
    }

}
