﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public class bdGasto
    {
        private gasto Gasto;

        public bdGasto()
        {
        }
        
        public bdGasto(gasto aux)
        {
            this.Gasto = aux;
        }
        
        public gasto getGasto()
        {
            return this.Gasto;
        }
        
        public void setGasto(gasto aux)
        {
            this.Gasto = aux;
        }

        public void inserir(){
            DBManager bd = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            bd.Insert("gastos", "tipo_gasto, descricao, data,valor_gasto", "'" + Gasto.getTipo() + "','" + Gasto.getDescricao() + "','" + Gasto.getData().ToString("yyyy-MM-dd HH:mm:ss") + "', '" + Gasto.getValor()+"'");
 
        }

        
    }
}
