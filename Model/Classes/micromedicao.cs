﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATS
{
    public class micromedicao
    {
        private DateTime dataInicial;
        private DateTime dataFinal;
        private int volume;

        public micromedicao(DateTime dataI, DateTime dataF, int vol)
        {
            this.dataInicial = dataI;
            this.dataFinal = dataF;
            this.volume = vol;
        }
        public DateTime getDataInicial()
        {
            return dataInicial;
        }
        public DateTime getDataFinal()
        {
            return dataFinal;
        }
        public int getVolume()
        {
            return volume;
        }
        public void setDataInicial(DateTime aux)
        {
            this.dataInicial = aux;
        }
        public void setDataFinal(DateTime aux)
        {
            this.dataFinal = aux;
        }
        public void setVolume(int vol)
        {
            this.volume = vol;
        }
    }
}
