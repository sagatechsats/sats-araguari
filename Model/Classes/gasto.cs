﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATS
{
    public class gasto
    {
        private String tipo;
        private String descricao;
        private DateTime data;
        private Decimal valor;

        public gasto(String tipo, String descricao, DateTime data, Decimal valor)
        {
            this.tipo = tipo;
            this.descricao = descricao;
            this.data = data;
            this.valor = valor;
        }
        public String getTipo()
        {
            return tipo;
        }
        public String getDescricao()
        {
            return descricao;
        }
        public DateTime getData()
        {
            return data;
        }
        public string getValor()
        {
            return valor.ToString("n").Replace(",",".");
        }
        public void setTipo(String tipo)
        {
            this.tipo = tipo;
        }
        public void setDescricao(String descricao)
        {
            this.descricao = descricao;
        }
        public void setData(DateTime data)
        {
            this.data = data;
        }
        public void setValor(Decimal valor)
        {
            this.valor = valor;
        }
    }
}
