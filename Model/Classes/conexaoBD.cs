﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public class conexaoBD
    {
        private static MySqlConnection mConn;
        public static MySqlConnection getConnection()
        {
            string connectionString = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
            mConn = new MySqlConnection(connectionString);
            return mConn;
        }
    }
}
