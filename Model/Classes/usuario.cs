﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATS
{
    public class usuario
    {
        private String nome;
        private String senha;
        private String permissao;
        private String email;
        private String celular;
        private Boolean send;

        public usuario(String nome, String senha, String permissao, String email, String celular, Boolean send)
        {
            this.nome = nome;
            this.senha = senha;
            this.permissao = permissao;
            this.email = email;
            this.celular = celular;
            this.send = send;
        }
        public String getNome()
        {
            return nome;
        }
        public String getSenha()
        {
            return senha;
        }
        public String getPermissao()
        {
            return permissao;
        }
        public String getEmail()
        {
            return email;
        }
        public String getCelular()
        {
            return celular;
        }
        public Boolean getSend()
        {
            return send;
        }
        public void setNome(String aux)
        {
            this.nome = aux;
        }
        public void setSenha(String aux)
        {
            this.senha = aux;
        }
        public void setPermissao(String aux)
        {
            this.permissao = aux;
        }
        public void setEmail(String aux)
        {
            this.email = aux;
        }
        public void setCelular(String aux)
        {
            this.celular = aux;
        }
        public void setSend(Boolean aux)
        {
            this.send = aux;
        }
    }
}
