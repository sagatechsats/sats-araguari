﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public class bdAcao
    {
        private acao Acao;

        public bdAcao()
        {
        }
        
        public bdAcao(acao aux)
        {
            this.Acao = aux;
        }
        
        public acao getAcao()
        {
            return this.Acao;
        }
        
        public void setAcao(acao aux)
        {
            this.Acao = aux;
        }

        public void inserir(){
            DBManager bd = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            bd.Insert("acao", "nome_acao, data_acao, descricao", "'" + Acao.getNome() + "','" + Acao.getData().ToString("yyyy-MM-dd HH:mm:ss") + "','" + Acao.getDescricao() + "'");
 
        }

        
    }
}
