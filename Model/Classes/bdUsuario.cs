﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public class bdUsuario
    { 
        private usuario Usuario;

        public bdUsuario()
        {
        }
        
        public bdUsuario(usuario aux)
        {
            this.Usuario = aux;
        }
        
        public usuario getUsuario()
        {
            return this.Usuario;
        }
        
        public void setUsuario(usuario aux)
        {
            this.Usuario = aux;
        }

        public void inserir(){
            DBManager bd = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            bd.Insert("sats_user", "nome_user, senha_user, permissao_user, email_user, celular_user, send_to_user", "'" + Usuario.getNome() + "','" + Usuario.getSenha() + "','" + Usuario.getPermissao() + "','" + Usuario.getEmail() + "','" + Usuario.getCelular() + "'," + Usuario.getSend());
 
        }

    }
}
