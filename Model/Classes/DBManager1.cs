﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;
using System.Collections.Generic;


public class DBManager
{
    private MySqlConnection connection;

    public DBManager(string connectionString)
    {
        //string connectionString = "SERVER=" + serverAddress + ";" + "DATABASE=" +
        //    database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
        try
        {
            connection = new MySqlConnection(connectionString);
        }
        catch (MySqlException e)
        {
            MessageBox.Show("MySQL Server connection failure: " + e);
        }
    }

    private bool OpenConnection()
    {
        try
        {
            connection.Open();
            return true;
        }

        catch (MySqlException e)
        {
            MessageBox.Show(e.Number.ToString());
            MessageBox.Show(e.Message);
            return false;
        }
    }

    private bool CloseConnection()
    {
        try
        {
            connection.Close();
            return true;
        }

        catch (MySqlException e)
        {
            MessageBox.Show(e.Message);
            return false;
        }

    }
    public void Insert(string table, string columns, string values)
    {
        string query = "INSERT INTO " + table + "(" + columns + ")"
               + "VALUES(" + values + " );";
        if (this.OpenConnection() == true)
        {
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteNonQuery();
            this.CloseConnection();
        }
    }

    public void Update(string table, string clmnValues, string where)
    {
        string query = "UPDATE " + table + " SET " + clmnValues + " WHERE " +
            where + ";";
        if (this.OpenConnection() == true)
        {
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteNonQuery();
            this.CloseConnection();
        }
    }

    public void Delete(string table, string where)
    {
        string query = "DELETE FROM " + table + " WHERE " + where + ";";
        if (this.OpenConnection() == true)
        {
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteNonQuery();
            this.CloseConnection();
        }
    }

    /* Exemplo utilizacao (a partir de outra classe)
        List<string []> res = manager.SelectAllFromTable("sigt_user");
        foreach(string [] item in res)
        {
           for (int i = 0; i < item.Length; ++i )
            MessageBox.Show(item[i]);
        }
    */
    // Esses quatro metodos de baixo sao usados pra realizar um select. Eles estao
    // separados pra dividir as tarefas, facilitar debug e poder reutilizar
    // no select de duas tabelas.
    private List<string[]> ReturnDataReaderAsStringList(MySqlDataReader dataReader)
    {
        List<string[]> list = new List<string[]>();
        while (dataReader.Read())
        {
            int colsNumber = dataReader.FieldCount;
            list.Add(new string[colsNumber]);

            //list[list.count - 1] pra acessar o ultimo elemento da lista
            for (var i = 0; i < colsNumber; ++i)
                list[list.Count - 1][i] = dataReader[i].ToString();
        }
        return list;
    }

    // Onde o comando select e realmente executado.
    private List<string[]> DeFactoSelect(string query)
    {
        List<string[]> list = new List<string[]>();
        // string[] vai ser o tamanho do numero
        // de colunas de determinada tabela.
        if (this.OpenConnection() == true)
        {
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            list = ReturnDataReaderAsStringList(dataReader);

            dataReader.Close();
            this.CloseConnection();
        }
        return list;
    }

    public List<string[]> Select(string table, string columns, string where, string orderBy)
    {
        string query = "SELECT " + columns + " FROM " + table;
        if (where != String.Empty)
            query += " WHERE " + where;
        if (orderBy != String.Empty)
            query += " ORDER BY " + orderBy + " ASC";
        query += ";";

        return DeFactoSelect(query);
    }

    public List<string[]> Select(string table, string columns, string where)
    {
        return Select(table, columns, where, String.Empty);
    }

    public List<string[]> SelectUnionTwoTables(string table1, string columns1, string where1,
        string table2, string columns2, string where2, string orderBy)
    {
        string query = "SELECT " + columns1 + " FROM " + table1;
        if (where1 != String.Empty)
            query += " WHERE " + where1;

        query += " UNION ";
        query += "SELECT " + columns2 + " FROM " + table2;
        if (where2 != String.Empty)
            query += " WHERE " + where2;

        if (orderBy != String.Empty)
            query += " ORDER BY " + orderBy + " ASC";
        query += ";";

        return DeFactoSelect(query);
    }

    public int Count(string table)
    {
        string query = "SELECT Count(*) FROM " + table;
        int count = -1;

        if (this.OpenConnection() == true)
        {
            MySqlCommand cmd = new MySqlCommand(query, connection);
            count = int.Parse(cmd.ExecuteScalar().ToString());

            this.CloseConnection();
        }

        return count;
    }
}
