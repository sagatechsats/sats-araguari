﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SATS.Controller.segurança;

namespace SATS.Model
{
    //Objeto do tipo Usuário que servirá para setar e getar os dados, em multiplos cenários
    class UsuarioModel
    {
        private string _Usuario;
        private string _Senha;
        private string _Permissao;
        public string Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        public string Senha
        {
            get { return _Senha; }
            set { _Senha = senhaHash.getHashSha256(value); }
        }

        public string Permissao
        {
            get { return _Permissao; }
            set { _Permissao = value; }
        }


    }
}
