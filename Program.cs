﻿using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System;
using System.Windows.Forms;

namespace SATS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new ImportGastos());
            if (System.IO.File.Exists(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector")) Application.Run(new Login());
            else Application.Run(new Connectst());
            //Application.Run(new programaligamento());
        }
    }
}
