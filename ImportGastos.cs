﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class ImportGastos : Office2007Form
    {
        protected DBManager dbManager;
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        public List<string> indexClientes { get; set; }
        Thread novaThread;
        public ImportGastos()
        {
            InitializeComponent();
            dateYear.Format = DateTimePickerFormat.Custom;
            dateYear.CustomFormat = "yyyy";
            dateYear.ShowUpDown = true;
            dateMonth.Format = DateTimePickerFormat.Custom;
            dateMonth.CustomFormat = "MMM";
            dateMonth.ShowUpDown = true;
            string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
            dbManager = new DBManager(stcon);
            dateTFinal.MinDate = dateTInicial.Value;
            dateMonth.Value = DateTime.Today;
            dateYear.Value = DateTime.Today;
        }

        private void ImportGastos_Load(object sender, EventArgs e)
        {
            novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();
        }

        private void dateTInicial_ValueChanged(object sender, EventArgs e)
        {
            dateTFinal.MinDate = dateTInicial.Value;
        }

        private void bTsave_Click(object sender, EventArgs e)
        {
            if (cBPonto.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione o Ponto deste gasto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (cBTipo.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione o tipo de gastos!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (txtQuantidade.Text.Trim() == "")
            {
                MessageBox.Show("Informe a quantidade de produtos químicos ou Kwh gastos!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (txtValor.Text.Trim() == "")
            {
                MessageBox.Show("Informe o valor unitário!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show("Após o salvamento não será possível realizar nenhuma alteração de dados. Você tem certeza que todos os dados inseridos estão corretos?", "Atenção!", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string cliente = cBPonto.Text;
                //Pega o valor selecionado no combobox de cliente         
                string[] words = cliente.Split('-');
                string datefinal = dateTFinal.Value.Year + "-" + dateTFinal.Value.Month + "-" + dateTFinal.Value.Day;
                //decimal volume = dbManager.SelectVolume(Convert.ToInt32(words[0]), dateTInicial.Value.Date.ToString("s"), datefinal);
                string datareferencia = dateYear.Value.Year.ToString() + "-" + dateMonth.Value.Month.ToString() + "-1";
                string values = "'" + words[0].ToString() + "', '" + (cBTipo.SelectedIndex + 1).ToString() + "', '" + datareferencia + "', '" + dateTInicial.Value.Date.ToString("s") + "', '" + datefinal + " 23:59:59', '" + txtQuantidade.Text + "', '" + txtValor.Text.Replace(',', '.') + "'";
                bool inserir = dbManager.Insert("gastos", "id_ponto, tipo_gasto, data_referencia, data_inicial, data_final, gasto, valor", values);
                if (inserir == false)
                {
                    MessageBox.Show("Erro ao inserir!", "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //else MessageBox.Show("Dados inseridos com sucesso!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
            }
        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {
                if (!CheckDateValidity()) return;
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
                    Invoke((MethodInvoker)(() => PassListToComboBox(consulta)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(consulta)));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
            cBPonto.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            cBPonto.Items.Clear();
            foreach (string concat in list)
                cBPonto.Items.Add(concat);
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select("ponto order by id_ponto asc", "id_ponto, nome_ponto", "");
            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                listConcat.Add(concat);
            }
            return listConcat;
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void bTcancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ImportGastos_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void clear()
        {
            cBPonto.Text = "";
            cBTipo.Text = "";
            txtQuantidade.Text = "";
        }

        protected virtual bool CheckDateValidity()
        {
            if (dateTFinal.Value < dateTInicial.Value)
            {
                MessageBox.Show("Data Inicial deve ser menor ou igual à Data Final.",
                    "Período Inválido");
                return false;
            }
            return true;
        }

        private void cBPonto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void cBTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cBTipo.SelectedIndex == 0)
            {
                label6.Text = "Quantidade (kWh):";
                label7.Text = "Valor unitário do kWh:";
            }
            else
            {
                label6.Text = "Quantidade (Kg):";
                label7.Text = "Valor unitário do Kg:";
            }
        }
    }
}
