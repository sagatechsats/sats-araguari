﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class RelatorioPai : Office2007Form
    {
        [DllImport("wininet.dll")]

        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        protected DBManager dbManager;
        protected List<string> listaClientes;
        protected List<string> indexClientes;

        // so id_clientes para busca as posicoes sao correspondentes a listaClientes
        // atributos para armazenar ulltima busca valida. Servem para nao gerar
        // relatorios inconsistentes entre  a listView e o cliente.

        protected string ultCliente;
        protected string ultDateIn;
        protected string ultDateFi;
        protected int ultClienteIndex;

        public RelatorioPai()
        {

            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
            InitDateTimes();
            Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();

            //dbManager = new DBManager("Server=localhost;port=3306;Database=sigt_1;Uid=root;Pwd=");

        }

        protected void InitDateTimes()//ja que o designer insiste em mudar os valores que eu ponho...
        {
            dateTimeInicial.Value = System.DateTime.Today;
            dateTimeFinal.Value = System.DateTime.Today;
            dateTimeFinal.Value = dateTimeFinal.Value.AddHours(23);
            dateTimeFinal.Value = dateTimeFinal.Value.AddMinutes(59);
            dateTimeFinal.Value = dateTimeFinal.Value.AddSeconds(59);
            dateTimeInicial.MaxDate = System.DateTime.Today;
            dateTimeInicial.Value = System.DateTime.Today;
            dateTimeInicial.Value = System.DateTime.Today;
        }

        protected void StoreSearchData()
        {
            ultCliente = comboBoxCliente.SelectedItem.ToString();
            ultDateIn = dateTimeInicial.Value.ToShortDateString();
            ultDateFi = dateTimeFinal.Value.ToShortDateString();
            ultClienteIndex = comboBoxCliente.SelectedIndex;
        }

        // Obtem os clientes a partir do banco de dados e retorna como uma lista de strings
        // Cada item e composto pelo id do cliente(sempre 6 casas, com zeros a esquerda) e o
        // nome concatenado.

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select2("ponto", "id_ponto, nome_ponto", String.Empty, "id_ponto");
            while (res.Count == 0) res = dbManager.Select2("ponto", "id_ponto, nome_ponto", String.Empty, "id_ponto");
            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];

                listConcat.Add(concat);
            }
            return listConcat;
        }

        protected void PassListToComboBox(List<string> list)
        {

            foreach (string concat in list)
                comboBoxCliente.Items.Add(concat);
        }
        // Usado para habilitar o recurso de auto-completar.
        protected void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
            comboBoxCliente.AutoCompleteCustomSource = stringCollection;
        }

        protected void FillListView(List<string[]> resultList)
        {
            ListViewItem listViewItem;
            listView.Items.Clear();
            foreach (string[] item in resultList)
            {
                listViewItem = new ListViewItem(item);
                listView.Items.Add(listViewItem);
            }
        }

        protected virtual bool CheckDateValidity()
        {
            if (dateTimeFinal.Value < dateTimeInicial.Value)
            {
                MessageBox.Show("Data Inicial deve ser menor ou igual à Data Final.",
                    "Período Inválido");
                return false;
            }
            return true;
        }

        // Salva um arquivo de texto puro, com seu conteudo a partir de uma string
        // utilizada na classe.  
        protected virtual void SaveFile(string content)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Arquivos txt (*.txt)|*.txt";
            //saveFileDialog1.Filter = "Arquivos txt (*.txt)|*.txt|Todos os arquivos (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                    sw.WriteLine(content);
            }
        }

        protected void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        protected void buttonTxt_Click(object sender, EventArgs e)
        {
            //SaveFile("asdasd");
        }

        protected void buttonLimpar_Click(object sender, EventArgs e)
        {
            listView.Items.Clear();
            labelListView.Text = "Lista de Resultados Vazia";

        }
        public void CarregaTela()
        {
            if (comboBoxCliente.Items.Count <= 1)
            {
                if (IsConnected() == true)
                {
                    Application.UseWaitCursor = true;
                    Application.DoEvents();

                    try
                    {
                        dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                        List<string> listaClientes = new List<string>();
                        listaClientes = GetClientes();
                        Invoke((MethodInvoker)(() => PassListToComboBox(listaClientes)));
                        Invoke((MethodInvoker)(() => PassListToAutoComplete(listaClientes)));
                    }
                    catch (Exception)
                    {
                    }
                    Application.UseWaitCursor = false;
                    Application.DoEvents();
                }
                else if (IsConnected() == false)
                {
                    MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }




            }

        }

        public static Boolean IsConnected()
        {

            bool result;

            try
            {

                int Description;

                result = InternetGetConnectedState(out Description, 0);

            }

            catch
            {

                result = false;

            }

            return result;

        }

        private void RelatorioPai_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {

        }













    }
}
