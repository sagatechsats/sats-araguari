﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DevComponents.DotNetBar;


namespace SATS
{
    public partial class InformaDados : Office2007Form
    {

        protected DBManager dbManager;
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private int index = 0;



        public InformaDados(int index, string Ponto)
        {
            InitializeComponent();
            this.index = index;
            listView1.Columns[1].Text = Ponto.ToUpper();
            this.Hide();
        }

        private void InformaDados_Load(object sender, EventArgs e)
        {
            this.Hide();
            this.carregar();
        }

        private void carregar()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    List<string[]> result = dbManager.Select("ponto", "*", "id_ponto='" + index + "'");
                    if (result.Count == 0)
                    {
                        MessageBox.Show("Dados não cadastrados!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        Application.UseWaitCursor = false;
                        Application.DoEvents();
                        this.Visible = false;
                        this.Hide();

                        this.Close();
                        return;
                    }
                    List<string[]> result2 = ResultList(result);
                    FillListView(result2);
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();

                this.Show();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        protected void FillListView(List<string[]> resultList)
        {
            ListViewItem listViewItem;
            listView1.Items.Clear();
            foreach (string[] item in resultList)
            {
                listViewItem = new ListViewItem(item);
                listView1.Items.Add(listViewItem);
            }
        }

        private List<string[]> ResultList(List<string[]> resultList)
        {
            List<string[]> resultList2 = new List<string[]>();
            for (int i = 0; i < resultList[0].Length - 1; i++)
            {
                resultList2.Add(new string[2]);
                if (i == 0) resultList2[i][0] = "Modelo da Bomba:";
                if (i == 1) resultList2[i][0] = "Potência da Bomba:";
                if (i == 2) resultList2[i][0] = "Tipo da Bomba:";
                if (i == 3) resultList2[i][0] = "Tensão da Bomba:";
                if (i == 4) resultList2[i][0] = "Profundidade do Poço:";
                if (i == 5) resultList2[i][0] = "Nível Estático:";
                if (i == 6) resultList2[i][0] = "Nível Dinâmico:";
                if (i == 7) resultList2[i][0] = "Bitola do Hidrômetro:";
                if (i == 8) resultList2[i][0] = "Bitola do Tubo:";
                if (i == 9) resultList2[i][0] = "Data de Instalação:";
                if (i == 2)
                {
                    if (resultList[0][3] == "1")
                        resultList2[i][1] = "Monofásica";
                    else if (resultList[0][3] == "2")
                        resultList2[i][1] = "Bifásica";
                    else if (resultList[0][3] == "3")
                        resultList2[i][1] = "Trifásica";
                    else resultList2[i][1] = "";
                }
                else if (i == 3)
                    resultList2[i][1] = resultList[0][4] + "V";
                else if (i > 3 && i < 7) resultList2[i][1] = resultList[0][i + 1] + " metros";
                else if (i == 7 || i == 8) resultList2[i][1] = resultList[0][i + 1] + " polegadas";
                else resultList2[i][1] = resultList[0][i + 1].Replace(" 00:00:00", "");
            }
            return resultList2;
        }

        private void bttnPrint_Click(object sender, EventArgs e)
        {
            pdocListView.Print();
        }
        // Print the ListView's data.
        private void pdocListView_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // Print the ListView.
            listView1.PrintData(e.MarginBounds.Location,
                e.Graphics, Brushes.Blue,
                Brushes.Black, Pens.Blue);
        }

        private void ppdListView_Load(object sender, EventArgs e)
        {

        }
    }
}
