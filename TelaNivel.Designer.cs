﻿namespace SATS
{
    partial class TelaNivel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRede = new System.Windows.Forms.Label();
            this.tanque1 = new ReservatorioLiquidos.TanqueEscala();
            this.labelAhora = new System.Windows.Forms.Label();
            this.labelAdescricao = new System.Windows.Forms.Label();
            this.labelAevento = new System.Windows.Forms.Label();
            this.labelHora = new System.Windows.Forms.Label();
            this.labelPonto = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.buttonFecha = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // labelRede
            // 
            this.labelRede.AutoSize = true;
            this.labelRede.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelRede.Location = new System.Drawing.Point(319, 137);
            this.labelRede.Name = "labelRede";
            this.labelRede.Size = new System.Drawing.Size(45, 16);
            this.labelRede.TabIndex = 68;
            this.labelRede.Text = "Rede:";
            // 
            // tanque1
            // 
            this.tanque1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tanque1.enableTransparentBackground = true;
            this.tanque1.largEscala = 50;
            this.tanque1.Location = new System.Drawing.Point(27, 63);
            this.tanque1.Name = "tanque1";
            this.tanque1.Size = new System.Drawing.Size(187, 173);
            this.tanque1.TabIndex = 67;
            this.tanque1.Transparency = false;
            this.tanque1.Visible = false;
            // 
            // labelAhora
            // 
            this.labelAhora.AutoSize = true;
            this.labelAhora.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAhora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAhora.Location = new System.Drawing.Point(245, 145);
            this.labelAhora.Name = "labelAhora";
            this.labelAhora.Size = new System.Drawing.Size(41, 16);
            this.labelAhora.TabIndex = 63;
            this.labelAhora.Text = "Hora:";
            this.labelAhora.Click += new System.EventHandler(this.labelAhora_Click);
            // 
            // labelAdescricao
            // 
            this.labelAdescricao.AutoSize = true;
            this.labelAdescricao.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAdescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdescricao.Location = new System.Drawing.Point(245, 171);
            this.labelAdescricao.MaximumSize = new System.Drawing.Size(290, 65);
            this.labelAdescricao.MinimumSize = new System.Drawing.Size(290, 65);
            this.labelAdescricao.Name = "labelAdescricao";
            this.labelAdescricao.Size = new System.Drawing.Size(290, 65);
            this.labelAdescricao.TabIndex = 64;
            this.labelAdescricao.Text = "Descrição:";
            this.labelAdescricao.Click += new System.EventHandler(this.labelAdescricao_Click);
            // 
            // labelAevento
            // 
            this.labelAevento.AutoSize = true;
            this.labelAevento.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAevento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAevento.Location = new System.Drawing.Point(245, 121);
            this.labelAevento.Name = "labelAevento";
            this.labelAevento.Size = new System.Drawing.Size(158, 16);
            this.labelAevento.TabIndex = 65;
            this.labelAevento.Text = "Último Alarme ou Evento:";
            this.labelAevento.Click += new System.EventHandler(this.labelAevento_Click);
            // 
            // labelHora
            // 
            this.labelHora.AutoSize = true;
            this.labelHora.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHora.Location = new System.Drawing.Point(245, 79);
            this.labelHora.Name = "labelHora";
            this.labelHora.Size = new System.Drawing.Size(119, 16);
            this.labelHora.TabIndex = 59;
            this.labelHora.Text = "Última Atualização";
            // 
            // labelPonto
            // 
            this.labelPonto.AutoSize = true;
            this.labelPonto.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPonto.Location = new System.Drawing.Point(243, 19);
            this.labelPonto.Name = "labelPonto";
            this.labelPonto.Size = new System.Drawing.Size(68, 25);
            this.labelPonto.TabIndex = 58;
            this.labelPonto.Text = "Ponto";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pictureBox2.BackgroundImage = global::SATS.Properties.Resources.ledgray;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(548, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(41, 41);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 66;
            this.pictureBox2.TabStop = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button4.BackgroundImage = global::SATS.Properties.Resources.crystal_clear_action_playlist__2_;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(548, 59);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(41, 36);
            this.button4.TabIndex = 61;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SATS.Properties.Resources.next1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::SATS.Properties.Resources.next1;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(-6, -5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(621, 284);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 62;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::SATS.Properties.Resources.desconect_img;
            this.pictureBox3.Location = new System.Drawing.Point(12, 47);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(523, 205);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 69;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // buttonFecha
            // 
            this.buttonFecha.BackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.BackgroundImage = global::SATS.Properties.Resources.Crystal_Clear_action_button_cancel;
            this.buttonFecha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFecha.FlatAppearance.BorderSize = 0;
            this.buttonFecha.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFecha.Location = new System.Drawing.Point(600, 3);
            this.buttonFecha.Name = "buttonFecha";
            this.buttonFecha.Size = new System.Drawing.Size(15, 15);
            this.buttonFecha.TabIndex = 70;
            this.buttonFecha.UseVisualStyleBackColor = false;
            this.buttonFecha.Click += new System.EventHandler(this.buttonFecha_Click);
            // 
            // TelaNivel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(621, 284);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.tanque1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.labelAhora);
            this.Controls.Add(this.labelAdescricao);
            this.Controls.Add(this.labelAevento);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.labelHora);
            this.Controls.Add(this.labelPonto);
            this.Controls.Add(this.buttonFecha);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelRede);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(621, 284);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(621, 284);
            this.Name = "TelaNivel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TelaNivel";
            this.TransparencyKey = System.Drawing.SystemColors.GradientInactiveCaption;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRede;
        private ReservatorioLiquidos.TanqueEscala tanque1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labelAhora;
        private System.Windows.Forms.Label labelAdescricao;
        private System.Windows.Forms.Label labelAevento;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label labelHora;
        private System.Windows.Forms.Label labelPonto;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button buttonFecha;
    }
}