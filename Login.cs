﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SATS.Controller.DAO;
using SATS.Model;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace SATS
{
    public partial class Login : Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        int X = 0;
        int Y = 0;

        public Login()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MouseDown += new MouseEventHandler(Login_MouseDown);
            this.MouseMove += new MouseEventHandler(Login_MouseMove);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    foreach (Process myService in Process.GetProcesses())
            //    {
            //        if (myService.ProcessName == "satsservice")
            //        {
            //            myService.Kill();
            //        }
            //    }
            //    Process.Start("C:\\Sats\\satsservice.exe");
            //}
            //catch (Exception)
            //{

            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            if (IsConnected() == true)
            {
                //Validação de Campos para as regras de Usuario e Permissao
                if ((this.textBox2.TextLength < 6 || this.textBox2.TextLength > 12) || this.textBox1.TextLength == 0)
                {
                    MessageBox.Show("Todos os Campos são Obrigatórios\nA Senha deve ser entre 6 e 12 caracteres","", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //for (int i=0; i < 3; i++)
                    {
                        try
                        {
                            UsuarioModel novoUsuarioModel = new UsuarioModel(); //Objeto 'Usuario' que conterá os paramentros digitados na interface
                            novoUsuarioModel.Usuario = this.textBox1.Text.ToString();
                            novoUsuarioModel.Senha = this.textBox2.Text.ToString();
                            //Classe que recebe o objeto e tenta realizar o Login validando-o no Banco de Dados
                            UsuarioDAO usuarioDao = new UsuarioDAO();
                            string logado = usuarioDao.Login(novoUsuarioModel);
                            if (logado == "ERRO SENHA")
                            {
                                MessageBox.Show("Usuário ou Senha Incorreto! Tente Novamente","", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                this.textBox2.Text = "";
                                this.textBox2.Focus();
                            }
                            if (logado == "Operador" || logado == "Supervisor")
                            {
                                this.Hide();
                                OnStartup(logado);
                            }
                        }
                        catch (Exception)
                        {
                            //MessageBox.Show("Tente novamente a conexão", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void OnStartup(string permition)
        {
            //base.OnStartup(e);
            Main mainWindow = new Main(textBox1.Text, permition);
            if (Screen.AllScreens.Length > 1)
            {
                Screen s2 = Screen.AllScreens[1];
                Rectangle r2 = s2.WorkingArea;
                mainWindow.Top = r2.Top;
                mainWindow.Left = r2.Left;
                mainWindow.Show();
            }
            else
            {
                Screen s1 = Screen.AllScreens[0];
                Rectangle r1 = s1.WorkingArea;
                mainWindow.Top = r1.Top;
                mainWindow.Left = r1.Left;
                //MessageBox.Show("Essa tela");
                mainWindow.Show();
            }
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void fecha()
        {
            foreach (var process in Process.GetProcessesByName("SATS"))
            {
                process.Kill();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fecha();
            Application.Exit();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) // aqui ele reconhece que foi apertado o ENTER, isso sei que está funcionando  
            {
                button1_Click(sender, e);
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) // aqui ele reconhece que foi apertado o ENTER, isso sei que está funcionando  
            {
                button1_Click(sender, e);
            }
        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            X = this.Left - MousePosition.X;
            Y = this.Top - MousePosition.Y;
        }

        private void Login_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            this.Left = X + MousePosition.X;
            this.Top = Y + MousePosition.Y;
        }
    }
}
