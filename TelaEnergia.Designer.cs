﻿namespace SATS
{
    partial class TelaEnergia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAhora = new System.Windows.Forms.Label();
            this.labelAdescricao = new System.Windows.Forms.Label();
            this.labelAevento = new System.Windows.Forms.Label();
            this.labelHora = new System.Windows.Forms.Label();
            this.labelPonto = new System.Windows.Forms.Label();
            this.gBxEnergia = new System.Windows.Forms.GroupBox();
            this.lbFase3 = new System.Windows.Forms.Label();
            this.lbFase2 = new System.Windows.Forms.Label();
            this.lbFase1 = new System.Windows.Forms.Label();
            this.lbCorrente = new System.Windows.Forms.Label();
            this.lbConsumo = new System.Windows.Forms.Label();
            this.lbPotencia = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.buttonFecha = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gBxEnergia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelAhora
            // 
            this.labelAhora.AutoSize = true;
            this.labelAhora.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAhora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAhora.Location = new System.Drawing.Point(259, 142);
            this.labelAhora.Name = "labelAhora";
            this.labelAhora.Size = new System.Drawing.Size(41, 16);
            this.labelAhora.TabIndex = 137;
            this.labelAhora.Text = "Hora:";
            // 
            // labelAdescricao
            // 
            this.labelAdescricao.AutoSize = true;
            this.labelAdescricao.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAdescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdescricao.Location = new System.Drawing.Point(259, 172);
            this.labelAdescricao.MaximumSize = new System.Drawing.Size(250, 65);
            this.labelAdescricao.MinimumSize = new System.Drawing.Size(250, 65);
            this.labelAdescricao.Name = "labelAdescricao";
            this.labelAdescricao.Size = new System.Drawing.Size(250, 65);
            this.labelAdescricao.TabIndex = 138;
            this.labelAdescricao.Text = "Descrição:";
            // 
            // labelAevento
            // 
            this.labelAevento.AutoSize = true;
            this.labelAevento.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAevento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAevento.Location = new System.Drawing.Point(259, 109);
            this.labelAevento.Name = "labelAevento";
            this.labelAevento.Size = new System.Drawing.Size(158, 16);
            this.labelAevento.TabIndex = 139;
            this.labelAevento.Text = "Último Alarme ou Evento:";
            // 
            // labelHora
            // 
            this.labelHora.AutoSize = true;
            this.labelHora.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHora.Location = new System.Drawing.Point(259, 53);
            this.labelHora.Name = "labelHora";
            this.labelHora.Size = new System.Drawing.Size(119, 16);
            this.labelHora.TabIndex = 134;
            this.labelHora.Text = "Última Atualização";
            // 
            // labelPonto
            // 
            this.labelPonto.AutoSize = true;
            this.labelPonto.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPonto.Location = new System.Drawing.Point(12, 25);
            this.labelPonto.Name = "labelPonto";
            this.labelPonto.Size = new System.Drawing.Size(68, 25);
            this.labelPonto.TabIndex = 133;
            this.labelPonto.Text = "Ponto";
            // 
            // gBxEnergia
            // 
            this.gBxEnergia.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBxEnergia.Controls.Add(this.lbFase3);
            this.gBxEnergia.Controls.Add(this.lbFase2);
            this.gBxEnergia.Controls.Add(this.lbFase1);
            this.gBxEnergia.Controls.Add(this.lbCorrente);
            this.gBxEnergia.Controls.Add(this.lbConsumo);
            this.gBxEnergia.Controls.Add(this.lbPotencia);
            this.gBxEnergia.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBxEnergia.Location = new System.Drawing.Point(12, 53);
            this.gBxEnergia.Name = "gBxEnergia";
            this.gBxEnergia.Size = new System.Drawing.Size(232, 236);
            this.gBxEnergia.TabIndex = 143;
            this.gBxEnergia.TabStop = false;
            this.gBxEnergia.Text = "ENERGIA";
            this.gBxEnergia.Visible = false;
            // 
            // lbFase3
            // 
            this.lbFase3.AutoSize = true;
            this.lbFase3.Location = new System.Drawing.Point(6, 198);
            this.lbFase3.Name = "lbFase3";
            this.lbFase3.Size = new System.Drawing.Size(121, 19);
            this.lbFase3.TabIndex = 34;
            this.lbFase3.Text = "Tensão Fase3:";
            // 
            // lbFase2
            // 
            this.lbFase2.AutoSize = true;
            this.lbFase2.Location = new System.Drawing.Point(8, 165);
            this.lbFase2.Name = "lbFase2";
            this.lbFase2.Size = new System.Drawing.Size(121, 19);
            this.lbFase2.TabIndex = 33;
            this.lbFase2.Text = "Tensão Fase2:";
            // 
            // lbFase1
            // 
            this.lbFase1.AutoSize = true;
            this.lbFase1.Location = new System.Drawing.Point(8, 133);
            this.lbFase1.Name = "lbFase1";
            this.lbFase1.Size = new System.Drawing.Size(121, 19);
            this.lbFase1.TabIndex = 32;
            this.lbFase1.Text = "Tensão Fase1:";
            // 
            // lbCorrente
            // 
            this.lbCorrente.AutoSize = true;
            this.lbCorrente.Location = new System.Drawing.Point(8, 36);
            this.lbCorrente.Name = "lbCorrente";
            this.lbCorrente.Size = new System.Drawing.Size(86, 19);
            this.lbCorrente.TabIndex = 29;
            this.lbCorrente.Text = "Corrente: ";
            // 
            // lbConsumo
            // 
            this.lbConsumo.AutoSize = true;
            this.lbConsumo.Location = new System.Drawing.Point(8, 100);
            this.lbConsumo.Name = "lbConsumo";
            this.lbConsumo.Size = new System.Drawing.Size(94, 19);
            this.lbConsumo.TabIndex = 31;
            this.lbConsumo.Text = "Consumo: ";
            // 
            // lbPotencia
            // 
            this.lbPotencia.AutoSize = true;
            this.lbPotencia.Location = new System.Drawing.Point(8, 67);
            this.lbPotencia.Name = "lbPotencia";
            this.lbPotencia.Size = new System.Drawing.Size(86, 19);
            this.lbPotencia.TabIndex = 30;
            this.lbPotencia.Text = "Potência: ";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::SATS.Properties.Resources.desconect_img;
            this.pictureBox3.Location = new System.Drawing.Point(12, 53);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(490, 248);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 144;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // buttonFecha
            // 
            this.buttonFecha.BackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.BackgroundImage = global::SATS.Properties.Resources.Crystal_Clear_action_button_cancel;
            this.buttonFecha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFecha.FlatAppearance.BorderSize = 0;
            this.buttonFecha.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFecha.Location = new System.Drawing.Point(594, 12);
            this.buttonFecha.Name = "buttonFecha";
            this.buttonFecha.Size = new System.Drawing.Size(15, 15);
            this.buttonFecha.TabIndex = 141;
            this.buttonFecha.UseVisualStyleBackColor = false;
            this.buttonFecha.Click += new System.EventHandler(this.buttonFecha_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pictureBox2.BackgroundImage = global::SATS.Properties.Resources.ledgray;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(565, 64);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(41, 41);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 140;
            this.pictureBox2.TabStop = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button4.BackgroundImage = global::SATS.Properties.Resources.crystal_clear_action_playlist__2_;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(526, 64);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 41);
            this.button4.TabIndex = 135;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SATS.Properties.Resources.next1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::SATS.Properties.Resources.next1;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(-2, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(624, 346);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 136;
            this.pictureBox1.TabStop = false;
            // 
            // TelaEnergia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(618, 340);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.gBxEnergia);
            this.Controls.Add(this.buttonFecha);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.labelAhora);
            this.Controls.Add(this.labelAdescricao);
            this.Controls.Add(this.labelAevento);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.labelHora);
            this.Controls.Add(this.labelPonto);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TelaEnergia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TelaEnergia";
            this.TransparencyKey = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gBxEnergia.ResumeLayout(false);
            this.gBxEnergia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonFecha;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labelAhora;
        private System.Windows.Forms.Label labelAdescricao;
        private System.Windows.Forms.Label labelAevento;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label labelHora;
        private System.Windows.Forms.Label labelPonto;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox gBxEnergia;
        private System.Windows.Forms.Label lbFase3;
        private System.Windows.Forms.Label lbFase2;
        private System.Windows.Forms.Label lbFase1;
        private System.Windows.Forms.Label lbCorrente;
        private System.Windows.Forms.Label lbConsumo;
        private System.Windows.Forms.Label lbPotencia;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}