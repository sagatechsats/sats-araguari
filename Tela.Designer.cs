﻿namespace SATS
{
    partial class Tela
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tela));
            this.labelPonto = new System.Windows.Forms.Label();
            this.labelHora = new System.Windows.Forms.Label();
            this.labelAevento = new System.Windows.Forms.Label();
            this.labelAdescricao = new System.Windows.Forms.Label();
            this.labelAhora = new System.Windows.Forms.Label();
            this.labelAciona = new System.Windows.Forms.Label();
            this.tanque1 = new ReservatorioLiquidos.TanqueEscala();
            this.aquaGaugeVazao = new AquaControls.AquaGauge();
            this.aquaGaugePh = new AquaControls.AquaGauge();
            this.lbCorrente = new System.Windows.Forms.Label();
            this.lbPotencia = new System.Windows.Forms.Label();
            this.lbConsumo = new System.Windows.Forms.Label();
            this.gBxEnergia = new System.Windows.Forms.GroupBox();
            this.lbFase3 = new System.Windows.Forms.Label();
            this.lbFase2 = new System.Windows.Forms.Label();
            this.lbFase1 = new System.Windows.Forms.Label();
            this.lblTurbidez = new System.Windows.Forms.Label();
            this.aquaGaugeInversor = new AquaControls.AquaGauge();
            this.aquaGaugePressao = new AquaControls.AquaGauge();
            this.lblTurbidezLbl = new System.Windows.Forms.Label();
            this.lblAcionamentoAutomatico = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnAcionamentoAutomatico = new System.Windows.Forms.Button();
            this.buttonAciona = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.buttonFecha = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gBxEnergia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelPonto
            // 
            this.labelPonto.AutoSize = true;
            this.labelPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPonto.Location = new System.Drawing.Point(259, 19);
            this.labelPonto.Name = "labelPonto";
            this.labelPonto.Size = new System.Drawing.Size(68, 25);
            this.labelPonto.TabIndex = 4;
            this.labelPonto.Text = "Ponto";
            // 
            // labelHora
            // 
            this.labelHora.AutoSize = true;
            this.labelHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHora.Location = new System.Drawing.Point(261, 61);
            this.labelHora.Name = "labelHora";
            this.labelHora.Size = new System.Drawing.Size(119, 16);
            this.labelHora.TabIndex = 7;
            this.labelHora.Text = "Última Atualização";
            // 
            // labelAevento
            // 
            this.labelAevento.AutoSize = true;
            this.labelAevento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAevento.Location = new System.Drawing.Point(261, 175);
            this.labelAevento.Name = "labelAevento";
            this.labelAevento.Size = new System.Drawing.Size(158, 16);
            this.labelAevento.TabIndex = 7;
            this.labelAevento.Text = "Último Alarme ou Evento:";
            // 
            // labelAdescricao
            // 
            this.labelAdescricao.AutoSize = true;
            this.labelAdescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdescricao.Location = new System.Drawing.Point(262, 239);
            this.labelAdescricao.MaximumSize = new System.Drawing.Size(290, 65);
            this.labelAdescricao.MinimumSize = new System.Drawing.Size(290, 65);
            this.labelAdescricao.Name = "labelAdescricao";
            this.labelAdescricao.Size = new System.Drawing.Size(290, 65);
            this.labelAdescricao.TabIndex = 7;
            this.labelAdescricao.Text = "Descrição:";
            // 
            // labelAhora
            // 
            this.labelAhora.AutoSize = true;
            this.labelAhora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAhora.Location = new System.Drawing.Point(261, 204);
            this.labelAhora.Name = "labelAhora";
            this.labelAhora.Size = new System.Drawing.Size(41, 16);
            this.labelAhora.TabIndex = 7;
            this.labelAhora.Text = "Hora:";
            // 
            // labelAciona
            // 
            this.labelAciona.AutoSize = true;
            this.labelAciona.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAciona.Location = new System.Drawing.Point(262, 100);
            this.labelAciona.Name = "labelAciona";
            this.labelAciona.Size = new System.Drawing.Size(127, 24);
            this.labelAciona.TabIndex = 8;
            this.labelAciona.Text = "Acionamento:";
            // 
            // tanque1
            // 
            this.tanque1.BackColor = System.Drawing.Color.Transparent;
            this.tanque1.enableTransparentBackground = true;
            this.tanque1.largEscala = 50;
            this.tanque1.Location = new System.Drawing.Point(12, 39);
            this.tanque1.Name = "tanque1";
            this.tanque1.Size = new System.Drawing.Size(244, 245);
            this.tanque1.TabIndex = 25;
            this.tanque1.Transparency = false;
            this.tanque1.Visible = false;
            // 
            // aquaGaugeVazao
            // 
            this.aquaGaugeVazao.BackColor = System.Drawing.Color.Transparent;
            this.aquaGaugeVazao.DecimalPlaces = 0;
            this.aquaGaugeVazao.DialAlpha = 255;
            this.aquaGaugeVazao.DialBorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.aquaGaugeVazao.DialColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.aquaGaugeVazao.DialText = "Vazão (l/s)";
            this.aquaGaugeVazao.DialTextColor = System.Drawing.Color.Black;
            this.aquaGaugeVazao.DialTextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aquaGaugeVazao.DialTextVOffset = 0;
            this.aquaGaugeVazao.DigitalValue = 150.12F;
            this.aquaGaugeVazao.DigitalValueBackAlpha = 5;
            this.aquaGaugeVazao.DigitalValueBackColor = System.Drawing.Color.Black;
            this.aquaGaugeVazao.DigitalValueColor = System.Drawing.Color.Black;
            this.aquaGaugeVazao.DigitalValueDecimalPlaces = 2;
            this.aquaGaugeVazao.EnableTransparentBackground = true;
            this.aquaGaugeVazao.ForeColor = System.Drawing.Color.White;
            this.aquaGaugeVazao.Glossiness = 140F;
            this.aquaGaugeVazao.Location = new System.Drawing.Point(-35, 7);
            this.aquaGaugeVazao.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.aquaGaugeVazao.MaxValue = 300F;
            this.aquaGaugeVazao.MinValue = 0F;
            this.aquaGaugeVazao.Name = "aquaGaugeVazao";
            this.aquaGaugeVazao.NoOfSubDivisions = 5;
            this.aquaGaugeVazao.PointerColor = System.Drawing.Color.Black;
            this.aquaGaugeVazao.RimAlpha = 255;
            this.aquaGaugeVazao.RimColor = System.Drawing.Color.White;
            this.aquaGaugeVazao.ScaleColor = System.Drawing.Color.Black;
            this.aquaGaugeVazao.ScaleFontSizeDivider = 22;
            this.aquaGaugeVazao.Size = new System.Drawing.Size(415, 415);
            this.aquaGaugeVazao.TabIndex = 27;
            this.aquaGaugeVazao.Threshold1Color = System.Drawing.Color.Yellow;
            this.aquaGaugeVazao.Threshold1Start = 3F;
            this.aquaGaugeVazao.Threshold1Stop = 6F;
            this.aquaGaugeVazao.Threshold2Color = System.Drawing.Color.Red;
            this.aquaGaugeVazao.Threshold2Start = 0F;
            this.aquaGaugeVazao.Threshold2Stop = 3F;
            this.aquaGaugeVazao.Value = 150.12F;
            this.aquaGaugeVazao.ValueToDigital = false;
            // 
            // aquaGaugePh
            // 
            this.aquaGaugePh.BackColor = System.Drawing.Color.Transparent;
            this.aquaGaugePh.DecimalPlaces = 0;
            this.aquaGaugePh.DialAlpha = 255;
            this.aquaGaugePh.DialBorderColor = System.Drawing.Color.SteelBlue;
            this.aquaGaugePh.DialColor = System.Drawing.Color.LightSteelBlue;
            this.aquaGaugePh.DialText = "PH";
            this.aquaGaugePh.DialTextColor = System.Drawing.Color.MidnightBlue;
            this.aquaGaugePh.DialTextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aquaGaugePh.DialTextVOffset = 0;
            this.aquaGaugePh.DigitalValue = 6.756F;
            this.aquaGaugePh.DigitalValueBackAlpha = 5;
            this.aquaGaugePh.DigitalValueBackColor = System.Drawing.Color.LightSteelBlue;
            this.aquaGaugePh.DigitalValueColor = System.Drawing.Color.MidnightBlue;
            this.aquaGaugePh.DigitalValueDecimalPlaces = 2;
            this.aquaGaugePh.EnableTransparentBackground = true;
            this.aquaGaugePh.ForeColor = System.Drawing.Color.White;
            this.aquaGaugePh.Glossiness = 140F;
            this.aquaGaugePh.Location = new System.Drawing.Point(-14, 93);
            this.aquaGaugePh.MaxValue = 14F;
            this.aquaGaugePh.MinValue = 0F;
            this.aquaGaugePh.Name = "aquaGaugePh";
            this.aquaGaugePh.NoOfSubDivisions = 9;
            this.aquaGaugePh.PointerColor = System.Drawing.Color.Black;
            this.aquaGaugePh.RimAlpha = 200;
            this.aquaGaugePh.RimColor = System.Drawing.Color.SpringGreen;
            this.aquaGaugePh.ScaleColor = System.Drawing.Color.Black;
            this.aquaGaugePh.ScaleFontSizeDivider = 25;
            this.aquaGaugePh.Size = new System.Drawing.Size(249, 249);
            this.aquaGaugePh.TabIndex = 28;
            this.aquaGaugePh.Threshold1Color = System.Drawing.Color.Red;
            this.aquaGaugePh.Threshold1Start = 0F;
            this.aquaGaugePh.Threshold1Stop = 6.2F;
            this.aquaGaugePh.Threshold2Color = System.Drawing.Color.Blue;
            this.aquaGaugePh.Threshold2Start = 7.8F;
            this.aquaGaugePh.Threshold2Stop = 14F;
            this.aquaGaugePh.Value = 6.756F;
            this.aquaGaugePh.ValueToDigital = false;
            // 
            // lbCorrente
            // 
            this.lbCorrente.AutoSize = true;
            this.lbCorrente.Location = new System.Drawing.Point(8, 36);
            this.lbCorrente.Name = "lbCorrente";
            this.lbCorrente.Size = new System.Drawing.Size(86, 19);
            this.lbCorrente.TabIndex = 29;
            this.lbCorrente.Text = "Corrente: ";
            // 
            // lbPotencia
            // 
            this.lbPotencia.AutoSize = true;
            this.lbPotencia.Location = new System.Drawing.Point(8, 67);
            this.lbPotencia.Name = "lbPotencia";
            this.lbPotencia.Size = new System.Drawing.Size(86, 19);
            this.lbPotencia.TabIndex = 30;
            this.lbPotencia.Text = "Potência: ";
            // 
            // lbConsumo
            // 
            this.lbConsumo.AutoSize = true;
            this.lbConsumo.Location = new System.Drawing.Point(8, 100);
            this.lbConsumo.Name = "lbConsumo";
            this.lbConsumo.Size = new System.Drawing.Size(94, 19);
            this.lbConsumo.TabIndex = 31;
            this.lbConsumo.Text = "Consumo: ";
            // 
            // gBxEnergia
            // 
            this.gBxEnergia.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBxEnergia.Controls.Add(this.pictureBox3);
            this.gBxEnergia.Controls.Add(this.lblTurbidez);
            this.gBxEnergia.Controls.Add(this.lblTurbidezLbl);
            this.gBxEnergia.Controls.Add(this.aquaGaugeVazao);
            this.gBxEnergia.Controls.Add(this.lbFase3);
            this.gBxEnergia.Controls.Add(this.aquaGaugeInversor);
            this.gBxEnergia.Controls.Add(this.aquaGaugePressao);
            this.gBxEnergia.Controls.Add(this.lbFase2);
            this.gBxEnergia.Controls.Add(this.lbFase1);
            this.gBxEnergia.Controls.Add(this.lbCorrente);
            this.gBxEnergia.Controls.Add(this.lbConsumo);
            this.gBxEnergia.Controls.Add(this.lbPotencia);
            this.gBxEnergia.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBxEnergia.Location = new System.Drawing.Point(21, 19);
            this.gBxEnergia.Name = "gBxEnergia";
            this.gBxEnergia.Size = new System.Drawing.Size(232, 236);
            this.gBxEnergia.TabIndex = 32;
            this.gBxEnergia.TabStop = false;
            this.gBxEnergia.Text = "ENERGIA";
            this.gBxEnergia.Visible = false;
            // 
            // lbFase3
            // 
            this.lbFase3.AutoSize = true;
            this.lbFase3.Location = new System.Drawing.Point(6, 198);
            this.lbFase3.Name = "lbFase3";
            this.lbFase3.Size = new System.Drawing.Size(121, 19);
            this.lbFase3.TabIndex = 34;
            this.lbFase3.Text = "Tensão Fase3:";
            // 
            // lbFase2
            // 
            this.lbFase2.AutoSize = true;
            this.lbFase2.Location = new System.Drawing.Point(8, 165);
            this.lbFase2.Name = "lbFase2";
            this.lbFase2.Size = new System.Drawing.Size(121, 19);
            this.lbFase2.TabIndex = 33;
            this.lbFase2.Text = "Tensão Fase2:";
            // 
            // lbFase1
            // 
            this.lbFase1.AutoSize = true;
            this.lbFase1.Location = new System.Drawing.Point(8, 133);
            this.lbFase1.Name = "lbFase1";
            this.lbFase1.Size = new System.Drawing.Size(121, 19);
            this.lbFase1.TabIndex = 32;
            this.lbFase1.Text = "Tensão Fase1:";
            // 
            // lblTurbidez
            // 
            this.lblTurbidez.BackColor = System.Drawing.Color.White;
            this.lblTurbidez.Font = new System.Drawing.Font("Arial", 69.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurbidez.Location = new System.Drawing.Point(-6, 99);
            this.lblTurbidez.Name = "lblTurbidez";
            this.lblTurbidez.Size = new System.Drawing.Size(232, 157);
            this.lblTurbidez.TabIndex = 36;
            this.lblTurbidez.Text = "7,00";
            this.lblTurbidez.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblTurbidez.Visible = false;
            // 
            // aquaGaugeInversor
            // 
            this.aquaGaugeInversor.BackColor = System.Drawing.Color.Transparent;
            this.aquaGaugeInversor.DecimalPlaces = 0;
            this.aquaGaugeInversor.DialAlpha = 255;
            this.aquaGaugeInversor.DialBorderColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.DialColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.aquaGaugeInversor.DialText = "Inversor (KHz)";
            this.aquaGaugeInversor.DialTextColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.DialTextFont = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aquaGaugeInversor.DialTextVOffset = 12;
            this.aquaGaugeInversor.DigitalValue = 0F;
            this.aquaGaugeInversor.DigitalValueBackAlpha = 255;
            this.aquaGaugeInversor.DigitalValueBackColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.DigitalValueColor = System.Drawing.Color.Red;
            this.aquaGaugeInversor.DigitalValueDecimalPlaces = 2;
            this.aquaGaugeInversor.Enabled = false;
            this.aquaGaugeInversor.EnableTransparentBackground = true;
            this.aquaGaugeInversor.ForeColor = System.Drawing.Color.White;
            this.aquaGaugeInversor.Glossiness = 40F;
            this.aquaGaugeInversor.Location = new System.Drawing.Point(0, 42);
            this.aquaGaugeInversor.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.aquaGaugeInversor.MaxValue = 60F;
            this.aquaGaugeInversor.MinValue = 0F;
            this.aquaGaugeInversor.Name = "aquaGaugeInversor";
            this.aquaGaugeInversor.NoOfDivisions = 12;
            this.aquaGaugeInversor.NoOfSubDivisions = 5;
            this.aquaGaugeInversor.PointerColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.RimAlpha = 255;
            this.aquaGaugeInversor.RimColor = System.Drawing.Color.MediumSpringGreen;
            this.aquaGaugeInversor.ScaleColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.ScaleFontSizeDivider = 20;
            this.aquaGaugeInversor.Size = new System.Drawing.Size(400, 400);
            this.aquaGaugeInversor.TabIndex = 34;
            this.aquaGaugeInversor.Threshold1Color = System.Drawing.Color.Yellow;
            this.aquaGaugeInversor.Threshold1Start = 30F;
            this.aquaGaugeInversor.Threshold1Stop = 50F;
            this.aquaGaugeInversor.Threshold2Color = System.Drawing.Color.Red;
            this.aquaGaugeInversor.Threshold2Start = 50F;
            this.aquaGaugeInversor.Threshold2Stop = 60F;
            this.aquaGaugeInversor.Value = 0F;
            this.aquaGaugeInversor.ValueToDigital = false;
            this.aquaGaugeInversor.Visible = false;
            // 
            // aquaGaugePressao
            // 
            this.aquaGaugePressao.BackColor = System.Drawing.Color.Transparent;
            this.aquaGaugePressao.DecimalPlaces = 0;
            this.aquaGaugePressao.DialAlpha = 255;
            this.aquaGaugePressao.DialBorderColor = System.Drawing.Color.Black;
            this.aquaGaugePressao.DialColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.aquaGaugePressao.DialText = "Pressão (mca)";
            this.aquaGaugePressao.DialTextColor = System.Drawing.Color.Black;
            this.aquaGaugePressao.DialTextFont = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aquaGaugePressao.DialTextVOffset = 12;
            this.aquaGaugePressao.DigitalValue = 0F;
            this.aquaGaugePressao.DigitalValueBackAlpha = 255;
            this.aquaGaugePressao.DigitalValueBackColor = System.Drawing.Color.Black;
            this.aquaGaugePressao.DigitalValueColor = System.Drawing.Color.Red;
            this.aquaGaugePressao.DigitalValueDecimalPlaces = 2;
            this.aquaGaugePressao.Enabled = false;
            this.aquaGaugePressao.EnableTransparentBackground = true;
            this.aquaGaugePressao.ForeColor = System.Drawing.Color.White;
            this.aquaGaugePressao.Glossiness = 40F;
            this.aquaGaugePressao.Location = new System.Drawing.Point(-26, 9);
            this.aquaGaugePressao.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.aquaGaugePressao.MaxValue = 50F;
            this.aquaGaugePressao.MinValue = 0F;
            this.aquaGaugePressao.Name = "aquaGaugePressao";
            this.aquaGaugePressao.NoOfSubDivisions = 5;
            this.aquaGaugePressao.PointerColor = System.Drawing.Color.Black;
            this.aquaGaugePressao.RimAlpha = 255;
            this.aquaGaugePressao.RimColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.aquaGaugePressao.ScaleColor = System.Drawing.Color.Black;
            this.aquaGaugePressao.ScaleFontSizeDivider = 22;
            this.aquaGaugePressao.Size = new System.Drawing.Size(400, 400);
            this.aquaGaugePressao.TabIndex = 33;
            this.aquaGaugePressao.Threshold1Color = System.Drawing.Color.Yellow;
            this.aquaGaugePressao.Threshold1Start = 15F;
            this.aquaGaugePressao.Threshold1Stop = 30F;
            this.aquaGaugePressao.Threshold2Color = System.Drawing.Color.Red;
            this.aquaGaugePressao.Threshold2Start = 30F;
            this.aquaGaugePressao.Threshold2Stop = 50F;
            this.aquaGaugePressao.Value = 0F;
            this.aquaGaugePressao.ValueToDigital = false;
            this.aquaGaugePressao.Visible = false;
            // 
            // lblTurbidezLbl
            // 
            this.lblTurbidezLbl.BackColor = System.Drawing.Color.Gainsboro;
            this.lblTurbidezLbl.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurbidezLbl.Location = new System.Drawing.Point(-26, 20);
            this.lblTurbidezLbl.Name = "lblTurbidezLbl";
            this.lblTurbidezLbl.Size = new System.Drawing.Size(232, 79);
            this.lblTurbidezLbl.TabIndex = 35;
            this.lblTurbidezLbl.Text = "Turbidez (NTU)";
            this.lblTurbidezLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTurbidezLbl.Visible = false;
            // 
            // lblAcionamentoAutomatico
            // 
            this.lblAcionamentoAutomatico.AutoSize = true;
            this.lblAcionamentoAutomatico.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblAcionamentoAutomatico.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcionamentoAutomatico.Location = new System.Drawing.Point(259, 134);
            this.lblAcionamentoAutomatico.Name = "lblAcionamentoAutomatico";
            this.lblAcionamentoAutomatico.Size = new System.Drawing.Size(226, 24);
            this.lblAcionamentoAutomatico.TabIndex = 35;
            this.lblAcionamentoAutomatico.Text = "Acionamento Automatico:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::SATS.Properties.Resources.desconect_img;
            this.pictureBox3.Location = new System.Drawing.Point(-82, 58);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(337, 265);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // btnAcionamentoAutomatico
            // 
            this.btnAcionamentoAutomatico.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnAcionamentoAutomatico.BackgroundImage = global::SATS.Properties.Resources.switchoff;
            this.btnAcionamentoAutomatico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAcionamentoAutomatico.Enabled = false;
            this.btnAcionamentoAutomatico.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnAcionamentoAutomatico.FlatAppearance.BorderSize = 0;
            this.btnAcionamentoAutomatico.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAcionamentoAutomatico.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnAcionamentoAutomatico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAcionamentoAutomatico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAcionamentoAutomatico.ForeColor = System.Drawing.Color.Black;
            this.btnAcionamentoAutomatico.Location = new System.Drawing.Point(511, 130);
            this.btnAcionamentoAutomatico.Name = "btnAcionamentoAutomatico";
            this.btnAcionamentoAutomatico.Size = new System.Drawing.Size(88, 28);
            this.btnAcionamentoAutomatico.TabIndex = 36;
            this.btnAcionamentoAutomatico.UseVisualStyleBackColor = false;
            this.btnAcionamentoAutomatico.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonAciona
            // 
            this.buttonAciona.BackColor = System.Drawing.Color.Transparent;
            this.buttonAciona.BackgroundImage = global::SATS.Properties.Resources.switchoff;
            this.buttonAciona.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonAciona.Enabled = false;
            this.buttonAciona.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonAciona.FlatAppearance.BorderSize = 0;
            this.buttonAciona.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonAciona.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonAciona.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAciona.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAciona.ForeColor = System.Drawing.Color.Black;
            this.buttonAciona.Location = new System.Drawing.Point(511, 96);
            this.buttonAciona.Name = "buttonAciona";
            this.buttonAciona.Size = new System.Drawing.Size(88, 28);
            this.buttonAciona.TabIndex = 6;
            this.buttonAciona.UseVisualStyleBackColor = false;
            this.buttonAciona.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::SATS.Properties.Resources.crystal_clear_action_playlist__2_;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(526, 50);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 34);
            this.button4.TabIndex = 10;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::SATS.Properties.Resources.ledgray;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(565, 43);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(41, 41);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // buttonFecha
            // 
            this.buttonFecha.BackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.BackgroundImage = global::SATS.Properties.Resources.Crystal_Clear_action_button_cancel;
            this.buttonFecha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFecha.FlatAppearance.BorderSize = 0;
            this.buttonFecha.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFecha.Location = new System.Drawing.Point(600, 3);
            this.buttonFecha.Name = "buttonFecha";
            this.buttonFecha.Size = new System.Drawing.Size(15, 15);
            this.buttonFecha.TabIndex = 2;
            this.buttonFecha.UseVisualStyleBackColor = false;
            this.buttonFecha.Click += new System.EventHandler(this.button1_Click);
            this.buttonFecha.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button1_MouseDown);
            this.buttonFecha.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.buttonFecha.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            this.buttonFecha.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button1_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SATS.Properties.Resources.next1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::SATS.Properties.Resources.next1;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(2, -7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(613, 350);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // Tela
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(618, 340);
            this.Controls.Add(this.btnAcionamentoAutomatico);
            this.Controls.Add(this.lblAcionamentoAutomatico);
            this.Controls.Add(this.buttonAciona);
            this.Controls.Add(this.gBxEnergia);
            this.Controls.Add(this.aquaGaugePh);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.labelAciona);
            this.Controls.Add(this.labelAhora);
            this.Controls.Add(this.labelAdescricao);
            this.Controls.Add(this.labelAevento);
            this.Controls.Add(this.labelHora);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.buttonFecha);
            this.Controls.Add(this.labelPonto);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tanque1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(618, 340);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(618, 340);
            this.Name = "Tela";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ponto";
            this.TransparencyKey = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Load += new System.EventHandler(this.Tela_Load);
            this.gBxEnergia.ResumeLayout(false);
            this.gBxEnergia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFecha;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelPonto;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button buttonAciona;
        private System.Windows.Forms.Label labelHora;
        private System.Windows.Forms.Label labelAevento;
        private System.Windows.Forms.Label labelAdescricao;
        private System.Windows.Forms.Label labelAhora;
        private System.Windows.Forms.Label labelAciona;
        private System.Windows.Forms.Button button4;
        private ReservatorioLiquidos.TanqueEscala tanque1;
        private AquaControls.AquaGauge aquaGaugeVazao;
        private AquaControls.AquaGauge aquaGaugePh;
        private System.Windows.Forms.Label lbCorrente;
        private System.Windows.Forms.Label lbPotencia;
        private System.Windows.Forms.Label lbConsumo;
        private System.Windows.Forms.GroupBox gBxEnergia;
        private System.Windows.Forms.Label lbFase3;
        private System.Windows.Forms.Label lbFase2;
        private System.Windows.Forms.Label lbFase1;
        private System.Windows.Forms.Label lblTurbidez;
        private System.Windows.Forms.Label lblTurbidezLbl;
        private AquaControls.AquaGauge aquaGaugePressao;
        private AquaControls.AquaGauge aquaGaugeInversor;
        private System.Windows.Forms.Label lblAcionamentoAutomatico;
        private System.Windows.Forms.Button btnAcionamentoAutomatico;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}