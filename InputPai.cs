﻿using System;
using System.Windows.Forms;
using DevComponents.DotNetBar;


namespace SATS
{
    public partial class InputPai : Office2007Form
    {
        public InputPai()
        {
            InitializeComponent();
        }

        public static DialogResult show(string Headtext, ref string value)
        {
            InputPai msbox = new InputPai();
            msbox.TitleText = Headtext;
            msbox.textBox1.Text = value;
            msbox.button1.DialogResult = DialogResult.OK;
            msbox.button2.DialogResult = DialogResult.Cancel;
            msbox.reflectionLabel1.Text = "<b><font size='+2'>" + Headtext.ToString() + "</font></b>";
            msbox.AcceptButton = msbox.button1;
            msbox.CancelButton = msbox.button2;
            DialogResult dialogResult = msbox.ShowDialog();
            value = msbox.textBox1.Text;
            return dialogResult;
        }

        private void inputPai_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void SeuTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }

        }
    }
}
