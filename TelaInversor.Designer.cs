﻿namespace SATS
{
    partial class TelaInversor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.buttonFecha = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelAhora = new System.Windows.Forms.Label();
            this.labelAdescricao = new System.Windows.Forms.Label();
            this.labelAevento = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.labelHora = new System.Windows.Forms.Label();
            this.labelPonto = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.aquaGaugeInversor = new AquaControls.AquaGauge();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pictureBox3.Image = global::SATS.Properties.Resources.desconect_img;
            this.pictureBox3.Location = new System.Drawing.Point(12, 56);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(509, 272);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 142;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // buttonFecha
            // 
            this.buttonFecha.BackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.BackgroundImage = global::SATS.Properties.Resources.Crystal_Clear_action_button_cancel;
            this.buttonFecha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFecha.FlatAppearance.BorderSize = 0;
            this.buttonFecha.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFecha.Location = new System.Drawing.Point(609, 2);
            this.buttonFecha.Name = "buttonFecha";
            this.buttonFecha.Size = new System.Drawing.Size(15, 15);
            this.buttonFecha.TabIndex = 141;
            this.buttonFecha.UseVisualStyleBackColor = false;
            this.buttonFecha.Click += new System.EventHandler(this.buttonFecha_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pictureBox2.BackgroundImage = global::SATS.Properties.Resources.ledgray;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(572, 81);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(41, 41);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 140;
            this.pictureBox2.TabStop = false;
            // 
            // labelAhora
            // 
            this.labelAhora.AutoSize = true;
            this.labelAhora.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAhora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAhora.Location = new System.Drawing.Point(259, 135);
            this.labelAhora.Name = "labelAhora";
            this.labelAhora.Size = new System.Drawing.Size(41, 16);
            this.labelAhora.TabIndex = 137;
            this.labelAhora.Text = "Hora:";
            // 
            // labelAdescricao
            // 
            this.labelAdescricao.AutoSize = true;
            this.labelAdescricao.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAdescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdescricao.Location = new System.Drawing.Point(259, 186);
            this.labelAdescricao.MaximumSize = new System.Drawing.Size(290, 65);
            this.labelAdescricao.MinimumSize = new System.Drawing.Size(290, 65);
            this.labelAdescricao.Name = "labelAdescricao";
            this.labelAdescricao.Size = new System.Drawing.Size(290, 65);
            this.labelAdescricao.TabIndex = 138;
            this.labelAdescricao.Text = "Descrição:";
            // 
            // labelAevento
            // 
            this.labelAevento.AutoSize = true;
            this.labelAevento.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAevento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAevento.Location = new System.Drawing.Point(259, 93);
            this.labelAevento.Name = "labelAevento";
            this.labelAevento.Size = new System.Drawing.Size(158, 16);
            this.labelAevento.TabIndex = 139;
            this.labelAevento.Text = "Último Alarme ou Evento:";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button4.BackgroundImage = global::SATS.Properties.Resources.crystal_clear_action_playlist__2_;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(527, 81);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(39, 41);
            this.button4.TabIndex = 135;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // labelHora
            // 
            this.labelHora.AutoSize = true;
            this.labelHora.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHora.Location = new System.Drawing.Point(259, 53);
            this.labelHora.Name = "labelHora";
            this.labelHora.Size = new System.Drawing.Size(119, 16);
            this.labelHora.TabIndex = 134;
            this.labelHora.Text = "Última Atualização";
            // 
            // labelPonto
            // 
            this.labelPonto.AutoSize = true;
            this.labelPonto.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPonto.Location = new System.Drawing.Point(34, 34);
            this.labelPonto.Name = "labelPonto";
            this.labelPonto.Size = new System.Drawing.Size(68, 25);
            this.labelPonto.TabIndex = 133;
            this.labelPonto.Text = "Ponto";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::SATS.Properties.Resources.next1;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(622, 349);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 136;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // aquaGaugeInversor
            // 
            this.aquaGaugeInversor.BackColor = System.Drawing.Color.Transparent;
            this.aquaGaugeInversor.DecimalPlaces = 0;
            this.aquaGaugeInversor.DialAlpha = 255;
            this.aquaGaugeInversor.DialBorderColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.DialColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.aquaGaugeInversor.DialText = "Inversor (KHz)";
            this.aquaGaugeInversor.DialTextColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.DialTextFont = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aquaGaugeInversor.DialTextVOffset = 12;
            this.aquaGaugeInversor.DigitalValue = 0F;
            this.aquaGaugeInversor.DigitalValueBackAlpha = 255;
            this.aquaGaugeInversor.DigitalValueBackColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.DigitalValueColor = System.Drawing.Color.Red;
            this.aquaGaugeInversor.DigitalValueDecimalPlaces = 2;
            this.aquaGaugeInversor.Enabled = false;
            this.aquaGaugeInversor.EnableTransparentBackground = true;
            this.aquaGaugeInversor.ForeColor = System.Drawing.Color.White;
            this.aquaGaugeInversor.Glossiness = 40F;
            this.aquaGaugeInversor.Location = new System.Drawing.Point(12, 62);
            this.aquaGaugeInversor.MaxValue = 60F;
            this.aquaGaugeInversor.MinValue = 0F;
            this.aquaGaugeInversor.Name = "aquaGaugeInversor";
            this.aquaGaugeInversor.NoOfDivisions = 12;
            this.aquaGaugeInversor.NoOfSubDivisions = 5;
            this.aquaGaugeInversor.PointerColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.RimAlpha = 255;
            this.aquaGaugeInversor.RimColor = System.Drawing.Color.MediumSpringGreen;
            this.aquaGaugeInversor.ScaleColor = System.Drawing.Color.Black;
            this.aquaGaugeInversor.ScaleFontSizeDivider = 20;
            this.aquaGaugeInversor.Size = new System.Drawing.Size(226, 226);
            this.aquaGaugeInversor.TabIndex = 143;
            this.aquaGaugeInversor.Threshold1Color = System.Drawing.Color.Yellow;
            this.aquaGaugeInversor.Threshold1Start = 30F;
            this.aquaGaugeInversor.Threshold1Stop = 50F;
            this.aquaGaugeInversor.Threshold2Color = System.Drawing.Color.Red;
            this.aquaGaugeInversor.Threshold2Start = 50F;
            this.aquaGaugeInversor.Threshold2Stop = 60F;
            this.aquaGaugeInversor.Value = 0F;
            this.aquaGaugeInversor.ValueToDigital = false;
            this.aquaGaugeInversor.Visible = false;
            // 
            // TelaInversor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(625, 340);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.aquaGaugeInversor);
            this.Controls.Add(this.buttonFecha);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.labelAhora);
            this.Controls.Add(this.labelAdescricao);
            this.Controls.Add(this.labelAevento);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.labelHora);
            this.Controls.Add(this.labelPonto);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TelaInversor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TelaInversor";
            this.TransparencyKey = System.Drawing.SystemColors.GradientInactiveCaption;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button buttonFecha;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labelAhora;
        private System.Windows.Forms.Label labelAdescricao;
        private System.Windows.Forms.Label labelAevento;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label labelHora;
        private System.Windows.Forms.Label labelPonto;
        public System.Windows.Forms.PictureBox pictureBox1;
        private AquaControls.AquaGauge aquaGaugeInversor;
    }
}