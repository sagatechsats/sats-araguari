﻿using System.Collections;

namespace SATS
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected void EnablePontoTransparencyPicture(Ponto ponto)
        {
            // Código da net pra transparencia
            // Comentar mais depois
            var pos = this.PointToScreen(ponto.Location);
            //ponto.Parent = pictureBox1;
            //ponto.Location = pictureBox1.PointToClient(pos);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        // string = "";
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeAçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dadosDoPoçoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pontoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preventivaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AutomaticoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraçãoSetPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraçãoDeAlarmeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alarmesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pressãoEFrequênciaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bombaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nívelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pressãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inversorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consumoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.energiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alarmesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gastosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chaveSeletoraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pSolteiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vazaoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarGastosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarMicromediçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ativarEdiçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripLastUpdate = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripAlarmeEvento = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.Fatima = new System.Windows.Forms.TabPage();
            this.fatimaVazaoBomba2 = new System.Windows.Forms.Label();
            this.waterMeter14 = new PumpControlLibrary.WaterMeter();
            this.fatimaVazaoBomba1 = new System.Windows.Forms.Label();
            this.waterMeter13 = new PumpControlLibrary.WaterMeter();
            this.label79 = new System.Windows.Forms.Label();
            this.junção97 = new PumpControlLibrary.Junção();
            this.junção89 = new PumpControlLibrary.Junção();
            this.label77 = new System.Windows.Forms.Label();
            this.junção85 = new PumpControlLibrary.Junção();
            this.junção86 = new PumpControlLibrary.Junção();
            this.junção88 = new PumpControlLibrary.Junção();
            this.fatimaBomba3 = new PumpControlLibrary.Pump();
            this.junção90 = new PumpControlLibrary.Junção();
            this.junção91 = new PumpControlLibrary.Junção();
            this.junção92 = new PumpControlLibrary.Junção();
            this.fatimaBomba2 = new PumpControlLibrary.Pump();
            this.fatimaCano2Bomba2 = new ReservatorioLiquidos.cano();
            this.junção93 = new PumpControlLibrary.Junção();
            this.junção95 = new PumpControlLibrary.Junção();
            this.junção96 = new PumpControlLibrary.Junção();
            this.label78 = new System.Windows.Forms.Label();
            this.fatimaBomba1 = new PumpControlLibrary.Pump();
            this.fatimaCano2Bomba1 = new ReservatorioLiquidos.cano();
            this.fatimaCano3Bomba1 = new ReservatorioLiquidos.cano();
            this.fatimaCano1Bomba3 = new ReservatorioLiquidos.cano();
            this.junção84 = new PumpControlLibrary.Junção();
            this.junçãoT51 = new PumpControlLibrary.JunçãoT();
            this.label69 = new System.Windows.Forms.Label();
            this.junçãoT44 = new PumpControlLibrary.JunçãoT();
            this.fatimaPoco11 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco11 = new ReservatorioLiquidos.cano();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.junçãoT45 = new PumpControlLibrary.JunçãoT();
            this.junção83 = new PumpControlLibrary.Junção();
            this.junçãoT46 = new PumpControlLibrary.JunçãoT();
            this.fatimaPoco12 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco13 = new ReservatorioLiquidos.cano();
            this.junçãoT47 = new PumpControlLibrary.JunçãoT();
            this.junçãoT48 = new PumpControlLibrary.JunçãoT();
            this.junçãoT49 = new PumpControlLibrary.JunçãoT();
            this.junçãoT50 = new PumpControlLibrary.JunçãoT();
            this.fatimaCanoPoco17 = new ReservatorioLiquidos.cano();
            this.fatimaPoco13 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco14 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco15 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco16 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco12 = new ReservatorioLiquidos.cano();
            this.fatimaCanoPoco14 = new ReservatorioLiquidos.cano();
            this.fatimaCanoPoco15 = new ReservatorioLiquidos.cano();
            this.fatimaCanoPoco16 = new ReservatorioLiquidos.cano();
            this.fatimaPoco18 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco17 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco18 = new ReservatorioLiquidos.cano();
            this.fatimaTanque2 = new ReservatorioLiquidos.Tanque();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.junçãoT41 = new PumpControlLibrary.JunçãoT();
            this.junçãoT42 = new PumpControlLibrary.JunçãoT();
            this.fatimaPoco1 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco2 = new ReservatorioLiquidos.cano();
            this.junçãoT43 = new PumpControlLibrary.JunçãoT();
            this.fatimaPoco2 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco3 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco3 = new ReservatorioLiquidos.cano();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.junçãoT35 = new PumpControlLibrary.JunçãoT();
            this.junção81 = new PumpControlLibrary.Junção();
            this.fatimaTanque1 = new ReservatorioLiquidos.Tanque();
            this.junção82 = new PumpControlLibrary.Junção();
            this.junçãoT36 = new PumpControlLibrary.JunçãoT();
            this.fatimaPoco4 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco5 = new ReservatorioLiquidos.cano();
            this.junçãoT37 = new PumpControlLibrary.JunçãoT();
            this.junçãoT38 = new PumpControlLibrary.JunçãoT();
            this.junçãoT39 = new PumpControlLibrary.JunçãoT();
            this.junçãoT40 = new PumpControlLibrary.JunçãoT();
            this.fatimaCanoPoco9 = new ReservatorioLiquidos.cano();
            this.fatimaPoco5 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco6 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco7 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco8 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco4 = new ReservatorioLiquidos.cano();
            this.fatimaCanoPoco6 = new ReservatorioLiquidos.cano();
            this.fatimaCanoPoco7 = new ReservatorioLiquidos.cano();
            this.fatimaCanoPoco8 = new ReservatorioLiquidos.cano();
            this.fatimaPoco10 = new PumpControlLibrary.SubmersivePump();
            this.fatimaPoco9 = new PumpControlLibrary.SubmersivePump();
            this.fatimaCanoPoco10 = new ReservatorioLiquidos.cano();
            this.fatimaCanoPoco1 = new ReservatorioLiquidos.cano();
            this.fatimaCano1Poco11 = new ReservatorioLiquidos.cano();
            this.fatimaCanoPocos = new ReservatorioLiquidos.cano();
            this.junção94 = new PumpControlLibrary.Junção();
            this.fatimaCanoBomba1 = new ReservatorioLiquidos.cano();
            this.fatimaCanoBomba2 = new ReservatorioLiquidos.cano();
            this.junção87 = new PumpControlLibrary.Junção();
            this.fatimaCanoBomba3 = new ReservatorioLiquidos.cano();
            this.fatimaCano3Bomba2 = new ReservatorioLiquidos.cano();
            this.junção98 = new PumpControlLibrary.Junção();
            this.fatimaCanoGoias = new ReservatorioLiquidos.cano();
            this.fatimaCano1Bomba2 = new ReservatorioLiquidos.cano();
            this.fatimaCano1Bomba1 = new ReservatorioLiquidos.cano();
            this.JardimBotanico = new System.Windows.Forms.TabPage();
            this.junção2 = new PumpControlLibrary.Junção();
            this.jardimTanque1 = new ReservatorioLiquidos.Tanque();
            this.junção32 = new PumpControlLibrary.Junção();
            this.junção29 = new PumpControlLibrary.Junção();
            this.junção1 = new PumpControlLibrary.Junção();
            this.junçãoT10 = new PumpControlLibrary.JunçãoT();
            this.jardimCano2Poco2 = new ReservatorioLiquidos.cano();
            this.jardimCano1Poco2 = new ReservatorioLiquidos.cano();
            this.junçãoT9 = new PumpControlLibrary.JunçãoT();
            this.junção28 = new PumpControlLibrary.Junção();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.jardimCanoOut = new ReservatorioLiquidos.cano();
            this.jardimCanoPoco = new ReservatorioLiquidos.cano();
            this.jardimCanoPoco1 = new ReservatorioLiquidos.cano();
            this.jardimCanoPoco2 = new ReservatorioLiquidos.cano();
            this.jardimPoco1 = new PumpControlLibrary.SubmersivePump();
            this.junção31 = new PumpControlLibrary.Junção();
            this.jardimCano2Poco3 = new ReservatorioLiquidos.cano();
            this.jardimCano1Poco3 = new ReservatorioLiquidos.cano();
            this.jardimPoco2 = new PumpControlLibrary.SubmersivePump();
            this.jardimPoco3 = new PumpControlLibrary.SubmersivePump();
            this.Gutierrez = new System.Windows.Forms.TabPage();
            this.gutierrezVazaoBomba2 = new System.Windows.Forms.Label();
            this.waterMeter12 = new PumpControlLibrary.WaterMeter();
            this.gutierrezVazaoBomba1 = new System.Windows.Forms.Label();
            this.waterMeter11 = new PumpControlLibrary.WaterMeter();
            this.junção130 = new PumpControlLibrary.Junção();
            this.junção73 = new PumpControlLibrary.Junção();
            this.label57 = new System.Windows.Forms.Label();
            this.junção74 = new PumpControlLibrary.Junção();
            this.label58 = new System.Windows.Forms.Label();
            this.junção75 = new PumpControlLibrary.Junção();
            this.junção76 = new PumpControlLibrary.Junção();
            this.junção77 = new PumpControlLibrary.Junção();
            this.gutierrezBomba2 = new PumpControlLibrary.Pump();
            this.gutierrezCano2Bomba2 = new ReservatorioLiquidos.cano();
            this.junção78 = new PumpControlLibrary.Junção();
            this.gutierrezCano3Bomba1 = new ReservatorioLiquidos.cano();
            this.junção79 = new PumpControlLibrary.Junção();
            this.junção80 = new PumpControlLibrary.Junção();
            this.gutierrezBomba1 = new PumpControlLibrary.Pump();
            this.gutierrezCano2Bomba1 = new ReservatorioLiquidos.cano();
            this.gutierrezCanoBomba1 = new ReservatorioLiquidos.cano();
            this.gutierrezCano1Bomba2 = new ReservatorioLiquidos.cano();
            this.junçãoT28 = new PumpControlLibrary.JunçãoT();
            this.gutierrezPoco6 = new PumpControlLibrary.SubmersivePump();
            this.label56 = new System.Windows.Forms.Label();
            this.junção71 = new PumpControlLibrary.Junção();
            this.junçãoT34 = new PumpControlLibrary.JunçãoT();
            this.junçãoT33 = new PumpControlLibrary.JunçãoT();
            this.gutierrezPoco7 = new PumpControlLibrary.SubmersivePump();
            this.gutierrezCanoPoco8 = new ReservatorioLiquidos.cano();
            this.gutierrezCanoPoco6 = new ReservatorioLiquidos.cano();
            this.label55 = new System.Windows.Forms.Label();
            this.junçãoT29 = new PumpControlLibrary.JunçãoT();
            this.junçãoT30 = new PumpControlLibrary.JunçãoT();
            this.junçãoT31 = new PumpControlLibrary.JunçãoT();
            this.junçãoT32 = new PumpControlLibrary.JunçãoT();
            this.gutierrezCanoPoco4 = new ReservatorioLiquidos.cano();
            this.gutierrezPoco1 = new PumpControlLibrary.SubmersivePump();
            this.gutierrezPoco2 = new PumpControlLibrary.SubmersivePump();
            this.gutierrezPoco3 = new PumpControlLibrary.SubmersivePump();
            this.gutierrezCanoPoco1 = new ReservatorioLiquidos.cano();
            this.gutierrezCanoPoco2 = new ReservatorioLiquidos.cano();
            this.gutierrezCanoPoco3 = new ReservatorioLiquidos.cano();
            this.junção72 = new PumpControlLibrary.Junção();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.gutierrezTanque1 = new ReservatorioLiquidos.Tanque();
            this.gutierrezPoco5 = new PumpControlLibrary.SubmersivePump();
            this.gutierrezPoco4 = new PumpControlLibrary.SubmersivePump();
            this.gutierrezCanoPoco5 = new ReservatorioLiquidos.cano();
            this.gutierrezCanoPoco7 = new ReservatorioLiquidos.cano();
            this.gutierrezCanoBomba2 = new ReservatorioLiquidos.cano();
            this.gutierrezCano1Bomba1 = new ReservatorioLiquidos.cano();
            this.gutierrezCano3Bomba2 = new ReservatorioLiquidos.cano();
            this.gutierrezPoco8 = new PumpControlLibrary.SubmersivePump();
            this.Estadual = new System.Windows.Forms.TabPage();
            this.label109 = new System.Windows.Forms.Label();
            this.junção133 = new PumpControlLibrary.Junção();
            this.estadualCano2Chamcia = new ReservatorioLiquidos.cano();
            this.junção132 = new PumpControlLibrary.Junção();
            this.estadualBombaChamcia = new PumpControlLibrary.Pump();
            this.junção131 = new PumpControlLibrary.Junção();
            this.estadualVazaoBomba2 = new System.Windows.Forms.Label();
            this.waterMeter10 = new PumpControlLibrary.WaterMeter();
            this.estadualVazaoBomba1 = new System.Windows.Forms.Label();
            this.waterMeter9 = new PumpControlLibrary.WaterMeter();
            this.junção129 = new PumpControlLibrary.Junção();
            this.junção70 = new PumpControlLibrary.Junção();
            this.label47 = new System.Windows.Forms.Label();
            this.junção63 = new PumpControlLibrary.Junção();
            this.label48 = new System.Windows.Forms.Label();
            this.junção64 = new PumpControlLibrary.Junção();
            this.junção65 = new PumpControlLibrary.Junção();
            this.junção66 = new PumpControlLibrary.Junção();
            this.estadualBomba2 = new PumpControlLibrary.Pump();
            this.estadualCano2Bomba2 = new ReservatorioLiquidos.cano();
            this.junção67 = new PumpControlLibrary.Junção();
            this.estadualCano3Bomba1 = new ReservatorioLiquidos.cano();
            this.junção68 = new PumpControlLibrary.Junção();
            this.junção69 = new PumpControlLibrary.Junção();
            this.estadualBomba1 = new PumpControlLibrary.Pump();
            this.estadualCano2Bomba1 = new ReservatorioLiquidos.cano();
            this.estadualCanoBomba1 = new ReservatorioLiquidos.cano();
            this.estadualCano1Bomba2 = new ReservatorioLiquidos.cano();
            this.junçãoT22 = new PumpControlLibrary.JunçãoT();
            this.junção54 = new PumpControlLibrary.Junção();
            this.estadualTanque1 = new ReservatorioLiquidos.Tanque();
            this.junção62 = new PumpControlLibrary.Junção();
            this.junçãoT23 = new PumpControlLibrary.JunçãoT();
            this.estadualPoco1 = new PumpControlLibrary.SubmersivePump();
            this.estadualCanoPoco2 = new ReservatorioLiquidos.cano();
            this.label40 = new System.Windows.Forms.Label();
            this.junçãoT24 = new PumpControlLibrary.JunçãoT();
            this.junçãoT25 = new PumpControlLibrary.JunçãoT();
            this.junçãoT26 = new PumpControlLibrary.JunçãoT();
            this.junçãoT27 = new PumpControlLibrary.JunçãoT();
            this.estadualCanoPoco6 = new ReservatorioLiquidos.cano();
            this.estadualPoco2 = new PumpControlLibrary.SubmersivePump();
            this.estadualPoco3 = new PumpControlLibrary.SubmersivePump();
            this.estadualPoco4 = new PumpControlLibrary.SubmersivePump();
            this.estadualPoco5 = new PumpControlLibrary.SubmersivePump();
            this.estadualCanoPoco1 = new ReservatorioLiquidos.cano();
            this.estadualCanoPoco3 = new ReservatorioLiquidos.cano();
            this.estadualCanoPoco4 = new ReservatorioLiquidos.cano();
            this.estadualCanoPoco5 = new ReservatorioLiquidos.cano();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.estadualPoco7 = new PumpControlLibrary.SubmersivePump();
            this.estadualPoco6 = new PumpControlLibrary.SubmersivePump();
            this.estadualCanoPoco7 = new ReservatorioLiquidos.cano();
            this.estadualTanque2 = new ReservatorioLiquidos.Tanque();
            this.estadualCanoTanque2 = new ReservatorioLiquidos.cano();
            this.estadualCanoBomba2 = new ReservatorioLiquidos.cano();
            this.estadualCano1Bomba1 = new ReservatorioLiquidos.cano();
            this.estadualCano3Bomba2 = new ReservatorioLiquidos.cano();
            this.estadualCanoChamcia = new ReservatorioLiquidos.cano();
            this.cano3 = new ReservatorioLiquidos.cano();
            this.cano4 = new ReservatorioLiquidos.cano();
            this.Independencia = new System.Windows.Forms.TabPage();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.junçãoT74 = new PumpControlLibrary.JunçãoT();
            this.junçãoT73 = new PumpControlLibrary.JunçãoT();
            this.junçãoT71 = new PumpControlLibrary.JunçãoT();
            this.junçãoT72 = new PumpControlLibrary.JunçãoT();
            this.junção127 = new PumpControlLibrary.Junção();
            this.junção125 = new PumpControlLibrary.Junção();
            this.junção126 = new PumpControlLibrary.Junção();
            this.independenciaBomba2 = new PumpControlLibrary.Pump();
            this.junção123 = new PumpControlLibrary.Junção();
            this.junçãoT70 = new PumpControlLibrary.JunçãoT();
            this.junção122 = new PumpControlLibrary.Junção();
            this.junção121 = new PumpControlLibrary.Junção();
            this.junção119 = new PumpControlLibrary.Junção();
            this.junção120 = new PumpControlLibrary.Junção();
            this.independenciaBomba1 = new PumpControlLibrary.Pump();
            this.independenciaCano1Bomba1 = new ReservatorioLiquidos.cano();
            this.junçãoT63 = new PumpControlLibrary.JunçãoT();
            this.junção118 = new PumpControlLibrary.Junção();
            this.junçãoT52 = new PumpControlLibrary.JunçãoT();
            this.independenciaTanque2 = new ReservatorioLiquidos.Tanque();
            this.independenciaTanque3 = new ReservatorioLiquidos.Tanque();
            this.junção117 = new PumpControlLibrary.Junção();
            this.junção114 = new PumpControlLibrary.Junção();
            this.independenciaTanque1 = new ReservatorioLiquidos.Tanque();
            this.junçãoT69 = new PumpControlLibrary.JunçãoT();
            this.junção116 = new PumpControlLibrary.Junção();
            this.junção115 = new PumpControlLibrary.Junção();
            this.junçãoT68 = new PumpControlLibrary.JunçãoT();
            this.independenciaCanoPocoSolt2 = new ReservatorioLiquidos.cano();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.independenciaPocoSolt1 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPocoSolt2 = new PumpControlLibrary.SubmersivePump();
            this.independenciaCanoPocoSolt1 = new ReservatorioLiquidos.cano();
            this.junçãoT64 = new PumpControlLibrary.JunçãoT();
            this.junçãoT65 = new PumpControlLibrary.JunçãoT();
            this.junçãoT66 = new PumpControlLibrary.JunçãoT();
            this.junçãoT67 = new PumpControlLibrary.JunçãoT();
            this.independenciaCanoPoco9 = new ReservatorioLiquidos.cano();
            this.independenciaPoco12 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPoco11 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPoco10 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPoco9 = new PumpControlLibrary.SubmersivePump();
            this.independenciaCanoPoco11 = new ReservatorioLiquidos.cano();
            this.independenciaCanoPoco10 = new ReservatorioLiquidos.cano();
            this.junção111 = new PumpControlLibrary.Junção();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.independenciaPoco7 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPoco8 = new PumpControlLibrary.SubmersivePump();
            this.independenciaCanoPoco8 = new ReservatorioLiquidos.cano();
            this.junção113 = new PumpControlLibrary.Junção();
            this.junçãoT62 = new PumpControlLibrary.JunçãoT();
            this.junçãoT58 = new PumpControlLibrary.JunçãoT();
            this.junçãoT59 = new PumpControlLibrary.JunçãoT();
            this.junçãoT60 = new PumpControlLibrary.JunçãoT();
            this.junçãoT61 = new PumpControlLibrary.JunçãoT();
            this.independenciaCanoPoco3 = new ReservatorioLiquidos.cano();
            this.independenciaPoco6 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPoco5 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPoco4 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPoco3 = new PumpControlLibrary.SubmersivePump();
            this.independenciaCano2Poco7 = new ReservatorioLiquidos.cano();
            this.independenciaCanoPoco6 = new ReservatorioLiquidos.cano();
            this.independenciaCanoPoco5 = new ReservatorioLiquidos.cano();
            this.independenciaCanoPoco4 = new ReservatorioLiquidos.cano();
            this.junção112 = new PumpControlLibrary.Junção();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.independenciaTanque4 = new ReservatorioLiquidos.Tanque();
            this.independenciaPoco1 = new PumpControlLibrary.SubmersivePump();
            this.independenciaPoco2 = new PumpControlLibrary.SubmersivePump();
            this.independenciaCanoPoco2 = new ReservatorioLiquidos.cano();
            this.independenciaCanoTanque2 = new ReservatorioLiquidos.cano();
            this.independenciaCanoTanque3 = new ReservatorioLiquidos.cano();
            this.independenciaCanoPoco12 = new ReservatorioLiquidos.cano();
            this.independenciaCanoTanque4 = new ReservatorioLiquidos.cano();
            this.independenciaCanoPoco1 = new ReservatorioLiquidos.cano();
            this.independenciaCano1Poco7 = new ReservatorioLiquidos.cano();
            this.independenciaCanoPoco7 = new ReservatorioLiquidos.cano();
            this.independenciaCano3Tanque1 = new ReservatorioLiquidos.cano();
            this.independenciaCano2Tanque1 = new ReservatorioLiquidos.cano();
            this.independenciaCanoTanque1 = new ReservatorioLiquidos.cano();
            this.independenciaCanoBomba2 = new ReservatorioLiquidos.cano();
            this.independenciaCano2Bomba1 = new ReservatorioLiquidos.cano();
            this.independenciaCano3Bomba1 = new ReservatorioLiquidos.cano();
            this.junção124 = new PumpControlLibrary.Junção();
            this.independenciaCanoBomba1 = new ReservatorioLiquidos.cano();
            this.SaoSebastiao = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.sebastiaoVazaoBomba1 = new System.Windows.Forms.Label();
            this.waterMeter4 = new PumpControlLibrary.WaterMeter();
            this.sebastiaoVazaoBomba2 = new System.Windows.Forms.Label();
            this.waterMeter2 = new PumpControlLibrary.WaterMeter();
            this.junção110 = new PumpControlLibrary.Junção();
            this.label86 = new System.Windows.Forms.Label();
            this.junção109 = new PumpControlLibrary.Junção();
            this.junção99 = new PumpControlLibrary.Junção();
            this.junçãoT57 = new PumpControlLibrary.JunçãoT();
            this.junção105 = new PumpControlLibrary.Junção();
            this.junção108 = new PumpControlLibrary.Junção();
            this.junçãoT55 = new PumpControlLibrary.JunçãoT();
            this.junçãoT56 = new PumpControlLibrary.JunçãoT();
            this.sebastiaoCanoPoco6 = new ReservatorioLiquidos.cano();
            this.sebastiaoPoco5 = new PumpControlLibrary.SubmersivePump();
            this.sebastiaoCanoPoco5 = new ReservatorioLiquidos.cano();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.sebastiaoPoco7 = new PumpControlLibrary.SubmersivePump();
            this.sebastiaoPoco6 = new PumpControlLibrary.SubmersivePump();
            this.sebastiaoCanoPoco7 = new ReservatorioLiquidos.cano();
            this.label85 = new System.Windows.Forms.Label();
            this.junção101 = new PumpControlLibrary.Junção();
            this.junção102 = new PumpControlLibrary.Junção();
            this.junção103 = new PumpControlLibrary.Junção();
            this.junção104 = new PumpControlLibrary.Junção();
            this.sebastiaoBomba1 = new PumpControlLibrary.Pump();
            this.sebastiaoCanoPoco4 = new ReservatorioLiquidos.cano();
            this.junção106 = new PumpControlLibrary.Junção();
            this.junção107 = new PumpControlLibrary.Junção();
            this.sebastiaoBomba2 = new PumpControlLibrary.Pump();
            this.junção100 = new PumpControlLibrary.Junção();
            this.junçãoT53 = new PumpControlLibrary.JunçãoT();
            this.junçãoT54 = new PumpControlLibrary.JunçãoT();
            this.sebastiaoCanoPoco2 = new ReservatorioLiquidos.cano();
            this.sebastiaoPoco4 = new PumpControlLibrary.SubmersivePump();
            this.sebastiaoPoco1 = new PumpControlLibrary.SubmersivePump();
            this.sebastiaoCanoPoco1 = new ReservatorioLiquidos.cano();
            this.label80 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.sebastiaoPoco3 = new PumpControlLibrary.SubmersivePump();
            this.sebastiaoPoco2 = new PumpControlLibrary.SubmersivePump();
            this.sebastiaoCanoPoco3 = new ReservatorioLiquidos.cano();
            this.label81 = new System.Windows.Forms.Label();
            this.sebastiaoTanque1 = new ReservatorioLiquidos.Tanque();
            this.sebastiaoTanque2 = new ReservatorioLiquidos.Tanque();
            this.sebastiaoCanoTanque2 = new ReservatorioLiquidos.cano();
            this.sebastiaoCanoBomba2 = new ReservatorioLiquidos.cano();
            this.sebastiaoCanoBomba1 = new ReservatorioLiquidos.cano();
            this.sebastiaoCano2Poco5 = new ReservatorioLiquidos.cano();
            this.sebastiaoCano2Poco1 = new ReservatorioLiquidos.cano();
            this.sebastiaoCano3Poco5 = new ReservatorioLiquidos.cano();
            this.sebastiaoCano1Bomba2 = new ReservatorioLiquidos.cano();
            this.sebastiaoCano1Bomba1 = new ReservatorioLiquidos.cano();
            this.OuroVerde = new System.Windows.Forms.TabPage();
            this.junçãoT5 = new PumpControlLibrary.JunçãoT();
            this.junçãoT4 = new PumpControlLibrary.JunçãoT();
            this.junçãoT3 = new PumpControlLibrary.JunçãoT();
            this.junçãoT2 = new PumpControlLibrary.JunçãoT();
            this.junçãoT1 = new PumpControlLibrary.JunçãoT();
            this.ouroCanoPoco5 = new ReservatorioLiquidos.cano();
            this.ouroPoco1 = new PumpControlLibrary.SubmersivePump();
            this.ouroPoco2 = new PumpControlLibrary.SubmersivePump();
            this.ouroPoco3 = new PumpControlLibrary.SubmersivePump();
            this.ouroPoco4 = new PumpControlLibrary.SubmersivePump();
            this.junção20 = new PumpControlLibrary.Junção();
            this.label15 = new System.Windows.Forms.Label();
            this.junção19 = new PumpControlLibrary.Junção();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.junção7 = new PumpControlLibrary.Junção();
            this.ouroCanoPoco1 = new ReservatorioLiquidos.cano();
            this.ouroCanoPoco2 = new ReservatorioLiquidos.cano();
            this.ouroCanoPoco3 = new ReservatorioLiquidos.cano();
            this.ouroCanoPoco4 = new ReservatorioLiquidos.cano();
            this.junção6 = new PumpControlLibrary.Junção();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ouroTanque3 = new ReservatorioLiquidos.Tanque();
            this.ouroTanque2 = new ReservatorioLiquidos.Tanque();
            this.ouroTanque1 = new ReservatorioLiquidos.Tanque();
            this.ouroCanoTanque2 = new ReservatorioLiquidos.cano();
            this.junção17 = new PumpControlLibrary.Junção();
            this.ouroCano2CidadeNova = new ReservatorioLiquidos.cano();
            this.ouroCanoPoco7 = new ReservatorioLiquidos.cano();
            this.junção4 = new PumpControlLibrary.Junção();
            this.junção18 = new PumpControlLibrary.Junção();
            this.ouroCanoIpe = new ReservatorioLiquidos.cano();
            this.ouroCanoOutTanque3 = new ReservatorioLiquidos.cano();
            this.ouroPoco6 = new PumpControlLibrary.SubmersivePump();
            this.ouroPoco5 = new PumpControlLibrary.SubmersivePump();
            this.ouroCanoPoco6 = new ReservatorioLiquidos.cano();
            this.ouroPoco7 = new PumpControlLibrary.SubmersivePump();
            this.junçãoT17 = new PumpControlLibrary.JunçãoT();
            this.ouroCanoGramVille = new ReservatorioLiquidos.cano();
            this.ouroCano1CidadeNova = new ReservatorioLiquidos.cano();
            this.SaoBenedito = new System.Windows.Forms.TabPage();
            this.juncao1Benedito = new PumpControlLibrary.JunçãoT();
            this.cano1 = new ReservatorioLiquidos.cano();
            this.beneditoVazaoBomba3 = new System.Windows.Forms.Label();
            this.waterMeter15 = new PumpControlLibrary.WaterMeter();
            this.junção128 = new PumpControlLibrary.Junção();
            this.beneditoVazaoBomba2 = new System.Windows.Forms.Label();
            this.waterMeter8 = new PumpControlLibrary.WaterMeter();
            this.beneditoVazaoBomba1 = new System.Windows.Forms.Label();
            this.waterMeter3 = new PumpControlLibrary.WaterMeter();
            this.junção46 = new PumpControlLibrary.Junção();
            this.label39 = new System.Windows.Forms.Label();
            this.junção53 = new PumpControlLibrary.Junção();
            this.beneditoPoco1 = new PumpControlLibrary.SubmersivePump();
            this.label31 = new System.Windows.Forms.Label();
            this.junção51 = new PumpControlLibrary.Junção();
            this.junção52 = new PumpControlLibrary.Junção();
            this.beneditoBombaChamcia = new PumpControlLibrary.Pump();
            this.beneditoCano2Chamcia = new ReservatorioLiquidos.cano();
            this.label38 = new System.Windows.Forms.Label();
            this.juncao2Benedito = new PumpControlLibrary.JunçãoT();
            this.junção16 = new PumpControlLibrary.Junção();
            this.junção47 = new PumpControlLibrary.Junção();
            this.label36 = new System.Windows.Forms.Label();
            this.junção48 = new PumpControlLibrary.Junção();
            this.junção49 = new PumpControlLibrary.Junção();
            this.junção50 = new PumpControlLibrary.Junção();
            this.beneditoBomba3 = new PumpControlLibrary.Pump();
            this.beneditoCano2Bomba3 = new ReservatorioLiquidos.cano();
            this.junção55 = new PumpControlLibrary.Junção();
            this.beneditoCano3Bomba2 = new ReservatorioLiquidos.cano();
            this.junção56 = new PumpControlLibrary.Junção();
            this.junção57 = new PumpControlLibrary.Junção();
            this.beneditoBomba2 = new PumpControlLibrary.Pump();
            this.beneditoCano2Bomba2 = new ReservatorioLiquidos.cano();
            this.junção58 = new PumpControlLibrary.Junção();
            this.junção59 = new PumpControlLibrary.Junção();
            this.junção60 = new PumpControlLibrary.Junção();
            this.junção61 = new PumpControlLibrary.Junção();
            this.label37 = new System.Windows.Forms.Label();
            this.beneditoBomba1 = new PumpControlLibrary.Pump();
            this.beneditoCano21Bomba1 = new ReservatorioLiquidos.cano();
            this.beneditoCanoBomba1 = new ReservatorioLiquidos.cano();
            this.beneditoCano2Bomba1 = new ReservatorioLiquidos.cano();
            this.beneditoCano3Bomba1 = new ReservatorioLiquidos.cano();
            this.beneditoCanoBomba2 = new ReservatorioLiquidos.cano();
            this.beneditoCano1Bomba3 = new ReservatorioLiquidos.cano();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.junção41 = new PumpControlLibrary.Junção();
            this.junçãoT18 = new PumpControlLibrary.JunçãoT();
            this.junção42 = new PumpControlLibrary.Junção();
            this.beneditoPocoSolt1 = new PumpControlLibrary.SubmersivePump();
            this.beneditoPocoSolt2 = new PumpControlLibrary.SubmersivePump();
            this.junção43 = new PumpControlLibrary.Junção();
            this.junção44 = new PumpControlLibrary.Junção();
            this.junção45 = new PumpControlLibrary.Junção();
            this.beneditoCano3PocoSolt1 = new ReservatorioLiquidos.cano();
            this.beneditoTanque1 = new ReservatorioLiquidos.Tanque();
            this.beneditoCano1PocoSolt1 = new ReservatorioLiquidos.cano();
            this.junçãoT19 = new PumpControlLibrary.JunçãoT();
            this.beneditoCano1BelaVista2 = new ReservatorioLiquidos.cano();
            this.beneditoCano2BelaVista2 = new ReservatorioLiquidos.cano();
            this.beneditoCanoBelaVista1 = new ReservatorioLiquidos.cano();
            this.beneditoCanoPocoSolt2 = new ReservatorioLiquidos.cano();
            this.beneditoCanoPoco1 = new ReservatorioLiquidos.cano();
            this.beneditoTanque2 = new ReservatorioLiquidos.Tanque();
            this.beneditoCanoBomba3 = new ReservatorioLiquidos.cano();
            this.beneditoCanoChamcia = new ReservatorioLiquidos.cano();
            this.beneditoCano1Bomba2 = new ReservatorioLiquidos.cano();
            this.beneditoCano1Bomba1 = new ReservatorioLiquidos.cano();
            this.beneditoCano2PocoSolt1 = new ReservatorioLiquidos.cano();
            this.beneditoCano3Bomba3 = new ReservatorioLiquidos.cano();
            this.cano2 = new ReservatorioLiquidos.cano();
            this.Chamcia = new System.Windows.Forms.TabPage();
            this.chamciaVazaoBomba4 = new System.Windows.Forms.Label();
            this.waterMeter7 = new PumpControlLibrary.WaterMeter();
            this.chamciaVazaoBomba3 = new System.Windows.Forms.Label();
            this.waterMeter6 = new PumpControlLibrary.WaterMeter();
            this.chamciaVazaoBomba2 = new System.Windows.Forms.Label();
            this.waterMeter5 = new PumpControlLibrary.WaterMeter();
            this.chamciaVazaoBomba1 = new System.Windows.Forms.Label();
            this.waterMeter1 = new PumpControlLibrary.WaterMeter();
            this.junçãoT16 = new PumpControlLibrary.JunçãoT();
            this.junção25 = new PumpControlLibrary.Junção();
            this.junção10 = new PumpControlLibrary.Junção();
            this.label30 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.junção33 = new PumpControlLibrary.Junção();
            this.junção38 = new PumpControlLibrary.Junção();
            this.junçãoT8 = new PumpControlLibrary.JunçãoT();
            this.junção39 = new PumpControlLibrary.Junção();
            this.junção40 = new PumpControlLibrary.Junção();
            this.chamciaBomba4 = new PumpControlLibrary.Pump();
            this.chamciaCano2Bomba4 = new ReservatorioLiquidos.cano();
            this.junção37 = new PumpControlLibrary.Junção();
            this.junção34 = new PumpControlLibrary.Junção();
            this.junção35 = new PumpControlLibrary.Junção();
            this.junção36 = new PumpControlLibrary.Junção();
            this.chamciaBomba3 = new PumpControlLibrary.Pump();
            this.chamciaCano2Bomba3 = new ReservatorioLiquidos.cano();
            this.junção26 = new PumpControlLibrary.Junção();
            this.junção14 = new PumpControlLibrary.Junção();
            this.chamciaCano3Bomba2 = new ReservatorioLiquidos.cano();
            this.junção11 = new PumpControlLibrary.Junção();
            this.junção12 = new PumpControlLibrary.Junção();
            this.chamciaBomba2 = new PumpControlLibrary.Pump();
            this.chamciaCano2Bomba2 = new ReservatorioLiquidos.cano();
            this.junção5 = new PumpControlLibrary.Junção();
            this.chamciaTanque1 = new ReservatorioLiquidos.Tanque();
            this.chamciaCanoBomba21 = new ReservatorioLiquidos.cano();
            this.junção30 = new PumpControlLibrary.Junção();
            this.junção15 = new PumpControlLibrary.Junção();
            this.junção13 = new PumpControlLibrary.Junção();
            this.junção9 = new PumpControlLibrary.Junção();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.junção8 = new PumpControlLibrary.Junção();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.junçãoT6 = new PumpControlLibrary.JunçãoT();
            this.junção21 = new PumpControlLibrary.Junção();
            this.chamciaPocoSolt1 = new PumpControlLibrary.SubmersivePump();
            this.chamciaPocoSolt2 = new PumpControlLibrary.SubmersivePump();
            this.chamciaBomba1 = new PumpControlLibrary.Pump();
            this.junção3 = new PumpControlLibrary.Junção();
            this.junçãoT15 = new PumpControlLibrary.JunçãoT();
            this.chamciaPoco1 = new PumpControlLibrary.SubmersivePump();
            this.chamciaCanoPoco2 = new ReservatorioLiquidos.cano();
            this.label25 = new System.Windows.Forms.Label();
            this.junçãoT11 = new PumpControlLibrary.JunçãoT();
            this.junçãoT12 = new PumpControlLibrary.JunçãoT();
            this.junçãoT13 = new PumpControlLibrary.JunçãoT();
            this.junçãoT14 = new PumpControlLibrary.JunçãoT();
            this.chamciaCanoPoco6 = new ReservatorioLiquidos.cano();
            this.chamciaPoco2 = new PumpControlLibrary.SubmersivePump();
            this.chamciaPoco3 = new PumpControlLibrary.SubmersivePump();
            this.chamciaPoco4 = new PumpControlLibrary.SubmersivePump();
            this.chamciaPoco5 = new PumpControlLibrary.SubmersivePump();
            this.chamciaCanoPoco1 = new ReservatorioLiquidos.cano();
            this.chamciaCanoPoco3 = new ReservatorioLiquidos.cano();
            this.chamciaCanoPoco4 = new ReservatorioLiquidos.cano();
            this.chamciaCanoPoco5 = new ReservatorioLiquidos.cano();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.chamciaPoco7 = new PumpControlLibrary.SubmersivePump();
            this.chamciaPoco6 = new PumpControlLibrary.SubmersivePump();
            this.chamciaCanoPoco7 = new ReservatorioLiquidos.cano();
            this.junção22 = new PumpControlLibrary.Junção();
            this.junção27 = new PumpControlLibrary.Junção();
            this.junção24 = new PumpControlLibrary.Junção();
            this.junção23 = new PumpControlLibrary.Junção();
            this.chamciaCano3PocoSolt1 = new ReservatorioLiquidos.cano();
            this.label18 = new System.Windows.Forms.Label();
            this.chamciaTanque4 = new ReservatorioLiquidos.Tanque();
            this.chamciaTanque3 = new ReservatorioLiquidos.Tanque();
            this.chamciaTanque2 = new ReservatorioLiquidos.Tanque();
            this.chamciaCano1PocoSolt1 = new ReservatorioLiquidos.cano();
            this.chamciaCano2PocoSolt1 = new ReservatorioLiquidos.cano();
            this.junçãoT7 = new PumpControlLibrary.JunçãoT();
            this.chamciaCano1Suica3 = new ReservatorioLiquidos.cano();
            this.chamciaCano2Suica3 = new ReservatorioLiquidos.cano();
            this.chamciaCanoSuica2 = new ReservatorioLiquidos.cano();
            this.chamciaCanoPocoSolt2 = new ReservatorioLiquidos.cano();
            this.chamciaCanoBomba1 = new ReservatorioLiquidos.cano();
            this.chamciaCano12Bomba1 = new ReservatorioLiquidos.cano();
            this.chamciaCano2Bomba1 = new ReservatorioLiquidos.cano();
            this.chamciaCanoBomba2 = new ReservatorioLiquidos.cano();
            this.chamciaCano3Bomba3 = new ReservatorioLiquidos.cano();
            this.chamciaCanoBomba3 = new ReservatorioLiquidos.cano();
            this.chamciaCanoBomba4 = new ReservatorioLiquidos.cano();
            this.chamciaCano1Bomba4 = new ReservatorioLiquidos.cano();
            this.chamciaCanoTanque2 = new ReservatorioLiquidos.cano();
            this.chamciaCanoTanque3 = new ReservatorioLiquidos.cano();
            this.chamciaCano1Bomba3 = new ReservatorioLiquidos.cano();
            this.chamciaCano3Bomba1 = new ReservatorioLiquidos.cano();
            this.chamciaCano1Bomba2 = new ReservatorioLiquidos.cano();
            this.chamciaCano1Bomba1 = new ReservatorioLiquidos.cano();
            this.chamciaCano3Bomba4 = new ReservatorioLiquidos.cano();
            this.Acionamentos = new System.Windows.Forms.TabPage();
            this.pbAcionamentos = new System.Windows.Forms.PictureBox();
            this.Reservatórios = new System.Windows.Forms.TabPage();
            this.panelNivel = new System.Windows.Forms.Panel();
            this.RouroVerdeN1 = new ReservatorioLiquidos.TanqueEscala();
            this.RIndependenciaN1 = new ReservatorioLiquidos.TanqueEscala();
            this.RIndependenciaN2 = new ReservatorioLiquidos.TanqueEscala();
            this.REstadualN1 = new ReservatorioLiquidos.TanqueEscala();
            this.REstadualN2 = new ReservatorioLiquidos.TanqueEscala();
            this.RGuitierrezN1 = new ReservatorioLiquidos.TanqueEscala();
            this.RJBN1 = new ReservatorioLiquidos.TanqueEscala();
            this.RFatimaN1 = new ReservatorioLiquidos.TanqueEscala();
            this.RSSebastiaoN1 = new ReservatorioLiquidos.TanqueEscala();
            this.RouroVN2 = new ReservatorioLiquidos.TanqueEscala();
            this.RsBeneditoN1 = new ReservatorioLiquidos.TanqueEscala();
            this.RsBeneditoN2 = new ReservatorioLiquidos.TanqueEscala();
            this.RChanciaN2 = new ReservatorioLiquidos.TanqueEscala();
            this.RChanciaNivel1 = new ReservatorioLiquidos.TanqueEscala();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.lblIndepN2 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.lblSsebastiao = new System.Windows.Forms.Label();
            this.lblOuroVN2 = new System.Windows.Forms.Label();
            this.lblOuroVN1 = new System.Windows.Forms.Label();
            this.lblSBN2 = new System.Windows.Forms.Label();
            this.lblSBeneditoN1 = new System.Windows.Forms.Label();
            this.lblCN2 = new System.Windows.Forms.Label();
            this.lblChamciaNivel1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.RelacaoDosPoçosSolteiros = new System.Windows.Forms.TabPage();
            this.pbRelacaoPocoSolteiro = new System.Windows.Forms.PictureBox();
            this.PocoSolteiro = new System.Windows.Forms.TabPage();
            this.pbPocoSolteiro = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.Fatima.SuspendLayout();
            this.JardimBotanico.SuspendLayout();
            this.Gutierrez.SuspendLayout();
            this.Estadual.SuspendLayout();
            this.Independencia.SuspendLayout();
            this.SaoSebastiao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.OuroVerde.SuspendLayout();
            this.SaoBenedito.SuspendLayout();
            this.Chamcia.SuspendLayout();
            this.Acionamentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAcionamentos)).BeginInit();
            this.Reservatórios.SuspendLayout();
            this.panelNivel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.RelacaoDosPoçosSolteiros.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelacaoPocoSolteiro)).BeginInit();
            this.PocoSolteiro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPocoSolteiro)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroToolStripMenuItem,
            this.consultaToolStripMenuItem,
            this.relatórioToolStripMenuItem,
            this.importaçãoToolStripMenuItem,
            this.sincronizaçãoToolStripMenuItem,
            this.ajudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1488, 26);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroDeAçõesToolStripMenuItem,
            this.dadosDoPoçoToolStripMenuItem,
            this.cadastroDeClienteToolStripMenuItem,
            this.pontoToolStripMenuItem,
            this.preventivaToolStripMenuItem,
            this.AutomaticoToolStripMenuItem,
            this.configuraçãoSetPointToolStripMenuItem,
            this.configuraçãoDeAlarmeToolStripMenuItem});
            this.cadastroToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_action_window_new1;
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            // 
            // cadastroDeAçõesToolStripMenuItem
            // 
            this.cadastroDeAçõesToolStripMenuItem.Enabled = false;
            this.cadastroDeAçõesToolStripMenuItem.Image = global::SATS.Properties.Resources.rsz_crystal_clear_app_ark;
            this.cadastroDeAçõesToolStripMenuItem.Name = "cadastroDeAçõesToolStripMenuItem";
            this.cadastroDeAçõesToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.cadastroDeAçõesToolStripMenuItem.Text = "Ações";
            this.cadastroDeAçõesToolStripMenuItem.Visible = false;
            this.cadastroDeAçõesToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeAçõesToolStripMenuItem_Click);
            // 
            // dadosDoPoçoToolStripMenuItem
            // 
            this.dadosDoPoçoToolStripMenuItem.Image = global::SATS.Properties.Resources.crystal_clear_action_playlist__2_;
            this.dadosDoPoçoToolStripMenuItem.Name = "dadosDoPoçoToolStripMenuItem";
            this.dadosDoPoçoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.dadosDoPoçoToolStripMenuItem.Text = "Dados do Poço";
            this.dadosDoPoçoToolStripMenuItem.Click += new System.EventHandler(this.dadosDoPoçoToolStripMenuItem_Click);
            // 
            // cadastroDeClienteToolStripMenuItem
            // 
            this.cadastroDeClienteToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_app_Login_Manager;
            this.cadastroDeClienteToolStripMenuItem.Name = "cadastroDeClienteToolStripMenuItem";
            this.cadastroDeClienteToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.cadastroDeClienteToolStripMenuItem.Text = "Usuário";
            this.cadastroDeClienteToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeClienteToolStripMenuItem_Click);
            // 
            // pontoToolStripMenuItem
            // 
            this.pontoToolStripMenuItem.Image = global::SATS.Properties.Resources.led;
            this.pontoToolStripMenuItem.Name = "pontoToolStripMenuItem";
            this.pontoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.pontoToolStripMenuItem.Text = "Ponto";
            this.pontoToolStripMenuItem.Click += new System.EventHandler(this.pontoToolStripMenuItem_Click);
            // 
            // preventivaToolStripMenuItem
            // 
            this.preventivaToolStripMenuItem.Image = global::SATS.Properties.Resources.ImageResizer_net___cvjcz39co6vnjt6;
            this.preventivaToolStripMenuItem.Name = "preventivaToolStripMenuItem";
            this.preventivaToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.preventivaToolStripMenuItem.Text = "Preventiva";
            this.preventivaToolStripMenuItem.Click += new System.EventHandler(this.preventivaToolStripMenuItem_Click);
            // 
            // AutomaticoToolStripMenuItem
            // 
            this.AutomaticoToolStripMenuItem.Image = global::SATS.Properties.Resources.crystal_clear_app_shutdown;
            this.AutomaticoToolStripMenuItem.Name = "AutomaticoToolStripMenuItem";
            this.AutomaticoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.AutomaticoToolStripMenuItem.Text = "Acionamento Automático";
            this.AutomaticoToolStripMenuItem.Click += new System.EventHandler(this.acionamentoAutomáticoToolStripMenuItem_Click);
            // 
            // configuraçãoSetPointToolStripMenuItem
            // 
            this.configuraçãoSetPointToolStripMenuItem.Name = "configuraçãoSetPointToolStripMenuItem";
            this.configuraçãoSetPointToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.configuraçãoSetPointToolStripMenuItem.Text = "Configuração SetPoint";
            this.configuraçãoSetPointToolStripMenuItem.Click += new System.EventHandler(this.configuraçãoSetPointToolStripMenuItem_Click);
            // 
            // configuraçãoDeAlarmeToolStripMenuItem
            // 
            this.configuraçãoDeAlarmeToolStripMenuItem.Name = "configuraçãoDeAlarmeToolStripMenuItem";
            this.configuraçãoDeAlarmeToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.configuraçãoDeAlarmeToolStripMenuItem.Text = "Configuração Nível Crítico";
            this.configuraçãoDeAlarmeToolStripMenuItem.Click += new System.EventHandler(this.configuraçãoDeAlarmeToolStripMenuItem_Click);
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alarmesToolStripMenuItem,
            this.dadosToolStripMenuItem,
            this.eventosToolStripMenuItem,
            this.pressãoEFrequênciaToolStripMenuItem});
            this.consultaToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_app_kfilereplace;
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(95, 22);
            this.consultaToolStripMenuItem.Text = "Consulta";
            // 
            // alarmesToolStripMenuItem
            // 
            this.alarmesToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_action_find__1_;
            this.alarmesToolStripMenuItem.Name = "alarmesToolStripMenuItem";
            this.alarmesToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.alarmesToolStripMenuItem.Text = "Alarmes";
            this.alarmesToolStripMenuItem.Click += new System.EventHandler(this.alarmesToolStripMenuItem_Click);
            // 
            // dadosToolStripMenuItem
            // 
            this.dadosToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_app_help_index__1_;
            this.dadosToolStripMenuItem.Name = "dadosToolStripMenuItem";
            this.dadosToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.dadosToolStripMenuItem.Text = "Dados";
            this.dadosToolStripMenuItem.Click += new System.EventHandler(this.dadosToolStripMenuItem_Click);
            // 
            // eventosToolStripMenuItem
            // 
            this.eventosToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_app_kappfinder__1_;
            this.eventosToolStripMenuItem.Name = "eventosToolStripMenuItem";
            this.eventosToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.eventosToolStripMenuItem.Text = "Eventos";
            this.eventosToolStripMenuItem.Click += new System.EventHandler(this.eventosToolStripMenuItem_Click);
            // 
            // pressãoEFrequênciaToolStripMenuItem
            // 
            this.pressãoEFrequênciaToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_app_no3D__1_;
            this.pressãoEFrequênciaToolStripMenuItem.Name = "pressãoEFrequênciaToolStripMenuItem";
            this.pressãoEFrequênciaToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.pressãoEFrequênciaToolStripMenuItem.Text = "Frequência x Pressão";
            this.pressãoEFrequênciaToolStripMenuItem.Click += new System.EventHandler(this.pressãoEFrequênciaToolStripMenuItem_Click);
            // 
            // relatórioToolStripMenuItem
            // 
            this.relatórioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bombaToolStripMenuItem,
            this.nívelToolStripMenuItem,
            this.pressãoToolStripMenuItem,
            this.inversorToolStripMenuItem,
            this.consumoToolStripMenuItem,
            this.energiaToolStripMenuItem,
            this.alarmesToolStripMenuItem1,
            this.gastosToolStripMenuItem,
            this.chaveSeletoraToolStripMenuItem,
            this.pSolteiroToolStripMenuItem,
            this.vazaoToolStripMenuItem});
            this.relatórioToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_app_kexi;
            this.relatórioToolStripMenuItem.Name = "relatórioToolStripMenuItem";
            this.relatórioToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.relatórioToolStripMenuItem.Text = "Relatório";
            // 
            // bombaToolStripMenuItem
            // 
            this.bombaToolStripMenuItem.Image = global::SATS.Properties.Resources.centrifugal_pump__1_;
            this.bombaToolStripMenuItem.Name = "bombaToolStripMenuItem";
            this.bombaToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.bombaToolStripMenuItem.Text = "Bomba";
            this.bombaToolStripMenuItem.Click += new System.EventHandler(this.bombaToolStripMenuItem_Click);
            // 
            // nívelToolStripMenuItem
            // 
            this.nívelToolStripMenuItem.Image = global::SATS.Properties.Resources.rsz_crystal_clear_app_kodo;
            this.nívelToolStripMenuItem.Name = "nívelToolStripMenuItem";
            this.nívelToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.nívelToolStripMenuItem.Text = "Nível";
            this.nívelToolStripMenuItem.Click += new System.EventHandler(this.nívelToolStripMenuItem_Click);
            // 
            // pressãoToolStripMenuItem
            // 
            this.pressãoToolStripMenuItem.Name = "pressãoToolStripMenuItem";
            this.pressãoToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.pressãoToolStripMenuItem.Text = "Pressão";
            this.pressãoToolStripMenuItem.Click += new System.EventHandler(this.pressãoToolStripMenuItem_Click);
            // 
            // inversorToolStripMenuItem
            // 
            this.inversorToolStripMenuItem.Name = "inversorToolStripMenuItem";
            this.inversorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.inversorToolStripMenuItem.Text = "Frequência";
            this.inversorToolStripMenuItem.Click += new System.EventHandler(this.inversorToolStripMenuItem_Click);
            // 
            // consumoToolStripMenuItem
            // 
            this.consumoToolStripMenuItem.Name = "consumoToolStripMenuItem";
            this.consumoToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.consumoToolStripMenuItem.Text = "Consumo";
            this.consumoToolStripMenuItem.Click += new System.EventHandler(this.consumoToolStripMenuItem_Click);
            // 
            // energiaToolStripMenuItem
            // 
            this.energiaToolStripMenuItem.Name = "energiaToolStripMenuItem";
            this.energiaToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.energiaToolStripMenuItem.Text = "Energia";
            this.energiaToolStripMenuItem.Click += new System.EventHandler(this.energiaToolStripMenuItem_Click);
            // 
            // alarmesToolStripMenuItem1
            // 
            this.alarmesToolStripMenuItem1.Image = global::SATS.Properties.Resources.ImageResizer_net___cvjcz39co6vnjt6;
            this.alarmesToolStripMenuItem1.Name = "alarmesToolStripMenuItem1";
            this.alarmesToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.alarmesToolStripMenuItem1.Text = "Alarmes";
            this.alarmesToolStripMenuItem1.Visible = false;
            this.alarmesToolStripMenuItem1.Click += new System.EventHandler(this.alarmesToolStripMenuItem1_Click);
            // 
            // gastosToolStripMenuItem
            // 
            this.gastosToolStripMenuItem.Image = global::SATS.Properties.Resources.Money_icon__1_;
            this.gastosToolStripMenuItem.Name = "gastosToolStripMenuItem";
            this.gastosToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.gastosToolStripMenuItem.Text = "Gastos";
            this.gastosToolStripMenuItem.Visible = false;
            this.gastosToolStripMenuItem.Click += new System.EventHandler(this.relatórioDeGastosToolStripMenuItem_Click_1);
            // 
            // chaveSeletoraToolStripMenuItem
            // 
            this.chaveSeletoraToolStripMenuItem.Name = "chaveSeletoraToolStripMenuItem";
            this.chaveSeletoraToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.chaveSeletoraToolStripMenuItem.Text = "Chave Seletora";
            this.chaveSeletoraToolStripMenuItem.Visible = false;
            this.chaveSeletoraToolStripMenuItem.Click += new System.EventHandler(this.chaveSeletoraToolStripMenuItem_Click);
            // 
            // pSolteiroToolStripMenuItem
            // 
            this.pSolteiroToolStripMenuItem.Image = global::SATS.Properties.Resources.centrifugal_pump__1_;
            this.pSolteiroToolStripMenuItem.Name = "pSolteiroToolStripMenuItem";
            this.pSolteiroToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.pSolteiroToolStripMenuItem.Text = "Poço Solteiro";
            this.pSolteiroToolStripMenuItem.Click += new System.EventHandler(this.PSolteiroToolStripMenuItem_Click);
            // 
            // vazaoToolStripMenuItem
            // 
            this.vazaoToolStripMenuItem.Image = global::SATS.Properties.Resources.vazao_120x120;
            this.vazaoToolStripMenuItem.Name = "vazaoToolStripMenuItem";
            this.vazaoToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.vazaoToolStripMenuItem.Text = "Vazao";
            this.vazaoToolStripMenuItem.Click += new System.EventHandler(this.VazaoToolStripMenuItem_Click);
            // 
            // importaçãoToolStripMenuItem
            // 
            this.importaçãoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importarGastosToolStripMenuItem,
            this.importarMicromediçãoToolStripMenuItem});
            this.importaçãoToolStripMenuItem.Image = global::SATS.Properties.Resources.rsz_crystal_clear_app_vcalendar;
            this.importaçãoToolStripMenuItem.Name = "importaçãoToolStripMenuItem";
            this.importaçãoToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.importaçãoToolStripMenuItem.Text = "Importação";
            // 
            // importarGastosToolStripMenuItem
            // 
            this.importarGastosToolStripMenuItem.Image = global::SATS.Properties.Resources.rsz_money_icon_1;
            this.importarGastosToolStripMenuItem.Name = "importarGastosToolStripMenuItem";
            this.importarGastosToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.importarGastosToolStripMenuItem.Text = "Importar Gastos";
            this.importarGastosToolStripMenuItem.Click += new System.EventHandler(this.importarGastosToolStripMenuItem_Click);
            // 
            // importarMicromediçãoToolStripMenuItem
            // 
            this.importarMicromediçãoToolStripMenuItem.Enabled = false;
            this.importarMicromediçãoToolStripMenuItem.Image = global::SATS.Properties.Resources.element_water;
            this.importarMicromediçãoToolStripMenuItem.Name = "importarMicromediçãoToolStripMenuItem";
            this.importarMicromediçãoToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.importarMicromediçãoToolStripMenuItem.Text = "Importar Micromedição";
            this.importarMicromediçãoToolStripMenuItem.Visible = false;
            this.importarMicromediçãoToolStripMenuItem.Click += new System.EventHandler(this.importarMicromediçãoToolStripMenuItem_Click);
            // 
            // sincronizaçãoToolStripMenuItem
            // 
            this.sincronizaçãoToolStripMenuItem.Name = "sincronizaçãoToolStripMenuItem";
            this.sincronizaçãoToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.sincronizaçãoToolStripMenuItem.Text = "Sincronização";
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sobreToolStripMenuItem,
            this.ativarEdiçãoToolStripMenuItem});
            this.ajudaToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_action_configure;
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(72, 22);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Image = global::SATS.Properties.Resources.Crystal_Clear_action_info;
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.sobreToolStripMenuItem.Text = "Sobre";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // ativarEdiçãoToolStripMenuItem
            // 
            this.ativarEdiçãoToolStripMenuItem.Image = global::SATS.Properties.Resources.ImageResizer_net___jbdgwk4ropct56d;
            this.ativarEdiçãoToolStripMenuItem.Name = "ativarEdiçãoToolStripMenuItem";
            this.ativarEdiçãoToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.ativarEdiçãoToolStripMenuItem.Text = "Ativar Edição de Ponto";
            this.ativarEdiçãoToolStripMenuItem.Click += new System.EventHandler(this.ativarEdiçãoToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLastUpdate,
            this.toolStripAlarmeEvento,
            this.toolStripUser,
            this.toolStripVersion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 714);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1488, 30);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripLastUpdate
            // 
            this.toolStripLastUpdate.AutoSize = false;
            this.toolStripLastUpdate.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripLastUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLastUpdate.Name = "toolStripLastUpdate";
            this.toolStripLastUpdate.Size = new System.Drawing.Size(200, 25);
            this.toolStripLastUpdate.Text = "Última Atualização: ";
            this.toolStripLastUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripAlarmeEvento
            // 
            this.toolStripAlarmeEvento.AutoSize = false;
            this.toolStripAlarmeEvento.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripAlarmeEvento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripAlarmeEvento.ForeColor = System.Drawing.Color.DarkRed;
            this.toolStripAlarmeEvento.Name = "toolStripAlarmeEvento";
            this.toolStripAlarmeEvento.Size = new System.Drawing.Size(1094, 25);
            this.toolStripAlarmeEvento.Spring = true;
            this.toolStripAlarmeEvento.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripUser
            // 
            this.toolStripUser.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.toolStripUser.Name = "toolStripUser";
            this.toolStripUser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripUser.Size = new System.Drawing.Size(72, 25);
            this.toolStripUser.Text = "Usuário: ";
            this.toolStripUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripVersion
            // 
            this.toolStripVersion.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripVersion.Name = "toolStripVersion";
            this.toolStripVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripVersion.Size = new System.Drawing.Size(107, 25);
            this.toolStripVersion.Text = "Versão: 1.0.01";
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerColorTint = System.Drawing.Color.CornflowerBlue;
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            // 
            // Fatima
            // 
            this.Fatima.Controls.Add(this.fatimaVazaoBomba2);
            this.Fatima.Controls.Add(this.waterMeter14);
            this.Fatima.Controls.Add(this.fatimaVazaoBomba1);
            this.Fatima.Controls.Add(this.waterMeter13);
            this.Fatima.Controls.Add(this.label79);
            this.Fatima.Controls.Add(this.junção97);
            this.Fatima.Controls.Add(this.junção89);
            this.Fatima.Controls.Add(this.label77);
            this.Fatima.Controls.Add(this.junção85);
            this.Fatima.Controls.Add(this.junção86);
            this.Fatima.Controls.Add(this.junção88);
            this.Fatima.Controls.Add(this.fatimaBomba3);
            this.Fatima.Controls.Add(this.junção90);
            this.Fatima.Controls.Add(this.junção91);
            this.Fatima.Controls.Add(this.junção92);
            this.Fatima.Controls.Add(this.fatimaBomba2);
            this.Fatima.Controls.Add(this.fatimaCano2Bomba2);
            this.Fatima.Controls.Add(this.junção93);
            this.Fatima.Controls.Add(this.junção95);
            this.Fatima.Controls.Add(this.junção96);
            this.Fatima.Controls.Add(this.label78);
            this.Fatima.Controls.Add(this.fatimaBomba1);
            this.Fatima.Controls.Add(this.fatimaCano2Bomba1);
            this.Fatima.Controls.Add(this.fatimaCano3Bomba1);
            this.Fatima.Controls.Add(this.fatimaCano1Bomba3);
            this.Fatima.Controls.Add(this.junção84);
            this.Fatima.Controls.Add(this.junçãoT51);
            this.Fatima.Controls.Add(this.label69);
            this.Fatima.Controls.Add(this.junçãoT44);
            this.Fatima.Controls.Add(this.fatimaPoco11);
            this.Fatima.Controls.Add(this.fatimaCanoPoco11);
            this.Fatima.Controls.Add(this.label70);
            this.Fatima.Controls.Add(this.label71);
            this.Fatima.Controls.Add(this.label72);
            this.Fatima.Controls.Add(this.label73);
            this.Fatima.Controls.Add(this.label74);
            this.Fatima.Controls.Add(this.label75);
            this.Fatima.Controls.Add(this.label76);
            this.Fatima.Controls.Add(this.junçãoT45);
            this.Fatima.Controls.Add(this.junção83);
            this.Fatima.Controls.Add(this.junçãoT46);
            this.Fatima.Controls.Add(this.fatimaPoco12);
            this.Fatima.Controls.Add(this.fatimaCanoPoco13);
            this.Fatima.Controls.Add(this.junçãoT47);
            this.Fatima.Controls.Add(this.junçãoT48);
            this.Fatima.Controls.Add(this.junçãoT49);
            this.Fatima.Controls.Add(this.junçãoT50);
            this.Fatima.Controls.Add(this.fatimaCanoPoco17);
            this.Fatima.Controls.Add(this.fatimaPoco13);
            this.Fatima.Controls.Add(this.fatimaPoco14);
            this.Fatima.Controls.Add(this.fatimaPoco15);
            this.Fatima.Controls.Add(this.fatimaPoco16);
            this.Fatima.Controls.Add(this.fatimaCanoPoco12);
            this.Fatima.Controls.Add(this.fatimaCanoPoco14);
            this.Fatima.Controls.Add(this.fatimaCanoPoco15);
            this.Fatima.Controls.Add(this.fatimaCanoPoco16);
            this.Fatima.Controls.Add(this.fatimaPoco18);
            this.Fatima.Controls.Add(this.fatimaPoco17);
            this.Fatima.Controls.Add(this.fatimaCanoPoco18);
            this.Fatima.Controls.Add(this.fatimaTanque2);
            this.Fatima.Controls.Add(this.label66);
            this.Fatima.Controls.Add(this.label67);
            this.Fatima.Controls.Add(this.label68);
            this.Fatima.Controls.Add(this.junçãoT41);
            this.Fatima.Controls.Add(this.junçãoT42);
            this.Fatima.Controls.Add(this.fatimaPoco1);
            this.Fatima.Controls.Add(this.fatimaCanoPoco2);
            this.Fatima.Controls.Add(this.junçãoT43);
            this.Fatima.Controls.Add(this.fatimaPoco2);
            this.Fatima.Controls.Add(this.fatimaPoco3);
            this.Fatima.Controls.Add(this.fatimaCanoPoco3);
            this.Fatima.Controls.Add(this.label59);
            this.Fatima.Controls.Add(this.label60);
            this.Fatima.Controls.Add(this.label61);
            this.Fatima.Controls.Add(this.label62);
            this.Fatima.Controls.Add(this.label63);
            this.Fatima.Controls.Add(this.label64);
            this.Fatima.Controls.Add(this.label65);
            this.Fatima.Controls.Add(this.junçãoT35);
            this.Fatima.Controls.Add(this.junção81);
            this.Fatima.Controls.Add(this.fatimaTanque1);
            this.Fatima.Controls.Add(this.junção82);
            this.Fatima.Controls.Add(this.junçãoT36);
            this.Fatima.Controls.Add(this.fatimaPoco4);
            this.Fatima.Controls.Add(this.fatimaCanoPoco5);
            this.Fatima.Controls.Add(this.junçãoT37);
            this.Fatima.Controls.Add(this.junçãoT38);
            this.Fatima.Controls.Add(this.junçãoT39);
            this.Fatima.Controls.Add(this.junçãoT40);
            this.Fatima.Controls.Add(this.fatimaCanoPoco9);
            this.Fatima.Controls.Add(this.fatimaPoco5);
            this.Fatima.Controls.Add(this.fatimaPoco6);
            this.Fatima.Controls.Add(this.fatimaPoco7);
            this.Fatima.Controls.Add(this.fatimaPoco8);
            this.Fatima.Controls.Add(this.fatimaCanoPoco4);
            this.Fatima.Controls.Add(this.fatimaCanoPoco6);
            this.Fatima.Controls.Add(this.fatimaCanoPoco7);
            this.Fatima.Controls.Add(this.fatimaCanoPoco8);
            this.Fatima.Controls.Add(this.fatimaPoco10);
            this.Fatima.Controls.Add(this.fatimaPoco9);
            this.Fatima.Controls.Add(this.fatimaCanoPoco10);
            this.Fatima.Controls.Add(this.fatimaCanoPoco1);
            this.Fatima.Controls.Add(this.fatimaCano1Poco11);
            this.Fatima.Controls.Add(this.fatimaCanoPocos);
            this.Fatima.Controls.Add(this.junção94);
            this.Fatima.Controls.Add(this.fatimaCanoBomba1);
            this.Fatima.Controls.Add(this.fatimaCanoBomba2);
            this.Fatima.Controls.Add(this.junção87);
            this.Fatima.Controls.Add(this.fatimaCanoBomba3);
            this.Fatima.Controls.Add(this.fatimaCano3Bomba2);
            this.Fatima.Controls.Add(this.junção98);
            this.Fatima.Controls.Add(this.fatimaCanoGoias);
            this.Fatima.Controls.Add(this.fatimaCano1Bomba2);
            this.Fatima.Controls.Add(this.fatimaCano1Bomba1);
            this.Fatima.Location = new System.Drawing.Point(4, 22);
            this.Fatima.Name = "Fatima";
            this.Fatima.Size = new System.Drawing.Size(1480, 662);
            this.Fatima.TabIndex = 10;
            this.Fatima.Text = "FÁTIMA";
            this.Fatima.UseVisualStyleBackColor = true;
            // 
            // fatimaVazaoBomba2
            // 
            this.fatimaVazaoBomba2.AutoSize = true;
            this.fatimaVazaoBomba2.BackColor = System.Drawing.Color.Black;
            this.fatimaVazaoBomba2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fatimaVazaoBomba2.ForeColor = System.Drawing.Color.Transparent;
            this.fatimaVazaoBomba2.Location = new System.Drawing.Point(458, 541);
            this.fatimaVazaoBomba2.Name = "fatimaVazaoBomba2";
            this.fatimaVazaoBomba2.Size = new System.Drawing.Size(76, 16);
            this.fatimaVazaoBomba2.TabIndex = 295;
            this.fatimaVazaoBomba2.Text = "00.00 (m³/h)";
            // 
            // waterMeter14
            // 
            this.waterMeter14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter14.Location = new System.Drawing.Point(470, 482);
            this.waterMeter14.Name = "waterMeter14";
            this.waterMeter14.Size = new System.Drawing.Size(55, 55);
            this.waterMeter14.TabIndex = 294;
            this.waterMeter14.Transparence = false;
            // 
            // fatimaVazaoBomba1
            // 
            this.fatimaVazaoBomba1.AutoSize = true;
            this.fatimaVazaoBomba1.BackColor = System.Drawing.Color.Black;
            this.fatimaVazaoBomba1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fatimaVazaoBomba1.ForeColor = System.Drawing.Color.Transparent;
            this.fatimaVazaoBomba1.Location = new System.Drawing.Point(458, 452);
            this.fatimaVazaoBomba1.Name = "fatimaVazaoBomba1";
            this.fatimaVazaoBomba1.Size = new System.Drawing.Size(76, 16);
            this.fatimaVazaoBomba1.TabIndex = 293;
            this.fatimaVazaoBomba1.Text = "00.00 (m³/h)";
            // 
            // waterMeter13
            // 
            this.waterMeter13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter13.Location = new System.Drawing.Point(470, 394);
            this.waterMeter13.Name = "waterMeter13";
            this.waterMeter13.Size = new System.Drawing.Size(55, 55);
            this.waterMeter13.TabIndex = 292;
            this.waterMeter13.Transparence = false;
            // 
            // label79
            // 
            this.label79.BackColor = System.Drawing.Color.Gainsboro;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(1056, 456);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(142, 81);
            this.label79.TabIndex = 253;
            this.label79.Text = "GOIÁS E INDEPENDENCIA";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção97
            // 
            this.junção97.Flip = 180;
            this.junção97.Location = new System.Drawing.Point(1145, 21);
            this.junção97.Margin = new System.Windows.Forms.Padding(0);
            this.junção97.Name = "junção97";
            this.junção97.Size = new System.Drawing.Size(29, 32);
            this.junção97.TabIndex = 252;
            // 
            // junção89
            // 
            this.junção89.Flip = 90;
            this.junção89.Location = new System.Drawing.Point(1048, 58);
            this.junção89.Margin = new System.Windows.Forms.Padding(0);
            this.junção89.Name = "junção89";
            this.junção89.Size = new System.Drawing.Size(30, 32);
            this.junção89.TabIndex = 251;
            // 
            // label77
            // 
            this.label77.BackColor = System.Drawing.Color.Gainsboro;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(279, 489);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(142, 81);
            this.label77.TabIndex = 223;
            this.label77.Text = "GOIÁS, INDUSTRIAL E CENTRO";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção85
            // 
            this.junção85.Flip = 180;
            this.junção85.Location = new System.Drawing.Point(850, 312);
            this.junção85.Margin = new System.Windows.Forms.Padding(0);
            this.junção85.Name = "junção85";
            this.junção85.Size = new System.Drawing.Size(29, 32);
            this.junção85.TabIndex = 230;
            // 
            // junção86
            // 
            this.junção86.Flip = 180;
            this.junção86.Location = new System.Drawing.Point(993, 312);
            this.junção86.Margin = new System.Windows.Forms.Padding(0);
            this.junção86.Name = "junção86";
            this.junção86.Size = new System.Drawing.Size(29, 32);
            this.junção86.TabIndex = 241;
            // 
            // junção88
            // 
            this.junção88.Flip = 90;
            this.junção88.Location = new System.Drawing.Point(1083, 22);
            this.junção88.Margin = new System.Windows.Forms.Padding(0);
            this.junção88.Name = "junção88";
            this.junção88.Size = new System.Drawing.Size(30, 32);
            this.junção88.TabIndex = 247;
            // 
            // fatimaBomba3
            // 
            this.fatimaBomba3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaBomba3.LigarBomba = true;
            this.fatimaBomba3.Location = new System.Drawing.Point(1075, 45);
            this.fatimaBomba3.Name = "fatimaBomba3";
            this.fatimaBomba3.Size = new System.Drawing.Size(60, 50);
            this.fatimaBomba3.TabIndex = 244;
            this.fatimaBomba3.Transparence = false;
            // 
            // junção90
            // 
            this.junção90.Flip = 270;
            this.junção90.Location = new System.Drawing.Point(993, 499);
            this.junção90.Margin = new System.Windows.Forms.Padding(0);
            this.junção90.Name = "junção90";
            this.junção90.Size = new System.Drawing.Size(29, 32);
            this.junção90.TabIndex = 236;
            // 
            // junção91
            // 
            this.junção91.Flip = 90;
            this.junção91.Location = new System.Drawing.Point(938, 312);
            this.junção91.Margin = new System.Windows.Forms.Padding(0);
            this.junção91.Name = "junção91";
            this.junção91.Size = new System.Drawing.Size(30, 32);
            this.junção91.TabIndex = 240;
            // 
            // junção92
            // 
            this.junção92.Flip = 0;
            this.junção92.Location = new System.Drawing.Point(901, 336);
            this.junção92.Margin = new System.Windows.Forms.Padding(0);
            this.junção92.Name = "junção92";
            this.junção92.Size = new System.Drawing.Size(31, 32);
            this.junção92.TabIndex = 238;
            // 
            // fatimaBomba2
            // 
            this.fatimaBomba2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaBomba2.LigarBomba = false;
            this.fatimaBomba2.Location = new System.Drawing.Point(930, 336);
            this.fatimaBomba2.Name = "fatimaBomba2";
            this.fatimaBomba2.Size = new System.Drawing.Size(60, 50);
            this.fatimaBomba2.TabIndex = 237;
            this.fatimaBomba2.Transparence = false;
            // 
            // fatimaCano2Bomba2
            // 
            this.fatimaCano2Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCano2Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCano2Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCano2Bomba2.diminuirbolha = 5D;
            this.fatimaCano2Bomba2.enableTransparentBackground = true;
            this.fatimaCano2Bomba2.fliped = false;
            this.fatimaCano2Bomba2.fluxo = 0;
            this.fatimaCano2Bomba2.Location = new System.Drawing.Point(1007, 336);
            this.fatimaCano2Bomba2.MaxVidro = 127;
            this.fatimaCano2Bomba2.MinVidro = 8;
            this.fatimaCano2Bomba2.Name = "fatimaCano2Bomba2";
            this.fatimaCano2Bomba2.RaioYElipse = 30;
            this.fatimaCano2Bomba2.Size = new System.Drawing.Size(13, 169);
            this.fatimaCano2Bomba2.TabIndex = 242;
            this.fatimaCano2Bomba2.vertical = true;
            // 
            // junção93
            // 
            this.junção93.Flip = 270;
            this.junção93.Location = new System.Drawing.Point(850, 412);
            this.junção93.Margin = new System.Windows.Forms.Padding(0);
            this.junção93.Name = "junção93";
            this.junção93.Size = new System.Drawing.Size(29, 32);
            this.junção93.TabIndex = 233;
            // 
            // junção95
            // 
            this.junção95.Flip = 90;
            this.junção95.Location = new System.Drawing.Point(795, 312);
            this.junção95.Margin = new System.Windows.Forms.Padding(0);
            this.junção95.Name = "junção95";
            this.junção95.Size = new System.Drawing.Size(30, 32);
            this.junção95.TabIndex = 227;
            // 
            // junção96
            // 
            this.junção96.Flip = 0;
            this.junção96.Location = new System.Drawing.Point(758, 336);
            this.junção96.Margin = new System.Windows.Forms.Padding(0);
            this.junção96.Name = "junção96";
            this.junção96.Size = new System.Drawing.Size(31, 32);
            this.junção96.TabIndex = 225;
            // 
            // label78
            // 
            this.label78.BackColor = System.Drawing.Color.Gainsboro;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(279, 391);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(142, 83);
            this.label78.TabIndex = 224;
            this.label78.Text = "CENTRO";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fatimaBomba1
            // 
            this.fatimaBomba1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaBomba1.LigarBomba = false;
            this.fatimaBomba1.Location = new System.Drawing.Point(787, 336);
            this.fatimaBomba1.Name = "fatimaBomba1";
            this.fatimaBomba1.Size = new System.Drawing.Size(60, 50);
            this.fatimaBomba1.TabIndex = 222;
            this.fatimaBomba1.Transparence = false;
            // 
            // fatimaCano2Bomba1
            // 
            this.fatimaCano2Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCano2Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCano2Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCano2Bomba1.diminuirbolha = 5D;
            this.fatimaCano2Bomba1.enableTransparentBackground = true;
            this.fatimaCano2Bomba1.fliped = false;
            this.fatimaCano2Bomba1.fluxo = 0;
            this.fatimaCano2Bomba1.Location = new System.Drawing.Point(864, 336);
            this.fatimaCano2Bomba1.MaxVidro = 127;
            this.fatimaCano2Bomba1.MinVidro = 8;
            this.fatimaCano2Bomba1.Name = "fatimaCano2Bomba1";
            this.fatimaCano2Bomba1.RaioYElipse = 30;
            this.fatimaCano2Bomba1.Size = new System.Drawing.Size(13, 85);
            this.fatimaCano2Bomba1.TabIndex = 231;
            this.fatimaCano2Bomba1.vertical = true;
            // 
            // fatimaCano3Bomba1
            // 
            this.fatimaCano3Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCano3Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCano3Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCano3Bomba1.diminuirbolha = 5D;
            this.fatimaCano3Bomba1.enableTransparentBackground = true;
            this.fatimaCano3Bomba1.fliped = false;
            this.fatimaCano3Bomba1.fluxo = 0;
            this.fatimaCano3Bomba1.Location = new System.Drawing.Point(400, 429);
            this.fatimaCano3Bomba1.MaxVidro = 127;
            this.fatimaCano3Bomba1.MinVidro = 8;
            this.fatimaCano3Bomba1.Name = "fatimaCano3Bomba1";
            this.fatimaCano3Bomba1.RaioYElipse = 30;
            this.fatimaCano3Bomba1.Size = new System.Drawing.Size(460, 13);
            this.fatimaCano3Bomba1.TabIndex = 232;
            this.fatimaCano3Bomba1.vertical = false;
            // 
            // fatimaCano1Bomba3
            // 
            this.fatimaCano1Bomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCano1Bomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCano1Bomba3.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCano1Bomba3.diminuirbolha = 5D;
            this.fatimaCano1Bomba3.enableTransparentBackground = true;
            this.fatimaCano1Bomba3.fliped = false;
            this.fatimaCano1Bomba3.fluxo = -1;
            this.fatimaCano1Bomba3.Location = new System.Drawing.Point(1103, 25);
            this.fatimaCano1Bomba3.MaxVidro = 127;
            this.fatimaCano1Bomba3.MinVidro = 8;
            this.fatimaCano1Bomba3.Name = "fatimaCano1Bomba3";
            this.fatimaCano1Bomba3.RaioYElipse = 30;
            this.fatimaCano1Bomba3.Size = new System.Drawing.Size(54, 13);
            this.fatimaCano1Bomba3.TabIndex = 250;
            this.fatimaCano1Bomba3.vertical = false;
            // 
            // junção84
            // 
            this.junção84.Flip = 270;
            this.junção84.Location = new System.Drawing.Point(723, 155);
            this.junção84.Margin = new System.Windows.Forms.Padding(0);
            this.junção84.Name = "junção84";
            this.junção84.Size = new System.Drawing.Size(29, 32);
            this.junção84.TabIndex = 188;
            // 
            // junçãoT51
            // 
            this.junçãoT51.Flip = 180;
            this.junçãoT51.Location = new System.Drawing.Point(722, 24);
            this.junçãoT51.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT51.Name = "junçãoT51";
            this.junçãoT51.Size = new System.Drawing.Size(42, 44);
            this.junçãoT51.TabIndex = 186;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(510, 275);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(70, 16);
            this.label69.TabIndex = 185;
            this.label69.Text = "POÇO 11";
            // 
            // junçãoT44
            // 
            this.junçãoT44.Flip = 180;
            this.junçãoT44.Location = new System.Drawing.Point(524, 163);
            this.junçãoT44.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT44.Name = "junçãoT44";
            this.junçãoT44.Size = new System.Drawing.Size(42, 44);
            this.junçãoT44.TabIndex = 182;
            // 
            // fatimaPoco11
            // 
            this.fatimaPoco11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco11.LigarBomba = true;
            this.fatimaPoco11.Location = new System.Drawing.Point(506, 194);
            this.fatimaPoco11.Name = "fatimaPoco11";
            this.fatimaPoco11.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco11.TabIndex = 184;
            this.fatimaPoco11.Transparence = false;
            // 
            // fatimaCanoPoco11
            // 
            this.fatimaCanoPoco11.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco11.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco11.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco11.diminuirbolha = 5D;
            this.fatimaCanoPoco11.enableTransparentBackground = true;
            this.fatimaCanoPoco11.fliped = false;
            this.fatimaCanoPoco11.fluxo = -1;
            this.fatimaCanoPoco11.Location = new System.Drawing.Point(552, 171);
            this.fatimaCanoPoco11.MaxVidro = 127;
            this.fatimaCanoPoco11.MinVidro = 8;
            this.fatimaCanoPoco11.Name = "fatimaCanoPoco11";
            this.fatimaCanoPoco11.RaioYElipse = 30;
            this.fatimaCanoPoco11.Size = new System.Drawing.Size(186, 13);
            this.fatimaCanoPoco11.TabIndex = 183;
            this.fatimaCanoPoco11.vertical = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(438, 275);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(70, 16);
            this.label70.TabIndex = 181;
            this.label70.Text = "POÇO 12";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(366, 275);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(70, 16);
            this.label71.TabIndex = 180;
            this.label71.Text = "POÇO 13";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(296, 275);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(70, 16);
            this.label72.TabIndex = 179;
            this.label72.Text = "POÇO 14";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(224, 275);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(70, 16);
            this.label73.TabIndex = 178;
            this.label73.Text = "POÇO 15";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(154, 275);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(70, 16);
            this.label74.TabIndex = 177;
            this.label74.Text = "POÇO 16";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(84, 275);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(70, 16);
            this.label75.TabIndex = 176;
            this.label75.Text = "POÇO 17";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(12, 275);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(70, 16);
            this.label76.TabIndex = 175;
            this.label76.Text = "POÇO 18";
            // 
            // junçãoT45
            // 
            this.junçãoT45.Flip = 180;
            this.junçãoT45.Location = new System.Drawing.Point(379, 163);
            this.junçãoT45.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT45.Name = "junçãoT45";
            this.junçãoT45.Size = new System.Drawing.Size(42, 44);
            this.junçãoT45.TabIndex = 162;
            // 
            // junção83
            // 
            this.junção83.Flip = 90;
            this.junção83.Location = new System.Drawing.Point(38, 169);
            this.junção83.Margin = new System.Windows.Forms.Padding(0);
            this.junção83.Name = "junção83";
            this.junção83.Size = new System.Drawing.Size(30, 32);
            this.junção83.TabIndex = 174;
            // 
            // junçãoT46
            // 
            this.junçãoT46.Flip = 180;
            this.junçãoT46.Location = new System.Drawing.Point(451, 163);
            this.junçãoT46.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT46.Name = "junçãoT46";
            this.junçãoT46.Size = new System.Drawing.Size(42, 44);
            this.junçãoT46.TabIndex = 171;
            // 
            // fatimaPoco12
            // 
            this.fatimaPoco12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco12.LigarBomba = true;
            this.fatimaPoco12.Location = new System.Drawing.Point(434, 194);
            this.fatimaPoco12.Name = "fatimaPoco12";
            this.fatimaPoco12.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco12.TabIndex = 173;
            this.fatimaPoco12.Transparence = false;
            // 
            // fatimaCanoPoco13
            // 
            this.fatimaCanoPoco13.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco13.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco13.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco13.diminuirbolha = 5D;
            this.fatimaCanoPoco13.enableTransparentBackground = true;
            this.fatimaCanoPoco13.fliped = false;
            this.fatimaCanoPoco13.fluxo = -1;
            this.fatimaCanoPoco13.Location = new System.Drawing.Point(417, 171);
            this.fatimaCanoPoco13.MaxVidro = 127;
            this.fatimaCanoPoco13.MinVidro = 8;
            this.fatimaCanoPoco13.Name = "fatimaCanoPoco13";
            this.fatimaCanoPoco13.RaioYElipse = 30;
            this.fatimaCanoPoco13.Size = new System.Drawing.Size(40, 13);
            this.fatimaCanoPoco13.TabIndex = 172;
            this.fatimaCanoPoco13.vertical = false;
            // 
            // junçãoT47
            // 
            this.junçãoT47.Flip = 180;
            this.junçãoT47.Location = new System.Drawing.Point(310, 163);
            this.junçãoT47.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT47.Name = "junçãoT47";
            this.junçãoT47.Size = new System.Drawing.Size(42, 44);
            this.junçãoT47.TabIndex = 160;
            // 
            // junçãoT48
            // 
            this.junçãoT48.Flip = 180;
            this.junçãoT48.Location = new System.Drawing.Point(238, 163);
            this.junçãoT48.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT48.Name = "junçãoT48";
            this.junçãoT48.Size = new System.Drawing.Size(42, 44);
            this.junçãoT48.TabIndex = 158;
            // 
            // junçãoT49
            // 
            this.junçãoT49.Flip = 180;
            this.junçãoT49.Location = new System.Drawing.Point(168, 163);
            this.junçãoT49.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT49.Name = "junçãoT49";
            this.junçãoT49.Size = new System.Drawing.Size(42, 44);
            this.junçãoT49.TabIndex = 156;
            // 
            // junçãoT50
            // 
            this.junçãoT50.Flip = 180;
            this.junçãoT50.Location = new System.Drawing.Point(98, 163);
            this.junçãoT50.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT50.Name = "junçãoT50";
            this.junçãoT50.Size = new System.Drawing.Size(42, 44);
            this.junçãoT50.TabIndex = 154;
            // 
            // fatimaCanoPoco17
            // 
            this.fatimaCanoPoco17.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco17.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco17.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco17.diminuirbolha = 5D;
            this.fatimaCanoPoco17.enableTransparentBackground = true;
            this.fatimaCanoPoco17.fliped = false;
            this.fatimaCanoPoco17.fluxo = -1;
            this.fatimaCanoPoco17.Location = new System.Drawing.Point(131, 171);
            this.fatimaCanoPoco17.MaxVidro = 127;
            this.fatimaCanoPoco17.MinVidro = 8;
            this.fatimaCanoPoco17.Name = "fatimaCanoPoco17";
            this.fatimaCanoPoco17.RaioYElipse = 30;
            this.fatimaCanoPoco17.Size = new System.Drawing.Size(37, 13);
            this.fatimaCanoPoco17.TabIndex = 157;
            this.fatimaCanoPoco17.vertical = false;
            // 
            // fatimaPoco13
            // 
            this.fatimaPoco13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco13.LigarBomba = true;
            this.fatimaPoco13.Location = new System.Drawing.Point(362, 194);
            this.fatimaPoco13.Name = "fatimaPoco13";
            this.fatimaPoco13.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco13.TabIndex = 170;
            this.fatimaPoco13.Transparence = false;
            // 
            // fatimaPoco14
            // 
            this.fatimaPoco14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco14.LigarBomba = true;
            this.fatimaPoco14.Location = new System.Drawing.Point(292, 194);
            this.fatimaPoco14.Name = "fatimaPoco14";
            this.fatimaPoco14.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco14.TabIndex = 169;
            this.fatimaPoco14.Transparence = false;
            // 
            // fatimaPoco15
            // 
            this.fatimaPoco15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco15.LigarBomba = true;
            this.fatimaPoco15.Location = new System.Drawing.Point(220, 194);
            this.fatimaPoco15.Name = "fatimaPoco15";
            this.fatimaPoco15.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco15.TabIndex = 168;
            this.fatimaPoco15.Transparence = false;
            // 
            // fatimaPoco16
            // 
            this.fatimaPoco16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco16.LigarBomba = true;
            this.fatimaPoco16.Location = new System.Drawing.Point(150, 194);
            this.fatimaPoco16.Name = "fatimaPoco16";
            this.fatimaPoco16.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco16.TabIndex = 167;
            this.fatimaPoco16.Transparence = false;
            // 
            // fatimaCanoPoco12
            // 
            this.fatimaCanoPoco12.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco12.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco12.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco12.diminuirbolha = 5D;
            this.fatimaCanoPoco12.enableTransparentBackground = true;
            this.fatimaCanoPoco12.fliped = false;
            this.fatimaCanoPoco12.fluxo = -1;
            this.fatimaCanoPoco12.Location = new System.Drawing.Point(490, 171);
            this.fatimaCanoPoco12.MaxVidro = 127;
            this.fatimaCanoPoco12.MinVidro = 8;
            this.fatimaCanoPoco12.Name = "fatimaCanoPoco12";
            this.fatimaCanoPoco12.RaioYElipse = 30;
            this.fatimaCanoPoco12.Size = new System.Drawing.Size(35, 13);
            this.fatimaCanoPoco12.TabIndex = 164;
            this.fatimaCanoPoco12.vertical = false;
            // 
            // fatimaCanoPoco14
            // 
            this.fatimaCanoPoco14.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco14.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco14.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco14.diminuirbolha = 5D;
            this.fatimaCanoPoco14.enableTransparentBackground = true;
            this.fatimaCanoPoco14.fliped = false;
            this.fatimaCanoPoco14.fluxo = -1;
            this.fatimaCanoPoco14.Location = new System.Drawing.Point(345, 171);
            this.fatimaCanoPoco14.MaxVidro = 127;
            this.fatimaCanoPoco14.MinVidro = 8;
            this.fatimaCanoPoco14.Name = "fatimaCanoPoco14";
            this.fatimaCanoPoco14.RaioYElipse = 30;
            this.fatimaCanoPoco14.Size = new System.Drawing.Size(40, 13);
            this.fatimaCanoPoco14.TabIndex = 163;
            this.fatimaCanoPoco14.vertical = false;
            // 
            // fatimaCanoPoco15
            // 
            this.fatimaCanoPoco15.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco15.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco15.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco15.diminuirbolha = 5D;
            this.fatimaCanoPoco15.enableTransparentBackground = true;
            this.fatimaCanoPoco15.fliped = false;
            this.fatimaCanoPoco15.fluxo = -1;
            this.fatimaCanoPoco15.Location = new System.Drawing.Point(274, 171);
            this.fatimaCanoPoco15.MaxVidro = 127;
            this.fatimaCanoPoco15.MinVidro = 8;
            this.fatimaCanoPoco15.Name = "fatimaCanoPoco15";
            this.fatimaCanoPoco15.RaioYElipse = 30;
            this.fatimaCanoPoco15.Size = new System.Drawing.Size(40, 13);
            this.fatimaCanoPoco15.TabIndex = 161;
            this.fatimaCanoPoco15.vertical = false;
            // 
            // fatimaCanoPoco16
            // 
            this.fatimaCanoPoco16.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco16.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco16.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco16.diminuirbolha = 5D;
            this.fatimaCanoPoco16.enableTransparentBackground = true;
            this.fatimaCanoPoco16.fliped = false;
            this.fatimaCanoPoco16.fluxo = -1;
            this.fatimaCanoPoco16.Location = new System.Drawing.Point(192, 171);
            this.fatimaCanoPoco16.MaxVidro = 127;
            this.fatimaCanoPoco16.MinVidro = 8;
            this.fatimaCanoPoco16.Name = "fatimaCanoPoco16";
            this.fatimaCanoPoco16.RaioYElipse = 30;
            this.fatimaCanoPoco16.Size = new System.Drawing.Size(56, 13);
            this.fatimaCanoPoco16.TabIndex = 159;
            this.fatimaCanoPoco16.vertical = false;
            // 
            // fatimaPoco18
            // 
            this.fatimaPoco18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco18.LigarBomba = true;
            this.fatimaPoco18.Location = new System.Drawing.Point(8, 194);
            this.fatimaPoco18.Name = "fatimaPoco18";
            this.fatimaPoco18.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco18.TabIndex = 165;
            this.fatimaPoco18.Transparence = false;
            // 
            // fatimaPoco17
            // 
            this.fatimaPoco17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco17.LigarBomba = true;
            this.fatimaPoco17.Location = new System.Drawing.Point(80, 194);
            this.fatimaPoco17.Name = "fatimaPoco17";
            this.fatimaPoco17.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco17.TabIndex = 166;
            this.fatimaPoco17.Transparence = false;
            // 
            // fatimaCanoPoco18
            // 
            this.fatimaCanoPoco18.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco18.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco18.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco18.diminuirbolha = 5D;
            this.fatimaCanoPoco18.enableTransparentBackground = true;
            this.fatimaCanoPoco18.fliped = false;
            this.fatimaCanoPoco18.fluxo = -1;
            this.fatimaCanoPoco18.Location = new System.Drawing.Point(65, 171);
            this.fatimaCanoPoco18.MaxVidro = 127;
            this.fatimaCanoPoco18.MinVidro = 8;
            this.fatimaCanoPoco18.Name = "fatimaCanoPoco18";
            this.fatimaCanoPoco18.RaioYElipse = 30;
            this.fatimaCanoPoco18.Size = new System.Drawing.Size(43, 13);
            this.fatimaCanoPoco18.TabIndex = 155;
            this.fatimaCanoPoco18.vertical = false;
            // 
            // fatimaTanque2
            // 
            this.fatimaTanque2.BackColor = System.Drawing.Color.Transparent;
            this.fatimaTanque2.Cano = 2;
            this.fatimaTanque2.CanoCheio = true;
            this.fatimaTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaTanque2.CorVidro = System.Drawing.Color.Silver;
            this.fatimaTanque2.enableTransparentBackground = true;
            this.fatimaTanque2.LarguraCano = 13;
            this.fatimaTanque2.Location = new System.Drawing.Point(1144, 44);
            this.fatimaTanque2.MaxVidro = 127;
            this.fatimaTanque2.MinVidro = 8;
            this.fatimaTanque2.Name = "fatimaTanque2";
            this.fatimaTanque2.PosCano = 15;
            this.fatimaTanque2.RaioYElipse = 30;
            this.fatimaTanque2.Size = new System.Drawing.Size(161, 259);
            this.fatimaTanque2.TabIndex = 153;
            this.fatimaTanque2.Value = 40D;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(645, 136);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(62, 16);
            this.label66.TabIndex = 152;
            this.label66.Text = "POÇO 1";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(573, 136);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(62, 16);
            this.label67.TabIndex = 151;
            this.label67.Text = "POÇO 2";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(503, 136);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(62, 16);
            this.label68.TabIndex = 150;
            this.label68.Text = "POÇO 3";
            // 
            // junçãoT41
            // 
            this.junçãoT41.Flip = 180;
            this.junçãoT41.Location = new System.Drawing.Point(586, 24);
            this.junçãoT41.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT41.Name = "junçãoT41";
            this.junçãoT41.Size = new System.Drawing.Size(42, 44);
            this.junçãoT41.TabIndex = 142;
            // 
            // junçãoT42
            // 
            this.junçãoT42.Flip = 180;
            this.junçãoT42.Location = new System.Drawing.Point(658, 24);
            this.junçãoT42.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT42.Name = "junçãoT42";
            this.junçãoT42.Size = new System.Drawing.Size(42, 44);
            this.junçãoT42.TabIndex = 147;
            // 
            // fatimaPoco1
            // 
            this.fatimaPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco1.LigarBomba = true;
            this.fatimaPoco1.Location = new System.Drawing.Point(641, 55);
            this.fatimaPoco1.Name = "fatimaPoco1";
            this.fatimaPoco1.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco1.TabIndex = 149;
            this.fatimaPoco1.Transparence = false;
            // 
            // fatimaCanoPoco2
            // 
            this.fatimaCanoPoco2.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco2.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco2.diminuirbolha = 5D;
            this.fatimaCanoPoco2.enableTransparentBackground = true;
            this.fatimaCanoPoco2.fliped = false;
            this.fatimaCanoPoco2.fluxo = -1;
            this.fatimaCanoPoco2.Location = new System.Drawing.Point(624, 32);
            this.fatimaCanoPoco2.MaxVidro = 127;
            this.fatimaCanoPoco2.MinVidro = 8;
            this.fatimaCanoPoco2.Name = "fatimaCanoPoco2";
            this.fatimaCanoPoco2.RaioYElipse = 30;
            this.fatimaCanoPoco2.Size = new System.Drawing.Size(40, 13);
            this.fatimaCanoPoco2.TabIndex = 148;
            this.fatimaCanoPoco2.vertical = false;
            // 
            // junçãoT43
            // 
            this.junçãoT43.Flip = 180;
            this.junçãoT43.Location = new System.Drawing.Point(517, 24);
            this.junçãoT43.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT43.Name = "junçãoT43";
            this.junçãoT43.Size = new System.Drawing.Size(42, 44);
            this.junçãoT43.TabIndex = 141;
            // 
            // fatimaPoco2
            // 
            this.fatimaPoco2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco2.LigarBomba = true;
            this.fatimaPoco2.Location = new System.Drawing.Point(569, 55);
            this.fatimaPoco2.Name = "fatimaPoco2";
            this.fatimaPoco2.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco2.TabIndex = 146;
            this.fatimaPoco2.Transparence = false;
            // 
            // fatimaPoco3
            // 
            this.fatimaPoco3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco3.LigarBomba = true;
            this.fatimaPoco3.Location = new System.Drawing.Point(499, 55);
            this.fatimaPoco3.Name = "fatimaPoco3";
            this.fatimaPoco3.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco3.TabIndex = 145;
            this.fatimaPoco3.Transparence = false;
            // 
            // fatimaCanoPoco3
            // 
            this.fatimaCanoPoco3.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco3.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco3.diminuirbolha = 5D;
            this.fatimaCanoPoco3.enableTransparentBackground = true;
            this.fatimaCanoPoco3.fliped = false;
            this.fatimaCanoPoco3.fluxo = -1;
            this.fatimaCanoPoco3.Location = new System.Drawing.Point(552, 32);
            this.fatimaCanoPoco3.MaxVidro = 127;
            this.fatimaCanoPoco3.MinVidro = 8;
            this.fatimaCanoPoco3.Name = "fatimaCanoPoco3";
            this.fatimaCanoPoco3.RaioYElipse = 30;
            this.fatimaCanoPoco3.Size = new System.Drawing.Size(40, 13);
            this.fatimaCanoPoco3.TabIndex = 143;
            this.fatimaCanoPoco3.vertical = false;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(431, 136);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(62, 16);
            this.label59.TabIndex = 140;
            this.label59.Text = "POÇO 4";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(359, 136);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(62, 16);
            this.label60.TabIndex = 139;
            this.label60.Text = "POÇO 5";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(289, 136);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(62, 16);
            this.label61.TabIndex = 138;
            this.label61.Text = "POÇO 6";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(217, 136);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(62, 16);
            this.label62.TabIndex = 137;
            this.label62.Text = "POÇO 7";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(147, 136);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(62, 16);
            this.label63.TabIndex = 136;
            this.label63.Text = "POÇO 8";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(77, 136);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(62, 16);
            this.label64.TabIndex = 135;
            this.label64.Text = "POÇO 9";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(5, 136);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(70, 16);
            this.label65.TabIndex = 134;
            this.label65.Text = "POÇO 10";
            // 
            // junçãoT35
            // 
            this.junçãoT35.Flip = 180;
            this.junçãoT35.Location = new System.Drawing.Point(372, 24);
            this.junçãoT35.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT35.Name = "junçãoT35";
            this.junçãoT35.Size = new System.Drawing.Size(42, 44);
            this.junçãoT35.TabIndex = 120;
            // 
            // junção81
            // 
            this.junção81.Flip = 180;
            this.junção81.Location = new System.Drawing.Point(784, 29);
            this.junção81.Margin = new System.Windows.Forms.Padding(0);
            this.junção81.Name = "junção81";
            this.junção81.Size = new System.Drawing.Size(29, 32);
            this.junção81.TabIndex = 133;
            // 
            // fatimaTanque1
            // 
            this.fatimaTanque1.BackColor = System.Drawing.Color.Transparent;
            this.fatimaTanque1.Cano = 2;
            this.fatimaTanque1.CanoCheio = true;
            this.fatimaTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaTanque1.CorVidro = System.Drawing.Color.Silver;
            this.fatimaTanque1.enableTransparentBackground = true;
            this.fatimaTanque1.LarguraCano = 13;
            this.fatimaTanque1.Location = new System.Drawing.Point(783, 56);
            this.fatimaTanque1.MaxVidro = 127;
            this.fatimaTanque1.MinVidro = 8;
            this.fatimaTanque1.Name = "fatimaTanque1";
            this.fatimaTanque1.PosCano = 15;
            this.fatimaTanque1.RaioYElipse = 30;
            this.fatimaTanque1.Size = new System.Drawing.Size(255, 198);
            this.fatimaTanque1.TabIndex = 111;
            this.fatimaTanque1.Value = 40D;
            // 
            // junção82
            // 
            this.junção82.Flip = 90;
            this.junção82.Location = new System.Drawing.Point(31, 30);
            this.junção82.Margin = new System.Windows.Forms.Padding(0);
            this.junção82.Name = "junção82";
            this.junção82.Size = new System.Drawing.Size(30, 32);
            this.junção82.TabIndex = 132;
            // 
            // junçãoT36
            // 
            this.junçãoT36.Flip = 180;
            this.junçãoT36.Location = new System.Drawing.Point(444, 24);
            this.junçãoT36.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT36.Name = "junçãoT36";
            this.junçãoT36.Size = new System.Drawing.Size(42, 44);
            this.junçãoT36.TabIndex = 129;
            // 
            // fatimaPoco4
            // 
            this.fatimaPoco4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco4.LigarBomba = true;
            this.fatimaPoco4.Location = new System.Drawing.Point(427, 55);
            this.fatimaPoco4.Name = "fatimaPoco4";
            this.fatimaPoco4.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco4.TabIndex = 131;
            this.fatimaPoco4.Transparence = false;
            // 
            // fatimaCanoPoco5
            // 
            this.fatimaCanoPoco5.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco5.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco5.diminuirbolha = 5D;
            this.fatimaCanoPoco5.enableTransparentBackground = true;
            this.fatimaCanoPoco5.fliped = false;
            this.fatimaCanoPoco5.fluxo = -1;
            this.fatimaCanoPoco5.Location = new System.Drawing.Point(410, 32);
            this.fatimaCanoPoco5.MaxVidro = 127;
            this.fatimaCanoPoco5.MinVidro = 8;
            this.fatimaCanoPoco5.Name = "fatimaCanoPoco5";
            this.fatimaCanoPoco5.RaioYElipse = 30;
            this.fatimaCanoPoco5.Size = new System.Drawing.Size(40, 13);
            this.fatimaCanoPoco5.TabIndex = 130;
            this.fatimaCanoPoco5.vertical = false;
            // 
            // junçãoT37
            // 
            this.junçãoT37.Flip = 180;
            this.junçãoT37.Location = new System.Drawing.Point(303, 24);
            this.junçãoT37.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT37.Name = "junçãoT37";
            this.junçãoT37.Size = new System.Drawing.Size(42, 44);
            this.junçãoT37.TabIndex = 118;
            // 
            // junçãoT38
            // 
            this.junçãoT38.Flip = 180;
            this.junçãoT38.Location = new System.Drawing.Point(231, 24);
            this.junçãoT38.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT38.Name = "junçãoT38";
            this.junçãoT38.Size = new System.Drawing.Size(42, 44);
            this.junçãoT38.TabIndex = 116;
            // 
            // junçãoT39
            // 
            this.junçãoT39.Flip = 180;
            this.junçãoT39.Location = new System.Drawing.Point(161, 24);
            this.junçãoT39.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT39.Name = "junçãoT39";
            this.junçãoT39.Size = new System.Drawing.Size(42, 44);
            this.junçãoT39.TabIndex = 114;
            // 
            // junçãoT40
            // 
            this.junçãoT40.Flip = 180;
            this.junçãoT40.Location = new System.Drawing.Point(91, 24);
            this.junçãoT40.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT40.Name = "junçãoT40";
            this.junçãoT40.Size = new System.Drawing.Size(42, 44);
            this.junçãoT40.TabIndex = 112;
            // 
            // fatimaCanoPoco9
            // 
            this.fatimaCanoPoco9.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco9.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco9.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco9.diminuirbolha = 5D;
            this.fatimaCanoPoco9.enableTransparentBackground = true;
            this.fatimaCanoPoco9.fliped = false;
            this.fatimaCanoPoco9.fluxo = -1;
            this.fatimaCanoPoco9.Location = new System.Drawing.Point(124, 32);
            this.fatimaCanoPoco9.MaxVidro = 127;
            this.fatimaCanoPoco9.MinVidro = 8;
            this.fatimaCanoPoco9.Name = "fatimaCanoPoco9";
            this.fatimaCanoPoco9.RaioYElipse = 30;
            this.fatimaCanoPoco9.Size = new System.Drawing.Size(44, 13);
            this.fatimaCanoPoco9.TabIndex = 115;
            this.fatimaCanoPoco9.vertical = false;
            // 
            // fatimaPoco5
            // 
            this.fatimaPoco5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco5.LigarBomba = true;
            this.fatimaPoco5.Location = new System.Drawing.Point(355, 55);
            this.fatimaPoco5.Name = "fatimaPoco5";
            this.fatimaPoco5.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco5.TabIndex = 128;
            this.fatimaPoco5.Transparence = false;
            // 
            // fatimaPoco6
            // 
            this.fatimaPoco6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco6.LigarBomba = true;
            this.fatimaPoco6.Location = new System.Drawing.Point(285, 55);
            this.fatimaPoco6.Name = "fatimaPoco6";
            this.fatimaPoco6.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco6.TabIndex = 127;
            this.fatimaPoco6.Transparence = false;
            // 
            // fatimaPoco7
            // 
            this.fatimaPoco7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco7.LigarBomba = true;
            this.fatimaPoco7.Location = new System.Drawing.Point(213, 55);
            this.fatimaPoco7.Name = "fatimaPoco7";
            this.fatimaPoco7.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco7.TabIndex = 126;
            this.fatimaPoco7.Transparence = false;
            // 
            // fatimaPoco8
            // 
            this.fatimaPoco8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco8.LigarBomba = true;
            this.fatimaPoco8.Location = new System.Drawing.Point(143, 55);
            this.fatimaPoco8.Name = "fatimaPoco8";
            this.fatimaPoco8.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco8.TabIndex = 125;
            this.fatimaPoco8.Transparence = false;
            // 
            // fatimaCanoPoco4
            // 
            this.fatimaCanoPoco4.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco4.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco4.diminuirbolha = 5D;
            this.fatimaCanoPoco4.enableTransparentBackground = true;
            this.fatimaCanoPoco4.fliped = false;
            this.fatimaCanoPoco4.fluxo = -1;
            this.fatimaCanoPoco4.Location = new System.Drawing.Point(483, 32);
            this.fatimaCanoPoco4.MaxVidro = 127;
            this.fatimaCanoPoco4.MinVidro = 8;
            this.fatimaCanoPoco4.Name = "fatimaCanoPoco4";
            this.fatimaCanoPoco4.RaioYElipse = 30;
            this.fatimaCanoPoco4.Size = new System.Drawing.Size(35, 13);
            this.fatimaCanoPoco4.TabIndex = 122;
            this.fatimaCanoPoco4.vertical = false;
            // 
            // fatimaCanoPoco6
            // 
            this.fatimaCanoPoco6.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco6.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco6.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco6.diminuirbolha = 5D;
            this.fatimaCanoPoco6.enableTransparentBackground = true;
            this.fatimaCanoPoco6.fliped = false;
            this.fatimaCanoPoco6.fluxo = -1;
            this.fatimaCanoPoco6.Location = new System.Drawing.Point(338, 32);
            this.fatimaCanoPoco6.MaxVidro = 127;
            this.fatimaCanoPoco6.MinVidro = 8;
            this.fatimaCanoPoco6.Name = "fatimaCanoPoco6";
            this.fatimaCanoPoco6.RaioYElipse = 30;
            this.fatimaCanoPoco6.Size = new System.Drawing.Size(40, 13);
            this.fatimaCanoPoco6.TabIndex = 121;
            this.fatimaCanoPoco6.vertical = false;
            // 
            // fatimaCanoPoco7
            // 
            this.fatimaCanoPoco7.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco7.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco7.diminuirbolha = 5D;
            this.fatimaCanoPoco7.enableTransparentBackground = true;
            this.fatimaCanoPoco7.fliped = false;
            this.fatimaCanoPoco7.fluxo = -1;
            this.fatimaCanoPoco7.Location = new System.Drawing.Point(267, 32);
            this.fatimaCanoPoco7.MaxVidro = 127;
            this.fatimaCanoPoco7.MinVidro = 8;
            this.fatimaCanoPoco7.Name = "fatimaCanoPoco7";
            this.fatimaCanoPoco7.RaioYElipse = 30;
            this.fatimaCanoPoco7.Size = new System.Drawing.Size(47, 13);
            this.fatimaCanoPoco7.TabIndex = 119;
            this.fatimaCanoPoco7.vertical = false;
            // 
            // fatimaCanoPoco8
            // 
            this.fatimaCanoPoco8.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco8.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco8.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco8.diminuirbolha = 5D;
            this.fatimaCanoPoco8.enableTransparentBackground = true;
            this.fatimaCanoPoco8.fliped = false;
            this.fatimaCanoPoco8.fluxo = -1;
            this.fatimaCanoPoco8.Location = new System.Drawing.Point(185, 32);
            this.fatimaCanoPoco8.MaxVidro = 127;
            this.fatimaCanoPoco8.MinVidro = 8;
            this.fatimaCanoPoco8.Name = "fatimaCanoPoco8";
            this.fatimaCanoPoco8.RaioYElipse = 30;
            this.fatimaCanoPoco8.Size = new System.Drawing.Size(56, 13);
            this.fatimaCanoPoco8.TabIndex = 117;
            this.fatimaCanoPoco8.vertical = false;
            // 
            // fatimaPoco10
            // 
            this.fatimaPoco10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco10.LigarBomba = true;
            this.fatimaPoco10.Location = new System.Drawing.Point(1, 55);
            this.fatimaPoco10.Name = "fatimaPoco10";
            this.fatimaPoco10.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco10.TabIndex = 123;
            this.fatimaPoco10.Transparence = false;
            // 
            // fatimaPoco9
            // 
            this.fatimaPoco9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fatimaPoco9.LigarBomba = true;
            this.fatimaPoco9.Location = new System.Drawing.Point(73, 55);
            this.fatimaPoco9.Name = "fatimaPoco9";
            this.fatimaPoco9.Size = new System.Drawing.Size(66, 78);
            this.fatimaPoco9.TabIndex = 124;
            this.fatimaPoco9.Transparence = false;
            // 
            // fatimaCanoPoco10
            // 
            this.fatimaCanoPoco10.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco10.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco10.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco10.diminuirbolha = 5D;
            this.fatimaCanoPoco10.enableTransparentBackground = true;
            this.fatimaCanoPoco10.fliped = false;
            this.fatimaCanoPoco10.fluxo = -1;
            this.fatimaCanoPoco10.Location = new System.Drawing.Point(58, 32);
            this.fatimaCanoPoco10.MaxVidro = 127;
            this.fatimaCanoPoco10.MinVidro = 8;
            this.fatimaCanoPoco10.Name = "fatimaCanoPoco10";
            this.fatimaCanoPoco10.RaioYElipse = 30;
            this.fatimaCanoPoco10.Size = new System.Drawing.Size(43, 13);
            this.fatimaCanoPoco10.TabIndex = 113;
            this.fatimaCanoPoco10.vertical = false;
            // 
            // fatimaCanoPoco1
            // 
            this.fatimaCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPoco1.diminuirbolha = 5D;
            this.fatimaCanoPoco1.enableTransparentBackground = true;
            this.fatimaCanoPoco1.fliped = false;
            this.fatimaCanoPoco1.fluxo = -1;
            this.fatimaCanoPoco1.Location = new System.Drawing.Point(693, 32);
            this.fatimaCanoPoco1.MaxVidro = 127;
            this.fatimaCanoPoco1.MinVidro = 8;
            this.fatimaCanoPoco1.Name = "fatimaCanoPoco1";
            this.fatimaCanoPoco1.RaioYElipse = 30;
            this.fatimaCanoPoco1.Size = new System.Drawing.Size(36, 13);
            this.fatimaCanoPoco1.TabIndex = 144;
            this.fatimaCanoPoco1.vertical = false;
            // 
            // fatimaCano1Poco11
            // 
            this.fatimaCano1Poco11.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCano1Poco11.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCano1Poco11.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCano1Poco11.diminuirbolha = 5D;
            this.fatimaCano1Poco11.enableTransparentBackground = true;
            this.fatimaCano1Poco11.fliped = false;
            this.fatimaCano1Poco11.fluxo = -1;
            this.fatimaCano1Poco11.Location = new System.Drawing.Point(737, 55);
            this.fatimaCano1Poco11.MaxVidro = 127;
            this.fatimaCano1Poco11.MinVidro = 8;
            this.fatimaCano1Poco11.Name = "fatimaCano1Poco11";
            this.fatimaCano1Poco11.RaioYElipse = 30;
            this.fatimaCano1Poco11.Size = new System.Drawing.Size(13, 110);
            this.fatimaCano1Poco11.TabIndex = 187;
            this.fatimaCano1Poco11.vertical = true;
            // 
            // fatimaCanoPocos
            // 
            this.fatimaCanoPocos.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoPocos.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoPocos.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoPocos.diminuirbolha = 5D;
            this.fatimaCanoPocos.enableTransparentBackground = true;
            this.fatimaCanoPocos.fliped = false;
            this.fatimaCanoPocos.fluxo = -1;
            this.fatimaCanoPocos.Location = new System.Drawing.Point(758, 32);
            this.fatimaCanoPocos.MaxVidro = 127;
            this.fatimaCanoPocos.MinVidro = 8;
            this.fatimaCanoPocos.Name = "fatimaCanoPocos";
            this.fatimaCanoPocos.RaioYElipse = 30;
            this.fatimaCanoPocos.Size = new System.Drawing.Size(35, 13);
            this.fatimaCanoPocos.TabIndex = 189;
            this.fatimaCanoPocos.vertical = false;
            // 
            // junção94
            // 
            this.junção94.Flip = 90;
            this.junção94.Location = new System.Drawing.Point(758, 202);
            this.junção94.Margin = new System.Windows.Forms.Padding(0);
            this.junção94.Name = "junção94";
            this.junção94.Size = new System.Drawing.Size(30, 32);
            this.junção94.TabIndex = 229;
            // 
            // fatimaCanoBomba1
            // 
            this.fatimaCanoBomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoBomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoBomba1.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoBomba1.diminuirbolha = 5D;
            this.fatimaCanoBomba1.enableTransparentBackground = true;
            this.fatimaCanoBomba1.fliped = false;
            this.fatimaCanoBomba1.fluxo = -1;
            this.fatimaCanoBomba1.Location = new System.Drawing.Point(760, 225);
            this.fatimaCanoBomba1.MaxVidro = 127;
            this.fatimaCanoBomba1.MinVidro = 8;
            this.fatimaCanoBomba1.Name = "fatimaCanoBomba1";
            this.fatimaCanoBomba1.RaioYElipse = 30;
            this.fatimaCanoBomba1.Size = new System.Drawing.Size(13, 119);
            this.fatimaCanoBomba1.TabIndex = 226;
            this.fatimaCanoBomba1.vertical = true;
            // 
            // fatimaCanoBomba2
            // 
            this.fatimaCanoBomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoBomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoBomba2.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoBomba2.diminuirbolha = 5D;
            this.fatimaCanoBomba2.enableTransparentBackground = true;
            this.fatimaCanoBomba2.fliped = false;
            this.fatimaCanoBomba2.fluxo = -1;
            this.fatimaCanoBomba2.Location = new System.Drawing.Point(903, 248);
            this.fatimaCanoBomba2.MaxVidro = 127;
            this.fatimaCanoBomba2.MinVidro = 8;
            this.fatimaCanoBomba2.Name = "fatimaCanoBomba2";
            this.fatimaCanoBomba2.RaioYElipse = 30;
            this.fatimaCanoBomba2.Size = new System.Drawing.Size(13, 96);
            this.fatimaCanoBomba2.TabIndex = 239;
            this.fatimaCanoBomba2.vertical = true;
            // 
            // junção87
            // 
            this.junção87.Flip = 270;
            this.junção87.Location = new System.Drawing.Point(1036, 194);
            this.junção87.Margin = new System.Windows.Forms.Padding(0);
            this.junção87.Name = "junção87";
            this.junção87.Size = new System.Drawing.Size(29, 32);
            this.junção87.TabIndex = 248;
            // 
            // fatimaCanoBomba3
            // 
            this.fatimaCanoBomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoBomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoBomba3.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoBomba3.diminuirbolha = 5D;
            this.fatimaCanoBomba3.enableTransparentBackground = true;
            this.fatimaCanoBomba3.fliped = false;
            this.fatimaCanoBomba3.fluxo = 1;
            this.fatimaCanoBomba3.Location = new System.Drawing.Point(1050, 87);
            this.fatimaCanoBomba3.MaxVidro = 127;
            this.fatimaCanoBomba3.MinVidro = 8;
            this.fatimaCanoBomba3.Name = "fatimaCanoBomba3";
            this.fatimaCanoBomba3.RaioYElipse = 30;
            this.fatimaCanoBomba3.Size = new System.Drawing.Size(13, 114);
            this.fatimaCanoBomba3.TabIndex = 246;
            this.fatimaCanoBomba3.vertical = true;
            // 
            // fatimaCano3Bomba2
            // 
            this.fatimaCano3Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCano3Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCano3Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCano3Bomba2.diminuirbolha = 5D;
            this.fatimaCano3Bomba2.enableTransparentBackground = true;
            this.fatimaCano3Bomba2.fliped = false;
            this.fatimaCano3Bomba2.fluxo = 0;
            this.fatimaCano3Bomba2.Location = new System.Drawing.Point(400, 516);
            this.fatimaCano3Bomba2.MaxVidro = 127;
            this.fatimaCano3Bomba2.MinVidro = 8;
            this.fatimaCano3Bomba2.Name = "fatimaCano3Bomba2";
            this.fatimaCano3Bomba2.RaioYElipse = 30;
            this.fatimaCano3Bomba2.Size = new System.Drawing.Size(602, 13);
            this.fatimaCano3Bomba2.TabIndex = 254;
            this.fatimaCano3Bomba2.vertical = false;
            // 
            // junção98
            // 
            this.junção98.Flip = 90;
            this.junção98.Location = new System.Drawing.Point(1114, 260);
            this.junção98.Margin = new System.Windows.Forms.Padding(0);
            this.junção98.Name = "junção98";
            this.junção98.Size = new System.Drawing.Size(30, 32);
            this.junção98.TabIndex = 255;
            // 
            // fatimaCanoGoias
            // 
            this.fatimaCanoGoias.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCanoGoias.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCanoGoias.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCanoGoias.diminuirbolha = 5D;
            this.fatimaCanoGoias.enableTransparentBackground = true;
            this.fatimaCanoGoias.fliped = false;
            this.fatimaCanoGoias.fluxo = -1;
            this.fatimaCanoGoias.Location = new System.Drawing.Point(1116, 285);
            this.fatimaCanoGoias.MaxVidro = 127;
            this.fatimaCanoGoias.MinVidro = 8;
            this.fatimaCanoGoias.Name = "fatimaCanoGoias";
            this.fatimaCanoGoias.RaioYElipse = 30;
            this.fatimaCanoGoias.Size = new System.Drawing.Size(13, 189);
            this.fatimaCanoGoias.TabIndex = 249;
            this.fatimaCanoGoias.vertical = true;
            // 
            // fatimaCano1Bomba2
            // 
            this.fatimaCano1Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCano1Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCano1Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCano1Bomba2.diminuirbolha = 5D;
            this.fatimaCano1Bomba2.enableTransparentBackground = true;
            this.fatimaCano1Bomba2.fliped = false;
            this.fatimaCano1Bomba2.fluxo = 0;
            this.fatimaCano1Bomba2.Location = new System.Drawing.Point(962, 315);
            this.fatimaCano1Bomba2.MaxVidro = 127;
            this.fatimaCano1Bomba2.MinVidro = 8;
            this.fatimaCano1Bomba2.Name = "fatimaCano1Bomba2";
            this.fatimaCano1Bomba2.RaioYElipse = 30;
            this.fatimaCano1Bomba2.Size = new System.Drawing.Size(34, 13);
            this.fatimaCano1Bomba2.TabIndex = 243;
            this.fatimaCano1Bomba2.vertical = false;
            // 
            // fatimaCano1Bomba1
            // 
            this.fatimaCano1Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.fatimaCano1Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.fatimaCano1Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.fatimaCano1Bomba1.diminuirbolha = 5D;
            this.fatimaCano1Bomba1.enableTransparentBackground = true;
            this.fatimaCano1Bomba1.fliped = false;
            this.fatimaCano1Bomba1.fluxo = 0;
            this.fatimaCano1Bomba1.Location = new System.Drawing.Point(817, 315);
            this.fatimaCano1Bomba1.MaxVidro = 127;
            this.fatimaCano1Bomba1.MinVidro = 8;
            this.fatimaCano1Bomba1.Name = "fatimaCano1Bomba1";
            this.fatimaCano1Bomba1.RaioYElipse = 30;
            this.fatimaCano1Bomba1.Size = new System.Drawing.Size(43, 13);
            this.fatimaCano1Bomba1.TabIndex = 234;
            this.fatimaCano1Bomba1.vertical = false;
            // 
            // JardimBotanico
            // 
            this.JardimBotanico.Controls.Add(this.junção2);
            this.JardimBotanico.Controls.Add(this.jardimTanque1);
            this.JardimBotanico.Controls.Add(this.junção32);
            this.JardimBotanico.Controls.Add(this.junção29);
            this.JardimBotanico.Controls.Add(this.junção1);
            this.JardimBotanico.Controls.Add(this.junçãoT10);
            this.JardimBotanico.Controls.Add(this.jardimCano2Poco2);
            this.JardimBotanico.Controls.Add(this.jardimCano1Poco2);
            this.JardimBotanico.Controls.Add(this.junçãoT9);
            this.JardimBotanico.Controls.Add(this.junção28);
            this.JardimBotanico.Controls.Add(this.label4);
            this.JardimBotanico.Controls.Add(this.label3);
            this.JardimBotanico.Controls.Add(this.label2);
            this.JardimBotanico.Controls.Add(this.label1);
            this.JardimBotanico.Controls.Add(this.jardimCanoOut);
            this.JardimBotanico.Controls.Add(this.jardimCanoPoco);
            this.JardimBotanico.Controls.Add(this.jardimCanoPoco1);
            this.JardimBotanico.Controls.Add(this.jardimCanoPoco2);
            this.JardimBotanico.Controls.Add(this.jardimPoco1);
            this.JardimBotanico.Controls.Add(this.junção31);
            this.JardimBotanico.Controls.Add(this.jardimCano2Poco3);
            this.JardimBotanico.Controls.Add(this.jardimCano1Poco3);
            this.JardimBotanico.Controls.Add(this.jardimPoco2);
            this.JardimBotanico.Controls.Add(this.jardimPoco3);
            this.JardimBotanico.Location = new System.Drawing.Point(4, 22);
            this.JardimBotanico.Name = "JardimBotanico";
            this.JardimBotanico.Size = new System.Drawing.Size(1480, 662);
            this.JardimBotanico.TabIndex = 9;
            this.JardimBotanico.Text = "JARDIM BOTÂNICO";
            this.JardimBotanico.UseVisualStyleBackColor = true;
            // 
            // junção2
            // 
            this.junção2.Flip = 180;
            this.junção2.Location = new System.Drawing.Point(596, 62);
            this.junção2.Margin = new System.Windows.Forms.Padding(0);
            this.junção2.Name = "junção2";
            this.junção2.Size = new System.Drawing.Size(35, 35);
            this.junção2.TabIndex = 11;
            // 
            // jardimTanque1
            // 
            this.jardimTanque1.BackColor = System.Drawing.Color.Transparent;
            this.jardimTanque1.Cano = 2;
            this.jardimTanque1.CanoCheio = true;
            this.jardimTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimTanque1.CorVidro = System.Drawing.Color.Silver;
            this.jardimTanque1.enableTransparentBackground = true;
            this.jardimTanque1.LarguraCano = 13;
            this.jardimTanque1.Location = new System.Drawing.Point(599, 92);
            this.jardimTanque1.MaxVidro = 127;
            this.jardimTanque1.MinVidro = 8;
            this.jardimTanque1.Name = "jardimTanque1";
            this.jardimTanque1.PosCano = 15;
            this.jardimTanque1.RaioYElipse = 30;
            this.jardimTanque1.Size = new System.Drawing.Size(213, 372);
            this.jardimTanque1.TabIndex = 112;
            this.jardimTanque1.Value = 65D;
            // 
            // junção32
            // 
            this.junção32.Flip = 90;
            this.junção32.Location = new System.Drawing.Point(172, 385);
            this.junção32.Margin = new System.Windows.Forms.Padding(0);
            this.junção32.Name = "junção32";
            this.junção32.Size = new System.Drawing.Size(42, 35);
            this.junção32.TabIndex = 30;
            // 
            // junção29
            // 
            this.junção29.Flip = 90;
            this.junção29.Location = new System.Drawing.Point(173, 218);
            this.junção29.Margin = new System.Windows.Forms.Padding(0);
            this.junção29.Name = "junção29";
            this.junção29.Size = new System.Drawing.Size(42, 35);
            this.junção29.TabIndex = 16;
            // 
            // junção1
            // 
            this.junção1.Flip = 270;
            this.junção1.Location = new System.Drawing.Point(248, 203);
            this.junção1.Margin = new System.Windows.Forms.Padding(0);
            this.junção1.Name = "junção1";
            this.junção1.Size = new System.Drawing.Size(42, 35);
            this.junção1.TabIndex = 23;
            // 
            // junçãoT10
            // 
            this.junçãoT10.Flip = 180;
            this.junçãoT10.Location = new System.Drawing.Point(256, 56);
            this.junçãoT10.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT10.Name = "junçãoT10";
            this.junçãoT10.Size = new System.Drawing.Size(42, 44);
            this.junçãoT10.TabIndex = 25;
            // 
            // jardimCano2Poco2
            // 
            this.jardimCano2Poco2.BackColor = System.Drawing.Color.Gainsboro;
            this.jardimCano2Poco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimCano2Poco2.CorVidro = System.Drawing.Color.Silver;
            this.jardimCano2Poco2.diminuirbolha = 5D;
            this.jardimCano2Poco2.enableTransparentBackground = true;
            this.jardimCano2Poco2.fliped = false;
            this.jardimCano2Poco2.fluxo = -1;
            this.jardimCano2Poco2.Location = new System.Drawing.Point(204, 221);
            this.jardimCano2Poco2.MaxVidro = 127;
            this.jardimCano2Poco2.MinVidro = 8;
            this.jardimCano2Poco2.Name = "jardimCano2Poco2";
            this.jardimCano2Poco2.RaioYElipse = 30;
            this.jardimCano2Poco2.Size = new System.Drawing.Size(54, 13);
            this.jardimCano2Poco2.TabIndex = 24;
            this.jardimCano2Poco2.vertical = false;
            // 
            // jardimCano1Poco2
            // 
            this.jardimCano1Poco2.BackColor = System.Drawing.Color.Gainsboro;
            this.jardimCano1Poco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimCano1Poco2.CorVidro = System.Drawing.Color.Silver;
            this.jardimCano1Poco2.diminuirbolha = 5D;
            this.jardimCano1Poco2.enableTransparentBackground = true;
            this.jardimCano1Poco2.fliped = false;
            this.jardimCano1Poco2.fluxo = 1;
            this.jardimCano1Poco2.Location = new System.Drawing.Point(270, 84);
            this.jardimCano1Poco2.MaxVidro = 127;
            this.jardimCano1Poco2.MinVidro = 8;
            this.jardimCano1Poco2.Name = "jardimCano1Poco2";
            this.jardimCano1Poco2.RaioYElipse = 30;
            this.jardimCano1Poco2.Size = new System.Drawing.Size(13, 125);
            this.jardimCano1Poco2.TabIndex = 22;
            this.jardimCano1Poco2.vertical = true;
            // 
            // junçãoT9
            // 
            this.junçãoT9.Flip = 180;
            this.junçãoT9.Location = new System.Drawing.Point(346, 56);
            this.junçãoT9.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT9.Name = "junçãoT9";
            this.junçãoT9.Size = new System.Drawing.Size(42, 44);
            this.junçãoT9.TabIndex = 19;
            // 
            // junção28
            // 
            this.junção28.Flip = 90;
            this.junção28.Location = new System.Drawing.Point(173, 61);
            this.junção28.Margin = new System.Windows.Forms.Padding(0);
            this.junção28.Name = "junção28";
            this.junção28.Size = new System.Drawing.Size(42, 35);
            this.junção28.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.LightGray;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1082, 385);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(195, 94);
            this.label4.TabIndex = 10;
            this.label4.Text = "BAIRRO JARDIM BOTÂNICO";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(151, 492);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "POÇO 3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(151, 325);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "POÇO 2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "POÇO 1";
            // 
            // jardimCanoOut
            // 
            this.jardimCanoOut.BackColor = System.Drawing.Color.Gainsboro;
            this.jardimCanoOut.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimCanoOut.CorVidro = System.Drawing.Color.Silver;
            this.jardimCanoOut.diminuirbolha = 5D;
            this.jardimCanoOut.enableTransparentBackground = true;
            this.jardimCanoOut.fliped = false;
            this.jardimCanoOut.fluxo = -1;
            this.jardimCanoOut.Location = new System.Drawing.Point(788, 425);
            this.jardimCanoOut.MaxVidro = 127;
            this.jardimCanoOut.MinVidro = 8;
            this.jardimCanoOut.Name = "jardimCanoOut";
            this.jardimCanoOut.RaioYElipse = 30;
            this.jardimCanoOut.Size = new System.Drawing.Size(300, 13);
            this.jardimCanoOut.TabIndex = 6;
            this.jardimCanoOut.vertical = false;
            // 
            // jardimCanoPoco
            // 
            this.jardimCanoPoco.BackColor = System.Drawing.Color.Gainsboro;
            this.jardimCanoPoco.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimCanoPoco.CorVidro = System.Drawing.Color.Silver;
            this.jardimCanoPoco.diminuirbolha = 5D;
            this.jardimCanoPoco.enableTransparentBackground = true;
            this.jardimCanoPoco.fliped = false;
            this.jardimCanoPoco.fluxo = -1;
            this.jardimCanoPoco.Location = new System.Drawing.Point(381, 65);
            this.jardimCanoPoco.MaxVidro = 127;
            this.jardimCanoPoco.MinVidro = 8;
            this.jardimCanoPoco.Name = "jardimCanoPoco";
            this.jardimCanoPoco.RaioYElipse = 30;
            this.jardimCanoPoco.Size = new System.Drawing.Size(223, 13);
            this.jardimCanoPoco.TabIndex = 5;
            this.jardimCanoPoco.vertical = false;
            // 
            // jardimCanoPoco1
            // 
            this.jardimCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.jardimCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.jardimCanoPoco1.diminuirbolha = 5D;
            this.jardimCanoPoco1.enableTransparentBackground = true;
            this.jardimCanoPoco1.fliped = false;
            this.jardimCanoPoco1.fluxo = -1;
            this.jardimCanoPoco1.Location = new System.Drawing.Point(204, 64);
            this.jardimCanoPoco1.MaxVidro = 127;
            this.jardimCanoPoco1.MinVidro = 8;
            this.jardimCanoPoco1.Name = "jardimCanoPoco1";
            this.jardimCanoPoco1.RaioYElipse = 30;
            this.jardimCanoPoco1.Size = new System.Drawing.Size(54, 13);
            this.jardimCanoPoco1.TabIndex = 20;
            this.jardimCanoPoco1.vertical = false;
            // 
            // jardimCanoPoco2
            // 
            this.jardimCanoPoco2.BackColor = System.Drawing.Color.Gainsboro;
            this.jardimCanoPoco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimCanoPoco2.CorVidro = System.Drawing.Color.Silver;
            this.jardimCanoPoco2.diminuirbolha = 5D;
            this.jardimCanoPoco2.enableTransparentBackground = true;
            this.jardimCanoPoco2.fliped = false;
            this.jardimCanoPoco2.fluxo = -1;
            this.jardimCanoPoco2.Location = new System.Drawing.Point(295, 65);
            this.jardimCanoPoco2.MaxVidro = 127;
            this.jardimCanoPoco2.MinVidro = 8;
            this.jardimCanoPoco2.Name = "jardimCanoPoco2";
            this.jardimCanoPoco2.RaioYElipse = 30;
            this.jardimCanoPoco2.Size = new System.Drawing.Size(54, 13);
            this.jardimCanoPoco2.TabIndex = 21;
            this.jardimCanoPoco2.vertical = false;
            // 
            // jardimPoco1
            // 
            this.jardimPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.jardimPoco1.LigarBomba = true;
            this.jardimPoco1.Location = new System.Drawing.Point(148, 88);
            this.jardimPoco1.Name = "jardimPoco1";
            this.jardimPoco1.Size = new System.Drawing.Size(66, 78);
            this.jardimPoco1.TabIndex = 26;
            this.jardimPoco1.Transparence = false;
            // 
            // junção31
            // 
            this.junção31.Flip = 270;
            this.junção31.Location = new System.Drawing.Point(338, 370);
            this.junção31.Margin = new System.Windows.Forms.Padding(0);
            this.junção31.Name = "junção31";
            this.junção31.Size = new System.Drawing.Size(42, 35);
            this.junção31.TabIndex = 28;
            // 
            // jardimCano2Poco3
            // 
            this.jardimCano2Poco3.BackColor = System.Drawing.Color.Gainsboro;
            this.jardimCano2Poco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimCano2Poco3.CorVidro = System.Drawing.Color.Silver;
            this.jardimCano2Poco3.diminuirbolha = 5D;
            this.jardimCano2Poco3.enableTransparentBackground = true;
            this.jardimCano2Poco3.fliped = false;
            this.jardimCano2Poco3.fluxo = -1;
            this.jardimCano2Poco3.Location = new System.Drawing.Point(197, 388);
            this.jardimCano2Poco3.MaxVidro = 127;
            this.jardimCano2Poco3.MinVidro = 8;
            this.jardimCano2Poco3.Name = "jardimCano2Poco3";
            this.jardimCano2Poco3.RaioYElipse = 30;
            this.jardimCano2Poco3.Size = new System.Drawing.Size(151, 13);
            this.jardimCano2Poco3.TabIndex = 29;
            this.jardimCano2Poco3.vertical = false;
            // 
            // jardimCano1Poco3
            // 
            this.jardimCano1Poco3.BackColor = System.Drawing.Color.Gainsboro;
            this.jardimCano1Poco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.jardimCano1Poco3.CorVidro = System.Drawing.Color.Silver;
            this.jardimCano1Poco3.diminuirbolha = 5D;
            this.jardimCano1Poco3.enableTransparentBackground = true;
            this.jardimCano1Poco3.fliped = false;
            this.jardimCano1Poco3.fluxo = 1;
            this.jardimCano1Poco3.Location = new System.Drawing.Point(360, 84);
            this.jardimCano1Poco3.MaxVidro = 127;
            this.jardimCano1Poco3.MinVidro = 8;
            this.jardimCano1Poco3.Name = "jardimCano1Poco3";
            this.jardimCano1Poco3.RaioYElipse = 30;
            this.jardimCano1Poco3.Size = new System.Drawing.Size(13, 292);
            this.jardimCano1Poco3.TabIndex = 27;
            this.jardimCano1Poco3.vertical = true;
            // 
            // jardimPoco2
            // 
            this.jardimPoco2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.jardimPoco2.LigarBomba = true;
            this.jardimPoco2.Location = new System.Drawing.Point(148, 244);
            this.jardimPoco2.Name = "jardimPoco2";
            this.jardimPoco2.Size = new System.Drawing.Size(66, 78);
            this.jardimPoco2.TabIndex = 32;
            this.jardimPoco2.Transparence = false;
            // 
            // jardimPoco3
            // 
            this.jardimPoco3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.jardimPoco3.LigarBomba = true;
            this.jardimPoco3.Location = new System.Drawing.Point(147, 411);
            this.jardimPoco3.Name = "jardimPoco3";
            this.jardimPoco3.Size = new System.Drawing.Size(66, 78);
            this.jardimPoco3.TabIndex = 33;
            this.jardimPoco3.Transparence = false;
            // 
            // Gutierrez
            // 
            this.Gutierrez.Controls.Add(this.gutierrezVazaoBomba2);
            this.Gutierrez.Controls.Add(this.waterMeter12);
            this.Gutierrez.Controls.Add(this.gutierrezVazaoBomba1);
            this.Gutierrez.Controls.Add(this.waterMeter11);
            this.Gutierrez.Controls.Add(this.junção130);
            this.Gutierrez.Controls.Add(this.junção73);
            this.Gutierrez.Controls.Add(this.label57);
            this.Gutierrez.Controls.Add(this.junção74);
            this.Gutierrez.Controls.Add(this.label58);
            this.Gutierrez.Controls.Add(this.junção75);
            this.Gutierrez.Controls.Add(this.junção76);
            this.Gutierrez.Controls.Add(this.junção77);
            this.Gutierrez.Controls.Add(this.gutierrezBomba2);
            this.Gutierrez.Controls.Add(this.gutierrezCano2Bomba2);
            this.Gutierrez.Controls.Add(this.junção78);
            this.Gutierrez.Controls.Add(this.gutierrezCano3Bomba1);
            this.Gutierrez.Controls.Add(this.junção79);
            this.Gutierrez.Controls.Add(this.junção80);
            this.Gutierrez.Controls.Add(this.gutierrezBomba1);
            this.Gutierrez.Controls.Add(this.gutierrezCano2Bomba1);
            this.Gutierrez.Controls.Add(this.gutierrezCanoBomba1);
            this.Gutierrez.Controls.Add(this.gutierrezCano1Bomba2);
            this.Gutierrez.Controls.Add(this.junçãoT28);
            this.Gutierrez.Controls.Add(this.gutierrezPoco6);
            this.Gutierrez.Controls.Add(this.label56);
            this.Gutierrez.Controls.Add(this.junção71);
            this.Gutierrez.Controls.Add(this.junçãoT34);
            this.Gutierrez.Controls.Add(this.junçãoT33);
            this.Gutierrez.Controls.Add(this.gutierrezPoco7);
            this.Gutierrez.Controls.Add(this.gutierrezCanoPoco8);
            this.Gutierrez.Controls.Add(this.gutierrezCanoPoco6);
            this.Gutierrez.Controls.Add(this.label55);
            this.Gutierrez.Controls.Add(this.junçãoT29);
            this.Gutierrez.Controls.Add(this.junçãoT30);
            this.Gutierrez.Controls.Add(this.junçãoT31);
            this.Gutierrez.Controls.Add(this.junçãoT32);
            this.Gutierrez.Controls.Add(this.gutierrezCanoPoco4);
            this.Gutierrez.Controls.Add(this.gutierrezPoco1);
            this.Gutierrez.Controls.Add(this.gutierrezPoco2);
            this.Gutierrez.Controls.Add(this.gutierrezPoco3);
            this.Gutierrez.Controls.Add(this.gutierrezCanoPoco1);
            this.Gutierrez.Controls.Add(this.gutierrezCanoPoco2);
            this.Gutierrez.Controls.Add(this.gutierrezCanoPoco3);
            this.Gutierrez.Controls.Add(this.junção72);
            this.Gutierrez.Controls.Add(this.label49);
            this.Gutierrez.Controls.Add(this.label50);
            this.Gutierrez.Controls.Add(this.label51);
            this.Gutierrez.Controls.Add(this.label52);
            this.Gutierrez.Controls.Add(this.label53);
            this.Gutierrez.Controls.Add(this.label54);
            this.Gutierrez.Controls.Add(this.gutierrezTanque1);
            this.Gutierrez.Controls.Add(this.gutierrezPoco5);
            this.Gutierrez.Controls.Add(this.gutierrezPoco4);
            this.Gutierrez.Controls.Add(this.gutierrezCanoPoco5);
            this.Gutierrez.Controls.Add(this.gutierrezCanoPoco7);
            this.Gutierrez.Controls.Add(this.gutierrezCanoBomba2);
            this.Gutierrez.Controls.Add(this.gutierrezCano1Bomba1);
            this.Gutierrez.Controls.Add(this.gutierrezCano3Bomba2);
            this.Gutierrez.Controls.Add(this.gutierrezPoco8);
            this.Gutierrez.Location = new System.Drawing.Point(4, 22);
            this.Gutierrez.Name = "Gutierrez";
            this.Gutierrez.Size = new System.Drawing.Size(1480, 662);
            this.Gutierrez.TabIndex = 8;
            this.Gutierrez.Text = "GUTIERREZ";
            this.Gutierrez.UseVisualStyleBackColor = true;
            // 
            // gutierrezVazaoBomba2
            // 
            this.gutierrezVazaoBomba2.AutoSize = true;
            this.gutierrezVazaoBomba2.BackColor = System.Drawing.Color.Black;
            this.gutierrezVazaoBomba2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gutierrezVazaoBomba2.ForeColor = System.Drawing.Color.Transparent;
            this.gutierrezVazaoBomba2.Location = new System.Drawing.Point(961, 477);
            this.gutierrezVazaoBomba2.Name = "gutierrezVazaoBomba2";
            this.gutierrezVazaoBomba2.Size = new System.Drawing.Size(76, 16);
            this.gutierrezVazaoBomba2.TabIndex = 291;
            this.gutierrezVazaoBomba2.Text = "00.00 (m³/h)";
            // 
            // waterMeter12
            // 
            this.waterMeter12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter12.Location = new System.Drawing.Point(973, 418);
            this.waterMeter12.Name = "waterMeter12";
            this.waterMeter12.Size = new System.Drawing.Size(55, 55);
            this.waterMeter12.TabIndex = 290;
            this.waterMeter12.Transparence = false;
            // 
            // gutierrezVazaoBomba1
            // 
            this.gutierrezVazaoBomba1.AutoSize = true;
            this.gutierrezVazaoBomba1.BackColor = System.Drawing.Color.Black;
            this.gutierrezVazaoBomba1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gutierrezVazaoBomba1.ForeColor = System.Drawing.Color.Transparent;
            this.gutierrezVazaoBomba1.Location = new System.Drawing.Point(231, 477);
            this.gutierrezVazaoBomba1.Name = "gutierrezVazaoBomba1";
            this.gutierrezVazaoBomba1.Size = new System.Drawing.Size(76, 16);
            this.gutierrezVazaoBomba1.TabIndex = 289;
            this.gutierrezVazaoBomba1.Text = "00.00 (m³/h)";
            // 
            // waterMeter11
            // 
            this.waterMeter11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter11.Location = new System.Drawing.Point(243, 418);
            this.waterMeter11.Name = "waterMeter11";
            this.waterMeter11.Size = new System.Drawing.Size(55, 55);
            this.waterMeter11.TabIndex = 288;
            this.waterMeter11.Transparence = false;
            // 
            // junção130
            // 
            this.junção130.Flip = 0;
            this.junção130.Location = new System.Drawing.Point(766, 435);
            this.junção130.Margin = new System.Windows.Forms.Padding(0);
            this.junção130.Name = "junção130";
            this.junção130.Size = new System.Drawing.Size(31, 32);
            this.junção130.TabIndex = 261;
            // 
            // junção73
            // 
            this.junção73.Flip = 90;
            this.junção73.Location = new System.Drawing.Point(486, 232);
            this.junção73.Margin = new System.Windows.Forms.Padding(0);
            this.junção73.Name = "junção73";
            this.junção73.Size = new System.Drawing.Size(30, 32);
            this.junção73.TabIndex = 259;
            // 
            // label57
            // 
            this.label57.BackColor = System.Drawing.Color.Gainsboro;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(63, 416);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(142, 81);
            this.label57.TabIndex = 242;
            this.label57.Text = "GUTIERREZ, AEROPORTO, AMORIN";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção74
            // 
            this.junção74.Flip = 180;
            this.junção74.Location = new System.Drawing.Point(578, 349);
            this.junção74.Margin = new System.Windows.Forms.Padding(0);
            this.junção74.Name = "junção74";
            this.junção74.Size = new System.Drawing.Size(29, 32);
            this.junção74.TabIndex = 249;
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.Color.Gainsboro;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(1046, 416);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(171, 84);
            this.label58.TabIndex = 241;
            this.label58.Text = "EDUARDO MOREIRA, MILLENIUM E PARTE DO FRAMBOIAN";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção75
            // 
            this.junção75.Flip = 180;
            this.junção75.Location = new System.Drawing.Point(754, 349);
            this.junção75.Margin = new System.Windows.Forms.Padding(0);
            this.junção75.Name = "junção75";
            this.junção75.Size = new System.Drawing.Size(29, 32);
            this.junção75.TabIndex = 256;
            // 
            // junção76
            // 
            this.junção76.Flip = 90;
            this.junção76.Location = new System.Drawing.Point(665, 349);
            this.junção76.Margin = new System.Windows.Forms.Padding(0);
            this.junção76.Name = "junção76";
            this.junção76.Size = new System.Drawing.Size(30, 32);
            this.junção76.TabIndex = 255;
            // 
            // junção77
            // 
            this.junção77.Flip = 0;
            this.junção77.Location = new System.Drawing.Point(628, 373);
            this.junção77.Margin = new System.Windows.Forms.Padding(0);
            this.junção77.Name = "junção77";
            this.junção77.Size = new System.Drawing.Size(31, 32);
            this.junção77.TabIndex = 253;
            // 
            // gutierrezBomba2
            // 
            this.gutierrezBomba2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezBomba2.LigarBomba = false;
            this.gutierrezBomba2.Location = new System.Drawing.Point(657, 373);
            this.gutierrezBomba2.Name = "gutierrezBomba2";
            this.gutierrezBomba2.Size = new System.Drawing.Size(60, 50);
            this.gutierrezBomba2.TabIndex = 252;
            this.gutierrezBomba2.Transparence = false;
            // 
            // gutierrezCano2Bomba2
            // 
            this.gutierrezCano2Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCano2Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCano2Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCano2Bomba2.diminuirbolha = 5D;
            this.gutierrezCano2Bomba2.enableTransparentBackground = true;
            this.gutierrezCano2Bomba2.fliped = false;
            this.gutierrezCano2Bomba2.fluxo = 0;
            this.gutierrezCano2Bomba2.Location = new System.Drawing.Point(768, 373);
            this.gutierrezCano2Bomba2.MaxVidro = 127;
            this.gutierrezCano2Bomba2.MinVidro = 8;
            this.gutierrezCano2Bomba2.Name = "gutierrezCano2Bomba2";
            this.gutierrezCano2Bomba2.RaioYElipse = 30;
            this.gutierrezCano2Bomba2.Size = new System.Drawing.Size(13, 72);
            this.gutierrezCano2Bomba2.TabIndex = 257;
            this.gutierrezCano2Bomba2.vertical = true;
            // 
            // junção78
            // 
            this.junção78.Flip = 270;
            this.junção78.Location = new System.Drawing.Point(578, 435);
            this.junção78.Margin = new System.Windows.Forms.Padding(0);
            this.junção78.Name = "junção78";
            this.junção78.Size = new System.Drawing.Size(29, 32);
            this.junção78.TabIndex = 244;
            // 
            // gutierrezCano3Bomba1
            // 
            this.gutierrezCano3Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCano3Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCano3Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCano3Bomba1.diminuirbolha = 5D;
            this.gutierrezCano3Bomba1.enableTransparentBackground = true;
            this.gutierrezCano3Bomba1.fliped = false;
            this.gutierrezCano3Bomba1.fluxo = 0;
            this.gutierrezCano3Bomba1.Location = new System.Drawing.Point(194, 452);
            this.gutierrezCano3Bomba1.MaxVidro = 127;
            this.gutierrezCano3Bomba1.MinVidro = 8;
            this.gutierrezCano3Bomba1.Name = "gutierrezCano3Bomba1";
            this.gutierrezCano3Bomba1.RaioYElipse = 30;
            this.gutierrezCano3Bomba1.Size = new System.Drawing.Size(394, 13);
            this.gutierrezCano3Bomba1.TabIndex = 243;
            this.gutierrezCano3Bomba1.vertical = false;
            // 
            // junção79
            // 
            this.junção79.Flip = 90;
            this.junção79.Location = new System.Drawing.Point(523, 349);
            this.junção79.Margin = new System.Windows.Forms.Padding(0);
            this.junção79.Name = "junção79";
            this.junção79.Size = new System.Drawing.Size(30, 32);
            this.junção79.TabIndex = 248;
            // 
            // junção80
            // 
            this.junção80.Flip = 0;
            this.junção80.Location = new System.Drawing.Point(486, 373);
            this.junção80.Margin = new System.Windows.Forms.Padding(0);
            this.junção80.Name = "junção80";
            this.junção80.Size = new System.Drawing.Size(31, 32);
            this.junção80.TabIndex = 246;
            // 
            // gutierrezBomba1
            // 
            this.gutierrezBomba1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezBomba1.LigarBomba = false;
            this.gutierrezBomba1.Location = new System.Drawing.Point(515, 373);
            this.gutierrezBomba1.Name = "gutierrezBomba1";
            this.gutierrezBomba1.Size = new System.Drawing.Size(60, 50);
            this.gutierrezBomba1.TabIndex = 245;
            this.gutierrezBomba1.Transparence = false;
            // 
            // gutierrezCano2Bomba1
            // 
            this.gutierrezCano2Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCano2Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCano2Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCano2Bomba1.diminuirbolha = 5D;
            this.gutierrezCano2Bomba1.enableTransparentBackground = true;
            this.gutierrezCano2Bomba1.fliped = false;
            this.gutierrezCano2Bomba1.fluxo = 0;
            this.gutierrezCano2Bomba1.Location = new System.Drawing.Point(592, 358);
            this.gutierrezCano2Bomba1.MaxVidro = 127;
            this.gutierrezCano2Bomba1.MinVidro = 8;
            this.gutierrezCano2Bomba1.Name = "gutierrezCano2Bomba1";
            this.gutierrezCano2Bomba1.RaioYElipse = 30;
            this.gutierrezCano2Bomba1.Size = new System.Drawing.Size(13, 87);
            this.gutierrezCano2Bomba1.TabIndex = 250;
            this.gutierrezCano2Bomba1.vertical = true;
            // 
            // gutierrezCanoBomba1
            // 
            this.gutierrezCanoBomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoBomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoBomba1.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoBomba1.diminuirbolha = 5D;
            this.gutierrezCanoBomba1.enableTransparentBackground = true;
            this.gutierrezCanoBomba1.fliped = false;
            this.gutierrezCanoBomba1.fluxo = -1;
            this.gutierrezCanoBomba1.Location = new System.Drawing.Point(488, 254);
            this.gutierrezCanoBomba1.MaxVidro = 127;
            this.gutierrezCanoBomba1.MinVidro = 8;
            this.gutierrezCanoBomba1.Name = "gutierrezCanoBomba1";
            this.gutierrezCanoBomba1.RaioYElipse = 30;
            this.gutierrezCanoBomba1.Size = new System.Drawing.Size(13, 127);
            this.gutierrezCanoBomba1.TabIndex = 247;
            this.gutierrezCanoBomba1.vertical = true;
            // 
            // gutierrezCano1Bomba2
            // 
            this.gutierrezCano1Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCano1Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCano1Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCano1Bomba2.diminuirbolha = 5D;
            this.gutierrezCano1Bomba2.enableTransparentBackground = true;
            this.gutierrezCano1Bomba2.fliped = false;
            this.gutierrezCano1Bomba2.fluxo = 0;
            this.gutierrezCano1Bomba2.Location = new System.Drawing.Point(693, 353);
            this.gutierrezCano1Bomba2.MaxVidro = 127;
            this.gutierrezCano1Bomba2.MinVidro = 8;
            this.gutierrezCano1Bomba2.Name = "gutierrezCano1Bomba2";
            this.gutierrezCano1Bomba2.RaioYElipse = 30;
            this.gutierrezCano1Bomba2.Size = new System.Drawing.Size(69, 13);
            this.gutierrezCano1Bomba2.TabIndex = 258;
            this.gutierrezCano1Bomba2.vertical = false;
            // 
            // junçãoT28
            // 
            this.junçãoT28.Flip = 180;
            this.junçãoT28.Location = new System.Drawing.Point(812, 28);
            this.junçãoT28.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT28.Name = "junçãoT28";
            this.junçãoT28.Size = new System.Drawing.Size(42, 44);
            this.junçãoT28.TabIndex = 111;
            // 
            // gutierrezPoco6
            // 
            this.gutierrezPoco6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezPoco6.LigarBomba = true;
            this.gutierrezPoco6.Location = new System.Drawing.Point(795, 59);
            this.gutierrezPoco6.Name = "gutierrezPoco6";
            this.gutierrezPoco6.Size = new System.Drawing.Size(66, 78);
            this.gutierrezPoco6.TabIndex = 113;
            this.gutierrezPoco6.Transparence = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(802, 140);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(62, 16);
            this.label56.TabIndex = 110;
            this.label56.Text = "POÇO 6";
            // 
            // junção71
            // 
            this.junção71.Flip = 180;
            this.junção71.Location = new System.Drawing.Point(956, 34);
            this.junção71.Margin = new System.Windows.Forms.Padding(0);
            this.junção71.Name = "junção71";
            this.junção71.Size = new System.Drawing.Size(30, 30);
            this.junção71.TabIndex = 87;
            // 
            // junçãoT34
            // 
            this.junçãoT34.Flip = 180;
            this.junçãoT34.Location = new System.Drawing.Point(514, 27);
            this.junçãoT34.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT34.Name = "junçãoT34";
            this.junçãoT34.Size = new System.Drawing.Size(42, 44);
            this.junçãoT34.TabIndex = 108;
            // 
            // junçãoT33
            // 
            this.junçãoT33.Flip = 180;
            this.junçãoT33.Location = new System.Drawing.Point(884, 28);
            this.junçãoT33.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT33.Name = "junçãoT33";
            this.junçãoT33.Size = new System.Drawing.Size(42, 44);
            this.junçãoT33.TabIndex = 104;
            // 
            // gutierrezPoco7
            // 
            this.gutierrezPoco7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezPoco7.LigarBomba = true;
            this.gutierrezPoco7.Location = new System.Drawing.Point(867, 59);
            this.gutierrezPoco7.Name = "gutierrezPoco7";
            this.gutierrezPoco7.Size = new System.Drawing.Size(66, 78);
            this.gutierrezPoco7.TabIndex = 107;
            this.gutierrezPoco7.Transparence = false;
            // 
            // gutierrezCanoPoco8
            // 
            this.gutierrezCanoPoco8.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoPoco8.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoPoco8.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoPoco8.diminuirbolha = 5D;
            this.gutierrezCanoPoco8.enableTransparentBackground = true;
            this.gutierrezCanoPoco8.fliped = false;
            this.gutierrezCanoPoco8.fluxo = 1;
            this.gutierrezCanoPoco8.Location = new System.Drawing.Point(915, 37);
            this.gutierrezCanoPoco8.MaxVidro = 127;
            this.gutierrezCanoPoco8.MinVidro = 8;
            this.gutierrezCanoPoco8.Name = "gutierrezCanoPoco8";
            this.gutierrezCanoPoco8.RaioYElipse = 30;
            this.gutierrezCanoPoco8.Size = new System.Drawing.Size(52, 13);
            this.gutierrezCanoPoco8.TabIndex = 106;
            this.gutierrezCanoPoco8.vertical = false;
            // 
            // gutierrezCanoPoco6
            // 
            this.gutierrezCanoPoco6.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoPoco6.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoPoco6.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoPoco6.diminuirbolha = 5D;
            this.gutierrezCanoPoco6.enableTransparentBackground = true;
            this.gutierrezCanoPoco6.fliped = false;
            this.gutierrezCanoPoco6.fluxo = 1;
            this.gutierrezCanoPoco6.Location = new System.Drawing.Point(547, 37);
            this.gutierrezCanoPoco6.MaxVidro = 127;
            this.gutierrezCanoPoco6.MinVidro = 8;
            this.gutierrezCanoPoco6.Name = "gutierrezCanoPoco6";
            this.gutierrezCanoPoco6.RaioYElipse = 30;
            this.gutierrezCanoPoco6.Size = new System.Drawing.Size(289, 13);
            this.gutierrezCanoPoco6.TabIndex = 105;
            this.gutierrezCanoPoco6.vertical = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(874, 140);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(62, 16);
            this.label55.TabIndex = 103;
            this.label55.Text = "POÇO 7";
            // 
            // junçãoT29
            // 
            this.junçãoT29.Flip = 180;
            this.junçãoT29.Location = new System.Drawing.Point(316, 27);
            this.junçãoT29.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT29.Name = "junçãoT29";
            this.junçãoT29.Size = new System.Drawing.Size(42, 44);
            this.junçãoT29.TabIndex = 92;
            // 
            // junçãoT30
            // 
            this.junçãoT30.Flip = 180;
            this.junçãoT30.Location = new System.Drawing.Point(244, 27);
            this.junçãoT30.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT30.Name = "junçãoT30";
            this.junçãoT30.Size = new System.Drawing.Size(42, 44);
            this.junçãoT30.TabIndex = 90;
            // 
            // junçãoT31
            // 
            this.junçãoT31.Flip = 180;
            this.junçãoT31.Location = new System.Drawing.Point(174, 27);
            this.junçãoT31.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT31.Name = "junçãoT31";
            this.junçãoT31.Size = new System.Drawing.Size(42, 44);
            this.junçãoT31.TabIndex = 88;
            // 
            // junçãoT32
            // 
            this.junçãoT32.Flip = 180;
            this.junçãoT32.Location = new System.Drawing.Point(104, 27);
            this.junçãoT32.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT32.Name = "junçãoT32";
            this.junçãoT32.Size = new System.Drawing.Size(42, 44);
            this.junçãoT32.TabIndex = 85;
            // 
            // gutierrezCanoPoco4
            // 
            this.gutierrezCanoPoco4.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoPoco4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoPoco4.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoPoco4.diminuirbolha = 5D;
            this.gutierrezCanoPoco4.enableTransparentBackground = true;
            this.gutierrezCanoPoco4.fliped = false;
            this.gutierrezCanoPoco4.fluxo = -1;
            this.gutierrezCanoPoco4.Location = new System.Drawing.Point(137, 36);
            this.gutierrezCanoPoco4.MaxVidro = 127;
            this.gutierrezCanoPoco4.MinVidro = 8;
            this.gutierrezCanoPoco4.Name = "gutierrezCanoPoco4";
            this.gutierrezCanoPoco4.RaioYElipse = 30;
            this.gutierrezCanoPoco4.Size = new System.Drawing.Size(37, 13);
            this.gutierrezCanoPoco4.TabIndex = 89;
            this.gutierrezCanoPoco4.vertical = false;
            // 
            // gutierrezPoco1
            // 
            this.gutierrezPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezPoco1.LigarBomba = true;
            this.gutierrezPoco1.Location = new System.Drawing.Point(298, 58);
            this.gutierrezPoco1.Name = "gutierrezPoco1";
            this.gutierrezPoco1.Size = new System.Drawing.Size(66, 78);
            this.gutierrezPoco1.TabIndex = 101;
            this.gutierrezPoco1.Transparence = false;
            // 
            // gutierrezPoco2
            // 
            this.gutierrezPoco2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezPoco2.LigarBomba = true;
            this.gutierrezPoco2.Location = new System.Drawing.Point(226, 58);
            this.gutierrezPoco2.Name = "gutierrezPoco2";
            this.gutierrezPoco2.Size = new System.Drawing.Size(66, 78);
            this.gutierrezPoco2.TabIndex = 100;
            this.gutierrezPoco2.Transparence = false;
            // 
            // gutierrezPoco3
            // 
            this.gutierrezPoco3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezPoco3.LigarBomba = true;
            this.gutierrezPoco3.Location = new System.Drawing.Point(156, 58);
            this.gutierrezPoco3.Name = "gutierrezPoco3";
            this.gutierrezPoco3.Size = new System.Drawing.Size(66, 78);
            this.gutierrezPoco3.TabIndex = 99;
            this.gutierrezPoco3.Transparence = false;
            // 
            // gutierrezCanoPoco1
            // 
            this.gutierrezCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoPoco1.diminuirbolha = 5D;
            this.gutierrezCanoPoco1.enableTransparentBackground = true;
            this.gutierrezCanoPoco1.fliped = false;
            this.gutierrezCanoPoco1.fluxo = -1;
            this.gutierrezCanoPoco1.Location = new System.Drawing.Point(351, 36);
            this.gutierrezCanoPoco1.MaxVidro = 127;
            this.gutierrezCanoPoco1.MinVidro = 8;
            this.gutierrezCanoPoco1.Name = "gutierrezCanoPoco1";
            this.gutierrezCanoPoco1.RaioYElipse = 30;
            this.gutierrezCanoPoco1.Size = new System.Drawing.Size(175, 13);
            this.gutierrezCanoPoco1.TabIndex = 95;
            this.gutierrezCanoPoco1.vertical = false;
            // 
            // gutierrezCanoPoco2
            // 
            this.gutierrezCanoPoco2.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoPoco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoPoco2.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoPoco2.diminuirbolha = 5D;
            this.gutierrezCanoPoco2.enableTransparentBackground = true;
            this.gutierrezCanoPoco2.fliped = false;
            this.gutierrezCanoPoco2.fluxo = -1;
            this.gutierrezCanoPoco2.Location = new System.Drawing.Point(280, 36);
            this.gutierrezCanoPoco2.MaxVidro = 127;
            this.gutierrezCanoPoco2.MinVidro = 8;
            this.gutierrezCanoPoco2.Name = "gutierrezCanoPoco2";
            this.gutierrezCanoPoco2.RaioYElipse = 30;
            this.gutierrezCanoPoco2.Size = new System.Drawing.Size(47, 13);
            this.gutierrezCanoPoco2.TabIndex = 93;
            this.gutierrezCanoPoco2.vertical = false;
            // 
            // gutierrezCanoPoco3
            // 
            this.gutierrezCanoPoco3.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoPoco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoPoco3.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoPoco3.diminuirbolha = 5D;
            this.gutierrezCanoPoco3.enableTransparentBackground = true;
            this.gutierrezCanoPoco3.fliped = false;
            this.gutierrezCanoPoco3.fluxo = -1;
            this.gutierrezCanoPoco3.Location = new System.Drawing.Point(206, 36);
            this.gutierrezCanoPoco3.MaxVidro = 127;
            this.gutierrezCanoPoco3.MinVidro = 8;
            this.gutierrezCanoPoco3.Name = "gutierrezCanoPoco3";
            this.gutierrezCanoPoco3.RaioYElipse = 30;
            this.gutierrezCanoPoco3.Size = new System.Drawing.Size(48, 13);
            this.gutierrezCanoPoco3.TabIndex = 91;
            this.gutierrezCanoPoco3.vertical = false;
            // 
            // junção72
            // 
            this.junção72.Flip = 90;
            this.junção72.Location = new System.Drawing.Point(44, 33);
            this.junção72.Margin = new System.Windows.Forms.Padding(0);
            this.junção72.Name = "junção72";
            this.junção72.Size = new System.Drawing.Size(35, 35);
            this.junção72.TabIndex = 84;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(943, 140);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(62, 16);
            this.label49.TabIndex = 83;
            this.label49.Text = "POÇO 8";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(305, 139);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(62, 16);
            this.label50.TabIndex = 82;
            this.label50.Text = "POÇO 1";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(233, 139);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(62, 16);
            this.label51.TabIndex = 81;
            this.label51.Text = "POÇO 2";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(163, 139);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(62, 16);
            this.label52.TabIndex = 80;
            this.label52.Text = "POÇO 3";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(93, 139);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(62, 16);
            this.label53.TabIndex = 79;
            this.label53.Text = "POÇO 4";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(21, 139);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(62, 16);
            this.label54.TabIndex = 78;
            this.label54.Text = "POÇO 5";
            // 
            // gutierrezTanque1
            // 
            this.gutierrezTanque1.BackColor = System.Drawing.Color.Transparent;
            this.gutierrezTanque1.Cano = 2;
            this.gutierrezTanque1.CanoCheio = true;
            this.gutierrezTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezTanque1.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezTanque1.enableTransparentBackground = true;
            this.gutierrezTanque1.LarguraCano = 13;
            this.gutierrezTanque1.Location = new System.Drawing.Point(514, 62);
            this.gutierrezTanque1.MaxVidro = 127;
            this.gutierrezTanque1.MinVidro = 8;
            this.gutierrezTanque1.Name = "gutierrezTanque1";
            this.gutierrezTanque1.PosCano = 15;
            this.gutierrezTanque1.RaioYElipse = 30;
            this.gutierrezTanque1.Size = new System.Drawing.Size(244, 217);
            this.gutierrezTanque1.TabIndex = 77;
            this.gutierrezTanque1.Value = 40D;
            // 
            // gutierrezPoco5
            // 
            this.gutierrezPoco5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezPoco5.LigarBomba = true;
            this.gutierrezPoco5.Location = new System.Drawing.Point(14, 59);
            this.gutierrezPoco5.Name = "gutierrezPoco5";
            this.gutierrezPoco5.Size = new System.Drawing.Size(66, 78);
            this.gutierrezPoco5.TabIndex = 97;
            this.gutierrezPoco5.Transparence = false;
            // 
            // gutierrezPoco4
            // 
            this.gutierrezPoco4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezPoco4.LigarBomba = true;
            this.gutierrezPoco4.Location = new System.Drawing.Point(86, 58);
            this.gutierrezPoco4.Name = "gutierrezPoco4";
            this.gutierrezPoco4.Size = new System.Drawing.Size(66, 78);
            this.gutierrezPoco4.TabIndex = 98;
            this.gutierrezPoco4.Transparence = false;
            // 
            // gutierrezCanoPoco5
            // 
            this.gutierrezCanoPoco5.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoPoco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoPoco5.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoPoco5.diminuirbolha = 5D;
            this.gutierrezCanoPoco5.enableTransparentBackground = true;
            this.gutierrezCanoPoco5.fliped = false;
            this.gutierrezCanoPoco5.fluxo = -1;
            this.gutierrezCanoPoco5.Location = new System.Drawing.Point(71, 36);
            this.gutierrezCanoPoco5.MaxVidro = 127;
            this.gutierrezCanoPoco5.MinVidro = 8;
            this.gutierrezCanoPoco5.Name = "gutierrezCanoPoco5";
            this.gutierrezCanoPoco5.RaioYElipse = 30;
            this.gutierrezCanoPoco5.Size = new System.Drawing.Size(39, 13);
            this.gutierrezCanoPoco5.TabIndex = 86;
            this.gutierrezCanoPoco5.vertical = false;
            // 
            // gutierrezCanoPoco7
            // 
            this.gutierrezCanoPoco7.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoPoco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoPoco7.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoPoco7.diminuirbolha = 5D;
            this.gutierrezCanoPoco7.enableTransparentBackground = true;
            this.gutierrezCanoPoco7.fliped = false;
            this.gutierrezCanoPoco7.fluxo = 1;
            this.gutierrezCanoPoco7.Location = new System.Drawing.Point(845, 37);
            this.gutierrezCanoPoco7.MaxVidro = 127;
            this.gutierrezCanoPoco7.MinVidro = 8;
            this.gutierrezCanoPoco7.Name = "gutierrezCanoPoco7";
            this.gutierrezCanoPoco7.RaioYElipse = 30;
            this.gutierrezCanoPoco7.Size = new System.Drawing.Size(47, 13);
            this.gutierrezCanoPoco7.TabIndex = 112;
            this.gutierrezCanoPoco7.vertical = false;
            // 
            // gutierrezCanoBomba2
            // 
            this.gutierrezCanoBomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCanoBomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCanoBomba2.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCanoBomba2.diminuirbolha = 5D;
            this.gutierrezCanoBomba2.enableTransparentBackground = true;
            this.gutierrezCanoBomba2.fliped = false;
            this.gutierrezCanoBomba2.fluxo = -1;
            this.gutierrezCanoBomba2.Location = new System.Drawing.Point(630, 267);
            this.gutierrezCanoBomba2.MaxVidro = 127;
            this.gutierrezCanoBomba2.MinVidro = 8;
            this.gutierrezCanoBomba2.Name = "gutierrezCanoBomba2";
            this.gutierrezCanoBomba2.RaioYElipse = 30;
            this.gutierrezCanoBomba2.Size = new System.Drawing.Size(13, 114);
            this.gutierrezCanoBomba2.TabIndex = 254;
            this.gutierrezCanoBomba2.vertical = true;
            // 
            // gutierrezCano1Bomba1
            // 
            this.gutierrezCano1Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCano1Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCano1Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCano1Bomba1.diminuirbolha = 5D;
            this.gutierrezCano1Bomba1.enableTransparentBackground = true;
            this.gutierrezCano1Bomba1.fliped = false;
            this.gutierrezCano1Bomba1.fluxo = 0;
            this.gutierrezCano1Bomba1.Location = new System.Drawing.Point(547, 353);
            this.gutierrezCano1Bomba1.MaxVidro = 127;
            this.gutierrezCano1Bomba1.MinVidro = 8;
            this.gutierrezCano1Bomba1.Name = "gutierrezCano1Bomba1";
            this.gutierrezCano1Bomba1.RaioYElipse = 30;
            this.gutierrezCano1Bomba1.Size = new System.Drawing.Size(34, 13);
            this.gutierrezCano1Bomba1.TabIndex = 251;
            this.gutierrezCano1Bomba1.vertical = false;
            // 
            // gutierrezCano3Bomba2
            // 
            this.gutierrezCano3Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.gutierrezCano3Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.gutierrezCano3Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.gutierrezCano3Bomba2.diminuirbolha = 5D;
            this.gutierrezCano3Bomba2.enableTransparentBackground = true;
            this.gutierrezCano3Bomba2.fliped = false;
            this.gutierrezCano3Bomba2.fluxo = 0;
            this.gutierrezCano3Bomba2.Location = new System.Drawing.Point(792, 452);
            this.gutierrezCano3Bomba2.MaxVidro = 127;
            this.gutierrezCano3Bomba2.MinVidro = 8;
            this.gutierrezCano3Bomba2.Name = "gutierrezCano3Bomba2";
            this.gutierrezCano3Bomba2.RaioYElipse = 30;
            this.gutierrezCano3Bomba2.Size = new System.Drawing.Size(281, 13);
            this.gutierrezCano3Bomba2.TabIndex = 260;
            this.gutierrezCano3Bomba2.vertical = false;
            // 
            // gutierrezPoco8
            // 
            this.gutierrezPoco8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gutierrezPoco8.LigarBomba = true;
            this.gutierrezPoco8.Location = new System.Drawing.Point(939, 59);
            this.gutierrezPoco8.Name = "gutierrezPoco8";
            this.gutierrezPoco8.Size = new System.Drawing.Size(66, 78);
            this.gutierrezPoco8.TabIndex = 109;
            this.gutierrezPoco8.Transparence = false;
            // 
            // Estadual
            // 
            this.Estadual.Controls.Add(this.label109);
            this.Estadual.Controls.Add(this.junção133);
            this.Estadual.Controls.Add(this.estadualCano2Chamcia);
            this.Estadual.Controls.Add(this.junção132);
            this.Estadual.Controls.Add(this.estadualBombaChamcia);
            this.Estadual.Controls.Add(this.junção131);
            this.Estadual.Controls.Add(this.estadualVazaoBomba2);
            this.Estadual.Controls.Add(this.waterMeter10);
            this.Estadual.Controls.Add(this.estadualVazaoBomba1);
            this.Estadual.Controls.Add(this.waterMeter9);
            this.Estadual.Controls.Add(this.junção129);
            this.Estadual.Controls.Add(this.junção70);
            this.Estadual.Controls.Add(this.label47);
            this.Estadual.Controls.Add(this.junção63);
            this.Estadual.Controls.Add(this.label48);
            this.Estadual.Controls.Add(this.junção64);
            this.Estadual.Controls.Add(this.junção65);
            this.Estadual.Controls.Add(this.junção66);
            this.Estadual.Controls.Add(this.estadualBomba2);
            this.Estadual.Controls.Add(this.estadualCano2Bomba2);
            this.Estadual.Controls.Add(this.junção67);
            this.Estadual.Controls.Add(this.estadualCano3Bomba1);
            this.Estadual.Controls.Add(this.junção68);
            this.Estadual.Controls.Add(this.junção69);
            this.Estadual.Controls.Add(this.estadualBomba1);
            this.Estadual.Controls.Add(this.estadualCano2Bomba1);
            this.Estadual.Controls.Add(this.estadualCanoBomba1);
            this.Estadual.Controls.Add(this.estadualCano1Bomba2);
            this.Estadual.Controls.Add(this.junçãoT22);
            this.Estadual.Controls.Add(this.junção54);
            this.Estadual.Controls.Add(this.estadualTanque1);
            this.Estadual.Controls.Add(this.junção62);
            this.Estadual.Controls.Add(this.junçãoT23);
            this.Estadual.Controls.Add(this.estadualPoco1);
            this.Estadual.Controls.Add(this.estadualCanoPoco2);
            this.Estadual.Controls.Add(this.label40);
            this.Estadual.Controls.Add(this.junçãoT24);
            this.Estadual.Controls.Add(this.junçãoT25);
            this.Estadual.Controls.Add(this.junçãoT26);
            this.Estadual.Controls.Add(this.junçãoT27);
            this.Estadual.Controls.Add(this.estadualCanoPoco6);
            this.Estadual.Controls.Add(this.estadualPoco2);
            this.Estadual.Controls.Add(this.estadualPoco3);
            this.Estadual.Controls.Add(this.estadualPoco4);
            this.Estadual.Controls.Add(this.estadualPoco5);
            this.Estadual.Controls.Add(this.estadualCanoPoco1);
            this.Estadual.Controls.Add(this.estadualCanoPoco3);
            this.Estadual.Controls.Add(this.estadualCanoPoco4);
            this.Estadual.Controls.Add(this.estadualCanoPoco5);
            this.Estadual.Controls.Add(this.label41);
            this.Estadual.Controls.Add(this.label42);
            this.Estadual.Controls.Add(this.label43);
            this.Estadual.Controls.Add(this.label44);
            this.Estadual.Controls.Add(this.label45);
            this.Estadual.Controls.Add(this.label46);
            this.Estadual.Controls.Add(this.estadualPoco7);
            this.Estadual.Controls.Add(this.estadualPoco6);
            this.Estadual.Controls.Add(this.estadualCanoPoco7);
            this.Estadual.Controls.Add(this.estadualTanque2);
            this.Estadual.Controls.Add(this.estadualCanoTanque2);
            this.Estadual.Controls.Add(this.estadualCanoBomba2);
            this.Estadual.Controls.Add(this.estadualCano1Bomba1);
            this.Estadual.Controls.Add(this.estadualCano3Bomba2);
            this.Estadual.Controls.Add(this.estadualCanoChamcia);
            this.Estadual.Controls.Add(this.cano3);
            this.Estadual.Controls.Add(this.cano4);
            this.Estadual.Location = new System.Drawing.Point(4, 22);
            this.Estadual.Name = "Estadual";
            this.Estadual.Size = new System.Drawing.Size(1480, 662);
            this.Estadual.TabIndex = 7;
            this.Estadual.Text = "ESTADUAL";
            this.Estadual.UseVisualStyleBackColor = true;
            // 
            // label109
            // 
            this.label109.BackColor = System.Drawing.Color.LightGray;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(20, 184);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(142, 55);
            this.label109.TabIndex = 296;
            this.label109.Text = "BATERIA CHANCIA";
            this.label109.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // junção133
            // 
            this.junção133.Flip = 90;
            this.junção133.Location = new System.Drawing.Point(81, 130);
            this.junção133.Margin = new System.Windows.Forms.Padding(0);
            this.junção133.Name = "junção133";
            this.junção133.Size = new System.Drawing.Size(30, 32);
            this.junção133.TabIndex = 295;
            // 
            // estadualCano2Chamcia
            // 
            this.estadualCano2Chamcia.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCano2Chamcia.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCano2Chamcia.CorVidro = System.Drawing.Color.Silver;
            this.estadualCano2Chamcia.diminuirbolha = 5D;
            this.estadualCano2Chamcia.enableTransparentBackground = true;
            this.estadualCano2Chamcia.fliped = false;
            this.estadualCano2Chamcia.fluxo = 1;
            this.estadualCano2Chamcia.Location = new System.Drawing.Point(83, 156);
            this.estadualCano2Chamcia.MaxVidro = 127;
            this.estadualCano2Chamcia.MinVidro = 8;
            this.estadualCano2Chamcia.Name = "estadualCano2Chamcia";
            this.estadualCano2Chamcia.RaioYElipse = 30;
            this.estadualCano2Chamcia.Size = new System.Drawing.Size(13, 37);
            this.estadualCano2Chamcia.TabIndex = 294;
            this.estadualCano2Chamcia.vertical = true;
            // 
            // junção132
            // 
            this.junção132.Flip = 90;
            this.junção132.Location = new System.Drawing.Point(114, 92);
            this.junção132.Margin = new System.Windows.Forms.Padding(0);
            this.junção132.Name = "junção132";
            this.junção132.Size = new System.Drawing.Size(30, 32);
            this.junção132.TabIndex = 292;
            // 
            // estadualBombaChamcia
            // 
            this.estadualBombaChamcia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualBombaChamcia.LigarBomba = false;
            this.estadualBombaChamcia.Location = new System.Drawing.Point(106, 116);
            this.estadualBombaChamcia.Name = "estadualBombaChamcia";
            this.estadualBombaChamcia.Size = new System.Drawing.Size(60, 50);
            this.estadualBombaChamcia.TabIndex = 291;
            this.estadualBombaChamcia.Transparence = false;
            // 
            // junção131
            // 
            this.junção131.Flip = 90;
            this.junção131.Location = new System.Drawing.Point(764, 53);
            this.junção131.Margin = new System.Windows.Forms.Padding(0);
            this.junção131.Name = "junção131";
            this.junção131.Size = new System.Drawing.Size(30, 32);
            this.junção131.TabIndex = 290;
            // 
            // estadualVazaoBomba2
            // 
            this.estadualVazaoBomba2.AutoSize = true;
            this.estadualVazaoBomba2.BackColor = System.Drawing.Color.Black;
            this.estadualVazaoBomba2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadualVazaoBomba2.ForeColor = System.Drawing.Color.Transparent;
            this.estadualVazaoBomba2.Location = new System.Drawing.Point(847, 498);
            this.estadualVazaoBomba2.Name = "estadualVazaoBomba2";
            this.estadualVazaoBomba2.Size = new System.Drawing.Size(76, 16);
            this.estadualVazaoBomba2.TabIndex = 289;
            this.estadualVazaoBomba2.Text = "00.00 (m³/h)";
            // 
            // waterMeter10
            // 
            this.waterMeter10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter10.Location = new System.Drawing.Point(859, 439);
            this.waterMeter10.Name = "waterMeter10";
            this.waterMeter10.Size = new System.Drawing.Size(55, 55);
            this.waterMeter10.TabIndex = 288;
            this.waterMeter10.Transparence = false;
            // 
            // estadualVazaoBomba1
            // 
            this.estadualVazaoBomba1.AutoSize = true;
            this.estadualVazaoBomba1.BackColor = System.Drawing.Color.Black;
            this.estadualVazaoBomba1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadualVazaoBomba1.ForeColor = System.Drawing.Color.Transparent;
            this.estadualVazaoBomba1.Location = new System.Drawing.Point(153, 498);
            this.estadualVazaoBomba1.Name = "estadualVazaoBomba1";
            this.estadualVazaoBomba1.Size = new System.Drawing.Size(76, 16);
            this.estadualVazaoBomba1.TabIndex = 287;
            this.estadualVazaoBomba1.Text = "00.00 (m³/h)";
            // 
            // waterMeter9
            // 
            this.waterMeter9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter9.Location = new System.Drawing.Point(165, 439);
            this.waterMeter9.Name = "waterMeter9";
            this.waterMeter9.Size = new System.Drawing.Size(55, 55);
            this.waterMeter9.TabIndex = 286;
            this.waterMeter9.Transparence = false;
            // 
            // junção129
            // 
            this.junção129.Flip = 0;
            this.junção129.Location = new System.Drawing.Point(601, 456);
            this.junção129.Margin = new System.Windows.Forms.Padding(0);
            this.junção129.Name = "junção129";
            this.junção129.Size = new System.Drawing.Size(31, 32);
            this.junção129.TabIndex = 241;
            // 
            // junção70
            // 
            this.junção70.Flip = 90;
            this.junção70.Location = new System.Drawing.Point(316, 241);
            this.junção70.Margin = new System.Windows.Forms.Padding(0);
            this.junção70.Name = "junção70";
            this.junção70.Size = new System.Drawing.Size(30, 32);
            this.junção70.TabIndex = 240;
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.Color.Gainsboro;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(5, 438);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(142, 81);
            this.label47.TabIndex = 223;
            this.label47.Text = "JARDIM REGINA, AEROPORTO, AMORIN CENTRO";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção63
            // 
            this.junção63.Flip = 180;
            this.junção63.Location = new System.Drawing.Point(413, 370);
            this.junção63.Margin = new System.Windows.Forms.Padding(0);
            this.junção63.Name = "junção63";
            this.junção63.Size = new System.Drawing.Size(29, 32);
            this.junção63.TabIndex = 230;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.Color.Gainsboro;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(949, 437);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(142, 69);
            this.label48.TabIndex = 222;
            this.label48.Text = "CENTRO, SANTA HELENA";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção64
            // 
            this.junção64.Flip = 180;
            this.junção64.Location = new System.Drawing.Point(589, 370);
            this.junção64.Margin = new System.Windows.Forms.Padding(0);
            this.junção64.Name = "junção64";
            this.junção64.Size = new System.Drawing.Size(29, 32);
            this.junção64.TabIndex = 237;
            // 
            // junção65
            // 
            this.junção65.Flip = 90;
            this.junção65.Location = new System.Drawing.Point(500, 370);
            this.junção65.Margin = new System.Windows.Forms.Padding(0);
            this.junção65.Name = "junção65";
            this.junção65.Size = new System.Drawing.Size(30, 32);
            this.junção65.TabIndex = 236;
            // 
            // junção66
            // 
            this.junção66.Flip = 0;
            this.junção66.Location = new System.Drawing.Point(463, 394);
            this.junção66.Margin = new System.Windows.Forms.Padding(0);
            this.junção66.Name = "junção66";
            this.junção66.Size = new System.Drawing.Size(31, 32);
            this.junção66.TabIndex = 234;
            // 
            // estadualBomba2
            // 
            this.estadualBomba2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualBomba2.LigarBomba = false;
            this.estadualBomba2.Location = new System.Drawing.Point(492, 394);
            this.estadualBomba2.Name = "estadualBomba2";
            this.estadualBomba2.Size = new System.Drawing.Size(60, 50);
            this.estadualBomba2.TabIndex = 233;
            this.estadualBomba2.Transparence = false;
            // 
            // estadualCano2Bomba2
            // 
            this.estadualCano2Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCano2Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCano2Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.estadualCano2Bomba2.diminuirbolha = 5D;
            this.estadualCano2Bomba2.enableTransparentBackground = true;
            this.estadualCano2Bomba2.fliped = false;
            this.estadualCano2Bomba2.fluxo = 0;
            this.estadualCano2Bomba2.Location = new System.Drawing.Point(603, 394);
            this.estadualCano2Bomba2.MaxVidro = 127;
            this.estadualCano2Bomba2.MinVidro = 8;
            this.estadualCano2Bomba2.Name = "estadualCano2Bomba2";
            this.estadualCano2Bomba2.RaioYElipse = 30;
            this.estadualCano2Bomba2.Size = new System.Drawing.Size(13, 72);
            this.estadualCano2Bomba2.TabIndex = 238;
            this.estadualCano2Bomba2.vertical = true;
            // 
            // junção67
            // 
            this.junção67.Flip = 270;
            this.junção67.Location = new System.Drawing.Point(413, 456);
            this.junção67.Margin = new System.Windows.Forms.Padding(0);
            this.junção67.Name = "junção67";
            this.junção67.Size = new System.Drawing.Size(29, 32);
            this.junção67.TabIndex = 225;
            // 
            // estadualCano3Bomba1
            // 
            this.estadualCano3Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCano3Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCano3Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.estadualCano3Bomba1.diminuirbolha = 5D;
            this.estadualCano3Bomba1.enableTransparentBackground = true;
            this.estadualCano3Bomba1.fliped = false;
            this.estadualCano3Bomba1.fluxo = 0;
            this.estadualCano3Bomba1.Location = new System.Drawing.Point(106, 472);
            this.estadualCano3Bomba1.MaxVidro = 127;
            this.estadualCano3Bomba1.MinVidro = 8;
            this.estadualCano3Bomba1.Name = "estadualCano3Bomba1";
            this.estadualCano3Bomba1.RaioYElipse = 30;
            this.estadualCano3Bomba1.Size = new System.Drawing.Size(317, 13);
            this.estadualCano3Bomba1.TabIndex = 224;
            this.estadualCano3Bomba1.vertical = false;
            // 
            // junção68
            // 
            this.junção68.Flip = 90;
            this.junção68.Location = new System.Drawing.Point(353, 370);
            this.junção68.Margin = new System.Windows.Forms.Padding(0);
            this.junção68.Name = "junção68";
            this.junção68.Size = new System.Drawing.Size(30, 32);
            this.junção68.TabIndex = 229;
            // 
            // junção69
            // 
            this.junção69.Flip = 0;
            this.junção69.Location = new System.Drawing.Point(316, 394);
            this.junção69.Margin = new System.Windows.Forms.Padding(0);
            this.junção69.Name = "junção69";
            this.junção69.Size = new System.Drawing.Size(31, 32);
            this.junção69.TabIndex = 227;
            // 
            // estadualBomba1
            // 
            this.estadualBomba1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualBomba1.LigarBomba = false;
            this.estadualBomba1.Location = new System.Drawing.Point(345, 394);
            this.estadualBomba1.Name = "estadualBomba1";
            this.estadualBomba1.Size = new System.Drawing.Size(60, 50);
            this.estadualBomba1.TabIndex = 226;
            this.estadualBomba1.Transparence = false;
            // 
            // estadualCano2Bomba1
            // 
            this.estadualCano2Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCano2Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCano2Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.estadualCano2Bomba1.diminuirbolha = 5D;
            this.estadualCano2Bomba1.enableTransparentBackground = true;
            this.estadualCano2Bomba1.fliped = false;
            this.estadualCano2Bomba1.fluxo = 0;
            this.estadualCano2Bomba1.Location = new System.Drawing.Point(427, 394);
            this.estadualCano2Bomba1.MaxVidro = 127;
            this.estadualCano2Bomba1.MinVidro = 8;
            this.estadualCano2Bomba1.Name = "estadualCano2Bomba1";
            this.estadualCano2Bomba1.RaioYElipse = 30;
            this.estadualCano2Bomba1.Size = new System.Drawing.Size(13, 72);
            this.estadualCano2Bomba1.TabIndex = 231;
            this.estadualCano2Bomba1.vertical = true;
            // 
            // estadualCanoBomba1
            // 
            this.estadualCanoBomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoBomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoBomba1.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoBomba1.diminuirbolha = 5D;
            this.estadualCanoBomba1.enableTransparentBackground = true;
            this.estadualCanoBomba1.fliped = false;
            this.estadualCanoBomba1.fluxo = -1;
            this.estadualCanoBomba1.Location = new System.Drawing.Point(318, 266);
            this.estadualCanoBomba1.MaxVidro = 127;
            this.estadualCanoBomba1.MinVidro = 8;
            this.estadualCanoBomba1.Name = "estadualCanoBomba1";
            this.estadualCanoBomba1.RaioYElipse = 30;
            this.estadualCanoBomba1.Size = new System.Drawing.Size(13, 136);
            this.estadualCanoBomba1.TabIndex = 228;
            this.estadualCanoBomba1.vertical = true;
            // 
            // estadualCano1Bomba2
            // 
            this.estadualCano1Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCano1Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCano1Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.estadualCano1Bomba2.diminuirbolha = 5D;
            this.estadualCano1Bomba2.enableTransparentBackground = true;
            this.estadualCano1Bomba2.fliped = false;
            this.estadualCano1Bomba2.fluxo = 0;
            this.estadualCano1Bomba2.Location = new System.Drawing.Point(527, 374);
            this.estadualCano1Bomba2.MaxVidro = 127;
            this.estadualCano1Bomba2.MinVidro = 8;
            this.estadualCano1Bomba2.Name = "estadualCano1Bomba2";
            this.estadualCano1Bomba2.RaioYElipse = 30;
            this.estadualCano1Bomba2.Size = new System.Drawing.Size(69, 13);
            this.estadualCano1Bomba2.TabIndex = 239;
            this.estadualCano1Bomba2.vertical = false;
            // 
            // junçãoT22
            // 
            this.junçãoT22.Flip = 180;
            this.junçãoT22.Location = new System.Drawing.Point(895, 49);
            this.junçãoT22.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT22.Name = "junçãoT22";
            this.junçãoT22.Size = new System.Drawing.Size(42, 44);
            this.junçãoT22.TabIndex = 126;
            // 
            // junção54
            // 
            this.junção54.Flip = 180;
            this.junção54.Location = new System.Drawing.Point(351, 91);
            this.junção54.Margin = new System.Windows.Forms.Padding(0);
            this.junção54.Name = "junção54";
            this.junção54.Size = new System.Drawing.Size(29, 32);
            this.junção54.TabIndex = 140;
            // 
            // estadualTanque1
            // 
            this.estadualTanque1.BackColor = System.Drawing.Color.Transparent;
            this.estadualTanque1.Cano = 2;
            this.estadualTanque1.CanoCheio = true;
            this.estadualTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualTanque1.CorVidro = System.Drawing.Color.Silver;
            this.estadualTanque1.enableTransparentBackground = true;
            this.estadualTanque1.LarguraCano = 13;
            this.estadualTanque1.Location = new System.Drawing.Point(350, 124);
            this.estadualTanque1.MaxVidro = 127;
            this.estadualTanque1.MinVidro = 8;
            this.estadualTanque1.Name = "estadualTanque1";
            this.estadualTanque1.PosCano = 15;
            this.estadualTanque1.RaioYElipse = 30;
            this.estadualTanque1.Size = new System.Drawing.Size(202, 161);
            this.estadualTanque1.TabIndex = 111;
            this.estadualTanque1.Value = 40D;
            // 
            // junção62
            // 
            this.junção62.Flip = 180;
            this.junção62.Location = new System.Drawing.Point(1250, 53);
            this.junção62.Margin = new System.Windows.Forms.Padding(0);
            this.junção62.Name = "junção62";
            this.junção62.Size = new System.Drawing.Size(30, 32);
            this.junção62.TabIndex = 139;
            // 
            // junçãoT23
            // 
            this.junçãoT23.Flip = 180;
            this.junçãoT23.Location = new System.Drawing.Point(826, 49);
            this.junçãoT23.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT23.Name = "junçãoT23";
            this.junçãoT23.Size = new System.Drawing.Size(42, 44);
            this.junçãoT23.TabIndex = 136;
            // 
            // estadualPoco1
            // 
            this.estadualPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualPoco1.LigarBomba = true;
            this.estadualPoco1.Location = new System.Drawing.Point(809, 80);
            this.estadualPoco1.Name = "estadualPoco1";
            this.estadualPoco1.Size = new System.Drawing.Size(66, 78);
            this.estadualPoco1.TabIndex = 138;
            this.estadualPoco1.Transparence = false;
            // 
            // estadualCanoPoco2
            // 
            this.estadualCanoPoco2.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoPoco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoPoco2.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoPoco2.diminuirbolha = 5D;
            this.estadualCanoPoco2.enableTransparentBackground = true;
            this.estadualCanoPoco2.fliped = false;
            this.estadualCanoPoco2.fluxo = 1;
            this.estadualCanoPoco2.Location = new System.Drawing.Point(792, 57);
            this.estadualCanoPoco2.MaxVidro = 127;
            this.estadualCanoPoco2.MinVidro = 8;
            this.estadualCanoPoco2.Name = "estadualCanoPoco2";
            this.estadualCanoPoco2.RaioYElipse = 30;
            this.estadualCanoPoco2.Size = new System.Drawing.Size(40, 13);
            this.estadualCanoPoco2.TabIndex = 137;
            this.estadualCanoPoco2.vertical = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(818, 161);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(62, 16);
            this.label40.TabIndex = 135;
            this.label40.Text = "POÇO 1";
            // 
            // junçãoT24
            // 
            this.junçãoT24.Flip = 180;
            this.junçãoT24.Location = new System.Drawing.Point(968, 49);
            this.junçãoT24.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT24.Name = "junçãoT24";
            this.junçãoT24.Size = new System.Drawing.Size(42, 44);
            this.junçãoT24.TabIndex = 124;
            // 
            // junçãoT25
            // 
            this.junçãoT25.Flip = 180;
            this.junçãoT25.Location = new System.Drawing.Point(1040, 49);
            this.junçãoT25.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT25.Name = "junçãoT25";
            this.junçãoT25.Size = new System.Drawing.Size(42, 44);
            this.junçãoT25.TabIndex = 122;
            // 
            // junçãoT26
            // 
            this.junçãoT26.Flip = 180;
            this.junçãoT26.Location = new System.Drawing.Point(1112, 49);
            this.junçãoT26.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT26.Name = "junçãoT26";
            this.junçãoT26.Size = new System.Drawing.Size(42, 44);
            this.junçãoT26.TabIndex = 120;
            // 
            // junçãoT27
            // 
            this.junçãoT27.Flip = 180;
            this.junçãoT27.Location = new System.Drawing.Point(1184, 49);
            this.junçãoT27.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT27.Name = "junçãoT27";
            this.junçãoT27.Size = new System.Drawing.Size(42, 44);
            this.junçãoT27.TabIndex = 118;
            // 
            // estadualCanoPoco6
            // 
            this.estadualCanoPoco6.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoPoco6.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoPoco6.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoPoco6.diminuirbolha = 5D;
            this.estadualCanoPoco6.enableTransparentBackground = true;
            this.estadualCanoPoco6.fliped = false;
            this.estadualCanoPoco6.fluxo = 1;
            this.estadualCanoPoco6.Location = new System.Drawing.Point(1217, 57);
            this.estadualCanoPoco6.MaxVidro = 127;
            this.estadualCanoPoco6.MinVidro = 8;
            this.estadualCanoPoco6.Name = "estadualCanoPoco6";
            this.estadualCanoPoco6.RaioYElipse = 30;
            this.estadualCanoPoco6.Size = new System.Drawing.Size(37, 13);
            this.estadualCanoPoco6.TabIndex = 121;
            this.estadualCanoPoco6.vertical = false;
            // 
            // estadualPoco2
            // 
            this.estadualPoco2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualPoco2.LigarBomba = true;
            this.estadualPoco2.Location = new System.Drawing.Point(878, 80);
            this.estadualPoco2.Name = "estadualPoco2";
            this.estadualPoco2.Size = new System.Drawing.Size(66, 78);
            this.estadualPoco2.TabIndex = 134;
            this.estadualPoco2.Transparence = false;
            // 
            // estadualPoco3
            // 
            this.estadualPoco3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualPoco3.LigarBomba = true;
            this.estadualPoco3.Location = new System.Drawing.Point(950, 80);
            this.estadualPoco3.Name = "estadualPoco3";
            this.estadualPoco3.Size = new System.Drawing.Size(66, 78);
            this.estadualPoco3.TabIndex = 133;
            this.estadualPoco3.Transparence = false;
            // 
            // estadualPoco4
            // 
            this.estadualPoco4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualPoco4.LigarBomba = true;
            this.estadualPoco4.Location = new System.Drawing.Point(1022, 80);
            this.estadualPoco4.Name = "estadualPoco4";
            this.estadualPoco4.Size = new System.Drawing.Size(66, 78);
            this.estadualPoco4.TabIndex = 132;
            this.estadualPoco4.Transparence = false;
            // 
            // estadualPoco5
            // 
            this.estadualPoco5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualPoco5.LigarBomba = true;
            this.estadualPoco5.Location = new System.Drawing.Point(1094, 80);
            this.estadualPoco5.Name = "estadualPoco5";
            this.estadualPoco5.Size = new System.Drawing.Size(66, 78);
            this.estadualPoco5.TabIndex = 131;
            this.estadualPoco5.Transparence = false;
            // 
            // estadualCanoPoco1
            // 
            this.estadualCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoPoco1.diminuirbolha = 5D;
            this.estadualCanoPoco1.enableTransparentBackground = true;
            this.estadualCanoPoco1.fliped = false;
            this.estadualCanoPoco1.fluxo = 1;
            this.estadualCanoPoco1.Location = new System.Drawing.Point(861, 57);
            this.estadualCanoPoco1.MaxVidro = 127;
            this.estadualCanoPoco1.MinVidro = 8;
            this.estadualCanoPoco1.Name = "estadualCanoPoco1";
            this.estadualCanoPoco1.RaioYElipse = 30;
            this.estadualCanoPoco1.Size = new System.Drawing.Size(37, 13);
            this.estadualCanoPoco1.TabIndex = 128;
            this.estadualCanoPoco1.vertical = false;
            // 
            // estadualCanoPoco3
            // 
            this.estadualCanoPoco3.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoPoco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoPoco3.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoPoco3.diminuirbolha = 5D;
            this.estadualCanoPoco3.enableTransparentBackground = true;
            this.estadualCanoPoco3.fliped = false;
            this.estadualCanoPoco3.fluxo = 1;
            this.estadualCanoPoco3.Location = new System.Drawing.Point(930, 57);
            this.estadualCanoPoco3.MaxVidro = 127;
            this.estadualCanoPoco3.MinVidro = 8;
            this.estadualCanoPoco3.Name = "estadualCanoPoco3";
            this.estadualCanoPoco3.RaioYElipse = 30;
            this.estadualCanoPoco3.Size = new System.Drawing.Size(40, 13);
            this.estadualCanoPoco3.TabIndex = 127;
            this.estadualCanoPoco3.vertical = false;
            // 
            // estadualCanoPoco4
            // 
            this.estadualCanoPoco4.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoPoco4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoPoco4.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoPoco4.diminuirbolha = 5D;
            this.estadualCanoPoco4.enableTransparentBackground = true;
            this.estadualCanoPoco4.fliped = false;
            this.estadualCanoPoco4.fluxo = 1;
            this.estadualCanoPoco4.Location = new System.Drawing.Point(1074, 57);
            this.estadualCanoPoco4.MaxVidro = 127;
            this.estadualCanoPoco4.MinVidro = 8;
            this.estadualCanoPoco4.Name = "estadualCanoPoco4";
            this.estadualCanoPoco4.RaioYElipse = 30;
            this.estadualCanoPoco4.Size = new System.Drawing.Size(47, 13);
            this.estadualCanoPoco4.TabIndex = 125;
            this.estadualCanoPoco4.vertical = false;
            // 
            // estadualCanoPoco5
            // 
            this.estadualCanoPoco5.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoPoco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoPoco5.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoPoco5.diminuirbolha = 5D;
            this.estadualCanoPoco5.enableTransparentBackground = true;
            this.estadualCanoPoco5.fliped = false;
            this.estadualCanoPoco5.fluxo = 1;
            this.estadualCanoPoco5.Location = new System.Drawing.Point(994, 57);
            this.estadualCanoPoco5.MaxVidro = 127;
            this.estadualCanoPoco5.MinVidro = 8;
            this.estadualCanoPoco5.Name = "estadualCanoPoco5";
            this.estadualCanoPoco5.RaioYElipse = 30;
            this.estadualCanoPoco5.Size = new System.Drawing.Size(56, 13);
            this.estadualCanoPoco5.TabIndex = 123;
            this.estadualCanoPoco5.vertical = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(887, 161);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(62, 16);
            this.label41.TabIndex = 117;
            this.label41.Text = "POÇO 2";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(959, 161);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(62, 16);
            this.label42.TabIndex = 116;
            this.label42.Text = "POÇO 3";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(1031, 161);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 16);
            this.label43.TabIndex = 115;
            this.label43.Text = "POÇO 4";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(1103, 161);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(62, 16);
            this.label44.TabIndex = 114;
            this.label44.Text = "POÇO 5";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(1175, 161);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(62, 16);
            this.label45.TabIndex = 113;
            this.label45.Text = "POÇO 6";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(1243, 161);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(62, 16);
            this.label46.TabIndex = 112;
            this.label46.Text = "POÇO 7";
            // 
            // estadualPoco7
            // 
            this.estadualPoco7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualPoco7.LigarBomba = true;
            this.estadualPoco7.Location = new System.Drawing.Point(1233, 80);
            this.estadualPoco7.Name = "estadualPoco7";
            this.estadualPoco7.Size = new System.Drawing.Size(66, 78);
            this.estadualPoco7.TabIndex = 129;
            this.estadualPoco7.Transparence = false;
            // 
            // estadualPoco6
            // 
            this.estadualPoco6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.estadualPoco6.LigarBomba = true;
            this.estadualPoco6.Location = new System.Drawing.Point(1166, 80);
            this.estadualPoco6.Name = "estadualPoco6";
            this.estadualPoco6.Size = new System.Drawing.Size(66, 78);
            this.estadualPoco6.TabIndex = 130;
            this.estadualPoco6.Transparence = false;
            // 
            // estadualCanoPoco7
            // 
            this.estadualCanoPoco7.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoPoco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoPoco7.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoPoco7.diminuirbolha = 5D;
            this.estadualCanoPoco7.enableTransparentBackground = true;
            this.estadualCanoPoco7.fliped = false;
            this.estadualCanoPoco7.fluxo = 1;
            this.estadualCanoPoco7.Location = new System.Drawing.Point(1151, 57);
            this.estadualCanoPoco7.MaxVidro = 127;
            this.estadualCanoPoco7.MinVidro = 8;
            this.estadualCanoPoco7.Name = "estadualCanoPoco7";
            this.estadualCanoPoco7.RaioYElipse = 30;
            this.estadualCanoPoco7.Size = new System.Drawing.Size(43, 13);
            this.estadualCanoPoco7.TabIndex = 119;
            this.estadualCanoPoco7.vertical = false;
            // 
            // estadualTanque2
            // 
            this.estadualTanque2.BackColor = System.Drawing.Color.Transparent;
            this.estadualTanque2.Cano = 2;
            this.estadualTanque2.CanoCheio = true;
            this.estadualTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualTanque2.CorVidro = System.Drawing.Color.Silver;
            this.estadualTanque2.enableTransparentBackground = true;
            this.estadualTanque2.LarguraCano = 15;
            this.estadualTanque2.Location = new System.Drawing.Point(625, 80);
            this.estadualTanque2.MaxVidro = 127;
            this.estadualTanque2.MinVidro = 8;
            this.estadualTanque2.Name = "estadualTanque2";
            this.estadualTanque2.PosCano = 140;
            this.estadualTanque2.RaioYElipse = 30;
            this.estadualTanque2.Size = new System.Drawing.Size(164, 205);
            this.estadualTanque2.TabIndex = 177;
            this.estadualTanque2.Value = 40D;
            // 
            // estadualCanoTanque2
            // 
            this.estadualCanoTanque2.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoTanque2.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoTanque2.diminuirbolha = 5D;
            this.estadualCanoTanque2.enableTransparentBackground = true;
            this.estadualCanoTanque2.fliped = false;
            this.estadualCanoTanque2.fluxo = -1;
            this.estadualCanoTanque2.Location = new System.Drawing.Point(542, 241);
            this.estadualCanoTanque2.MaxVidro = 127;
            this.estadualCanoTanque2.MinVidro = 8;
            this.estadualCanoTanque2.Name = "estadualCanoTanque2";
            this.estadualCanoTanque2.RaioYElipse = 30;
            this.estadualCanoTanque2.Size = new System.Drawing.Size(112, 15);
            this.estadualCanoTanque2.TabIndex = 178;
            this.estadualCanoTanque2.vertical = false;
            // 
            // estadualCanoBomba2
            // 
            this.estadualCanoBomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoBomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoBomba2.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoBomba2.diminuirbolha = 5D;
            this.estadualCanoBomba2.enableTransparentBackground = true;
            this.estadualCanoBomba2.fliped = false;
            this.estadualCanoBomba2.fluxo = -1;
            this.estadualCanoBomba2.Location = new System.Drawing.Point(465, 266);
            this.estadualCanoBomba2.MaxVidro = 127;
            this.estadualCanoBomba2.MinVidro = 8;
            this.estadualCanoBomba2.Name = "estadualCanoBomba2";
            this.estadualCanoBomba2.RaioYElipse = 30;
            this.estadualCanoBomba2.Size = new System.Drawing.Size(13, 136);
            this.estadualCanoBomba2.TabIndex = 235;
            this.estadualCanoBomba2.vertical = true;
            // 
            // estadualCano1Bomba1
            // 
            this.estadualCano1Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCano1Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCano1Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.estadualCano1Bomba1.diminuirbolha = 5D;
            this.estadualCano1Bomba1.enableTransparentBackground = true;
            this.estadualCano1Bomba1.fliped = false;
            this.estadualCano1Bomba1.fluxo = 0;
            this.estadualCano1Bomba1.Location = new System.Drawing.Point(379, 374);
            this.estadualCano1Bomba1.MaxVidro = 127;
            this.estadualCano1Bomba1.MinVidro = 8;
            this.estadualCano1Bomba1.Name = "estadualCano1Bomba1";
            this.estadualCano1Bomba1.RaioYElipse = 30;
            this.estadualCano1Bomba1.Size = new System.Drawing.Size(36, 13);
            this.estadualCano1Bomba1.TabIndex = 232;
            this.estadualCano1Bomba1.vertical = false;
            // 
            // estadualCano3Bomba2
            // 
            this.estadualCano3Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCano3Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCano3Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.estadualCano3Bomba2.diminuirbolha = 5D;
            this.estadualCano3Bomba2.enableTransparentBackground = true;
            this.estadualCano3Bomba2.fliped = false;
            this.estadualCano3Bomba2.fluxo = 0;
            this.estadualCano3Bomba2.Location = new System.Drawing.Point(625, 472);
            this.estadualCano3Bomba2.MaxVidro = 127;
            this.estadualCano3Bomba2.MinVidro = 8;
            this.estadualCano3Bomba2.Name = "estadualCano3Bomba2";
            this.estadualCano3Bomba2.RaioYElipse = 30;
            this.estadualCano3Bomba2.Size = new System.Drawing.Size(341, 15);
            this.estadualCano3Bomba2.TabIndex = 242;
            this.estadualCano3Bomba2.vertical = false;
            // 
            // estadualCanoChamcia
            // 
            this.estadualCanoChamcia.BackColor = System.Drawing.Color.Gainsboro;
            this.estadualCanoChamcia.CorFluido = System.Drawing.Color.DodgerBlue;
            this.estadualCanoChamcia.CorVidro = System.Drawing.Color.Silver;
            this.estadualCanoChamcia.diminuirbolha = 5D;
            this.estadualCanoChamcia.enableTransparentBackground = true;
            this.estadualCanoChamcia.fliped = false;
            this.estadualCanoChamcia.fluxo = 0;
            this.estadualCanoChamcia.Location = new System.Drawing.Point(135, 95);
            this.estadualCanoChamcia.MaxVidro = 127;
            this.estadualCanoChamcia.MinVidro = 8;
            this.estadualCanoChamcia.Name = "estadualCanoChamcia";
            this.estadualCanoChamcia.RaioYElipse = 30;
            this.estadualCanoChamcia.Size = new System.Drawing.Size(226, 13);
            this.estadualCanoChamcia.TabIndex = 293;
            this.estadualCanoChamcia.vertical = false;
            // 
            // cano3
            // 
            this.cano3.BackColor = System.Drawing.Color.Gainsboro;
            this.cano3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.cano3.CorVidro = System.Drawing.Color.Silver;
            this.cano3.diminuirbolha = 5D;
            this.cano3.enableTransparentBackground = true;
            this.cano3.fliped = false;
            this.cano3.fluxo = -1;
            this.cano3.Location = new System.Drawing.Point(328, 244);
            this.cano3.MaxVidro = 127;
            this.cano3.MinVidro = 8;
            this.cano3.Name = "cano3";
            this.cano3.RaioYElipse = 30;
            this.cano3.Size = new System.Drawing.Size(31, 15);
            this.cano3.TabIndex = 297;
            this.cano3.vertical = false;
            // 
            // cano4
            // 
            this.cano4.BackColor = System.Drawing.Color.Gainsboro;
            this.cano4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.cano4.CorVidro = System.Drawing.Color.Silver;
            this.cano4.diminuirbolha = 5D;
            this.cano4.enableTransparentBackground = true;
            this.cano4.fliped = false;
            this.cano4.fluxo = -1;
            this.cano4.Location = new System.Drawing.Point(365, 116);
            this.cano4.MaxVidro = 127;
            this.cano4.MinVidro = 8;
            this.cano4.Name = "cano4";
            this.cano4.RaioYElipse = 30;
            this.cano4.Size = new System.Drawing.Size(13, 17);
            this.cano4.TabIndex = 298;
            this.cano4.vertical = true;
            // 
            // Independencia
            // 
            this.Independencia.Controls.Add(this.label108);
            this.Independencia.Controls.Add(this.label107);
            this.Independencia.Controls.Add(this.label106);
            this.Independencia.Controls.Add(this.label105);
            this.Independencia.Controls.Add(this.label104);
            this.Independencia.Controls.Add(this.junçãoT74);
            this.Independencia.Controls.Add(this.junçãoT73);
            this.Independencia.Controls.Add(this.junçãoT71);
            this.Independencia.Controls.Add(this.junçãoT72);
            this.Independencia.Controls.Add(this.junção127);
            this.Independencia.Controls.Add(this.junção125);
            this.Independencia.Controls.Add(this.junção126);
            this.Independencia.Controls.Add(this.independenciaBomba2);
            this.Independencia.Controls.Add(this.junção123);
            this.Independencia.Controls.Add(this.junçãoT70);
            this.Independencia.Controls.Add(this.junção122);
            this.Independencia.Controls.Add(this.junção121);
            this.Independencia.Controls.Add(this.junção119);
            this.Independencia.Controls.Add(this.junção120);
            this.Independencia.Controls.Add(this.independenciaBomba1);
            this.Independencia.Controls.Add(this.independenciaCano1Bomba1);
            this.Independencia.Controls.Add(this.junçãoT63);
            this.Independencia.Controls.Add(this.junção118);
            this.Independencia.Controls.Add(this.junçãoT52);
            this.Independencia.Controls.Add(this.independenciaTanque2);
            this.Independencia.Controls.Add(this.independenciaTanque3);
            this.Independencia.Controls.Add(this.junção117);
            this.Independencia.Controls.Add(this.junção114);
            this.Independencia.Controls.Add(this.independenciaTanque1);
            this.Independencia.Controls.Add(this.junçãoT69);
            this.Independencia.Controls.Add(this.junção116);
            this.Independencia.Controls.Add(this.junção115);
            this.Independencia.Controls.Add(this.junçãoT68);
            this.Independencia.Controls.Add(this.independenciaCanoPocoSolt2);
            this.Independencia.Controls.Add(this.label102);
            this.Independencia.Controls.Add(this.label103);
            this.Independencia.Controls.Add(this.independenciaPocoSolt1);
            this.Independencia.Controls.Add(this.independenciaPocoSolt2);
            this.Independencia.Controls.Add(this.independenciaCanoPocoSolt1);
            this.Independencia.Controls.Add(this.junçãoT64);
            this.Independencia.Controls.Add(this.junçãoT65);
            this.Independencia.Controls.Add(this.junçãoT66);
            this.Independencia.Controls.Add(this.junçãoT67);
            this.Independencia.Controls.Add(this.independenciaCanoPoco9);
            this.Independencia.Controls.Add(this.independenciaPoco12);
            this.Independencia.Controls.Add(this.independenciaPoco11);
            this.Independencia.Controls.Add(this.independenciaPoco10);
            this.Independencia.Controls.Add(this.independenciaPoco9);
            this.Independencia.Controls.Add(this.independenciaCanoPoco11);
            this.Independencia.Controls.Add(this.independenciaCanoPoco10);
            this.Independencia.Controls.Add(this.junção111);
            this.Independencia.Controls.Add(this.label96);
            this.Independencia.Controls.Add(this.label97);
            this.Independencia.Controls.Add(this.label98);
            this.Independencia.Controls.Add(this.label99);
            this.Independencia.Controls.Add(this.label100);
            this.Independencia.Controls.Add(this.label101);
            this.Independencia.Controls.Add(this.independenciaPoco7);
            this.Independencia.Controls.Add(this.independenciaPoco8);
            this.Independencia.Controls.Add(this.independenciaCanoPoco8);
            this.Independencia.Controls.Add(this.junção113);
            this.Independencia.Controls.Add(this.junçãoT62);
            this.Independencia.Controls.Add(this.junçãoT58);
            this.Independencia.Controls.Add(this.junçãoT59);
            this.Independencia.Controls.Add(this.junçãoT60);
            this.Independencia.Controls.Add(this.junçãoT61);
            this.Independencia.Controls.Add(this.independenciaCanoPoco3);
            this.Independencia.Controls.Add(this.independenciaPoco6);
            this.Independencia.Controls.Add(this.independenciaPoco5);
            this.Independencia.Controls.Add(this.independenciaPoco4);
            this.Independencia.Controls.Add(this.independenciaPoco3);
            this.Independencia.Controls.Add(this.independenciaCano2Poco7);
            this.Independencia.Controls.Add(this.independenciaCanoPoco6);
            this.Independencia.Controls.Add(this.independenciaCanoPoco5);
            this.Independencia.Controls.Add(this.independenciaCanoPoco4);
            this.Independencia.Controls.Add(this.junção112);
            this.Independencia.Controls.Add(this.label90);
            this.Independencia.Controls.Add(this.label91);
            this.Independencia.Controls.Add(this.label92);
            this.Independencia.Controls.Add(this.label93);
            this.Independencia.Controls.Add(this.label94);
            this.Independencia.Controls.Add(this.label95);
            this.Independencia.Controls.Add(this.independenciaTanque4);
            this.Independencia.Controls.Add(this.independenciaPoco1);
            this.Independencia.Controls.Add(this.independenciaPoco2);
            this.Independencia.Controls.Add(this.independenciaCanoPoco2);
            this.Independencia.Controls.Add(this.independenciaCanoTanque2);
            this.Independencia.Controls.Add(this.independenciaCanoTanque3);
            this.Independencia.Controls.Add(this.independenciaCanoPoco12);
            this.Independencia.Controls.Add(this.independenciaCanoTanque4);
            this.Independencia.Controls.Add(this.independenciaCanoPoco1);
            this.Independencia.Controls.Add(this.independenciaCano1Poco7);
            this.Independencia.Controls.Add(this.independenciaCanoPoco7);
            this.Independencia.Controls.Add(this.independenciaCano3Tanque1);
            this.Independencia.Controls.Add(this.independenciaCano2Tanque1);
            this.Independencia.Controls.Add(this.independenciaCanoTanque1);
            this.Independencia.Controls.Add(this.independenciaCanoBomba2);
            this.Independencia.Controls.Add(this.independenciaCano2Bomba1);
            this.Independencia.Controls.Add(this.independenciaCano3Bomba1);
            this.Independencia.Controls.Add(this.junção124);
            this.Independencia.Controls.Add(this.independenciaCanoBomba1);
            this.Independencia.Location = new System.Drawing.Point(4, 22);
            this.Independencia.Name = "Independencia";
            this.Independencia.Size = new System.Drawing.Size(1480, 662);
            this.Independencia.TabIndex = 6;
            this.Independencia.Text = "INDEPENDÊNCIA";
            this.Independencia.UseVisualStyleBackColor = true;
            // 
            // label108
            // 
            this.label108.BackColor = System.Drawing.Color.Gainsboro;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(1144, 453);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(142, 83);
            this.label108.TabIndex = 371;
            this.label108.Text = "MIRANDA; OURO VERDE E INTERLAGOS 01";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label107
            // 
            this.label107.BackColor = System.Drawing.Color.Gainsboro;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(839, 453);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(142, 83);
            this.label107.TabIndex = 370;
            this.label107.Text = "INTERLAGOS 02 E 03; PAINEIRAS";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label106
            // 
            this.label106.BackColor = System.Drawing.Color.Gainsboro;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(1144, 333);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(142, 83);
            this.label106.TabIndex = 369;
            this.label106.Text = "IBITURUNA, CENTRO E GOIÁS";
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label105
            // 
            this.label105.BackColor = System.Drawing.Color.Gainsboro;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(320, 333);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(142, 83);
            this.label105.TabIndex = 368;
            this.label105.Text = "SANTA TEREZINHA E JOKEY CLUB";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label104
            // 
            this.label104.BackColor = System.Drawing.Color.Gainsboro;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(320, 453);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(142, 83);
            this.label104.TabIndex = 367;
            this.label104.Text = "BAIRRO INDEPENDENCIA E SANTIAGO";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junçãoT74
            // 
            this.junçãoT74.Flip = 180;
            this.junçãoT74.Location = new System.Drawing.Point(885, 419);
            this.junçãoT74.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT74.Name = "junçãoT74";
            this.junçãoT74.Size = new System.Drawing.Size(42, 44);
            this.junçãoT74.TabIndex = 383;
            // 
            // junçãoT73
            // 
            this.junçãoT73.Flip = 270;
            this.junçãoT73.Location = new System.Drawing.Point(1187, 413);
            this.junçãoT73.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT73.Name = "junçãoT73";
            this.junçãoT73.Size = new System.Drawing.Size(42, 44);
            this.junçãoT73.TabIndex = 382;
            // 
            // junçãoT71
            // 
            this.junçãoT71.Flip = 0;
            this.junçãoT71.Location = new System.Drawing.Point(612, 407);
            this.junçãoT71.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT71.Name = "junçãoT71";
            this.junçãoT71.Size = new System.Drawing.Size(42, 44);
            this.junçãoT71.TabIndex = 373;
            // 
            // junçãoT72
            // 
            this.junçãoT72.Flip = 90;
            this.junçãoT72.Location = new System.Drawing.Point(373, 413);
            this.junçãoT72.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT72.Name = "junçãoT72";
            this.junçãoT72.Size = new System.Drawing.Size(42, 44);
            this.junçãoT72.TabIndex = 379;
            // 
            // junção127
            // 
            this.junção127.Flip = 180;
            this.junção127.Location = new System.Drawing.Point(612, 318);
            this.junção127.Margin = new System.Windows.Forms.Padding(0);
            this.junção127.Name = "junção127";
            this.junção127.Size = new System.Drawing.Size(29, 32);
            this.junção127.TabIndex = 378;
            // 
            // junção125
            // 
            this.junção125.Flip = 90;
            this.junção125.Location = new System.Drawing.Point(692, 318);
            this.junção125.Margin = new System.Windows.Forms.Padding(0);
            this.junção125.Name = "junção125";
            this.junção125.Size = new System.Drawing.Size(30, 32);
            this.junção125.TabIndex = 376;
            // 
            // junção126
            // 
            this.junção126.Flip = 0;
            this.junção126.Location = new System.Drawing.Point(655, 342);
            this.junção126.Margin = new System.Windows.Forms.Padding(0);
            this.junção126.Name = "junção126";
            this.junção126.Size = new System.Drawing.Size(31, 32);
            this.junção126.TabIndex = 375;
            // 
            // independenciaBomba2
            // 
            this.independenciaBomba2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaBomba2.LigarBomba = false;
            this.independenciaBomba2.Location = new System.Drawing.Point(684, 342);
            this.independenciaBomba2.Name = "independenciaBomba2";
            this.independenciaBomba2.Size = new System.Drawing.Size(60, 50);
            this.independenciaBomba2.TabIndex = 374;
            this.independenciaBomba2.Transparence = false;
            // 
            // junção123
            // 
            this.junção123.Flip = 270;
            this.junção123.Location = new System.Drawing.Point(456, 229);
            this.junção123.Margin = new System.Windows.Forms.Padding(0);
            this.junção123.Name = "junção123";
            this.junção123.Size = new System.Drawing.Size(29, 32);
            this.junção123.TabIndex = 366;
            // 
            // junçãoT70
            // 
            this.junçãoT70.Flip = 180;
            this.junçãoT70.Location = new System.Drawing.Point(549, -3);
            this.junçãoT70.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT70.Name = "junçãoT70";
            this.junçãoT70.Size = new System.Drawing.Size(42, 44);
            this.junçãoT70.TabIndex = 363;
            // 
            // junção122
            // 
            this.junção122.Flip = 180;
            this.junção122.Location = new System.Drawing.Point(816, 68);
            this.junção122.Margin = new System.Windows.Forms.Padding(0);
            this.junção122.Name = "junção122";
            this.junção122.Size = new System.Drawing.Size(29, 32);
            this.junção122.TabIndex = 360;
            // 
            // junção121
            // 
            this.junção121.Flip = 90;
            this.junção121.Location = new System.Drawing.Point(468, 4);
            this.junção121.Margin = new System.Windows.Forms.Padding(0);
            this.junção121.Name = "junção121";
            this.junção121.Size = new System.Drawing.Size(30, 32);
            this.junção121.TabIndex = 358;
            // 
            // junção119
            // 
            this.junção119.Flip = 90;
            this.junção119.Location = new System.Drawing.Point(557, 318);
            this.junção119.Margin = new System.Windows.Forms.Padding(0);
            this.junção119.Name = "junção119";
            this.junção119.Size = new System.Drawing.Size(30, 32);
            this.junção119.TabIndex = 355;
            // 
            // junção120
            // 
            this.junção120.Flip = 0;
            this.junção120.Location = new System.Drawing.Point(520, 342);
            this.junção120.Margin = new System.Windows.Forms.Padding(0);
            this.junção120.Name = "junção120";
            this.junção120.Size = new System.Drawing.Size(31, 32);
            this.junção120.TabIndex = 353;
            // 
            // independenciaBomba1
            // 
            this.independenciaBomba1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaBomba1.LigarBomba = false;
            this.independenciaBomba1.Location = new System.Drawing.Point(549, 342);
            this.independenciaBomba1.Name = "independenciaBomba1";
            this.independenciaBomba1.Size = new System.Drawing.Size(60, 50);
            this.independenciaBomba1.TabIndex = 352;
            this.independenciaBomba1.Transparence = false;
            // 
            // independenciaCano1Bomba1
            // 
            this.independenciaCano1Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCano1Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCano1Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCano1Bomba1.diminuirbolha = 5D;
            this.independenciaCano1Bomba1.enableTransparentBackground = true;
            this.independenciaCano1Bomba1.fliped = false;
            this.independenciaCano1Bomba1.fluxo = 0;
            this.independenciaCano1Bomba1.Location = new System.Drawing.Point(581, 321);
            this.independenciaCano1Bomba1.MaxVidro = 127;
            this.independenciaCano1Bomba1.MinVidro = 8;
            this.independenciaCano1Bomba1.Name = "independenciaCano1Bomba1";
            this.independenciaCano1Bomba1.RaioYElipse = 30;
            this.independenciaCano1Bomba1.Size = new System.Drawing.Size(36, 13);
            this.independenciaCano1Bomba1.TabIndex = 356;
            this.independenciaCano1Bomba1.vertical = false;
            // 
            // junçãoT63
            // 
            this.junçãoT63.Flip = 180;
            this.junçãoT63.Location = new System.Drawing.Point(902, -2);
            this.junçãoT63.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT63.Name = "junçãoT63";
            this.junçãoT63.Size = new System.Drawing.Size(42, 44);
            this.junçãoT63.TabIndex = 350;
            // 
            // junção118
            // 
            this.junção118.Flip = 180;
            this.junção118.Location = new System.Drawing.Point(1260, 171);
            this.junção118.Margin = new System.Windows.Forms.Padding(0);
            this.junção118.Name = "junção118";
            this.junção118.Size = new System.Drawing.Size(29, 32);
            this.junção118.TabIndex = 349;
            // 
            // junçãoT52
            // 
            this.junçãoT52.Flip = 180;
            this.junçãoT52.Location = new System.Drawing.Point(905, 165);
            this.junçãoT52.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT52.Name = "junçãoT52";
            this.junçãoT52.Size = new System.Drawing.Size(42, 44);
            this.junçãoT52.TabIndex = 299;
            // 
            // independenciaTanque2
            // 
            this.independenciaTanque2.BackColor = System.Drawing.Color.Transparent;
            this.independenciaTanque2.Cano = 0;
            this.independenciaTanque2.CanoCheio = false;
            this.independenciaTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaTanque2.CorVidro = System.Drawing.Color.Silver;
            this.independenciaTanque2.enableTransparentBackground = true;
            this.independenciaTanque2.LarguraCano = 30;
            this.independenciaTanque2.Location = new System.Drawing.Point(33, 286);
            this.independenciaTanque2.MaxVidro = 127;
            this.independenciaTanque2.MinVidro = 8;
            this.independenciaTanque2.Name = "independenciaTanque2";
            this.independenciaTanque2.PosCano = 15;
            this.independenciaTanque2.RaioYElipse = 30;
            this.independenciaTanque2.Size = new System.Drawing.Size(181, 135);
            this.independenciaTanque2.TabIndex = 181;
            this.independenciaTanque2.Value = 40D;
            // 
            // independenciaTanque3
            // 
            this.independenciaTanque3.BackColor = System.Drawing.Color.Transparent;
            this.independenciaTanque3.Cano = 0;
            this.independenciaTanque3.CanoCheio = false;
            this.independenciaTanque3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaTanque3.CorVidro = System.Drawing.Color.Silver;
            this.independenciaTanque3.enableTransparentBackground = true;
            this.independenciaTanque3.LarguraCano = 30;
            this.independenciaTanque3.Location = new System.Drawing.Point(33, 440);
            this.independenciaTanque3.MaxVidro = 127;
            this.independenciaTanque3.MinVidro = 8;
            this.independenciaTanque3.Name = "independenciaTanque3";
            this.independenciaTanque3.PosCano = 15;
            this.independenciaTanque3.RaioYElipse = 30;
            this.independenciaTanque3.Size = new System.Drawing.Size(181, 135);
            this.independenciaTanque3.TabIndex = 180;
            this.independenciaTanque3.Value = 40D;
            // 
            // junção117
            // 
            this.junção117.Flip = 0;
            this.junção117.Location = new System.Drawing.Point(5, 517);
            this.junção117.Margin = new System.Windows.Forms.Padding(0);
            this.junção117.Name = "junção117";
            this.junção117.Size = new System.Drawing.Size(35, 35);
            this.junção117.TabIndex = 347;
            // 
            // junção114
            // 
            this.junção114.Flip = 90;
            this.junção114.Location = new System.Drawing.Point(46, 26);
            this.junção114.Margin = new System.Windows.Forms.Padding(0);
            this.junção114.Name = "junção114";
            this.junção114.Size = new System.Drawing.Size(35, 35);
            this.junção114.TabIndex = 337;
            // 
            // independenciaTanque1
            // 
            this.independenciaTanque1.BackColor = System.Drawing.Color.Transparent;
            this.independenciaTanque1.Cano = 2;
            this.independenciaTanque1.CanoCheio = true;
            this.independenciaTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaTanque1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaTanque1.enableTransparentBackground = true;
            this.independenciaTanque1.LarguraCano = 13;
            this.independenciaTanque1.Location = new System.Drawing.Point(33, 54);
            this.independenciaTanque1.MaxVidro = 127;
            this.independenciaTanque1.MinVidro = 8;
            this.independenciaTanque1.Name = "independenciaTanque1";
            this.independenciaTanque1.PosCano = 15;
            this.independenciaTanque1.RaioYElipse = 30;
            this.independenciaTanque1.Size = new System.Drawing.Size(240, 180);
            this.independenciaTanque1.TabIndex = 178;
            this.independenciaTanque1.Value = 40D;
            // 
            // junçãoT69
            // 
            this.junçãoT69.Flip = 90;
            this.junçãoT69.Location = new System.Drawing.Point(0, 365);
            this.junçãoT69.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT69.Name = "junçãoT69";
            this.junçãoT69.Size = new System.Drawing.Size(42, 44);
            this.junçãoT69.TabIndex = 345;
            // 
            // junção116
            // 
            this.junção116.Flip = 90;
            this.junção116.Location = new System.Drawing.Point(5, 186);
            this.junção116.Margin = new System.Windows.Forms.Padding(0);
            this.junção116.Name = "junção116";
            this.junção116.Size = new System.Drawing.Size(35, 35);
            this.junção116.TabIndex = 344;
            // 
            // junção115
            // 
            this.junção115.Flip = 180;
            this.junção115.Location = new System.Drawing.Point(381, 25);
            this.junção115.Margin = new System.Windows.Forms.Padding(0);
            this.junção115.Name = "junção115";
            this.junção115.Size = new System.Drawing.Size(29, 32);
            this.junção115.TabIndex = 343;
            // 
            // junçãoT68
            // 
            this.junçãoT68.Flip = 180;
            this.junçãoT68.Location = new System.Drawing.Point(309, 19);
            this.junçãoT68.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT68.Name = "junçãoT68";
            this.junçãoT68.Size = new System.Drawing.Size(42, 44);
            this.junçãoT68.TabIndex = 338;
            // 
            // independenciaCanoPocoSolt2
            // 
            this.independenciaCanoPocoSolt2.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPocoSolt2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPocoSolt2.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPocoSolt2.diminuirbolha = 5D;
            this.independenciaCanoPocoSolt2.enableTransparentBackground = true;
            this.independenciaCanoPocoSolt2.fliped = false;
            this.independenciaCanoPocoSolt2.fluxo = 1;
            this.independenciaCanoPocoSolt2.Location = new System.Drawing.Point(346, 28);
            this.independenciaCanoPocoSolt2.MaxVidro = 127;
            this.independenciaCanoPocoSolt2.MinVidro = 8;
            this.independenciaCanoPocoSolt2.Name = "independenciaCanoPocoSolt2";
            this.independenciaCanoPocoSolt2.RaioYElipse = 30;
            this.independenciaCanoPocoSolt2.Size = new System.Drawing.Size(37, 13);
            this.independenciaCanoPocoSolt2.TabIndex = 340;
            this.independenciaCanoPocoSolt2.vertical = false;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(370, 133);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(83, 16);
            this.label102.TabIndex = 336;
            this.label102.Text = "Poço Solt2";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(287, 134);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(83, 16);
            this.label103.TabIndex = 335;
            this.label103.Text = "Poço Solt1";
            // 
            // independenciaPocoSolt1
            // 
            this.independenciaPocoSolt1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPocoSolt1.LigarBomba = true;
            this.independenciaPocoSolt1.Location = new System.Drawing.Point(291, 52);
            this.independenciaPocoSolt1.Name = "independenciaPocoSolt1";
            this.independenciaPocoSolt1.Size = new System.Drawing.Size(66, 78);
            this.independenciaPocoSolt1.TabIndex = 341;
            this.independenciaPocoSolt1.Transparence = false;
            // 
            // independenciaPocoSolt2
            // 
            this.independenciaPocoSolt2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPocoSolt2.LigarBomba = true;
            this.independenciaPocoSolt2.Location = new System.Drawing.Point(363, 51);
            this.independenciaPocoSolt2.Name = "independenciaPocoSolt2";
            this.independenciaPocoSolt2.Size = new System.Drawing.Size(66, 78);
            this.independenciaPocoSolt2.TabIndex = 342;
            this.independenciaPocoSolt2.Transparence = false;
            // 
            // independenciaCanoPocoSolt1
            // 
            this.independenciaCanoPocoSolt1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPocoSolt1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPocoSolt1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPocoSolt1.diminuirbolha = 5D;
            this.independenciaCanoPocoSolt1.enableTransparentBackground = true;
            this.independenciaCanoPocoSolt1.fliped = false;
            this.independenciaCanoPocoSolt1.fluxo = 1;
            this.independenciaCanoPocoSolt1.Location = new System.Drawing.Point(73, 29);
            this.independenciaCanoPocoSolt1.MaxVidro = 127;
            this.independenciaCanoPocoSolt1.MinVidro = 8;
            this.independenciaCanoPocoSolt1.Name = "independenciaCanoPocoSolt1";
            this.independenciaCanoPocoSolt1.RaioYElipse = 30;
            this.independenciaCanoPocoSolt1.Size = new System.Drawing.Size(247, 13);
            this.independenciaCanoPocoSolt1.TabIndex = 339;
            this.independenciaCanoPocoSolt1.vertical = false;
            // 
            // junçãoT64
            // 
            this.junçãoT64.Flip = 180;
            this.junçãoT64.Location = new System.Drawing.Point(1190, 165);
            this.junçãoT64.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT64.Name = "junçãoT64";
            this.junçãoT64.Size = new System.Drawing.Size(42, 44);
            this.junçãoT64.TabIndex = 325;
            // 
            // junçãoT65
            // 
            this.junçãoT65.Flip = 180;
            this.junçãoT65.Location = new System.Drawing.Point(1118, 165);
            this.junçãoT65.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT65.Name = "junçãoT65";
            this.junçãoT65.Size = new System.Drawing.Size(42, 44);
            this.junçãoT65.TabIndex = 323;
            // 
            // junçãoT66
            // 
            this.junçãoT66.Flip = 180;
            this.junçãoT66.Location = new System.Drawing.Point(1048, 165);
            this.junçãoT66.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT66.Name = "junçãoT66";
            this.junçãoT66.Size = new System.Drawing.Size(42, 44);
            this.junçãoT66.TabIndex = 321;
            // 
            // junçãoT67
            // 
            this.junçãoT67.Flip = 180;
            this.junçãoT67.Location = new System.Drawing.Point(978, 165);
            this.junçãoT67.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT67.Name = "junçãoT67";
            this.junçãoT67.Size = new System.Drawing.Size(42, 44);
            this.junçãoT67.TabIndex = 319;
            // 
            // independenciaCanoPoco9
            // 
            this.independenciaCanoPoco9.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco9.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco9.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco9.diminuirbolha = 5D;
            this.independenciaCanoPoco9.enableTransparentBackground = true;
            this.independenciaCanoPoco9.fliped = false;
            this.independenciaCanoPoco9.fluxo = 1;
            this.independenciaCanoPoco9.Location = new System.Drawing.Point(1015, 174);
            this.independenciaCanoPoco9.MaxVidro = 127;
            this.independenciaCanoPoco9.MinVidro = 8;
            this.independenciaCanoPoco9.Name = "independenciaCanoPoco9";
            this.independenciaCanoPoco9.RaioYElipse = 30;
            this.independenciaCanoPoco9.Size = new System.Drawing.Size(37, 13);
            this.independenciaCanoPoco9.TabIndex = 322;
            this.independenciaCanoPoco9.vertical = false;
            // 
            // independenciaPoco12
            // 
            this.independenciaPoco12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco12.LigarBomba = true;
            this.independenciaPoco12.Location = new System.Drawing.Point(1242, 196);
            this.independenciaPoco12.Name = "independenciaPoco12";
            this.independenciaPoco12.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco12.TabIndex = 334;
            this.independenciaPoco12.Transparence = false;
            // 
            // independenciaPoco11
            // 
            this.independenciaPoco11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco11.LigarBomba = true;
            this.independenciaPoco11.Location = new System.Drawing.Point(1172, 196);
            this.independenciaPoco11.Name = "independenciaPoco11";
            this.independenciaPoco11.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco11.TabIndex = 333;
            this.independenciaPoco11.Transparence = false;
            // 
            // independenciaPoco10
            // 
            this.independenciaPoco10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco10.LigarBomba = true;
            this.independenciaPoco10.Location = new System.Drawing.Point(1100, 196);
            this.independenciaPoco10.Name = "independenciaPoco10";
            this.independenciaPoco10.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco10.TabIndex = 332;
            this.independenciaPoco10.Transparence = false;
            // 
            // independenciaPoco9
            // 
            this.independenciaPoco9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco9.LigarBomba = true;
            this.independenciaPoco9.Location = new System.Drawing.Point(1030, 196);
            this.independenciaPoco9.Name = "independenciaPoco9";
            this.independenciaPoco9.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco9.TabIndex = 331;
            this.independenciaPoco9.Transparence = false;
            // 
            // independenciaCanoPoco11
            // 
            this.independenciaCanoPoco11.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco11.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco11.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco11.diminuirbolha = 5D;
            this.independenciaCanoPoco11.enableTransparentBackground = true;
            this.independenciaCanoPoco11.fliped = false;
            this.independenciaCanoPoco11.fluxo = 1;
            this.independenciaCanoPoco11.Location = new System.Drawing.Point(1154, 174);
            this.independenciaCanoPoco11.MaxVidro = 127;
            this.independenciaCanoPoco11.MinVidro = 8;
            this.independenciaCanoPoco11.Name = "independenciaCanoPoco11";
            this.independenciaCanoPoco11.RaioYElipse = 30;
            this.independenciaCanoPoco11.Size = new System.Drawing.Size(47, 13);
            this.independenciaCanoPoco11.TabIndex = 326;
            this.independenciaCanoPoco11.vertical = false;
            // 
            // independenciaCanoPoco10
            // 
            this.independenciaCanoPoco10.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco10.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco10.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco10.diminuirbolha = 5D;
            this.independenciaCanoPoco10.enableTransparentBackground = true;
            this.independenciaCanoPoco10.fliped = false;
            this.independenciaCanoPoco10.fluxo = 1;
            this.independenciaCanoPoco10.Location = new System.Drawing.Point(1080, 174);
            this.independenciaCanoPoco10.MaxVidro = 127;
            this.independenciaCanoPoco10.MinVidro = 8;
            this.independenciaCanoPoco10.Name = "independenciaCanoPoco10";
            this.independenciaCanoPoco10.RaioYElipse = 30;
            this.independenciaCanoPoco10.Size = new System.Drawing.Size(48, 13);
            this.independenciaCanoPoco10.TabIndex = 324;
            this.independenciaCanoPoco10.vertical = false;
            // 
            // junção111
            // 
            this.junção111.Flip = 0;
            this.junção111.Location = new System.Drawing.Point(127, 228);
            this.junção111.Margin = new System.Windows.Forms.Padding(0);
            this.junção111.Name = "junção111";
            this.junção111.Size = new System.Drawing.Size(35, 35);
            this.junção111.TabIndex = 318;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(1249, 277);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(70, 16);
            this.label96.TabIndex = 317;
            this.label96.Text = "POÇO 12";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(1179, 277);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(70, 16);
            this.label97.TabIndex = 316;
            this.label97.Text = "POÇO 11";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(1107, 277);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(70, 16);
            this.label98.TabIndex = 315;
            this.label98.Text = "POÇO 10";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(1037, 277);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(62, 16);
            this.label99.TabIndex = 314;
            this.label99.Text = "POÇO 9";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(967, 277);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(62, 16);
            this.label100.TabIndex = 313;
            this.label100.Text = "POÇO 8";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(895, 277);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(62, 16);
            this.label101.TabIndex = 312;
            this.label101.Text = "POÇO 7";
            // 
            // independenciaPoco7
            // 
            this.independenciaPoco7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco7.LigarBomba = true;
            this.independenciaPoco7.Location = new System.Drawing.Point(888, 197);
            this.independenciaPoco7.Name = "independenciaPoco7";
            this.independenciaPoco7.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco7.TabIndex = 329;
            this.independenciaPoco7.Transparence = false;
            // 
            // independenciaPoco8
            // 
            this.independenciaPoco8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco8.LigarBomba = true;
            this.independenciaPoco8.Location = new System.Drawing.Point(960, 196);
            this.independenciaPoco8.Name = "independenciaPoco8";
            this.independenciaPoco8.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco8.TabIndex = 330;
            this.independenciaPoco8.Transparence = false;
            // 
            // independenciaCanoPoco8
            // 
            this.independenciaCanoPoco8.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco8.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco8.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco8.diminuirbolha = 5D;
            this.independenciaCanoPoco8.enableTransparentBackground = true;
            this.independenciaCanoPoco8.fliped = false;
            this.independenciaCanoPoco8.fluxo = 1;
            this.independenciaCanoPoco8.Location = new System.Drawing.Point(945, 174);
            this.independenciaCanoPoco8.MaxVidro = 127;
            this.independenciaCanoPoco8.MinVidro = 8;
            this.independenciaCanoPoco8.Name = "independenciaCanoPoco8";
            this.independenciaCanoPoco8.RaioYElipse = 30;
            this.independenciaCanoPoco8.Size = new System.Drawing.Size(39, 13);
            this.independenciaCanoPoco8.TabIndex = 320;
            this.independenciaCanoPoco8.vertical = false;
            // 
            // junção113
            // 
            this.junção113.Flip = 180;
            this.junção113.Location = new System.Drawing.Point(1257, 4);
            this.junção113.Margin = new System.Windows.Forms.Padding(0);
            this.junção113.Name = "junção113";
            this.junção113.Size = new System.Drawing.Size(29, 32);
            this.junção113.TabIndex = 308;
            // 
            // junçãoT62
            // 
            this.junçãoT62.Flip = 90;
            this.junçãoT62.Location = new System.Drawing.Point(555, 56);
            this.junçãoT62.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT62.Name = "junçãoT62";
            this.junçãoT62.Size = new System.Drawing.Size(42, 44);
            this.junçãoT62.TabIndex = 309;
            // 
            // junçãoT58
            // 
            this.junçãoT58.Flip = 180;
            this.junçãoT58.Location = new System.Drawing.Point(1187, -2);
            this.junçãoT58.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT58.Name = "junçãoT58";
            this.junçãoT58.Size = new System.Drawing.Size(42, 44);
            this.junçãoT58.TabIndex = 297;
            // 
            // junçãoT59
            // 
            this.junçãoT59.Flip = 180;
            this.junçãoT59.Location = new System.Drawing.Point(1115, -2);
            this.junçãoT59.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT59.Name = "junçãoT59";
            this.junçãoT59.Size = new System.Drawing.Size(42, 44);
            this.junçãoT59.TabIndex = 295;
            // 
            // junçãoT60
            // 
            this.junçãoT60.Flip = 180;
            this.junçãoT60.Location = new System.Drawing.Point(1045, -2);
            this.junçãoT60.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT60.Name = "junçãoT60";
            this.junçãoT60.Size = new System.Drawing.Size(42, 44);
            this.junçãoT60.TabIndex = 293;
            // 
            // junçãoT61
            // 
            this.junçãoT61.Flip = 180;
            this.junçãoT61.Location = new System.Drawing.Point(975, -2);
            this.junçãoT61.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT61.Name = "junçãoT61";
            this.junçãoT61.Size = new System.Drawing.Size(42, 44);
            this.junçãoT61.TabIndex = 290;
            // 
            // independenciaCanoPoco3
            // 
            this.independenciaCanoPoco3.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco3.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco3.diminuirbolha = 5D;
            this.independenciaCanoPoco3.enableTransparentBackground = true;
            this.independenciaCanoPoco3.fliped = false;
            this.independenciaCanoPoco3.fluxo = 1;
            this.independenciaCanoPoco3.Location = new System.Drawing.Point(1012, 7);
            this.independenciaCanoPoco3.MaxVidro = 127;
            this.independenciaCanoPoco3.MinVidro = 8;
            this.independenciaCanoPoco3.Name = "independenciaCanoPoco3";
            this.independenciaCanoPoco3.RaioYElipse = 30;
            this.independenciaCanoPoco3.Size = new System.Drawing.Size(37, 13);
            this.independenciaCanoPoco3.TabIndex = 294;
            this.independenciaCanoPoco3.vertical = false;
            // 
            // independenciaPoco6
            // 
            this.independenciaPoco6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco6.LigarBomba = true;
            this.independenciaPoco6.Location = new System.Drawing.Point(1239, 29);
            this.independenciaPoco6.Name = "independenciaPoco6";
            this.independenciaPoco6.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco6.TabIndex = 307;
            this.independenciaPoco6.Transparence = false;
            // 
            // independenciaPoco5
            // 
            this.independenciaPoco5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco5.LigarBomba = true;
            this.independenciaPoco5.Location = new System.Drawing.Point(1169, 29);
            this.independenciaPoco5.Name = "independenciaPoco5";
            this.independenciaPoco5.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco5.TabIndex = 306;
            this.independenciaPoco5.Transparence = false;
            // 
            // independenciaPoco4
            // 
            this.independenciaPoco4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco4.LigarBomba = true;
            this.independenciaPoco4.Location = new System.Drawing.Point(1097, 29);
            this.independenciaPoco4.Name = "independenciaPoco4";
            this.independenciaPoco4.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco4.TabIndex = 305;
            this.independenciaPoco4.Transparence = false;
            // 
            // independenciaPoco3
            // 
            this.independenciaPoco3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco3.LigarBomba = true;
            this.independenciaPoco3.Location = new System.Drawing.Point(1027, 29);
            this.independenciaPoco3.Name = "independenciaPoco3";
            this.independenciaPoco3.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco3.TabIndex = 304;
            this.independenciaPoco3.Transparence = false;
            // 
            // independenciaCano2Poco7
            // 
            this.independenciaCano2Poco7.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCano2Poco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCano2Poco7.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCano2Poco7.diminuirbolha = 5D;
            this.independenciaCano2Poco7.enableTransparentBackground = true;
            this.independenciaCano2Poco7.fliped = false;
            this.independenciaCano2Poco7.fluxo = 1;
            this.independenciaCano2Poco7.Location = new System.Drawing.Point(588, 72);
            this.independenciaCano2Poco7.MaxVidro = 127;
            this.independenciaCano2Poco7.MinVidro = 8;
            this.independenciaCano2Poco7.Name = "independenciaCano2Poco7";
            this.independenciaCano2Poco7.RaioYElipse = 30;
            this.independenciaCano2Poco7.Size = new System.Drawing.Size(234, 13);
            this.independenciaCano2Poco7.TabIndex = 301;
            this.independenciaCano2Poco7.vertical = false;
            // 
            // independenciaCanoPoco6
            // 
            this.independenciaCanoPoco6.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco6.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco6.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco6.diminuirbolha = 5D;
            this.independenciaCanoPoco6.enableTransparentBackground = true;
            this.independenciaCanoPoco6.fliped = false;
            this.independenciaCanoPoco6.fluxo = 1;
            this.independenciaCanoPoco6.Location = new System.Drawing.Point(1222, 7);
            this.independenciaCanoPoco6.MaxVidro = 127;
            this.independenciaCanoPoco6.MinVidro = 8;
            this.independenciaCanoPoco6.Name = "independenciaCanoPoco6";
            this.independenciaCanoPoco6.RaioYElipse = 30;
            this.independenciaCanoPoco6.Size = new System.Drawing.Size(40, 13);
            this.independenciaCanoPoco6.TabIndex = 300;
            this.independenciaCanoPoco6.vertical = false;
            // 
            // independenciaCanoPoco5
            // 
            this.independenciaCanoPoco5.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco5.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco5.diminuirbolha = 5D;
            this.independenciaCanoPoco5.enableTransparentBackground = true;
            this.independenciaCanoPoco5.fliped = false;
            this.independenciaCanoPoco5.fluxo = 1;
            this.independenciaCanoPoco5.Location = new System.Drawing.Point(1151, 7);
            this.independenciaCanoPoco5.MaxVidro = 127;
            this.independenciaCanoPoco5.MinVidro = 8;
            this.independenciaCanoPoco5.Name = "independenciaCanoPoco5";
            this.independenciaCanoPoco5.RaioYElipse = 30;
            this.independenciaCanoPoco5.Size = new System.Drawing.Size(47, 13);
            this.independenciaCanoPoco5.TabIndex = 298;
            this.independenciaCanoPoco5.vertical = false;
            // 
            // independenciaCanoPoco4
            // 
            this.independenciaCanoPoco4.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco4.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco4.diminuirbolha = 5D;
            this.independenciaCanoPoco4.enableTransparentBackground = true;
            this.independenciaCanoPoco4.fliped = false;
            this.independenciaCanoPoco4.fluxo = 1;
            this.independenciaCanoPoco4.Location = new System.Drawing.Point(1077, 7);
            this.independenciaCanoPoco4.MaxVidro = 127;
            this.independenciaCanoPoco4.MinVidro = 8;
            this.independenciaCanoPoco4.Name = "independenciaCanoPoco4";
            this.independenciaCanoPoco4.RaioYElipse = 30;
            this.independenciaCanoPoco4.Size = new System.Drawing.Size(48, 13);
            this.independenciaCanoPoco4.TabIndex = 296;
            this.independenciaCanoPoco4.vertical = false;
            // 
            // junção112
            // 
            this.junção112.Flip = 0;
            this.junção112.Location = new System.Drawing.Point(828, 154);
            this.junção112.Margin = new System.Windows.Forms.Padding(0);
            this.junção112.Name = "junção112";
            this.junção112.Size = new System.Drawing.Size(35, 35);
            this.junção112.TabIndex = 289;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(1246, 110);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(62, 16);
            this.label90.TabIndex = 288;
            this.label90.Text = "POÇO 6";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(1176, 110);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(62, 16);
            this.label91.TabIndex = 287;
            this.label91.Text = "POÇO 5";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(1104, 110);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(62, 16);
            this.label92.TabIndex = 286;
            this.label92.Text = "POÇO 4";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(1034, 110);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(62, 16);
            this.label93.TabIndex = 285;
            this.label93.Text = "POÇO 3";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(964, 110);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(62, 16);
            this.label94.TabIndex = 284;
            this.label94.Text = "POÇO 2";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(892, 110);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(62, 16);
            this.label95.TabIndex = 283;
            this.label95.Text = "POÇO 1";
            // 
            // independenciaTanque4
            // 
            this.independenciaTanque4.BackColor = System.Drawing.Color.Transparent;
            this.independenciaTanque4.Cano = 2;
            this.independenciaTanque4.CanoCheio = true;
            this.independenciaTanque4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaTanque4.CorVidro = System.Drawing.Color.Silver;
            this.independenciaTanque4.enableTransparentBackground = true;
            this.independenciaTanque4.LarguraCano = 13;
            this.independenciaTanque4.Location = new System.Drawing.Point(547, 93);
            this.independenciaTanque4.MaxVidro = 127;
            this.independenciaTanque4.MinVidro = 8;
            this.independenciaTanque4.Name = "independenciaTanque4";
            this.independenciaTanque4.PosCano = 15;
            this.independenciaTanque4.RaioYElipse = 30;
            this.independenciaTanque4.Size = new System.Drawing.Size(244, 217);
            this.independenciaTanque4.TabIndex = 282;
            this.independenciaTanque4.Value = 40D;
            // 
            // independenciaPoco1
            // 
            this.independenciaPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco1.LigarBomba = true;
            this.independenciaPoco1.Location = new System.Drawing.Point(885, 30);
            this.independenciaPoco1.Name = "independenciaPoco1";
            this.independenciaPoco1.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco1.TabIndex = 302;
            this.independenciaPoco1.Transparence = false;
            // 
            // independenciaPoco2
            // 
            this.independenciaPoco2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.independenciaPoco2.LigarBomba = true;
            this.independenciaPoco2.Location = new System.Drawing.Point(957, 29);
            this.independenciaPoco2.Name = "independenciaPoco2";
            this.independenciaPoco2.Size = new System.Drawing.Size(66, 78);
            this.independenciaPoco2.TabIndex = 303;
            this.independenciaPoco2.Transparence = false;
            // 
            // independenciaCanoPoco2
            // 
            this.independenciaCanoPoco2.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco2.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco2.diminuirbolha = 5D;
            this.independenciaCanoPoco2.enableTransparentBackground = true;
            this.independenciaCanoPoco2.fliped = false;
            this.independenciaCanoPoco2.fluxo = 1;
            this.independenciaCanoPoco2.Location = new System.Drawing.Point(942, 7);
            this.independenciaCanoPoco2.MaxVidro = 127;
            this.independenciaCanoPoco2.MinVidro = 8;
            this.independenciaCanoPoco2.Name = "independenciaCanoPoco2";
            this.independenciaCanoPoco2.RaioYElipse = 30;
            this.independenciaCanoPoco2.Size = new System.Drawing.Size(39, 13);
            this.independenciaCanoPoco2.TabIndex = 291;
            this.independenciaCanoPoco2.vertical = false;
            // 
            // independenciaCanoTanque2
            // 
            this.independenciaCanoTanque2.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoTanque2.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoTanque2.diminuirbolha = 5D;
            this.independenciaCanoTanque2.enableTransparentBackground = true;
            this.independenciaCanoTanque2.fliped = false;
            this.independenciaCanoTanque2.fluxo = -1;
            this.independenciaCanoTanque2.Location = new System.Drawing.Point(8, 208);
            this.independenciaCanoTanque2.MaxVidro = 127;
            this.independenciaCanoTanque2.MinVidro = 8;
            this.independenciaCanoTanque2.Name = "independenciaCanoTanque2";
            this.independenciaCanoTanque2.RaioYElipse = 30;
            this.independenciaCanoTanque2.Size = new System.Drawing.Size(13, 170);
            this.independenciaCanoTanque2.TabIndex = 281;
            this.independenciaCanoTanque2.vertical = true;
            // 
            // independenciaCanoTanque3
            // 
            this.independenciaCanoTanque3.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoTanque3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoTanque3.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoTanque3.diminuirbolha = 5D;
            this.independenciaCanoTanque3.enableTransparentBackground = true;
            this.independenciaCanoTanque3.fliped = false;
            this.independenciaCanoTanque3.fluxo = -1;
            this.independenciaCanoTanque3.Location = new System.Drawing.Point(8, 394);
            this.independenciaCanoTanque3.MaxVidro = 127;
            this.independenciaCanoTanque3.MinVidro = 8;
            this.independenciaCanoTanque3.Name = "independenciaCanoTanque3";
            this.independenciaCanoTanque3.RaioYElipse = 30;
            this.independenciaCanoTanque3.Size = new System.Drawing.Size(13, 133);
            this.independenciaCanoTanque3.TabIndex = 346;
            this.independenciaCanoTanque3.vertical = true;
            // 
            // independenciaCanoPoco12
            // 
            this.independenciaCanoPoco12.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco12.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco12.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco12.diminuirbolha = 5D;
            this.independenciaCanoPoco12.enableTransparentBackground = true;
            this.independenciaCanoPoco12.fliped = false;
            this.independenciaCanoPoco12.fluxo = 1;
            this.independenciaCanoPoco12.Location = new System.Drawing.Point(1226, 174);
            this.independenciaCanoPoco12.MaxVidro = 127;
            this.independenciaCanoPoco12.MinVidro = 8;
            this.independenciaCanoPoco12.Name = "independenciaCanoPoco12";
            this.independenciaCanoPoco12.RaioYElipse = 30;
            this.independenciaCanoPoco12.Size = new System.Drawing.Size(40, 13);
            this.independenciaCanoPoco12.TabIndex = 348;
            this.independenciaCanoPoco12.vertical = false;
            // 
            // independenciaCanoTanque4
            // 
            this.independenciaCanoTanque4.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoTanque4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoTanque4.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoTanque4.diminuirbolha = 5D;
            this.independenciaCanoTanque4.enableTransparentBackground = true;
            this.independenciaCanoTanque4.fliped = false;
            this.independenciaCanoTanque4.fluxo = 1;
            this.independenciaCanoTanque4.Location = new System.Drawing.Point(563, 27);
            this.independenciaCanoTanque4.MaxVidro = 127;
            this.independenciaCanoTanque4.MinVidro = 8;
            this.independenciaCanoTanque4.Name = "independenciaCanoTanque4";
            this.independenciaCanoTanque4.RaioYElipse = 30;
            this.independenciaCanoTanque4.Size = new System.Drawing.Size(13, 36);
            this.independenciaCanoTanque4.TabIndex = 357;
            this.independenciaCanoTanque4.vertical = true;
            // 
            // independenciaCanoPoco1
            // 
            this.independenciaCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco1.diminuirbolha = 5D;
            this.independenciaCanoPoco1.enableTransparentBackground = true;
            this.independenciaCanoPoco1.fliped = false;
            this.independenciaCanoPoco1.fluxo = 1;
            this.independenciaCanoPoco1.Location = new System.Drawing.Point(579, 6);
            this.independenciaCanoPoco1.MaxVidro = 127;
            this.independenciaCanoPoco1.MinVidro = 8;
            this.independenciaCanoPoco1.Name = "independenciaCanoPoco1";
            this.independenciaCanoPoco1.RaioYElipse = 30;
            this.independenciaCanoPoco1.Size = new System.Drawing.Size(333, 13);
            this.independenciaCanoPoco1.TabIndex = 359;
            this.independenciaCanoPoco1.vertical = false;
            // 
            // independenciaCano1Poco7
            // 
            this.independenciaCano1Poco7.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCano1Poco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCano1Poco7.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCano1Poco7.diminuirbolha = 5D;
            this.independenciaCano1Poco7.enableTransparentBackground = true;
            this.independenciaCano1Poco7.fliped = false;
            this.independenciaCano1Poco7.fluxo = 1;
            this.independenciaCano1Poco7.Location = new System.Drawing.Point(830, 85);
            this.independenciaCano1Poco7.MaxVidro = 127;
            this.independenciaCano1Poco7.MinVidro = 8;
            this.independenciaCano1Poco7.Name = "independenciaCano1Poco7";
            this.independenciaCano1Poco7.RaioYElipse = 30;
            this.independenciaCano1Poco7.Size = new System.Drawing.Size(13, 80);
            this.independenciaCano1Poco7.TabIndex = 361;
            this.independenciaCano1Poco7.vertical = true;
            // 
            // independenciaCanoPoco7
            // 
            this.independenciaCanoPoco7.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoPoco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoPoco7.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoPoco7.diminuirbolha = 5D;
            this.independenciaCanoPoco7.enableTransparentBackground = true;
            this.independenciaCanoPoco7.fliped = false;
            this.independenciaCanoPoco7.fluxo = 1;
            this.independenciaCanoPoco7.Location = new System.Drawing.Point(854, 174);
            this.independenciaCanoPoco7.MaxVidro = 127;
            this.independenciaCanoPoco7.MinVidro = 8;
            this.independenciaCanoPoco7.Name = "independenciaCanoPoco7";
            this.independenciaCanoPoco7.RaioYElipse = 30;
            this.independenciaCanoPoco7.Size = new System.Drawing.Size(61, 13);
            this.independenciaCanoPoco7.TabIndex = 362;
            this.independenciaCanoPoco7.vertical = false;
            // 
            // independenciaCano3Tanque1
            // 
            this.independenciaCano3Tanque1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCano3Tanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCano3Tanque1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCano3Tanque1.diminuirbolha = 5D;
            this.independenciaCano3Tanque1.enableTransparentBackground = true;
            this.independenciaCano3Tanque1.fliped = false;
            this.independenciaCano3Tanque1.fluxo = -1;
            this.independenciaCano3Tanque1.Location = new System.Drawing.Point(487, 7);
            this.independenciaCano3Tanque1.MaxVidro = 127;
            this.independenciaCano3Tanque1.MinVidro = 8;
            this.independenciaCano3Tanque1.Name = "independenciaCano3Tanque1";
            this.independenciaCano3Tanque1.RaioYElipse = 30;
            this.independenciaCano3Tanque1.Size = new System.Drawing.Size(72, 13);
            this.independenciaCano3Tanque1.TabIndex = 364;
            this.independenciaCano3Tanque1.vertical = false;
            // 
            // independenciaCano2Tanque1
            // 
            this.independenciaCano2Tanque1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCano2Tanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCano2Tanque1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCano2Tanque1.diminuirbolha = 5D;
            this.independenciaCano2Tanque1.enableTransparentBackground = true;
            this.independenciaCano2Tanque1.fliped = false;
            this.independenciaCano2Tanque1.fluxo = 1;
            this.independenciaCano2Tanque1.Location = new System.Drawing.Point(470, 30);
            this.independenciaCano2Tanque1.MaxVidro = 127;
            this.independenciaCano2Tanque1.MinVidro = 8;
            this.independenciaCano2Tanque1.Name = "independenciaCano2Tanque1";
            this.independenciaCano2Tanque1.RaioYElipse = 30;
            this.independenciaCano2Tanque1.Size = new System.Drawing.Size(13, 204);
            this.independenciaCano2Tanque1.TabIndex = 365;
            this.independenciaCano2Tanque1.vertical = true;
            // 
            // independenciaCanoTanque1
            // 
            this.independenciaCanoTanque1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoTanque1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoTanque1.diminuirbolha = 5D;
            this.independenciaCanoTanque1.enableTransparentBackground = true;
            this.independenciaCanoTanque1.fliped = false;
            this.independenciaCanoTanque1.fluxo = -1;
            this.independenciaCanoTanque1.Location = new System.Drawing.Point(151, 246);
            this.independenciaCanoTanque1.MaxVidro = 127;
            this.independenciaCanoTanque1.MinVidro = 8;
            this.independenciaCanoTanque1.Name = "independenciaCanoTanque1";
            this.independenciaCanoTanque1.RaioYElipse = 30;
            this.independenciaCanoTanque1.Size = new System.Drawing.Size(308, 13);
            this.independenciaCanoTanque1.TabIndex = 351;
            this.independenciaCanoTanque1.vertical = false;
            // 
            // independenciaCanoBomba2
            // 
            this.independenciaCanoBomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoBomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoBomba2.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoBomba2.diminuirbolha = 5D;
            this.independenciaCanoBomba2.enableTransparentBackground = true;
            this.independenciaCanoBomba2.fliped = false;
            this.independenciaCanoBomba2.fluxo = -1;
            this.independenciaCanoBomba2.Location = new System.Drawing.Point(657, 298);
            this.independenciaCanoBomba2.MaxVidro = 127;
            this.independenciaCanoBomba2.MinVidro = 8;
            this.independenciaCanoBomba2.Name = "independenciaCanoBomba2";
            this.independenciaCanoBomba2.RaioYElipse = 30;
            this.independenciaCanoBomba2.Size = new System.Drawing.Size(13, 52);
            this.independenciaCanoBomba2.TabIndex = 377;
            this.independenciaCanoBomba2.vertical = true;
            // 
            // independenciaCano2Bomba1
            // 
            this.independenciaCano2Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCano2Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCano2Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCano2Bomba1.diminuirbolha = 5D;
            this.independenciaCano2Bomba1.enableTransparentBackground = true;
            this.independenciaCano2Bomba1.fliped = false;
            this.independenciaCano2Bomba1.fluxo = 0;
            this.independenciaCano2Bomba1.Location = new System.Drawing.Point(626, 340);
            this.independenciaCano2Bomba1.MaxVidro = 127;
            this.independenciaCano2Bomba1.MinVidro = 8;
            this.independenciaCano2Bomba1.Name = "independenciaCano2Bomba1";
            this.independenciaCano2Bomba1.RaioYElipse = 30;
            this.independenciaCano2Bomba1.Size = new System.Drawing.Size(13, 81);
            this.independenciaCano2Bomba1.TabIndex = 380;
            this.independenciaCano2Bomba1.vertical = true;
            // 
            // independenciaCano3Bomba1
            // 
            this.independenciaCano3Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCano3Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCano3Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCano3Bomba1.diminuirbolha = 5D;
            this.independenciaCano3Bomba1.enableTransparentBackground = true;
            this.independenciaCano3Bomba1.fliped = false;
            this.independenciaCano3Bomba1.fluxo = 0;
            this.independenciaCano3Bomba1.Location = new System.Drawing.Point(400, 429);
            this.independenciaCano3Bomba1.MaxVidro = 127;
            this.independenciaCano3Bomba1.MinVidro = 8;
            this.independenciaCano3Bomba1.Name = "independenciaCano3Bomba1";
            this.independenciaCano3Bomba1.RaioYElipse = 30;
            this.independenciaCano3Bomba1.Size = new System.Drawing.Size(801, 13);
            this.independenciaCano3Bomba1.TabIndex = 381;
            this.independenciaCano3Bomba1.vertical = false;
            // 
            // junção124
            // 
            this.junção124.Flip = 90;
            this.junção124.Location = new System.Drawing.Point(520, 261);
            this.junção124.Margin = new System.Windows.Forms.Padding(0);
            this.junção124.Name = "junção124";
            this.junção124.Size = new System.Drawing.Size(30, 32);
            this.junção124.TabIndex = 372;
            // 
            // independenciaCanoBomba1
            // 
            this.independenciaCanoBomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.independenciaCanoBomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.independenciaCanoBomba1.CorVidro = System.Drawing.Color.Silver;
            this.independenciaCanoBomba1.diminuirbolha = 5D;
            this.independenciaCanoBomba1.enableTransparentBackground = true;
            this.independenciaCanoBomba1.fliped = false;
            this.independenciaCanoBomba1.fluxo = -1;
            this.independenciaCanoBomba1.Location = new System.Drawing.Point(522, 286);
            this.independenciaCanoBomba1.MaxVidro = 127;
            this.independenciaCanoBomba1.MinVidro = 8;
            this.independenciaCanoBomba1.Name = "independenciaCanoBomba1";
            this.independenciaCanoBomba1.RaioYElipse = 30;
            this.independenciaCanoBomba1.Size = new System.Drawing.Size(13, 64);
            this.independenciaCanoBomba1.TabIndex = 354;
            this.independenciaCanoBomba1.vertical = true;
            // 
            // SaoSebastiao
            // 
            this.SaoSebastiao.Controls.Add(this.pictureBox2);
            this.SaoSebastiao.Controls.Add(this.sebastiaoVazaoBomba1);
            this.SaoSebastiao.Controls.Add(this.waterMeter4);
            this.SaoSebastiao.Controls.Add(this.sebastiaoVazaoBomba2);
            this.SaoSebastiao.Controls.Add(this.waterMeter2);
            this.SaoSebastiao.Controls.Add(this.junção110);
            this.SaoSebastiao.Controls.Add(this.label86);
            this.SaoSebastiao.Controls.Add(this.junção109);
            this.SaoSebastiao.Controls.Add(this.junção99);
            this.SaoSebastiao.Controls.Add(this.junçãoT57);
            this.SaoSebastiao.Controls.Add(this.junção105);
            this.SaoSebastiao.Controls.Add(this.junção108);
            this.SaoSebastiao.Controls.Add(this.junçãoT55);
            this.SaoSebastiao.Controls.Add(this.junçãoT56);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoPoco6);
            this.SaoSebastiao.Controls.Add(this.sebastiaoPoco5);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoPoco5);
            this.SaoSebastiao.Controls.Add(this.label87);
            this.SaoSebastiao.Controls.Add(this.label88);
            this.SaoSebastiao.Controls.Add(this.label89);
            this.SaoSebastiao.Controls.Add(this.sebastiaoPoco7);
            this.SaoSebastiao.Controls.Add(this.sebastiaoPoco6);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoPoco7);
            this.SaoSebastiao.Controls.Add(this.label85);
            this.SaoSebastiao.Controls.Add(this.junção101);
            this.SaoSebastiao.Controls.Add(this.junção102);
            this.SaoSebastiao.Controls.Add(this.junção103);
            this.SaoSebastiao.Controls.Add(this.junção104);
            this.SaoSebastiao.Controls.Add(this.sebastiaoBomba1);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoPoco4);
            this.SaoSebastiao.Controls.Add(this.junção106);
            this.SaoSebastiao.Controls.Add(this.junção107);
            this.SaoSebastiao.Controls.Add(this.sebastiaoBomba2);
            this.SaoSebastiao.Controls.Add(this.junção100);
            this.SaoSebastiao.Controls.Add(this.junçãoT53);
            this.SaoSebastiao.Controls.Add(this.junçãoT54);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoPoco2);
            this.SaoSebastiao.Controls.Add(this.sebastiaoPoco4);
            this.SaoSebastiao.Controls.Add(this.sebastiaoPoco1);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoPoco1);
            this.SaoSebastiao.Controls.Add(this.label80);
            this.SaoSebastiao.Controls.Add(this.label82);
            this.SaoSebastiao.Controls.Add(this.label83);
            this.SaoSebastiao.Controls.Add(this.label84);
            this.SaoSebastiao.Controls.Add(this.sebastiaoPoco3);
            this.SaoSebastiao.Controls.Add(this.sebastiaoPoco2);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoPoco3);
            this.SaoSebastiao.Controls.Add(this.label81);
            this.SaoSebastiao.Controls.Add(this.sebastiaoTanque1);
            this.SaoSebastiao.Controls.Add(this.sebastiaoTanque2);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoTanque2);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoBomba2);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCanoBomba1);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCano2Poco5);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCano2Poco1);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCano3Poco5);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCano1Bomba2);
            this.SaoSebastiao.Controls.Add(this.sebastiaoCano1Bomba1);
            this.SaoSebastiao.Location = new System.Drawing.Point(4, 22);
            this.SaoSebastiao.Name = "SaoSebastiao";
            this.SaoSebastiao.Size = new System.Drawing.Size(1480, 662);
            this.SaoSebastiao.TabIndex = 5;
            this.SaoSebastiao.Text = "SÃO SEBASTIAO";
            this.SaoSebastiao.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(57, 309);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.TabIndex = 288;
            this.pictureBox2.TabStop = false;
            // 
            // sebastiaoVazaoBomba1
            // 
            this.sebastiaoVazaoBomba1.AutoSize = true;
            this.sebastiaoVazaoBomba1.BackColor = System.Drawing.Color.Black;
            this.sebastiaoVazaoBomba1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sebastiaoVazaoBomba1.ForeColor = System.Drawing.Color.Transparent;
            this.sebastiaoVazaoBomba1.Location = new System.Drawing.Point(933, 386);
            this.sebastiaoVazaoBomba1.Name = "sebastiaoVazaoBomba1";
            this.sebastiaoVazaoBomba1.Size = new System.Drawing.Size(76, 16);
            this.sebastiaoVazaoBomba1.TabIndex = 287;
            this.sebastiaoVazaoBomba1.Text = "00.00 (m³/h)";
            // 
            // waterMeter4
            // 
            this.waterMeter4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter4.Location = new System.Drawing.Point(945, 328);
            this.waterMeter4.Name = "waterMeter4";
            this.waterMeter4.Size = new System.Drawing.Size(55, 55);
            this.waterMeter4.TabIndex = 286;
            this.waterMeter4.Transparence = false;
            // 
            // sebastiaoVazaoBomba2
            // 
            this.sebastiaoVazaoBomba2.AutoSize = true;
            this.sebastiaoVazaoBomba2.BackColor = System.Drawing.Color.Black;
            this.sebastiaoVazaoBomba2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sebastiaoVazaoBomba2.ForeColor = System.Drawing.Color.Transparent;
            this.sebastiaoVazaoBomba2.Location = new System.Drawing.Point(763, 494);
            this.sebastiaoVazaoBomba2.Name = "sebastiaoVazaoBomba2";
            this.sebastiaoVazaoBomba2.Size = new System.Drawing.Size(76, 16);
            this.sebastiaoVazaoBomba2.TabIndex = 285;
            this.sebastiaoVazaoBomba2.Text = "00.00 (m³/h)";
            // 
            // waterMeter2
            // 
            this.waterMeter2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter2.Location = new System.Drawing.Point(775, 435);
            this.waterMeter2.Name = "waterMeter2";
            this.waterMeter2.Size = new System.Drawing.Size(55, 55);
            this.waterMeter2.TabIndex = 284;
            this.waterMeter2.Transparence = false;
            // 
            // junção110
            // 
            this.junção110.Flip = 90;
            this.junção110.Location = new System.Drawing.Point(38, 11);
            this.junção110.Margin = new System.Windows.Forms.Padding(0);
            this.junção110.Name = "junção110";
            this.junção110.Size = new System.Drawing.Size(30, 32);
            this.junção110.TabIndex = 283;
            // 
            // label86
            // 
            this.label86.BackColor = System.Drawing.Color.Gainsboro;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(874, 34);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(142, 69);
            this.label86.TabIndex = 249;
            this.label86.Text = "PARTE ALTA BAIRRO GOIÁS";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção109
            // 
            this.junção109.Flip = 90;
            this.junção109.Location = new System.Drawing.Point(320, 106);
            this.junção109.Margin = new System.Windows.Forms.Padding(0);
            this.junção109.Name = "junção109";
            this.junção109.Size = new System.Drawing.Size(30, 32);
            this.junção109.TabIndex = 282;
            // 
            // junção99
            // 
            this.junção99.Flip = 180;
            this.junção99.Location = new System.Drawing.Point(386, 44);
            this.junção99.Margin = new System.Windows.Forms.Padding(0);
            this.junção99.Name = "junção99";
            this.junção99.Size = new System.Drawing.Size(29, 32);
            this.junção99.TabIndex = 179;
            // 
            // junçãoT57
            // 
            this.junçãoT57.Flip = 270;
            this.junçãoT57.Location = new System.Drawing.Point(379, 94);
            this.junçãoT57.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT57.Name = "junçãoT57";
            this.junçãoT57.Size = new System.Drawing.Size(42, 44);
            this.junçãoT57.TabIndex = 279;
            // 
            // junção105
            // 
            this.junção105.Flip = 270;
            this.junção105.Location = new System.Drawing.Point(309, 184);
            this.junção105.Margin = new System.Windows.Forms.Padding(0);
            this.junção105.Name = "junção105";
            this.junção105.Size = new System.Drawing.Size(29, 32);
            this.junção105.TabIndex = 252;
            // 
            // junção108
            // 
            this.junção108.Flip = 90;
            this.junção108.Location = new System.Drawing.Point(102, 198);
            this.junção108.Margin = new System.Windows.Forms.Padding(0);
            this.junção108.Name = "junção108";
            this.junção108.Size = new System.Drawing.Size(30, 32);
            this.junção108.TabIndex = 278;
            // 
            // junçãoT55
            // 
            this.junçãoT55.Flip = 180;
            this.junçãoT55.Location = new System.Drawing.Point(232, 192);
            this.junçãoT55.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT55.Name = "junçãoT55";
            this.junçãoT55.Size = new System.Drawing.Size(42, 44);
            this.junçãoT55.TabIndex = 272;
            // 
            // junçãoT56
            // 
            this.junçãoT56.Flip = 180;
            this.junçãoT56.Location = new System.Drawing.Point(162, 192);
            this.junçãoT56.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT56.Name = "junçãoT56";
            this.junçãoT56.Size = new System.Drawing.Size(42, 44);
            this.junçãoT56.TabIndex = 270;
            // 
            // sebastiaoCanoPoco6
            // 
            this.sebastiaoCanoPoco6.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoPoco6.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoPoco6.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoPoco6.diminuirbolha = 5D;
            this.sebastiaoCanoPoco6.enableTransparentBackground = true;
            this.sebastiaoCanoPoco6.fliped = false;
            this.sebastiaoCanoPoco6.fluxo = -1;
            this.sebastiaoCanoPoco6.Location = new System.Drawing.Point(195, 200);
            this.sebastiaoCanoPoco6.MaxVidro = 127;
            this.sebastiaoCanoPoco6.MinVidro = 8;
            this.sebastiaoCanoPoco6.Name = "sebastiaoCanoPoco6";
            this.sebastiaoCanoPoco6.RaioYElipse = 30;
            this.sebastiaoCanoPoco6.Size = new System.Drawing.Size(55, 13);
            this.sebastiaoCanoPoco6.TabIndex = 273;
            this.sebastiaoCanoPoco6.vertical = false;
            // 
            // sebastiaoPoco5
            // 
            this.sebastiaoPoco5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoPoco5.LigarBomba = true;
            this.sebastiaoPoco5.Location = new System.Drawing.Point(214, 223);
            this.sebastiaoPoco5.Name = "sebastiaoPoco5";
            this.sebastiaoPoco5.Size = new System.Drawing.Size(66, 78);
            this.sebastiaoPoco5.TabIndex = 277;
            this.sebastiaoPoco5.Transparence = false;
            // 
            // sebastiaoCanoPoco5
            // 
            this.sebastiaoCanoPoco5.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoPoco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoPoco5.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoPoco5.diminuirbolha = 5D;
            this.sebastiaoCanoPoco5.enableTransparentBackground = true;
            this.sebastiaoCanoPoco5.fliped = false;
            this.sebastiaoCanoPoco5.fluxo = -1;
            this.sebastiaoCanoPoco5.Location = new System.Drawing.Point(263, 200);
            this.sebastiaoCanoPoco5.MaxVidro = 127;
            this.sebastiaoCanoPoco5.MinVidro = 8;
            this.sebastiaoCanoPoco5.Name = "sebastiaoCanoPoco5";
            this.sebastiaoCanoPoco5.RaioYElipse = 30;
            this.sebastiaoCanoPoco5.Size = new System.Drawing.Size(49, 13);
            this.sebastiaoCanoPoco5.TabIndex = 274;
            this.sebastiaoCanoPoco5.vertical = false;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(223, 304);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(62, 16);
            this.label87.TabIndex = 269;
            this.label87.Text = "POÇO 5";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(153, 304);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(62, 16);
            this.label88.TabIndex = 268;
            this.label88.Text = "POÇO 6";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(81, 304);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(62, 16);
            this.label89.TabIndex = 267;
            this.label89.Text = "POÇO 7";
            // 
            // sebastiaoPoco7
            // 
            this.sebastiaoPoco7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoPoco7.LigarBomba = true;
            this.sebastiaoPoco7.Location = new System.Drawing.Point(72, 223);
            this.sebastiaoPoco7.Name = "sebastiaoPoco7";
            this.sebastiaoPoco7.Size = new System.Drawing.Size(66, 78);
            this.sebastiaoPoco7.TabIndex = 275;
            this.sebastiaoPoco7.Transparence = false;
            // 
            // sebastiaoPoco6
            // 
            this.sebastiaoPoco6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoPoco6.LigarBomba = true;
            this.sebastiaoPoco6.Location = new System.Drawing.Point(145, 222);
            this.sebastiaoPoco6.Name = "sebastiaoPoco6";
            this.sebastiaoPoco6.Size = new System.Drawing.Size(66, 78);
            this.sebastiaoPoco6.TabIndex = 276;
            this.sebastiaoPoco6.Transparence = false;
            // 
            // sebastiaoCanoPoco7
            // 
            this.sebastiaoCanoPoco7.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoPoco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoPoco7.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoPoco7.diminuirbolha = 5D;
            this.sebastiaoCanoPoco7.enableTransparentBackground = true;
            this.sebastiaoCanoPoco7.fliped = false;
            this.sebastiaoCanoPoco7.fluxo = -1;
            this.sebastiaoCanoPoco7.Location = new System.Drawing.Point(129, 200);
            this.sebastiaoCanoPoco7.MaxVidro = 127;
            this.sebastiaoCanoPoco7.MinVidro = 8;
            this.sebastiaoCanoPoco7.Name = "sebastiaoCanoPoco7";
            this.sebastiaoCanoPoco7.RaioYElipse = 30;
            this.sebastiaoCanoPoco7.Size = new System.Drawing.Size(43, 13);
            this.sebastiaoCanoPoco7.TabIndex = 271;
            this.sebastiaoCanoPoco7.vertical = false;
            // 
            // label85
            // 
            this.label85.BackColor = System.Drawing.Color.Gainsboro;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(903, 489);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(142, 81);
            this.label85.TabIndex = 250;
            this.label85.Text = "BAIRRO SÃO SEBASTIÃO";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção101
            // 
            this.junção101.Flip = 180;
            this.junção101.Location = new System.Drawing.Point(923, 10);
            this.junção101.Margin = new System.Windows.Forms.Padding(0);
            this.junção101.Name = "junção101";
            this.junção101.Size = new System.Drawing.Size(29, 32);
            this.junção101.TabIndex = 257;
            // 
            // junção102
            // 
            this.junção102.Flip = 180;
            this.junção102.Location = new System.Drawing.Point(955, 466);
            this.junção102.Margin = new System.Windows.Forms.Padding(0);
            this.junção102.Name = "junção102";
            this.junção102.Size = new System.Drawing.Size(29, 32);
            this.junção102.TabIndex = 264;
            // 
            // junção103
            // 
            this.junção103.Flip = 90;
            this.junção103.Location = new System.Drawing.Point(547, 359);
            this.junção103.Margin = new System.Windows.Forms.Padding(0);
            this.junção103.Name = "junção103";
            this.junção103.Size = new System.Drawing.Size(30, 32);
            this.junção103.TabIndex = 263;
            // 
            // junção104
            // 
            this.junção104.Flip = 0;
            this.junção104.Location = new System.Drawing.Point(510, 383);
            this.junção104.Margin = new System.Windows.Forms.Padding(0);
            this.junção104.Name = "junção104";
            this.junção104.Size = new System.Drawing.Size(31, 32);
            this.junção104.TabIndex = 261;
            // 
            // sebastiaoBomba1
            // 
            this.sebastiaoBomba1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoBomba1.LigarBomba = false;
            this.sebastiaoBomba1.Location = new System.Drawing.Point(539, 383);
            this.sebastiaoBomba1.Name = "sebastiaoBomba1";
            this.sebastiaoBomba1.Size = new System.Drawing.Size(60, 50);
            this.sebastiaoBomba1.TabIndex = 260;
            this.sebastiaoBomba1.Transparence = false;
            // 
            // sebastiaoCanoPoco4
            // 
            this.sebastiaoCanoPoco4.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoPoco4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoPoco4.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoPoco4.diminuirbolha = 1D;
            this.sebastiaoCanoPoco4.enableTransparentBackground = true;
            this.sebastiaoCanoPoco4.fliped = false;
            this.sebastiaoCanoPoco4.fluxo = -1;
            this.sebastiaoCanoPoco4.Location = new System.Drawing.Point(48, 14);
            this.sebastiaoCanoPoco4.MaxVidro = 127;
            this.sebastiaoCanoPoco4.MinVidro = 8;
            this.sebastiaoCanoPoco4.Name = "sebastiaoCanoPoco4";
            this.sebastiaoCanoPoco4.RaioYElipse = 15;
            this.sebastiaoCanoPoco4.Size = new System.Drawing.Size(880, 13);
            this.sebastiaoCanoPoco4.TabIndex = 259;
            this.sebastiaoCanoPoco4.vertical = false;
            // 
            // junção106
            // 
            this.junção106.Flip = 90;
            this.junção106.Location = new System.Drawing.Point(520, 465);
            this.junção106.Margin = new System.Windows.Forms.Padding(0);
            this.junção106.Name = "junção106";
            this.junção106.Size = new System.Drawing.Size(30, 32);
            this.junção106.TabIndex = 256;
            // 
            // junção107
            // 
            this.junção107.Flip = 0;
            this.junção107.Location = new System.Drawing.Point(483, 489);
            this.junção107.Margin = new System.Windows.Forms.Padding(0);
            this.junção107.Name = "junção107";
            this.junção107.Size = new System.Drawing.Size(31, 32);
            this.junção107.TabIndex = 254;
            // 
            // sebastiaoBomba2
            // 
            this.sebastiaoBomba2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoBomba2.LigarBomba = false;
            this.sebastiaoBomba2.Location = new System.Drawing.Point(512, 489);
            this.sebastiaoBomba2.Name = "sebastiaoBomba2";
            this.sebastiaoBomba2.Size = new System.Drawing.Size(60, 50);
            this.sebastiaoBomba2.TabIndex = 253;
            this.sebastiaoBomba2.Transparence = false;
            // 
            // junção100
            // 
            this.junção100.Flip = 90;
            this.junção100.Location = new System.Drawing.Point(108, 46);
            this.junção100.Margin = new System.Windows.Forms.Padding(0);
            this.junção100.Name = "junção100";
            this.junção100.Size = new System.Drawing.Size(30, 32);
            this.junção100.TabIndex = 248;
            // 
            // junçãoT53
            // 
            this.junçãoT53.Flip = 180;
            this.junçãoT53.Location = new System.Drawing.Point(238, 40);
            this.junçãoT53.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT53.Name = "junçãoT53";
            this.junçãoT53.Size = new System.Drawing.Size(42, 44);
            this.junçãoT53.TabIndex = 239;
            // 
            // junçãoT54
            // 
            this.junçãoT54.Flip = 180;
            this.junçãoT54.Location = new System.Drawing.Point(168, 40);
            this.junçãoT54.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT54.Name = "junçãoT54";
            this.junçãoT54.Size = new System.Drawing.Size(42, 44);
            this.junçãoT54.TabIndex = 237;
            // 
            // sebastiaoCanoPoco2
            // 
            this.sebastiaoCanoPoco2.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoPoco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoPoco2.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoPoco2.diminuirbolha = 5D;
            this.sebastiaoCanoPoco2.enableTransparentBackground = true;
            this.sebastiaoCanoPoco2.fliped = false;
            this.sebastiaoCanoPoco2.fluxo = -1;
            this.sebastiaoCanoPoco2.Location = new System.Drawing.Point(201, 48);
            this.sebastiaoCanoPoco2.MaxVidro = 127;
            this.sebastiaoCanoPoco2.MinVidro = 8;
            this.sebastiaoCanoPoco2.Name = "sebastiaoCanoPoco2";
            this.sebastiaoCanoPoco2.RaioYElipse = 30;
            this.sebastiaoCanoPoco2.Size = new System.Drawing.Size(49, 13);
            this.sebastiaoCanoPoco2.TabIndex = 240;
            this.sebastiaoCanoPoco2.vertical = false;
            // 
            // sebastiaoPoco4
            // 
            this.sebastiaoPoco4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoPoco4.LigarBomba = true;
            this.sebastiaoPoco4.Location = new System.Drawing.Point(8, 36);
            this.sebastiaoPoco4.Name = "sebastiaoPoco4";
            this.sebastiaoPoco4.Size = new System.Drawing.Size(66, 78);
            this.sebastiaoPoco4.TabIndex = 247;
            this.sebastiaoPoco4.Transparence = false;
            // 
            // sebastiaoPoco1
            // 
            this.sebastiaoPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoPoco1.LigarBomba = true;
            this.sebastiaoPoco1.Location = new System.Drawing.Point(220, 71);
            this.sebastiaoPoco1.Name = "sebastiaoPoco1";
            this.sebastiaoPoco1.Size = new System.Drawing.Size(66, 78);
            this.sebastiaoPoco1.TabIndex = 246;
            this.sebastiaoPoco1.Transparence = false;
            // 
            // sebastiaoCanoPoco1
            // 
            this.sebastiaoCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoPoco1.diminuirbolha = 5D;
            this.sebastiaoCanoPoco1.enableTransparentBackground = true;
            this.sebastiaoCanoPoco1.fliped = false;
            this.sebastiaoCanoPoco1.fluxo = -1;
            this.sebastiaoCanoPoco1.Location = new System.Drawing.Point(263, 48);
            this.sebastiaoCanoPoco1.MaxVidro = 127;
            this.sebastiaoCanoPoco1.MinVidro = 8;
            this.sebastiaoCanoPoco1.Name = "sebastiaoCanoPoco1";
            this.sebastiaoCanoPoco1.RaioYElipse = 30;
            this.sebastiaoCanoPoco1.Size = new System.Drawing.Size(130, 13);
            this.sebastiaoCanoPoco1.TabIndex = 242;
            this.sebastiaoCanoPoco1.vertical = false;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(12, 117);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(62, 16);
            this.label80.TabIndex = 236;
            this.label80.Text = "POÇO 4";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(229, 152);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(62, 16);
            this.label82.TabIndex = 235;
            this.label82.Text = "POÇO 1";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(159, 152);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(62, 16);
            this.label83.TabIndex = 234;
            this.label83.Text = "POÇO 2";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(87, 152);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(62, 16);
            this.label84.TabIndex = 233;
            this.label84.Text = "POÇO 3";
            // 
            // sebastiaoPoco3
            // 
            this.sebastiaoPoco3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoPoco3.LigarBomba = true;
            this.sebastiaoPoco3.Location = new System.Drawing.Point(78, 71);
            this.sebastiaoPoco3.Name = "sebastiaoPoco3";
            this.sebastiaoPoco3.Size = new System.Drawing.Size(66, 78);
            this.sebastiaoPoco3.TabIndex = 244;
            this.sebastiaoPoco3.Transparence = false;
            // 
            // sebastiaoPoco2
            // 
            this.sebastiaoPoco2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sebastiaoPoco2.LigarBomba = true;
            this.sebastiaoPoco2.Location = new System.Drawing.Point(150, 71);
            this.sebastiaoPoco2.Name = "sebastiaoPoco2";
            this.sebastiaoPoco2.Size = new System.Drawing.Size(66, 78);
            this.sebastiaoPoco2.TabIndex = 245;
            this.sebastiaoPoco2.Transparence = false;
            // 
            // sebastiaoCanoPoco3
            // 
            this.sebastiaoCanoPoco3.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoPoco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoPoco3.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoPoco3.diminuirbolha = 5D;
            this.sebastiaoCanoPoco3.enableTransparentBackground = true;
            this.sebastiaoCanoPoco3.fliped = false;
            this.sebastiaoCanoPoco3.fluxo = -1;
            this.sebastiaoCanoPoco3.Location = new System.Drawing.Point(135, 48);
            this.sebastiaoCanoPoco3.MaxVidro = 127;
            this.sebastiaoCanoPoco3.MinVidro = 8;
            this.sebastiaoCanoPoco3.Name = "sebastiaoCanoPoco3";
            this.sebastiaoCanoPoco3.RaioYElipse = 30;
            this.sebastiaoCanoPoco3.Size = new System.Drawing.Size(43, 13);
            this.sebastiaoCanoPoco3.TabIndex = 238;
            this.sebastiaoCanoPoco3.vertical = false;
            // 
            // label81
            // 
            this.label81.BackColor = System.Drawing.Color.Gainsboro;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(1118, 337);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(158, 65);
            this.label81.TabIndex = 232;
            this.label81.Text = "BATERIA INDEPENDENCIA";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sebastiaoTanque1
            // 
            this.sebastiaoTanque1.BackColor = System.Drawing.Color.Transparent;
            this.sebastiaoTanque1.Cano = 2;
            this.sebastiaoTanque1.CanoCheio = true;
            this.sebastiaoTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoTanque1.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoTanque1.enableTransparentBackground = true;
            this.sebastiaoTanque1.LarguraCano = 13;
            this.sebastiaoTanque1.Location = new System.Drawing.Point(385, 131);
            this.sebastiaoTanque1.MaxVidro = 127;
            this.sebastiaoTanque1.MinVidro = 8;
            this.sebastiaoTanque1.Name = "sebastiaoTanque1";
            this.sebastiaoTanque1.PosCano = 15;
            this.sebastiaoTanque1.RaioYElipse = 30;
            this.sebastiaoTanque1.Size = new System.Drawing.Size(240, 180);
            this.sebastiaoTanque1.TabIndex = 177;
            this.sebastiaoTanque1.Value = 40D;
            // 
            // sebastiaoTanque2
            // 
            this.sebastiaoTanque2.BackColor = System.Drawing.Color.Transparent;
            this.sebastiaoTanque2.Cano = 0;
            this.sebastiaoTanque2.CanoCheio = false;
            this.sebastiaoTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoTanque2.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoTanque2.enableTransparentBackground = true;
            this.sebastiaoTanque2.LarguraCano = 30;
            this.sebastiaoTanque2.Location = new System.Drawing.Point(667, 131);
            this.sebastiaoTanque2.MaxVidro = 127;
            this.sebastiaoTanque2.MinVidro = 8;
            this.sebastiaoTanque2.Name = "sebastiaoTanque2";
            this.sebastiaoTanque2.PosCano = 15;
            this.sebastiaoTanque2.RaioYElipse = 30;
            this.sebastiaoTanque2.Size = new System.Drawing.Size(240, 180);
            this.sebastiaoTanque2.TabIndex = 178;
            this.sebastiaoTanque2.Value = 40D;
            // 
            // sebastiaoCanoTanque2
            // 
            this.sebastiaoCanoTanque2.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoTanque2.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoTanque2.diminuirbolha = 5D;
            this.sebastiaoCanoTanque2.enableTransparentBackground = true;
            this.sebastiaoCanoTanque2.fliped = false;
            this.sebastiaoCanoTanque2.fluxo = -1;
            this.sebastiaoCanoTanque2.Location = new System.Drawing.Point(615, 268);
            this.sebastiaoCanoTanque2.MaxVidro = 127;
            this.sebastiaoCanoTanque2.MinVidro = 8;
            this.sebastiaoCanoTanque2.Name = "sebastiaoCanoTanque2";
            this.sebastiaoCanoTanque2.RaioYElipse = 30;
            this.sebastiaoCanoTanque2.Size = new System.Drawing.Size(59, 15);
            this.sebastiaoCanoTanque2.TabIndex = 180;
            this.sebastiaoCanoTanque2.vertical = false;
            // 
            // sebastiaoCanoBomba2
            // 
            this.sebastiaoCanoBomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoBomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoBomba2.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoBomba2.diminuirbolha = 5D;
            this.sebastiaoCanoBomba2.enableTransparentBackground = true;
            this.sebastiaoCanoBomba2.fliped = false;
            this.sebastiaoCanoBomba2.fluxo = -1;
            this.sebastiaoCanoBomba2.Location = new System.Drawing.Point(483, 305);
            this.sebastiaoCanoBomba2.MaxVidro = 127;
            this.sebastiaoCanoBomba2.MinVidro = 8;
            this.sebastiaoCanoBomba2.Name = "sebastiaoCanoBomba2";
            this.sebastiaoCanoBomba2.RaioYElipse = 30;
            this.sebastiaoCanoBomba2.Size = new System.Drawing.Size(13, 193);
            this.sebastiaoCanoBomba2.TabIndex = 255;
            this.sebastiaoCanoBomba2.vertical = true;
            // 
            // sebastiaoCanoBomba1
            // 
            this.sebastiaoCanoBomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCanoBomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCanoBomba1.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCanoBomba1.diminuirbolha = 5D;
            this.sebastiaoCanoBomba1.enableTransparentBackground = true;
            this.sebastiaoCanoBomba1.fliped = false;
            this.sebastiaoCanoBomba1.fluxo = -1;
            this.sebastiaoCanoBomba1.Location = new System.Drawing.Point(512, 296);
            this.sebastiaoCanoBomba1.MaxVidro = 127;
            this.sebastiaoCanoBomba1.MinVidro = 8;
            this.sebastiaoCanoBomba1.Name = "sebastiaoCanoBomba1";
            this.sebastiaoCanoBomba1.RaioYElipse = 30;
            this.sebastiaoCanoBomba1.Size = new System.Drawing.Size(13, 95);
            this.sebastiaoCanoBomba1.TabIndex = 262;
            this.sebastiaoCanoBomba1.vertical = true;
            // 
            // sebastiaoCano2Poco5
            // 
            this.sebastiaoCano2Poco5.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCano2Poco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCano2Poco5.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCano2Poco5.diminuirbolha = 5D;
            this.sebastiaoCano2Poco5.enableTransparentBackground = true;
            this.sebastiaoCano2Poco5.fliped = false;
            this.sebastiaoCano2Poco5.fluxo = 1;
            this.sebastiaoCano2Poco5.Location = new System.Drawing.Point(323, 131);
            this.sebastiaoCano2Poco5.MaxVidro = 127;
            this.sebastiaoCano2Poco5.MinVidro = 8;
            this.sebastiaoCano2Poco5.Name = "sebastiaoCano2Poco5";
            this.sebastiaoCano2Poco5.RaioYElipse = 30;
            this.sebastiaoCano2Poco5.Size = new System.Drawing.Size(13, 61);
            this.sebastiaoCano2Poco5.TabIndex = 258;
            this.sebastiaoCano2Poco5.vertical = true;
            // 
            // sebastiaoCano2Poco1
            // 
            this.sebastiaoCano2Poco1.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCano2Poco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCano2Poco1.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCano2Poco1.diminuirbolha = 5D;
            this.sebastiaoCano2Poco1.enableTransparentBackground = true;
            this.sebastiaoCano2Poco1.fliped = false;
            this.sebastiaoCano2Poco1.fluxo = -1;
            this.sebastiaoCano2Poco1.Location = new System.Drawing.Point(400, 67);
            this.sebastiaoCano2Poco1.MaxVidro = 127;
            this.sebastiaoCano2Poco1.MinVidro = 8;
            this.sebastiaoCano2Poco1.Name = "sebastiaoCano2Poco1";
            this.sebastiaoCano2Poco1.RaioYElipse = 30;
            this.sebastiaoCano2Poco1.Size = new System.Drawing.Size(13, 36);
            this.sebastiaoCano2Poco1.TabIndex = 280;
            this.sebastiaoCano2Poco1.vertical = true;
            // 
            // sebastiaoCano3Poco5
            // 
            this.sebastiaoCano3Poco5.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCano3Poco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCano3Poco5.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCano3Poco5.diminuirbolha = 5D;
            this.sebastiaoCano3Poco5.enableTransparentBackground = true;
            this.sebastiaoCano3Poco5.fliped = false;
            this.sebastiaoCano3Poco5.fluxo = -1;
            this.sebastiaoCano3Poco5.Location = new System.Drawing.Point(344, 109);
            this.sebastiaoCano3Poco5.MaxVidro = 127;
            this.sebastiaoCano3Poco5.MinVidro = 8;
            this.sebastiaoCano3Poco5.Name = "sebastiaoCano3Poco5";
            this.sebastiaoCano3Poco5.RaioYElipse = 30;
            this.sebastiaoCano3Poco5.Size = new System.Drawing.Size(48, 13);
            this.sebastiaoCano3Poco5.TabIndex = 281;
            this.sebastiaoCano3Poco5.vertical = false;
            // 
            // sebastiaoCano1Bomba2
            // 
            this.sebastiaoCano1Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCano1Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCano1Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCano1Bomba2.diminuirbolha = 5D;
            this.sebastiaoCano1Bomba2.enableTransparentBackground = true;
            this.sebastiaoCano1Bomba2.fliped = false;
            this.sebastiaoCano1Bomba2.fluxo = 0;
            this.sebastiaoCano1Bomba2.Location = new System.Drawing.Point(539, 469);
            this.sebastiaoCano1Bomba2.MaxVidro = 127;
            this.sebastiaoCano1Bomba2.MinVidro = 8;
            this.sebastiaoCano1Bomba2.Name = "sebastiaoCano1Bomba2";
            this.sebastiaoCano1Bomba2.RaioYElipse = 30;
            this.sebastiaoCano1Bomba2.Size = new System.Drawing.Size(427, 13);
            this.sebastiaoCano1Bomba2.TabIndex = 251;
            this.sebastiaoCano1Bomba2.vertical = false;
            // 
            // sebastiaoCano1Bomba1
            // 
            this.sebastiaoCano1Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.sebastiaoCano1Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.sebastiaoCano1Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.sebastiaoCano1Bomba1.diminuirbolha = 5D;
            this.sebastiaoCano1Bomba1.enableTransparentBackground = true;
            this.sebastiaoCano1Bomba1.fliped = false;
            this.sebastiaoCano1Bomba1.fluxo = 0;
            this.sebastiaoCano1Bomba1.Location = new System.Drawing.Point(571, 362);
            this.sebastiaoCano1Bomba1.MaxVidro = 127;
            this.sebastiaoCano1Bomba1.MinVidro = 8;
            this.sebastiaoCano1Bomba1.Name = "sebastiaoCano1Bomba1";
            this.sebastiaoCano1Bomba1.RaioYElipse = 30;
            this.sebastiaoCano1Bomba1.Size = new System.Drawing.Size(553, 13);
            this.sebastiaoCano1Bomba1.TabIndex = 266;
            this.sebastiaoCano1Bomba1.vertical = false;
            // 
            // OuroVerde
            // 
            this.OuroVerde.Controls.Add(this.junçãoT5);
            this.OuroVerde.Controls.Add(this.junçãoT4);
            this.OuroVerde.Controls.Add(this.junçãoT3);
            this.OuroVerde.Controls.Add(this.junçãoT2);
            this.OuroVerde.Controls.Add(this.junçãoT1);
            this.OuroVerde.Controls.Add(this.ouroCanoPoco5);
            this.OuroVerde.Controls.Add(this.ouroPoco1);
            this.OuroVerde.Controls.Add(this.ouroPoco2);
            this.OuroVerde.Controls.Add(this.ouroPoco3);
            this.OuroVerde.Controls.Add(this.ouroPoco4);
            this.OuroVerde.Controls.Add(this.junção20);
            this.OuroVerde.Controls.Add(this.label15);
            this.OuroVerde.Controls.Add(this.junção19);
            this.OuroVerde.Controls.Add(this.label14);
            this.OuroVerde.Controls.Add(this.label13);
            this.OuroVerde.Controls.Add(this.label12);
            this.OuroVerde.Controls.Add(this.junção7);
            this.OuroVerde.Controls.Add(this.ouroCanoPoco1);
            this.OuroVerde.Controls.Add(this.ouroCanoPoco2);
            this.OuroVerde.Controls.Add(this.ouroCanoPoco3);
            this.OuroVerde.Controls.Add(this.ouroCanoPoco4);
            this.OuroVerde.Controls.Add(this.junção6);
            this.OuroVerde.Controls.Add(this.label11);
            this.OuroVerde.Controls.Add(this.label10);
            this.OuroVerde.Controls.Add(this.label9);
            this.OuroVerde.Controls.Add(this.label8);
            this.OuroVerde.Controls.Add(this.label7);
            this.OuroVerde.Controls.Add(this.label6);
            this.OuroVerde.Controls.Add(this.label5);
            this.OuroVerde.Controls.Add(this.ouroTanque3);
            this.OuroVerde.Controls.Add(this.ouroTanque2);
            this.OuroVerde.Controls.Add(this.ouroTanque1);
            this.OuroVerde.Controls.Add(this.ouroCanoTanque2);
            this.OuroVerde.Controls.Add(this.junção17);
            this.OuroVerde.Controls.Add(this.ouroCano2CidadeNova);
            this.OuroVerde.Controls.Add(this.ouroCanoPoco7);
            this.OuroVerde.Controls.Add(this.junção4);
            this.OuroVerde.Controls.Add(this.junção18);
            this.OuroVerde.Controls.Add(this.ouroCanoIpe);
            this.OuroVerde.Controls.Add(this.ouroCanoOutTanque3);
            this.OuroVerde.Controls.Add(this.ouroPoco6);
            this.OuroVerde.Controls.Add(this.ouroPoco5);
            this.OuroVerde.Controls.Add(this.ouroCanoPoco6);
            this.OuroVerde.Controls.Add(this.ouroPoco7);
            this.OuroVerde.Controls.Add(this.junçãoT17);
            this.OuroVerde.Controls.Add(this.ouroCanoGramVille);
            this.OuroVerde.Controls.Add(this.ouroCano1CidadeNova);
            this.OuroVerde.Location = new System.Drawing.Point(4, 22);
            this.OuroVerde.Name = "OuroVerde";
            this.OuroVerde.Size = new System.Drawing.Size(1480, 662);
            this.OuroVerde.TabIndex = 4;
            this.OuroVerde.Text = "OURO VERDE";
            this.OuroVerde.UseVisualStyleBackColor = true;
            // 
            // junçãoT5
            // 
            this.junçãoT5.Flip = 180;
            this.junçãoT5.Location = new System.Drawing.Point(379, 38);
            this.junçãoT5.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT5.Name = "junçãoT5";
            this.junçãoT5.Size = new System.Drawing.Size(42, 44);
            this.junçãoT5.TabIndex = 35;
            // 
            // junçãoT4
            // 
            this.junçãoT4.Flip = 180;
            this.junçãoT4.Location = new System.Drawing.Point(310, 38);
            this.junçãoT4.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT4.Name = "junçãoT4";
            this.junçãoT4.Size = new System.Drawing.Size(42, 44);
            this.junçãoT4.TabIndex = 32;
            // 
            // junçãoT3
            // 
            this.junçãoT3.Flip = 180;
            this.junçãoT3.Location = new System.Drawing.Point(238, 38);
            this.junçãoT3.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT3.Name = "junçãoT3";
            this.junçãoT3.Size = new System.Drawing.Size(42, 44);
            this.junçãoT3.TabIndex = 29;
            // 
            // junçãoT2
            // 
            this.junçãoT2.Flip = 180;
            this.junçãoT2.Location = new System.Drawing.Point(168, 38);
            this.junçãoT2.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT2.Name = "junçãoT2";
            this.junçãoT2.Size = new System.Drawing.Size(42, 44);
            this.junçãoT2.TabIndex = 26;
            // 
            // junçãoT1
            // 
            this.junçãoT1.Flip = 180;
            this.junçãoT1.Location = new System.Drawing.Point(98, 38);
            this.junçãoT1.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT1.Name = "junçãoT1";
            this.junçãoT1.Size = new System.Drawing.Size(42, 44);
            this.junçãoT1.TabIndex = 21;
            // 
            // ouroCanoPoco5
            // 
            this.ouroCanoPoco5.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoPoco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoPoco5.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoPoco5.diminuirbolha = 5D;
            this.ouroCanoPoco5.enableTransparentBackground = true;
            this.ouroCanoPoco5.fliped = false;
            this.ouroCanoPoco5.fluxo = -1;
            this.ouroCanoPoco5.Location = new System.Drawing.Point(131, 47);
            this.ouroCanoPoco5.MaxVidro = 127;
            this.ouroCanoPoco5.MinVidro = 8;
            this.ouroCanoPoco5.Name = "ouroCanoPoco5";
            this.ouroCanoPoco5.RaioYElipse = 30;
            this.ouroCanoPoco5.Size = new System.Drawing.Size(37, 13);
            this.ouroCanoPoco5.TabIndex = 27;
            this.ouroCanoPoco5.vertical = false;
            // 
            // ouroPoco1
            // 
            this.ouroPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ouroPoco1.LigarBomba = true;
            this.ouroPoco1.Location = new System.Drawing.Point(362, 69);
            this.ouroPoco1.Name = "ouroPoco1";
            this.ouroPoco1.Size = new System.Drawing.Size(66, 78);
            this.ouroPoco1.TabIndex = 76;
            this.ouroPoco1.Transparence = false;
            // 
            // ouroPoco2
            // 
            this.ouroPoco2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ouroPoco2.LigarBomba = true;
            this.ouroPoco2.Location = new System.Drawing.Point(292, 69);
            this.ouroPoco2.Name = "ouroPoco2";
            this.ouroPoco2.Size = new System.Drawing.Size(66, 78);
            this.ouroPoco2.TabIndex = 75;
            this.ouroPoco2.Transparence = false;
            // 
            // ouroPoco3
            // 
            this.ouroPoco3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ouroPoco3.LigarBomba = true;
            this.ouroPoco3.Location = new System.Drawing.Point(220, 69);
            this.ouroPoco3.Name = "ouroPoco3";
            this.ouroPoco3.Size = new System.Drawing.Size(66, 78);
            this.ouroPoco3.TabIndex = 74;
            this.ouroPoco3.Transparence = false;
            // 
            // ouroPoco4
            // 
            this.ouroPoco4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ouroPoco4.LigarBomba = true;
            this.ouroPoco4.Location = new System.Drawing.Point(150, 69);
            this.ouroPoco4.Name = "ouroPoco4";
            this.ouroPoco4.Size = new System.Drawing.Size(66, 78);
            this.ouroPoco4.TabIndex = 73;
            this.ouroPoco4.Transparence = false;
            // 
            // junção20
            // 
            this.junção20.Flip = 90;
            this.junção20.Location = new System.Drawing.Point(1170, 86);
            this.junção20.Margin = new System.Windows.Forms.Padding(0);
            this.junção20.Name = "junção20";
            this.junção20.Size = new System.Drawing.Size(35, 35);
            this.junção20.TabIndex = 68;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(1231, 196);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 16);
            this.label15.TabIndex = 67;
            this.label15.Text = "POÇO 7";
            // 
            // junção19
            // 
            this.junção19.Flip = 180;
            this.junção19.Location = new System.Drawing.Point(1241, 86);
            this.junção19.Margin = new System.Windows.Forms.Padding(0);
            this.junção19.Name = "junção19";
            this.junção19.Size = new System.Drawing.Size(35, 35);
            this.junção19.TabIndex = 66;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Gainsboro;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(679, 476);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(190, 65);
            this.label14.TabIndex = 57;
            this.label14.Text = "BAIRROS IPÊ 1,2";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Gainsboro;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(215, 497);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(184, 65);
            this.label13.TabIndex = 56;
            this.label13.Text = "BAIRRO CIDADE NOVA";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Gainsboro;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(405, 425);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(198, 60);
            this.label12.TabIndex = 55;
            this.label12.Text = "BAIRRO GRAM VILLE";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // junção7
            // 
            this.junção7.Flip = 180;
            this.junção7.Location = new System.Drawing.Point(508, 45);
            this.junção7.Margin = new System.Windows.Forms.Padding(0);
            this.junção7.Name = "junção7";
            this.junção7.Size = new System.Drawing.Size(30, 30);
            this.junção7.TabIndex = 25;
            // 
            // ouroCanoPoco1
            // 
            this.ouroCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoPoco1.diminuirbolha = 5D;
            this.ouroCanoPoco1.enableTransparentBackground = true;
            this.ouroCanoPoco1.fliped = false;
            this.ouroCanoPoco1.fluxo = -1;
            this.ouroCanoPoco1.Location = new System.Drawing.Point(412, 47);
            this.ouroCanoPoco1.MaxVidro = 127;
            this.ouroCanoPoco1.MinVidro = 8;
            this.ouroCanoPoco1.Name = "ouroCanoPoco1";
            this.ouroCanoPoco1.RaioYElipse = 30;
            this.ouroCanoPoco1.Size = new System.Drawing.Size(104, 13);
            this.ouroCanoPoco1.TabIndex = 38;
            this.ouroCanoPoco1.vertical = false;
            // 
            // ouroCanoPoco2
            // 
            this.ouroCanoPoco2.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoPoco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoPoco2.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoPoco2.diminuirbolha = 5D;
            this.ouroCanoPoco2.enableTransparentBackground = true;
            this.ouroCanoPoco2.fliped = false;
            this.ouroCanoPoco2.fluxo = -1;
            this.ouroCanoPoco2.Location = new System.Drawing.Point(345, 47);
            this.ouroCanoPoco2.MaxVidro = 127;
            this.ouroCanoPoco2.MinVidro = 8;
            this.ouroCanoPoco2.Name = "ouroCanoPoco2";
            this.ouroCanoPoco2.RaioYElipse = 30;
            this.ouroCanoPoco2.Size = new System.Drawing.Size(40, 13);
            this.ouroCanoPoco2.TabIndex = 36;
            this.ouroCanoPoco2.vertical = false;
            // 
            // ouroCanoPoco3
            // 
            this.ouroCanoPoco3.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoPoco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoPoco3.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoPoco3.diminuirbolha = 5D;
            this.ouroCanoPoco3.enableTransparentBackground = true;
            this.ouroCanoPoco3.fliped = false;
            this.ouroCanoPoco3.fluxo = -1;
            this.ouroCanoPoco3.Location = new System.Drawing.Point(274, 47);
            this.ouroCanoPoco3.MaxVidro = 127;
            this.ouroCanoPoco3.MinVidro = 8;
            this.ouroCanoPoco3.Name = "ouroCanoPoco3";
            this.ouroCanoPoco3.RaioYElipse = 30;
            this.ouroCanoPoco3.Size = new System.Drawing.Size(47, 13);
            this.ouroCanoPoco3.TabIndex = 33;
            this.ouroCanoPoco3.vertical = false;
            // 
            // ouroCanoPoco4
            // 
            this.ouroCanoPoco4.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoPoco4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoPoco4.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoPoco4.diminuirbolha = 5D;
            this.ouroCanoPoco4.enableTransparentBackground = true;
            this.ouroCanoPoco4.fliped = false;
            this.ouroCanoPoco4.fluxo = -1;
            this.ouroCanoPoco4.Location = new System.Drawing.Point(200, 47);
            this.ouroCanoPoco4.MaxVidro = 127;
            this.ouroCanoPoco4.MinVidro = 8;
            this.ouroCanoPoco4.Name = "ouroCanoPoco4";
            this.ouroCanoPoco4.RaioYElipse = 30;
            this.ouroCanoPoco4.Size = new System.Drawing.Size(48, 13);
            this.ouroCanoPoco4.TabIndex = 30;
            this.ouroCanoPoco4.vertical = false;
            // 
            // junção6
            // 
            this.junção6.Flip = 90;
            this.junção6.Location = new System.Drawing.Point(38, 44);
            this.junção6.Margin = new System.Windows.Forms.Padding(0);
            this.junção6.Name = "junção6";
            this.junção6.Size = new System.Drawing.Size(35, 35);
            this.junção6.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Gainsboro;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(1121, 425);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(190, 65);
            this.label11.TabIndex = 11;
            this.label11.Text = "BAIRRO OURO VERDE";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(369, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 16);
            this.label10.TabIndex = 9;
            this.label10.Text = "POÇO 1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(299, 150);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 16);
            this.label9.TabIndex = 8;
            this.label9.Text = "POÇO 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(227, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "POÇO 3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(157, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "POÇO 4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(87, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "POÇO 5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "POÇO 6";
            // 
            // ouroTanque3
            // 
            this.ouroTanque3.BackColor = System.Drawing.Color.Transparent;
            this.ouroTanque3.Cano = 2;
            this.ouroTanque3.CanoCheio = true;
            this.ouroTanque3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroTanque3.CorVidro = System.Drawing.Color.Silver;
            this.ouroTanque3.enableTransparentBackground = true;
            this.ouroTanque3.LarguraCano = 13;
            this.ouroTanque3.Location = new System.Drawing.Point(1053, 115);
            this.ouroTanque3.MaxVidro = 127;
            this.ouroTanque3.MinVidro = 8;
            this.ouroTanque3.Name = "ouroTanque3";
            this.ouroTanque3.PosCano = 120;
            this.ouroTanque3.RaioYElipse = 30;
            this.ouroTanque3.Size = new System.Drawing.Size(150, 200);
            this.ouroTanque3.TabIndex = 2;
            this.ouroTanque3.Value = 40D;
            // 
            // ouroTanque2
            // 
            this.ouroTanque2.BackColor = System.Drawing.Color.Transparent;
            this.ouroTanque2.Cano = 0;
            this.ouroTanque2.CanoCheio = false;
            this.ouroTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroTanque2.CorVidro = System.Drawing.Color.Silver;
            this.ouroTanque2.enableTransparentBackground = true;
            this.ouroTanque2.LarguraCano = 30;
            this.ouroTanque2.Location = new System.Drawing.Point(857, 115);
            this.ouroTanque2.MaxVidro = 127;
            this.ouroTanque2.MinVidro = 8;
            this.ouroTanque2.Name = "ouroTanque2";
            this.ouroTanque2.PosCano = 15;
            this.ouroTanque2.RaioYElipse = 30;
            this.ouroTanque2.Size = new System.Drawing.Size(159, 200);
            this.ouroTanque2.TabIndex = 1;
            this.ouroTanque2.Value = 40D;
            // 
            // ouroTanque1
            // 
            this.ouroTanque1.BackColor = System.Drawing.Color.Transparent;
            this.ouroTanque1.Cano = 2;
            this.ouroTanque1.CanoCheio = true;
            this.ouroTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroTanque1.CorVidro = System.Drawing.Color.Silver;
            this.ouroTanque1.enableTransparentBackground = true;
            this.ouroTanque1.LarguraCano = 13;
            this.ouroTanque1.Location = new System.Drawing.Point(508, 74);
            this.ouroTanque1.MaxVidro = 127;
            this.ouroTanque1.MinVidro = 8;
            this.ouroTanque1.Name = "ouroTanque1";
            this.ouroTanque1.PosCano = 15;
            this.ouroTanque1.RaioYElipse = 30;
            this.ouroTanque1.Size = new System.Drawing.Size(244, 217);
            this.ouroTanque1.TabIndex = 0;
            this.ouroTanque1.Value = 40D;
            // 
            // ouroCanoTanque2
            // 
            this.ouroCanoTanque2.BackColor = System.Drawing.Color.Transparent;
            this.ouroCanoTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoTanque2.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoTanque2.diminuirbolha = 5D;
            this.ouroCanoTanque2.enableTransparentBackground = true;
            this.ouroCanoTanque2.fliped = false;
            this.ouroCanoTanque2.fluxo = -1;
            this.ouroCanoTanque2.Location = new System.Drawing.Point(1012, 278);
            this.ouroCanoTanque2.MaxVidro = 127;
            this.ouroCanoTanque2.MinVidro = 8;
            this.ouroCanoTanque2.Name = "ouroCanoTanque2";
            this.ouroCanoTanque2.RaioYElipse = 30;
            this.ouroCanoTanque2.Size = new System.Drawing.Size(42, 13);
            this.ouroCanoTanque2.TabIndex = 10;
            this.ouroCanoTanque2.vertical = false;
            // 
            // junção17
            // 
            this.junção17.Flip = 90;
            this.junção17.Location = new System.Drawing.Point(291, 245);
            this.junção17.Margin = new System.Windows.Forms.Padding(0);
            this.junção17.Name = "junção17";
            this.junção17.Size = new System.Drawing.Size(30, 30);
            this.junção17.TabIndex = 61;
            // 
            // ouroCano2CidadeNova
            // 
            this.ouroCano2CidadeNova.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCano2CidadeNova.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCano2CidadeNova.CorVidro = System.Drawing.Color.Silver;
            this.ouroCano2CidadeNova.diminuirbolha = 5D;
            this.ouroCano2CidadeNova.enableTransparentBackground = true;
            this.ouroCano2CidadeNova.fliped = false;
            this.ouroCano2CidadeNova.fluxo = -1;
            this.ouroCano2CidadeNova.Location = new System.Drawing.Point(293, 262);
            this.ouroCano2CidadeNova.MaxVidro = 127;
            this.ouroCano2CidadeNova.MinVidro = 8;
            this.ouroCano2CidadeNova.Name = "ouroCano2CidadeNova";
            this.ouroCano2CidadeNova.RaioYElipse = 30;
            this.ouroCano2CidadeNova.Size = new System.Drawing.Size(13, 248);
            this.ouroCano2CidadeNova.TabIndex = 60;
            this.ouroCano2CidadeNova.vertical = true;
            // 
            // ouroCanoPoco7
            // 
            this.ouroCanoPoco7.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoPoco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoPoco7.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoPoco7.diminuirbolha = 5D;
            this.ouroCanoPoco7.enableTransparentBackground = true;
            this.ouroCanoPoco7.fliped = false;
            this.ouroCanoPoco7.fluxo = 1;
            this.ouroCanoPoco7.Location = new System.Drawing.Point(1198, 90);
            this.ouroCanoPoco7.MaxVidro = 127;
            this.ouroCanoPoco7.MinVidro = 8;
            this.ouroCanoPoco7.Name = "ouroCanoPoco7";
            this.ouroCanoPoco7.RaioYElipse = 30;
            this.ouroCanoPoco7.Size = new System.Drawing.Size(44, 13);
            this.ouroCanoPoco7.TabIndex = 69;
            this.ouroCanoPoco7.vertical = false;
            // 
            // junção4
            // 
            this.junção4.Flip = 180;
            this.junção4.Location = new System.Drawing.Point(1198, 270);
            this.junção4.Margin = new System.Windows.Forms.Padding(0);
            this.junção4.Name = "junção4";
            this.junção4.Size = new System.Drawing.Size(32, 32);
            this.junção4.TabIndex = 13;
            // 
            // junção18
            // 
            this.junção18.Flip = 180;
            this.junção18.Location = new System.Drawing.Point(745, 246);
            this.junção18.Margin = new System.Windows.Forms.Padding(0);
            this.junção18.Name = "junção18";
            this.junção18.Size = new System.Drawing.Size(30, 30);
            this.junção18.TabIndex = 63;
            // 
            // ouroCanoIpe
            // 
            this.ouroCanoIpe.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoIpe.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoIpe.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoIpe.diminuirbolha = 5D;
            this.ouroCanoIpe.enableTransparentBackground = true;
            this.ouroCanoIpe.fliped = false;
            this.ouroCanoIpe.fluxo = -1;
            this.ouroCanoIpe.Location = new System.Drawing.Point(760, 270);
            this.ouroCanoIpe.MaxVidro = 127;
            this.ouroCanoIpe.MinVidro = 8;
            this.ouroCanoIpe.Name = "ouroCanoIpe";
            this.ouroCanoIpe.RaioYElipse = 30;
            this.ouroCanoIpe.Size = new System.Drawing.Size(13, 215);
            this.ouroCanoIpe.TabIndex = 62;
            this.ouroCanoIpe.vertical = true;
            // 
            // ouroCanoOutTanque3
            // 
            this.ouroCanoOutTanque3.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoOutTanque3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoOutTanque3.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoOutTanque3.diminuirbolha = 5D;
            this.ouroCanoOutTanque3.enableTransparentBackground = true;
            this.ouroCanoOutTanque3.fliped = false;
            this.ouroCanoOutTanque3.fluxo = -1;
            this.ouroCanoOutTanque3.Location = new System.Drawing.Point(1214, 297);
            this.ouroCanoOutTanque3.MaxVidro = 127;
            this.ouroCanoOutTanque3.MinVidro = 8;
            this.ouroCanoOutTanque3.Name = "ouroCanoOutTanque3";
            this.ouroCanoOutTanque3.RaioYElipse = 30;
            this.ouroCanoOutTanque3.Size = new System.Drawing.Size(13, 141);
            this.ouroCanoOutTanque3.TabIndex = 70;
            this.ouroCanoOutTanque3.vertical = true;
            // 
            // ouroPoco6
            // 
            this.ouroPoco6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ouroPoco6.LigarBomba = true;
            this.ouroPoco6.Location = new System.Drawing.Point(8, 70);
            this.ouroPoco6.Name = "ouroPoco6";
            this.ouroPoco6.Size = new System.Drawing.Size(66, 78);
            this.ouroPoco6.TabIndex = 71;
            this.ouroPoco6.Transparence = false;
            // 
            // ouroPoco5
            // 
            this.ouroPoco5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ouroPoco5.LigarBomba = true;
            this.ouroPoco5.Location = new System.Drawing.Point(80, 69);
            this.ouroPoco5.Name = "ouroPoco5";
            this.ouroPoco5.Size = new System.Drawing.Size(66, 78);
            this.ouroPoco5.TabIndex = 72;
            this.ouroPoco5.Transparence = false;
            // 
            // ouroCanoPoco6
            // 
            this.ouroCanoPoco6.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoPoco6.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoPoco6.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoPoco6.diminuirbolha = 5D;
            this.ouroCanoPoco6.enableTransparentBackground = true;
            this.ouroCanoPoco6.fliped = false;
            this.ouroCanoPoco6.fluxo = -1;
            this.ouroCanoPoco6.Location = new System.Drawing.Point(65, 47);
            this.ouroCanoPoco6.MaxVidro = 127;
            this.ouroCanoPoco6.MinVidro = 8;
            this.ouroCanoPoco6.Name = "ouroCanoPoco6";
            this.ouroCanoPoco6.RaioYElipse = 30;
            this.ouroCanoPoco6.Size = new System.Drawing.Size(39, 13);
            this.ouroCanoPoco6.TabIndex = 22;
            this.ouroCanoPoco6.vertical = false;
            // 
            // ouroPoco7
            // 
            this.ouroPoco7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ouroPoco7.LigarBomba = true;
            this.ouroPoco7.Location = new System.Drawing.Point(1227, 115);
            this.ouroPoco7.Name = "ouroPoco7";
            this.ouroPoco7.Size = new System.Drawing.Size(66, 78);
            this.ouroPoco7.TabIndex = 77;
            this.ouroPoco7.Transparence = false;
            // 
            // junçãoT17
            // 
            this.junçãoT17.Flip = 180;
            this.junçãoT17.Location = new System.Drawing.Point(472, 238);
            this.junçãoT17.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT17.Name = "junçãoT17";
            this.junçãoT17.Size = new System.Drawing.Size(42, 44);
            this.junçãoT17.TabIndex = 78;
            // 
            // ouroCanoGramVille
            // 
            this.ouroCanoGramVille.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCanoGramVille.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCanoGramVille.CorVidro = System.Drawing.Color.Silver;
            this.ouroCanoGramVille.diminuirbolha = 5D;
            this.ouroCanoGramVille.enableTransparentBackground = true;
            this.ouroCanoGramVille.fliped = false;
            this.ouroCanoGramVille.fluxo = -1;
            this.ouroCanoGramVille.Location = new System.Drawing.Point(486, 268);
            this.ouroCanoGramVille.MaxVidro = 127;
            this.ouroCanoGramVille.MinVidro = 8;
            this.ouroCanoGramVille.Name = "ouroCanoGramVille";
            this.ouroCanoGramVille.RaioYElipse = 30;
            this.ouroCanoGramVille.Size = new System.Drawing.Size(13, 170);
            this.ouroCanoGramVille.TabIndex = 58;
            this.ouroCanoGramVille.vertical = true;
            // 
            // ouroCano1CidadeNova
            // 
            this.ouroCano1CidadeNova.BackColor = System.Drawing.Color.Gainsboro;
            this.ouroCano1CidadeNova.CorFluido = System.Drawing.Color.DodgerBlue;
            this.ouroCano1CidadeNova.CorVidro = System.Drawing.Color.Silver;
            this.ouroCano1CidadeNova.diminuirbolha = 5D;
            this.ouroCano1CidadeNova.enableTransparentBackground = true;
            this.ouroCano1CidadeNova.fliped = false;
            this.ouroCano1CidadeNova.fluxo = 1;
            this.ouroCano1CidadeNova.Location = new System.Drawing.Point(310, 247);
            this.ouroCano1CidadeNova.MaxVidro = 127;
            this.ouroCano1CidadeNova.MinVidro = 8;
            this.ouroCano1CidadeNova.Name = "ouroCano1CidadeNova";
            this.ouroCano1CidadeNova.RaioYElipse = 30;
            this.ouroCano1CidadeNova.Size = new System.Drawing.Size(175, 13);
            this.ouroCano1CidadeNova.TabIndex = 64;
            this.ouroCano1CidadeNova.vertical = false;
            // 
            // SaoBenedito
            // 
            this.SaoBenedito.Controls.Add(this.juncao1Benedito);
            this.SaoBenedito.Controls.Add(this.cano1);
            this.SaoBenedito.Controls.Add(this.beneditoVazaoBomba3);
            this.SaoBenedito.Controls.Add(this.waterMeter15);
            this.SaoBenedito.Controls.Add(this.junção128);
            this.SaoBenedito.Controls.Add(this.beneditoVazaoBomba2);
            this.SaoBenedito.Controls.Add(this.waterMeter8);
            this.SaoBenedito.Controls.Add(this.beneditoVazaoBomba1);
            this.SaoBenedito.Controls.Add(this.waterMeter3);
            this.SaoBenedito.Controls.Add(this.junção46);
            this.SaoBenedito.Controls.Add(this.label39);
            this.SaoBenedito.Controls.Add(this.junção53);
            this.SaoBenedito.Controls.Add(this.beneditoPoco1);
            this.SaoBenedito.Controls.Add(this.label31);
            this.SaoBenedito.Controls.Add(this.junção51);
            this.SaoBenedito.Controls.Add(this.junção52);
            this.SaoBenedito.Controls.Add(this.beneditoBombaChamcia);
            this.SaoBenedito.Controls.Add(this.beneditoCano2Chamcia);
            this.SaoBenedito.Controls.Add(this.label38);
            this.SaoBenedito.Controls.Add(this.juncao2Benedito);
            this.SaoBenedito.Controls.Add(this.junção16);
            this.SaoBenedito.Controls.Add(this.junção47);
            this.SaoBenedito.Controls.Add(this.label36);
            this.SaoBenedito.Controls.Add(this.junção48);
            this.SaoBenedito.Controls.Add(this.junção49);
            this.SaoBenedito.Controls.Add(this.junção50);
            this.SaoBenedito.Controls.Add(this.beneditoBomba3);
            this.SaoBenedito.Controls.Add(this.beneditoCano2Bomba3);
            this.SaoBenedito.Controls.Add(this.junção55);
            this.SaoBenedito.Controls.Add(this.beneditoCano3Bomba2);
            this.SaoBenedito.Controls.Add(this.junção56);
            this.SaoBenedito.Controls.Add(this.junção57);
            this.SaoBenedito.Controls.Add(this.beneditoBomba2);
            this.SaoBenedito.Controls.Add(this.beneditoCano2Bomba2);
            this.SaoBenedito.Controls.Add(this.junção58);
            this.SaoBenedito.Controls.Add(this.junção59);
            this.SaoBenedito.Controls.Add(this.junção60);
            this.SaoBenedito.Controls.Add(this.junção61);
            this.SaoBenedito.Controls.Add(this.label37);
            this.SaoBenedito.Controls.Add(this.beneditoBomba1);
            this.SaoBenedito.Controls.Add(this.beneditoCano21Bomba1);
            this.SaoBenedito.Controls.Add(this.beneditoCanoBomba1);
            this.SaoBenedito.Controls.Add(this.beneditoCano2Bomba1);
            this.SaoBenedito.Controls.Add(this.beneditoCano3Bomba1);
            this.SaoBenedito.Controls.Add(this.beneditoCanoBomba2);
            this.SaoBenedito.Controls.Add(this.beneditoCano1Bomba3);
            this.SaoBenedito.Controls.Add(this.label32);
            this.SaoBenedito.Controls.Add(this.label33);
            this.SaoBenedito.Controls.Add(this.label34);
            this.SaoBenedito.Controls.Add(this.label35);
            this.SaoBenedito.Controls.Add(this.junção41);
            this.SaoBenedito.Controls.Add(this.junçãoT18);
            this.SaoBenedito.Controls.Add(this.junção42);
            this.SaoBenedito.Controls.Add(this.beneditoPocoSolt1);
            this.SaoBenedito.Controls.Add(this.beneditoPocoSolt2);
            this.SaoBenedito.Controls.Add(this.junção43);
            this.SaoBenedito.Controls.Add(this.junção44);
            this.SaoBenedito.Controls.Add(this.junção45);
            this.SaoBenedito.Controls.Add(this.beneditoCano3PocoSolt1);
            this.SaoBenedito.Controls.Add(this.beneditoTanque1);
            this.SaoBenedito.Controls.Add(this.beneditoCano1PocoSolt1);
            this.SaoBenedito.Controls.Add(this.junçãoT19);
            this.SaoBenedito.Controls.Add(this.beneditoCano1BelaVista2);
            this.SaoBenedito.Controls.Add(this.beneditoCano2BelaVista2);
            this.SaoBenedito.Controls.Add(this.beneditoCanoBelaVista1);
            this.SaoBenedito.Controls.Add(this.beneditoCanoPocoSolt2);
            this.SaoBenedito.Controls.Add(this.beneditoCanoPoco1);
            this.SaoBenedito.Controls.Add(this.beneditoTanque2);
            this.SaoBenedito.Controls.Add(this.beneditoCanoBomba3);
            this.SaoBenedito.Controls.Add(this.beneditoCanoChamcia);
            this.SaoBenedito.Controls.Add(this.beneditoCano1Bomba2);
            this.SaoBenedito.Controls.Add(this.beneditoCano1Bomba1);
            this.SaoBenedito.Controls.Add(this.beneditoCano2PocoSolt1);
            this.SaoBenedito.Controls.Add(this.beneditoCano3Bomba3);
            this.SaoBenedito.Controls.Add(this.cano2);
            this.SaoBenedito.Location = new System.Drawing.Point(4, 22);
            this.SaoBenedito.Name = "SaoBenedito";
            this.SaoBenedito.Size = new System.Drawing.Size(1480, 662);
            this.SaoBenedito.TabIndex = 3;
            this.SaoBenedito.Text = "SÃO BENEDITO";
            this.SaoBenedito.UseVisualStyleBackColor = true;
            // 
            // juncao1Benedito
            // 
            this.juncao1Benedito.Flip = 180;
            this.juncao1Benedito.Location = new System.Drawing.Point(537, 16);
            this.juncao1Benedito.Margin = new System.Windows.Forms.Padding(0);
            this.juncao1Benedito.Name = "juncao1Benedito";
            this.juncao1Benedito.Size = new System.Drawing.Size(42, 30);
            this.juncao1Benedito.TabIndex = 222;
            // 
            // cano1
            // 
            this.cano1.BackColor = System.Drawing.Color.Gainsboro;
            this.cano1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.cano1.CorVidro = System.Drawing.Color.Silver;
            this.cano1.diminuirbolha = 5D;
            this.cano1.enableTransparentBackground = true;
            this.cano1.fliped = false;
            this.cano1.fluxo = -1;
            this.cano1.Location = new System.Drawing.Point(552, 30);
            this.cano1.MaxVidro = 127;
            this.cano1.MinVidro = 8;
            this.cano1.Name = "cano1";
            this.cano1.RaioYElipse = 30;
            this.cano1.Size = new System.Drawing.Size(13, 20);
            this.cano1.TabIndex = 249;
            this.cano1.vertical = true;
            // 
            // beneditoVazaoBomba3
            // 
            this.beneditoVazaoBomba3.AutoSize = true;
            this.beneditoVazaoBomba3.BackColor = System.Drawing.Color.Black;
            this.beneditoVazaoBomba3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.beneditoVazaoBomba3.ForeColor = System.Drawing.Color.Transparent;
            this.beneditoVazaoBomba3.Location = new System.Drawing.Point(874, 549);
            this.beneditoVazaoBomba3.Name = "beneditoVazaoBomba3";
            this.beneditoVazaoBomba3.Size = new System.Drawing.Size(76, 16);
            this.beneditoVazaoBomba3.TabIndex = 248;
            this.beneditoVazaoBomba3.Text = "00.00 (m³/h)";
            // 
            // waterMeter15
            // 
            this.waterMeter15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter15.Location = new System.Drawing.Point(877, 491);
            this.waterMeter15.Name = "waterMeter15";
            this.waterMeter15.Size = new System.Drawing.Size(55, 55);
            this.waterMeter15.TabIndex = 247;
            this.waterMeter15.Transparence = false;
            // 
            // junção128
            // 
            this.junção128.Flip = 0;
            this.junção128.Location = new System.Drawing.Point(752, 508);
            this.junção128.Margin = new System.Windows.Forms.Padding(0);
            this.junção128.Name = "junção128";
            this.junção128.Size = new System.Drawing.Size(31, 32);
            this.junção128.TabIndex = 246;
            // 
            // beneditoVazaoBomba2
            // 
            this.beneditoVazaoBomba2.AutoSize = true;
            this.beneditoVazaoBomba2.BackColor = System.Drawing.Color.Black;
            this.beneditoVazaoBomba2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.beneditoVazaoBomba2.ForeColor = System.Drawing.Color.Transparent;
            this.beneditoVazaoBomba2.Location = new System.Drawing.Point(225, 524);
            this.beneditoVazaoBomba2.Name = "beneditoVazaoBomba2";
            this.beneditoVazaoBomba2.Size = new System.Drawing.Size(76, 16);
            this.beneditoVazaoBomba2.TabIndex = 244;
            this.beneditoVazaoBomba2.Text = "00.00 (m³/h)";
            // 
            // waterMeter8
            // 
            this.waterMeter8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter8.Location = new System.Drawing.Point(228, 467);
            this.waterMeter8.Name = "waterMeter8";
            this.waterMeter8.Size = new System.Drawing.Size(55, 55);
            this.waterMeter8.TabIndex = 243;
            this.waterMeter8.Transparence = false;
            // 
            // beneditoVazaoBomba1
            // 
            this.beneditoVazaoBomba1.AutoSize = true;
            this.beneditoVazaoBomba1.BackColor = System.Drawing.Color.Black;
            this.beneditoVazaoBomba1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.beneditoVazaoBomba1.ForeColor = System.Drawing.Color.Transparent;
            this.beneditoVazaoBomba1.Location = new System.Drawing.Point(225, 430);
            this.beneditoVazaoBomba1.Name = "beneditoVazaoBomba1";
            this.beneditoVazaoBomba1.Size = new System.Drawing.Size(76, 16);
            this.beneditoVazaoBomba1.TabIndex = 242;
            this.beneditoVazaoBomba1.Text = "00.00 (m³/h)";
            // 
            // waterMeter3
            // 
            this.waterMeter3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter3.Location = new System.Drawing.Point(228, 372);
            this.waterMeter3.Name = "waterMeter3";
            this.waterMeter3.Size = new System.Drawing.Size(55, 55);
            this.waterMeter3.TabIndex = 241;
            this.waterMeter3.Transparence = false;
            // 
            // junção46
            // 
            this.junção46.Flip = 180;
            this.junção46.Location = new System.Drawing.Point(1018, 165);
            this.junção46.Margin = new System.Windows.Forms.Padding(0);
            this.junção46.Name = "junção46";
            this.junção46.Size = new System.Drawing.Size(29, 32);
            this.junção46.TabIndex = 123;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(819, 125);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(62, 16);
            this.label39.TabIndex = 240;
            this.label39.Text = "POÇO 1";
            // 
            // junção53
            // 
            this.junção53.Flip = 180;
            this.junção53.Location = new System.Drawing.Point(832, 15);
            this.junção53.Margin = new System.Windows.Forms.Padding(0);
            this.junção53.Name = "junção53";
            this.junção53.Size = new System.Drawing.Size(29, 32);
            this.junção53.TabIndex = 238;
            // 
            // beneditoPoco1
            // 
            this.beneditoPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.beneditoPoco1.LigarBomba = true;
            this.beneditoPoco1.Location = new System.Drawing.Point(814, 42);
            this.beneditoPoco1.Name = "beneditoPoco1";
            this.beneditoPoco1.Size = new System.Drawing.Size(66, 78);
            this.beneditoPoco1.TabIndex = 237;
            this.beneditoPoco1.Transparence = false;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.LightGray;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(8, 104);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(142, 55);
            this.label31.TabIndex = 230;
            this.label31.Text = "BATERIA CHANCIA";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // junção51
            // 
            this.junção51.Flip = 90;
            this.junção51.Location = new System.Drawing.Point(72, 50);
            this.junção51.Margin = new System.Windows.Forms.Padding(0);
            this.junção51.Name = "junção51";
            this.junção51.Size = new System.Drawing.Size(30, 32);
            this.junção51.TabIndex = 231;
            // 
            // junção52
            // 
            this.junção52.Flip = 90;
            this.junção52.Location = new System.Drawing.Point(105, 13);
            this.junção52.Margin = new System.Windows.Forms.Padding(0);
            this.junção52.Name = "junção52";
            this.junção52.Size = new System.Drawing.Size(30, 32);
            this.junção52.TabIndex = 235;
            // 
            // beneditoBombaChamcia
            // 
            this.beneditoBombaChamcia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.beneditoBombaChamcia.LigarBomba = false;
            this.beneditoBombaChamcia.Location = new System.Drawing.Point(97, 37);
            this.beneditoBombaChamcia.Name = "beneditoBombaChamcia";
            this.beneditoBombaChamcia.Size = new System.Drawing.Size(60, 50);
            this.beneditoBombaChamcia.TabIndex = 232;
            this.beneditoBombaChamcia.Transparence = false;
            // 
            // beneditoCano2Chamcia
            // 
            this.beneditoCano2Chamcia.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano2Chamcia.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano2Chamcia.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano2Chamcia.diminuirbolha = 5D;
            this.beneditoCano2Chamcia.enableTransparentBackground = true;
            this.beneditoCano2Chamcia.fliped = false;
            this.beneditoCano2Chamcia.fluxo = 1;
            this.beneditoCano2Chamcia.Location = new System.Drawing.Point(74, 74);
            this.beneditoCano2Chamcia.MaxVidro = 127;
            this.beneditoCano2Chamcia.MinVidro = 8;
            this.beneditoCano2Chamcia.Name = "beneditoCano2Chamcia";
            this.beneditoCano2Chamcia.RaioYElipse = 10;
            this.beneditoCano2Chamcia.Size = new System.Drawing.Size(13, 38);
            this.beneditoCano2Chamcia.TabIndex = 234;
            this.beneditoCano2Chamcia.vertical = true;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.Gainsboro;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(54, 466);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(142, 81);
            this.label38.TabIndex = 183;
            this.label38.Text = "SANTA HELENA E GUTIERREZ";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // juncao2Benedito
            // 
            this.juncao2Benedito.Flip = 180;
            this.juncao2Benedito.Location = new System.Drawing.Point(491, 179);
            this.juncao2Benedito.Margin = new System.Windows.Forms.Padding(0);
            this.juncao2Benedito.Name = "juncao2Benedito";
            this.juncao2Benedito.Size = new System.Drawing.Size(42, 38);
            this.juncao2Benedito.TabIndex = 196;
            // 
            // junção16
            // 
            this.junção16.Flip = 180;
            this.junção16.Location = new System.Drawing.Point(445, 290);
            this.junção16.Margin = new System.Windows.Forms.Padding(0);
            this.junção16.Name = "junção16";
            this.junção16.Size = new System.Drawing.Size(29, 32);
            this.junção16.TabIndex = 191;
            // 
            // junção47
            // 
            this.junção47.Flip = 180;
            this.junção47.Location = new System.Drawing.Point(598, 298);
            this.junção47.Margin = new System.Windows.Forms.Padding(0);
            this.junção47.Name = "junção47";
            this.junção47.Size = new System.Drawing.Size(29, 32);
            this.junção47.TabIndex = 203;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.Gainsboro;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(965, 492);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(142, 69);
            this.label36.TabIndex = 181;
            this.label36.Text = "AEROPORTO E PARTE DO AMORIN";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // junção48
            // 
            this.junção48.Flip = 180;
            this.junção48.Location = new System.Drawing.Point(740, 298);
            this.junção48.Margin = new System.Windows.Forms.Padding(0);
            this.junção48.Name = "junção48";
            this.junção48.Size = new System.Drawing.Size(29, 32);
            this.junção48.TabIndex = 219;
            // 
            // junção49
            // 
            this.junção49.Flip = 90;
            this.junção49.Location = new System.Drawing.Point(685, 298);
            this.junção49.Margin = new System.Windows.Forms.Padding(0);
            this.junção49.Name = "junção49";
            this.junção49.Size = new System.Drawing.Size(30, 32);
            this.junção49.TabIndex = 218;
            // 
            // junção50
            // 
            this.junção50.Flip = 0;
            this.junção50.Location = new System.Drawing.Point(648, 322);
            this.junção50.Margin = new System.Windows.Forms.Padding(0);
            this.junção50.Name = "junção50";
            this.junção50.Size = new System.Drawing.Size(31, 32);
            this.junção50.TabIndex = 216;
            // 
            // beneditoBomba3
            // 
            this.beneditoBomba3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.beneditoBomba3.LigarBomba = false;
            this.beneditoBomba3.Location = new System.Drawing.Point(677, 322);
            this.beneditoBomba3.Name = "beneditoBomba3";
            this.beneditoBomba3.Size = new System.Drawing.Size(60, 50);
            this.beneditoBomba3.TabIndex = 215;
            this.beneditoBomba3.Transparence = false;
            // 
            // beneditoCano2Bomba3
            // 
            this.beneditoCano2Bomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano2Bomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano2Bomba3.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano2Bomba3.diminuirbolha = 5D;
            this.beneditoCano2Bomba3.enableTransparentBackground = true;
            this.beneditoCano2Bomba3.fliped = false;
            this.beneditoCano2Bomba3.fluxo = 0;
            this.beneditoCano2Bomba3.Location = new System.Drawing.Point(754, 314);
            this.beneditoCano2Bomba3.MaxVidro = 127;
            this.beneditoCano2Bomba3.MinVidro = 8;
            this.beneditoCano2Bomba3.Name = "beneditoCano2Bomba3";
            this.beneditoCano2Bomba3.RaioYElipse = 30;
            this.beneditoCano2Bomba3.Size = new System.Drawing.Size(13, 203);
            this.beneditoCano2Bomba3.TabIndex = 220;
            this.beneditoCano2Bomba3.vertical = true;
            // 
            // junção55
            // 
            this.junção55.Flip = 270;
            this.junção55.Location = new System.Drawing.Point(598, 484);
            this.junção55.Margin = new System.Windows.Forms.Padding(0);
            this.junção55.Name = "junção55";
            this.junção55.Size = new System.Drawing.Size(29, 32);
            this.junção55.TabIndex = 198;
            // 
            // beneditoCano3Bomba2
            // 
            this.beneditoCano3Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano3Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano3Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano3Bomba2.diminuirbolha = 5D;
            this.beneditoCano3Bomba2.enableTransparentBackground = true;
            this.beneditoCano3Bomba2.fliped = false;
            this.beneditoCano3Bomba2.fluxo = 0;
            this.beneditoCano3Bomba2.Location = new System.Drawing.Point(173, 501);
            this.beneditoCano3Bomba2.MaxVidro = 127;
            this.beneditoCano3Bomba2.MinVidro = 8;
            this.beneditoCano3Bomba2.Name = "beneditoCano3Bomba2";
            this.beneditoCano3Bomba2.RaioYElipse = 30;
            this.beneditoCano3Bomba2.Size = new System.Drawing.Size(435, 13);
            this.beneditoCano3Bomba2.TabIndex = 197;
            this.beneditoCano3Bomba2.vertical = false;
            // 
            // junção56
            // 
            this.junção56.Flip = 90;
            this.junção56.Location = new System.Drawing.Point(541, 298);
            this.junção56.Margin = new System.Windows.Forms.Padding(0);
            this.junção56.Name = "junção56";
            this.junção56.Size = new System.Drawing.Size(30, 32);
            this.junção56.TabIndex = 202;
            // 
            // junção57
            // 
            this.junção57.Flip = 0;
            this.junção57.Location = new System.Drawing.Point(504, 322);
            this.junção57.Margin = new System.Windows.Forms.Padding(0);
            this.junção57.Name = "junção57";
            this.junção57.Size = new System.Drawing.Size(31, 32);
            this.junção57.TabIndex = 200;
            // 
            // beneditoBomba2
            // 
            this.beneditoBomba2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.beneditoBomba2.LigarBomba = false;
            this.beneditoBomba2.Location = new System.Drawing.Point(533, 322);
            this.beneditoBomba2.Name = "beneditoBomba2";
            this.beneditoBomba2.Size = new System.Drawing.Size(60, 50);
            this.beneditoBomba2.TabIndex = 199;
            this.beneditoBomba2.Transparence = false;
            // 
            // beneditoCano2Bomba2
            // 
            this.beneditoCano2Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano2Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano2Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano2Bomba2.diminuirbolha = 5D;
            this.beneditoCano2Bomba2.enableTransparentBackground = true;
            this.beneditoCano2Bomba2.fliped = false;
            this.beneditoCano2Bomba2.fluxo = 0;
            this.beneditoCano2Bomba2.Location = new System.Drawing.Point(612, 314);
            this.beneditoCano2Bomba2.MaxVidro = 127;
            this.beneditoCano2Bomba2.MinVidro = 8;
            this.beneditoCano2Bomba2.Name = "beneditoCano2Bomba2";
            this.beneditoCano2Bomba2.RaioYElipse = 30;
            this.beneditoCano2Bomba2.Size = new System.Drawing.Size(13, 177);
            this.beneditoCano2Bomba2.TabIndex = 204;
            this.beneditoCano2Bomba2.vertical = true;
            // 
            // junção58
            // 
            this.junção58.Flip = 270;
            this.junção58.Location = new System.Drawing.Point(446, 390);
            this.junção58.Margin = new System.Windows.Forms.Padding(0);
            this.junção58.Name = "junção58";
            this.junção58.Size = new System.Drawing.Size(29, 32);
            this.junção58.TabIndex = 194;
            // 
            // junção59
            // 
            this.junção59.Flip = 90;
            this.junção59.Location = new System.Drawing.Point(353, 180);
            this.junção59.Margin = new System.Windows.Forms.Padding(0);
            this.junção59.Name = "junção59";
            this.junção59.Size = new System.Drawing.Size(30, 32);
            this.junção59.TabIndex = 190;
            // 
            // junção60
            // 
            this.junção60.Flip = 90;
            this.junção60.Location = new System.Drawing.Point(390, 290);
            this.junção60.Margin = new System.Windows.Forms.Padding(0);
            this.junção60.Name = "junção60";
            this.junção60.Size = new System.Drawing.Size(30, 32);
            this.junção60.TabIndex = 188;
            // 
            // junção61
            // 
            this.junção61.Flip = 0;
            this.junção61.Location = new System.Drawing.Point(353, 314);
            this.junção61.Margin = new System.Windows.Forms.Padding(0);
            this.junção61.Name = "junção61";
            this.junção61.Size = new System.Drawing.Size(31, 32);
            this.junção61.TabIndex = 186;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.Gainsboro;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(54, 371);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(142, 83);
            this.label37.TabIndex = 184;
            this.label37.Text = "BAIRRO SANTA HELENA";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // beneditoBomba1
            // 
            this.beneditoBomba1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.beneditoBomba1.LigarBomba = false;
            this.beneditoBomba1.Location = new System.Drawing.Point(382, 314);
            this.beneditoBomba1.Name = "beneditoBomba1";
            this.beneditoBomba1.Size = new System.Drawing.Size(60, 50);
            this.beneditoBomba1.TabIndex = 182;
            this.beneditoBomba1.Transparence = false;
            // 
            // beneditoCano21Bomba1
            // 
            this.beneditoCano21Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano21Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano21Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano21Bomba1.diminuirbolha = 5D;
            this.beneditoCano21Bomba1.enableTransparentBackground = true;
            this.beneditoCano21Bomba1.fliped = false;
            this.beneditoCano21Bomba1.fluxo = -1;
            this.beneditoCano21Bomba1.Location = new System.Drawing.Point(355, 203);
            this.beneditoCano21Bomba1.MaxVidro = 127;
            this.beneditoCano21Bomba1.MinVidro = 8;
            this.beneditoCano21Bomba1.Name = "beneditoCano21Bomba1";
            this.beneditoCano21Bomba1.RaioYElipse = 30;
            this.beneditoCano21Bomba1.Size = new System.Drawing.Size(13, 119);
            this.beneditoCano21Bomba1.TabIndex = 187;
            this.beneditoCano21Bomba1.vertical = true;
            // 
            // beneditoCanoBomba1
            // 
            this.beneditoCanoBomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCanoBomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCanoBomba1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCanoBomba1.diminuirbolha = 5D;
            this.beneditoCanoBomba1.enableTransparentBackground = true;
            this.beneditoCanoBomba1.fliped = false;
            this.beneditoCanoBomba1.fluxo = 1;
            this.beneditoCanoBomba1.Location = new System.Drawing.Point(371, 185);
            this.beneditoCanoBomba1.MaxVidro = 127;
            this.beneditoCanoBomba1.MinVidro = 8;
            this.beneditoCanoBomba1.Name = "beneditoCanoBomba1";
            this.beneditoCanoBomba1.RaioYElipse = 30;
            this.beneditoCanoBomba1.Size = new System.Drawing.Size(133, 13);
            this.beneditoCanoBomba1.TabIndex = 189;
            this.beneditoCanoBomba1.vertical = false;
            // 
            // beneditoCano2Bomba1
            // 
            this.beneditoCano2Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano2Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano2Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano2Bomba1.diminuirbolha = 5D;
            this.beneditoCano2Bomba1.enableTransparentBackground = true;
            this.beneditoCano2Bomba1.fliped = false;
            this.beneditoCano2Bomba1.fluxo = 0;
            this.beneditoCano2Bomba1.Location = new System.Drawing.Point(459, 314);
            this.beneditoCano2Bomba1.MaxVidro = 127;
            this.beneditoCano2Bomba1.MinVidro = 8;
            this.beneditoCano2Bomba1.Name = "beneditoCano2Bomba1";
            this.beneditoCano2Bomba1.RaioYElipse = 30;
            this.beneditoCano2Bomba1.Size = new System.Drawing.Size(13, 85);
            this.beneditoCano2Bomba1.TabIndex = 192;
            this.beneditoCano2Bomba1.vertical = true;
            // 
            // beneditoCano3Bomba1
            // 
            this.beneditoCano3Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano3Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano3Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano3Bomba1.diminuirbolha = 5D;
            this.beneditoCano3Bomba1.enableTransparentBackground = true;
            this.beneditoCano3Bomba1.fliped = false;
            this.beneditoCano3Bomba1.fluxo = 0;
            this.beneditoCano3Bomba1.Location = new System.Drawing.Point(188, 406);
            this.beneditoCano3Bomba1.MaxVidro = 127;
            this.beneditoCano3Bomba1.MinVidro = 8;
            this.beneditoCano3Bomba1.Name = "beneditoCano3Bomba1";
            this.beneditoCano3Bomba1.RaioYElipse = 30;
            this.beneditoCano3Bomba1.Size = new System.Drawing.Size(267, 13);
            this.beneditoCano3Bomba1.TabIndex = 193;
            this.beneditoCano3Bomba1.vertical = false;
            // 
            // beneditoCanoBomba2
            // 
            this.beneditoCanoBomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCanoBomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCanoBomba2.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCanoBomba2.diminuirbolha = 5D;
            this.beneditoCanoBomba2.enableTransparentBackground = true;
            this.beneditoCanoBomba2.fliped = false;
            this.beneditoCanoBomba2.fluxo = -1;
            this.beneditoCanoBomba2.Location = new System.Drawing.Point(506, 203);
            this.beneditoCanoBomba2.MaxVidro = 127;
            this.beneditoCanoBomba2.MinVidro = 8;
            this.beneditoCanoBomba2.Name = "beneditoCanoBomba2";
            this.beneditoCanoBomba2.RaioYElipse = 30;
            this.beneditoCanoBomba2.Size = new System.Drawing.Size(13, 127);
            this.beneditoCanoBomba2.TabIndex = 201;
            this.beneditoCanoBomba2.vertical = true;
            // 
            // beneditoCano1Bomba3
            // 
            this.beneditoCano1Bomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano1Bomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano1Bomba3.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano1Bomba3.diminuirbolha = 5D;
            this.beneditoCano1Bomba3.enableTransparentBackground = true;
            this.beneditoCano1Bomba3.fliped = false;
            this.beneditoCano1Bomba3.fluxo = 0;
            this.beneditoCano1Bomba3.Location = new System.Drawing.Point(703, 302);
            this.beneditoCano1Bomba3.MaxVidro = 127;
            this.beneditoCano1Bomba3.MinVidro = 8;
            this.beneditoCano1Bomba3.Name = "beneditoCano1Bomba3";
            this.beneditoCano1Bomba3.RaioYElipse = 30;
            this.beneditoCano1Bomba3.Size = new System.Drawing.Size(45, 13);
            this.beneditoCano1Bomba3.TabIndex = 221;
            this.beneditoCano1Bomba3.vertical = false;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Gainsboro;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(1095, 96);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(96, 50);
            this.label32.TabIndex = 137;
            this.label32.Text = "BELA VISTA 1";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(1210, 545);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(91, 16);
            this.label33.TabIndex = 141;
            this.label33.Text = "Poço Solt. 2";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(1113, 545);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(91, 16);
            this.label34.TabIndex = 140;
            this.label34.Text = "Poço Solt. 1";
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.Gainsboro;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(1200, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(109, 58);
            this.label35.TabIndex = 138;
            this.label35.Text = "BELA  VISTA 2";
            this.label35.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // junção41
            // 
            this.junção41.Flip = 180;
            this.junção41.Location = new System.Drawing.Point(1183, 114);
            this.junção41.Margin = new System.Windows.Forms.Padding(0);
            this.junção41.Name = "junção41";
            this.junção41.Size = new System.Drawing.Size(29, 32);
            this.junção41.TabIndex = 135;
            // 
            // junçãoT18
            // 
            this.junçãoT18.Flip = 180;
            this.junçãoT18.Location = new System.Drawing.Point(1133, 434);
            this.junçãoT18.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT18.Name = "junçãoT18";
            this.junçãoT18.Size = new System.Drawing.Size(42, 44);
            this.junçãoT18.TabIndex = 131;
            // 
            // junção42
            // 
            this.junção42.Flip = 180;
            this.junção42.Location = new System.Drawing.Point(1222, 441);
            this.junção42.Margin = new System.Windows.Forms.Padding(0);
            this.junção42.Name = "junção42";
            this.junção42.Size = new System.Drawing.Size(46, 32);
            this.junção42.TabIndex = 120;
            // 
            // beneditoPocoSolt1
            // 
            this.beneditoPocoSolt1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.beneditoPocoSolt1.LigarBomba = true;
            this.beneditoPocoSolt1.Location = new System.Drawing.Point(1116, 462);
            this.beneditoPocoSolt1.Name = "beneditoPocoSolt1";
            this.beneditoPocoSolt1.Size = new System.Drawing.Size(66, 78);
            this.beneditoPocoSolt1.TabIndex = 133;
            this.beneditoPocoSolt1.Transparence = false;
            // 
            // beneditoPocoSolt2
            // 
            this.beneditoPocoSolt2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.beneditoPocoSolt2.LigarBomba = true;
            this.beneditoPocoSolt2.Location = new System.Drawing.Point(1213, 464);
            this.beneditoPocoSolt2.Name = "beneditoPocoSolt2";
            this.beneditoPocoSolt2.Size = new System.Drawing.Size(66, 78);
            this.beneditoPocoSolt2.TabIndex = 132;
            this.beneditoPocoSolt2.Transparence = false;
            // 
            // junção43
            // 
            this.junção43.Flip = 0;
            this.junção43.Location = new System.Drawing.Point(930, 426);
            this.junção43.Margin = new System.Windows.Forms.Padding(0);
            this.junção43.Name = "junção43";
            this.junção43.Size = new System.Drawing.Size(47, 32);
            this.junção43.TabIndex = 122;
            // 
            // junção44
            // 
            this.junção44.Flip = 270;
            this.junção44.Location = new System.Drawing.Point(1238, 373);
            this.junção44.Margin = new System.Windows.Forms.Padding(0);
            this.junção44.Name = "junção44";
            this.junção44.Size = new System.Drawing.Size(29, 32);
            this.junção44.TabIndex = 128;
            // 
            // junção45
            // 
            this.junção45.Flip = 90;
            this.junção45.Location = new System.Drawing.Point(938, 165);
            this.junção45.Margin = new System.Windows.Forms.Padding(0);
            this.junção45.Name = "junção45";
            this.junção45.Size = new System.Drawing.Size(30, 32);
            this.junção45.TabIndex = 124;
            // 
            // beneditoCano3PocoSolt1
            // 
            this.beneditoCano3PocoSolt1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano3PocoSolt1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano3PocoSolt1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano3PocoSolt1.diminuirbolha = 5D;
            this.beneditoCano3PocoSolt1.enableTransparentBackground = true;
            this.beneditoCano3PocoSolt1.fliped = false;
            this.beneditoCano3PocoSolt1.fluxo = -1;
            this.beneditoCano3PocoSolt1.Location = new System.Drawing.Point(960, 443);
            this.beneditoCano3PocoSolt1.MaxVidro = 127;
            this.beneditoCano3PocoSolt1.MinVidro = 8;
            this.beneditoCano3PocoSolt1.Name = "beneditoCano3PocoSolt1";
            this.beneditoCano3PocoSolt1.RaioYElipse = 30;
            this.beneditoCano3PocoSolt1.Size = new System.Drawing.Size(178, 13);
            this.beneditoCano3PocoSolt1.TabIndex = 121;
            this.beneditoCano3PocoSolt1.vertical = false;
            // 
            // beneditoTanque1
            // 
            this.beneditoTanque1.BackColor = System.Drawing.Color.Transparent;
            this.beneditoTanque1.Cano = 2;
            this.beneditoTanque1.CanoCheio = true;
            this.beneditoTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoTanque1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoTanque1.enableTransparentBackground = true;
            this.beneditoTanque1.LarguraCano = 13;
            this.beneditoTanque1.Location = new System.Drawing.Point(1017, 194);
            this.beneditoTanque1.MaxVidro = 127;
            this.beneditoTanque1.MinVidro = 8;
            this.beneditoTanque1.Name = "beneditoTanque1";
            this.beneditoTanque1.PosCano = 15;
            this.beneditoTanque1.RaioYElipse = 30;
            this.beneditoTanque1.Size = new System.Drawing.Size(174, 237);
            this.beneditoTanque1.TabIndex = 119;
            this.beneditoTanque1.Value = 40D;
            // 
            // beneditoCano1PocoSolt1
            // 
            this.beneditoCano1PocoSolt1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano1PocoSolt1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano1PocoSolt1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano1PocoSolt1.diminuirbolha = 5D;
            this.beneditoCano1PocoSolt1.enableTransparentBackground = true;
            this.beneditoCano1PocoSolt1.fliped = false;
            this.beneditoCano1PocoSolt1.fluxo = -1;
            this.beneditoCano1PocoSolt1.Location = new System.Drawing.Point(960, 169);
            this.beneditoCano1PocoSolt1.MaxVidro = 127;
            this.beneditoCano1PocoSolt1.MinVidro = 8;
            this.beneditoCano1PocoSolt1.Name = "beneditoCano1PocoSolt1";
            this.beneditoCano1PocoSolt1.RaioYElipse = 30;
            this.beneditoCano1PocoSolt1.Size = new System.Drawing.Size(63, 13);
            this.beneditoCano1PocoSolt1.TabIndex = 125;
            this.beneditoCano1PocoSolt1.vertical = false;
            // 
            // junçãoT19
            // 
            this.junçãoT19.Flip = 0;
            this.junçãoT19.Location = new System.Drawing.Point(1183, 368);
            this.junçãoT19.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT19.Name = "junçãoT19";
            this.junçãoT19.Size = new System.Drawing.Size(42, 44);
            this.junçãoT19.TabIndex = 126;
            // 
            // beneditoCano1BelaVista2
            // 
            this.beneditoCano1BelaVista2.BackColor = System.Drawing.Color.Transparent;
            this.beneditoCano1BelaVista2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano1BelaVista2.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano1BelaVista2.diminuirbolha = 5D;
            this.beneditoCano1BelaVista2.enableTransparentBackground = true;
            this.beneditoCano1BelaVista2.fliped = false;
            this.beneditoCano1BelaVista2.fluxo = -1;
            this.beneditoCano1BelaVista2.Location = new System.Drawing.Point(1213, 390);
            this.beneditoCano1BelaVista2.MaxVidro = 127;
            this.beneditoCano1BelaVista2.MinVidro = 8;
            this.beneditoCano1BelaVista2.Name = "beneditoCano1BelaVista2";
            this.beneditoCano1BelaVista2.RaioYElipse = 30;
            this.beneditoCano1BelaVista2.Size = new System.Drawing.Size(35, 13);
            this.beneditoCano1BelaVista2.TabIndex = 127;
            this.beneditoCano1BelaVista2.vertical = false;
            // 
            // beneditoCano2BelaVista2
            // 
            this.beneditoCano2BelaVista2.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano2BelaVista2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano2BelaVista2.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano2BelaVista2.diminuirbolha = 5D;
            this.beneditoCano2BelaVista2.enableTransparentBackground = true;
            this.beneditoCano2BelaVista2.fliped = false;
            this.beneditoCano2BelaVista2.fluxo = 1;
            this.beneditoCano2BelaVista2.Location = new System.Drawing.Point(1251, 50);
            this.beneditoCano2BelaVista2.MaxVidro = 127;
            this.beneditoCano2BelaVista2.MinVidro = 8;
            this.beneditoCano2BelaVista2.Name = "beneditoCano2BelaVista2";
            this.beneditoCano2BelaVista2.RaioYElipse = 30;
            this.beneditoCano2BelaVista2.Size = new System.Drawing.Size(13, 328);
            this.beneditoCano2BelaVista2.TabIndex = 129;
            this.beneditoCano2BelaVista2.vertical = true;
            // 
            // beneditoCanoBelaVista1
            // 
            this.beneditoCanoBelaVista1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCanoBelaVista1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCanoBelaVista1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCanoBelaVista1.diminuirbolha = 5D;
            this.beneditoCanoBelaVista1.enableTransparentBackground = true;
            this.beneditoCanoBelaVista1.fliped = false;
            this.beneditoCanoBelaVista1.fluxo = 1;
            this.beneditoCanoBelaVista1.Location = new System.Drawing.Point(1197, 140);
            this.beneditoCanoBelaVista1.MaxVidro = 127;
            this.beneditoCanoBelaVista1.MinVidro = 8;
            this.beneditoCanoBelaVista1.Name = "beneditoCanoBelaVista1";
            this.beneditoCanoBelaVista1.RaioYElipse = 30;
            this.beneditoCanoBelaVista1.Size = new System.Drawing.Size(13, 243);
            this.beneditoCanoBelaVista1.TabIndex = 130;
            this.beneditoCanoBelaVista1.vertical = true;
            // 
            // beneditoCanoPocoSolt2
            // 
            this.beneditoCanoPocoSolt2.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCanoPocoSolt2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCanoPocoSolt2.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCanoPocoSolt2.diminuirbolha = 5D;
            this.beneditoCanoPocoSolt2.enableTransparentBackground = true;
            this.beneditoCanoPocoSolt2.fliped = false;
            this.beneditoCanoPocoSolt2.fluxo = -1;
            this.beneditoCanoPocoSolt2.Location = new System.Drawing.Point(1167, 443);
            this.beneditoCanoPocoSolt2.MaxVidro = 127;
            this.beneditoCanoPocoSolt2.MinVidro = 8;
            this.beneditoCanoPocoSolt2.Name = "beneditoCanoPocoSolt2";
            this.beneditoCanoPocoSolt2.RaioYElipse = 30;
            this.beneditoCanoPocoSolt2.Size = new System.Drawing.Size(81, 13);
            this.beneditoCanoPocoSolt2.TabIndex = 134;
            this.beneditoCanoPocoSolt2.vertical = false;
            // 
            // beneditoCanoPoco1
            // 
            this.beneditoCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCanoPoco1.diminuirbolha = 5D;
            this.beneditoCanoPoco1.enableTransparentBackground = true;
            this.beneditoCanoPoco1.fliped = false;
            this.beneditoCanoPoco1.fluxo = 1;
            this.beneditoCanoPoco1.Location = new System.Drawing.Point(573, 19);
            this.beneditoCanoPoco1.MaxVidro = 127;
            this.beneditoCanoPoco1.MinVidro = 8;
            this.beneditoCanoPoco1.Name = "beneditoCanoPoco1";
            this.beneditoCanoPoco1.RaioYElipse = 30;
            this.beneditoCanoPoco1.Size = new System.Drawing.Size(265, 13);
            this.beneditoCanoPoco1.TabIndex = 239;
            this.beneditoCanoPoco1.vertical = false;
            // 
            // beneditoTanque2
            // 
            this.beneditoTanque2.BackColor = System.Drawing.Color.Transparent;
            this.beneditoTanque2.Cano = 2;
            this.beneditoTanque2.CanoCheio = true;
            this.beneditoTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoTanque2.CorVidro = System.Drawing.Color.Silver;
            this.beneditoTanque2.enableTransparentBackground = true;
            this.beneditoTanque2.LarguraCano = 13;
            this.beneditoTanque2.Location = new System.Drawing.Point(537, 49);
            this.beneditoTanque2.MaxVidro = 127;
            this.beneditoTanque2.MinVidro = 8;
            this.beneditoTanque2.Name = "beneditoTanque2";
            this.beneditoTanque2.PosCano = 15;
            this.beneditoTanque2.RaioYElipse = 30;
            this.beneditoTanque2.Size = new System.Drawing.Size(249, 176);
            this.beneditoTanque2.TabIndex = 180;
            this.beneditoTanque2.Value = 40D;
            // 
            // beneditoCanoBomba3
            // 
            this.beneditoCanoBomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCanoBomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCanoBomba3.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCanoBomba3.diminuirbolha = 5D;
            this.beneditoCanoBomba3.enableTransparentBackground = true;
            this.beneditoCanoBomba3.fliped = false;
            this.beneditoCanoBomba3.fluxo = -1;
            this.beneditoCanoBomba3.Location = new System.Drawing.Point(650, 216);
            this.beneditoCanoBomba3.MaxVidro = 127;
            this.beneditoCanoBomba3.MinVidro = 8;
            this.beneditoCanoBomba3.Name = "beneditoCanoBomba3";
            this.beneditoCanoBomba3.RaioYElipse = 30;
            this.beneditoCanoBomba3.Size = new System.Drawing.Size(13, 114);
            this.beneditoCanoBomba3.TabIndex = 217;
            this.beneditoCanoBomba3.vertical = true;
            // 
            // beneditoCanoChamcia
            // 
            this.beneditoCanoChamcia.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCanoChamcia.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCanoChamcia.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCanoChamcia.diminuirbolha = 5D;
            this.beneditoCanoChamcia.enableTransparentBackground = true;
            this.beneditoCanoChamcia.fliped = false;
            this.beneditoCanoChamcia.fluxo = 0;
            this.beneditoCanoChamcia.Location = new System.Drawing.Point(124, 17);
            this.beneditoCanoChamcia.MaxVidro = 127;
            this.beneditoCanoChamcia.MinVidro = 8;
            this.beneditoCanoChamcia.Name = "beneditoCanoChamcia";
            this.beneditoCanoChamcia.RaioYElipse = 30;
            this.beneditoCanoChamcia.Size = new System.Drawing.Size(418, 13);
            this.beneditoCanoChamcia.TabIndex = 236;
            this.beneditoCanoChamcia.vertical = false;
            // 
            // beneditoCano1Bomba2
            // 
            this.beneditoCano1Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano1Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano1Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano1Bomba2.diminuirbolha = 5D;
            this.beneditoCano1Bomba2.enableTransparentBackground = true;
            this.beneditoCano1Bomba2.fliped = false;
            this.beneditoCano1Bomba2.fluxo = 0;
            this.beneditoCano1Bomba2.Location = new System.Drawing.Point(566, 302);
            this.beneditoCano1Bomba2.MaxVidro = 127;
            this.beneditoCano1Bomba2.MinVidro = 8;
            this.beneditoCano1Bomba2.Name = "beneditoCano1Bomba2";
            this.beneditoCano1Bomba2.RaioYElipse = 30;
            this.beneditoCano1Bomba2.Size = new System.Drawing.Size(35, 13);
            this.beneditoCano1Bomba2.TabIndex = 205;
            this.beneditoCano1Bomba2.vertical = false;
            // 
            // beneditoCano1Bomba1
            // 
            this.beneditoCano1Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano1Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano1Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano1Bomba1.diminuirbolha = 5D;
            this.beneditoCano1Bomba1.enableTransparentBackground = true;
            this.beneditoCano1Bomba1.fliped = false;
            this.beneditoCano1Bomba1.fluxo = 0;
            this.beneditoCano1Bomba1.Location = new System.Drawing.Point(414, 294);
            this.beneditoCano1Bomba1.MaxVidro = 127;
            this.beneditoCano1Bomba1.MinVidro = 8;
            this.beneditoCano1Bomba1.Name = "beneditoCano1Bomba1";
            this.beneditoCano1Bomba1.RaioYElipse = 30;
            this.beneditoCano1Bomba1.Size = new System.Drawing.Size(35, 13);
            this.beneditoCano1Bomba1.TabIndex = 195;
            this.beneditoCano1Bomba1.vertical = false;
            // 
            // beneditoCano2PocoSolt1
            // 
            this.beneditoCano2PocoSolt1.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano2PocoSolt1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano2PocoSolt1.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano2PocoSolt1.diminuirbolha = 5D;
            this.beneditoCano2PocoSolt1.enableTransparentBackground = true;
            this.beneditoCano2PocoSolt1.fliped = false;
            this.beneditoCano2PocoSolt1.fluxo = -1;
            this.beneditoCano2PocoSolt1.Location = new System.Drawing.Point(940, 183);
            this.beneditoCano2PocoSolt1.MaxVidro = 127;
            this.beneditoCano2PocoSolt1.MinVidro = 8;
            this.beneditoCano2PocoSolt1.Name = "beneditoCano2PocoSolt1";
            this.beneditoCano2PocoSolt1.RaioYElipse = 30;
            this.beneditoCano2PocoSolt1.Size = new System.Drawing.Size(13, 247);
            this.beneditoCano2PocoSolt1.TabIndex = 139;
            this.beneditoCano2PocoSolt1.vertical = true;
            // 
            // beneditoCano3Bomba3
            // 
            this.beneditoCano3Bomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.beneditoCano3Bomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.beneditoCano3Bomba3.CorVidro = System.Drawing.Color.Silver;
            this.beneditoCano3Bomba3.diminuirbolha = 5D;
            this.beneditoCano3Bomba3.enableTransparentBackground = true;
            this.beneditoCano3Bomba3.fliped = false;
            this.beneditoCano3Bomba3.fluxo = 0;
            this.beneditoCano3Bomba3.Location = new System.Drawing.Point(782, 524);
            this.beneditoCano3Bomba3.MaxVidro = 127;
            this.beneditoCano3Bomba3.MinVidro = 8;
            this.beneditoCano3Bomba3.Name = "beneditoCano3Bomba3";
            this.beneditoCano3Bomba3.RaioYElipse = 30;
            this.beneditoCano3Bomba3.Size = new System.Drawing.Size(186, 13);
            this.beneditoCano3Bomba3.TabIndex = 245;
            this.beneditoCano3Bomba3.vertical = false;
            // 
            // cano2
            // 
            this.cano2.BackColor = System.Drawing.Color.Gainsboro;
            this.cano2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.cano2.CorVidro = System.Drawing.Color.Silver;
            this.cano2.diminuirbolha = 5D;
            this.cano2.enableTransparentBackground = true;
            this.cano2.fliped = false;
            this.cano2.fluxo = 1;
            this.cano2.Location = new System.Drawing.Point(522, 185);
            this.cano2.MaxVidro = 127;
            this.cano2.MinVidro = 8;
            this.cano2.Name = "cano2";
            this.cano2.RaioYElipse = 30;
            this.cano2.Size = new System.Drawing.Size(20, 13);
            this.cano2.TabIndex = 250;
            this.cano2.vertical = false;
            // 
            // Chamcia
            // 
            this.Chamcia.Controls.Add(this.chamciaVazaoBomba4);
            this.Chamcia.Controls.Add(this.waterMeter7);
            this.Chamcia.Controls.Add(this.chamciaVazaoBomba3);
            this.Chamcia.Controls.Add(this.waterMeter6);
            this.Chamcia.Controls.Add(this.chamciaVazaoBomba2);
            this.Chamcia.Controls.Add(this.waterMeter5);
            this.Chamcia.Controls.Add(this.chamciaVazaoBomba1);
            this.Chamcia.Controls.Add(this.waterMeter1);
            this.Chamcia.Controls.Add(this.junçãoT16);
            this.Chamcia.Controls.Add(this.junção25);
            this.Chamcia.Controls.Add(this.junção10);
            this.Chamcia.Controls.Add(this.label30);
            this.Chamcia.Controls.Add(this.label16);
            this.Chamcia.Controls.Add(this.junção33);
            this.Chamcia.Controls.Add(this.junção38);
            this.Chamcia.Controls.Add(this.junçãoT8);
            this.Chamcia.Controls.Add(this.junção39);
            this.Chamcia.Controls.Add(this.junção40);
            this.Chamcia.Controls.Add(this.chamciaBomba4);
            this.Chamcia.Controls.Add(this.chamciaCano2Bomba4);
            this.Chamcia.Controls.Add(this.junção37);
            this.Chamcia.Controls.Add(this.junção34);
            this.Chamcia.Controls.Add(this.junção35);
            this.Chamcia.Controls.Add(this.junção36);
            this.Chamcia.Controls.Add(this.chamciaBomba3);
            this.Chamcia.Controls.Add(this.chamciaCano2Bomba3);
            this.Chamcia.Controls.Add(this.junção26);
            this.Chamcia.Controls.Add(this.junção14);
            this.Chamcia.Controls.Add(this.chamciaCano3Bomba2);
            this.Chamcia.Controls.Add(this.junção11);
            this.Chamcia.Controls.Add(this.junção12);
            this.Chamcia.Controls.Add(this.chamciaBomba2);
            this.Chamcia.Controls.Add(this.chamciaCano2Bomba2);
            this.Chamcia.Controls.Add(this.junção5);
            this.Chamcia.Controls.Add(this.chamciaTanque1);
            this.Chamcia.Controls.Add(this.chamciaCanoBomba21);
            this.Chamcia.Controls.Add(this.junção30);
            this.Chamcia.Controls.Add(this.junção15);
            this.Chamcia.Controls.Add(this.junção13);
            this.Chamcia.Controls.Add(this.junção9);
            this.Chamcia.Controls.Add(this.label29);
            this.Chamcia.Controls.Add(this.label28);
            this.Chamcia.Controls.Add(this.label17);
            this.Chamcia.Controls.Add(this.junção8);
            this.Chamcia.Controls.Add(this.label27);
            this.Chamcia.Controls.Add(this.label26);
            this.Chamcia.Controls.Add(this.junçãoT6);
            this.Chamcia.Controls.Add(this.junção21);
            this.Chamcia.Controls.Add(this.chamciaPocoSolt1);
            this.Chamcia.Controls.Add(this.chamciaPocoSolt2);
            this.Chamcia.Controls.Add(this.chamciaBomba1);
            this.Chamcia.Controls.Add(this.junção3);
            this.Chamcia.Controls.Add(this.junçãoT15);
            this.Chamcia.Controls.Add(this.chamciaPoco1);
            this.Chamcia.Controls.Add(this.chamciaCanoPoco2);
            this.Chamcia.Controls.Add(this.label25);
            this.Chamcia.Controls.Add(this.junçãoT11);
            this.Chamcia.Controls.Add(this.junçãoT12);
            this.Chamcia.Controls.Add(this.junçãoT13);
            this.Chamcia.Controls.Add(this.junçãoT14);
            this.Chamcia.Controls.Add(this.chamciaCanoPoco6);
            this.Chamcia.Controls.Add(this.chamciaPoco2);
            this.Chamcia.Controls.Add(this.chamciaPoco3);
            this.Chamcia.Controls.Add(this.chamciaPoco4);
            this.Chamcia.Controls.Add(this.chamciaPoco5);
            this.Chamcia.Controls.Add(this.chamciaCanoPoco1);
            this.Chamcia.Controls.Add(this.chamciaCanoPoco3);
            this.Chamcia.Controls.Add(this.chamciaCanoPoco4);
            this.Chamcia.Controls.Add(this.chamciaCanoPoco5);
            this.Chamcia.Controls.Add(this.label19);
            this.Chamcia.Controls.Add(this.label20);
            this.Chamcia.Controls.Add(this.label21);
            this.Chamcia.Controls.Add(this.label22);
            this.Chamcia.Controls.Add(this.label23);
            this.Chamcia.Controls.Add(this.label24);
            this.Chamcia.Controls.Add(this.chamciaPoco7);
            this.Chamcia.Controls.Add(this.chamciaPoco6);
            this.Chamcia.Controls.Add(this.chamciaCanoPoco7);
            this.Chamcia.Controls.Add(this.junção22);
            this.Chamcia.Controls.Add(this.junção27);
            this.Chamcia.Controls.Add(this.junção24);
            this.Chamcia.Controls.Add(this.junção23);
            this.Chamcia.Controls.Add(this.chamciaCano3PocoSolt1);
            this.Chamcia.Controls.Add(this.label18);
            this.Chamcia.Controls.Add(this.chamciaTanque4);
            this.Chamcia.Controls.Add(this.chamciaTanque3);
            this.Chamcia.Controls.Add(this.chamciaTanque2);
            this.Chamcia.Controls.Add(this.chamciaCano1PocoSolt1);
            this.Chamcia.Controls.Add(this.chamciaCano2PocoSolt1);
            this.Chamcia.Controls.Add(this.junçãoT7);
            this.Chamcia.Controls.Add(this.chamciaCano1Suica3);
            this.Chamcia.Controls.Add(this.chamciaCano2Suica3);
            this.Chamcia.Controls.Add(this.chamciaCanoSuica2);
            this.Chamcia.Controls.Add(this.chamciaCanoPocoSolt2);
            this.Chamcia.Controls.Add(this.chamciaCanoBomba1);
            this.Chamcia.Controls.Add(this.chamciaCano12Bomba1);
            this.Chamcia.Controls.Add(this.chamciaCano2Bomba1);
            this.Chamcia.Controls.Add(this.chamciaCanoBomba2);
            this.Chamcia.Controls.Add(this.chamciaCano3Bomba3);
            this.Chamcia.Controls.Add(this.chamciaCanoBomba3);
            this.Chamcia.Controls.Add(this.chamciaCanoBomba4);
            this.Chamcia.Controls.Add(this.chamciaCano1Bomba4);
            this.Chamcia.Controls.Add(this.chamciaCanoTanque2);
            this.Chamcia.Controls.Add(this.chamciaCanoTanque3);
            this.Chamcia.Controls.Add(this.chamciaCano1Bomba3);
            this.Chamcia.Controls.Add(this.chamciaCano3Bomba1);
            this.Chamcia.Controls.Add(this.chamciaCano1Bomba2);
            this.Chamcia.Controls.Add(this.chamciaCano1Bomba1);
            this.Chamcia.Controls.Add(this.chamciaCano3Bomba4);
            this.Chamcia.Location = new System.Drawing.Point(4, 22);
            this.Chamcia.Name = "Chamcia";
            this.Chamcia.Size = new System.Drawing.Size(1480, 662);
            this.Chamcia.TabIndex = 2;
            this.Chamcia.Text = "CHANCIA";
            this.Chamcia.UseVisualStyleBackColor = true;
            // 
            // chamciaVazaoBomba4
            // 
            this.chamciaVazaoBomba4.AutoSize = true;
            this.chamciaVazaoBomba4.BackColor = System.Drawing.Color.Black;
            this.chamciaVazaoBomba4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chamciaVazaoBomba4.ForeColor = System.Drawing.Color.Transparent;
            this.chamciaVazaoBomba4.Location = new System.Drawing.Point(890, 535);
            this.chamciaVazaoBomba4.Name = "chamciaVazaoBomba4";
            this.chamciaVazaoBomba4.Size = new System.Drawing.Size(76, 16);
            this.chamciaVazaoBomba4.TabIndex = 188;
            this.chamciaVazaoBomba4.Text = "00.00 (m³/h)";
            // 
            // waterMeter7
            // 
            this.waterMeter7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter7.Location = new System.Drawing.Point(893, 477);
            this.waterMeter7.Name = "waterMeter7";
            this.waterMeter7.Size = new System.Drawing.Size(55, 55);
            this.waterMeter7.TabIndex = 187;
            this.waterMeter7.Transparence = false;
            // 
            // chamciaVazaoBomba3
            // 
            this.chamciaVazaoBomba3.AutoSize = true;
            this.chamciaVazaoBomba3.BackColor = System.Drawing.Color.Black;
            this.chamciaVazaoBomba3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chamciaVazaoBomba3.ForeColor = System.Drawing.Color.Transparent;
            this.chamciaVazaoBomba3.Location = new System.Drawing.Point(468, 519);
            this.chamciaVazaoBomba3.Name = "chamciaVazaoBomba3";
            this.chamciaVazaoBomba3.Size = new System.Drawing.Size(76, 16);
            this.chamciaVazaoBomba3.TabIndex = 186;
            this.chamciaVazaoBomba3.Text = "00.00 (m³/h)";
            // 
            // waterMeter6
            // 
            this.waterMeter6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter6.Location = new System.Drawing.Point(471, 461);
            this.waterMeter6.Name = "waterMeter6";
            this.waterMeter6.Size = new System.Drawing.Size(55, 55);
            this.waterMeter6.TabIndex = 185;
            this.waterMeter6.Transparence = false;
            // 
            // chamciaVazaoBomba2
            // 
            this.chamciaVazaoBomba2.AutoSize = true;
            this.chamciaVazaoBomba2.BackColor = System.Drawing.Color.Black;
            this.chamciaVazaoBomba2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chamciaVazaoBomba2.ForeColor = System.Drawing.Color.Transparent;
            this.chamciaVazaoBomba2.Location = new System.Drawing.Point(168, 436);
            this.chamciaVazaoBomba2.Name = "chamciaVazaoBomba2";
            this.chamciaVazaoBomba2.Size = new System.Drawing.Size(76, 16);
            this.chamciaVazaoBomba2.TabIndex = 184;
            this.chamciaVazaoBomba2.Text = "00.00 (m³/h)";
            // 
            // waterMeter5
            // 
            this.waterMeter5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter5.Location = new System.Drawing.Point(173, 377);
            this.waterMeter5.Name = "waterMeter5";
            this.waterMeter5.Size = new System.Drawing.Size(55, 55);
            this.waterMeter5.TabIndex = 183;
            this.waterMeter5.Transparence = false;
            // 
            // chamciaVazaoBomba1
            // 
            this.chamciaVazaoBomba1.AutoSize = true;
            this.chamciaVazaoBomba1.BackColor = System.Drawing.Color.Black;
            this.chamciaVazaoBomba1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chamciaVazaoBomba1.ForeColor = System.Drawing.Color.Transparent;
            this.chamciaVazaoBomba1.Location = new System.Drawing.Point(168, 356);
            this.chamciaVazaoBomba1.Name = "chamciaVazaoBomba1";
            this.chamciaVazaoBomba1.Size = new System.Drawing.Size(76, 16);
            this.chamciaVazaoBomba1.TabIndex = 181;
            this.chamciaVazaoBomba1.Text = "00.00 (m³/h)";
            // 
            // waterMeter1
            // 
            this.waterMeter1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.waterMeter1.Location = new System.Drawing.Point(173, 301);
            this.waterMeter1.Name = "waterMeter1";
            this.waterMeter1.Size = new System.Drawing.Size(55, 55);
            this.waterMeter1.TabIndex = 180;
            this.waterMeter1.Transparence = true;
            // 
            // junçãoT16
            // 
            this.junçãoT16.Flip = 180;
            this.junçãoT16.Location = new System.Drawing.Point(411, 168);
            this.junçãoT16.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT16.Name = "junçãoT16";
            this.junçãoT16.Size = new System.Drawing.Size(42, 38);
            this.junçãoT16.TabIndex = 147;
            // 
            // junção25
            // 
            this.junção25.Flip = 180;
            this.junção25.Location = new System.Drawing.Point(384, 219);
            this.junção25.Margin = new System.Windows.Forms.Padding(0);
            this.junção25.Name = "junção25";
            this.junção25.Size = new System.Drawing.Size(29, 32);
            this.junção25.TabIndex = 140;
            // 
            // junção10
            // 
            this.junção10.Flip = 180;
            this.junção10.Location = new System.Drawing.Point(516, 242);
            this.junção10.Margin = new System.Windows.Forms.Padding(0);
            this.junção10.Name = "junção10";
            this.junção10.Size = new System.Drawing.Size(29, 32);
            this.junção10.TabIndex = 156;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Gainsboro;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(3, 433);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(142, 72);
            this.label30.TabIndex = 121;
            this.label30.Text = "BAIRROS FÁTIMA CHANCIA";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Gainsboro;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(972, 484);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(142, 69);
            this.label16.TabIndex = 57;
            this.label16.Text = "BATERIA SÃO BENEDITO E ESTADUAL";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // junção33
            // 
            this.junção33.Flip = 0;
            this.junção33.Location = new System.Drawing.Point(719, 495);
            this.junção33.Margin = new System.Windows.Forms.Padding(0);
            this.junção33.Name = "junção33";
            this.junção33.Size = new System.Drawing.Size(47, 32);
            this.junção33.TabIndex = 150;
            // 
            // junção38
            // 
            this.junção38.Flip = 180;
            this.junção38.Location = new System.Drawing.Point(715, 275);
            this.junção38.Margin = new System.Windows.Forms.Padding(0);
            this.junção38.Name = "junção38";
            this.junção38.Size = new System.Drawing.Size(29, 32);
            this.junção38.TabIndex = 173;
            // 
            // junçãoT8
            // 
            this.junçãoT8.Flip = 180;
            this.junçãoT8.Location = new System.Drawing.Point(375, 31);
            this.junçãoT8.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT8.Name = "junçãoT8";
            this.junçãoT8.Size = new System.Drawing.Size(42, 44);
            this.junçãoT8.TabIndex = 96;
            // 
            // junção39
            // 
            this.junção39.Flip = 90;
            this.junção39.Location = new System.Drawing.Point(647, 275);
            this.junção39.Margin = new System.Windows.Forms.Padding(0);
            this.junção39.Name = "junção39";
            this.junção39.Size = new System.Drawing.Size(30, 32);
            this.junção39.TabIndex = 172;
            // 
            // junção40
            // 
            this.junção40.Flip = 0;
            this.junção40.Location = new System.Drawing.Point(610, 299);
            this.junção40.Margin = new System.Windows.Forms.Padding(0);
            this.junção40.Name = "junção40";
            this.junção40.Size = new System.Drawing.Size(31, 32);
            this.junção40.TabIndex = 170;
            // 
            // chamciaBomba4
            // 
            this.chamciaBomba4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaBomba4.LigarBomba = false;
            this.chamciaBomba4.Location = new System.Drawing.Point(639, 299);
            this.chamciaBomba4.Name = "chamciaBomba4";
            this.chamciaBomba4.Size = new System.Drawing.Size(60, 50);
            this.chamciaBomba4.TabIndex = 169;
            this.chamciaBomba4.Transparence = false;
            // 
            // chamciaCano2Bomba4
            // 
            this.chamciaCano2Bomba4.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano2Bomba4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano2Bomba4.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano2Bomba4.diminuirbolha = 5D;
            this.chamciaCano2Bomba4.enableTransparentBackground = true;
            this.chamciaCano2Bomba4.fliped = false;
            this.chamciaCano2Bomba4.fluxo = 0;
            this.chamciaCano2Bomba4.Location = new System.Drawing.Point(729, 299);
            this.chamciaCano2Bomba4.MaxVidro = 127;
            this.chamciaCano2Bomba4.MinVidro = 8;
            this.chamciaCano2Bomba4.Name = "chamciaCano2Bomba4";
            this.chamciaCano2Bomba4.RaioYElipse = 30;
            this.chamciaCano2Bomba4.Size = new System.Drawing.Size(13, 205);
            this.chamciaCano2Bomba4.TabIndex = 174;
            this.chamciaCano2Bomba4.vertical = true;
            // 
            // junção37
            // 
            this.junção37.BackColor = System.Drawing.Color.Transparent;
            this.junção37.Flip = 270;
            this.junção37.Location = new System.Drawing.Point(679, 478);
            this.junção37.Margin = new System.Windows.Forms.Padding(0);
            this.junção37.Name = "junção37";
            this.junção37.Size = new System.Drawing.Size(29, 32);
            this.junção37.TabIndex = 167;
            // 
            // junção34
            // 
            this.junção34.Flip = 180;
            this.junção34.Location = new System.Drawing.Point(679, 371);
            this.junção34.Margin = new System.Windows.Forms.Padding(0);
            this.junção34.Name = "junção34";
            this.junção34.Size = new System.Drawing.Size(29, 32);
            this.junção34.TabIndex = 164;
            // 
            // junção35
            // 
            this.junção35.Flip = 90;
            this.junção35.Location = new System.Drawing.Point(624, 371);
            this.junção35.Margin = new System.Windows.Forms.Padding(0);
            this.junção35.Name = "junção35";
            this.junção35.Size = new System.Drawing.Size(30, 32);
            this.junção35.TabIndex = 163;
            // 
            // junção36
            // 
            this.junção36.Flip = 0;
            this.junção36.Location = new System.Drawing.Point(587, 395);
            this.junção36.Margin = new System.Windows.Forms.Padding(0);
            this.junção36.Name = "junção36";
            this.junção36.Size = new System.Drawing.Size(31, 32);
            this.junção36.TabIndex = 161;
            // 
            // chamciaBomba3
            // 
            this.chamciaBomba3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaBomba3.LigarBomba = false;
            this.chamciaBomba3.Location = new System.Drawing.Point(616, 395);
            this.chamciaBomba3.Name = "chamciaBomba3";
            this.chamciaBomba3.Size = new System.Drawing.Size(60, 50);
            this.chamciaBomba3.TabIndex = 160;
            this.chamciaBomba3.Transparence = false;
            // 
            // chamciaCano2Bomba3
            // 
            this.chamciaCano2Bomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano2Bomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano2Bomba3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano2Bomba3.diminuirbolha = 5D;
            this.chamciaCano2Bomba3.enableTransparentBackground = true;
            this.chamciaCano2Bomba3.fliped = false;
            this.chamciaCano2Bomba3.fluxo = 0;
            this.chamciaCano2Bomba3.Location = new System.Drawing.Point(693, 395);
            this.chamciaCano2Bomba3.MaxVidro = 127;
            this.chamciaCano2Bomba3.MinVidro = 8;
            this.chamciaCano2Bomba3.Name = "chamciaCano2Bomba3";
            this.chamciaCano2Bomba3.RaioYElipse = 30;
            this.chamciaCano2Bomba3.Size = new System.Drawing.Size(13, 89);
            this.chamciaCano2Bomba3.TabIndex = 165;
            this.chamciaCano2Bomba3.vertical = true;
            // 
            // junção26
            // 
            this.junção26.Flip = 90;
            this.junção26.Location = new System.Drawing.Point(65, 408);
            this.junção26.Margin = new System.Windows.Forms.Padding(0);
            this.junção26.Name = "junção26";
            this.junção26.Size = new System.Drawing.Size(30, 32);
            this.junção26.TabIndex = 158;
            // 
            // junção14
            // 
            this.junção14.Flip = 270;
            this.junção14.Location = new System.Drawing.Point(516, 395);
            this.junção14.Margin = new System.Windows.Forms.Padding(0);
            this.junção14.Name = "junção14";
            this.junção14.Size = new System.Drawing.Size(29, 32);
            this.junção14.TabIndex = 149;
            // 
            // chamciaCano3Bomba2
            // 
            this.chamciaCano3Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano3Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano3Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano3Bomba2.diminuirbolha = 5D;
            this.chamciaCano3Bomba2.enableTransparentBackground = true;
            this.chamciaCano3Bomba2.fliped = false;
            this.chamciaCano3Bomba2.fluxo = 0;
            this.chamciaCano3Bomba2.Location = new System.Drawing.Point(83, 411);
            this.chamciaCano3Bomba2.MaxVidro = 127;
            this.chamciaCano3Bomba2.MinVidro = 8;
            this.chamciaCano3Bomba2.Name = "chamciaCano3Bomba2";
            this.chamciaCano3Bomba2.RaioYElipse = 30;
            this.chamciaCano3Bomba2.Size = new System.Drawing.Size(443, 13);
            this.chamciaCano3Bomba2.TabIndex = 148;
            this.chamciaCano3Bomba2.vertical = false;
            // 
            // junção11
            // 
            this.junção11.Flip = 90;
            this.junção11.Location = new System.Drawing.Point(461, 242);
            this.junção11.Margin = new System.Windows.Forms.Padding(0);
            this.junção11.Name = "junção11";
            this.junção11.Size = new System.Drawing.Size(30, 32);
            this.junção11.TabIndex = 155;
            // 
            // junção12
            // 
            this.junção12.Flip = 0;
            this.junção12.Location = new System.Drawing.Point(424, 266);
            this.junção12.Margin = new System.Windows.Forms.Padding(0);
            this.junção12.Name = "junção12";
            this.junção12.Size = new System.Drawing.Size(31, 32);
            this.junção12.TabIndex = 153;
            // 
            // chamciaBomba2
            // 
            this.chamciaBomba2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaBomba2.LigarBomba = false;
            this.chamciaBomba2.Location = new System.Drawing.Point(453, 266);
            this.chamciaBomba2.Name = "chamciaBomba2";
            this.chamciaBomba2.Size = new System.Drawing.Size(60, 50);
            this.chamciaBomba2.TabIndex = 152;
            this.chamciaBomba2.Transparence = false;
            // 
            // chamciaCano2Bomba2
            // 
            this.chamciaCano2Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano2Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano2Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano2Bomba2.diminuirbolha = 5D;
            this.chamciaCano2Bomba2.enableTransparentBackground = true;
            this.chamciaCano2Bomba2.fliped = false;
            this.chamciaCano2Bomba2.fluxo = 0;
            this.chamciaCano2Bomba2.Location = new System.Drawing.Point(530, 266);
            this.chamciaCano2Bomba2.MaxVidro = 127;
            this.chamciaCano2Bomba2.MinVidro = 8;
            this.chamciaCano2Bomba2.Name = "chamciaCano2Bomba2";
            this.chamciaCano2Bomba2.RaioYElipse = 30;
            this.chamciaCano2Bomba2.Size = new System.Drawing.Size(13, 139);
            this.chamciaCano2Bomba2.TabIndex = 157;
            this.chamciaCano2Bomba2.vertical = true;
            // 
            // junção5
            // 
            this.junção5.Flip = 180;
            this.junção5.Location = new System.Drawing.Point(511, 36);
            this.junção5.Margin = new System.Windows.Forms.Padding(0);
            this.junção5.Name = "junção5";
            this.junção5.Size = new System.Drawing.Size(29, 32);
            this.junção5.TabIndex = 110;
            // 
            // chamciaTanque1
            // 
            this.chamciaTanque1.BackColor = System.Drawing.Color.Transparent;
            this.chamciaTanque1.Cano = 2;
            this.chamciaTanque1.CanoCheio = true;
            this.chamciaTanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaTanque1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaTanque1.enableTransparentBackground = true;
            this.chamciaTanque1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chamciaTanque1.LarguraCano = 13;
            this.chamciaTanque1.Location = new System.Drawing.Point(506, 57);
            this.chamciaTanque1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chamciaTanque1.MaxVidro = 127;
            this.chamciaTanque1.MinVidro = 8;
            this.chamciaTanque1.Name = "chamciaTanque1";
            this.chamciaTanque1.PosCano = 15;
            this.chamciaTanque1.RaioYElipse = 30;
            this.chamciaTanque1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chamciaTanque1.Size = new System.Drawing.Size(229, 161);
            this.chamciaTanque1.TabIndex = 0;
            this.chamciaTanque1.Value = 25D;
            // 
            // chamciaCanoBomba21
            // 
            this.chamciaCanoBomba21.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoBomba21.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoBomba21.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoBomba21.diminuirbolha = 5D;
            this.chamciaCanoBomba21.enableTransparentBackground = true;
            this.chamciaCanoBomba21.fliped = false;
            this.chamciaCanoBomba21.fluxo = 1;
            this.chamciaCanoBomba21.Location = new System.Drawing.Point(441, 174);
            this.chamciaCanoBomba21.MaxVidro = 127;
            this.chamciaCanoBomba21.MinVidro = 8;
            this.chamciaCanoBomba21.Name = "chamciaCanoBomba21";
            this.chamciaCanoBomba21.RaioYElipse = 30;
            this.chamciaCanoBomba21.Size = new System.Drawing.Size(85, 13);
            this.chamciaCanoBomba21.TabIndex = 179;
            this.chamciaCanoBomba21.vertical = false;
            // 
            // junção30
            // 
            this.junção30.Flip = 270;
            this.junção30.Location = new System.Drawing.Point(384, 319);
            this.junção30.Margin = new System.Windows.Forms.Padding(0);
            this.junção30.Name = "junção30";
            this.junção30.Size = new System.Drawing.Size(29, 32);
            this.junção30.TabIndex = 145;
            // 
            // junção15
            // 
            this.junção15.Flip = 90;
            this.junção15.Location = new System.Drawing.Point(292, 171);
            this.junção15.Margin = new System.Windows.Forms.Padding(0);
            this.junção15.Name = "junção15";
            this.junção15.Size = new System.Drawing.Size(30, 32);
            this.junção15.TabIndex = 139;
            // 
            // junção13
            // 
            this.junção13.Flip = 90;
            this.junção13.Location = new System.Drawing.Point(329, 219);
            this.junção13.Margin = new System.Windows.Forms.Padding(0);
            this.junção13.Name = "junção13";
            this.junção13.Size = new System.Drawing.Size(30, 32);
            this.junção13.TabIndex = 136;
            // 
            // junção9
            // 
            this.junção9.Flip = 0;
            this.junção9.Location = new System.Drawing.Point(292, 243);
            this.junção9.Margin = new System.Windows.Forms.Padding(0);
            this.junção9.Name = "junção9";
            this.junção9.Size = new System.Drawing.Size(31, 32);
            this.junção9.TabIndex = 125;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Gainsboro;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(3, 299);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(142, 83);
            this.label29.TabIndex = 120;
            this.label29.Text = "CENTRO - BELA SUIÇA 1 - FÁTIMA 1 E 2 - MONTE MURIÁ";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Gainsboro;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(311, 461);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(142, 81);
            this.label28.TabIndex = 119;
            this.label28.Text = "CENTRO - BRASILIA - MARIA EUGÊNIA - FÁTIMA - GOÁIS";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Gainsboro;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1107, 78);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 50);
            this.label17.TabIndex = 58;
            this.label17.Text = "BELA SUIÇA 2";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // junção8
            // 
            this.junção8.Flip = 180;
            this.junção8.Location = new System.Drawing.Point(1200, 94);
            this.junção8.Margin = new System.Windows.Forms.Padding(0);
            this.junção8.Name = "junção8";
            this.junção8.Size = new System.Drawing.Size(29, 32);
            this.junção8.TabIndex = 118;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(1222, 559);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(91, 16);
            this.label27.TabIndex = 117;
            this.label27.Text = "Poço Solt. 2";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(1125, 559);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 16);
            this.label26.TabIndex = 116;
            this.label26.Text = "Poço Solt. 1";
            // 
            // junçãoT6
            // 
            this.junçãoT6.Flip = 180;
            this.junçãoT6.Location = new System.Drawing.Point(1147, 447);
            this.junçãoT6.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT6.Name = "junçãoT6";
            this.junçãoT6.Size = new System.Drawing.Size(42, 44);
            this.junçãoT6.TabIndex = 112;
            // 
            // junção21
            // 
            this.junção21.Flip = 180;
            this.junção21.Location = new System.Drawing.Point(1238, 454);
            this.junção21.Margin = new System.Windows.Forms.Padding(0);
            this.junção21.Name = "junção21";
            this.junção21.Size = new System.Drawing.Size(46, 32);
            this.junção21.TabIndex = 62;
            // 
            // chamciaPocoSolt1
            // 
            this.chamciaPocoSolt1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPocoSolt1.LigarBomba = true;
            this.chamciaPocoSolt1.Location = new System.Drawing.Point(1130, 475);
            this.chamciaPocoSolt1.Name = "chamciaPocoSolt1";
            this.chamciaPocoSolt1.Size = new System.Drawing.Size(66, 78);
            this.chamciaPocoSolt1.TabIndex = 114;
            this.chamciaPocoSolt1.Transparence = false;
            // 
            // chamciaPocoSolt2
            // 
            this.chamciaPocoSolt2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPocoSolt2.LigarBomba = true;
            this.chamciaPocoSolt2.Location = new System.Drawing.Point(1229, 477);
            this.chamciaPocoSolt2.Name = "chamciaPocoSolt2";
            this.chamciaPocoSolt2.Size = new System.Drawing.Size(66, 78);
            this.chamciaPocoSolt2.TabIndex = 113;
            this.chamciaPocoSolt2.Transparence = false;
            // 
            // chamciaBomba1
            // 
            this.chamciaBomba1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaBomba1.LigarBomba = false;
            this.chamciaBomba1.Location = new System.Drawing.Point(321, 243);
            this.chamciaBomba1.Name = "chamciaBomba1";
            this.chamciaBomba1.Size = new System.Drawing.Size(60, 50);
            this.chamciaBomba1.TabIndex = 111;
            this.chamciaBomba1.Transparence = false;
            // 
            // junção3
            // 
            this.junção3.Flip = 90;
            this.junção3.Location = new System.Drawing.Point(34, 37);
            this.junção3.Margin = new System.Windows.Forms.Padding(0);
            this.junção3.Name = "junção3";
            this.junção3.Size = new System.Drawing.Size(30, 32);
            this.junção3.TabIndex = 109;
            // 
            // junçãoT15
            // 
            this.junçãoT15.Flip = 180;
            this.junçãoT15.Location = new System.Drawing.Point(447, 31);
            this.junçãoT15.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT15.Name = "junçãoT15";
            this.junçãoT15.Size = new System.Drawing.Size(42, 44);
            this.junçãoT15.TabIndex = 106;
            // 
            // chamciaPoco1
            // 
            this.chamciaPoco1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPoco1.LigarBomba = true;
            this.chamciaPoco1.Location = new System.Drawing.Point(430, 62);
            this.chamciaPoco1.Name = "chamciaPoco1";
            this.chamciaPoco1.Size = new System.Drawing.Size(66, 78);
            this.chamciaPoco1.TabIndex = 108;
            this.chamciaPoco1.Transparence = false;
            // 
            // chamciaCanoPoco2
            // 
            this.chamciaCanoPoco2.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoPoco2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoPoco2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoPoco2.diminuirbolha = 5D;
            this.chamciaCanoPoco2.enableTransparentBackground = true;
            this.chamciaCanoPoco2.fliped = false;
            this.chamciaCanoPoco2.fluxo = -1;
            this.chamciaCanoPoco2.Location = new System.Drawing.Point(413, 39);
            this.chamciaCanoPoco2.MaxVidro = 127;
            this.chamciaCanoPoco2.MinVidro = 8;
            this.chamciaCanoPoco2.Name = "chamciaCanoPoco2";
            this.chamciaCanoPoco2.RaioYElipse = 30;
            this.chamciaCanoPoco2.Size = new System.Drawing.Size(40, 13);
            this.chamciaCanoPoco2.TabIndex = 107;
            this.chamciaCanoPoco2.vertical = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(439, 143);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(62, 16);
            this.label25.TabIndex = 105;
            this.label25.Text = "POÇO 1";
            // 
            // junçãoT11
            // 
            this.junçãoT11.Flip = 180;
            this.junçãoT11.Location = new System.Drawing.Point(306, 31);
            this.junçãoT11.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT11.Name = "junçãoT11";
            this.junçãoT11.Size = new System.Drawing.Size(42, 44);
            this.junçãoT11.TabIndex = 94;
            // 
            // junçãoT12
            // 
            this.junçãoT12.Flip = 180;
            this.junçãoT12.Location = new System.Drawing.Point(234, 31);
            this.junçãoT12.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT12.Name = "junçãoT12";
            this.junçãoT12.Size = new System.Drawing.Size(42, 44);
            this.junçãoT12.TabIndex = 92;
            // 
            // junçãoT13
            // 
            this.junçãoT13.Flip = 180;
            this.junçãoT13.Location = new System.Drawing.Point(164, 31);
            this.junçãoT13.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT13.Name = "junçãoT13";
            this.junçãoT13.Size = new System.Drawing.Size(42, 44);
            this.junçãoT13.TabIndex = 90;
            // 
            // junçãoT14
            // 
            this.junçãoT14.Flip = 180;
            this.junçãoT14.Location = new System.Drawing.Point(94, 31);
            this.junçãoT14.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT14.Name = "junçãoT14";
            this.junçãoT14.Size = new System.Drawing.Size(42, 44);
            this.junçãoT14.TabIndex = 88;
            // 
            // chamciaCanoPoco6
            // 
            this.chamciaCanoPoco6.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoPoco6.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoPoco6.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoPoco6.diminuirbolha = 5D;
            this.chamciaCanoPoco6.enableTransparentBackground = true;
            this.chamciaCanoPoco6.fliped = false;
            this.chamciaCanoPoco6.fluxo = -1;
            this.chamciaCanoPoco6.Location = new System.Drawing.Point(127, 39);
            this.chamciaCanoPoco6.MaxVidro = 127;
            this.chamciaCanoPoco6.MinVidro = 8;
            this.chamciaCanoPoco6.Name = "chamciaCanoPoco6";
            this.chamciaCanoPoco6.RaioYElipse = 30;
            this.chamciaCanoPoco6.Size = new System.Drawing.Size(37, 13);
            this.chamciaCanoPoco6.TabIndex = 91;
            this.chamciaCanoPoco6.vertical = false;
            // 
            // chamciaPoco2
            // 
            this.chamciaPoco2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPoco2.LigarBomba = true;
            this.chamciaPoco2.Location = new System.Drawing.Point(358, 62);
            this.chamciaPoco2.Name = "chamciaPoco2";
            this.chamciaPoco2.Size = new System.Drawing.Size(66, 78);
            this.chamciaPoco2.TabIndex = 104;
            this.chamciaPoco2.Transparence = false;
            // 
            // chamciaPoco3
            // 
            this.chamciaPoco3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPoco3.LigarBomba = true;
            this.chamciaPoco3.Location = new System.Drawing.Point(288, 62);
            this.chamciaPoco3.Name = "chamciaPoco3";
            this.chamciaPoco3.Size = new System.Drawing.Size(66, 78);
            this.chamciaPoco3.TabIndex = 103;
            this.chamciaPoco3.Transparence = false;
            // 
            // chamciaPoco4
            // 
            this.chamciaPoco4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPoco4.LigarBomba = true;
            this.chamciaPoco4.Location = new System.Drawing.Point(216, 62);
            this.chamciaPoco4.Name = "chamciaPoco4";
            this.chamciaPoco4.Size = new System.Drawing.Size(66, 78);
            this.chamciaPoco4.TabIndex = 102;
            this.chamciaPoco4.Transparence = false;
            // 
            // chamciaPoco5
            // 
            this.chamciaPoco5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPoco5.LigarBomba = true;
            this.chamciaPoco5.Location = new System.Drawing.Point(146, 62);
            this.chamciaPoco5.Name = "chamciaPoco5";
            this.chamciaPoco5.Size = new System.Drawing.Size(66, 78);
            this.chamciaPoco5.TabIndex = 101;
            this.chamciaPoco5.Transparence = false;
            // 
            // chamciaCanoPoco1
            // 
            this.chamciaCanoPoco1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoPoco1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoPoco1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoPoco1.diminuirbolha = 5D;
            this.chamciaCanoPoco1.enableTransparentBackground = true;
            this.chamciaCanoPoco1.fliped = false;
            this.chamciaCanoPoco1.fluxo = -1;
            this.chamciaCanoPoco1.Location = new System.Drawing.Point(482, 39);
            this.chamciaCanoPoco1.MaxVidro = 127;
            this.chamciaCanoPoco1.MinVidro = 8;
            this.chamciaCanoPoco1.Name = "chamciaCanoPoco1";
            this.chamciaCanoPoco1.RaioYElipse = 30;
            this.chamciaCanoPoco1.Size = new System.Drawing.Size(30, 13);
            this.chamciaCanoPoco1.TabIndex = 98;
            this.chamciaCanoPoco1.vertical = false;
            // 
            // chamciaCanoPoco3
            // 
            this.chamciaCanoPoco3.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoPoco3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoPoco3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoPoco3.diminuirbolha = 5D;
            this.chamciaCanoPoco3.enableTransparentBackground = true;
            this.chamciaCanoPoco3.fliped = false;
            this.chamciaCanoPoco3.fluxo = -1;
            this.chamciaCanoPoco3.Location = new System.Drawing.Point(341, 39);
            this.chamciaCanoPoco3.MaxVidro = 127;
            this.chamciaCanoPoco3.MinVidro = 8;
            this.chamciaCanoPoco3.Name = "chamciaCanoPoco3";
            this.chamciaCanoPoco3.RaioYElipse = 30;
            this.chamciaCanoPoco3.Size = new System.Drawing.Size(40, 13);
            this.chamciaCanoPoco3.TabIndex = 97;
            this.chamciaCanoPoco3.vertical = false;
            // 
            // chamciaCanoPoco4
            // 
            this.chamciaCanoPoco4.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoPoco4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoPoco4.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoPoco4.diminuirbolha = 5D;
            this.chamciaCanoPoco4.enableTransparentBackground = true;
            this.chamciaCanoPoco4.fliped = false;
            this.chamciaCanoPoco4.fluxo = -1;
            this.chamciaCanoPoco4.Location = new System.Drawing.Point(270, 39);
            this.chamciaCanoPoco4.MaxVidro = 127;
            this.chamciaCanoPoco4.MinVidro = 8;
            this.chamciaCanoPoco4.Name = "chamciaCanoPoco4";
            this.chamciaCanoPoco4.RaioYElipse = 30;
            this.chamciaCanoPoco4.Size = new System.Drawing.Size(47, 13);
            this.chamciaCanoPoco4.TabIndex = 95;
            this.chamciaCanoPoco4.vertical = false;
            // 
            // chamciaCanoPoco5
            // 
            this.chamciaCanoPoco5.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoPoco5.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoPoco5.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoPoco5.diminuirbolha = 5D;
            this.chamciaCanoPoco5.enableTransparentBackground = true;
            this.chamciaCanoPoco5.fliped = false;
            this.chamciaCanoPoco5.fluxo = -1;
            this.chamciaCanoPoco5.Location = new System.Drawing.Point(196, 39);
            this.chamciaCanoPoco5.MaxVidro = 127;
            this.chamciaCanoPoco5.MinVidro = 8;
            this.chamciaCanoPoco5.Name = "chamciaCanoPoco5";
            this.chamciaCanoPoco5.RaioYElipse = 30;
            this.chamciaCanoPoco5.Size = new System.Drawing.Size(48, 13);
            this.chamciaCanoPoco5.TabIndex = 93;
            this.chamciaCanoPoco5.vertical = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(367, 143);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 16);
            this.label19.TabIndex = 86;
            this.label19.Text = "POÇO 2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(297, 143);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 16);
            this.label20.TabIndex = 85;
            this.label20.Text = "POÇO 3";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(225, 143);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 16);
            this.label21.TabIndex = 84;
            this.label21.Text = "POÇO 4";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(155, 143);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 16);
            this.label22.TabIndex = 83;
            this.label22.Text = "POÇO 5";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(85, 143);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(62, 16);
            this.label23.TabIndex = 82;
            this.label23.Text = "POÇO 6";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(13, 143);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(62, 16);
            this.label24.TabIndex = 81;
            this.label24.Text = "POÇO 7";
            // 
            // chamciaPoco7
            // 
            this.chamciaPoco7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPoco7.LigarBomba = true;
            this.chamciaPoco7.Location = new System.Drawing.Point(4, 62);
            this.chamciaPoco7.Name = "chamciaPoco7";
            this.chamciaPoco7.Size = new System.Drawing.Size(66, 78);
            this.chamciaPoco7.TabIndex = 99;
            this.chamciaPoco7.Transparence = false;
            // 
            // chamciaPoco6
            // 
            this.chamciaPoco6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chamciaPoco6.LigarBomba = true;
            this.chamciaPoco6.Location = new System.Drawing.Point(77, 62);
            this.chamciaPoco6.Name = "chamciaPoco6";
            this.chamciaPoco6.Size = new System.Drawing.Size(66, 78);
            this.chamciaPoco6.TabIndex = 100;
            this.chamciaPoco6.Transparence = false;
            // 
            // chamciaCanoPoco7
            // 
            this.chamciaCanoPoco7.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoPoco7.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoPoco7.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoPoco7.diminuirbolha = 5D;
            this.chamciaCanoPoco7.enableTransparentBackground = true;
            this.chamciaCanoPoco7.fliped = false;
            this.chamciaCanoPoco7.fluxo = -1;
            this.chamciaCanoPoco7.Location = new System.Drawing.Point(61, 39);
            this.chamciaCanoPoco7.MaxVidro = 127;
            this.chamciaCanoPoco7.MinVidro = 8;
            this.chamciaCanoPoco7.Name = "chamciaCanoPoco7";
            this.chamciaCanoPoco7.RaioYElipse = 30;
            this.chamciaCanoPoco7.Size = new System.Drawing.Size(43, 13);
            this.chamciaCanoPoco7.TabIndex = 89;
            this.chamciaCanoPoco7.vertical = false;
            // 
            // junção22
            // 
            this.junção22.Flip = 0;
            this.junção22.Location = new System.Drawing.Point(975, 440);
            this.junção22.Margin = new System.Windows.Forms.Padding(0);
            this.junção22.Name = "junção22";
            this.junção22.Size = new System.Drawing.Size(47, 32);
            this.junção22.TabIndex = 64;
            // 
            // junção27
            // 
            this.junção27.Flip = 270;
            this.junção27.Location = new System.Drawing.Point(1268, 385);
            this.junção27.Margin = new System.Windows.Forms.Padding(0);
            this.junção27.Name = "junção27";
            this.junção27.Size = new System.Drawing.Size(29, 32);
            this.junção27.TabIndex = 78;
            // 
            // junção24
            // 
            this.junção24.Flip = 90;
            this.junção24.Location = new System.Drawing.Point(983, 178);
            this.junção24.Margin = new System.Windows.Forms.Padding(0);
            this.junção24.Name = "junção24";
            this.junção24.Size = new System.Drawing.Size(30, 32);
            this.junção24.TabIndex = 66;
            // 
            // junção23
            // 
            this.junção23.Flip = 180;
            this.junção23.Location = new System.Drawing.Point(1030, 177);
            this.junção23.Margin = new System.Windows.Forms.Padding(0);
            this.junção23.Name = "junção23";
            this.junção23.Size = new System.Drawing.Size(29, 32);
            this.junção23.TabIndex = 65;
            // 
            // chamciaCano3PocoSolt1
            // 
            this.chamciaCano3PocoSolt1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano3PocoSolt1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano3PocoSolt1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano3PocoSolt1.diminuirbolha = 5D;
            this.chamciaCano3PocoSolt1.enableTransparentBackground = true;
            this.chamciaCano3PocoSolt1.fliped = false;
            this.chamciaCano3PocoSolt1.fluxo = 1;
            this.chamciaCano3PocoSolt1.Location = new System.Drawing.Point(1010, 456);
            this.chamciaCano3PocoSolt1.MaxVidro = 127;
            this.chamciaCano3PocoSolt1.MinVidro = 8;
            this.chamciaCano3PocoSolt1.Name = "chamciaCano3PocoSolt1";
            this.chamciaCano3PocoSolt1.RaioYElipse = 30;
            this.chamciaCano3PocoSolt1.Size = new System.Drawing.Size(149, 13);
            this.chamciaCano3PocoSolt1.TabIndex = 63;
            this.chamciaCano3PocoSolt1.vertical = false;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Gainsboro;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(1201, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 58);
            this.label18.TabIndex = 59;
            this.label18.Text = "BELA SUIÇA 3";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // chamciaTanque4
            // 
            this.chamciaTanque4.BackColor = System.Drawing.Color.Transparent;
            this.chamciaTanque4.Cano = 2;
            this.chamciaTanque4.CanoCheio = true;
            this.chamciaTanque4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaTanque4.CorVidro = System.Drawing.Color.Silver;
            this.chamciaTanque4.enableTransparentBackground = true;
            this.chamciaTanque4.LarguraCano = 13;
            this.chamciaTanque4.Location = new System.Drawing.Point(1029, 206);
            this.chamciaTanque4.MaxVidro = 127;
            this.chamciaTanque4.MinVidro = 8;
            this.chamciaTanque4.Name = "chamciaTanque4";
            this.chamciaTanque4.PosCano = 15;
            this.chamciaTanque4.RaioYElipse = 30;
            this.chamciaTanque4.Size = new System.Drawing.Size(174, 237);
            this.chamciaTanque4.TabIndex = 3;
            this.chamciaTanque4.Value = 75D;
            // 
            // chamciaTanque3
            // 
            this.chamciaTanque3.BackColor = System.Drawing.Color.Transparent;
            this.chamciaTanque3.Cano = 0;
            this.chamciaTanque3.CanoCheio = false;
            this.chamciaTanque3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaTanque3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaTanque3.enableTransparentBackground = true;
            this.chamciaTanque3.LarguraCano = 30;
            this.chamciaTanque3.Location = new System.Drawing.Point(793, 249);
            this.chamciaTanque3.MaxVidro = 127;
            this.chamciaTanque3.MinVidro = 8;
            this.chamciaTanque3.Name = "chamciaTanque3";
            this.chamciaTanque3.PosCano = 15;
            this.chamciaTanque3.RaioYElipse = 30;
            this.chamciaTanque3.Size = new System.Drawing.Size(173, 139);
            this.chamciaTanque3.TabIndex = 2;
            this.chamciaTanque3.Value = 40D;
            // 
            // chamciaTanque2
            // 
            this.chamciaTanque2.BackColor = System.Drawing.Color.Transparent;
            this.chamciaTanque2.Cano = 0;
            this.chamciaTanque2.CanoCheio = false;
            this.chamciaTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaTanque2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaTanque2.enableTransparentBackground = true;
            this.chamciaTanque2.LarguraCano = 30;
            this.chamciaTanque2.Location = new System.Drawing.Point(770, 57);
            this.chamciaTanque2.MaxVidro = 127;
            this.chamciaTanque2.MinVidro = 8;
            this.chamciaTanque2.Name = "chamciaTanque2";
            this.chamciaTanque2.PosCano = 15;
            this.chamciaTanque2.RaioYElipse = 30;
            this.chamciaTanque2.Size = new System.Drawing.Size(205, 161);
            this.chamciaTanque2.TabIndex = 1;
            this.chamciaTanque2.Value = 40D;
            // 
            // chamciaCano1PocoSolt1
            // 
            this.chamciaCano1PocoSolt1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano1PocoSolt1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano1PocoSolt1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano1PocoSolt1.diminuirbolha = 5D;
            this.chamciaCano1PocoSolt1.enableTransparentBackground = true;
            this.chamciaCano1PocoSolt1.fliped = false;
            this.chamciaCano1PocoSolt1.fluxo = -1;
            this.chamciaCano1PocoSolt1.Location = new System.Drawing.Point(1010, 181);
            this.chamciaCano1PocoSolt1.MaxVidro = 127;
            this.chamciaCano1PocoSolt1.MinVidro = 8;
            this.chamciaCano1PocoSolt1.Name = "chamciaCano1PocoSolt1";
            this.chamciaCano1PocoSolt1.RaioYElipse = 30;
            this.chamciaCano1PocoSolt1.Size = new System.Drawing.Size(27, 13);
            this.chamciaCano1PocoSolt1.TabIndex = 73;
            this.chamciaCano1PocoSolt1.vertical = false;
            // 
            // chamciaCano2PocoSolt1
            // 
            this.chamciaCano2PocoSolt1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano2PocoSolt1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano2PocoSolt1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano2PocoSolt1.diminuirbolha = 5D;
            this.chamciaCano2PocoSolt1.enableTransparentBackground = true;
            this.chamciaCano2PocoSolt1.fliped = false;
            this.chamciaCano2PocoSolt1.fluxo = 1;
            this.chamciaCano2PocoSolt1.Location = new System.Drawing.Point(985, 207);
            this.chamciaCano2PocoSolt1.MaxVidro = 127;
            this.chamciaCano2PocoSolt1.MinVidro = 8;
            this.chamciaCano2PocoSolt1.Name = "chamciaCano2PocoSolt1";
            this.chamciaCano2PocoSolt1.RaioYElipse = 30;
            this.chamciaCano2PocoSolt1.Size = new System.Drawing.Size(13, 237);
            this.chamciaCano2PocoSolt1.TabIndex = 75;
            this.chamciaCano2PocoSolt1.vertical = true;
            // 
            // junçãoT7
            // 
            this.junçãoT7.Flip = 0;
            this.junçãoT7.Location = new System.Drawing.Point(1200, 380);
            this.junçãoT7.Margin = new System.Windows.Forms.Padding(0);
            this.junçãoT7.Name = "junçãoT7";
            this.junçãoT7.Size = new System.Drawing.Size(42, 44);
            this.junçãoT7.TabIndex = 76;
            // 
            // chamciaCano1Suica3
            // 
            this.chamciaCano1Suica3.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano1Suica3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano1Suica3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano1Suica3.diminuirbolha = 5D;
            this.chamciaCano1Suica3.enableTransparentBackground = true;
            this.chamciaCano1Suica3.fliped = false;
            this.chamciaCano1Suica3.fluxo = -1;
            this.chamciaCano1Suica3.Location = new System.Drawing.Point(1234, 402);
            this.chamciaCano1Suica3.MaxVidro = 127;
            this.chamciaCano1Suica3.MinVidro = 8;
            this.chamciaCano1Suica3.Name = "chamciaCano1Suica3";
            this.chamciaCano1Suica3.RaioYElipse = 30;
            this.chamciaCano1Suica3.Size = new System.Drawing.Size(41, 13);
            this.chamciaCano1Suica3.TabIndex = 77;
            this.chamciaCano1Suica3.vertical = false;
            // 
            // chamciaCano2Suica3
            // 
            this.chamciaCano2Suica3.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano2Suica3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano2Suica3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano2Suica3.diminuirbolha = 5D;
            this.chamciaCano2Suica3.enableTransparentBackground = true;
            this.chamciaCano2Suica3.fliped = false;
            this.chamciaCano2Suica3.fluxo = 1;
            this.chamciaCano2Suica3.Location = new System.Drawing.Point(1281, 62);
            this.chamciaCano2Suica3.MaxVidro = 127;
            this.chamciaCano2Suica3.MinVidro = 8;
            this.chamciaCano2Suica3.Name = "chamciaCano2Suica3";
            this.chamciaCano2Suica3.RaioYElipse = 30;
            this.chamciaCano2Suica3.Size = new System.Drawing.Size(13, 328);
            this.chamciaCano2Suica3.TabIndex = 79;
            this.chamciaCano2Suica3.vertical = true;
            // 
            // chamciaCanoSuica2
            // 
            this.chamciaCanoSuica2.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoSuica2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoSuica2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoSuica2.diminuirbolha = 5D;
            this.chamciaCanoSuica2.enableTransparentBackground = true;
            this.chamciaCanoSuica2.fliped = false;
            this.chamciaCanoSuica2.fluxo = 1;
            this.chamciaCanoSuica2.Location = new System.Drawing.Point(1214, 117);
            this.chamciaCanoSuica2.MaxVidro = 127;
            this.chamciaCanoSuica2.MinVidro = 8;
            this.chamciaCanoSuica2.Name = "chamciaCanoSuica2";
            this.chamciaCanoSuica2.RaioYElipse = 30;
            this.chamciaCanoSuica2.Size = new System.Drawing.Size(13, 278);
            this.chamciaCanoSuica2.TabIndex = 80;
            this.chamciaCanoSuica2.vertical = true;
            // 
            // chamciaCanoPocoSolt2
            // 
            this.chamciaCanoPocoSolt2.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoPocoSolt2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoPocoSolt2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoPocoSolt2.diminuirbolha = 5D;
            this.chamciaCanoPocoSolt2.enableTransparentBackground = true;
            this.chamciaCanoPocoSolt2.fliped = false;
            this.chamciaCanoPocoSolt2.fluxo = 1;
            this.chamciaCanoPocoSolt2.Location = new System.Drawing.Point(1178, 456);
            this.chamciaCanoPocoSolt2.MaxVidro = 127;
            this.chamciaCanoPocoSolt2.MinVidro = 8;
            this.chamciaCanoPocoSolt2.Name = "chamciaCanoPocoSolt2";
            this.chamciaCanoPocoSolt2.RaioYElipse = 30;
            this.chamciaCanoPocoSolt2.Size = new System.Drawing.Size(72, 13);
            this.chamciaCanoPocoSolt2.TabIndex = 115;
            this.chamciaCanoPocoSolt2.vertical = false;
            // 
            // chamciaCanoBomba1
            // 
            this.chamciaCanoBomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoBomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoBomba1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoBomba1.diminuirbolha = 5D;
            this.chamciaCanoBomba1.enableTransparentBackground = true;
            this.chamciaCanoBomba1.fliped = false;
            this.chamciaCanoBomba1.fluxo = -1;
            this.chamciaCanoBomba1.Location = new System.Drawing.Point(294, 193);
            this.chamciaCanoBomba1.MaxVidro = 127;
            this.chamciaCanoBomba1.MinVidro = 8;
            this.chamciaCanoBomba1.Name = "chamciaCanoBomba1";
            this.chamciaCanoBomba1.RaioYElipse = 30;
            this.chamciaCanoBomba1.Size = new System.Drawing.Size(13, 58);
            this.chamciaCanoBomba1.TabIndex = 135;
            this.chamciaCanoBomba1.vertical = true;
            // 
            // chamciaCano12Bomba1
            // 
            this.chamciaCano12Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano12Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano12Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano12Bomba1.diminuirbolha = 5D;
            this.chamciaCano12Bomba1.enableTransparentBackground = true;
            this.chamciaCano12Bomba1.fliped = false;
            this.chamciaCano12Bomba1.fluxo = 1;
            this.chamciaCano12Bomba1.Location = new System.Drawing.Point(306, 174);
            this.chamciaCano12Bomba1.MaxVidro = 127;
            this.chamciaCano12Bomba1.MinVidro = 8;
            this.chamciaCano12Bomba1.Name = "chamciaCano12Bomba1";
            this.chamciaCano12Bomba1.RaioYElipse = 30;
            this.chamciaCano12Bomba1.Size = new System.Drawing.Size(111, 13);
            this.chamciaCano12Bomba1.TabIndex = 138;
            this.chamciaCano12Bomba1.vertical = false;
            // 
            // chamciaCano2Bomba1
            // 
            this.chamciaCano2Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano2Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano2Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano2Bomba1.diminuirbolha = 5D;
            this.chamciaCano2Bomba1.enableTransparentBackground = true;
            this.chamciaCano2Bomba1.fliped = false;
            this.chamciaCano2Bomba1.fluxo = 0;
            this.chamciaCano2Bomba1.Location = new System.Drawing.Point(398, 243);
            this.chamciaCano2Bomba1.MaxVidro = 127;
            this.chamciaCano2Bomba1.MinVidro = 8;
            this.chamciaCano2Bomba1.Name = "chamciaCano2Bomba1";
            this.chamciaCano2Bomba1.RaioYElipse = 30;
            this.chamciaCano2Bomba1.Size = new System.Drawing.Size(13, 85);
            this.chamciaCano2Bomba1.TabIndex = 142;
            this.chamciaCano2Bomba1.vertical = true;
            // 
            // chamciaCanoBomba2
            // 
            this.chamciaCanoBomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoBomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoBomba2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoBomba2.diminuirbolha = 5D;
            this.chamciaCanoBomba2.enableTransparentBackground = true;
            this.chamciaCanoBomba2.fliped = false;
            this.chamciaCanoBomba2.fluxo = -1;
            this.chamciaCanoBomba2.Location = new System.Drawing.Point(426, 193);
            this.chamciaCanoBomba2.MaxVidro = 127;
            this.chamciaCanoBomba2.MinVidro = 8;
            this.chamciaCanoBomba2.Name = "chamciaCanoBomba2";
            this.chamciaCanoBomba2.RaioYElipse = 30;
            this.chamciaCanoBomba2.Size = new System.Drawing.Size(13, 81);
            this.chamciaCanoBomba2.TabIndex = 154;
            this.chamciaCanoBomba2.vertical = true;
            // 
            // chamciaCano3Bomba3
            // 
            this.chamciaCano3Bomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano3Bomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano3Bomba3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano3Bomba3.diminuirbolha = 5D;
            this.chamciaCano3Bomba3.enableTransparentBackground = true;
            this.chamciaCano3Bomba3.fliped = false;
            this.chamciaCano3Bomba3.fluxo = 0;
            this.chamciaCano3Bomba3.Location = new System.Drawing.Point(430, 495);
            this.chamciaCano3Bomba3.MaxVidro = 127;
            this.chamciaCano3Bomba3.MinVidro = 8;
            this.chamciaCano3Bomba3.Name = "chamciaCano3Bomba3";
            this.chamciaCano3Bomba3.RaioYElipse = 30;
            this.chamciaCano3Bomba3.Size = new System.Drawing.Size(262, 13);
            this.chamciaCano3Bomba3.TabIndex = 168;
            this.chamciaCano3Bomba3.vertical = false;
            // 
            // chamciaCanoBomba3
            // 
            this.chamciaCanoBomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoBomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoBomba3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoBomba3.diminuirbolha = 5D;
            this.chamciaCanoBomba3.enableTransparentBackground = true;
            this.chamciaCanoBomba3.fliped = false;
            this.chamciaCanoBomba3.fluxo = -1;
            this.chamciaCanoBomba3.Location = new System.Drawing.Point(589, 206);
            this.chamciaCanoBomba3.MaxVidro = 127;
            this.chamciaCanoBomba3.MinVidro = 8;
            this.chamciaCanoBomba3.Name = "chamciaCanoBomba3";
            this.chamciaCanoBomba3.RaioYElipse = 30;
            this.chamciaCanoBomba3.Size = new System.Drawing.Size(13, 197);
            this.chamciaCanoBomba3.TabIndex = 162;
            this.chamciaCanoBomba3.vertical = true;
            // 
            // chamciaCanoBomba4
            // 
            this.chamciaCanoBomba4.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoBomba4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoBomba4.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoBomba4.diminuirbolha = 5D;
            this.chamciaCanoBomba4.enableTransparentBackground = true;
            this.chamciaCanoBomba4.fliped = false;
            this.chamciaCanoBomba4.fluxo = -1;
            this.chamciaCanoBomba4.Location = new System.Drawing.Point(612, 209);
            this.chamciaCanoBomba4.MaxVidro = 127;
            this.chamciaCanoBomba4.MinVidro = 8;
            this.chamciaCanoBomba4.Name = "chamciaCanoBomba4";
            this.chamciaCanoBomba4.RaioYElipse = 30;
            this.chamciaCanoBomba4.Size = new System.Drawing.Size(13, 98);
            this.chamciaCanoBomba4.TabIndex = 171;
            this.chamciaCanoBomba4.vertical = true;
            // 
            // chamciaCano1Bomba4
            // 
            this.chamciaCano1Bomba4.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano1Bomba4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano1Bomba4.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano1Bomba4.diminuirbolha = 5D;
            this.chamciaCano1Bomba4.enableTransparentBackground = true;
            this.chamciaCano1Bomba4.fliped = false;
            this.chamciaCano1Bomba4.fluxo = 0;
            this.chamciaCano1Bomba4.Location = new System.Drawing.Point(675, 279);
            this.chamciaCano1Bomba4.MaxVidro = 127;
            this.chamciaCano1Bomba4.MinVidro = 8;
            this.chamciaCano1Bomba4.Name = "chamciaCano1Bomba4";
            this.chamciaCano1Bomba4.RaioYElipse = 30;
            this.chamciaCano1Bomba4.Size = new System.Drawing.Size(45, 13);
            this.chamciaCano1Bomba4.TabIndex = 175;
            this.chamciaCano1Bomba4.vertical = false;
            // 
            // chamciaCanoTanque2
            // 
            this.chamciaCanoTanque2.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoTanque2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoTanque2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoTanque2.diminuirbolha = 5D;
            this.chamciaCanoTanque2.enableTransparentBackground = true;
            this.chamciaCanoTanque2.fliped = false;
            this.chamciaCanoTanque2.fluxo = -1;
            this.chamciaCanoTanque2.Location = new System.Drawing.Point(723, 177);
            this.chamciaCanoTanque2.MaxVidro = 127;
            this.chamciaCanoTanque2.MinVidro = 8;
            this.chamciaCanoTanque2.Name = "chamciaCanoTanque2";
            this.chamciaCanoTanque2.RaioYElipse = 30;
            this.chamciaCanoTanque2.Size = new System.Drawing.Size(59, 15);
            this.chamciaCanoTanque2.TabIndex = 176;
            this.chamciaCanoTanque2.vertical = false;
            // 
            // chamciaCanoTanque3
            // 
            this.chamciaCanoTanque3.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCanoTanque3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCanoTanque3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCanoTanque3.diminuirbolha = 5D;
            this.chamciaCanoTanque3.enableTransparentBackground = true;
            this.chamciaCanoTanque3.fliped = false;
            this.chamciaCanoTanque3.fluxo = -1;
            this.chamciaCanoTanque3.Location = new System.Drawing.Point(870, 209);
            this.chamciaCanoTanque3.MaxVidro = 127;
            this.chamciaCanoTanque3.MinVidro = 8;
            this.chamciaCanoTanque3.Name = "chamciaCanoTanque3";
            this.chamciaCanoTanque3.RaioYElipse = 30;
            this.chamciaCanoTanque3.Size = new System.Drawing.Size(15, 50);
            this.chamciaCanoTanque3.TabIndex = 177;
            this.chamciaCanoTanque3.vertical = true;
            // 
            // chamciaCano1Bomba3
            // 
            this.chamciaCano1Bomba3.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano1Bomba3.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano1Bomba3.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano1Bomba3.diminuirbolha = 5D;
            this.chamciaCano1Bomba3.enableTransparentBackground = true;
            this.chamciaCano1Bomba3.fliped = false;
            this.chamciaCano1Bomba3.fluxo = 0;
            this.chamciaCano1Bomba3.Location = new System.Drawing.Point(649, 375);
            this.chamciaCano1Bomba3.MaxVidro = 127;
            this.chamciaCano1Bomba3.MinVidro = 8;
            this.chamciaCano1Bomba3.Name = "chamciaCano1Bomba3";
            this.chamciaCano1Bomba3.RaioYElipse = 30;
            this.chamciaCano1Bomba3.Size = new System.Drawing.Size(40, 13);
            this.chamciaCano1Bomba3.TabIndex = 166;
            this.chamciaCano1Bomba3.vertical = false;
            // 
            // chamciaCano3Bomba1
            // 
            this.chamciaCano3Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano3Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano3Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano3Bomba1.diminuirbolha = 5D;
            this.chamciaCano3Bomba1.enableTransparentBackground = true;
            this.chamciaCano3Bomba1.fliped = false;
            this.chamciaCano3Bomba1.fluxo = 0;
            this.chamciaCano3Bomba1.Location = new System.Drawing.Point(127, 335);
            this.chamciaCano3Bomba1.MaxVidro = 127;
            this.chamciaCano3Bomba1.MinVidro = 8;
            this.chamciaCano3Bomba1.Name = "chamciaCano3Bomba1";
            this.chamciaCano3Bomba1.RaioYElipse = 30;
            this.chamciaCano3Bomba1.Size = new System.Drawing.Size(267, 13);
            this.chamciaCano3Bomba1.TabIndex = 144;
            this.chamciaCano3Bomba1.vertical = false;
            // 
            // chamciaCano1Bomba2
            // 
            this.chamciaCano1Bomba2.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano1Bomba2.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano1Bomba2.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano1Bomba2.diminuirbolha = 5D;
            this.chamciaCano1Bomba2.enableTransparentBackground = true;
            this.chamciaCano1Bomba2.fliped = false;
            this.chamciaCano1Bomba2.fluxo = 0;
            this.chamciaCano1Bomba2.Location = new System.Drawing.Point(482, 246);
            this.chamciaCano1Bomba2.MaxVidro = 127;
            this.chamciaCano1Bomba2.MinVidro = 8;
            this.chamciaCano1Bomba2.Name = "chamciaCano1Bomba2";
            this.chamciaCano1Bomba2.RaioYElipse = 30;
            this.chamciaCano1Bomba2.Size = new System.Drawing.Size(37, 13);
            this.chamciaCano1Bomba2.TabIndex = 159;
            this.chamciaCano1Bomba2.vertical = false;
            // 
            // chamciaCano1Bomba1
            // 
            this.chamciaCano1Bomba1.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano1Bomba1.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano1Bomba1.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano1Bomba1.diminuirbolha = 5D;
            this.chamciaCano1Bomba1.enableTransparentBackground = true;
            this.chamciaCano1Bomba1.fliped = false;
            this.chamciaCano1Bomba1.fluxo = 0;
            this.chamciaCano1Bomba1.Location = new System.Drawing.Point(358, 223);
            this.chamciaCano1Bomba1.MaxVidro = 127;
            this.chamciaCano1Bomba1.MinVidro = 8;
            this.chamciaCano1Bomba1.Name = "chamciaCano1Bomba1";
            this.chamciaCano1Bomba1.RaioYElipse = 30;
            this.chamciaCano1Bomba1.Size = new System.Drawing.Size(29, 13);
            this.chamciaCano1Bomba1.TabIndex = 146;
            this.chamciaCano1Bomba1.vertical = false;
            // 
            // chamciaCano3Bomba4
            // 
            this.chamciaCano3Bomba4.BackColor = System.Drawing.Color.Gainsboro;
            this.chamciaCano3Bomba4.CorFluido = System.Drawing.Color.DodgerBlue;
            this.chamciaCano3Bomba4.CorVidro = System.Drawing.Color.Silver;
            this.chamciaCano3Bomba4.diminuirbolha = 5D;
            this.chamciaCano3Bomba4.enableTransparentBackground = true;
            this.chamciaCano3Bomba4.fliped = false;
            this.chamciaCano3Bomba4.fluxo = 0;
            this.chamciaCano3Bomba4.Location = new System.Drawing.Point(749, 511);
            this.chamciaCano3Bomba4.MaxVidro = 127;
            this.chamciaCano3Bomba4.MinVidro = 8;
            this.chamciaCano3Bomba4.Name = "chamciaCano3Bomba4";
            this.chamciaCano3Bomba4.RaioYElipse = 30;
            this.chamciaCano3Bomba4.Size = new System.Drawing.Size(236, 13);
            this.chamciaCano3Bomba4.TabIndex = 182;
            this.chamciaCano3Bomba4.vertical = false;
            // 
            // Acionamentos
            // 
            this.Acionamentos.Controls.Add(this.pbAcionamentos);
            this.Acionamentos.Location = new System.Drawing.Point(4, 22);
            this.Acionamentos.Name = "Acionamentos";
            this.Acionamentos.Size = new System.Drawing.Size(1480, 662);
            this.Acionamentos.TabIndex = 13;
            this.Acionamentos.Text = "ACIONAMENTOS";
            this.Acionamentos.UseVisualStyleBackColor = true;
            // 
            // pbAcionamentos
            // 
            this.pbAcionamentos.BackColor = System.Drawing.Color.White;
            this.pbAcionamentos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAcionamentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbAcionamentos.ImageLocation = "";
            this.pbAcionamentos.Location = new System.Drawing.Point(0, 0);
            this.pbAcionamentos.Name = "pbAcionamentos";
            this.pbAcionamentos.Size = new System.Drawing.Size(1480, 662);
            this.pbAcionamentos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAcionamentos.TabIndex = 3;
            this.pbAcionamentos.TabStop = false;
            // 
            // Reservatórios
            // 
            this.Reservatórios.Controls.Add(this.panelNivel);
            this.Reservatórios.Location = new System.Drawing.Point(4, 22);
            this.Reservatórios.Name = "Reservatórios";
            this.Reservatórios.Size = new System.Drawing.Size(1480, 662);
            this.Reservatórios.TabIndex = 11;
            this.Reservatórios.Text = "RESERVATÓRIOS";
            this.Reservatórios.UseVisualStyleBackColor = true;
            // 
            // panelNivel
            // 
            this.panelNivel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelNivel.Controls.Add(this.RouroVerdeN1);
            this.panelNivel.Controls.Add(this.RIndependenciaN1);
            this.panelNivel.Controls.Add(this.RIndependenciaN2);
            this.panelNivel.Controls.Add(this.REstadualN1);
            this.panelNivel.Controls.Add(this.REstadualN2);
            this.panelNivel.Controls.Add(this.RGuitierrezN1);
            this.panelNivel.Controls.Add(this.RJBN1);
            this.panelNivel.Controls.Add(this.RFatimaN1);
            this.panelNivel.Controls.Add(this.RSSebastiaoN1);
            this.panelNivel.Controls.Add(this.RouroVN2);
            this.panelNivel.Controls.Add(this.RsBeneditoN1);
            this.panelNivel.Controls.Add(this.RsBeneditoN2);
            this.panelNivel.Controls.Add(this.RChanciaN2);
            this.panelNivel.Controls.Add(this.RChanciaNivel1);
            this.panelNivel.Controls.Add(this.label116);
            this.panelNivel.Controls.Add(this.label115);
            this.panelNivel.Controls.Add(this.label114);
            this.panelNivel.Controls.Add(this.label113);
            this.panelNivel.Controls.Add(this.lblIndepN2);
            this.panelNivel.Controls.Add(this.label111);
            this.panelNivel.Controls.Add(this.label110);
            this.panelNivel.Controls.Add(this.lblSsebastiao);
            this.panelNivel.Controls.Add(this.lblOuroVN2);
            this.panelNivel.Controls.Add(this.lblOuroVN1);
            this.panelNivel.Controls.Add(this.lblSBN2);
            this.panelNivel.Controls.Add(this.lblSBeneditoN1);
            this.panelNivel.Controls.Add(this.lblCN2);
            this.panelNivel.Controls.Add(this.lblChamciaNivel1);
            this.panelNivel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelNivel.Location = new System.Drawing.Point(0, 0);
            this.panelNivel.Name = "panelNivel";
            this.panelNivel.Size = new System.Drawing.Size(1480, 662);
            this.panelNivel.TabIndex = 0;
            // 
            // RouroVerdeN1
            // 
            this.RouroVerdeN1.BackColor = System.Drawing.Color.Transparent;
            this.RouroVerdeN1.enableTransparentBackground = true;
            this.RouroVerdeN1.largEscala = 50;
            this.RouroVerdeN1.Location = new System.Drawing.Point(733, 50);
            this.RouroVerdeN1.Name = "RouroVerdeN1";
            this.RouroVerdeN1.Size = new System.Drawing.Size(176, 183);
            this.RouroVerdeN1.TabIndex = 79;
            this.RouroVerdeN1.Transparency = false;
            // 
            // RIndependenciaN1
            // 
            this.RIndependenciaN1.BackColor = System.Drawing.Color.Transparent;
            this.RIndependenciaN1.enableTransparentBackground = true;
            this.RIndependenciaN1.largEscala = 50;
            this.RIndependenciaN1.Location = new System.Drawing.Point(11, 338);
            this.RIndependenciaN1.Name = "RIndependenciaN1";
            this.RIndependenciaN1.Size = new System.Drawing.Size(176, 183);
            this.RIndependenciaN1.TabIndex = 78;
            this.RIndependenciaN1.Transparency = false;
            // 
            // RIndependenciaN2
            // 
            this.RIndependenciaN2.BackColor = System.Drawing.Color.Transparent;
            this.RIndependenciaN2.enableTransparentBackground = true;
            this.RIndependenciaN2.largEscala = 50;
            this.RIndependenciaN2.Location = new System.Drawing.Point(187, 338);
            this.RIndependenciaN2.Name = "RIndependenciaN2";
            this.RIndependenciaN2.Size = new System.Drawing.Size(176, 183);
            this.RIndependenciaN2.TabIndex = 77;
            this.RIndependenciaN2.Transparency = false;
            // 
            // REstadualN1
            // 
            this.REstadualN1.BackColor = System.Drawing.Color.Transparent;
            this.REstadualN1.enableTransparentBackground = true;
            this.REstadualN1.largEscala = 50;
            this.REstadualN1.Location = new System.Drawing.Point(369, 338);
            this.REstadualN1.Name = "REstadualN1";
            this.REstadualN1.Size = new System.Drawing.Size(176, 183);
            this.REstadualN1.TabIndex = 76;
            this.REstadualN1.Transparency = false;
            // 
            // REstadualN2
            // 
            this.REstadualN2.BackColor = System.Drawing.Color.Transparent;
            this.REstadualN2.enableTransparentBackground = true;
            this.REstadualN2.largEscala = 50;
            this.REstadualN2.Location = new System.Drawing.Point(551, 338);
            this.REstadualN2.Name = "REstadualN2";
            this.REstadualN2.Size = new System.Drawing.Size(176, 183);
            this.REstadualN2.TabIndex = 75;
            this.REstadualN2.Transparency = false;
            // 
            // RGuitierrezN1
            // 
            this.RGuitierrezN1.BackColor = System.Drawing.Color.Transparent;
            this.RGuitierrezN1.enableTransparentBackground = true;
            this.RGuitierrezN1.largEscala = 50;
            this.RGuitierrezN1.Location = new System.Drawing.Point(733, 338);
            this.RGuitierrezN1.Name = "RGuitierrezN1";
            this.RGuitierrezN1.Size = new System.Drawing.Size(176, 183);
            this.RGuitierrezN1.TabIndex = 74;
            this.RGuitierrezN1.Transparency = false;
            // 
            // RJBN1
            // 
            this.RJBN1.BackColor = System.Drawing.Color.Transparent;
            this.RJBN1.enableTransparentBackground = true;
            this.RJBN1.largEscala = 50;
            this.RJBN1.Location = new System.Drawing.Point(915, 338);
            this.RJBN1.Name = "RJBN1";
            this.RJBN1.Size = new System.Drawing.Size(176, 183);
            this.RJBN1.TabIndex = 73;
            this.RJBN1.Transparency = false;
            // 
            // RFatimaN1
            // 
            this.RFatimaN1.BackColor = System.Drawing.Color.Transparent;
            this.RFatimaN1.enableTransparentBackground = true;
            this.RFatimaN1.largEscala = 50;
            this.RFatimaN1.Location = new System.Drawing.Point(1097, 338);
            this.RFatimaN1.Name = "RFatimaN1";
            this.RFatimaN1.Size = new System.Drawing.Size(176, 183);
            this.RFatimaN1.TabIndex = 72;
            this.RFatimaN1.Transparency = false;
            // 
            // RSSebastiaoN1
            // 
            this.RSSebastiaoN1.BackColor = System.Drawing.Color.Transparent;
            this.RSSebastiaoN1.enableTransparentBackground = true;
            this.RSSebastiaoN1.largEscala = 50;
            this.RSSebastiaoN1.Location = new System.Drawing.Point(1097, 50);
            this.RSSebastiaoN1.Name = "RSSebastiaoN1";
            this.RSSebastiaoN1.Size = new System.Drawing.Size(176, 183);
            this.RSSebastiaoN1.TabIndex = 70;
            this.RSSebastiaoN1.Transparency = false;
            // 
            // RouroVN2
            // 
            this.RouroVN2.BackColor = System.Drawing.Color.Transparent;
            this.RouroVN2.enableTransparentBackground = true;
            this.RouroVN2.largEscala = 50;
            this.RouroVN2.Location = new System.Drawing.Point(915, 50);
            this.RouroVN2.Name = "RouroVN2";
            this.RouroVN2.Size = new System.Drawing.Size(176, 183);
            this.RouroVN2.TabIndex = 69;
            this.RouroVN2.Transparency = false;
            // 
            // RsBeneditoN1
            // 
            this.RsBeneditoN1.BackColor = System.Drawing.Color.Transparent;
            this.RsBeneditoN1.enableTransparentBackground = true;
            this.RsBeneditoN1.largEscala = 50;
            this.RsBeneditoN1.Location = new System.Drawing.Point(369, 50);
            this.RsBeneditoN1.Name = "RsBeneditoN1";
            this.RsBeneditoN1.Size = new System.Drawing.Size(176, 183);
            this.RsBeneditoN1.TabIndex = 68;
            this.RsBeneditoN1.Transparency = false;
            // 
            // RsBeneditoN2
            // 
            this.RsBeneditoN2.BackColor = System.Drawing.Color.Transparent;
            this.RsBeneditoN2.enableTransparentBackground = true;
            this.RsBeneditoN2.largEscala = 50;
            this.RsBeneditoN2.Location = new System.Drawing.Point(551, 50);
            this.RsBeneditoN2.Name = "RsBeneditoN2";
            this.RsBeneditoN2.Size = new System.Drawing.Size(176, 183);
            this.RsBeneditoN2.TabIndex = 67;
            this.RsBeneditoN2.Transparency = false;
            // 
            // RChanciaN2
            // 
            this.RChanciaN2.BackColor = System.Drawing.Color.Transparent;
            this.RChanciaN2.enableTransparentBackground = true;
            this.RChanciaN2.largEscala = 50;
            this.RChanciaN2.Location = new System.Drawing.Point(187, 50);
            this.RChanciaN2.Name = "RChanciaN2";
            this.RChanciaN2.Size = new System.Drawing.Size(176, 183);
            this.RChanciaN2.TabIndex = 66;
            this.RChanciaN2.Transparency = false;
            // 
            // RChanciaNivel1
            // 
            this.RChanciaNivel1.BackColor = System.Drawing.Color.Transparent;
            this.RChanciaNivel1.enableTransparentBackground = true;
            this.RChanciaNivel1.largEscala = 50;
            this.RChanciaNivel1.Location = new System.Drawing.Point(5, 50);
            this.RChanciaNivel1.Name = "RChanciaNivel1";
            this.RChanciaNivel1.Size = new System.Drawing.Size(176, 183);
            this.RChanciaNivel1.TabIndex = 65;
            this.RChanciaNivel1.Transparency = false;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.Location = new System.Drawing.Point(1163, 524);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(110, 15);
            this.label116.TabIndex = 64;
            this.label116.Text = "FATIMA NIVEL 1";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(950, 524);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(141, 15);
            this.label115.TabIndex = 63;
            this.label115.Text = "J.BOTANICO NIVEL 1";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(769, 524);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(140, 15);
            this.label114.TabIndex = 62;
            this.label114.Text = "GUTIERREZ NIVEL 1";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(595, 524);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(132, 15);
            this.label113.TabIndex = 61;
            this.label113.Text = "ESTADUAL NIVEL 2";
            // 
            // lblIndepN2
            // 
            this.lblIndepN2.AutoSize = true;
            this.lblIndepN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIndepN2.Location = new System.Drawing.Point(190, 524);
            this.lblIndepN2.Name = "lblIndepN2";
            this.lblIndepN2.Size = new System.Drawing.Size(173, 15);
            this.lblIndepN2.TabIndex = 60;
            this.lblIndepN2.Text = "INDEPENDENCIA NIVEL 2";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(413, 524);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(132, 15);
            this.label111.TabIndex = 59;
            this.label111.Text = "ESTADUAL NIVEL 1";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(8, 524);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(173, 15);
            this.label110.TabIndex = 58;
            this.label110.Text = "INDEPENDENCIA NIVEL 1";
            // 
            // lblSsebastiao
            // 
            this.lblSsebastiao.AutoSize = true;
            this.lblSsebastiao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSsebastiao.Location = new System.Drawing.Point(1124, 236);
            this.lblSsebastiao.Name = "lblSsebastiao";
            this.lblSsebastiao.Size = new System.Drawing.Size(149, 15);
            this.lblSsebastiao.TabIndex = 57;
            this.lblSsebastiao.Text = "S.SEBASTIAO NIVEL 1";
            // 
            // lblOuroVN2
            // 
            this.lblOuroVN2.AutoSize = true;
            this.lblOuroVN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOuroVN2.Location = new System.Drawing.Point(973, 236);
            this.lblOuroVN2.Name = "lblOuroVN2";
            this.lblOuroVN2.Size = new System.Drawing.Size(118, 15);
            this.lblOuroVN2.TabIndex = 56;
            this.lblOuroVN2.Text = "OURO V. NIVEL 2";
            // 
            // lblOuroVN1
            // 
            this.lblOuroVN1.AutoSize = true;
            this.lblOuroVN1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOuroVN1.Location = new System.Drawing.Point(791, 236);
            this.lblOuroVN1.Name = "lblOuroVN1";
            this.lblOuroVN1.Size = new System.Drawing.Size(118, 15);
            this.lblOuroVN1.TabIndex = 55;
            this.lblOuroVN1.Text = "OURO V. NIVEL 1";
            // 
            // lblSBN2
            // 
            this.lblSBN2.AutoSize = true;
            this.lblSBN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSBN2.Location = new System.Drawing.Point(583, 236);
            this.lblSBN2.Name = "lblSBN2";
            this.lblSBN2.Size = new System.Drawing.Size(144, 15);
            this.lblSBN2.TabIndex = 54;
            this.lblSBN2.Text = "S.BENEDITO NIVEL 2";
            // 
            // lblSBeneditoN1
            // 
            this.lblSBeneditoN1.AutoSize = true;
            this.lblSBeneditoN1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSBeneditoN1.Location = new System.Drawing.Point(401, 236);
            this.lblSBeneditoN1.Name = "lblSBeneditoN1";
            this.lblSBeneditoN1.Size = new System.Drawing.Size(144, 15);
            this.lblSBeneditoN1.TabIndex = 53;
            this.lblSBeneditoN1.Text = "S.BENEDITO NIVEL 1";
            // 
            // lblCN2
            // 
            this.lblCN2.AutoSize = true;
            this.lblCN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCN2.Location = new System.Drawing.Point(243, 236);
            this.lblCN2.Name = "lblCN2";
            this.lblCN2.Size = new System.Drawing.Size(120, 15);
            this.lblCN2.TabIndex = 52;
            this.lblCN2.Text = "CHANCIA NIVEL 2";
            // 
            // lblChamciaNivel1
            // 
            this.lblChamciaNivel1.AutoSize = true;
            this.lblChamciaNivel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChamciaNivel1.Location = new System.Drawing.Point(61, 236);
            this.lblChamciaNivel1.Name = "lblChamciaNivel1";
            this.lblChamciaNivel1.Size = new System.Drawing.Size(120, 15);
            this.lblChamciaNivel1.TabIndex = 51;
            this.lblChamciaNivel1.Text = "CHANCIA NIVEL 1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Reservatórios);
            this.tabControl1.Controls.Add(this.Acionamentos);
            this.tabControl1.Controls.Add(this.Chamcia);
            this.tabControl1.Controls.Add(this.SaoBenedito);
            this.tabControl1.Controls.Add(this.OuroVerde);
            this.tabControl1.Controls.Add(this.SaoSebastiao);
            this.tabControl1.Controls.Add(this.Independencia);
            this.tabControl1.Controls.Add(this.Estadual);
            this.tabControl1.Controls.Add(this.Gutierrez);
            this.tabControl1.Controls.Add(this.JardimBotanico);
            this.tabControl1.Controls.Add(this.Fatima);
            this.tabControl1.Controls.Add(this.PocoSolteiro);
            this.tabControl1.Controls.Add(this.RelacaoDosPoçosSolteiros);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1488, 688);
            this.tabControl1.TabIndex = 32;
            // 
            // RelacaoDosPoçosSolteiros
            // 
            this.RelacaoDosPoçosSolteiros.Controls.Add(this.pbRelacaoPocoSolteiro);
            this.RelacaoDosPoçosSolteiros.Location = new System.Drawing.Point(4, 22);
            this.RelacaoDosPoçosSolteiros.Name = "RelacaoDosPoçosSolteiros";
            this.RelacaoDosPoçosSolteiros.Size = new System.Drawing.Size(1480, 662);
            this.RelacaoDosPoçosSolteiros.TabIndex = 19;
            this.RelacaoDosPoçosSolteiros.Text = "RELAÇÃO DOS POÇOS SOLTEIROS";
            this.RelacaoDosPoçosSolteiros.UseVisualStyleBackColor = true;
            // 
            // pbRelacaoPocoSolteiro
            // 
            this.pbRelacaoPocoSolteiro.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pbRelacaoPocoSolteiro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbRelacaoPocoSolteiro.Location = new System.Drawing.Point(0, 0);
            this.pbRelacaoPocoSolteiro.Name = "pbRelacaoPocoSolteiro";
            this.pbRelacaoPocoSolteiro.Size = new System.Drawing.Size(1480, 662);
            this.pbRelacaoPocoSolteiro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRelacaoPocoSolteiro.TabIndex = 0;
            this.pbRelacaoPocoSolteiro.TabStop = false;
            // 
            // PocoSolteiro
            // 
            this.PocoSolteiro.Controls.Add(this.pbPocoSolteiro);
            this.PocoSolteiro.Location = new System.Drawing.Point(4, 22);
            this.PocoSolteiro.Name = "PocoSolteiro";
            this.PocoSolteiro.Size = new System.Drawing.Size(1480, 662);
            this.PocoSolteiro.TabIndex = 20;
            this.PocoSolteiro.Text = "POÇOS SOLTEIROS";
            this.PocoSolteiro.UseVisualStyleBackColor = true;
            // 
            // pbPocoSolteiro
            // 
            this.pbPocoSolteiro.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pbPocoSolteiro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbPocoSolteiro.Location = new System.Drawing.Point(0, 0);
            this.pbPocoSolteiro.Name = "pbPocoSolteiro";
            this.pbPocoSolteiro.Size = new System.Drawing.Size(1480, 662);
            this.pbPocoSolteiro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPocoSolteiro.TabIndex = 1;
            this.pbPocoSolteiro.TabStop = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(1488, 744);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(945, 706);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SATS - ARAGUARI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.Fatima.ResumeLayout(false);
            this.Fatima.PerformLayout();
            this.JardimBotanico.ResumeLayout(false);
            this.JardimBotanico.PerformLayout();
            this.Gutierrez.ResumeLayout(false);
            this.Gutierrez.PerformLayout();
            this.Estadual.ResumeLayout(false);
            this.Estadual.PerformLayout();
            this.Independencia.ResumeLayout(false);
            this.Independencia.PerformLayout();
            this.SaoSebastiao.ResumeLayout(false);
            this.SaoSebastiao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.OuroVerde.ResumeLayout(false);
            this.OuroVerde.PerformLayout();
            this.SaoBenedito.ResumeLayout(false);
            this.SaoBenedito.PerformLayout();
            this.Chamcia.ResumeLayout(false);
            this.Chamcia.PerformLayout();
            this.Acionamentos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbAcionamentos)).EndInit();
            this.Reservatórios.ResumeLayout(false);
            this.panelNivel.ResumeLayout(false);
            this.panelNivel.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.RelacaoDosPoçosSolteiros.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRelacaoPocoSolteiro)).EndInit();
            this.PocoSolteiro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbPocoSolteiro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatórioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizaçãoToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripLastUpdate;
        private System.Windows.Forms.ToolStripStatusLabel toolStripAlarmeEvento;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeAçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alarmesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eventosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarGastosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarMicromediçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private System.Windows.Forms.ToolStripMenuItem ativarEdiçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preventivaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pontoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gastosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dadosDoPoçoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nívelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bombaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AutomaticoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alarmesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem energiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripUser;
        private System.Windows.Forms.ToolStripStatusLabel toolStripVersion;
        private System.Windows.Forms.ToolStripMenuItem pressãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inversorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chaveSeletoraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraçãoDeAlarmeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraçãoSetPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consumoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pressãoEFrequênciaToolStripMenuItem;
        private System.Windows.Forms.TabPage Fatima;
        private System.Windows.Forms.Label fatimaVazaoBomba2;
        private PumpControlLibrary.WaterMeter waterMeter14;
        private System.Windows.Forms.Label fatimaVazaoBomba1;
        private PumpControlLibrary.WaterMeter waterMeter13;
        private System.Windows.Forms.Label label79;
        private PumpControlLibrary.Junção junção97;
        private PumpControlLibrary.Junção junção89;
        private System.Windows.Forms.Label label77;
        private PumpControlLibrary.Junção junção85;
        private PumpControlLibrary.Junção junção86;
        private PumpControlLibrary.Junção junção88;
        private PumpControlLibrary.Pump fatimaBomba3;
        private PumpControlLibrary.Junção junção90;
        private PumpControlLibrary.Junção junção91;
        private PumpControlLibrary.Junção junção92;
        private PumpControlLibrary.Pump fatimaBomba2;
        private ReservatorioLiquidos.cano fatimaCano2Bomba2;
        private PumpControlLibrary.Junção junção93;
        private PumpControlLibrary.Junção junção95;
        private PumpControlLibrary.Junção junção96;
        private System.Windows.Forms.Label label78;
        private PumpControlLibrary.Pump fatimaBomba1;
        private ReservatorioLiquidos.cano fatimaCano2Bomba1;
        private ReservatorioLiquidos.cano fatimaCano3Bomba1;
        private ReservatorioLiquidos.cano fatimaCano1Bomba3;
        private PumpControlLibrary.Junção junção84;
        private PumpControlLibrary.JunçãoT junçãoT51;
        private System.Windows.Forms.Label label69;
        private PumpControlLibrary.JunçãoT junçãoT44;
        private PumpControlLibrary.SubmersivePump fatimaPoco11;
        private ReservatorioLiquidos.cano fatimaCanoPoco11;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private PumpControlLibrary.JunçãoT junçãoT45;
        private PumpControlLibrary.Junção junção83;
        private PumpControlLibrary.JunçãoT junçãoT46;
        private PumpControlLibrary.SubmersivePump fatimaPoco12;
        private ReservatorioLiquidos.cano fatimaCanoPoco13;
        private PumpControlLibrary.JunçãoT junçãoT47;
        private PumpControlLibrary.JunçãoT junçãoT48;
        private PumpControlLibrary.JunçãoT junçãoT49;
        private PumpControlLibrary.JunçãoT junçãoT50;
        private ReservatorioLiquidos.cano fatimaCanoPoco17;
        private PumpControlLibrary.SubmersivePump fatimaPoco13;
        private PumpControlLibrary.SubmersivePump fatimaPoco14;
        private PumpControlLibrary.SubmersivePump fatimaPoco15;
        private PumpControlLibrary.SubmersivePump fatimaPoco16;
        private ReservatorioLiquidos.cano fatimaCanoPoco12;
        private ReservatorioLiquidos.cano fatimaCanoPoco14;
        private ReservatorioLiquidos.cano fatimaCanoPoco15;
        private ReservatorioLiquidos.cano fatimaCanoPoco16;
        private PumpControlLibrary.SubmersivePump fatimaPoco18;
        private PumpControlLibrary.SubmersivePump fatimaPoco17;
        private ReservatorioLiquidos.cano fatimaCanoPoco18;
        private ReservatorioLiquidos.Tanque fatimaTanque2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private PumpControlLibrary.JunçãoT junçãoT41;
        private PumpControlLibrary.JunçãoT junçãoT42;
        private PumpControlLibrary.SubmersivePump fatimaPoco1;
        private ReservatorioLiquidos.cano fatimaCanoPoco2;
        private PumpControlLibrary.JunçãoT junçãoT43;
        private PumpControlLibrary.SubmersivePump fatimaPoco2;
        private PumpControlLibrary.SubmersivePump fatimaPoco3;
        private ReservatorioLiquidos.cano fatimaCanoPoco3;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private PumpControlLibrary.JunçãoT junçãoT35;
        private PumpControlLibrary.Junção junção81;
        private ReservatorioLiquidos.Tanque fatimaTanque1;
        private PumpControlLibrary.Junção junção82;
        private PumpControlLibrary.JunçãoT junçãoT36;
        private PumpControlLibrary.SubmersivePump fatimaPoco4;
        private ReservatorioLiquidos.cano fatimaCanoPoco5;
        private PumpControlLibrary.JunçãoT junçãoT37;
        private PumpControlLibrary.JunçãoT junçãoT38;
        private PumpControlLibrary.JunçãoT junçãoT39;
        private PumpControlLibrary.JunçãoT junçãoT40;
        private ReservatorioLiquidos.cano fatimaCanoPoco9;
        private PumpControlLibrary.SubmersivePump fatimaPoco5;
        private PumpControlLibrary.SubmersivePump fatimaPoco6;
        private PumpControlLibrary.SubmersivePump fatimaPoco7;
        private PumpControlLibrary.SubmersivePump fatimaPoco8;
        private ReservatorioLiquidos.cano fatimaCanoPoco4;
        private ReservatorioLiquidos.cano fatimaCanoPoco6;
        private ReservatorioLiquidos.cano fatimaCanoPoco7;
        private ReservatorioLiquidos.cano fatimaCanoPoco8;
        private PumpControlLibrary.SubmersivePump fatimaPoco10;
        private PumpControlLibrary.SubmersivePump fatimaPoco9;
        private ReservatorioLiquidos.cano fatimaCanoPoco10;
        private ReservatorioLiquidos.cano fatimaCanoPoco1;
        private ReservatorioLiquidos.cano fatimaCano1Poco11;
        private ReservatorioLiquidos.cano fatimaCanoPocos;
        private PumpControlLibrary.Junção junção94;
        private ReservatorioLiquidos.cano fatimaCanoBomba1;
        private ReservatorioLiquidos.cano fatimaCanoBomba2;
        private PumpControlLibrary.Junção junção87;
        private ReservatorioLiquidos.cano fatimaCanoBomba3;
        private ReservatorioLiquidos.cano fatimaCano3Bomba2;
        private PumpControlLibrary.Junção junção98;
        private ReservatorioLiquidos.cano fatimaCanoGoias;
        private ReservatorioLiquidos.cano fatimaCano1Bomba2;
        private ReservatorioLiquidos.cano fatimaCano1Bomba1;
        private System.Windows.Forms.TabPage JardimBotanico;
        private PumpControlLibrary.Junção junção2;
        private ReservatorioLiquidos.Tanque jardimTanque1;
        private PumpControlLibrary.Junção junção32;
        private PumpControlLibrary.Junção junção29;
        private PumpControlLibrary.Junção junção1;
        private PumpControlLibrary.JunçãoT junçãoT10;
        private ReservatorioLiquidos.cano jardimCano2Poco2;
        private ReservatorioLiquidos.cano jardimCano1Poco2;
        private PumpControlLibrary.JunçãoT junçãoT9;
        private PumpControlLibrary.Junção junção28;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ReservatorioLiquidos.cano jardimCanoOut;
        private ReservatorioLiquidos.cano jardimCanoPoco;
        private ReservatorioLiquidos.cano jardimCanoPoco1;
        private ReservatorioLiquidos.cano jardimCanoPoco2;
        private PumpControlLibrary.SubmersivePump jardimPoco1;
        private PumpControlLibrary.Junção junção31;
        private ReservatorioLiquidos.cano jardimCano2Poco3;
        private ReservatorioLiquidos.cano jardimCano1Poco3;
        private PumpControlLibrary.SubmersivePump jardimPoco2;
        private PumpControlLibrary.SubmersivePump jardimPoco3;
        private System.Windows.Forms.TabPage Gutierrez;
        private System.Windows.Forms.Label gutierrezVazaoBomba2;
        private PumpControlLibrary.WaterMeter waterMeter12;
        private System.Windows.Forms.Label gutierrezVazaoBomba1;
        private PumpControlLibrary.WaterMeter waterMeter11;
        private PumpControlLibrary.Junção junção130;
        private PumpControlLibrary.Junção junção73;
        private System.Windows.Forms.Label label57;
        private PumpControlLibrary.Junção junção74;
        private System.Windows.Forms.Label label58;
        private PumpControlLibrary.Junção junção75;
        private PumpControlLibrary.Junção junção76;
        private PumpControlLibrary.Junção junção77;
        private PumpControlLibrary.Pump gutierrezBomba2;
        private ReservatorioLiquidos.cano gutierrezCano2Bomba2;
        private PumpControlLibrary.Junção junção78;
        private ReservatorioLiquidos.cano gutierrezCano3Bomba1;
        private PumpControlLibrary.Junção junção79;
        private PumpControlLibrary.Junção junção80;
        private PumpControlLibrary.Pump gutierrezBomba1;
        private ReservatorioLiquidos.cano gutierrezCano2Bomba1;
        private ReservatorioLiquidos.cano gutierrezCanoBomba1;
        private ReservatorioLiquidos.cano gutierrezCano1Bomba2;
        private PumpControlLibrary.JunçãoT junçãoT28;
        private PumpControlLibrary.SubmersivePump gutierrezPoco6;
        private System.Windows.Forms.Label label56;
        private PumpControlLibrary.Junção junção71;
        private PumpControlLibrary.JunçãoT junçãoT34;
        private PumpControlLibrary.JunçãoT junçãoT33;
        private PumpControlLibrary.SubmersivePump gutierrezPoco7;
        private ReservatorioLiquidos.cano gutierrezCanoPoco8;
        private ReservatorioLiquidos.cano gutierrezCanoPoco6;
        private System.Windows.Forms.Label label55;
        private PumpControlLibrary.JunçãoT junçãoT29;
        private PumpControlLibrary.JunçãoT junçãoT30;
        private PumpControlLibrary.JunçãoT junçãoT31;
        private PumpControlLibrary.JunçãoT junçãoT32;
        private ReservatorioLiquidos.cano gutierrezCanoPoco4;
        private PumpControlLibrary.SubmersivePump gutierrezPoco1;
        private PumpControlLibrary.SubmersivePump gutierrezPoco2;
        private PumpControlLibrary.SubmersivePump gutierrezPoco3;
        private ReservatorioLiquidos.cano gutierrezCanoPoco1;
        private ReservatorioLiquidos.cano gutierrezCanoPoco2;
        private ReservatorioLiquidos.cano gutierrezCanoPoco3;
        private PumpControlLibrary.Junção junção72;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private ReservatorioLiquidos.Tanque gutierrezTanque1;
        private PumpControlLibrary.SubmersivePump gutierrezPoco5;
        private PumpControlLibrary.SubmersivePump gutierrezPoco4;
        private ReservatorioLiquidos.cano gutierrezCanoPoco5;
        private ReservatorioLiquidos.cano gutierrezCanoPoco7;
        private ReservatorioLiquidos.cano gutierrezCanoBomba2;
        private ReservatorioLiquidos.cano gutierrezCano1Bomba1;
        private ReservatorioLiquidos.cano gutierrezCano3Bomba2;
        private PumpControlLibrary.SubmersivePump gutierrezPoco8;
        private System.Windows.Forms.TabPage Estadual;
        private System.Windows.Forms.Label label109;
        private PumpControlLibrary.Junção junção133;
        private ReservatorioLiquidos.cano estadualCano2Chamcia;
        private PumpControlLibrary.Junção junção132;
        private PumpControlLibrary.Pump estadualBombaChamcia;
        private PumpControlLibrary.Junção junção131;
        private System.Windows.Forms.Label estadualVazaoBomba2;
        private PumpControlLibrary.WaterMeter waterMeter10;
        private System.Windows.Forms.Label estadualVazaoBomba1;
        private PumpControlLibrary.WaterMeter waterMeter9;
        private PumpControlLibrary.Junção junção129;
        private PumpControlLibrary.Junção junção70;
        private System.Windows.Forms.Label label47;
        private PumpControlLibrary.Junção junção63;
        private System.Windows.Forms.Label label48;
        private PumpControlLibrary.Junção junção64;
        private PumpControlLibrary.Junção junção65;
        private PumpControlLibrary.Junção junção66;
        private PumpControlLibrary.Pump estadualBomba2;
        private ReservatorioLiquidos.cano estadualCano2Bomba2;
        private PumpControlLibrary.Junção junção67;
        private ReservatorioLiquidos.cano estadualCano3Bomba1;
        private PumpControlLibrary.Junção junção68;
        private PumpControlLibrary.Junção junção69;
        private PumpControlLibrary.Pump estadualBomba1;
        private ReservatorioLiquidos.cano estadualCano2Bomba1;
        private ReservatorioLiquidos.cano estadualCanoBomba1;
        private ReservatorioLiquidos.cano estadualCano1Bomba2;
        private PumpControlLibrary.JunçãoT junçãoT22;
        private PumpControlLibrary.Junção junção54;
        private ReservatorioLiquidos.Tanque estadualTanque1;
        private PumpControlLibrary.Junção junção62;
        private PumpControlLibrary.JunçãoT junçãoT23;
        private PumpControlLibrary.SubmersivePump estadualPoco1;
        private ReservatorioLiquidos.cano estadualCanoPoco2;
        private System.Windows.Forms.Label label40;
        private PumpControlLibrary.JunçãoT junçãoT24;
        private PumpControlLibrary.JunçãoT junçãoT25;
        private PumpControlLibrary.JunçãoT junçãoT26;
        private PumpControlLibrary.JunçãoT junçãoT27;
        private ReservatorioLiquidos.cano estadualCanoPoco6;
        private PumpControlLibrary.SubmersivePump estadualPoco2;
        private PumpControlLibrary.SubmersivePump estadualPoco3;
        private PumpControlLibrary.SubmersivePump estadualPoco4;
        private PumpControlLibrary.SubmersivePump estadualPoco5;
        private ReservatorioLiquidos.cano estadualCanoPoco1;
        private ReservatorioLiquidos.cano estadualCanoPoco3;
        private ReservatorioLiquidos.cano estadualCanoPoco4;
        private ReservatorioLiquidos.cano estadualCanoPoco5;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private PumpControlLibrary.SubmersivePump estadualPoco7;
        private PumpControlLibrary.SubmersivePump estadualPoco6;
        private ReservatorioLiquidos.cano estadualCanoPoco7;
        private ReservatorioLiquidos.Tanque estadualTanque2;
        private ReservatorioLiquidos.cano estadualCanoTanque2;
        private ReservatorioLiquidos.cano estadualCanoBomba2;
        private ReservatorioLiquidos.cano estadualCano1Bomba1;
        private ReservatorioLiquidos.cano estadualCano3Bomba2;
        private ReservatorioLiquidos.cano estadualCanoChamcia;
        private ReservatorioLiquidos.cano cano3;
        private ReservatorioLiquidos.cano cano4;
        private System.Windows.Forms.TabPage Independencia;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private PumpControlLibrary.JunçãoT junçãoT74;
        private PumpControlLibrary.JunçãoT junçãoT73;
        private PumpControlLibrary.JunçãoT junçãoT71;
        private PumpControlLibrary.JunçãoT junçãoT72;
        private PumpControlLibrary.Junção junção127;
        private PumpControlLibrary.Junção junção125;
        private PumpControlLibrary.Junção junção126;
        private PumpControlLibrary.Pump independenciaBomba2;
        private PumpControlLibrary.Junção junção123;
        private PumpControlLibrary.JunçãoT junçãoT70;
        private PumpControlLibrary.Junção junção122;
        private PumpControlLibrary.Junção junção121;
        private PumpControlLibrary.Junção junção119;
        private PumpControlLibrary.Junção junção120;
        private PumpControlLibrary.Pump independenciaBomba1;
        private ReservatorioLiquidos.cano independenciaCano1Bomba1;
        private PumpControlLibrary.JunçãoT junçãoT63;
        private PumpControlLibrary.Junção junção118;
        private PumpControlLibrary.JunçãoT junçãoT52;
        private ReservatorioLiquidos.Tanque independenciaTanque2;
        private ReservatorioLiquidos.Tanque independenciaTanque3;
        private PumpControlLibrary.Junção junção117;
        private PumpControlLibrary.Junção junção114;
        private ReservatorioLiquidos.Tanque independenciaTanque1;
        private PumpControlLibrary.JunçãoT junçãoT69;
        private PumpControlLibrary.Junção junção116;
        private PumpControlLibrary.Junção junção115;
        private PumpControlLibrary.JunçãoT junçãoT68;
        private ReservatorioLiquidos.cano independenciaCanoPocoSolt2;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private PumpControlLibrary.SubmersivePump independenciaPocoSolt1;
        private PumpControlLibrary.SubmersivePump independenciaPocoSolt2;
        private ReservatorioLiquidos.cano independenciaCanoPocoSolt1;
        private PumpControlLibrary.JunçãoT junçãoT64;
        private PumpControlLibrary.JunçãoT junçãoT65;
        private PumpControlLibrary.JunçãoT junçãoT66;
        private PumpControlLibrary.JunçãoT junçãoT67;
        private ReservatorioLiquidos.cano independenciaCanoPoco9;
        private PumpControlLibrary.SubmersivePump independenciaPoco12;
        private PumpControlLibrary.SubmersivePump independenciaPoco11;
        private PumpControlLibrary.SubmersivePump independenciaPoco10;
        private PumpControlLibrary.SubmersivePump independenciaPoco9;
        private ReservatorioLiquidos.cano independenciaCanoPoco11;
        private ReservatorioLiquidos.cano independenciaCanoPoco10;
        private PumpControlLibrary.Junção junção111;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private PumpControlLibrary.SubmersivePump independenciaPoco7;
        private PumpControlLibrary.SubmersivePump independenciaPoco8;
        private ReservatorioLiquidos.cano independenciaCanoPoco8;
        private PumpControlLibrary.Junção junção113;
        private PumpControlLibrary.JunçãoT junçãoT62;
        private PumpControlLibrary.JunçãoT junçãoT58;
        private PumpControlLibrary.JunçãoT junçãoT59;
        private PumpControlLibrary.JunçãoT junçãoT60;
        private PumpControlLibrary.JunçãoT junçãoT61;
        private ReservatorioLiquidos.cano independenciaCanoPoco3;
        private PumpControlLibrary.SubmersivePump independenciaPoco6;
        private PumpControlLibrary.SubmersivePump independenciaPoco5;
        private PumpControlLibrary.SubmersivePump independenciaPoco4;
        private PumpControlLibrary.SubmersivePump independenciaPoco3;
        private ReservatorioLiquidos.cano independenciaCano2Poco7;
        private ReservatorioLiquidos.cano independenciaCanoPoco6;
        private ReservatorioLiquidos.cano independenciaCanoPoco5;
        private ReservatorioLiquidos.cano independenciaCanoPoco4;
        private PumpControlLibrary.Junção junção112;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private ReservatorioLiquidos.Tanque independenciaTanque4;
        private PumpControlLibrary.SubmersivePump independenciaPoco1;
        private PumpControlLibrary.SubmersivePump independenciaPoco2;
        private ReservatorioLiquidos.cano independenciaCanoPoco2;
        private ReservatorioLiquidos.cano independenciaCanoTanque2;
        private ReservatorioLiquidos.cano independenciaCanoTanque3;
        private ReservatorioLiquidos.cano independenciaCanoPoco12;
        private ReservatorioLiquidos.cano independenciaCanoTanque4;
        private ReservatorioLiquidos.cano independenciaCanoPoco1;
        private ReservatorioLiquidos.cano independenciaCano1Poco7;
        private ReservatorioLiquidos.cano independenciaCanoPoco7;
        private ReservatorioLiquidos.cano independenciaCano3Tanque1;
        private ReservatorioLiquidos.cano independenciaCano2Tanque1;
        private ReservatorioLiquidos.cano independenciaCanoTanque1;
        private ReservatorioLiquidos.cano independenciaCanoBomba2;
        private ReservatorioLiquidos.cano independenciaCano2Bomba1;
        private ReservatorioLiquidos.cano independenciaCano3Bomba1;
        private PumpControlLibrary.Junção junção124;
        private ReservatorioLiquidos.cano independenciaCanoBomba1;
        private System.Windows.Forms.TabPage SaoSebastiao;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label sebastiaoVazaoBomba1;
        private PumpControlLibrary.WaterMeter waterMeter4;
        private System.Windows.Forms.Label sebastiaoVazaoBomba2;
        private PumpControlLibrary.WaterMeter waterMeter2;
        private PumpControlLibrary.Junção junção110;
        private System.Windows.Forms.Label label86;
        private PumpControlLibrary.Junção junção109;
        private PumpControlLibrary.Junção junção99;
        private PumpControlLibrary.JunçãoT junçãoT57;
        private PumpControlLibrary.Junção junção105;
        private PumpControlLibrary.Junção junção108;
        private PumpControlLibrary.JunçãoT junçãoT55;
        private PumpControlLibrary.JunçãoT junçãoT56;
        private ReservatorioLiquidos.cano sebastiaoCanoPoco6;
        private PumpControlLibrary.SubmersivePump sebastiaoPoco5;
        private ReservatorioLiquidos.cano sebastiaoCanoPoco5;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private PumpControlLibrary.SubmersivePump sebastiaoPoco7;
        private PumpControlLibrary.SubmersivePump sebastiaoPoco6;
        private ReservatorioLiquidos.cano sebastiaoCanoPoco7;
        private System.Windows.Forms.Label label85;
        private PumpControlLibrary.Junção junção101;
        private PumpControlLibrary.Junção junção102;
        private PumpControlLibrary.Junção junção103;
        private PumpControlLibrary.Junção junção104;
        private PumpControlLibrary.Pump sebastiaoBomba1;
        private ReservatorioLiquidos.cano sebastiaoCanoPoco4;
        private PumpControlLibrary.Junção junção106;
        private PumpControlLibrary.Junção junção107;
        private PumpControlLibrary.Pump sebastiaoBomba2;
        private PumpControlLibrary.Junção junção100;
        private PumpControlLibrary.JunçãoT junçãoT53;
        private PumpControlLibrary.JunçãoT junçãoT54;
        private ReservatorioLiquidos.cano sebastiaoCanoPoco2;
        private PumpControlLibrary.SubmersivePump sebastiaoPoco4;
        private PumpControlLibrary.SubmersivePump sebastiaoPoco1;
        private ReservatorioLiquidos.cano sebastiaoCanoPoco1;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private PumpControlLibrary.SubmersivePump sebastiaoPoco3;
        private PumpControlLibrary.SubmersivePump sebastiaoPoco2;
        private ReservatorioLiquidos.cano sebastiaoCanoPoco3;
        private System.Windows.Forms.Label label81;
        private ReservatorioLiquidos.Tanque sebastiaoTanque1;
        private ReservatorioLiquidos.Tanque sebastiaoTanque2;
        private ReservatorioLiquidos.cano sebastiaoCanoTanque2;
        private ReservatorioLiquidos.cano sebastiaoCanoBomba2;
        private ReservatorioLiquidos.cano sebastiaoCanoBomba1;
        private ReservatorioLiquidos.cano sebastiaoCano2Poco5;
        private ReservatorioLiquidos.cano sebastiaoCano2Poco1;
        private ReservatorioLiquidos.cano sebastiaoCano3Poco5;
        private ReservatorioLiquidos.cano sebastiaoCano1Bomba2;
        private ReservatorioLiquidos.cano sebastiaoCano1Bomba1;
        private System.Windows.Forms.TabPage OuroVerde;
        private PumpControlLibrary.JunçãoT junçãoT5;
        private PumpControlLibrary.JunçãoT junçãoT4;
        private PumpControlLibrary.JunçãoT junçãoT3;
        private PumpControlLibrary.JunçãoT junçãoT2;
        private PumpControlLibrary.JunçãoT junçãoT1;
        private ReservatorioLiquidos.cano ouroCanoPoco5;
        private PumpControlLibrary.SubmersivePump ouroPoco1;
        private PumpControlLibrary.SubmersivePump ouroPoco2;
        private PumpControlLibrary.SubmersivePump ouroPoco3;
        private PumpControlLibrary.SubmersivePump ouroPoco4;
        private PumpControlLibrary.Junção junção20;
        private System.Windows.Forms.Label label15;
        private PumpControlLibrary.Junção junção19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private PumpControlLibrary.Junção junção7;
        private ReservatorioLiquidos.cano ouroCanoPoco1;
        private ReservatorioLiquidos.cano ouroCanoPoco2;
        private ReservatorioLiquidos.cano ouroCanoPoco3;
        private ReservatorioLiquidos.cano ouroCanoPoco4;
        private PumpControlLibrary.Junção junção6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private ReservatorioLiquidos.Tanque ouroTanque3;
        private ReservatorioLiquidos.Tanque ouroTanque2;
        private ReservatorioLiquidos.Tanque ouroTanque1;
        private ReservatorioLiquidos.cano ouroCanoTanque2;
        private PumpControlLibrary.Junção junção17;
        private ReservatorioLiquidos.cano ouroCano2CidadeNova;
        private ReservatorioLiquidos.cano ouroCanoPoco7;
        private PumpControlLibrary.Junção junção4;
        private PumpControlLibrary.Junção junção18;
        private ReservatorioLiquidos.cano ouroCanoIpe;
        private ReservatorioLiquidos.cano ouroCanoOutTanque3;
        private PumpControlLibrary.SubmersivePump ouroPoco6;
        private PumpControlLibrary.SubmersivePump ouroPoco5;
        private ReservatorioLiquidos.cano ouroCanoPoco6;
        private PumpControlLibrary.SubmersivePump ouroPoco7;
        private PumpControlLibrary.JunçãoT junçãoT17;
        private ReservatorioLiquidos.cano ouroCanoGramVille;
        private ReservatorioLiquidos.cano ouroCano1CidadeNova;
        private System.Windows.Forms.TabPage SaoBenedito;
        private PumpControlLibrary.JunçãoT juncao1Benedito;
        private ReservatorioLiquidos.cano cano1;
        private System.Windows.Forms.Label beneditoVazaoBomba3;
        private PumpControlLibrary.WaterMeter waterMeter15;
        private PumpControlLibrary.Junção junção128;
        private System.Windows.Forms.Label beneditoVazaoBomba2;
        private PumpControlLibrary.WaterMeter waterMeter8;
        private System.Windows.Forms.Label beneditoVazaoBomba1;
        private PumpControlLibrary.WaterMeter waterMeter3;
        private PumpControlLibrary.Junção junção46;
        private System.Windows.Forms.Label label39;
        private PumpControlLibrary.Junção junção53;
        private PumpControlLibrary.SubmersivePump beneditoPoco1;
        private System.Windows.Forms.Label label31;
        private PumpControlLibrary.Junção junção51;
        private PumpControlLibrary.Junção junção52;
        private PumpControlLibrary.Pump beneditoBombaChamcia;
        private ReservatorioLiquidos.cano beneditoCano2Chamcia;
        private System.Windows.Forms.Label label38;
        private PumpControlLibrary.JunçãoT juncao2Benedito;
        private PumpControlLibrary.Junção junção16;
        private PumpControlLibrary.Junção junção47;
        private System.Windows.Forms.Label label36;
        private PumpControlLibrary.Junção junção48;
        private PumpControlLibrary.Junção junção49;
        private PumpControlLibrary.Junção junção50;
        private PumpControlLibrary.Pump beneditoBomba3;
        private ReservatorioLiquidos.cano beneditoCano2Bomba3;
        private PumpControlLibrary.Junção junção55;
        private ReservatorioLiquidos.cano beneditoCano3Bomba2;
        private PumpControlLibrary.Junção junção56;
        private PumpControlLibrary.Junção junção57;
        private PumpControlLibrary.Pump beneditoBomba2;
        private ReservatorioLiquidos.cano beneditoCano2Bomba2;
        private PumpControlLibrary.Junção junção58;
        private PumpControlLibrary.Junção junção59;
        private PumpControlLibrary.Junção junção60;
        private PumpControlLibrary.Junção junção61;
        private System.Windows.Forms.Label label37;
        private PumpControlLibrary.Pump beneditoBomba1;
        private ReservatorioLiquidos.cano beneditoCano21Bomba1;
        private ReservatorioLiquidos.cano beneditoCanoBomba1;
        private ReservatorioLiquidos.cano beneditoCano2Bomba1;
        private ReservatorioLiquidos.cano beneditoCano3Bomba1;
        private ReservatorioLiquidos.cano beneditoCanoBomba2;
        private ReservatorioLiquidos.cano beneditoCano1Bomba3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private PumpControlLibrary.Junção junção41;
        private PumpControlLibrary.JunçãoT junçãoT18;
        private PumpControlLibrary.Junção junção42;
        private PumpControlLibrary.SubmersivePump beneditoPocoSolt1;
        private PumpControlLibrary.SubmersivePump beneditoPocoSolt2;
        private PumpControlLibrary.Junção junção43;
        private PumpControlLibrary.Junção junção44;
        private PumpControlLibrary.Junção junção45;
        private ReservatorioLiquidos.cano beneditoCano3PocoSolt1;
        private ReservatorioLiquidos.Tanque beneditoTanque1;
        private ReservatorioLiquidos.cano beneditoCano1PocoSolt1;
        private PumpControlLibrary.JunçãoT junçãoT19;
        private ReservatorioLiquidos.cano beneditoCano1BelaVista2;
        private ReservatorioLiquidos.cano beneditoCano2BelaVista2;
        private ReservatorioLiquidos.cano beneditoCanoBelaVista1;
        private ReservatorioLiquidos.cano beneditoCanoPocoSolt2;
        private ReservatorioLiquidos.cano beneditoCanoPoco1;
        private ReservatorioLiquidos.Tanque beneditoTanque2;
        private ReservatorioLiquidos.cano beneditoCanoBomba3;
        private ReservatorioLiquidos.cano beneditoCanoChamcia;
        private ReservatorioLiquidos.cano beneditoCano1Bomba2;
        private ReservatorioLiquidos.cano beneditoCano1Bomba1;
        private ReservatorioLiquidos.cano beneditoCano2PocoSolt1;
        private ReservatorioLiquidos.cano beneditoCano3Bomba3;
        private ReservatorioLiquidos.cano cano2;
        private System.Windows.Forms.TabPage Chamcia;
        private System.Windows.Forms.Label chamciaVazaoBomba4;
        private PumpControlLibrary.WaterMeter waterMeter7;
        private System.Windows.Forms.Label chamciaVazaoBomba3;
        private PumpControlLibrary.WaterMeter waterMeter6;
        private System.Windows.Forms.Label chamciaVazaoBomba2;
        private PumpControlLibrary.WaterMeter waterMeter5;
        private System.Windows.Forms.Label chamciaVazaoBomba1;
        private PumpControlLibrary.WaterMeter waterMeter1;
        private PumpControlLibrary.JunçãoT junçãoT16;
        private PumpControlLibrary.Junção junção25;
        private PumpControlLibrary.Junção junção10;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label16;
        private PumpControlLibrary.Junção junção33;
        private PumpControlLibrary.Junção junção38;
        private PumpControlLibrary.JunçãoT junçãoT8;
        private PumpControlLibrary.Junção junção39;
        private PumpControlLibrary.Junção junção40;
        private PumpControlLibrary.Pump chamciaBomba4;
        private ReservatorioLiquidos.cano chamciaCano2Bomba4;
        private PumpControlLibrary.Junção junção37;
        private PumpControlLibrary.Junção junção34;
        private PumpControlLibrary.Junção junção35;
        private PumpControlLibrary.Junção junção36;
        private PumpControlLibrary.Pump chamciaBomba3;
        private ReservatorioLiquidos.cano chamciaCano2Bomba3;
        private PumpControlLibrary.Junção junção26;
        private PumpControlLibrary.Junção junção14;
        private ReservatorioLiquidos.cano chamciaCano3Bomba2;
        private PumpControlLibrary.Junção junção11;
        private PumpControlLibrary.Junção junção12;
        private PumpControlLibrary.Pump chamciaBomba2;
        private ReservatorioLiquidos.cano chamciaCano2Bomba2;
        private PumpControlLibrary.Junção junção5;
        private ReservatorioLiquidos.Tanque chamciaTanque1;
        private ReservatorioLiquidos.cano chamciaCanoBomba21;
        private PumpControlLibrary.Junção junção30;
        private PumpControlLibrary.Junção junção15;
        private PumpControlLibrary.Junção junção13;
        private PumpControlLibrary.Junção junção9;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label17;
        private PumpControlLibrary.Junção junção8;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private PumpControlLibrary.JunçãoT junçãoT6;
        private PumpControlLibrary.Junção junção21;
        private PumpControlLibrary.SubmersivePump chamciaPocoSolt1;
        private PumpControlLibrary.SubmersivePump chamciaPocoSolt2;
        private PumpControlLibrary.Pump chamciaBomba1;
        private PumpControlLibrary.Junção junção3;
        private PumpControlLibrary.JunçãoT junçãoT15;
        private PumpControlLibrary.SubmersivePump chamciaPoco1;
        private ReservatorioLiquidos.cano chamciaCanoPoco2;
        private System.Windows.Forms.Label label25;
        private PumpControlLibrary.JunçãoT junçãoT11;
        private PumpControlLibrary.JunçãoT junçãoT12;
        private PumpControlLibrary.JunçãoT junçãoT13;
        private PumpControlLibrary.JunçãoT junçãoT14;
        private ReservatorioLiquidos.cano chamciaCanoPoco6;
        private PumpControlLibrary.SubmersivePump chamciaPoco2;
        private PumpControlLibrary.SubmersivePump chamciaPoco3;
        private PumpControlLibrary.SubmersivePump chamciaPoco4;
        private PumpControlLibrary.SubmersivePump chamciaPoco5;
        private ReservatorioLiquidos.cano chamciaCanoPoco1;
        private ReservatorioLiquidos.cano chamciaCanoPoco3;
        private ReservatorioLiquidos.cano chamciaCanoPoco4;
        private ReservatorioLiquidos.cano chamciaCanoPoco5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private PumpControlLibrary.SubmersivePump chamciaPoco7;
        private PumpControlLibrary.SubmersivePump chamciaPoco6;
        private ReservatorioLiquidos.cano chamciaCanoPoco7;
        private PumpControlLibrary.Junção junção22;
        private PumpControlLibrary.Junção junção27;
        private PumpControlLibrary.Junção junção24;
        private PumpControlLibrary.Junção junção23;
        private ReservatorioLiquidos.cano chamciaCano3PocoSolt1;
        private System.Windows.Forms.Label label18;
        private ReservatorioLiquidos.Tanque chamciaTanque4;
        private ReservatorioLiquidos.Tanque chamciaTanque3;
        private ReservatorioLiquidos.Tanque chamciaTanque2;
        private ReservatorioLiquidos.cano chamciaCano1PocoSolt1;
        private ReservatorioLiquidos.cano chamciaCano2PocoSolt1;
        private PumpControlLibrary.JunçãoT junçãoT7;
        private ReservatorioLiquidos.cano chamciaCano1Suica3;
        private ReservatorioLiquidos.cano chamciaCano2Suica3;
        private ReservatorioLiquidos.cano chamciaCanoSuica2;
        private ReservatorioLiquidos.cano chamciaCanoPocoSolt2;
        private ReservatorioLiquidos.cano chamciaCanoBomba1;
        private ReservatorioLiquidos.cano chamciaCano12Bomba1;
        private ReservatorioLiquidos.cano chamciaCano2Bomba1;
        private ReservatorioLiquidos.cano chamciaCanoBomba2;
        private ReservatorioLiquidos.cano chamciaCano3Bomba3;
        private ReservatorioLiquidos.cano chamciaCanoBomba3;
        private ReservatorioLiquidos.cano chamciaCanoBomba4;
        private ReservatorioLiquidos.cano chamciaCano1Bomba4;
        private ReservatorioLiquidos.cano chamciaCanoTanque2;
        private ReservatorioLiquidos.cano chamciaCanoTanque3;
        private ReservatorioLiquidos.cano chamciaCano1Bomba3;
        private ReservatorioLiquidos.cano chamciaCano3Bomba1;
        private ReservatorioLiquidos.cano chamciaCano1Bomba2;
        private ReservatorioLiquidos.cano chamciaCano1Bomba1;
        private ReservatorioLiquidos.cano chamciaCano3Bomba4;
        private System.Windows.Forms.TabPage Acionamentos;
        private System.Windows.Forms.PictureBox pbAcionamentos;
        private System.Windows.Forms.TabPage Reservatórios;
        private System.Windows.Forms.Panel panelNivel;
        private ReservatorioLiquidos.TanqueEscala RouroVerdeN1;
        private ReservatorioLiquidos.TanqueEscala RIndependenciaN1;
        private ReservatorioLiquidos.TanqueEscala RIndependenciaN2;
        private ReservatorioLiquidos.TanqueEscala REstadualN1;
        private ReservatorioLiquidos.TanqueEscala REstadualN2;
        private ReservatorioLiquidos.TanqueEscala RGuitierrezN1;
        private ReservatorioLiquidos.TanqueEscala RJBN1;
        private ReservatorioLiquidos.TanqueEscala RFatimaN1;
        private ReservatorioLiquidos.TanqueEscala RSSebastiaoN1;
        private ReservatorioLiquidos.TanqueEscala RouroVN2;
        private ReservatorioLiquidos.TanqueEscala RsBeneditoN1;
        private ReservatorioLiquidos.TanqueEscala RsBeneditoN2;
        private ReservatorioLiquidos.TanqueEscala RChanciaN2;
        private ReservatorioLiquidos.TanqueEscala RChanciaNivel1;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label lblIndepN2;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label lblSsebastiao;
        private System.Windows.Forms.Label lblOuroVN2;
        private System.Windows.Forms.Label lblOuroVN1;
        private System.Windows.Forms.Label lblSBN2;
        private System.Windows.Forms.Label lblSBeneditoN1;
        private System.Windows.Forms.Label lblCN2;
        private System.Windows.Forms.Label lblChamciaNivel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStripMenuItem pSolteiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vazaoToolStripMenuItem;
        private System.Windows.Forms.TabPage RelacaoDosPoçosSolteiros;
        private System.Windows.Forms.PictureBox pbRelacaoPocoSolteiro;
        private System.Windows.Forms.TabPage PocoSolteiro;
        private System.Windows.Forms.PictureBox pbPocoSolteiro;
    }
}