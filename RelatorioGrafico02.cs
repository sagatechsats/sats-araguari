﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.DataVisualization.Charting.Borders3D;
using System.Windows.Forms.DataVisualization.Charting.ChartTypes;
using System.Windows.Forms.DataVisualization.Charting.Data;
using System.Windows.Forms.DataVisualization.Charting.Formulas;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using DevComponents.DotNetBar;


namespace SATS
{
    public partial class RelatorioGrafico02 : Office2007Form
    {
        List<string[]> paraGrafico;

        public RelatorioGrafico02(List<string[]> resultGrafics) {
           
            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
            
            this.paraGrafico = resultGrafics;
            this.ResizeRedraw = true;
            chart1.Series[0].Enabled = false;

        }

        private void relatorioGrafico_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }


        public void relatorioGrafico_Paint()
        {
            List<string> rotulos = new List<string>();

            //chart1.ChartAreas[0].AxisY.Maximum = this.achaMaior() + 50;
            //chart1.ChartAreas[0].AxisY.Minimum = this.achaMenor(this.achaMaior()) - 50;
            for (int contItens = 0; contItens < paraGrafico.Count; contItens++) {
                if (!rotulos.Contains(paraGrafico[contItens][0]))
                {
                    rotulos.Add(paraGrafico[contItens][0]);
                    this.chart1.Series.Add( paraGrafico[contItens][0]);
                }
                chart1.Series[ paraGrafico[contItens][0]].Points.AddXY(paraGrafico[contItens][2].ToString(), StringToDecimal(paraGrafico[contItens][1].ToString()));
                chart1.Series[paraGrafico[contItens][0]].ChartType = SeriesChartType.Line;
            }
        }

        private float achaMaior()
        {
            float maior = 0;
            for (int contItens = 0; contItens < paraGrafico.Count; contItens++)
            {
                if (Convert.ToInt32(paraGrafico[contItens][1]) > maior) {
                    maior = Convert.ToInt32(paraGrafico[contItens][1]);
                }
            }
            return (maior);
        }
        
        private float achaMenor(float maior)
        {
            float menor = maior;

            for (int contItens = 0; contItens < paraGrafico.Count; contItens++)
            {
                if (Convert.ToInt32(paraGrafico[contItens][1]) < menor)
                {
                    menor = Convert.ToInt32(paraGrafico[contItens][1]);
                }
            }
            return (menor);
        }


        private string ReturnDecimalWithDot(string commaDecimal)
        {
            char[] newDecimal = commaDecimal.ToCharArray();
            for (int i = 0; i < newDecimal.Length; ++i)
            {
                if (newDecimal[i] == ',')
                    newDecimal[i] = '.';
            }
            return new string(newDecimal);
        }

        private float StringToFloat(string value)
        {
            float temp = (float)Convert.ToDouble(value);
            return temp;
        }

        private decimal StringToDecimal(string value)
        {
            try
            {
                decimal temp = Convert.ToDecimal(value);
                return Decimal.Round(temp, 2);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
