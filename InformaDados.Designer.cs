﻿namespace SATS
{
    partial class InformaDados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InformaDados));
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pdocListView = new System.Drawing.Printing.PrintDocument();
            this.ppdListView = new System.Windows.Forms.PrintPreviewDialog();
            this.bttnPrint = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.Location = new System.Drawing.Point(12, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(641, 315);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "PONTO:";
            this.columnHeader1.Width = 209;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Ponto";
            this.columnHeader2.Width = 360;
            // 
            // pdocListView
            // 
            this.pdocListView.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.pdocListView_PrintPage);
            // 
            // ppdListView
            // 
            this.ppdListView.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.ppdListView.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.ppdListView.ClientSize = new System.Drawing.Size(400, 300);
            this.ppdListView.Document = this.pdocListView;
            this.ppdListView.Enabled = true;
            this.ppdListView.Icon = ((System.Drawing.Icon)(resources.GetObject("ppdListView.Icon")));
            this.ppdListView.Name = "ppdListView";
            this.ppdListView.Visible = false;
            this.ppdListView.Load += new System.EventHandler(this.ppdListView_Load);
            // 
            // bttnPrint
            // 
            this.bttnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnPrint.Image = global::SATS.Properties.Resources.crystal_clear_action_fileprint__1_;
            this.bttnPrint.Location = new System.Drawing.Point(536, 333);
            this.bttnPrint.Name = "bttnPrint";
            this.bttnPrint.Size = new System.Drawing.Size(117, 32);
            this.bttnPrint.TabIndex = 1;
            this.bttnPrint.Text = "Imprimir";
            this.bttnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnPrint.UseVisualStyleBackColor = true;
            this.bttnPrint.Click += new System.EventHandler(this.bttnPrint_Click);
            // 
            // InformaDados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(665, 377);
            this.Controls.Add(this.bttnPrint);
            this.Controls.Add(this.listView1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(681, 416);
            this.MinimumSize = new System.Drawing.Size(681, 416);
            this.Name = "InformaDados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dados Poço";
            this.Load += new System.EventHandler(this.InformaDados_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button bttnPrint;
        private System.Drawing.Printing.PrintDocument pdocListView;
        private System.Windows.Forms.PrintPreviewDialog ppdListView;



    }
}