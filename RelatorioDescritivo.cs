﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace SATS
{
    public partial class RelatorioDescritivo : RelatorioPai
    {
        int count = 0;
        int stempo = 0;
        List<string[]> result2 = new List<string[]>();
        List<string[]> result3 = new List<string[]>();
        public RelatorioDescritivo()
        {
            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
            comboBoxCliente.Items.Clear();
            comboBoxCliente.AutoCompleteCustomSource.Clear();
            comboBoxCliente.Items.Add("TODOS OS PONTOS DE PRODUÇÃO");
            comboBoxCliente.Items.Add("TODOS OS PONTOS DE PROCESSO");
            this.WindowState = FormWindowState.Maximized;
        }

        private void Relatorio_Load(object sender, EventArgs e)
        {

        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            //result2.Clear();
            try
            {
                result2.Clear();
                result3.Clear();
            }
            catch (Exception)
            {

            }
            listView.Items.Clear();
            if (CheckDateValidity() == false)
                return;

            int index = comboBoxCliente.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar-se um ponto.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            StoreSearchData();
            Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTela));
            novaThread.Start(index);
            labelListView.Text = "Resultados para Ponto:" + ultCliente + " de " + ultDateIn +
                " a " + ultDateFi;
        }

        private void preencheList(List<string[]> consultalist)
        {
            //listView.Items.Clear();
            int j = 0;
            TimeSpan ts = Convert.ToDateTime(dateTimeFinal.Value.ToString("s")).Subtract(Convert.ToDateTime(dateTimeInicial.Value.ToString("s")));
            stempo = Convert.ToInt32(ts.TotalDays);
            for (int i = 0; i < (24 * count); i++)
            {
                string[] subitems = new string[5];
                {
                    subitems[0] = "";
                    subitems[1] = "";
                    subitems[2] = "";
                    subitems[3] = "";
                    subitems[4] = j.ToString();
                }
                //List<string> item = new List<string>(subitems);
                result3.Add(subitems);
                //listView.Items.Add(item); //carrega os itens no list view
                //j=i;
                j++;
                j = j % 24;
            }
            // label1.Text = "Quantidade de dados: " + consultalist.Count; //escreve na label a quantidade de dados exibidos no listview
            int i4 = 0;
            for (int i3 = 0; i3 < consultalist.Count; i3++)
            {
                for (int i2 = i4; i2 < 24 * count; i2++)
                {
                    if (result3[i2][4].ToString().CompareTo(consultalist[i3][4].ToString()) == 0)
                    {
                        result3[i2][0] = consultalist[i3][0].ToString();
                        result3[i2][1] = consultalist[i3][1].ToString();
                        result3[i2][2] = consultalist[i3][2].ToString();
                        result3[i2][3] = consultalist[i3][3].ToString();
                        //listView.Items[i2].SubItems[4].Text = consultalist[i3][4].ToString();
                        i4 = i2;
                        break;
                    }
                }
            }
            string[] rotulos = new string[28];
            rotulos[0] = consultalist[0][0];

            int k = 0;
            for (int i = 0; i < consultalist.Count; i++)
            {
                if (!rotulos[k].Contains(consultalist[i][0]))
                {
                    k++;
                    rotulos[k] = consultalist[i][0];
                }
            }

            k = -1;
            for (int i2 = 0; i2 < result3.Count; i2++)
            {
                if (i2 % (24 * stempo) == 0)
                {
                    k++;
                }
                result3[i2][0] = rotulos[k];
                //if (listView.Items[i2].SubItems[4].Text.CompareTo(consultalist[i3][4].ToString()) == 0)
            }
        }

        private void CarregaTela(object parametro)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            try
            {
                int index = Convert.ToInt32(parametro);
                if (checkBox1.Checked == true)
                {
                    try
                    {
                        List<string[]> result;
                        if (index > 1)
                        {
                            count = 0;
                            result = dbManager.Obterporhora(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), indexClientes[index - 2].ToString());
                            count = dbManager.Obterporquatidade(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), indexClientes[index - 2].ToString());
                            Invoke((MethodInvoker)(() => gerarGrafico.Enabled = true));
                            Invoke((MethodInvoker)(() => geraCurvaAbc.Enabled = false));
                        }
                        else if (index == 0)
                        {
                            result = dbManager.Obterporhora(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), "producao");
                            count = dbManager.Obterporquatidade(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), "");
                            Invoke((MethodInvoker)(() => gerarGrafico.Enabled = false));
                            Invoke((MethodInvoker)(() => geraCurvaAbc.Enabled = true));
                        }
                        else
                        {
                            result = dbManager.Obterporhora(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), "processo");
                            count = dbManager.Obterporquatidade(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), "");
                            Invoke((MethodInvoker)(() => gerarGrafico.Enabled = false));
                            Invoke((MethodInvoker)(() => geraCurvaAbc.Enabled = true));
                        }

                        if (result.Count <= 0)
                        {
                            Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum resultado para o ponto selecionado.",
                                "Aviso: Nenhum resultado encontrado.",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                            Application.UseWaitCursor = false;
                            Invoke((MethodInvoker)(() => gerarGrafico.Enabled = true));
                            Application.DoEvents();
                            return;
                        }
                        Invoke((MethodInvoker)(() => FillListView(result)));
                        result2 = result;
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    List<string[]> result;
                    if (index > 1)
                    {
                        string whereId = "ponto_leitura.id_ponto=" + indexClientes[index - 2].ToString();
                        string whereDate = " and data_hora > '" + dateTimeInicial.Value.ToString("s") +
                                           "' and data_hora  < '" + dateTimeFinal.Value.ToString("s") + "'";
                        result = dbManager.SelectInnerJoin("ponto", "nome_ponto, Round(volume/1000, 0), Round(vazao/1000,2), data_hora", "ponto_leitura", "ponto.id_ponto = ponto_leitura.id_ponto", whereId + whereDate, "data_hora, ponto_leitura.id_ponto", " ASC");
                        Invoke((MethodInvoker)(() => gerarGrafico.Enabled = true));
                        Invoke((MethodInvoker)(() => geraCurvaAbc.Enabled = false));
                    }
                    else if (index == 0)
                    {
                        string whereId = "ponto_tipo='producao' ";
                        string whereDate = " and data_hora > '" + dateTimeInicial.Value.ToString("s") +
                                           "' and data_hora  < '" + dateTimeFinal.Value.ToString("s") + "'";
                        result = dbManager.SelectInnerJoin("ponto", "nome_ponto, Round(volume/1000, 0), Round(vazao/1000,2), data_hora", "ponto_leitura", "ponto.id_ponto = ponto_leitura.id_ponto", whereId + whereDate, "ponto_leitura.id_ponto, data_hora", " ASC");
                        Invoke((MethodInvoker)(() => gerarGrafico.Enabled = false));
                        Invoke((MethodInvoker)(() => geraCurvaAbc.Enabled = true));
                    }
                    else
                    {
                        string whereId = "ponto_tipo='processo' ";
                        string whereDate = " and data_hora > '" + dateTimeInicial.Value.ToString("s") +
                                           "' and data_hora  < '" + dateTimeFinal.Value.ToString("s") + "'";
                        result = dbManager.SelectInnerJoin("ponto", "nome_ponto, Round(volume/1000, 0), Round(vazao/1000,2), data_hora", "ponto_leitura", "ponto.id_ponto = ponto_leitura.id_ponto", whereId + whereDate, "ponto_leitura.id_ponto, data_hora", " ASC");
                        Invoke((MethodInvoker)(() => gerarGrafico.Enabled = false));
                        Invoke((MethodInvoker)(() => geraCurvaAbc.Enabled = true));
                    }
                    if (result.Count <= 0)
                    {
                        Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum resultado para o ponto selecionado.",
                            "Aviso: Nenhum resultado encontrado.",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                        Application.UseWaitCursor = false;
                        Invoke((MethodInvoker)(() => gerarGrafico.Enabled = true));
                        Application.DoEvents();
                        return;
                    }
                    result2 = result;
                    Invoke((MethodInvoker)(() => FillListView(result)));
                }
            }
            catch (Exception)
            {
            }
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void preencheTabela(string where)
        {
            // List<string[]> resultSet = conecta.SelectInnerJoin("ponto", "nome_ponto, volume, vazao, data_hora", "ponto_leitura", "ponto.id_ponto = ponto_leitura.id_ponto", where, "data_hora", " ASC");
            //tabelaPontosGridView
        }

        private void gerarGrafico_Click(object sender, EventArgs e)
        {
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar gráfico.",
                "Aviso: Lista de Resultados Vazia",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            RelatorioGrafico01 chart = new RelatorioGrafico01(result2);
            chart.relatorioGrafico_Paint();
            chart.ShowDialog();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void buttonLimpar_Click_1(object sender, EventArgs e)
        {
            Invoke((MethodInvoker)(() => gerarGrafico.Enabled = true));
            Invoke((MethodInvoker)(() => geraCurvaAbc.Enabled = true));
        }

        private void geraCurvaAbc_Click(object sender, EventArgs e)
        {
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar planilha.",
                    "Aviso: Lista de Resultados Vazia",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            new CurvaAbc(result2).Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread novaThread = new Thread(new ThreadStart(Carregagastos));
            novaThread.Start();
        }

        private void buttonXls_Click(object sender, EventArgs e)
        {
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar planilha.",
                    "Aviso: Lista de Resultados Vazia",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            SpreadsheetCreator2 creator = new SpreadsheetCreator2(listView);
            creator.CreateFile("Descritivo", ultCliente, ultDateIn, ultDateFi);
        }

        private void Carregagastos()
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            ultDateIn = dateTimeInicial.Value.ToShortDateString();
            ultDateFi = dateTimeFinal.Value.ToShortDateString();
            string table1 = "gastos";
            string cols1 = "*";
            string whereDate1 = " gastos.data between '" + dateTimeInicial.Value.ToString("s") +
                               "' and '" + dateTimeFinal.Value.ToString("s") + "'";
            string orderBy1 = "gastos.data";
            string[] subitems = new string[5];
            subitems = new string[] { "identificador", "Tipo de Gasto", "Descrição", "Data", "Valor(R$)" };
            List<string[]> result = dbManager.Select2(table1, cols1, whereDate1, orderBy1);
            if (result.Count <= 0)
            {
                Invoke((MethodInvoker)(() => MessageBox.Show("Sem resultados para gerar planilha.",
                    "Aviso: Lista de Resultados Vazia",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                Application.UseWaitCursor = false;
                Application.DoEvents();
                return;
            }
            SpreadsheetCreator creator = new SpreadsheetCreator(result, subitems);
            Invoke((MethodInvoker)(() => creator.CreateFile("Indicadores", "Gasto no Período: ", ultDateIn, ultDateFi)));
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            string where = "";
            string whereMicro = "";
            ultDateIn = dateTimeInicial.Value.ToShortDateString();
            ultDateFi = dateTimeFinal.Value.ToShortDateString();
            whereMicro = whereMicro + "DATE(data_inicial) = DATE('" + dateTimeInicial.Value.ToString("s") + "') AND DATE(data_final) = DATE('" + dateTimeFinal.Value.ToString("s") + "')";
            where = "";
            where = where + "(data_hora BETWEEN '" + dateTimeInicial.Value.ToString("s") + "' AND '" + dateTimeFinal.Value.ToString("s") + "')";
            List<string[]> resultSet = dbManager.Select("micromedicao", "*", whereMicro, " data_inicial");
            if (resultSet.Count <= 0)
            {
                Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum registros de micromedição para o periodo selecionado.",
                    "Aviso: Nenhum resultado encontrado",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                Application.UseWaitCursor = false;
                Application.DoEvents();
                return;
            }
            List<string[]> resultSetTotal = dbManager.obtermicro(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"));
            if (resultSet.Count <= 0)
            {
                Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum registros de macromedição para o periodo selecionado.",
                    "Aviso: Nenhum resultado encontrado",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                Application.UseWaitCursor = false;
                Application.DoEvents();
                return;
            }
            SpreadsheetCreator3 creator = new SpreadsheetCreator3(resultSetTotal);
            Invoke((MethodInvoker)(() => creator.CreateFile("Indicadores", "Gasto no Período: ", ultDateIn, ultDateFi, resultSet)));
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }
    }
}
