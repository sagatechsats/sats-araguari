﻿using System;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class ImportarMicromedicao : Office2007Form
    {
        public ImportarMicromedicao()
        {
            InitializeComponent();
        }

        private void importarMicromedicao_Load(object sender, EventArgs e)
        {
            dataInicial.Value = DateTime.Now;
            dataFinal.Value = DateTime.Now;
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            if (volumeMicro.Text == "")
            {
                MessageBox.Show("Favor preencher todos os campos");
            }
            else
            {
                micromedicao Micromedicao = new micromedicao(dataInicial.Value, dataFinal.Value, Convert.ToInt32(volumeMicro.Text));
                bdMicromedicao trabMicro = new bdMicromedicao();

                trabMicro.setMicromedicao(Micromedicao);
                trabMicro.inserir();
                this.Close();
            }
        }
    }
}
