﻿namespace SATS
{
    partial class ImportarGastos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportarGastos));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rEnergia = new System.Windows.Forms.RadioButton();
            this.rPQuimicos = new System.Windows.Forms.RadioButton();
            this.rOutros = new System.Windows.Forms.RadioButton();
            this.descricaoGasto = new System.Windows.Forms.TextBox();
            this.dataGasto = new System.Windows.Forms.DateTimePicker();
            this.valorGasto = new System.Windows.Forms.MaskedTextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(56, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Descrição:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(57, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mês:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(53, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Valor:";
            // 
            // rEnergia
            // 
            this.rEnergia.AutoSize = true;
            this.rEnergia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rEnergia.Location = new System.Drawing.Point(106, 28);
            this.rEnergia.Name = "rEnergia";
            this.rEnergia.Size = new System.Drawing.Size(73, 20);
            this.rEnergia.TabIndex = 1;
            this.rEnergia.TabStop = true;
            this.rEnergia.Text = "Energia";
            this.rEnergia.UseVisualStyleBackColor = true;
            // 
            // rPQuimicos
            // 
            this.rPQuimicos.AutoSize = true;
            this.rPQuimicos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rPQuimicos.Location = new System.Drawing.Point(231, 28);
            this.rPQuimicos.Name = "rPQuimicos";
            this.rPQuimicos.Size = new System.Drawing.Size(139, 20);
            this.rPQuimicos.TabIndex = 2;
            this.rPQuimicos.TabStop = true;
            this.rPQuimicos.Text = "Produtos Químicos";
            this.rPQuimicos.UseVisualStyleBackColor = true;
            // 
            // rOutros
            // 
            this.rOutros.AutoSize = true;
            this.rOutros.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rOutros.Location = new System.Drawing.Point(399, 30);
            this.rOutros.Name = "rOutros";
            this.rOutros.Size = new System.Drawing.Size(65, 20);
            this.rOutros.TabIndex = 3;
            this.rOutros.TabStop = true;
            this.rOutros.Text = "Outros";
            this.rOutros.UseVisualStyleBackColor = true;
            // 
            // descricaoGasto
            // 
            this.descricaoGasto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descricaoGasto.Location = new System.Drawing.Point(106, 58);
            this.descricaoGasto.MaxLength = 200;
            this.descricaoGasto.Multiline = true;
            this.descricaoGasto.Name = "descricaoGasto";
            this.descricaoGasto.Size = new System.Drawing.Size(349, 107);
            this.descricaoGasto.TabIndex = 4;
            // 
            // dataGasto
            // 
            this.dataGasto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGasto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataGasto.Location = new System.Drawing.Point(106, 188);
            this.dataGasto.Name = "dataGasto";
            this.dataGasto.Size = new System.Drawing.Size(349, 22);
            this.dataGasto.TabIndex = 5;
            // 
            // valorGasto
            // 
            this.valorGasto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valorGasto.Location = new System.Drawing.Point(106, 233);
            this.valorGasto.Name = "valorGasto";
            this.valorGasto.Size = new System.Drawing.Size(349, 22);
            this.valorGasto.TabIndex = 8;
            this.valorGasto.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_apply1;
            this.btnSalvar.Location = new System.Drawing.Point(231, 280);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(89, 37);
            this.btnSalvar.TabIndex = 7;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // importarGastos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(529, 342);
            this.Controls.Add(this.valorGasto);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.dataGasto);
            this.Controls.Add(this.descricaoGasto);
            this.Controls.Add(this.rOutros);
            this.Controls.Add(this.rPQuimicos);
            this.Controls.Add(this.rEnergia);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(545, 381);
            this.MinimumSize = new System.Drawing.Size(545, 381);
            this.Name = "importarGastos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Importar Gastos";
            this.Load += new System.EventHandler(this.importarGastos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rEnergia;
        private System.Windows.Forms.RadioButton rPQuimicos;
        private System.Windows.Forms.RadioButton rOutros;
        private System.Windows.Forms.TextBox descricaoGasto;
        private System.Windows.Forms.DateTimePicker dataGasto;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.MaskedTextBox valorGasto;
    }
}