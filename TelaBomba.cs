﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public partial class TelaBomba : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
        private int X = 0;
        private int Y = 0;
        private int id = 0;
        private string nome = "";
        private bool acionado;
        private bool acionadoAutomatico;
        private int acionamento = 2;
        private Thread Threadupdate;
        private int TipoDados;
        private String Status;

        public TelaBomba(int id, string nomep, int bomba, int tipodados, float nivel_max, string status)
        {
            InitializeComponent();
            this.id = id;
            nome = nomep;
            TipoDados = tipodados;
            Status = status;
            acionamento = 2;
            verifica(id);
            enableBomba(bomba);
        }

        private void enableBomba(int bomba)
        {
            if (bomba == 1)
            {
                buttonAciona.Enabled = true;
                buttonAciona.Visible = true;
                labelAciona.Enabled = true;
                labelAciona.Visible = true;
                lblAcionamentoAutomatico.Visible = true;
                btnAcionamentoAutomatico.Visible = true;
                lblAcionamentoAutomatico.Enabled = true;
                btnAcionamentoAutomatico.Enabled = true;
                //SetLed(vazao, datahoraleitura);
            }
            else if (bomba == 0)
            {
                buttonAciona.Enabled = false;
                buttonAciona.Visible = false;
                labelAciona.Enabled = false;
                labelAciona.Visible = false;
                buttonAciona.Text = "";
                lblAcionamentoAutomatico.Visible = false;
                btnAcionamentoAutomatico.Visible = false;
                lblAcionamentoAutomatico.Enabled = false;
                btnAcionamentoAutomatico.Enabled = false;
                //SetLed(vazao, datahoraleitura);
            }
        }


        public static bool IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public bool verifica(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> Acionamento = DatabaseManager.Select("ponto_aciona", "acionar, porta, data_hora, status_acionar", "id_ponto= '" + id + "'");
                List<string[]> Automatico = DatabaseManager.Select("ponto_automatico", "habilitado ", "id_bomba= '" + id + "'");

                if (Automatico.Count == 0)
                {
                    AUTOMATICO(Convert.ToBoolean(false));
                }
                else
                {
                    AUTOMATICO(Convert.ToBoolean(Automatico[0][0]));
                }

                if (Acionamento.Count > 0 && (acionamento == Convert.ToInt32(Acionamento[0][3]) || acionamento == 2))
                {
                    LEDS(Convert.ToInt32(Acionamento[0][3]));
                    //  Invoke((MethodInvoker)(() => AUTOMATICO(Convert.ToInt32(Automatico[0][0]))));
                    acionamento = 2;
                    SetLed(float.Parse(Acionamento[0][3]), Acionamento[0][2]);
                    this.UseWaitCursor = false; //Cursor de espera do mouse false
                    Application.DoEvents();
                }

                if (TipoDados == 1)
                {
                    if (Status.Length == 0)
                        labelHora.Text = "Última atualização: " + Acionamento[0][2];
                    else
                        labelHora.Text = "Última atualização: " + Acionamento[0][2] + "\r\n" + Status;

                    buttonAciona.Enabled = true;
                    btnAcionamentoAutomatico.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }


        public void AUTOMATICO(bool automatico)
        {

            if (automatico == false)
            {

                btnAcionamentoAutomatico.BackgroundImage = ((Image)(Properties.Resources.switchoff));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                btnAcionamentoAutomatico.Enabled = true;
                acionadoAutomatico = false;

            }
            else if (automatico == true)
            {

                btnAcionamentoAutomatico.BackgroundImage = ((Image)(Properties.Resources.switchon));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                btnAcionamentoAutomatico.Enabled = true;
                acionadoAutomatico = true;

            }
            else
            {
                pictureBox2.BackgroundImage = ((Image)(Properties.Resources.ledgray));
                btnAcionamentoAutomatico.Enabled = false;
            }

        }



        public void SetLed(float vazao, string datahora)
        {
            decimal stempo = 0;

            DateTime datanow = DateTime.Now;
            DateTime data = DateTime.Now;
            try
            {
                data = Convert.ToDateTime(datahora);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

            TimeSpan ts = datanow.Subtract(data);
            stempo = Convert.ToDecimal(ts.TotalMinutes);


            if (vazao == 0)
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                pictureBox3.Visible = false;
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    pictureBox3.Visible = true;
                }
            }
            else
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                pictureBox3.Visible = false;
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    pictureBox3.Visible = false;
                }
            }
        }


        public static DialogResult show(string ponto, int bomba, string alarmeE, string descricao, string horaAE, int identificador, int tipodados, string nivel_max, string status)
        {
            if (IsConnected() == true)
            {
                TelaBomba MsgBox = new TelaBomba(identificador, ponto, bomba, tipodados, float.Parse(nivel_max), status);
                //MsgBox.AutoValidate = AutoValidate.EnablePreventFocusChange;
                //MsgBox.aquaGaugeVazao.DigitalValue = vazao / 1000;
                //MsgBox.aquaGaugeVazao.Value = vazao / 1000;
                //MsgBox.sevenSvolume.Value = (volume / 1000).ToString().PadLeft(7, '0');
                //MsgBox.label2.Text = "Última atulização: " + datahoraleitura;
                MsgBox.labelPonto.Text = ponto;
                MsgBox.labelAevento.Text = "Último Alarme ou Evento: " + alarmeE;
                MsgBox.labelAhora.Text = "Hora: " + horaAE;
                MsgBox.labelAdescricao.Text = "Descrição: " + descricao;

                Console.WriteLine(ponto +""+alarmeE+" "+horaAE+" "+descricao);
                DialogResult result = MsgBox.ShowDialog();
                return result;
            }
            else
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return DialogResult.Cancel;
            }
        }

        public void LEDS(int led)
        {
            //button2.Visible = true;
            if (led == 0)
            {
                buttonAciona.BackgroundImage = ((Image)(Properties.Resources.switchoff));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
               buttonAciona.Enabled = true;


                acionado = false;
            }
            else if (led == 1)
            {
                buttonAciona.BackgroundImage = ((Image)(Properties.Resources.switchon));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                buttonAciona.Enabled = true;
                acionado = true;
            }
            else
            {
                pictureBox2.BackgroundImage = ((Image)(Properties.Resources.ledgray));
                buttonAciona.Enabled = false;
            }
        }

        public void ligaAutomatico()
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                if (acionadoAutomatico == false)
                {
                    DatabaseManager.Update("ponto_automatico", "habilitado='1'", "id_bomba='" + id + "'");
                    acionadoAutomatico = true;
                    //  acionamento = 1;
                }
                else
                {
                    DatabaseManager.Update("ponto_automatico", "habilitado='0'", "id_bomba='" + id + "'");
                    acionadoAutomatico = false;
                    //    acionamento = 0;
                }
            }
            catch (Exception)
            {
                try
                {
                    if (acionado == false)
                    {
                        btnAcionamentoAutomatico.BackgroundImage = Properties.Resources.switchoff;
                        btnAcionamentoAutomatico.Enabled = true;
                    }
                    else
                    {
                        btnAcionamentoAutomatico.BackgroundImage = ((Image)(Properties.Resources.switchon));
                        btnAcionamentoAutomatico.Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }

        }

        public void ligabomba()
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                if (acionado == false)
                {
                    DatabaseManager.Update("ponto_aciona", "acionar='1'", "id_ponto='" + id + "'");
                    acionado = true;
                    acionamento = 1;
                }
                else
                {
                    DatabaseManager.Update("ponto_aciona", "acionar='0'", "id_ponto='" + id + "'");
                    acionado = false;
                    acionamento = 0;
                }
            }
            catch (Exception)
            {
                try
                {
                    if (acionado == false)
                    {
                        buttonAciona.BackgroundImage = Properties.Resources.switchoff;
                        buttonAciona.Enabled = true;
                    }
                    else
                    {
                        buttonAciona.BackgroundImage = ((Image)(Properties.Resources.switchon));
                        buttonAciona.Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }
        }


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            X = this.Left - MousePosition.X;
            Y = this.Top - MousePosition.Y;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            this.Left = X + MousePosition.X;
            this.Top = Y + MousePosition.Y;
        }

        private void buttonFecha_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmInformacao frm = new FrmInformacao(id,nome);
            frm.Show();
        }

        private void buttonAciona_Click(object sender, EventArgs e)
        {
            // Set cursor as hourglass
            //Cursor.Current = Cursors.WaitCursor;
            this.UseWaitCursor = true; //curso de espera do mouse true
            Application.DoEvents();

            buttonAciona.Enabled = false;

            if (acionado == false)
            {
                buttonAciona.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.switchon));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                Thread novaThread1 = new Thread(new ThreadStart(ligabomba));
                novaThread1.Start();
            }
            else
            {
                buttonAciona.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.switchoff));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledgray));
                Thread novaThread1 = new Thread(new ThreadStart(ligabomba));
                novaThread1.Start();
            }
        }

        private void btnAcionamentoAutomatico_Click(object sender, EventArgs e)
        {
            this.UseWaitCursor = true; //curso de espera do mouse true
            Application.DoEvents();

            btnAcionamentoAutomatico.Enabled = false;

            if (acionadoAutomatico == false)
            {
                btnAcionamentoAutomatico.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.switchon));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                Thread novaThread1 = new Thread(new ThreadStart(ligaAutomatico));
                novaThread1.Start();
            }
            else
            {
                btnAcionamentoAutomatico.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.switchoff));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledgray));
                Thread novaThread1 = new Thread(new ThreadStart(ligaAutomatico));
                novaThread1.Start();
            }
        }
    }
}
