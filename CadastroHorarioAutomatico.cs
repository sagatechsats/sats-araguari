﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class CadastroHorarioAutomatico : Office2007Form
    {
        protected DBManager dbManager;

        public string idBomba { get; private set; }
        public CadastroHorarioAutomatico(String idBomba)
        {
            InitializeComponent();
            this.idBomba = idBomba;
        }

        private void CadastroHorarioAutomatico_Load(object sender, EventArgs e)
        {
            bttnSalvar.Enabled = true;
        }
        private void Salvar()
        {

            dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            bool resultbol = dbManager.Insert("ponto_horario", "id_bomba, hora_liga, hora_desliga",
                        "'" + idBomba.ToString() + "', '" + dateTimeLiga.Text.ToString() + "' ,'" + dateTimeDesliga.Text.ToString() + "'");


            bttnSalvar.Enabled = true;
            this.Close();
        }

        private void bttnSalvar_Click_1(object sender, EventArgs e)
        {
            bttnSalvar.Enabled = false;
            Salvar();
        }
    }
}
