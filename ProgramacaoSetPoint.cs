﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;
using System.Data;

namespace SATS
{
    //SetPoint
    public partial class ProgramacaoSetPoint : Office2007Form
    {
        protected DBManager dbManager;
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        public List<string> indexClientes { get; set; }
        private Thread novaThread;
        private int opcao = 0;
        List<string[]> res;
        List<string[]> res1;
        List<string> listBomba;
        private DataTable dt = new DataTable();

        public ProgramacaoSetPoint()
        {
            InitializeComponent();
            //dTLiga.Format = DateTimePickerFormat.Time;
            //dTLiga.ShowUpDown = true;
            //dTDesliga.Format = DateTimePickerFormat.Time;
            //dTDesliga.ShowUpDown = true;

            dt.Columns.Add("Codigo", typeof(string));
            dt.Columns.Add("SetPoint", typeof(string));
            dt.Columns.Add("HoraInicio", typeof(string)); //Hora de Ligar a Bomba
            dt.Columns.Add("HoraFim", typeof(string)); //Hora de Desligar a Bomba
            dt.Columns.Add("idHora", typeof(int)); 
            dgvTemporizacao.DataSource = dt;
            dgvTemporizacao.Columns["idHora"].Visible = false;
        }

        private void programaligamento_Load(object sender, EventArgs e)
        {
            novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();
           
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
                    Invoke((MethodInvoker)(() => PassListToComboBox(consulta)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(consulta)));

                    Invoke((MethodInvoker)(() => PassListToComboBox2(listBomba)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete2(listBomba)));
                    btnCadHSetPoint.Enabled = false;
                    btnAtualizarH.Enabled = false; 
                }
                catch (Exception)
                {
                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);

            cBbomba.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            cBbomba.Items.Clear();
            foreach (string concat in list)
                cBbomba.Items.Add(concat);
        }

        private void PassListToAutoComplete2(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);

            cBpressao.AutoCompleteCustomSource = stringCollection;
            //cBres2.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox2(List<string> list)
        {
            cBpressao.Items.Clear();
            foreach (string concat in list)
                cBpressao.Items.Add(concat);

            //cBres2.Items.Clear();
            //foreach (string concat in list)
            //    cBres2.Items.Add(concat);
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            listBomba = new List<string>();
            res = dbManager.Select2("ponto", "id_ponto, nome_ponto", "ponto_tipo='7'", "id_ponto");
            res1 = dbManager.Select2("ponto", "id_ponto, nome_ponto", "ponto_tipo='1' and inversor_ponto ='1'", "id_ponto");
            //res1 = dbManager.Select("ponto p inner join ponto_aciona pa on pa.id_ponto= p.id_ponto order by id_ponto asc", "p.id_ponto, nome_ponto", "");
            foreach (string[] item in res1)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                listConcat.Add(concat);
            }

            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                listBomba.Add(concat);
            }
            return listConcat;
        }

        private void bttnEditar_Click(object sender, EventArgs e)
        {
            int index = cBbomba.SelectedIndex;
            if (cBbomba.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione o Ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            enableComponets(true);
            bttnNovo.Enabled = false;
            bttnEditar.Enabled = false;
            btnCadHSetPoint.Enabled = true;
            btnCadHSetPoint.Enabled = true;
            btnAtualizarH.Enabled = true;
            //btAcionamento.Enabled = false;
            cBbomba.Enabled = false;
            opcao = 2;
            Thread novaThread = new Thread(new ParameterizedThreadStart(carregar));
            novaThread.Start(index);
        }


        private void enableComponets(bool choice)
        {
            checkBox.Enabled = choice;
            cBpressao.Enabled = choice;
            txtSetPoint.Enabled = choice;
            txtFreqMin.Enabled = choice;
            bttnSalvar.Enabled = choice;
            bttnCancelar.Enabled = choice;
            btnAtualizarH.Enabled = choice;
            btnCadHSetPoint.Enabled = choice;
            btnDelete.Enabled = choice;
            //dTLiga.Enabled = choice;
            //dTDesliga.Enabled = choice;
            txtFreqMax.Enabled = choice;
            txtGanhoProp.Enabled = choice;
            //ckResDependente.Enabled = choice;
            //ckTemporizacao.Enabled = choice;
            //ckBombaPoco.Enabled = choice;
            //cBres2.Enabled = choice;
            //txtMinRes2.Enabled = choice;
            groupBoxSemana.Enabled = choice;
        }

        private void carregar(object parametro)
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                int index = Convert.ToInt32(parametro);
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    List<string[]> result = dbManager.Select("ponto_pid", "*", "id_ponto='" + res1[index][0] + "'");
                    List<string[]> resultSemana = dbManager.Select("setpoint_semana", "*", "id_ponto='" + res1[index][0] + "'");
                    if (result.Count == 0)
                    {
                        MessageBox.Show("Dados não cadastrados!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Invoke((MethodInvoker)(() => limpa()));
                        Application.UseWaitCursor = false;
                        Application.DoEvents();
                        return;
                    }

                    for (int i = 0; i < res.Count(); i++)
                    {
                        if (res[i][0].CompareTo(result[0][1]) == 0)
                        {
                            Invoke((MethodInvoker)(() => cBpressao.SelectedIndex = i));
                            Invoke((MethodInvoker)(() => cBpressao.Text = res[i][0].ToString().PadLeft(6, '0') + " - " + res[i][1].ToString()));
                        }
                    }
                    if(resultSemana.Count() > 0)
                    {
                        Invoke((MethodInvoker)(() => textSun.Text = resultSemana[0][1].ToString()));
                        Invoke((MethodInvoker)(() => textMon.Text = resultSemana[0][2].ToString()));
                        Invoke((MethodInvoker)(() => textTue.Text = resultSemana[0][3].ToString()));
                        Invoke((MethodInvoker)(() => textWed.Text = resultSemana[0][4].ToString()));
                        Invoke((MethodInvoker)(() => textThu.Text = resultSemana[0][5].ToString()));
                        Invoke((MethodInvoker)(() => textFri.Text = resultSemana[0][6].ToString()));
                        Invoke((MethodInvoker)(() => textSat.Text = resultSemana[0][7].ToString()));
                    }
                    //for (int i = 0; i < res.Count(); i++)
                    //{
                    //    if (res[i][0].CompareTo(result[0][9]) == 0)
                    //    {
                    //        Invoke((MethodInvoker)(() => cBres2.SelectedIndex = i));
                    //        Invoke((MethodInvoker)(() => cBres2.Text = res[i][0].ToString().PadLeft(6, '0') + " - " + res[i][1].ToString()));
                    //        Invoke((MethodInvoker)(() => ckResDependente.Checked = true));
                    //        Invoke((MethodInvoker)(() => txtMinRes2.Text = result[0][10].ToString()));
                    //    }
                    //}
                    Invoke((MethodInvoker)(() => checkBox.Checked = Convert.ToBoolean(result[0][2])));
                    //Invoke((MethodInvoker)(() => dTLiga.Value = Convert.ToDateTime(result[0][3])));
                    //Invoke((MethodInvoker)(() => dTDesliga.Value = Convert.ToDateTime(result[0][4])));
                    Invoke((MethodInvoker)(() => txtSetPoint.Text = result[0][3].ToString()));
                    Invoke((MethodInvoker)(() => txtFreqMin.Text = result[0][4].ToString()));
                    Invoke((MethodInvoker)(() => txtFreqMax.Text = result[0][5].ToString()));
                    Invoke((MethodInvoker)(() => txtGanhoProp.Text = result[0][6].ToString()));

                    //Invoke((MethodInvoker)(() => ckTemporizacao.Checked = Convert.ToBoolean(result[0][11])));
                    //Invoke((MethodInvoker)(() => ckBombaPoco.Checked = Convert.ToBoolean(result[0][12])));
                    CarregaGrid(res1[index][0]);

                }
              
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void atualizar()
        {

            int indexbomba = 0;
            Invoke((MethodInvoker)(() => indexbomba = cBbomba.SelectedIndex));
            res1[indexbomba][0].ToString();

            CarregaGrid(res1[indexbomba][0]);

            btnAtualizarH.Enabled = true;


        }

        private void btnAtualizarH_Click_1(object sender, EventArgs e)
        {
            btnAtualizarH.Enabled = false;
            limparGrid();
            atualizar();

        }
        private void limparGrid()
        {
            dt = new DataTable();
            dt.Columns.Add("Codigo", typeof(string));
            dt.Columns.Add("SetPoint", typeof(string));
            dt.Columns.Add("HoraInicio", typeof(string)); //Hora de Ligar a Bomba
            dt.Columns.Add("HoraFim", typeof(string)); //Hora de Desligar a Bomba
            dt.Columns.Add("idHora", typeof(int));
            dgvTemporizacao.DataSource = dt;
            dgvTemporizacao.Columns["idHora"].Visible = false;
        }
        private void CarregaGrid(string idbomba)
        {
            dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            List<string[]> result = dbManager.Select("setpoint_horario", "*", "id_bomba='" + idbomba + "'");
            

            try
            {
                for(int i = 0; i< result.Count(); i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["Codigo"] = result[i][0].ToString();
                    dr["SetPoint"] = result[i][2].ToString();
                    dr["HoraInicio"] = result[i][3].ToString();
                    dr["HoraFim"] = result[i][4].ToString();
                    dr["idHora"] = Convert.ToInt32(result[i][0]);

                    dgvTemporizacao.Columns["idHora"].Visible = false;
                    dgvTemporizacao.DataSource = dt;
                    dt.Rows.Add(dr);
                }
            }
            catch (Exception)
            {

            }
        }

        private void limpa()
        {
            //dTLiga.Value = Convert.ToDateTime("00:00:00");
            //dTDesliga.Value = Convert.ToDateTime("00:00:00");
            cBbomba.SelectedIndex = -1;
            cBbomba.SelectedIndex = -1;
            cBbomba.Text = "";
            cBpressao.SelectedIndex = -1;
            cBpressao.Text = "";
            //cBres2.SelectedIndex = -1;
            //cBres2.Text = "";
            //txtMinRes2.Text = "";
            //ckResDependente.Checked = false;
            //ckTemporizacao.Checked = false;
            //ckBombaPoco.Checked = false;
            txtSetPoint.Text = "";
            txtFreqMin.Text = "";
            txtFreqMax.Text = "";
            txtGanhoProp.Text = "";
            checkBox.Checked = false;
            enableComponets(false);
            bttnNovo.Enabled = true;
            bttnEditar.Enabled = true;
            //btAcionamento.Enabled = true;
            cBbomba.Enabled = true;
            //dTLiga.Enabled = false;
            //dTDesliga.Enabled = false;
            opcao = 0;

            dt = new DataTable();
            dt.Columns.Add("Codigo", typeof(string));
            dt.Columns.Add("SetPoint", typeof(string));
            dt.Columns.Add("HoraInicio", typeof(string)); //Hora de Ligar a Bomba
            dt.Columns.Add("HoraFim", typeof(string)); //Hora de Desligar a Bomba
            dt.Columns.Add("idHora", typeof(int));
            dgvTemporizacao.DataSource = dt;
            dgvTemporizacao.Columns["idHora"].Visible = false;
            textMon.Text = textSun.Text = textTue.Text = textWed.Text = textThu.Text = textFri.Text = textSat.Text = "";
            
        }

        private void bttnCancelar_Click(object sender, EventArgs e)
        {
            limpa();
        }

        private void bttnNovo_Click(object sender, EventArgs e)
        {
            enableComponets(true);
            bttnNovo.Enabled = false;
            bttnEditar.Enabled = false;
            //btAcionamento.Enabled = false;
            opcao = 1;
        }

        private void bttnSalvar_Click(object sender, EventArgs e)
        {
            int index = cBpressao.SelectedIndex;
            if (cBbomba.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione o Ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (opcao == 1)
            {
                if (cBpressao.SelectedIndex < 0)
                {
                    MessageBox.Show("Selecione o Reservatório!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                Thread novaThread = new Thread(new ParameterizedThreadStart(inserir));
                novaThread.Start(index);
            }
            if (opcao == 2)
            {
                Thread novaThread = new Thread(new ParameterizedThreadStart(update));
                novaThread.Start(index);
            }
        }

        private void inserir(object parametro)
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                int indexbomba = 0;
                Invoke((MethodInvoker)(() => indexbomba = cBbomba.SelectedIndex));
                int index = Convert.ToInt32(parametro);

                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    bool resultbol = dbManager.Insert2("ponto_pid", "id_pressao, habilitado, id_ponto,  set_point, frequency_min, frequecy_max, ganho_proporcional",
                        "'" + res[index][0].ToString() + "', '" + Convert.ToInt32(checkBox.Checked) + "', '" + res1[indexbomba][0].ToString() + "', '" + txtSetPoint.Text.ToString() + "','" + txtFreqMin.Text.ToString() + "' ,'" + txtFreqMax.Text.ToString() + "' , '" + txtGanhoProp.Text.ToString() + "'");

                    if (resultbol != true)
                    {
                        MessageBox.Show("Dados Não cadastrados, verifique os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        MessageBox.Show("Dados cadastrados com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    Invoke((MethodInvoker)(() => limpa()));
                }
                catch (Exception)
                {
                    MessageBox.Show("Dados já cadastrados no Banco de Dados!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void update(object parametro)
        {
            if (IsConnected() == true)
            {
                int index = Convert.ToInt32(parametro);
                int indexbomba = 0;
                //int indexRes2 = 0;

                //string ResDependente = "0";
                //string minResDependente = "0.00";

                Invoke((MethodInvoker)(() => indexbomba = cBbomba.SelectedIndex));
                //Invoke((MethodInvoker)(() => indexRes2 = cBres2.SelectedIndex));

                //if (indexRes2 >= 0)
                //{
                //    ResDependente = res[indexRes2][0];
                //    minResDependente = txtMinRes2.Text.Replace(",", ".");
                //}

                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                { 
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    bool resultbol = dbManager.Update("ponto_pid", "habilitado= '" + Convert.ToInt32(checkBox.Checked) + "', id_pressao='" + res[index][0] + "', set_point='" + txtSetPoint.Text.Replace(",", ".") + "', frequecy_max='" + txtFreqMax.Text.Replace(",", ".") + "', ganho_proporcional='" + txtGanhoProp.Text.Replace(",", ".") + "', frequency_min='" + txtFreqMin.Text.Replace(",", ".") + "'", "id_ponto='" + res1[indexbomba][0].ToString() + "'");
                    bool resultbol2 = dbManager.Update("setpoint_semana", "sun='" + textSun.Text.Replace(",", ".") + "', mon='" + textMon.Text.Replace(",", ".") + "', tue='" + textTue.Text.Replace(",", ".") + "', wed='" + textWed.Text.Replace(",", ".") + "', thu='" + textThu.Text.Replace(",", ".") + "', fri='" + textFri.Text.Replace(",", ".") + "', sat='" + textSat.Text.Replace(",", ".") + "'", "id_ponto='" + res1[indexbomba][0].ToString() + "'");
                    updateHora();
                    if (resultbol == false)
                    {
                        MessageBox.Show("Erro ao editar dados!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Invoke((MethodInvoker)(() => limpa()));
                    }
                    else
                    {
                        MessageBox.Show("Dados alterados com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Invoke((MethodInvoker)(() => limpa()));
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show( e.ToString(),"errro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void updateHora()
        {
            foreach (DataGridViewRow row in dgvTemporizacao.Rows)
            {
                dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                bool resultbol = dbManager.Update("setpoint_horario", "hora_inicio= '" + Convert.ToString(row.Cells["HoraInicio"].Value) + "', hora_fim='" + Convert.ToString(row.Cells["HoraFim"].Value) + "', set_point='" + Convert.ToString(row.Cells["SetPoint"].Value).Replace(",",".") + "'", "id_hora='" + Convert.ToUInt32(row.Cells["idHora"].Value) + "'");
            }
            return;
        }


        private void txtVazao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

        }

        private void txtminFponta_TextChanged(object sender, EventArgs e)
        {

        }

        //private void cbResDependente_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ckResDependente.Checked == true)
        //        gbResDependente.Visible = true;
        //    else
        //        gbResDependente.Visible = false;
        //}

        //private void ckTemporizacao_CheckedChanged(object sender, EventArgs e)
        //{

        //}

        private void btAcionamento_Click(object sender, EventArgs e)
        {
            string[] searcharray = new string[10];
            int index = 0;
            FormPesquisa newForm = new FormPesquisa(ref searcharray);
            newForm.ShowDialog();
            try
            {
                List<string[]> result1 = dbManager.Select("ponto", "id_ponto", "ponto_tipo='" + 1 + "' order by id_ponto asc");
                for (int i = 0; i < result1.Count(); i++)
                {
                    if (result1[i][0] == searcharray[0].ToString())
                    {
                        index = i;
                        break;
                    }
                }
                // index = Convert.ToInt32(searcharray[0]);
                Invoke((MethodInvoker)(() => cBbomba.SelectedIndex = index));
                enableComponets(true);
                //btAcionamento.Enabled = false;
                bttnNovo.Enabled = false;
                bttnEditar.Enabled = false;
                //btAcionamento.Enabled = false;
                cBbomba.Enabled = false;
                opcao = 2;
                Thread novaThread = new Thread(new ParameterizedThreadStart(carregar));
                novaThread.Start(index);
            }
            catch
            {

            }

        }

        private void btnCadHSetPoint_Click(object sender, EventArgs e)
        {
            int indexbomba = 0;
            Invoke((MethodInvoker)(() => indexbomba = cBbomba.SelectedIndex));
            res1[indexbomba][0].ToString();
            CadastroHorarioSetPoint newform = new CadastroHorarioSetPoint(res1[indexbomba][0].ToString());
            newform.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
            Excluir(); 
        }
        private void Excluir()
        {
            
            string Codigo = dgvTemporizacao.CurrentRow.Cells[0].Value.ToString();

            dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            bool resultbol = dbManager.Delete("setpoint_horario", "id_hora=" + Convert.ToInt32(Codigo) + "");

            if (resultbol == true)
            {
                MessageBox.Show("Dados excluidos com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnDelete.Enabled = true;
            }

        }
    }
}