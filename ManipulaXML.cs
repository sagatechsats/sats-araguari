﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace SATS
{
    class ManipulaXML
    {
        public void GeraXML(List<string[]> ponto)
        {
            XmlTextWriter writer = new XmlTextWriter(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "pontos.xml", Encoding.UTF8);
            //inicia o documento xml
            writer.WriteStartDocument();
            //Usa a formatação
            writer.Formatting = Formatting.Indented;
            //Escreve o elemento raiz
            writer.WriteStartElement("pontos");
            //e sub-elementos
            
            for (int i = 0; i < ponto.Count; i++)
            {
                //Inicia um elemento
                writer.WriteStartElement("pontos");
                writer.WriteElementString("pontoDB", ponto[i][0].ToString());
                writer.WriteElementString("ponto", ponto[i][1].ToString());
                writer.WriteElementString("x", ponto[i][2].ToString());
                writer.WriteElementString("y", ponto[i][3].ToString());
                writer.WriteElementString("ponto_tipo", ponto[i][4].ToString());

                //encerra os elementos itens
                writer.WriteEndElement();
            }
            //escreve o XML para o arquivo e fecha o escritor
            writer.Close();
        }

        public List<string[]> LerXML()
        {
            List<string[]> result = new List<string[]>();
            List<string[]> resultWonull = new List<string[]>();

            if (System.IO.File.Exists(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "pontos.xml"))
            {
                System.IO.StreamReader Reader = new System.IO.StreamReader(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "pontos.xml", System.Text.Encoding.UTF8);
                XmlTextReader reader = new XmlTextReader(Reader);
                XmlNodeType type;
                int i = 0;
                result.Add(new string[5]);

                while (reader.Read())
                {
                    type = reader.NodeType;

                    if (type == XmlNodeType.Element)
                    {
                        if (reader.Name == "pontoDB")
                        {
                            reader.Read();
                            result[i][0] = reader.Value;
                        }

                        if (reader.Name == "ponto")
                        {
                            reader.Read();
                            result[i][1] = reader.Value;
                        }

                        if (reader.Name == "x")
                        {
                            reader.Read();
                            result[i][2] = reader.Value;
                        }
                        if (reader.Name == "y")
                        {
                            reader.Read();
                            result[i][3] = reader.Value;
                        }
                        if (reader.Name == "ponto_tipo")
                        {
                            reader.Read();
                            result[i][4] = reader.Value;
                            i++;
                            result.Add(new string[5]);
                        }
                    }
                }
                reader.Close();
                foreach (string[] item in result)
                {
                    if (item[0] != null) resultWonull.Add(item);
                }
            }
            return resultWonull;
        }

        //public List<string[]> LerXMLNivel()
        //{
        //    List<string[]> result = new List<string[]>();
        //    List<string[]> resultWonull = new List<string[]>();

        //    if (System.IO.File.Exists(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "pontos.xml"))
        //    {
        //        System.IO.StreamReader Reader = new System.IO.StreamReader(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "pontos.xml", System.Text.Encoding.UTF8);
        //        XmlTextReader reader = new XmlTextReader(Reader);
        //        XmlNodeType type;

        //        int i = 0;
        //        result.Add(new string[5]);

        //        while (reader.Read())
        //        {
        //            type = reader.NodeType;

        //            if (type == XmlNodeType.Element)
        //            {
                           
        //                if (reader.Name == "pontoDB")
        //                    {
        //                        reader.Read();
        //                        result[i][0] = reader.Value;
        //                    }

        //                    if (reader.Name == "ponto")
        //                    {
        //                        reader.Read();
        //                        result[i][1] = reader.Value;
        //                    }

        //                    if (reader.Name == "x")
        //                    {
        //                        reader.Read();
        //                        result[i][2] = reader.Value;
        //                    }
        //                    if (reader.Name == "y")
        //                    {
        //                        reader.Read();
        //                        result[i][3] = reader.Value;
        //                    }

        //                    if (reader.Name == "ponto_tipo")
        //                    {
        //                        reader.Read();
        //                        result[i][4] = reader.Value;
        //                        i++;
        //                        result.Add(new string[5]);
        //                }
        //            }
        //        }

        //        reader.Close();
        //        foreach (string[] item in result)
        //        {
        //            if (item[0] != null) resultWonull.Add(item);
        //        }
        //    }
        //    return resultWonull;
        //}

        public List<string[]> DeletePonto(string ponto, List<string[]> pontos)
        {
            bool addp = false;
            List<string[]> result = new List<string[]>();
            foreach (string[] item in pontos)
            {
                if (item[0].ToString() != ponto || addp == true)
                {
                    result.Add(item);
                }
                else addp = true;
            }
            return result;
        }
    }
}
