﻿using System.Net.NetworkInformation;

namespace SATS
{
    public class InternetConnectionVerifier
    {
        private bool PingSite(string url)
        {
            var ping = new Ping();
            try
            {
                var reply1 = ping.Send(url);
            }
            catch (PingException)
            {
                return false;
            }
            return true;
        }

        public bool CheckInternet()
        {
            // Se esses tres estiverem fora do ar, a internet entrou em colapso
            if (!PingSite("google.com") && !PingSite("bing.com") && !PingSite("yahoo.com"))
                return false;
            else
                return true;
        }
        public bool CheckSaga() { return PingSite("araguariserver.ddns.net"); }
    }
}
