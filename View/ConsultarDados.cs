﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class ConsultarDados : Office2007Form
    {
        protected DBManager dbManager;
        [DllImport("wininet.dll")]

        private extern static Boolean InternetGetConnectedState(out int Description, int ReservedValue);
        public ConsultarDados()
        {
            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();
        }

        public class drop
        {
            public drop(string nome, int valor)
            {
                Nome = nome;
                Valor = valor;
            }
            string _nome;
            int _valor;
            public int Valor
            {
                get
                {
                    return _valor;
                }
                set
                {
                    _valor = value;
                }
            }
            public string Nome
            {
                get
                {
                    return _nome;
                }
                set
                {
                    _nome = value;
                }
            }
        }

        private void consultar_Click(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar-se um ponto.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            String cliente = comboBox1.Text; //Pega o valor selecionado no combobox de cliente 
            Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTabela));
            novaThread.Start(cliente);
        }

        private void preencheList(List<string[]> consultalist)
        {
            listView1.Items.Clear();
            foreach (string[] linha in consultalist)
            {
                string[] subitems = new string[3];
                object[] o = linha;
                for (int i = 0; i < 3; i++)
                {
                    subitems[i] = o[i].ToString();
                }
                ListViewItem item = new ListViewItem(subitems);
                listView1.Items.Add(item); //carrega os itens no list view
            }
            label1.Text = "Quantidade de dados: " + consultalist.Count; //escreve na label a quantidade de dados exibidos no listview
            this.Controls.Add(listView1);
        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();

                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
                    Invoke((MethodInvoker)(() => PassListToComboBox(consulta)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(consulta)));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select("ponto order by id_ponto ASC", "id_ponto, nome_ponto", "");
            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];

                listConcat.Add(concat);
            }
            return listConcat;
        }

        private void CarregaTabela(object parametro)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            try
            {
                String cliente = parametro.ToString();
                string[] words = cliente.Split('-'); //Separa o id do nome do cliente   
                DateTime result = dateTimePicker1.Value;
                String data = result.ToString("yyyy-MM-dd "); //pega a data escolhida pelo usuário
                String hora = maskedTextBox1.Text; // pega a hora escolhida pelo usuário
                String datahora = String.Concat(data, hora); //concatena data e hora conforme usado no banco para poder fazer a consulta 
                dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                ArrayList arr = new ArrayList();
                List<string[]> consultalist;
                System.Diagnostics.Debug.WriteLine("*****************" + hora);

                //if (Int32.Parse(words[0]) == 1)
                //{

                //}
                if (hora == "  :") //consulta se o horário não for preenchdio
                {
                    consultalist = dbManager.Select("ponto_leitura", "Round(volume/1000,2), Round(vazao/1000,2), data_hora", "id_ponto=" + words[0] + " and data_hora between '" + data + "00:00:00' and '" + data + "23:59:59' order by data_hora");
                    System.Diagnostics.Debug.WriteLine("id_ponto=" + words[0] + " and data_hora between '" + data + "00:00:01' and '" + data + "23:59:59' order by data_hora");
                }
                else
                {
                    consultalist = dbManager.Select("ponto_leitura", "Round(volume/1000,2), Round(vazao/1000,2), data_hora", "id_ponto=" + words[0] + " and data_hora between '" + datahora + ":00' and '" + datahora + ":59' order by data_hora");

                }
                Invoke((MethodInvoker)(() => preencheList(consultalist)));
            }
            catch (Exception)
            {
            }
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
            comboBox1.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            comboBox1.Items.Clear();
            foreach (string concat in list)
                comboBox1.Items.Add(concat);
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public List<string> indexClientes { get; set; }
    }
}
