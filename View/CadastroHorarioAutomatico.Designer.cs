﻿namespace SATS
{
    partial class CadastroHorarioAutomatico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroHorarioAutomatico));
            this.grupHorarioSetPoint = new System.Windows.Forms.GroupBox();
            this.dateTimeLiga = new System.Windows.Forms.DateTimePicker();
            this.lblHoraFinal = new System.Windows.Forms.Label();
            this.dateTimeDesliga = new System.Windows.Forms.DateTimePicker();
            this.lblHoraInicial = new System.Windows.Forms.Label();
            this.lblSetPoint = new System.Windows.Forms.Label();
            this.bttnSalvar = new System.Windows.Forms.Button();
            this.grupHorarioSetPoint.SuspendLayout();
            this.SuspendLayout();
            // 
            // grupHorarioSetPoint
            // 
            this.grupHorarioSetPoint.Controls.Add(this.dateTimeLiga);
            this.grupHorarioSetPoint.Controls.Add(this.lblHoraFinal);
            this.grupHorarioSetPoint.Controls.Add(this.dateTimeDesliga);
            this.grupHorarioSetPoint.Controls.Add(this.lblHoraInicial);
            this.grupHorarioSetPoint.Controls.Add(this.lblSetPoint);
            this.grupHorarioSetPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupHorarioSetPoint.Location = new System.Drawing.Point(12, 12);
            this.grupHorarioSetPoint.Name = "grupHorarioSetPoint";
            this.grupHorarioSetPoint.Size = new System.Drawing.Size(311, 117);
            this.grupHorarioSetPoint.TabIndex = 14;
            this.grupHorarioSetPoint.TabStop = false;
            this.grupHorarioSetPoint.Text = "Horário programação automatica:";
            // 
            // dateTimeLiga
            // 
            this.dateTimeLiga.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimeLiga.Location = new System.Drawing.Point(44, 71);
            this.dateTimeLiga.Name = "dateTimeLiga";
            this.dateTimeLiga.Size = new System.Drawing.Size(92, 21);
            this.dateTimeLiga.TabIndex = 1;
            // 
            // lblHoraFinal
            // 
            this.lblHoraFinal.AutoSize = true;
            this.lblHoraFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraFinal.Location = new System.Drawing.Point(163, 53);
            this.lblHoraFinal.Name = "lblHoraFinal";
            this.lblHoraFinal.Size = new System.Drawing.Size(78, 15);
            this.lblHoraFinal.TabIndex = 4;
            this.lblHoraFinal.Text = "Hora Final:";
            // 
            // dateTimeDesliga
            // 
            this.dateTimeDesliga.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimeDesliga.Location = new System.Drawing.Point(166, 71);
            this.dateTimeDesliga.Name = "dateTimeDesliga";
            this.dateTimeDesliga.Size = new System.Drawing.Size(98, 21);
            this.dateTimeDesliga.TabIndex = 0;
            // 
            // lblHoraInicial
            // 
            this.lblHoraInicial.AutoSize = true;
            this.lblHoraInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraInicial.Location = new System.Drawing.Point(41, 53);
            this.lblHoraInicial.Name = "lblHoraInicial";
            this.lblHoraInicial.Size = new System.Drawing.Size(85, 15);
            this.lblHoraInicial.TabIndex = 3;
            this.lblHoraInicial.Text = "Hora Inicial:";
            // 
            // lblSetPoint
            // 
            this.lblSetPoint.AutoSize = true;
            this.lblSetPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetPoint.Location = new System.Drawing.Point(66, 53);
            this.lblSetPoint.Name = "lblSetPoint";
            this.lblSetPoint.Size = new System.Drawing.Size(0, 15);
            this.lblSetPoint.TabIndex = 2;
            // 
            // bttnSalvar
            // 
            this.bttnSalvar.Enabled = false;
            this.bttnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnSalvar.Image = ((System.Drawing.Image)(resources.GetObject("bttnSalvar.Image")));
            this.bttnSalvar.Location = new System.Drawing.Point(118, 160);
            this.bttnSalvar.Name = "bttnSalvar";
            this.bttnSalvar.Size = new System.Drawing.Size(97, 35);
            this.bttnSalvar.TabIndex = 13;
            this.bttnSalvar.Text = "Salvar";
            this.bttnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnSalvar.UseVisualStyleBackColor = true;
            this.bttnSalvar.Click += new System.EventHandler(this.bttnSalvar_Click_1);
            // 
            // CadastroHorarioAutomatico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 207);
            this.Controls.Add(this.grupHorarioSetPoint);
            this.Controls.Add(this.bttnSalvar);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastroHorarioAutomatico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro Horario Automatico";
            this.Load += new System.EventHandler(this.CadastroHorarioAutomatico_Load);
            this.grupHorarioSetPoint.ResumeLayout(false);
            this.grupHorarioSetPoint.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grupHorarioSetPoint;
        private System.Windows.Forms.DateTimePicker dateTimeLiga;
        private System.Windows.Forms.Label lblHoraFinal;
        private System.Windows.Forms.DateTimePicker dateTimeDesliga;
        private System.Windows.Forms.Label lblHoraInicial;
        private System.Windows.Forms.Label lblSetPoint;
        private System.Windows.Forms.Button bttnSalvar;
    }
}