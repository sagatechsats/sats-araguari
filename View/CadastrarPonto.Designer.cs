﻿namespace SATS
{
    partial class CadastrarPonto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastrarPonto));
            this.txtPonto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMaximo = new System.Windows.Forms.Label();
            this.lblPorta = new System.Windows.Forms.Label();
            this.cbPorta = new System.Windows.Forms.ComboBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDns = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtEnderecoInversor = new System.Windows.Forms.TextBox();
            this.lblInvB = new System.Windows.Forms.Label();
            this.txtEnderecoMod = new System.Windows.Forms.TextBox();
            this.lblMod = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ckNivel = new System.Windows.Forms.RadioButton();
            this.ckInversor = new System.Windows.Forms.RadioButton();
            this.ckBombaSomente = new System.Windows.Forms.CheckBox();
            this.ckPressao = new System.Windows.Forms.RadioButton();
            this.ckVazao = new System.Windows.Forms.RadioButton();
            this.ckTurbidez = new System.Windows.Forms.RadioButton();
            this.ckBombaMedidor = new System.Windows.Forms.CheckBox();
            this.ckMultK = new System.Windows.Forms.RadioButton();
            this.ckPh = new System.Windows.Forms.RadioButton();
            this.gBbomba = new System.Windows.Forms.GroupBox();
            this.cBfFase = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cBpbomba = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gBnivel = new System.Windows.Forms.GroupBox();
            this.txtNmin = new System.Windows.Forms.TextBox();
            this.lblNmin = new System.Windows.Forms.Label();
            this.lbTipoVazao = new System.Windows.Forms.Label();
            this.cbTipo = new System.Windows.Forms.ComboBox();
            this.txtNmax = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bttnEditar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cBPonto = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btSalvar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gBbomba.SuspendLayout();
            this.gBnivel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPonto
            // 
            this.txtPonto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPonto.Location = new System.Drawing.Point(175, 21);
            this.txtPonto.Margin = new System.Windows.Forms.Padding(4);
            this.txtPonto.MaxLength = 30;
            this.txtPonto.Name = "txtPonto";
            this.txtPonto.Size = new System.Drawing.Size(414, 22);
            this.txtPonto.TabIndex = 3;
            this.txtPonto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPonto_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(62, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Nome do Ponto:";
            // 
            // lblMaximo
            // 
            this.lblMaximo.AutoSize = true;
            this.lblMaximo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaximo.Location = new System.Drawing.Point(3, 60);
            this.lblMaximo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaximo.Name = "lblMaximo";
            this.lblMaximo.Size = new System.Drawing.Size(92, 16);
            this.lblMaximo.TabIndex = 9;
            this.lblMaximo.Text = "Nível Máximo:";
            // 
            // lblPorta
            // 
            this.lblPorta.AutoSize = true;
            this.lblPorta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPorta.Location = new System.Drawing.Point(3, 25);
            this.lblPorta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPorta.Name = "lblPorta";
            this.lblPorta.Size = new System.Drawing.Size(74, 16);
            this.lblPorta.TabIndex = 10;
            this.lblPorta.Text = "Porta nível:";
            // 
            // cbPorta
            // 
            this.cbPorta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPorta.FormattingEnabled = true;
            this.cbPorta.Location = new System.Drawing.Point(132, 25);
            this.cbPorta.Margin = new System.Windows.Forms.Padding(4);
            this.cbPorta.Name = "cbPorta";
            this.cbPorta.Size = new System.Drawing.Size(126, 24);
            this.cbPorta.TabIndex = 15;
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(175, 51);
            this.txtNome.Margin = new System.Windows.Forms.Padding(4);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(414, 22);
            this.txtNome.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 16);
            this.label4.TabIndex = 20;
            this.label4.Text = "Nome de Apresentação:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(118, 84);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 16);
            this.label6.TabIndex = 22;
            this.label6.Text = "DDNS:";
            // 
            // txtDns
            // 
            this.txtDns.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtDns.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDns.Location = new System.Drawing.Point(176, 81);
            this.txtDns.Margin = new System.Windows.Forms.Padding(4);
            this.txtDns.MaxLength = 30;
            this.txtDns.Name = "txtDns";
            this.txtDns.Size = new System.Drawing.Size(413, 22);
            this.txtDns.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.gBbomba);
            this.groupBox1.Controls.Add(this.gBnivel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtPonto);
            this.groupBox1.Controls.Add(this.txtDns);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtNome);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(924, 426);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cadastrar Ponto";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button4);
            this.groupBox5.Location = new System.Drawing.Point(630, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(281, 79);
            this.groupBox5.TabIndex = 36;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Cadastro Infomações do Ponto";
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::SATS.Properties.Resources.Crystal_Clear_action_editpaste;
            this.button4.Location = new System.Drawing.Point(113, 30);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(115, 37);
            this.button4.TabIndex = 28;
            this.button4.Text = "Infomações";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtEnderecoInversor);
            this.groupBox4.Controls.Add(this.lblInvB);
            this.groupBox4.Controls.Add(this.txtEnderecoMod);
            this.groupBox4.Controls.Add(this.lblMod);
            this.groupBox4.Location = new System.Drawing.Point(581, 225);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(330, 177);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Informações Módulo";
            // 
            // txtEnderecoInversor
            // 
            this.txtEnderecoInversor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEnderecoInversor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnderecoInversor.Location = new System.Drawing.Point(183, 62);
            this.txtEnderecoInversor.Margin = new System.Windows.Forms.Padding(4);
            this.txtEnderecoInversor.MaxLength = 10;
            this.txtEnderecoInversor.Name = "txtEnderecoInversor";
            this.txtEnderecoInversor.Size = new System.Drawing.Size(126, 22);
            this.txtEnderecoInversor.TabIndex = 22;
            // 
            // lblInvB
            // 
            this.lblInvB.AutoSize = true;
            this.lblInvB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvB.Location = new System.Drawing.Point(7, 65);
            this.lblInvB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInvB.Name = "lblInvB";
            this.lblInvB.Size = new System.Drawing.Size(133, 16);
            this.lblInvB.TabIndex = 15;
            this.lblInvB.Text = "End Inversor Bomba:";
            // 
            // txtEnderecoMod
            // 
            this.txtEnderecoMod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEnderecoMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnderecoMod.Location = new System.Drawing.Point(183, 35);
            this.txtEnderecoMod.Margin = new System.Windows.Forms.Padding(4);
            this.txtEnderecoMod.MaxLength = 10;
            this.txtEnderecoMod.Name = "txtEnderecoMod";
            this.txtEnderecoMod.Size = new System.Drawing.Size(126, 22);
            this.txtEnderecoMod.TabIndex = 21;
            // 
            // lblMod
            // 
            this.lblMod.AutoSize = true;
            this.lblMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMod.Location = new System.Drawing.Point(7, 35);
            this.lblMod.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMod.Name = "lblMod";
            this.lblMod.Size = new System.Drawing.Size(83, 16);
            this.lblMod.TabIndex = 13;
            this.lblMod.Text = "End Modulo:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ckNivel);
            this.groupBox3.Controls.Add(this.ckInversor);
            this.groupBox3.Controls.Add(this.ckBombaSomente);
            this.groupBox3.Controls.Add(this.ckPressao);
            this.groupBox3.Controls.Add(this.ckVazao);
            this.groupBox3.Controls.Add(this.ckTurbidez);
            this.groupBox3.Controls.Add(this.ckBombaMedidor);
            this.groupBox3.Controls.Add(this.ckMultK);
            this.groupBox3.Controls.Add(this.ckPh);
            this.groupBox3.Location = new System.Drawing.Point(17, 119);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(894, 72);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipo Ponto";
            // 
            // ckNivel
            // 
            this.ckNivel.AutoSize = true;
            this.ckNivel.Checked = true;
            this.ckNivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckNivel.Location = new System.Drawing.Point(88, 21);
            this.ckNivel.Name = "ckNivel";
            this.ckNivel.Size = new System.Drawing.Size(60, 20);
            this.ckNivel.TabIndex = 7;
            this.ckNivel.TabStop = true;
            this.ckNivel.Text = "Nível:";
            this.ckNivel.UseVisualStyleBackColor = true;
            this.ckNivel.CheckedChanged += new System.EventHandler(this.ckNivel_CheckedChanged);
            // 
            // ckInversor
            // 
            this.ckInversor.AutoSize = true;
            this.ckInversor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckInversor.Location = new System.Drawing.Point(356, 23);
            this.ckInversor.Name = "ckInversor";
            this.ckInversor.Size = new System.Drawing.Size(74, 20);
            this.ckInversor.TabIndex = 9;
            this.ckInversor.Text = "Inversor";
            this.ckInversor.UseVisualStyleBackColor = true;
            this.ckInversor.CheckedChanged += new System.EventHandler(this.ckInversor_CheckedChanged);
            // 
            // ckBombaSomente
            // 
            this.ckBombaSomente.AutoSize = true;
            this.ckBombaSomente.Location = new System.Drawing.Point(613, 47);
            this.ckBombaSomente.Name = "ckBombaSomente";
            this.ckBombaSomente.Size = new System.Drawing.Size(145, 20);
            this.ckBombaSomente.TabIndex = 14;
            this.ckBombaSomente.Text = "Somente Bomba:";
            this.ckBombaSomente.UseVisualStyleBackColor = true;
            this.ckBombaSomente.CheckedChanged += new System.EventHandler(this.ckBombaSomente_CheckedChanged);
            // 
            // ckPressao
            // 
            this.ckPressao.AutoSize = true;
            this.ckPressao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckPressao.Location = new System.Drawing.Point(232, 22);
            this.ckPressao.Name = "ckPressao";
            this.ckPressao.Size = new System.Drawing.Size(80, 20);
            this.ckPressao.TabIndex = 8;
            this.ckPressao.Text = "Pressão:";
            this.ckPressao.UseVisualStyleBackColor = true;
            this.ckPressao.CheckedChanged += new System.EventHandler(this.ckPressao_CheckedChanged);
            // 
            // ckVazao
            // 
            this.ckVazao.AutoSize = true;
            this.ckVazao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVazao.Location = new System.Drawing.Point(160, 21);
            this.ckVazao.Name = "ckVazao";
            this.ckVazao.Size = new System.Drawing.Size(68, 20);
            this.ckVazao.TabIndex = 8;
            this.ckVazao.Text = "Vazão:";
            this.ckVazao.UseVisualStyleBackColor = true;
            this.ckVazao.CheckedChanged += new System.EventHandler(this.ckVazao_CheckedChanged);
            // 
            // ckTurbidez
            // 
            this.ckTurbidez.AutoSize = true;
            this.ckTurbidez.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckTurbidez.Location = new System.Drawing.Point(232, 42);
            this.ckTurbidez.Name = "ckTurbidez";
            this.ckTurbidez.Size = new System.Drawing.Size(82, 20);
            this.ckTurbidez.TabIndex = 12;
            this.ckTurbidez.Text = "Turbidez:";
            this.ckTurbidez.UseVisualStyleBackColor = true;
            this.ckTurbidez.CheckedChanged += new System.EventHandler(this.ckTurbidez_CheckedChanged);
            // 
            // ckBombaMedidor
            // 
            this.ckBombaMedidor.AutoSize = true;
            this.ckBombaMedidor.Location = new System.Drawing.Point(613, 21);
            this.ckBombaMedidor.Name = "ckBombaMedidor";
            this.ckBombaMedidor.Size = new System.Drawing.Size(132, 20);
            this.ckBombaMedidor.TabIndex = 13;
            this.ckBombaMedidor.Text = "Bomba + Nível:";
            this.ckBombaMedidor.UseVisualStyleBackColor = true;
            this.ckBombaMedidor.CheckedChanged += new System.EventHandler(this.ckBombaMedidor_CheckedChanged);
            // 
            // ckMultK
            // 
            this.ckMultK.AutoSize = true;
            this.ckMultK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckMultK.Location = new System.Drawing.Point(88, 42);
            this.ckMultK.Name = "ckMultK";
            this.ckMultK.Size = new System.Drawing.Size(61, 20);
            this.ckMultK.TabIndex = 10;
            this.ckMultK.Text = "MultK:";
            this.ckMultK.UseVisualStyleBackColor = true;
            this.ckMultK.CheckedChanged += new System.EventHandler(this.ckMultK_CheckedChanged);
            // 
            // ckPh
            // 
            this.ckPh.AutoSize = true;
            this.ckPh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckPh.Location = new System.Drawing.Point(161, 42);
            this.ckPh.Name = "ckPh";
            this.ckPh.Size = new System.Drawing.Size(47, 20);
            this.ckPh.TabIndex = 11;
            this.ckPh.Text = "pH:";
            this.ckPh.UseVisualStyleBackColor = true;
            this.ckPh.CheckedChanged += new System.EventHandler(this.ckPh_CheckedChanged);
            // 
            // gBbomba
            // 
            this.gBbomba.Controls.Add(this.cBfFase);
            this.gBbomba.Controls.Add(this.label9);
            this.gBbomba.Controls.Add(this.cBpbomba);
            this.gBbomba.Controls.Add(this.label5);
            this.gBbomba.Controls.Add(this.label7);
            this.gBbomba.Enabled = false;
            this.gBbomba.Location = new System.Drawing.Point(287, 225);
            this.gBbomba.Name = "gBbomba";
            this.gBbomba.Size = new System.Drawing.Size(283, 177);
            this.gBbomba.TabIndex = 25;
            this.gBbomba.TabStop = false;
            this.gBbomba.Text = "Informações Bomba:";
            // 
            // cBfFase
            // 
            this.cBfFase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBfFase.FormattingEnabled = true;
            this.cBfFase.Items.AddRange(new object[] {
            "Porta 0",
            "Porta 1",
            "Porta 2",
            "Porta 3",
            "Porta 4",
            "Porta 5",
            "Porta 6",
            "Porta 7",
            "Porta 8"});
            this.cBfFase.Location = new System.Drawing.Point(86, 54);
            this.cBfFase.Margin = new System.Windows.Forms.Padding(4);
            this.cBfFase.Name = "cBfFase";
            this.cBfFase.Size = new System.Drawing.Size(129, 24);
            this.cBfFase.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 46);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 32);
            this.label9.TabIndex = 12;
            this.label9.Text = "Porta falta \r\nde fase:";
            // 
            // cBpbomba
            // 
            this.cBpbomba.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBpbomba.FormattingEnabled = true;
            this.cBpbomba.Items.AddRange(new object[] {
            "Porta 0",
            "Porta 1",
            "Porta 2",
            "Porta 3",
            "Porta 4",
            "Porta 5",
            "Porta 6",
            "Porta 7",
            "Porta 8"});
            this.cBpbomba.Location = new System.Drawing.Point(104, 22);
            this.cBpbomba.Margin = new System.Windows.Forms.Padding(4);
            this.cBpbomba.Name = "cBpbomba";
            this.cBpbomba.Size = new System.Drawing.Size(111, 24);
            this.cBpbomba.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 25);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Porta bomba:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 57);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 16);
            this.label7.TabIndex = 9;
            // 
            // gBnivel
            // 
            this.gBnivel.Controls.Add(this.txtNmin);
            this.gBnivel.Controls.Add(this.lblNmin);
            this.gBnivel.Controls.Add(this.lbTipoVazao);
            this.gBnivel.Controls.Add(this.cbPorta);
            this.gBnivel.Controls.Add(this.cbTipo);
            this.gBnivel.Controls.Add(this.lblPorta);
            this.gBnivel.Controls.Add(this.txtNmax);
            this.gBnivel.Controls.Add(this.lblMaximo);
            this.gBnivel.Location = new System.Drawing.Point(6, 225);
            this.gBnivel.Name = "gBnivel";
            this.gBnivel.Size = new System.Drawing.Size(275, 177);
            this.gBnivel.TabIndex = 23;
            this.gBnivel.TabStop = false;
            this.gBnivel.Text = "Informações Nível:";
            // 
            // txtNmin
            // 
            this.txtNmin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNmin.Location = new System.Drawing.Point(132, 87);
            this.txtNmin.Margin = new System.Windows.Forms.Padding(4);
            this.txtNmin.MaxLength = 10;
            this.txtNmin.Name = "txtNmin";
            this.txtNmin.Size = new System.Drawing.Size(126, 22);
            this.txtNmin.TabIndex = 17;
            // 
            // lblNmin
            // 
            this.lblNmin.AutoSize = true;
            this.lblNmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNmin.Location = new System.Drawing.Point(3, 90);
            this.lblNmin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNmin.Name = "lblNmin";
            this.lblNmin.Size = new System.Drawing.Size(88, 16);
            this.lblNmin.TabIndex = 14;
            this.lblNmin.Text = "Nível Minimo:";
            // 
            // lbTipoVazao
            // 
            this.lbTipoVazao.AutoSize = true;
            this.lbTipoVazao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTipoVazao.Location = new System.Drawing.Point(3, 123);
            this.lbTipoVazao.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTipoVazao.Name = "lbTipoVazao";
            this.lbTipoVazao.Size = new System.Drawing.Size(100, 16);
            this.lbTipoVazao.TabIndex = 12;
            this.lbTipoVazao.Text = "Tipo de Vazão:";
            // 
            // cbTipo
            // 
            this.cbTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipo.FormattingEnabled = true;
            this.cbTipo.Items.AddRange(new object[] {
            "Analógica",
            "Pulsada"});
            this.cbTipo.Location = new System.Drawing.Point(132, 120);
            this.cbTipo.Margin = new System.Windows.Forms.Padding(4);
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Size = new System.Drawing.Size(126, 24);
            this.cbTipo.TabIndex = 18;
            // 
            // txtNmax
            // 
            this.txtNmax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNmax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNmax.Location = new System.Drawing.Point(132, 57);
            this.txtNmax.Margin = new System.Windows.Forms.Padding(4);
            this.txtNmax.MaxLength = 10;
            this.txtNmax.Name = "txtNmax";
            this.txtNmax.Size = new System.Drawing.Size(126, 22);
            this.txtNmax.TabIndex = 16;
            this.txtNmax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHidrometro_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bttnEditar);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cBPonto);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(924, 87);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Selecione Ponto";
            // 
            // bttnEditar
            // 
            this.bttnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnEditar.Image = global::SATS.Properties.Resources.edit_button;
            this.bttnEditar.Location = new System.Drawing.Point(743, 33);
            this.bttnEditar.Name = "bttnEditar";
            this.bttnEditar.Size = new System.Drawing.Size(107, 35);
            this.bttnEditar.TabIndex = 2;
            this.bttnEditar.Text = "Editar";
            this.bttnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnEditar.UseVisualStyleBackColor = true;
            this.bttnEditar.Click += new System.EventHandler(this.bttnEditar_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(119, 42);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 16);
            this.label8.TabIndex = 9;
            this.label8.Text = "Ponto:";
            // 
            // cBPonto
            // 
            this.cBPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBPonto.FormattingEnabled = true;
            this.cBPonto.Location = new System.Drawing.Point(172, 38);
            this.cBPonto.Name = "cBPonto";
            this.cBPonto.Size = new System.Drawing.Size(417, 24);
            this.cBPonto.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::SATS.Properties.Resources.broom;
            this.button1.Location = new System.Drawing.Point(341, 552);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 37);
            this.button1.TabIndex = 25;
            this.button1.Text = "Limpar";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = global::SATS.Properties.Resources.Action_delete_icon;
            this.btnDelete.Location = new System.Drawing.Point(466, 551);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(110, 38);
            this.btnDelete.TabIndex = 26;
            this.btnDelete.Text = "Deletar";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.button3_Click);
            // 
            // btSalvar
            // 
            this.btSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSalvar.Image = global::SATS.Properties.Resources.rsz_crystal_clear_device_floppy_unmount;
            this.btSalvar.Location = new System.Drawing.Point(216, 552);
            this.btSalvar.Margin = new System.Windows.Forms.Padding(4);
            this.btSalvar.Name = "btSalvar";
            this.btSalvar.Size = new System.Drawing.Size(106, 37);
            this.btSalvar.TabIndex = 24;
            this.btSalvar.Text = "Salvar";
            this.btSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSalvar.UseVisualStyleBackColor = true;
            this.btSalvar.Click += new System.EventHandler(this.btSalvar_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::SATS.Properties.Resources.Crystal_Clear_action_button_cancel;
            this.button2.Location = new System.Drawing.Point(593, 552);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 37);
            this.button2.TabIndex = 27;
            this.button2.Text = "Cancelar";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovo.Image = global::SATS.Properties.Resources.Crystal_Clear_action_edit_add;
            this.btnNovo.Location = new System.Drawing.Point(90, 551);
            this.btnNovo.Margin = new System.Windows.Forms.Padding(4);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(106, 37);
            this.btnNovo.TabIndex = 23;
            this.btnNovo.Text = "Novo";
            this.btnNovo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // CadastrarPonto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(944, 602);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btSalvar);
            this.Controls.Add(this.button2);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(629, 451);
            this.Name = "CadastrarPonto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro Ponto";
            this.Load += new System.EventHandler(this.CadastrarPonto_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gBbomba.ResumeLayout(false);
            this.gBbomba.PerformLayout();
            this.gBnivel.ResumeLayout(false);
            this.gBnivel.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtPonto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMaximo;
        private System.Windows.Forms.Label lblPorta;
        private System.Windows.Forms.Button btSalvar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cbPorta;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDns;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cBPonto;
        private System.Windows.Forms.TextBox txtNmax;
        private System.Windows.Forms.GroupBox gBnivel;
        private System.Windows.Forms.CheckBox ckBombaSomente;
        private System.Windows.Forms.GroupBox gBbomba;
        private System.Windows.Forms.ComboBox cBpbomba;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton ckNivel;
        private System.Windows.Forms.RadioButton ckVazao;
        private System.Windows.Forms.ComboBox cBfFase;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox ckBombaMedidor;
        private System.Windows.Forms.RadioButton ckPh;
        private System.Windows.Forms.RadioButton ckMultK;
        private System.Windows.Forms.Label lbTipoVazao;
        private System.Windows.Forms.ComboBox cbTipo;
        private System.Windows.Forms.RadioButton ckTurbidez;
        private System.Windows.Forms.RadioButton ckPressao;
        private System.Windows.Forms.RadioButton ckInversor;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblMod;
        private System.Windows.Forms.TextBox txtEnderecoMod;
        private System.Windows.Forms.TextBox txtEnderecoInversor;
        private System.Windows.Forms.Label lblInvB;
        private System.Windows.Forms.Button bttnEditar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtNmin;
        private System.Windows.Forms.Label lblNmin;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnNovo;
    }
}