﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class ConsultarEventos : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        protected DBManager dbManager;
        public ConsultarEventos()
        {
            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
        }

        private void consultareventos_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();
            ArrayList evento = new ArrayList();
            evento.Add(new drop("Todos", 1));
            evento.Add(new drop("Sugestão de Vazamento", 2));
            evento.Add(new drop("Vazão muito Baixa", 3));
            evento.Add(new drop("Vazão Alta", 4));
            evento.Add(new drop("Rede de Celular", 5));
            comboBox2.DataSource = evento;
            comboBox2.DisplayMember = "nome";
            comboBox2.ValueMember = "valor";
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }

            catch
            {
                result = false;
            }
            return result;
        }

        public class drop
        {
            public drop(string nome, int valor)
            {
                Nome = nome;
                Valor = valor;
            }
            string _nome;
            int _valor;
            public int Valor
            {
                get
                {
                    return _valor;
                }
                set
                {
                    _valor = value;
                }
            }
            public string Nome
            {
                get
                {
                    return _nome;
                }
                set
                {
                    _nome = value;
                }
            }
        }



        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
                    Invoke((MethodInvoker)(() => PassListToComboBox(consulta)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(consulta)));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar-se um ponto.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            String cliente = comboBox1.Text; //Pega o valor selecionado no combobox de cliente   
            Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTabela));
            novaThread.Start(cliente);
        }

        private void CarregaTabela(object parametro)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            try
            {
                String tipodeevento = "Todos";
                String cliente = parametro.ToString();
                string[] words = cliente.Split('-'); //Separa o id do nome do cliente        
                DateTime result = dateTimePicker1.Value;
                string data = result.ToString("yyyy-MM-dd "); //pega a data escolhida pelo usuário
                Invoke((MethodInvoker)(() => tipodeevento = comboBox2.Text));
                // trabalhei com uma sugestão de string que indentificará cada tipo de evento no banco de dados
                dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                ArrayList arr = new ArrayList();
                List<string[]> consultalist;
                if (tipodeevento == "Todos")
                {
                    consultalist = dbManager.Select("ponto_eventos", "tipo_evento, descricao, data_hora", "id_ponto=" + words[0] + " and data_hora between '" + data + " 00:00:00' and '" + data + " 23:59:59' order by data_hora");
                }
                else
                {
                    consultalist = dbManager.Select("ponto_eventos", "tipo_evento, descricao, data_hora", "id_ponto=" + words[0] + " and tipo_evento='" + tipodeevento + "' and data_hora between '" + data + " 00:00:01' and '" + data + " 23:59:59' order by data_hora");
                }
                Invoke((MethodInvoker)(() => preencheList(consultalist)));
            }
            catch (Exception)
            {

            }
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void preencheList(List<string[]> consultalist)
        {
            listView1.Items.Clear();
            foreach (string[] linha in consultalist)
            {
                string[] subitems = new string[3];
                object[] o = linha;
                for (int i = 0; i < 3; i++)
                {
                    subitems[i] = o[i].ToString();
                }
                ListViewItem item = new ListViewItem(subitems);
                listView1.Items.Add(item); //carrega os itens no list view
            }
            label4.Text = "Quantidade de dados: " + consultalist.Count; //escreve na label a quantidade de dados exibidos no listview
            this.Controls.Add(listView1);
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select("ponto order by id_ponto ASC", "id_ponto, nome_ponto", "");
            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];

                listConcat.Add(concat);
            }
            return listConcat;
        }

        protected void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
            comboBox1.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            comboBox1.Items.Clear();
            foreach (string concat in list)
                comboBox1.Items.Add(concat);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public List<string> indexClientes { get; set; }
    }
}
