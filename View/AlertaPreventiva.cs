﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace SATS
{
    partial class AlertaPreventiva : Office2007Form
    {

        public AlertaPreventiva(List<string[]> lista)
        {
            InitializeComponent();
            FillListView(lista);
        }

        private void AboutBox2_Load(object sender, EventArgs e)
        {
            this.Text = "Alerta de preventiva";
        }

        protected void FillListView(List<string[]> resultList)
        {
            ListViewItem listViewItem;
            listView1.Items.Clear();
            foreach (string[] item in resultList)
            {
                listViewItem = new ListViewItem(item);
                listView1.Items.Add(listViewItem);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
