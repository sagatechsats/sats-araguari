﻿namespace SATS
{
    partial class CadastroHorarioSetPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroHorarioSetPoint));
            this.bttnSalvar = new System.Windows.Forms.Button();
            this.lblSetPoint = new System.Windows.Forms.Label();
            this.lblHoraInicial = new System.Windows.Forms.Label();
            this.dateTimeFinal = new System.Windows.Forms.DateTimePicker();
            this.lblHoraFinal = new System.Windows.Forms.Label();
            this.dateTimeInicial = new System.Windows.Forms.DateTimePicker();
            this.txtSetpoint = new System.Windows.Forms.TextBox();
            this.grupHorarioSetPoint = new System.Windows.Forms.GroupBox();
            this.grupHorarioSetPoint.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnSalvar
            // 
            this.bttnSalvar.Enabled = false;
            this.bttnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnSalvar.Image = ((System.Drawing.Image)(resources.GetObject("bttnSalvar.Image")));
            this.bttnSalvar.Location = new System.Drawing.Point(217, 156);
            this.bttnSalvar.Name = "bttnSalvar";
            this.bttnSalvar.Size = new System.Drawing.Size(97, 35);
            this.bttnSalvar.TabIndex = 11;
            this.bttnSalvar.Text = "Salvar";
            this.bttnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnSalvar.UseVisualStyleBackColor = true;
            this.bttnSalvar.Click += new System.EventHandler(this.bttnSalvar_Click);
            // 
            // lblSetPoint
            // 
            this.lblSetPoint.AutoSize = true;
            this.lblSetPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetPoint.Location = new System.Drawing.Point(66, 53);
            this.lblSetPoint.Name = "lblSetPoint";
            this.lblSetPoint.Size = new System.Drawing.Size(64, 15);
            this.lblSetPoint.TabIndex = 2;
            this.lblSetPoint.Text = "Setpoint:";
            // 
            // lblHoraInicial
            // 
            this.lblHoraInicial.AutoSize = true;
            this.lblHoraInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraInicial.Location = new System.Drawing.Point(202, 53);
            this.lblHoraInicial.Name = "lblHoraInicial";
            this.lblHoraInicial.Size = new System.Drawing.Size(85, 15);
            this.lblHoraInicial.TabIndex = 3;
            this.lblHoraInicial.Text = "Hora Inicial:";
            // 
            // dateTimeFinal
            // 
            this.dateTimeFinal.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimeFinal.Location = new System.Drawing.Point(331, 71);
            this.dateTimeFinal.Name = "dateTimeFinal";
            this.dateTimeFinal.Size = new System.Drawing.Size(98, 21);
            this.dateTimeFinal.TabIndex = 0;
            // 
            // lblHoraFinal
            // 
            this.lblHoraFinal.AutoSize = true;
            this.lblHoraFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraFinal.Location = new System.Drawing.Point(328, 53);
            this.lblHoraFinal.Name = "lblHoraFinal";
            this.lblHoraFinal.Size = new System.Drawing.Size(78, 15);
            this.lblHoraFinal.TabIndex = 4;
            this.lblHoraFinal.Text = "Hora Final:";
            // 
            // dateTimeInicial
            // 
            this.dateTimeInicial.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimeInicial.Location = new System.Drawing.Point(205, 71);
            this.dateTimeInicial.Name = "dateTimeInicial";
            this.dateTimeInicial.Size = new System.Drawing.Size(92, 21);
            this.dateTimeInicial.TabIndex = 1;
            // 
            // txtSetpoint
            // 
            this.txtSetpoint.Location = new System.Drawing.Point(69, 71);
            this.txtSetpoint.Name = "txtSetpoint";
            this.txtSetpoint.Size = new System.Drawing.Size(100, 21);
            this.txtSetpoint.TabIndex = 5;
            // 
            // grupHorarioSetPoint
            // 
            this.grupHorarioSetPoint.Controls.Add(this.txtSetpoint);
            this.grupHorarioSetPoint.Controls.Add(this.dateTimeInicial);
            this.grupHorarioSetPoint.Controls.Add(this.lblHoraFinal);
            this.grupHorarioSetPoint.Controls.Add(this.dateTimeFinal);
            this.grupHorarioSetPoint.Controls.Add(this.lblHoraInicial);
            this.grupHorarioSetPoint.Controls.Add(this.lblSetPoint);
            this.grupHorarioSetPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupHorarioSetPoint.Location = new System.Drawing.Point(12, 12);
            this.grupHorarioSetPoint.Name = "grupHorarioSetPoint";
            this.grupHorarioSetPoint.Size = new System.Drawing.Size(492, 117);
            this.grupHorarioSetPoint.TabIndex = 12;
            this.grupHorarioSetPoint.TabStop = false;
            this.grupHorarioSetPoint.Text = "Horário Set Point";
            // 
            // CadastroHorarioSetPoint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 219);
            this.Controls.Add(this.grupHorarioSetPoint);
            this.Controls.Add(this.bttnSalvar);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastroHorarioSetPoint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Horario SetPoint";
            this.Load += new System.EventHandler(this.CadastroHorarioSetPoint_Load);
            this.grupHorarioSetPoint.ResumeLayout(false);
            this.grupHorarioSetPoint.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnSalvar;
        private System.Windows.Forms.Label lblSetPoint;
        private System.Windows.Forms.Label lblHoraInicial;
        private System.Windows.Forms.DateTimePicker dateTimeFinal;
        private System.Windows.Forms.Label lblHoraFinal;
        private System.Windows.Forms.DateTimePicker dateTimeInicial;
        private System.Windows.Forms.TextBox txtSetpoint;
        private System.Windows.Forms.GroupBox grupHorarioSetPoint;
    }
}