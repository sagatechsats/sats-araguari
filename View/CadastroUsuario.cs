﻿using DevComponents.DotNetBar;
using SATS.Controller.segurança;
using System;
using System.Windows.Forms;

namespace SATS
{
    public partial class CadastroUsuario : Office2007Form
    {
        public CadastroUsuario()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (nomeUsuario.Text == "" || senhaUsuario.Text == "" || permissaoUsuario.Text == "")
            {
                MessageBox.Show("Favor preencher os campos de usuário senha e permissão!");
            }
            else if (nomeUsuario.TextLength < 5)
            {
                MessageBox.Show("Seu usuário deve ter entre 5 e 24 caracteres.");
            }
            else if (senhaUsuario.TextLength < 6)
            {
                MessageBox.Show("Sua senha deve ter entre 6 e 12 caracteres.");
            }
            else if (checkEmail.Checked && emailUsuario.Text == "")
            {
                MessageBox.Show("Favor preencher o email");
            }
            else
            {
                usuario User = new usuario(nomeUsuario.Text, senhaHash.getHashSha256(senhaUsuario.Text.ToString()), permissaoUsuario.Text, emailUsuario.Text, celularUsuario.Text, checkEmail.Checked);
                bdUsuario trabUsuario = new bdUsuario();

                trabUsuario.setUsuario(User);
                trabUsuario.inserir();
                this.Close();
            }
        }

        private void cadastroUsuario_Load(object sender, EventArgs e)
        {

        }

        private void nomeUsuario_Leave(object sender, EventArgs e)
        {
            nomeUsuario.Text = nomeUsuario.Text.Replace(" ", "_");
        }

        private void nomeUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;
            }
        }
    }
}
