﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using DevComponents.DotNetBar;
using System.Drawing;

namespace SATS
{
    public partial class CadastrarPonto : Office2007Form
    {
        [DllImport("wininet.dll")]

        private extern static Boolean InternetGetConnectedState(out int Description, int ReservedValue);
        private List<string> indexClientes;
        private List<string[]> res;
        private DBManager dB;
        private ManipulaXML mXml;
        private Main mainForm = null;

        private string text { get; set; }
        private string statusPag { get; set;}

        FrmInfoPonto tela;


        Thread novaThread;
        int ip = 0;



        public CadastrarPonto(Form callingForm)
        {
            string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
            dB = new DBManager(stcon);
            mainForm = callingForm as Main;
            InitializeComponent();
        }

        private void CadastrarPonto_Load(object sender, EventArgs e)
        {
            novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();
            btSalvar.Enabled = true;
            limpa();
        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();

                try
                {
                    dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();

                    Invoke((MethodInvoker)(() => PassListToComboBox(consulta)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(consulta)));
                }
                catch (Exception)
                {
                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            //Thread.Sleep(1000 * 60);
        }

        private int baspulsos(string value)
        {
            if (value == "1 litro por pulso")
                return 1;
            else if (value == "10 litros por pulso")
                return 10;
            else if (value == "100 litros por pulso")
                return 100;
            else if (value == "1000 litros por pulso")
                return 1000;
            else
                return 0;
        }

        private string tipo(string value)
        {
            if (value == "Produção")
                return "producao";
            else if (value == "Processo")
                return "processo";
            else
                return "";
        }

        private string porta(string value)
        {
            if (value.Length > 0)
                value = value.Replace("Porta ", "");
            else
                value = "";
            return value;
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            #region script
            //List<string[]> result = new List<string[]>();

            //if (txtNome.Text.Length == 0)
            //{
            //    MessageBox.Show("Digite o Nome de apresentação do ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            //if (txtPonto.Text.Length == 0)
            //{
            //    MessageBox.Show("Digite o Nome do ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            //if (ckBombaSomente.Checked == false && ckBombaMedidor.Checked == true && 
            //    ckNivel.Checked == false && ckVazao.Checked == false)
            //{
            //    MessageBox.Show("Selecione um tipo de ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}
            //if (ckBombaMedidor.Checked == true && ckNivel.Checked == false && ckVazao.Checked == false)
            //{
            //    MessageBox.Show("Selecione um tipo de ponto além da bomba!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            //int tipo_ponto = 0;
            //int bomba_ponto = 0;

            //if (ckNivel.Checked == true)
            //{
            //    tipo_ponto = 0;
            //}
            //else if (ckVazao.Checked == true)
            //{
            //    if (cbTipo.Text == "Pulsada")
            //        tipo_ponto = 5;
            //    else
            //        tipo_ponto = 2;
            //}
            //else if (ckPh.Checked == true)
            //{
            //    tipo_ponto = 3;
            //}
            //else if (ckMultK.Checked == true)
            //{
            //    tipo_ponto = 4;
            //}
            //else if (ckTurbidez.Checked == true)
            //{
            //    tipo_ponto = 6;
            //}
            //else if (ckTurbidez.Checked == true)
            //{
            //    tipo_ponto = 7;
            //}
            //else if (ckInversor.Checked == true)
            //{
            //    tipo_ponto = 8;
            //}
            //else
            //{
            //    tipo_ponto = 1;
            //}

            //if (ckBombaSomente.Checked == true)
            //{
            //    bomba_ponto = 1;
            //    tipo_ponto = 1;
            //}
            //else if (ckBombaMedidor.Checked == true)
            //{
            //    bomba_ponto = 1;
            //}
            //else
            //{
            //    bomba_ponto = 0;
            //}

            //string portanivel = "";
            //string portabomba = "";
            //string faltafase = "";

            //try
            //{
            //    portanivel = porta(cbPorta.SelectedItem.ToString()) == "" ? "0" : porta(cbPorta.SelectedItem.ToString());
            //    portabomba = porta(cBpbomba.SelectedItem.ToString()) == "" ? "0" : porta(cBpbomba.SelectedItem.ToString());
            //    faltafase = porta(cBfFase.SelectedItem.ToString()) == "" ? "0" : porta(cBfFase.SelectedItem.ToString());
            //}
            //catch (Exception)
            //{

            //}
            //portanivel = portanivel == "" ? "0" : portanivel;
            //portabomba = portabomba == "" ? "0" : portabomba;
            //faltafase = faltafase == "" ? "0" : faltafase;

            //int cPortaNivel = Convert.ToInt32(portanivel);
            //int cPortaBomba = Convert.ToInt32(portabomba);
            //int cFaltaFase = Convert.ToInt32(faltafase);

            //cPortaNivel = cPortaNivel == 0 ? cPortaNivel : cPortaNivel = cPortaNivel - 1;
            //cPortaBomba = cPortaBomba == 0 ? cPortaBomba : cPortaBomba = cPortaBomba - 1;
            //cFaltaFase = cFaltaFase == 0 ? cFaltaFase : cFaltaFase = cFaltaFase - 1;

            //string adress = txtEnderecoMod.Text == "" ? "0" : txtEnderecoMod.Text;
            //string adressIn = txtEnderecoInversor.Text == "" ? "0" : txtEnderecoInversor.Text;
            //string max_reservatorio = txtNmax.Text.ToString() == "" ? "0" : txtNmax.Text.ToString();
            //string min_reservatorio = txtNmin.Text.ToString() == "" ? "0" : txtNmin.Text.ToString();
            //double max = Convert.ToDouble(max_reservatorio);
            //double min = Convert.ToDouble(min_reservatorio);

            //if(min > max)
            //{
            //    MessageBox.Show("Valor minimo Invalido !" , "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            //string dados = "'" + txtPonto.Text + "', '" + txtDns.Text + "', '" + tipo_ponto + "', '" + bomba_ponto + "', '" + cPortaNivel + "', '" + min + "', '" + cFaltaFase + "', '" + max + "', '" + max + "', '" + adress+ "', '" +adressIn + "'";
            //bool pode = dB.Insert2("ponto", "nome_ponto, ddns, ponto_tipo, bomba_ponto, porta_nivel,nivel_min, porta_rele, max_reservatorio, nivel_max,adress,adress_inversor", dados.ToString());
            //if (bomba_ponto == 1)
            //{
            //    try
            //    {
            //        List<string[]> id_ponto = dB.Select("ponto", "id_ponto", "nome_ponto='" + txtPonto.Text + "'");
            //        dB.Insert2("ponto_aciona", "id_ponto, acionar, porta, data_hora", "'" + id_ponto[0][0] + "', '0', '" + cPortaBomba + "', now()");
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("Erro ao Inserir!\n" + ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    }
            //}

            //string[] subitems = new string[4];
            //mXml = new ManipulaXML();

            //result = mXml.LerXML();
            //subitems[0] = txtPonto.Text.ToString();
            //subitems[1] = txtNome.Text.ToString();
            //subitems[2] = "40";
            //subitems[3] = (40 + ip).ToString();

            //result.Add(subitems);

            //mXml.GeraXML(result);

            //this.mainForm.criaPonto(txtPonto.Text.ToString(), txtNome.Text.ToString(), 40, 40 + ip);
            //ip = ip + 40;

            //limpa();

            //if (pode == true)
            //{
            //    MessageBox.Show("Ponto criado com sucesso!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    novaThread = new Thread(new ThreadStart(CarregaTela));
            //    novaThread.Start();
            //}
            //else
            //{
            //    MessageBox.Show("Ponto criado com sucesso, mas o ponto pode estar duplicado!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            #endregion

            if (btnDelete.Enabled == false)
            {
                Invoke((MethodInvoker)(() => salvar())); 

            }else if(btnDelete.Enabled)
            {
                Invoke((MethodInvoker)(() => atualizar()));
            }
        }



        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("As informações não seram salvas", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Dispose();
        }


        public static Boolean IsConnected()
        {
            bool result;

            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }

            catch
            {
                result = false;
            }
            return result;
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            res = dB.Select("ponto order by id_ponto ASC", "id_ponto, nome_ponto", "");
            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                listConcat.Add(concat);
            }
            return listConcat;
        }

        protected void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
            cBPonto.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            cBPonto.Items.Clear();
            foreach (string concat in list)
            {
                cBPonto.Items.Add(concat);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (cBPonto.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione o ponto para exclusão!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show("Você tem certeza sobre a exclusão do ponto?", "Atenção!", MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (dB.Delete("ponto", "id_ponto='" + res[cBPonto.SelectedIndex][0].ToString() + "'"))
                {
                    try
                    {
                        dB.Delete("ponto_informacao","id_ponto='" + res[cBPonto.SelectedIndex][0].ToString() + "'");
                        dB.Delete("ponto_posicao", "id_ponto = '" + res[cBPonto.SelectedIndex][0].ToString() + "'");
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    //mXml = new ManipulaXML();
                    //List<string[]> pontos = mXml.LerXML();
                    //try
                    //{
                    //    pontos = mXml.DeletePonto(res[cBPonto.SelectedIndex][1].ToString(), pontos);
                    //}
                    //catch (Exception)
                    //{
                    //}
                    //mXml.GeraXML(pontos);
                    //this.mainForm.deletePonto(res[cBPonto.SelectedIndex][1].ToString());

                    res[cBPonto.SelectedIndex][0] = null;
                    res[cBPonto.SelectedIndex][1] = null;
                    cBPonto.Items.Clear();
                    cBPonto.Text = "";
                    List<string> listConcat = new List<string>();

                    List<string[]> listres = new List<string[]>();

                    foreach (string[] item in res)
                    {
                        if (item[0] != null) listres.Add(item);
                    }
                    res = listres;
                    foreach (string[] item in res)
                    {
                        if (item[0] != null)
                        {
                            string concat = String.Empty;
                            indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                            concat += Int32.Parse(item[0]).ToString("D6");
                            concat += " - ";
                            concat += item[1];

                            listConcat.Add(concat);
                        }
                    }

                    PassListToComboBox(listConcat);
                    PassListToAutoComplete(listConcat);
                    MessageBox.Show("Ponto exlcuído com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpa();
                }
                else MessageBox.Show("Erro ao excluir ponto!", "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetterOrDigit(e.KeyChar)     // Allowing only any letter OR Digit
            || e.KeyChar == '\b' || e.KeyChar == ' ' || (e.KeyChar == '.')) // Allowing BackSpace character
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtHidrometro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

   

        private void ckBombaSomente_CheckedChanged(object sender, EventArgs e)
        {
            if (ckBombaSomente.Checked == true)
            {
                ckBombaMedidor.Checked = false;
                ckNivel.Checked = false;
                ckVazao.Checked = false;
                ckPh.Checked = false;
                gBbomba.Enabled = true;
                gBnivel.Enabled = false;
                cbPorta.Text = "";
                txtNmax.Text = "";
                txtNmin.Visible = false;
                lblNmin.Visible = false;
                lblInvB.Visible = false;
                txtEnderecoInversor.Visible = false;
                lblMod.Visible = true;
                txtEnderecoMod.Visible = true;
            }
            else
            {
                gBbomba.Enabled = false;
                gBnivel.Enabled = true;
                cBfFase.Text = "";
                cBpbomba.Text = "";
            }
        }

  

        private void ckNivel_CheckedChanged(object sender, EventArgs e)
        {
            if (ckNivel.Checked == true)
            {
                lblPorta.Text = "Porta Nível:";
                lblMaximo.Text = "Nível Máximo:";
                lblMaximo.Visible = true;
                lblNmin.Text = "Nível Minimo";
                lblInvB.Visible = false;
                txtEnderecoInversor.Visible = false;
                txtNmax.Visible = true;
                ckBombaMedidor.Text = "Bomba + Nível";
                gBnivel.Text = "Informações Nível: ";
                cbTipo.Visible = false;
                lbTipoVazao.Visible = false;
                txtNmin.Visible = true;
                lblNmin.Visible = true;
                lblMod.Visible = true;
                txtEnderecoMod.Visible = true;
                cbPorta.Items.Clear();
                for (int i = 1; i <= 10; i++)
                    cbPorta.Items.Add("Porta " + i);
            }
            else
            {
                cbPorta.Text = "";
                txtNmax.Text = "";
            }
        }

        private void ckVazao_CheckedChanged(object sender, EventArgs e)
        {
            if (ckVazao.Checked == true)
            {
                lblPorta.Text = "Porta Vazão:";
                lblMaximo.Text = "Vazão Máxima:";
                lblMaximo.Visible = true;
                lblNmin.Text = "Vazão Minimo";
                lblInvB.Visible = false;
                txtEnderecoInversor.Visible = false;
                txtNmax.Visible = true;
                ckBombaMedidor.Text = "Bomba + Vazão";
                gBnivel.Text = "Informações Vazão: ";
                cbTipo.Visible = true;
                lbTipoVazao.Visible = true;
                txtNmin.Visible = true;
                lblNmin.Visible = true;
                cbPorta.Items.Clear();
                lblMod.Visible = true;
                txtEnderecoMod.Visible = true;
                for (int i = 1; i <= 10;i++)
                    cbPorta.Items.Add("Porta "+i);
                

            }
            else
            {
                cbPorta.Text = "";
                txtNmax.Text = "";
                txtNmin.Text = "";
                cbTipo.Text = "";
            }
        }

        private void ckBombaMedidor_CheckedChanged(object sender, EventArgs e)
        {
            if (ckBombaMedidor.Checked == true)
            {
                ckBombaSomente.Checked = false;
                gBbomba.Enabled = true;
                gBnivel.Enabled = true;
            }
            else
            {
                gBbomba.Enabled = false;
                cBfFase.Text = "";
                cBpbomba.Text = "";
            }
        }

        private void ckPh_CheckedChanged(object sender, EventArgs e)
        {
            if (ckPh.Checked == true)
            {
                lblPorta.Text = "Porta pH:";
                lblMaximo.Text = "pH Máximo:";
                lblNmin.Text = "pH Minimo";
                lblInvB.Visible = false;
                txtEnderecoInversor.Visible = false;
                lblMaximo.Visible = true;
                txtNmax.Visible = true;
                ckBombaMedidor.Text = "Bomba + Ph";
                gBnivel.Text = "Informações pH: ";
                gBbomba.Enabled = false;
                cBfFase.Text = "";
                cBpbomba.Text = "";
                cbTipo.Visible = false;
                lbTipoVazao.Visible = false;
                txtNmin.Visible = true;
                lblNmin.Visible = true;
                lblMod.Visible = true;
                txtEnderecoMod.Visible = true;
                cbPorta.Items.Clear();
                for (int i = 1; i <= 10; i++)
                    cbPorta.Items.Add("Porta " + i);
            }
            else
            {
                cbPorta.Text = "";
                txtNmax.Text = "";
                txtNmin.Text = "";

            }
        }

        private void ckMultK_CheckedChanged(object sender, EventArgs e)
        {
            if (ckMultK.Checked == true)
            {
                lblPorta.Text = "Porta MultK:";
                lblMaximo.Text = "MultK Máximo:";
                lblNmin.Text = "Multk Minimo";
                lblInvB.Visible = false;
                txtEnderecoInversor.Visible = false;
                lblMaximo.Visible = false;
                txtNmax.Visible = false;
                ckBombaMedidor.Text = "Bomba + MultK";
                gBnivel.Text = "Informações MultK: ";
                gBbomba.Enabled = false;
                cBfFase.Text = "";
                cBpbomba.Text = "";
                cbTipo.Visible = false;
                lbTipoVazao.Visible = false;
                txtNmin.Visible = false;
                txtNmin.Visible = false;
                lblNmin.Visible = false;
                lblMod.Visible = true;
                txtEnderecoMod.Visible = true;
                cbPorta.Items.Clear();
                for (int i = 1; i <= 10; i++)
                    cbPorta.Items.Add("Porta " + i);
            }
            else
            {
                cbPorta.Text = "";
                txtNmax.Text = "";
                txtNmin.Text = "";

            }
        }

        private void ckTurbidez_CheckedChanged(object sender, EventArgs e)
        {
            if (ckTurbidez.Checked == true)
            {
                lblPorta.Text = "Porta Turbidez:";
                cbPorta.Size = new Size(126,24);
                cbPorta.Location = new Point(109, 22); //109; 22
                lblMaximo.Text = "Turbidez Máximo:";
                lblMaximo.Visible = false;
                txtNmax.Visible = false;
                ckBombaMedidor.Text = "Bomba + Turbidez";
                gBnivel.Text = "Informações Turbidez: ";
                gBbomba.Enabled = false;
                lblInvB.Visible = false;
                txtEnderecoInversor.Visible = false;
                cBfFase.Text = "";
                cBpbomba.Text = "";
                cbTipo.Visible = false;
                lbTipoVazao.Visible = false;
                txtNmin.Visible = false;
                lblNmin.Visible = false;
                lblMod.Visible = true;
                txtEnderecoMod.Visible = true;
                cbPorta.Items.Clear();
                for (int i = 1; i <= 10; i++)
                    cbPorta.Items.Add("Porta " + i);
            }
            else
            {
                cbPorta.Size = new Size(146, 24);
                cbPorta.Location = new Point(89, 22);// 89; 22
                cbPorta.Text = "";
                txtNmax.Text = "";
                txtNmin.Text = "";

            }
        }

        private void ckPressao_CheckedChanged(object sender, EventArgs e)
        {
            if (ckPressao.Checked == true)
            {
                lblPorta.Text = "Porta Pressão:";
                cbPorta.Size = new Size(126, 24);
                cbPorta.Location = new Point(109, 22); //109; 22
                lblMaximo.Text = "Pressão Máxima:";
                lblNmin.Text = "Pressão Minimo";
                lblInvB.Visible = false;
                txtEnderecoInversor.Visible = false;
                lblMaximo.Visible = true;
                txtNmax.Visible = true;
                ckBombaMedidor.Text = "Bomba + Pressão";
                gBnivel.Text = "Informações Pressão: ";
                gBbomba.Enabled = false;
                cBfFase.Text = "";
                cBpbomba.Text = "";
                cbTipo.Visible = false;
                lbTipoVazao.Visible = false;
                txtNmin.Visible = true;
                lblNmin.Visible = true;
                lblMod.Visible = true;
                txtEnderecoMod.Visible = true;
                cbPorta.Items.Clear();
                for (int i = 1; i <= 10; i++)
                    cbPorta.Items.Add("Porta " + i);

            }
            else
            {
                cbPorta.Size = new Size(146, 24);
                cbPorta.Location = new Point(89, 22);// 89; 22
                cbPorta.Text = "";
                txtNmax.Text = "";
                txtNmin.Text = "";

            }

        }

        private void ckInversor_CheckedChanged(object sender, EventArgs e)
        {
            if (ckInversor.Checked == true)
            {
                lblPorta.Text = "Porta Inversor:";
                cbPorta.Size = new Size(126, 24);
                cbPorta.Location = new Point(109, 22); //109; 22
                lblMaximo.Text = "Frequência Máx:";
                lblNmin.Text = "Frequência Minimo";
                lblMod.Visible = false;
                txtEnderecoMod.Visible = false;
                lblInvB.Visible = true;
                txtEnderecoInversor.Visible = true;
                lblMaximo.Visible = true;
                txtNmax.Visible = true;
                ckBombaMedidor.Text = "Bomba + Inversor";
                gBnivel.Text = "Informações Inversor: ";
                gBbomba.Enabled = false;
                cBfFase.Text = "";
                cBpbomba.Text = "";
                cbTipo.Visible = false;
                lbTipoVazao.Visible = false;
                txtNmin.Visible = true;
                lblNmin.Visible = true;
                cbPorta.Items.Clear();
                for (int i = 1; i <= 10; i++)
                    cbPorta.Items.Add("Porta " + i);

            }
            else
            {
                cbPorta.Size = new Size(146, 24);
                cbPorta.Location = new Point(89, 22);// 89; 22
                cbPorta.Text = "";
                txtNmax.Text = "";
                txtNmin.Text = "";

            }

        }

        private void bttnEditar_Click(object sender, EventArgs e)
        {

            int index = cBPonto.SelectedIndex;
            if (cBPonto.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione o Ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            bttnEditar.Enabled = false;
            cBPonto.Enabled = false;
            btnNovo.Enabled = false;
            btSalvar.Enabled = true;

            Thread novaThread = new Thread(new ParameterizedThreadStart(carregar));
            novaThread.Start(index);

        }

        private void carregar(object parametro)
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                int index = Convert.ToInt32(parametro);
                try
                {
                    dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    List<string[]> result = dB.Select("ponto", "*", "id_ponto='" + res[index][0] + "'");

                    //Verifica se e bomba 
                    if (result[0][3] == "1")
                    {
                        result = dB.SelectInnerJoin2("ponto", "*", "ponto_aciona", "ponto.id_ponto = ponto_aciona.id_ponto", " ponto.id_ponto=" + res[index][0] + "");
                    }

                    if (result.Count == 0)
                    {
                        MessageBox.Show("Dados não cadastrados!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Invoke((MethodInvoker)(() => limpa()));
                        Application.UseWaitCursor = false;
                        Application.DoEvents();
                        
                        return;
                    }

                    //Invoke((MethodInvoker)(() => checkBox.Checked = Convert.ToBoolean(result[0][1])));

                    //Verifica o tipo ponto 
                    int port = Convert.ToInt32(result[0][5]);
                    port = port + 1;
                 

                    switch (result[0][3])
                    {
                        case "0":
                          
                            Invoke((MethodInvoker)(() => ckNivel.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => txtNmax.Text = (result[0][9].ToString() == "0" ? txtNmax.Text = "" : result[0][9].ToString())));
                            Invoke((MethodInvoker)(() => txtNmin.Text = (result[0][6].ToString() == "0" ? txtNmin.Text = "" : result[0][6].ToString())));
                            Invoke((MethodInvoker)(() => cbPorta.Text = ("Porta " + port)));
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));

                        break;


                        case "1":
                            int portBomba = Convert.ToInt32(result[0][22]);
                            portBomba = portBomba + 1;
                            int portFase = Convert.ToInt32(result[0][8]);
                            portFase = portFase + 1;


                            Invoke((MethodInvoker)(() => ckBombaSomente.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => cBfFase.Text = "Porta " + portFase));
                            Invoke((MethodInvoker)(() => cBpbomba.Text = "Porta " + portBomba));
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));

                        break;

                        case "2":
                            Invoke((MethodInvoker)(() => ckVazao.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => txtNmax.Text = (result[0][9].ToString() == "0" ? txtNmax.Text = "" : result[0][9].ToString())));
                            Invoke((MethodInvoker)(() => txtNmin.Text = (result[0][6].ToString() == "0" ? txtNmin.Text = "" : result[0][6].ToString())));
                            Invoke((MethodInvoker)(() => cbPorta.Text = ("Porta " + port)));
                            Invoke((MethodInvoker)(() => cbTipo.Text = (result[0][3] == "2" ? cbTipo.Text = "Analógica" : "")));
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));

                       break;


                        case "3":
                            Invoke((MethodInvoker)(() => ckPh.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => txtNmax.Text = (result[0][9].ToString() == "0" ? txtNmax.Text = "" : result[0][9].ToString())));
                            Invoke((MethodInvoker)(() => txtNmin.Text = (result[0][6].ToString() == "0" ? txtNmin.Text = "" : result[0][6].ToString())));
                            Invoke((MethodInvoker)(() => cbPorta.Text = ("Porta " + port)));
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));
                       break;

                       case "4":
                            Invoke((MethodInvoker)(() => ckMultK.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => txtNmax.Text = (result[0][9].ToString() == "0" ? txtNmax.Text = "" : result[0][9].ToString())));
                            Invoke((MethodInvoker)(() => txtNmin.Text = (result[0][6].ToString() == "0" ? txtNmin.Text = "" : result[0][6].ToString())));
                            Invoke((MethodInvoker)(() => cbPorta.Text = ("Porta " + port)));
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));
                       break;

                       case "5":
                            Invoke((MethodInvoker)(() => ckVazao.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => txtNmax.Text = (result[0][9].ToString() == "0" ? txtNmax.Text = "" : result[0][9].ToString())));
                            Invoke((MethodInvoker)(() => txtNmin.Text = (result[0][6].ToString() == "0" ? txtNmin.Text = "" : result[0][6].ToString())));
                            Invoke((MethodInvoker)(() => cbPorta.Text = ("Porta " + port)));
                            Invoke((MethodInvoker)(() => cbTipo.Text = (result[0][3] == "5" ? cbTipo.Text = "Pulsada" : ""))); 
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));

                        break;

                        case "6":
                            Invoke((MethodInvoker)(() => ckTurbidez.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => cbPorta.Text = ("Porta " + port)));
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));
                       break;

                        case "7":
                            Invoke((MethodInvoker)(() => ckTurbidez.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => txtNmax.Text = (result[0][9].ToString() == "0" ? txtNmax.Text = "" : result[0][9].ToString())));
                            Invoke((MethodInvoker)(() => txtNmin.Text = (result[0][6].ToString() == "0" ? txtNmin.Text = "" : result[0][6].ToString())));
                            Invoke((MethodInvoker)(() => cbPorta.Text = ("Porta " + port)));
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = (result[0][18].ToString() == "0"?"": result[0][18].ToString())));

                        break;

                        case "8":
                            Invoke((MethodInvoker)(() => ckTurbidez.Checked = true));
                            Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtNome.Text = result[0][1].ToString()));
                            Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                            Invoke((MethodInvoker)(() => cbPorta.Text = ("Porta " + port)));
                            Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                            Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));
                       break;
                    }

                    #region opção
                    //if (result[0][3] == "0")
                    //{
                    //    Invoke((MethodInvoker)(() => ckNivel.Checked = true));
                    //    Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                    //    Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                    //    Invoke((MethodInvoker)(() => txtNmax.Text = (result[0][9].ToString() == "0" ? txtNmax.Text = "" : result[0][9].ToString())));
                    //    Invoke((MethodInvoker)(() => cbPorta.Text = (result[0][5] == "0" ? cbPorta.Text = "" : "Porta " + (result[0][5]))));
                    //    Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                    //    Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));

                    //}
                    //else if(result[0][3] == "1")
                    //{
                    //    Invoke((MethodInvoker)(() => ckBombaSomente.Checked  = true));
                    //    Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                    //    Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                    //    Invoke((MethodInvoker)(() => cbPorta.Text = (result[0][5] == "0" ? cbPorta.Text = "" : "Porta " + (result[0][5]))));
                    //    Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                    //    Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));

                    //}

                    //else if(result[0][3] == "7")
                    //{
                    //    Invoke((MethodInvoker)(() => ckTurbidez.Checked = true));
                    //    Invoke((MethodInvoker)(() => txtPonto.Text = result[0][1].ToString()));
                    //    Invoke((MethodInvoker)(() => txtDns.Text = result[0][2].ToString()));
                    //    Invoke((MethodInvoker)(() => txtNmax.Text = (result[0][9].ToString() == "0" ? txtNmax.Text = "" : result[0][9].ToString())));
                    //    Invoke((MethodInvoker)(() => cbPorta.Text = (result[0][5] == "0" ? cbPorta.Text = "" : "Porta " + (result[0][5]))));
                    //    Invoke((MethodInvoker)(() => txtEnderecoMod.Text = result[0][13].ToString()));
                    //    Invoke((MethodInvoker)(() => txtEnderecoInversor.Text = result[0][18].ToString()));
                    //}
                    #endregion

                }

                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        private void limpa()
        {
            bttnEditar.Enabled = true;
            cBPonto.Enabled = true;
            cBPonto.Text = "";
            cbTipo.Text = "";
            ckBombaMedidor.Checked = false;
            ckBombaSomente.Checked = false;
            ckNivel.Checked = false;
            ckTurbidez.Checked = false;
            ckMultK.Checked = false;
            ckPh.Checked = false;
            ckVazao.Checked = false;
            ckInversor.Checked = false;
            ckPressao.Checked = false;
            cbPorta.Text = "";
            cBfFase.Text = "";
            cBpbomba.Text = "";
            txtNmax.Text = "";
            txtNome.Text = "";
            txtPonto.Text = "";
            txtDns.Text = "";
            txtEnderecoInversor.Text = "";
            txtEnderecoMod.Text = "";
            txtNmin.Text = "";
            btnNovo.Enabled = true;
            btSalvar.Enabled = false;
            btnDelete.Enabled = true;

            novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            limpa();
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            #region script

            //int index = cBPonto.SelectedIndex;
            //if(bttnEditar.Enabled == false && cBPonto.Enabled == false)
            //{

            //    if (txtPonto.Text.Length == 0)
            //    {
            //        MessageBox.Show("Digite o Nome do ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }

            //    if (ckBombaSomente.Checked == false && ckBombaMedidor.Checked == true &&
            //        ckNivel.Checked == false && ckVazao.Checked == false)
            //    {
            //        MessageBox.Show("Selecione um tipo de ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }
            //    if (ckBombaMedidor.Checked == true && ckNivel.Checked == false && ckVazao.Checked == false)
            //    {
            //        MessageBox.Show("Selecione um tipo de ponto além da bomba!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }

            //    int tipo_ponto = 0;
            //    int bomba_ponto = 0;

            //    if (ckNivel.Checked == true)
            //    {
            //        tipo_ponto = 0;
            //    }
            //    else if (ckVazao.Checked == true)
            //    {
            //        if (cbTipo.Text == "Pulsada")
            //            tipo_ponto = 5;
            //        else
            //            tipo_ponto = 2;
            //    }
            //    else if (ckPh.Checked == true)
            //    {
            //        tipo_ponto = 3;
            //    }
            //    else if (ckMultK.Checked == true)
            //    {
            //        tipo_ponto = 4;
            //    }
            //    else if (ckTurbidez.Checked == true)
            //    {
            //        tipo_ponto = 6;
            //    }
            //    else if (ckTurbidez.Checked == true)
            //    {
            //        tipo_ponto = 7;
            //    }
            //    else if (ckInversor.Checked == true)
            //    {
            //        tipo_ponto = 8;
            //    }
            //    else
            //    {
            //        tipo_ponto = 1;
            //    }

            //    if (ckBombaSomente.Checked == true)
            //    {
            //        bomba_ponto = 1;
            //        tipo_ponto = 1;
            //    }
            //    else if (ckBombaMedidor.Checked == true)
            //    {
            //        bomba_ponto = 1;
            //    }
            //    else
            //    {
            //        bomba_ponto = 0;
            //    }

            //    string portanivel = "";
            //    string portabomba = "";
            //    string faltafase = "";

            //    try
            //    {
            //        portanivel = porta(cbPorta.SelectedItem.ToString()) == "" ? "0" : porta(cbPorta.SelectedItem.ToString());
            //        portabomba = porta(cBpbomba.SelectedItem.ToString()) == "" ? "0" : porta(cBpbomba.SelectedItem.ToString());
            //        faltafase = porta(cBfFase.SelectedItem.ToString()) == "" ? "0" : porta(cBfFase.SelectedItem.ToString());
            //    }
            //    catch (Exception)
            //    {

            //    }

            //    portanivel = portanivel == "" ? "0" : portanivel;
            //    portabomba = portabomba == "" ? "0" : portabomba;
            //    faltafase = faltafase == "" ? "0" : faltafase;

            //    int cPortaNivel = Convert.ToInt32(portanivel);
            //    int cPortaBomba = Convert.ToInt32(portabomba);
            //    int cFaltaFase = Convert.ToInt32(faltafase);

            //    cPortaNivel = cPortaNivel == 0 ? cPortaNivel : cPortaNivel = cPortaNivel - 1;
            //    cPortaBomba = cPortaBomba == 0 ? cPortaBomba : cPortaBomba = cPortaBomba - 1;
            //    cFaltaFase  = cFaltaFase == 0  ? cFaltaFase:  cFaltaFase = cFaltaFase - 1;


            //    string adress = txtEnderecoMod.Text == "" ? "0" : txtEnderecoMod.Text;
            //    string adressIn = txtEnderecoInversor.Text == "" ? "0" : txtEnderecoInversor.Text;
            //    string max_reservatorio = txtNmax.Text.ToString() == "" ? "0" : txtNmax.Text.ToString();
            //    string min_reservatorio = txtNmin.Text.ToString() == "" ? "0" : txtNmin.Text.ToString();

            //    double max = Convert.ToDouble(max_reservatorio);
            //    double min = Convert.ToDouble(min_reservatorio);



            //    if (min > max)
            //    {
            //        MessageBox.Show("Valor minimo Invalido !", "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }


            //    try
            //    {
            //        dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            //        bool resultbol = dB.Update("ponto", "nome_ponto = '" + txtPonto.Text + "', ddns = '" + txtDns.Text + "'" + ", ponto_tipo = " + tipo_ponto + ", bomba_ponto = " + bomba_ponto + ", porta_nivel = " + cPortaNivel + ", nivel_min = " + min_reservatorio.Replace(",", ".") + ", porta_rele = " + cFaltaFase + ",max_reservatorio=" + max_reservatorio.Replace(",", ".") + ",nivel_max=" + max_reservatorio.Replace(",", ".") + ", adress = '" + adress + "'" + ", adress_inversor = '" + adressIn + "'", "id_ponto = " + res[index][0]);
            //        //bool pode = dB.Insert2("ponto", "nome_ponto, ddns, ponto_tipo, bomba_ponto, porta_nivel, porta_rele, max_reservatorio, nivel_max,adress,adress_inversor", dados.ToString());


            //        if (ckBombaSomente.Checked)
            //        {
            //            //List<string[]> id_ponto = dB.Select("ponto", "id_ponto", "nome_ponto='" + txtPonto.Text + "'");
            //            //dB.Insert2("ponto_aciona", "id_ponto, acionar, porta, data_hora", "'" + id_ponto[0][0] + "', '0', '" + portabomba + "', now()");
            //            try
            //            {

            //                bool resultbol2 = dB.Update("ponto_aciona","porta= "+ cPortaBomba,"id_ponto = "+res[index][0]);
            //                //dB.Insert2("ponto_aciona", "id_ponto, acionar, porta, data_hora", "'" + id_ponto[0][0] + "', '0', '" + portabomba + "', now()");

            //            }
            //            catch (Exception er)
            //            {

            //               MessageBox.Show(er.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            }
            //            Application.UseWaitCursor = false;
            //            Application.DoEvents();
            //        }

            //        if (resultbol == false)
            //        {
            //            MessageBox.Show("Erro ao editar dados!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            Invoke((MethodInvoker)(() => limpa()));
            //        }
            //        else
            //        {
            //            MessageBox.Show("Dados alterados com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            Invoke((MethodInvoker)(() => limpa()));
            //            novaThread = new Thread(new ThreadStart(CarregaTela));
            //            novaThread.Start();
            //        }

            //    }
            //    catch (Exception er)
            //    {
            //        MessageBox.Show(er.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //    //Application.UseWaitCursor = false;
            //    //Application.DoEvents();

            //}
            //else
            //{
            //    MessageBox.Show("Selecione o Ponto e clique no botão  Editar!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
            #endregion
        }

        private void salvar()
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();

            List<string[]> result = new List<string[]>();

            if (txtNome.Text.Length == 0)
            {
                MessageBox.Show("Digite o Nome de apresentação do ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (txtPonto.Text.Length == 0)
            {
                MessageBox.Show("Digite o Nome do ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (ckBombaSomente.Checked == false && ckBombaMedidor.Checked == true &&
                ckNivel.Checked == false && ckVazao.Checked == false)
            {
                MessageBox.Show("Selecione um tipo de ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (ckBombaMedidor.Checked == true && ckNivel.Checked == false && ckVazao.Checked == false)
            {
                MessageBox.Show("Selecione um tipo de ponto além da bomba!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int tipo_ponto = 0;
            int bomba_ponto = 0;

            if (ckNivel.Checked == true)
            {
                tipo_ponto = 0;
            }
            else if (ckVazao.Checked == true)
            {
                if (cbTipo.Text == "Pulsada")
                    tipo_ponto = 5;
                else
                    tipo_ponto = 2;
            }
            else if (ckPh.Checked == true)
            {
                tipo_ponto = 3;
            }
            else if (ckMultK.Checked == true)
            {
                tipo_ponto = 4;
            }
            else if (ckTurbidez.Checked == true)
            {
                tipo_ponto = 6;
            }
            else if (ckTurbidez.Checked == true)
            {
                tipo_ponto = 7;
            }
            else if (ckInversor.Checked == true)
            {
                tipo_ponto = 8;
            }
            else
            {
                tipo_ponto = 1;
            }

            if (ckBombaSomente.Checked == true)
            {
                bomba_ponto = 1;
                tipo_ponto = 1;
            }
            else if (ckBombaMedidor.Checked == true)
            {
                bomba_ponto = 1;
            }
            else
            {
                bomba_ponto = 0;
            }

            string portanivel = "";
            string portabomba = "";
            string faltafase = "";

            try
            {
                portanivel = porta(cbPorta.SelectedItem.ToString()) == "" ? "0" : porta(cbPorta.SelectedItem.ToString());
                portabomba = porta(cBpbomba.SelectedItem.ToString()) == "" ? "0" : porta(cBpbomba.SelectedItem.ToString());
                faltafase = porta(cBfFase.SelectedItem.ToString()) == "" ? "0" : porta(cBfFase.SelectedItem.ToString());
            }
            catch (Exception)
            {

            }
            portanivel = portanivel == "" ? "0" : portanivel;
            portabomba = portabomba == "" ? "0" : portabomba;
            faltafase = faltafase == "" ? "0" : faltafase;

            int cPortaNivel = Convert.ToInt32(portanivel);
            int cPortaBomba = Convert.ToInt32(portabomba);
            int cFaltaFase = Convert.ToInt32(faltafase);

            cPortaNivel = cPortaNivel == 0 ? cPortaNivel : cPortaNivel = cPortaNivel - 1;
            cPortaBomba = cPortaBomba == 0 ? cPortaBomba : cPortaBomba = cPortaBomba - 1;
            cFaltaFase = cFaltaFase == 0 ? cFaltaFase : cFaltaFase = cFaltaFase - 1;

            string adress = txtEnderecoMod.Text == "" ? "0" : txtEnderecoMod.Text;
            string adressIn = txtEnderecoInversor.Text == "" ? "0" : txtEnderecoInversor.Text;
            string max_reservatorio = txtNmax.Text.ToString() == "" ? "0" : txtNmax.Text.ToString();
            string min_reservatorio = txtNmin.Text.ToString() == "" ? "0" : txtNmin.Text.ToString();
            double max = Convert.ToDouble(max_reservatorio);
            double min = Convert.ToDouble(min_reservatorio);

            if (min > max)
            {
                MessageBox.Show("Valor minimo Invalido !", "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int indexAux = Convert.ToInt32(res[res.Count - 1][0]);
            indexAux = indexAux + 1;
            
            string dados = "'" + indexAux + "', '" + txtPonto.Text + "', '" + txtDns.Text + "', '" + tipo_ponto + "', '" + bomba_ponto + "', '" + cPortaNivel + "', '" + min + "', '" + cFaltaFase + "', '" + max + "', '" + max + "', '" + adress + "', '" + adressIn + "'";
            bool pode = dB.Insert2("ponto", "id_ponto,nome_ponto, ddns, ponto_tipo, bomba_ponto, porta_nivel,nivel_min, porta_rele, max_reservatorio, nivel_max,adress,adress_inversor", dados.ToString());

            string valores = "'" + indexAux + "', '" + 40 + "', '" + (40 + ip) + "'";
            dB.Insert("ponto_posicao", "id_ponto, eixoX, eixoY", valores);

            if (bomba_ponto == 1)
            {
                try
                {
                    List<string[]> id_ponto = dB.Select("ponto", "id_ponto", "nome_ponto='" + txtPonto.Text + "'");
                    dB.Insert2("ponto_aciona", "id_ponto, acionar, porta, data_hora", "'" + id_ponto[0][0] + "', '0', '" + cPortaBomba + "', now()");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro ao Inserir!\n" + ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            int auxIndex = Convert.ToInt32(res[res.Count - 1][0]);
            salvarInfo(auxIndex);

#region
            //try
            //{
            //    string valores = "'" + indexAux + "', '" + 40 + "', '" + (40+ip) + "'";
            //    dB.Insert("ponto_posicao", "id_ponto, eixoX, eixoY", valores);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}


            //if (!statusPag)
            //{
            //    int index = cBPonto.SelectedIndex;

            //    try
            //    {

            //        dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            //        bool resultbol = dB.Insert("ponto_informacao", "id_ponto, informacao, data_hora",
            //                    "'" + index + "', '" + text + "', now()");
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }

            //}



            //string[] subitems = new string[4];
            //mXml = new ManipulaXML();

            //result = mXml.LerXML();
            //subitems[0] = txtPonto.Text.ToString();
            //subitems[1] = txtNome.Text.ToString();
            //subitems[2] = "40";
            //subitems[3] = (40 + ip).ToString();

            //result.Add(subitems);




            //mXml.GeraXML(result);

            //this.mainForm.criaPonto(txtPonto.Text.ToString(), txtNome.Text.ToString(), 40, 40 + ip);
#endregion
            //ip = ip + 40;

            limpa();

            if (pode)
            {
                MessageBox.Show("Ponto criado com sucesso!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Ponto criado com sucesso, mas o ponto pode estar duplicado!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

       

    private    void atualizar ()
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();

            int index = cBPonto.SelectedIndex;
            if (bttnEditar.Enabled == false && cBPonto.Enabled == false)
            {

                if (txtPonto.Text.Length == 0)
                {
                    MessageBox.Show("Digite o Nome do ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (ckBombaSomente.Checked == false && ckBombaMedidor.Checked == true &&
                    ckNivel.Checked == false && ckVazao.Checked == false)
                {
                    MessageBox.Show("Selecione um tipo de ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (ckBombaMedidor.Checked == true && ckNivel.Checked == false && ckVazao.Checked == false)
                {
                    MessageBox.Show("Selecione um tipo de ponto além da bomba!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int tipo_ponto = 0;
                int bomba_ponto = 0;

                if (ckNivel.Checked == true)
                {
                    tipo_ponto = 0;
                }
                else if (ckVazao.Checked == true)
                {
                    if (cbTipo.Text == "Pulsada")
                        tipo_ponto = 5;
                    else
                        tipo_ponto = 2;
                }
                else if (ckPh.Checked == true)
                {
                    tipo_ponto = 3;
                }
                else if (ckMultK.Checked == true)
                {
                    tipo_ponto = 4;
                }
                else if (ckTurbidez.Checked == true)
                {
                    tipo_ponto = 6;
                }
                else if (ckTurbidez.Checked == true)
                {
                    tipo_ponto = 7;
                }
                else if (ckInversor.Checked == true)
                {
                    tipo_ponto = 8;
                }
                else
                {
                    tipo_ponto = 1;
                }

                if (ckBombaSomente.Checked == true)
                {
                    bomba_ponto = 1;
                    tipo_ponto = 1;
                }
                else if (ckBombaMedidor.Checked == true)
                {
                    bomba_ponto = 1;
                }
                else
                {
                    bomba_ponto = 0;
                }

                string portanivel = "";
                string portabomba = "";
                string faltafase = "";

                try
                {
                    portanivel = porta(cbPorta.SelectedItem.ToString()) == "" ? "0" : porta(cbPorta.SelectedItem.ToString());
                    portabomba = porta(cBpbomba.SelectedItem.ToString()) == "" ? "0" : porta(cBpbomba.SelectedItem.ToString());
                    faltafase = porta(cBfFase.SelectedItem.ToString()) == "" ? "0" : porta(cBfFase.SelectedItem.ToString());
                }
                catch (Exception)
                {

                }

                portanivel = portanivel == "" ? "0" : portanivel;
                portabomba = portabomba == "" ? "0" : portabomba;
                faltafase = faltafase == "" ? "0" : faltafase;

                int cPortaNivel = Convert.ToInt32(portanivel);
                int cPortaBomba = Convert.ToInt32(portabomba);
                int cFaltaFase = Convert.ToInt32(faltafase);

                cPortaNivel = cPortaNivel == 0 ? cPortaNivel : cPortaNivel = cPortaNivel - 1;
                cPortaBomba = cPortaBomba == 0 ? cPortaBomba : cPortaBomba = cPortaBomba - 1;
                cFaltaFase = cFaltaFase == 0 ? cFaltaFase : cFaltaFase = cFaltaFase - 1;


                string adress = txtEnderecoMod.Text == "" ? "0" : txtEnderecoMod.Text;
                string adressIn = txtEnderecoInversor.Text == "" ? "0" : txtEnderecoInversor.Text;
                string max_reservatorio = txtNmax.Text.ToString() == "" ? "0" : txtNmax.Text.ToString();
                string min_reservatorio = txtNmin.Text.ToString() == "" ? "0" : txtNmin.Text.ToString();

                double max = Convert.ToDouble(max_reservatorio);
                double min = Convert.ToDouble(min_reservatorio);



                if (min > max)
                {
                    MessageBox.Show("Valor minimo Invalido !", "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                try
                {
                    dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    bool resultbol = dB.Update("ponto", "nome_ponto = '" + txtPonto.Text + "', ddns = '" + txtDns.Text + "'" + ", ponto_tipo = " + tipo_ponto + ", bomba_ponto = " + bomba_ponto + ", porta_nivel = " + cPortaNivel + ", nivel_min = " + min_reservatorio.Replace(",", ".") + ", porta_rele = " + cFaltaFase + ",max_reservatorio=" + max_reservatorio.Replace(",", ".") + ",nivel_max=" + max_reservatorio.Replace(",", ".") + ", adress = '" + adress + "'" + ", adress_inversor = '" + adressIn + "'", "id_ponto = " + res[index][0]);
                    //bool pode = dB.Insert2("ponto", "nome_ponto, ddns, ponto_tipo, bomba_ponto, porta_nivel, porta_rele, max_reservatorio, nivel_max,adress,adress_inversor", dados.ToString());


                    if (ckBombaSomente.Checked)
                    {
                        //List<string[]> id_ponto = dB.Select("ponto", "id_ponto", "nome_ponto='" + txtPonto.Text + "'");
                        //dB.Insert2("ponto_aciona", "id_ponto, acionar, porta, data_hora", "'" + id_ponto[0][0] + "', '0', '" + portabomba + "', now()");
                        try
                        {

                            bool resultbol2 = dB.Update("ponto_aciona", "porta= " + cPortaBomba, "id_ponto = " + res[index][0]);
                            //dB.Insert2("ponto_aciona", "id_ponto, acionar, porta, data_hora", "'" + id_ponto[0][0] + "', '0', '" + portabomba + "', now()");

                        }
                        catch (Exception er)
                        {

                            MessageBox.Show(er.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        Application.UseWaitCursor = false;
                        Application.DoEvents();
                    }


                    //Console.WriteLine(statusPag);

                    if (statusPag == "atualizar")
                        atualizarInfo(index);
                    else
                        salvarInfo(index);

                    if (resultbol == false)
                    {
                        MessageBox.Show("Erro ao editar dados!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    else
                    {
                        MessageBox.Show("Dados alterados com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Invoke((MethodInvoker)(() => limpa()));
                    }

                }
                catch (Exception er)
                {
                    MessageBox.Show(er.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
             

            }
            else
            {
                MessageBox.Show("Selecione o Ponto e clique no botão  Editar!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int index = cBPonto.SelectedIndex == -1 ? -1 : Convert.ToInt16(res[cBPonto.SelectedIndex][0]);
            abrirTela(index);
        }

        void abrirTela(int ind)
        {
            FrmInfoPonto tela = new FrmInfoPonto(ind);

            if (ind < 0 && btnNovo.Enabled)
                return;
            else
                tela.ShowDialog();

            text = tela.getValores();
            statusPag = tela.getTestStatus();

            //Console.WriteLine("valor "+statusPag);            

        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            btnNovo.Enabled = false;
            btnDelete.Enabled = false;
            bttnEditar.Enabled = false;
            cBPonto.Enabled = false;
            btSalvar.Enabled = true;
            
        }

      private  void salvarInfo(int aux)
        {
            int index = aux + 1;

            try
                {
                    dB = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    bool resultbol = dB.Insert("ponto_informacao", "id_ponto, informacao, data_hora",
                                "'" + index + "', '" + text + "', now()");

                   
               }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

      private  void atualizarInfo(int aux)
        {
            int index = Convert.ToInt32(res[aux][0]);
            bool resultbol2 = false;

                try
                {
                    resultbol2 = dB.Update("ponto_informacao", "informacao = '" + text + "', data_hora = now()", "id_ponto = " + index);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (resultbol2)
                {
                    MessageBox.Show("Infomação de Ponto Atualizada!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Informação do Ponto Não atualizado", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            
            Application.UseWaitCursor = false;
            Application.DoEvents();

        }



    }
}