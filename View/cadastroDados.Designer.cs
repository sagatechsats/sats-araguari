﻿namespace SATS
{
    partial class CadastroDados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroDados));
            this.bttnEditar = new System.Windows.Forms.Button();
            this.bttnCancelar = new System.Windows.Forms.Button();
            this.bttnSalvar = new System.Windows.Forms.Button();
            this.bttnNovo = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNe = new System.Windows.Forms.TextBox();
            this.txtPpoco = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNd = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBitola = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMbomba = new System.Windows.Forms.TextBox();
            this.cBtipoBomba = new System.Windows.Forms.ComboBox();
            this.txtPbomba = new System.Windows.Forms.TextBox();
            this.cBtsBomba = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cBPonto = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBiltolaTubo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnEditar
            // 
            this.bttnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnEditar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_editpaste;
            this.bttnEditar.Location = new System.Drawing.Point(478, 10);
            this.bttnEditar.Name = "bttnEditar";
            this.bttnEditar.Size = new System.Drawing.Size(107, 35);
            this.bttnEditar.TabIndex = 2;
            this.bttnEditar.Text = "Editar";
            this.bttnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnEditar.UseVisualStyleBackColor = true;
            this.bttnEditar.Click += new System.EventHandler(this.bttnEditar_Click);
            // 
            // bttnCancelar
            // 
            this.bttnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnCancelar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_button_cancel;
            this.bttnCancelar.Location = new System.Drawing.Point(363, 435);
            this.bttnCancelar.Name = "bttnCancelar";
            this.bttnCancelar.Size = new System.Drawing.Size(97, 35);
            this.bttnCancelar.TabIndex = 15;
            this.bttnCancelar.Text = "Cancelar";
            this.bttnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnCancelar.UseVisualStyleBackColor = true;
            this.bttnCancelar.Click += new System.EventHandler(this.bttnCancelar_Click);
            // 
            // bttnSalvar
            // 
            this.bttnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnSalvar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_apply1;
            this.bttnSalvar.Location = new System.Drawing.Point(260, 435);
            this.bttnSalvar.Name = "bttnSalvar";
            this.bttnSalvar.Size = new System.Drawing.Size(97, 35);
            this.bttnSalvar.TabIndex = 14;
            this.bttnSalvar.Text = "Salvar";
            this.bttnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnSalvar.UseVisualStyleBackColor = true;
            this.bttnSalvar.Click += new System.EventHandler(this.bttnSalvar_Click);
            // 
            // bttnNovo
            // 
            this.bttnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnNovo.Image = global::SATS.Properties.Resources.Crystal_Clear_action_edit_add;
            this.bttnNovo.Location = new System.Drawing.Point(157, 435);
            this.bttnNovo.Name = "bttnNovo";
            this.bttnNovo.Size = new System.Drawing.Size(97, 35);
            this.bttnNovo.TabIndex = 13;
            this.bttnNovo.Text = "Novo";
            this.bttnNovo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnNovo.UseVisualStyleBackColor = true;
            this.bttnNovo.Click += new System.EventHandler(this.bttnNovo_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dateTimePicker);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtBiltolaTubo);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtNe);
            this.groupBox2.Controls.Add(this.txtPpoco);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtNd);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtBitola);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 212);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(573, 217);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados do Poço:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(350, 136);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 15);
            this.label13.TabIndex = 31;
            this.label13.Text = "Polegadas";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(350, 108);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 15);
            this.label12.TabIndex = 30;
            this.label12.Text = "Metros";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(350, 80);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 15);
            this.label11.TabIndex = 29;
            this.label11.Text = "Metros";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(350, 52);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 15);
            this.label10.TabIndex = 28;
            this.label10.Text = "Metros";
            // 
            // txtNe
            // 
            this.txtNe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNe.Location = new System.Drawing.Point(174, 77);
            this.txtNe.MaxLength = 12;
            this.txtNe.Name = "txtNe";
            this.txtNe.Size = new System.Drawing.Size(169, 22);
            this.txtNe.TabIndex = 9;
            this.txtNe.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNe_KeyPress);
            // 
            // txtPpoco
            // 
            this.txtPpoco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPpoco.Location = new System.Drawing.Point(174, 49);
            this.txtPpoco.MaxLength = 12;
            this.txtPpoco.Name = "txtPpoco";
            this.txtPpoco.Size = new System.Drawing.Size(169, 22);
            this.txtPpoco.TabIndex = 8;
            this.txtPpoco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPpoco.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            this.txtPpoco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPpoco_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(38, 136);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 16);
            this.label9.TabIndex = 27;
            this.label9.Text = "Bitola Hidrômetro:";
            // 
            // txtNd
            // 
            this.txtNd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNd.Location = new System.Drawing.Point(174, 105);
            this.txtNd.MaxLength = 12;
            this.txtNd.Name = "txtNd";
            this.txtNd.Size = new System.Drawing.Size(169, 22);
            this.txtNd.TabIndex = 10;
            this.txtNd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNd_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(51, 108);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 16);
            this.label7.TabIndex = 26;
            this.label7.Text = "Nível Dinâmico:";
            // 
            // txtBitola
            // 
            this.txtBitola.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBitola.Location = new System.Drawing.Point(174, 133);
            this.txtBitola.MaxLength = 12;
            this.txtBitola.Name = "txtBitola";
            this.txtBitola.Size = new System.Drawing.Size(169, 22);
            this.txtBitola.TabIndex = 11;
            this.txtBitola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBitola.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBitola_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(60, 80);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 16);
            this.label6.TabIndex = 25;
            this.label6.Text = "Nivel Estático:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 52);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 16);
            this.label5.TabIndex = 24;
            this.label5.Text = "Profundidade do Poço:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMbomba);
            this.groupBox1.Controls.Add(this.cBtipoBomba);
            this.groupBox1.Controls.Add(this.txtPbomba);
            this.groupBox1.Controls.Add(this.cBtsBomba);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(573, 148);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados da Bomba:";
            // 
            // txtMbomba
            // 
            this.txtMbomba.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMbomba.Location = new System.Drawing.Point(174, 21);
            this.txtMbomba.MaxLength = 50;
            this.txtMbomba.Name = "txtMbomba";
            this.txtMbomba.Size = new System.Drawing.Size(255, 22);
            this.txtMbomba.TabIndex = 3;
            // 
            // cBtipoBomba
            // 
            this.cBtipoBomba.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBtipoBomba.FormattingEnabled = true;
            this.cBtipoBomba.Items.AddRange(new object[] {
            "Monofásica",
            "Bifásica",
            "Trifásica"});
            this.cBtipoBomba.Location = new System.Drawing.Point(174, 77);
            this.cBtipoBomba.Name = "cBtipoBomba";
            this.cBtipoBomba.Size = new System.Drawing.Size(169, 24);
            this.cBtipoBomba.TabIndex = 5;
            // 
            // txtPbomba
            // 
            this.txtPbomba.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPbomba.Location = new System.Drawing.Point(174, 49);
            this.txtPbomba.MaxLength = 30;
            this.txtPbomba.Name = "txtPbomba";
            this.txtPbomba.Size = new System.Drawing.Size(255, 22);
            this.txtPbomba.TabIndex = 4;
            // 
            // cBtsBomba
            // 
            this.cBtsBomba.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBtsBomba.FormattingEnabled = true;
            this.cBtsBomba.Items.AddRange(new object[] {
            "110V",
            "220V",
            "380V"});
            this.cBtsBomba.Location = new System.Drawing.Point(174, 107);
            this.cBtsBomba.Name = "cBtsBomba";
            this.cBtsBomba.Size = new System.Drawing.Size(169, 24);
            this.cBtsBomba.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(44, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 16);
            this.label1.TabIndex = 20;
            this.label1.Text = "Modelo da Bomba:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(62, 80);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 16);
            this.label4.TabIndex = 23;
            this.label4.Text = "Tipo de Bomba:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 16);
            this.label2.TabIndex = 21;
            this.label2.Text = "Potencia da Bomba:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 110);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 16);
            this.label3.TabIndex = 22;
            this.label3.Text = "Tensão da Bomba:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 19);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 16);
            this.label8.TabIndex = 36;
            this.label8.Text = "Ponto:";
            // 
            // cBPonto
            // 
            this.cBPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBPonto.FormattingEnabled = true;
            this.cBPonto.Location = new System.Drawing.Point(62, 16);
            this.cBPonto.Name = "cBPonto";
            this.cBPonto.Size = new System.Drawing.Size(410, 24);
            this.cBPonto.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(350, 165);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 15);
            this.label14.TabIndex = 34;
            this.label14.Text = "Polegadas";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(21, 164);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(132, 16);
            this.label15.TabIndex = 33;
            this.label15.Text = "Bitola da Tubulação:";
            // 
            // txtBiltolaTubo
            // 
            this.txtBiltolaTubo.Enabled = false;
            this.txtBiltolaTubo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBiltolaTubo.Location = new System.Drawing.Point(174, 162);
            this.txtBiltolaTubo.MaxLength = 12;
            this.txtBiltolaTubo.Name = "txtBiltolaTubo";
            this.txtBiltolaTubo.Size = new System.Drawing.Size(169, 22);
            this.txtBiltolaTubo.TabIndex = 12;
            this.txtBiltolaTubo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBiltolaTubo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBitola_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(29, 26);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(124, 16);
            this.label16.TabIndex = 35;
            this.label16.Text = "Data da Instalação:";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Enabled = false;
            this.dateTimePicker.Location = new System.Drawing.Point(174, 21);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(219, 22);
            this.dateTimePicker.TabIndex = 7;
            // 
            // cadastroDados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(597, 482);
            this.Controls.Add(this.bttnEditar);
            this.Controls.Add(this.bttnCancelar);
            this.Controls.Add(this.bttnSalvar);
            this.Controls.Add(this.bttnNovo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cBPonto);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(613, 521);
            this.MinimumSize = new System.Drawing.Size(613, 521);
            this.Name = "cadastroDados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar Dados do Poço";
            this.Load += new System.EventHandler(this.cadastroDados_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bttnEditar;
        private System.Windows.Forms.Button bttnCancelar;
        private System.Windows.Forms.Button bttnSalvar;
        private System.Windows.Forms.Button bttnNovo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtNe;
        private System.Windows.Forms.TextBox txtPpoco;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBitola;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtMbomba;
        private System.Windows.Forms.ComboBox cBtipoBomba;
        private System.Windows.Forms.TextBox txtPbomba;
        private System.Windows.Forms.ComboBox cBtsBomba;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cBPonto;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBiltolaTubo;
    }
}