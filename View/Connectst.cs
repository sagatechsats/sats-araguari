﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class Connectst : Office2007Form
    {
        protected DBManager dbManager;
        [DllImport("wininet.dll")]

        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        public static Boolean IsConnected()
        {

            bool result;

            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public Connectst()
        {
            InitializeComponent();
        }

        string EncryptOrDecrypt(string text, string key)
        {
            var result = new StringBuilder();

            for (int c = 0; c < text.Length; c++)
                result.Append((char)((uint)text[c] ^ (uint)key[c % key.Length]));

            return result.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            if (IsConnected() == true)
            {
                try
                {
                    if (txtDbName.Text.Length > 0 && txtDbUser.Text.Length > 0 && txtDbPass.Text.Length > 0)
                    {

                        string dado = EncryptOrDecrypt("Server="+txtDbIp.Text+";port=3306;Database=" + txtDbName.Text + ";Uid=" + txtDbUser.Text + ";Pwd=" + txtDbPass.Text + ";", "saga*245");

                        dbManager = new DBManager(dado);
                        if (dbManager.OpenConnection() == true)
                        {
                            dbManager.CloseConnection();
                            System.IO.File.WriteAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector", dado);

                            this.Hide();
                            Login telaInicial = new Login();
                            telaInicial.Show();
                        }
                        else
                        {
                            MessageBox.Show("Os dados para conexão com o Banco de dados estão incorretos",
                                "ERRO",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);

                            txtDbPass.Text = "";
                        }

                    }
                    else MessageBox.Show("Para utilizar o SATS é necessário o preenchimento correto da de conexão",
                        "Preencha os campos",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {

                }
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
