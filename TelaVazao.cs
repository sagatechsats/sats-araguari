﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public partial class TelaVazao : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
        private int X = 0;
        private int Y = 0;
        private int id = 0;
        private string nome = "";
        private String Status;
        List<string[]> Ponto;

        public TelaVazao(int id, string nomep, int bomba, int tipodados, float nivel_max, string status)
        {
            InitializeComponent();
            Status = status;
            this.id = id;
            nome = nomep;
            vazao(id);
            verifica();
        }

        public static bool IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public void verifica()
        {

            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                Ponto = DatabaseManager.Select("ponto", "nome_ponto", "id_ponto= '" + id + "'");
                //Evento = DatabaseManager.Select("ponto_eventos", "tipo_evento, descricao, max(data_hora)", "id_ponto= '" + id + "'");
                passaResultadoTela();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void vazao(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                //List<string[]> leituravazao = DatabaseManager.Select("ponto_leitura", "vazao/3600, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                List<string[]> leituramaxvazao = DatabaseManager.Select("ponto", "nivel_max/1000", "id_ponto=" + id);
                List<string[]> leituravazao = DatabaseManager.SelectInnerJoin2(" ponto_leitura l ", "l.vazao/1000, p.nome_ponto, l.data_hora, l.id_ponto ", "  ponto p ", " p.id_ponto = l.id_ponto ", " p.id_ponto = '" + id + "' and l.id_leitura in (select max(id_leitura) as id_leitura from ponto_leitura group by id_ponto desc)");
                float vazao = float.Parse(leituravazao[0][0]);
                float maxVazao = float.Parse(leituramaxvazao[0][0]);
                SetLed(float.Parse(leituravazao[0][0]), leituravazao[0][2]);
                AutoValidate = AutoValidate.EnablePreventFocusChange;
                aquaGaugeVazao.MaxValue = maxVazao;
                aquaGaugeVazao.Value = vazao;
                aquaGaugeVazao.DigitalValue = vazao;
                //aquaGaugeVazao.Tanque1.LabelValue.Text = vazao.ToString() + " m³/H";
                //aquaGaugeVazao.Value = vazao;
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atualização: " + leituravazao[0][1];
                }
                else
                {
                    labelHora.Text = "Última atualização: " + leituravazao[0][1] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }
        public void passaResultadoTela()
        {
            labelPonto.Text = "Ponto " + Ponto[0][0];
        }

        public void SetLed(float vazao, string datahora)
        {
            decimal stempo = 0;

            DateTime datanow = DateTime.Now;
            DateTime data = DateTime.Now;
            try
            {
                data = Convert.ToDateTime(datahora);
            }
            catch (Exception)
            {
            }

            TimeSpan ts = datanow.Subtract(data);
            stempo = Convert.ToDecimal(ts.TotalMinutes);


            if (vazao == 0)
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                Invoke((MethodInvoker)(() => pictureBox3.Visible = false));
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    Invoke((MethodInvoker)(() => pictureBox3.Visible = true));
                }
            }
            else
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                Invoke((MethodInvoker)(() => pictureBox3.Visible = false));
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    Invoke((MethodInvoker)(() => pictureBox3.Visible = true));
                }
            }
        }


        public static DialogResult show(string ponto, int bomba, string alarmeE, string descricao, string horaAE, int identificador, int tipodados, string nivel_max, string status)
        {
            if (IsConnected() == true)
            {
                TelaVazao MsgBox = new TelaVazao(identificador, ponto, bomba, tipodados, float.Parse(nivel_max), status);
                //MsgBox.AutoValidate = AutoValidate.EnablePreventFocusChange;
                //MsgBox.aquaGaugeVazao.DigitalValue = vazao / 1000;
                //MsgBox.aquaGaugeVazao.Value = vazao / 1000;
                //MsgBox.sevenSvolume.Value = (volume / 1000).ToString().PadLeft(7, '0');
                //MsgBox.label2.Text = "Última atulização: " + datahoraleitura;
                MsgBox.labelPonto.Text = ponto;
                MsgBox.labelAevento.Text = "Último Alarme ou Evento: " + alarmeE;
                MsgBox.labelAhora.Text = "Hora: " + horaAE;
                MsgBox.labelAdescricao.Text = "Descrição: " + descricao;
                DialogResult result = MsgBox.ShowDialog();
                return result;
            }
            else
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return DialogResult.Cancel;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmInformacao frm = new FrmInformacao(id,Ponto[0][0]);
        }
    }
}
