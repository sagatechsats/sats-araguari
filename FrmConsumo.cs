﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using DevComponents.DotNetBar;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;

namespace SATS
{
    public partial class FrmConsumo : Office2007Form
    {
        private int data1 = 0;
        private int type = 1;
        private int grafico = 1;

        List<string[]> paraGrafico;
        public FrmConsumo(List<string[]> resultGrafics)
        {
            this.paraGrafico = resultGrafics;
            InitializeComponent();
        }

        private void FrmConsumo_Load(object sender, EventArgs e)
        {
           
            this.WindowState = FormWindowState.Maximized;
        }
        
        public void gerarGrafico()
        {
            try {
                Invoke(new MethodInvoker(() => cartesianChart1.Series.Clear()));
                Invoke(new MethodInvoker(() => cartesianChart1.AxisX.Clear()));
                Invoke(new MethodInvoker(() => cartesianChart1.AxisY.Clear()));
                var datas = new List<string>();

                var chart = new ChartValues<double>();

                foreach (var l in paraGrafico)
                {
                    // _date_time = Convert.ToDateTime(l[2]);
                    // _leitura = Convert.ToDouble(l[1]);
                    chart.Add(Convert.ToDouble(l[1]));
                    datas.Add(l[2]);

                }
                //  valorLeitura.Add(_date_time, _leitura);
                //  var datas = _date_time;
                //  var chart = new ChartValues<double>();

                // valorLeitura.Add(l);

                //foreach (var item in valorLeitura)
                //{
                //    chart.Add(item.valor);
                //}

                if (grafico == 1)
                {
                    Invoke(new MethodInvoker(() => cartesianChart1.Series = new LiveCharts.SeriesCollection
                                {
                                    new LineSeries
                                    {
                                        Title = "Consumo",
                                        Values = chart,
                                        PointGeometry = DefaultGeometries.Circle,
                                        PointGeometrySize = 10,
                                        LineSmoothness = 0.3
                                    },
                                    //new LineSeries
                                    //{
                                    //    Title = "Despesa",
                                    //    Values = chart1,
                                    //    PointGeometry = DefaultGeometries.Circle,
                                    //    PointGeometrySize = 10,
                                    //    LineSmoothness = 0.2
                                    //}
                                }));
                }
                else if (grafico == 2)
                {
                    Invoke(new MethodInvoker(() => cartesianChart1.Series = new LiveCharts.SeriesCollection
                                {
                                    new ColumnSeries
                                    {
                                        Title = "Consumo",
                                        Values = chart,
                                    },

                                }));
                }

                Invoke(new MethodInvoker(() => cartesianChart1.AxisX.Add(new LiveCharts.Wpf.Axis
                {
                    Title = "Data e Hora",
                    Labels = datas
                })));
                Invoke(new MethodInvoker(() => cartesianChart1.AxisY.Clear()));
                Invoke(new MethodInvoker(() => cartesianChart1.AxisY.Add(new LiveCharts.Wpf.Axis
                {
                    Title = "Consumo",
                    //  LabelFormatter = value => value.ToString("C")
                })));
                Invoke(new MethodInvoker(() => cartesianChart1.LegendLocation = LegendLocation.Right));
                Invoke(new MethodInvoker(() => this.Cursor = Cursors.Default));
            }
            catch
            {

            }
            }
    }
}
