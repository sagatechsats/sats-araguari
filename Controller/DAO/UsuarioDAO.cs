﻿using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using SATS.Model;

namespace SATS.Controller.DAO
{
    class UsuarioDAO
    {
        private MySqlConnection connection;
        private bool statusConexao;

        //Verificador de nome de Usuário para que não haja dois usuário com o mesmo nome
        public int VerificaUsuario(string UserName)
        {
            BD acessoBd = new BD();
            // Objeto que realiza conexao com o Banco de Dados
            statusConexao = acessoBd.OpenConnection();
            // Ponteiro de conexao com a conexao retornada pela tentativa de conexao
            connection = acessoBd._Connection;
            //verificação do estado da conexao
            // Parametros e Querys a serem usadas
            var query = "SELECT Count(*) FROM sigt_user where sigt_user.nome_user = " + "'" + UserName + "';";
            int cont = -1;
            //Realiza a instrução da query se, e somente se, houver conexao aberta
            if (statusConexao)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cont = int.Parse(cmd.ExecuteScalar().ToString());
            }

            acessoBd.CloseConnection();
            return cont;
        }
        public bool CadastraNovo(UsuarioModel novoDao)
        {
            BD acessoBd = new BD();
            // Objeto que realiza conexao com o Banco de Dados
            statusConexao = acessoBd.OpenConnection();
            // Ponteiro de conexao com a conexao retornada pela tentativa de conexao
            connection = acessoBd._Connection;
            //verificação do estado da conexao
            // Parametros e Querys a serem usadas
            string query = "INSERT INTO sigt_user (nome_user, senha_user, permissao_user) VALUES (" + "'" + novoDao.Usuario + "','" + novoDao.Senha + "','" + novoDao.Permissao + "')";
            if (statusConexao)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            acessoBd.CloseConnection();
            return false;
        }
        public string Login(UsuarioModel userModel)
        {
            BD acessoBd = new BD();
            // Objeto que realiza conexao com o Banco de Dados
            statusConexao = acessoBd.OpenConnection();
            // Ponteiro de conexao com a conexao retornada pela tentativa de conexao
            connection = acessoBd._Connection;
            //verificação do estado da conexao
            // Parametros e Querys a serem usadas
            SqlParameter param = new SqlParameter();
            UsuarioModel buscaModel = new UsuarioModel();
            string senha = null;
            string query = "SELECT * FROM sats_user WHERE nome_user = '" + userModel.Usuario + "'";
            if (statusConexao)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    buscaModel.Usuario = dataReader.GetString(0);
                    senha = dataReader.GetString(1);
                    buscaModel.Permissao = dataReader.GetString(2);
                }
                if (userModel.Senha == senha)
                {
                    return buscaModel.Permissao;
                }
                if (userModel.Senha != senha)
                {
                    return "ERRO SENHA";
                }
            }
            acessoBd.CloseConnection();
            return "ERRO";
        }
    }
}
