﻿using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace SATS.Controller
{
    class VerificaConexao
    {

        public bool StatusServidor()
        {

            var ping = new Ping();

            try
            {
                var reply = ping.Send("sagamedicao.com.br");

            }
            catch (PingException)
            {
                MessageBox.Show("ERRO NA REDE");
                return false;
            }

            return true;

        }


    }
}
