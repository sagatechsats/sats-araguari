﻿using System;


namespace SATS.Controller

{
    class Worker
    {
        public void DoWork()
        {
            while (!_shouldStop)
            {
                VerificaConexao statusConexao = new VerificaConexao();
                bool status = statusConexao.StatusServidor();
                Console.WriteLine("this....");
                if (!status)
                {
                    Console.WriteLine("this not found....");
                }
            }
            Console.WriteLine("worker thread: terminating gracefully.");
        }
        public void RequestStop()
        {
            _shouldStop = true;
        }
        // Volatile is used as hint to the compiler that this data
        // member will be accessed by multiple threads.
        private volatile bool _shouldStop;
    }
}
