﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public partial class TelaInversor : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
        private int X = 0;
        private int Y = 0;
        private int id = 0;
        private string nome = "";
        private bool acionado;
        private bool acionadoAutomatico;
        private int acionamento = 2;
        private Thread Threadupdate;
        private int TipoDados;
        private float NivelMax;
        private String Status;
        List<string[]> Ponto;

        public TelaInversor(int id, string nomep, int bomba, int tipodados, float nivel_max, string status)
        {
            InitializeComponent();
            this.id = id;
            Status = status;
            nome = nomep;
            verifica();
            aquaGaugeInversor.Visible = true;
            inversor(id);
            


        }

        public void verifica()
        {

            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                Ponto = DatabaseManager.Select("ponto", "nome_ponto", "id_ponto= '" + id + "'");
                //Evento = DatabaseManager.Select("ponto_eventos", "tipo_evento, descricao, max(data_hora)", "id_ponto= '" + id + "'");
                passaResultadoTela();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        public void passaResultadoTela()
        {
            labelPonto.Text = "Ponto " + Ponto[0][0];
        }

        public static bool IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public void SetLed(float vazao, string datahora)
        {
            decimal stempo = 0;

            DateTime datanow = DateTime.Now;
            DateTime data = DateTime.Now;
            try
            {
                data = Convert.ToDateTime(datahora);
            }
            catch (Exception)
            {
            }

            TimeSpan ts = datanow.Subtract(data);
            stempo = Convert.ToDecimal(ts.TotalMinutes);


            if (vazao == 0)
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                pictureBox3.Visible = false; 
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    //pictureBox3.Visible = true;
                }
            }
            else
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                pictureBox3.Visible = false;
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    //pictureBox3.Visible = true;
                }
            }
        }


        public static DialogResult show(string ponto, int bomba, string alarmeE, string descricao, string horaAE, int identificador, int tipodados, string nivel_max, string status)
        {
            if (IsConnected() == true)
            {
                TelaInversor MsgBox = new TelaInversor(identificador, ponto, bomba, tipodados, float.Parse(nivel_max), status);
                //MsgBox.AutoValidate = AutoValidate.EnablePreventFocusChange;
                //MsgBox.aquaGaugeVazao.DigitalValue = vazao / 1000;
                //MsgBox.aquaGaugeVazao.Value = vazao / 1000;
                //MsgBox.sevenSvolume.Value = (volume / 1000).ToString().PadLeft(7, '0');
                //MsgBox.label2.Text = "Última atulização: " + datahoraleitura;
                MsgBox.labelPonto.Text = ponto;
                MsgBox.labelAevento.Text = "Último Alarme ou Evento: " + alarmeE;
                MsgBox.labelAhora.Text = "Hora: " + horaAE;
                MsgBox.labelAdescricao.Text = "Descrição: " + descricao;
                DialogResult result = MsgBox.ShowDialog();
                return result;
            }
            else
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return DialogResult.Cancel;
            }
        }

        private void inversor(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituraInversor = DatabaseManager.Select("ponto_inversor", "frequencia, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                //List<string[]> leituramaxPressao = DatabaseManager.Select("ponto", "ponto", "id_ponto=" + id);
                float Inversor = float.Parse(leituraInversor[0][0]);
                //float maxPressao = float.Parse(leituramaxPressao[0][0]);
                float maxFrequencia = 60;
                SetLed(float.Parse(leituraInversor[0][0]), leituraInversor[0][1]);
                AutoValidate = AutoValidate.EnablePreventFocusChange;
                aquaGaugeInversor.MaxValue = maxFrequencia;
                aquaGaugeInversor.Value = Inversor;
                aquaGaugeInversor.DigitalValue = Inversor;
                //aquaGaugeVazao.Tanque1.LabelValue.Text = vazao.ToString() + " m³/H";
                //aquaGaugeVazao.Value = vazao;
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atulização: " + leituraInversor[0][1];
                }
                else
                {
                    labelHora.Text = "Última atulização: " + leituraInversor[0][1] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }

        private void buttonFecha_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            X = this.Left - MousePosition.X;
            Y = this.Top - MousePosition.Y;

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            this.Left = X + MousePosition.X;
            this.Top = Y + MousePosition.Y;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmInformacao frm = new FrmInformacao(id, Ponto[0][0]);
            frm.Show();
        }
    }
}
