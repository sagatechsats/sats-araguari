﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using DevComponents.DotNetBar;
using System.Runtime.InteropServices;

namespace SATS
{
    public partial class SearchBalaco : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private DBManager dbManager;
        private string tipoponto;
        private string mes;
        private string ano;
        private static List<string[]> listaretorno;
        private List<string[]> Resultlist;
        private bool busca = false;


        public SearchBalaco()
        {
            InitializeComponent();
            listView.View = View.Details;
            listView.GridLines = true;
            listView.FullRowSelect = true;
            busca = false;

        }

        public static DialogResult show(ref List<string[]> rLista)
        {
            SearchBalaco msbox = new SearchBalaco();
            listaretorno = new List<string[]>();

            DialogResult dialogResult = msbox.ShowDialog();

            rLista = listaretorno;
            return dialogResult;
        }

        private void SearchBalaco_Load(object sender, EventArgs e)
        {

            dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            busca = false;

        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int index;

                index = listView.SelectedItems[0].Index;
                listaretorno.Add(Resultlist[index]);



            }
            catch (Exception)
            {
                MessageBox.Show("É necessário selecionar o balanço hídrico.",
                "Selecione o balanço hídrico!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);


            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            tipoponto = "";
            mes = "";
            ano = "";

            if (comboBoxCliente.SelectedIndex == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar o tipo do ponto.",
                "Selecione o tipo do ponto!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (comboBoxMes.SelectedIndex == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar o mês.",
                "Selecione o mês!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (comboBoxAno.SelectedIndex == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar o ano.",
                "Selecione o ano!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (comboBoxCliente.SelectedIndex == 0) tipoponto = "producao";
            else tipoponto = "processo";
            mes = (comboBoxMes.SelectedIndex + 1).ToString();
            ano = comboBoxAno.SelectedItem.ToString();

            Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();


        }

        protected void FillListView(List<string[]> resultList)
        {

            ListViewItem listViewItem;
            listView.Items.Clear();
            foreach (string[] item in resultList)
            {
                listViewItem = new ListViewItem(item);
                listView.Items.Add(listViewItem);
            }
        }

        private List<string[]> simpleDate(List<string[]> resultList)
        {
            for (int i = 0; i < resultList.Count; i++)
            {
                resultList[i][0] = resultList[i][0].Substring(0, resultList[i][0].Length - 9);
                resultList[i][1] = resultList[i][1].Substring(0, resultList[i][1].Length - 9);
            }
            return resultList;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {



                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    List<string[]> resultlista = dbManager.Select("balanco_h", "data_ini, data_fim, ponto_tipo, volume_total, consumo_mf, consumo_ef, consumo_mnf, consumo_enf, consumo_na, erro_medicao, vazamento_redes, vazmento_reserv"
                , "ponto_tipo='" + tipoponto + "' and month(data_ini)='" + mes + "' and year(data_ini)='" + ano + "' order by data_ini asc");

                    if (resultlista.Count <= 0)
                    {

                        Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum resultado para a pesquisa.",
                            "Aviso: Nenhum resultado encontrado.",

                            MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                        Application.UseWaitCursor = false;

                        Application.DoEvents();
                        busca = false;
                        return;

                    }
                    Resultlist = simpleDate(resultlista);

                    Invoke((MethodInvoker)(() => FillListView(Resultlist)));
                    busca = true;

                }
                catch (Exception)
                {
                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
                dbManager.CloseConnection();

            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public static Boolean IsConnected()
        {

            bool result;

            try
            {

                int Description;

                result = InternetGetConnectedState(out Description, 0);

            }

            catch
            {

                result = false;

            }

            return result;

        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (busca == true)
            {
                buttonOK.DialogResult = DialogResult.OK;
            }
        }

    }
}
