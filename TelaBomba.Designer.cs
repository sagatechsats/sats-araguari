﻿namespace SATS
{
    partial class TelaBomba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAcionamentoAutomatico = new System.Windows.Forms.Label();
            this.labelAciona = new System.Windows.Forms.Label();
            this.labelAhora = new System.Windows.Forms.Label();
            this.labelAdescricao = new System.Windows.Forms.Label();
            this.labelAevento = new System.Windows.Forms.Label();
            this.labelHora = new System.Windows.Forms.Label();
            this.labelPonto = new System.Windows.Forms.Label();
            this.btnAcionamentoAutomatico = new System.Windows.Forms.Button();
            this.buttonAciona = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonFecha = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAcionamentoAutomatico
            // 
            this.lblAcionamentoAutomatico.AutoSize = true;
            this.lblAcionamentoAutomatico.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblAcionamentoAutomatico.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcionamentoAutomatico.Location = new System.Drawing.Point(36, 145);
            this.lblAcionamentoAutomatico.Name = "lblAcionamentoAutomatico";
            this.lblAcionamentoAutomatico.Size = new System.Drawing.Size(226, 24);
            this.lblAcionamentoAutomatico.TabIndex = 46;
            this.lblAcionamentoAutomatico.Text = "Acionamento Automatico:";
            // 
            // labelAciona
            // 
            this.labelAciona.AutoSize = true;
            this.labelAciona.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAciona.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAciona.Location = new System.Drawing.Point(39, 111);
            this.labelAciona.Name = "labelAciona";
            this.labelAciona.Size = new System.Drawing.Size(127, 24);
            this.labelAciona.TabIndex = 44;
            this.labelAciona.Text = "Acionamento:";
            // 
            // labelAhora
            // 
            this.labelAhora.AutoSize = true;
            this.labelAhora.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAhora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAhora.Location = new System.Drawing.Point(38, 215);
            this.labelAhora.Name = "labelAhora";
            this.labelAhora.Size = new System.Drawing.Size(41, 16);
            this.labelAhora.TabIndex = 40;
            this.labelAhora.Text = "Hora:";
            // 
            // labelAdescricao
            // 
            this.labelAdescricao.AutoSize = true;
            this.labelAdescricao.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAdescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdescricao.Location = new System.Drawing.Point(39, 250);
            this.labelAdescricao.MaximumSize = new System.Drawing.Size(290, 65);
            this.labelAdescricao.MinimumSize = new System.Drawing.Size(290, 65);
            this.labelAdescricao.Name = "labelAdescricao";
            this.labelAdescricao.Size = new System.Drawing.Size(290, 65);
            this.labelAdescricao.TabIndex = 41;
            this.labelAdescricao.Text = "Descrição:";
            // 
            // labelAevento
            // 
            this.labelAevento.AutoSize = true;
            this.labelAevento.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelAevento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAevento.Location = new System.Drawing.Point(38, 186);
            this.labelAevento.Name = "labelAevento";
            this.labelAevento.Size = new System.Drawing.Size(158, 16);
            this.labelAevento.TabIndex = 42;
            this.labelAevento.Text = "Último Alarme ou Evento:";
            // 
            // labelHora
            // 
            this.labelHora.AutoSize = true;
            this.labelHora.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHora.Location = new System.Drawing.Point(38, 56);
            this.labelHora.Name = "labelHora";
            this.labelHora.Size = new System.Drawing.Size(119, 16);
            this.labelHora.TabIndex = 43;
            this.labelHora.Text = "Última Atualização";
            // 
            // labelPonto
            // 
            this.labelPonto.AutoSize = true;
            this.labelPonto.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPonto.Location = new System.Drawing.Point(104, 12);
            this.labelPonto.Name = "labelPonto";
            this.labelPonto.Size = new System.Drawing.Size(68, 25);
            this.labelPonto.TabIndex = 37;
            this.labelPonto.Text = "Ponto";
            // 
            // btnAcionamentoAutomatico
            // 
            this.btnAcionamentoAutomatico.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnAcionamentoAutomatico.BackgroundImage = global::SATS.Properties.Resources.switchoff;
            this.btnAcionamentoAutomatico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAcionamentoAutomatico.Enabled = false;
            this.btnAcionamentoAutomatico.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnAcionamentoAutomatico.FlatAppearance.BorderSize = 0;
            this.btnAcionamentoAutomatico.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAcionamentoAutomatico.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnAcionamentoAutomatico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAcionamentoAutomatico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAcionamentoAutomatico.ForeColor = System.Drawing.Color.Black;
            this.btnAcionamentoAutomatico.Location = new System.Drawing.Point(288, 141);
            this.btnAcionamentoAutomatico.Name = "btnAcionamentoAutomatico";
            this.btnAcionamentoAutomatico.Size = new System.Drawing.Size(88, 28);
            this.btnAcionamentoAutomatico.TabIndex = 47;
            this.btnAcionamentoAutomatico.UseVisualStyleBackColor = false;
            this.btnAcionamentoAutomatico.Click += new System.EventHandler(this.btnAcionamentoAutomatico_Click);
            // 
            // buttonAciona
            // 
            this.buttonAciona.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonAciona.BackgroundImage = global::SATS.Properties.Resources.switchoff;
            this.buttonAciona.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonAciona.Enabled = false;
            this.buttonAciona.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonAciona.FlatAppearance.BorderSize = 0;
            this.buttonAciona.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonAciona.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonAciona.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAciona.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAciona.ForeColor = System.Drawing.Color.Black;
            this.buttonAciona.Location = new System.Drawing.Point(288, 107);
            this.buttonAciona.Name = "buttonAciona";
            this.buttonAciona.Size = new System.Drawing.Size(88, 28);
            this.buttonAciona.TabIndex = 39;
            this.buttonAciona.UseVisualStyleBackColor = false;
            this.buttonAciona.Click += new System.EventHandler(this.buttonAciona_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button4.BackgroundImage = global::SATS.Properties.Resources.crystal_clear_action_playlist__2_;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(406, 56);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(41, 41);
            this.button4.TabIndex = 45;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pictureBox2.BackgroundImage = global::SATS.Properties.Resources.ledgray;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(406, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(41, 38);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 38;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SATS.Properties.Resources.next1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::SATS.Properties.Resources.next1;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(-2, -8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(477, 346);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // buttonFecha
            // 
            this.buttonFecha.BackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.BackgroundImage = global::SATS.Properties.Resources.Crystal_Clear_action_button_cancel;
            this.buttonFecha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFecha.FlatAppearance.BorderSize = 0;
            this.buttonFecha.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonFecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFecha.Location = new System.Drawing.Point(460, 1);
            this.buttonFecha.Name = "buttonFecha";
            this.buttonFecha.Size = new System.Drawing.Size(15, 15);
            this.buttonFecha.TabIndex = 4;
            this.buttonFecha.UseVisualStyleBackColor = false;
            this.buttonFecha.Click += new System.EventHandler(this.buttonFecha_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pictureBox3.Image = global::SATS.Properties.Resources.desconect_img;
            this.pictureBox3.Location = new System.Drawing.Point(31, 40);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(360, 261);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 48;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // TelaBomba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(479, 331);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.btnAcionamentoAutomatico);
            this.Controls.Add(this.lblAcionamentoAutomatico);
            this.Controls.Add(this.buttonAciona);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.labelAciona);
            this.Controls.Add(this.labelAhora);
            this.Controls.Add(this.labelAdescricao);
            this.Controls.Add(this.labelAevento);
            this.Controls.Add(this.labelHora);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.labelPonto);
            this.Controls.Add(this.buttonFecha);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TelaBomba";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tela Bomba";
            this.TransparencyKey = System.Drawing.SystemColors.GradientInactiveCaption;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAcionamentoAutomatico;
        private System.Windows.Forms.Label lblAcionamentoAutomatico;
        private System.Windows.Forms.Button buttonAciona;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label labelAciona;
        private System.Windows.Forms.Label labelAhora;
        private System.Windows.Forms.Label labelAdescricao;
        private System.Windows.Forms.Label labelAevento;
        private System.Windows.Forms.Label labelHora;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labelPonto;
        private System.Windows.Forms.Button buttonFecha;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}