﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Diagnostics;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class RelatorioConsumo : Office2007Form
    {

        [DllImport("wininet.dll")]

        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        protected DBManager dbManager;
        protected List<string> listaClientes;
        protected List<string> indexClientes;
        List<string[]> result2 = new List<string[]>();

        // so id_clientes para busca as posicoes sao correspondentes a listaClientes
        // atributos para armazenar ulltima busca valida. Servem para nao gerar
        // relatorios inconsistentes entre  a listView e o cliente.

        protected string ultCliente;
        protected string ultDateIn;
        protected string ultDateFi;
        protected int ultClienteIndex;

        private String cliente;

        public RelatorioConsumo()
        {
            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
            InitDateTimes();
            //Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            //novaThread.Start();

        }


        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void InitDateTimes()//ja que o designer insiste em mudar os valores que eu ponho...
        {
            dateTimeInicial.Value = System.DateTime.Today;
            dateTimeFinal.Value = System.DateTime.Today;
            dateTimeFinal.Value = dateTimeFinal.Value.AddHours(23);
            dateTimeFinal.Value = dateTimeFinal.Value.AddMinutes(59);
            dateTimeFinal.Value = dateTimeFinal.Value.AddSeconds(59);
            dateTimeInicial.MaxDate = System.DateTime.Today;
            dateTimeInicial.Value = System.DateTime.Today;
            dateTimeInicial.Value = System.DateTime.Today;
        }

        protected void StoreSearchData()
        {
            ultCliente = comboBoxCliente.SelectedItem.ToString();
            ultDateIn = dateTimeInicial.Value.ToShortDateString();
            ultDateFi = dateTimeFinal.Value.ToShortDateString();
            ultClienteIndex = comboBoxCliente.SelectedIndex;
        }

        // Obtem os clientes a partir do banco de dados e retorna como uma lista de strings
        // Cada item e composto pelo id do cliente(sempre 6 casas, com zeros a esquerda) e o
        // nome concatenado.

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select2("ponto", "id_ponto, nome_ponto", "ponto_tipo='7'", "id_ponto");

            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];

                listConcat.Add(concat);
            }
            return listConcat;
        }

        protected void PassListToComboBox(List<string> list)
        {

            foreach (string concat in list)
            comboBoxCliente.Items.Add(concat);
        }
        // Usado para habilitar o recurso de auto-completar.
        protected void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
            comboBoxCliente.AutoCompleteCustomSource = stringCollection;
        }

        protected void FillListView(List<string[]> resultList)
        {
            ListViewItem listViewItem;
            listView.Items.Clear();
            foreach (string[] item in resultList)
            {
                listViewItem = new ListViewItem(item);
                listView.Items.Add(listViewItem);
            }
        }

        protected virtual bool CheckDateValidity()
        {
            if (dateTimeFinal.Value < dateTimeInicial.Value)
            {
                MessageBox.Show("Data Inicial deve ser menor ou igual à Data Final.",
                    "Período Inválido");
                return false;
            }
            return true;
        }

        // Salva um arquivo de texto puro, com seu conteudo a partir de uma string
        // utilizada na classe.  
        protected virtual void SaveFile(string content)
        {
            System.Windows.Forms.SaveFileDialog saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            saveFileDialog1.Filter = "Arquivos txt (*.txt)|*.txt";
            //saveFileDialog1.Filter = "Arquivos txt (*.txt)|*.txt|Todos os arquivos (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                    sw.WriteLine(content);
            }
        }

        protected void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        protected void buttonTxt_Click(object sender, EventArgs e)
        {
            //SaveFile("asdasd");
        }

        protected void buttonLimpar_Click(object sender, EventArgs e)
        {
            listView.Items.Clear();
            labelListView.Text = "Lista de Resultados Vazia";

        }
        public void CarregaTela()
        {
            if (comboBoxCliente.Items.Count <= 1)
            {
                if (IsConnected() == true)
                {
                    Application.UseWaitCursor = true;
                    Application.DoEvents();

                    try
                    {
                        dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                        List<string> listaClientes = new List<string>();
                        listaClientes = GetClientes();
                        Invoke((MethodInvoker)(() => PassListToComboBox(listaClientes)));
                        Invoke((MethodInvoker)(() => PassListToAutoComplete(listaClientes)));
                    }
                    catch (Exception)
                    {
                    }
                    Application.UseWaitCursor = false;
                    Application.DoEvents();
                }
                else if (IsConnected() == false)
                {
                    MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }

        }

        public static Boolean IsConnected()
        {

            bool result;

            try
            {

                int Description;

                result = InternetGetConnectedState(out Description, 0);

            }

            catch
            {

                result = false;

            }

            return result;

        }

        private void dateTimeInicial_ValueChanged(object sender, EventArgs e)
        {
            dateTimeFinal.MinDate = Convert.ToDateTime(dateTimeInicial.Value);
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            ////result2.Clear();
            try
            {
                result2.Clear();

            }
            catch (Exception)
            {
            }
            listView.Items.Clear();
            if (CheckDateValidity() == false)
                return;
            cliente = comboBoxCliente.Text;
            int index = comboBoxCliente.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar-se um ponto.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            index = Convert.ToInt32(comboBoxCliente.Text.Substring(0, 6));
            StoreSearchData();
            Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTela));
            novaThread.Start(index);


            labelListView.Text = "Resultados para Ponto:" + ultCliente + " de " + ultDateIn +
                " a " + ultDateFi;
        }


        private void CarregaTela(object parametro)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            try
            {
                int index = Convert.ToInt32(parametro);
                if (checkBox1.Checked == true)
                {

                    try
                    {
                        dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));

                        List<string[]> result;
                        result = dbManager.Select("ponto_inversor_dados inner join ponto on ponto_inversor_dados.id_ponto = ponto.id_ponto", "nome_ponto, avg(power), data_hora",
                            "data_hora >= '" + dateTimeInicial.Value.ToString("s") + "'  and data_hora<='" + dateTimeFinal.Value.ToString("s") + "' and ponto_inversor_dados.id_ponto ='" + index + "'"
                            + "GROUP BY ponto_inversor_dados.id_ponto, month(data_hora), day(data_hora), hour(data_hora)");


                        if (result.Count <= 0)
                        {
                            Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum resultado para o ponto selecionado.",
                                "Aviso: Nenhum resultado encontrado.",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                            Application.UseWaitCursor = false;

                            Application.DoEvents();
                            return;

                        }
                        result2 = result;
                        Invoke((MethodInvoker)(() => FillListView(result)));


                    }
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    List<string[]> result;

                    result = dbManager.Select("ponto_inversor_dados pp inner join ponto p on pp.id_ponto = p.id_ponto", "p.nome_ponto, pp.power, pp.data_hora",
                            "pp.id_ponto = '" + index + "' and data_hora>='" + dateTimeInicial.Value.ToString("s") + "' and data_hora<='" + dateTimeFinal.Value.ToString("s") + "' order by data_hora asc");

                    if (result.Count <= 0)
                    {
                        Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum resultado para o ponto selecionado.",
                            "Aviso: Nenhum resultado encontrado.",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                        Application.UseWaitCursor = false;

                        Application.DoEvents();
                        return;

                    }

                    result2 = result;
                    Invoke((MethodInvoker)(() => FillListView(result)));

                }

                string[] ponto = cliente.Split('-');
                /*Authorship auditoria = new Authorship();
                if (ponto.Count() > 1)
                {
                    auditoria.SetData(Users, "Busca do relatório de nível: " + ponto[1] + " período de: " + ultDateIn.ToString() + " 00:00:00 até " + ultDateFi.ToString() + " 23:59:59", DateTime.Now.ToString("s"));
                }
                else
                {
                    auditoria.SetData(Users, "Busca do relatório de nível: " + ponto[0] + " período de: " + ultDateIn.ToString() + " 00:00:00 até " + ultDateFi.ToString() + " 23:59:59", DateTime.Now.ToString("s"));
                }*/
            }
            catch (Exception)
            {
            }
            Application.UseWaitCursor = false;
            Application.DoEvents();

        }

        private void buttonLimpar_Click_1(object sender, EventArgs e)
        {
            listView.Items.Clear();
            labelListView.Text = "Lista de Resultados Vazia";
        }
  
        private void gerarGrafico_Click(object sender, EventArgs e)
        {
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar gráfico.",
                "Aviso: Lista de Resultados Vazia",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            RelatorioGrafico02 chart = new RelatorioGrafico02(result2);
            chart.relatorioGrafico_Paint();
            chart.ShowDialog();

            //FrmConsumo chart = new FrmConsumo(result2);
            //threadGraficoConsumo = new Thread(() => { chart.gerarGrafico(); });
            //threadGraficoConsumo.Start();
            //chart.ShowDialog();
        }

        private void buttonXls_Click(object sender, EventArgs e)
        {
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar planilha.",
                    "Aviso: Lista de Resultados Vazia",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SpreadsheetCreator2 creator = new SpreadsheetCreator2(listView);
            creator.CreateFile("Descritivo", ultCliente, ultDateIn, ultDateFi);
        }


        private void relatorioNivel_Load(object sender, EventArgs e)
        {
            Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();

        }
        private Thread threadGraficoConsumo;
        private void button1_Click(object sender, EventArgs e)
        {
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar gráfico.",
                "Aviso: Lista de Resultados Vazia",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            FrmConsumo chart = new FrmConsumo(result2);

            threadGraficoConsumo = new Thread(() => { chart.gerarGrafico(); });
            threadGraficoConsumo.Start();
            chart.ShowDialog();
            threadGraficoConsumo.Interrupt();
        }
    }
}
