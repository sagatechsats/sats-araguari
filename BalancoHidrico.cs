﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using DevComponents.DotNetBar;
using System.Runtime.InteropServices;

namespace SATS
{
    public partial class BalancoHidrico : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        DBManager dbManager;
        protected List<string> indexPonto = new List<string>();
        protected string ultCliente;
        protected string ultDateIn;
        protected string ultDateFi;
        protected int ultClienteIndex;
        private int Entrada = 0;
        private int index2 = -1;
        private bool Contador = false;
        bool[] txtb1 = new bool[] { false, false, false, false, false, false, false, false };


        protected void InitDateTimes()//ja que o designer insiste em mudar os valores que eu ponho...
        {
            dateTimeInicial.Value = System.DateTime.Today;
            dateTimeFinal.Value = System.DateTime.Today;
            dateTimeFinal.Value = dateTimeFinal.Value.AddHours(23);
            dateTimeFinal.Value = dateTimeFinal.Value.AddMinutes(59);
            dateTimeFinal.Value = dateTimeFinal.Value.AddSeconds(59);
            dateTimeInicial.MaxDate = System.DateTime.Today;
            dateTimeInicial.Value = System.DateTime.Today;
            dateTimeInicial.Value = System.DateTime.Today;
        }

        protected void StoreSearchData()
        {
            ultCliente = comboBoxCliente.SelectedItem.ToString();
            ultDateIn = dateTimeInicial.Value.ToShortDateString();
            ultDateFi = dateTimeFinal.Value.ToShortDateString();
            ultClienteIndex = comboBoxCliente.SelectedIndex;
        }

        public BalancoHidrico()
        {
            InitializeComponent();
            initilizetextbox();
            InitDateTimes();
            indexPonto.Add("producao");
            indexPonto.Add("processo");
        }

        private void initilizetextbox()
        {
            textBox1.Text = "Consumo Medido Faturado";
            textBox1.ReadOnly = false;
            textBox1.Font = new Font(textBox1.Font.FontFamily, 8);
            textBox1.ForeColor = SystemColors.ActiveBorder;
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            this.textBox1.Enter += new System.EventHandler(this.textBox1_Enter);

            textBox2.Text = "Consumo Estimado Faturado";
            textBox2.ReadOnly = false;
            textBox2.Font = new Font(textBox2.Font.FontFamily, 8);
            textBox2.ForeColor = SystemColors.ActiveBorder;
            this.textBox2.Leave += new System.EventHandler(this.textBox2_Leave);
            this.textBox2.Enter += new System.EventHandler(this.textBox2_Enter);

            textBox3.Text = "Consumo Medido não Faturado";
            textBox3.ReadOnly = false;
            textBox3.Font = new Font(textBox3.Font.FontFamily, 8);
            textBox3.ForeColor = SystemColors.ActiveBorder;
            this.textBox3.Leave += new System.EventHandler(this.textBox3_Leave);
            this.textBox3.Enter += new System.EventHandler(this.textBox3_Enter);

            textBox4.Text = "Consumo Estimado não Faturado";
            textBox4.ReadOnly = false;
            textBox4.Font = new Font(textBox4.Font.FontFamily, 8);
            textBox4.ForeColor = SystemColors.ActiveBorder;
            this.textBox4.Leave += new System.EventHandler(this.textBox4_Leave);
            this.textBox4.Enter += new System.EventHandler(this.textBox4_Enter);

            textBox5.Text = "Consumo não Autorizado";
            textBox5.ReadOnly = false;
            textBox5.Font = new Font(textBox5.Font.FontFamily, 8);
            textBox5.ForeColor = SystemColors.ActiveBorder;
            this.textBox5.Leave += new System.EventHandler(this.textBox5_Leave);
            this.textBox5.Enter += new System.EventHandler(this.textBox5_Enter);

            textBox6.Text = "Erro de Medição";
            textBox6.ReadOnly = false;
            textBox6.Font = new Font(textBox6.Font.FontFamily, 8);
            textBox6.ForeColor = SystemColors.ActiveBorder;
            this.textBox6.Leave += new System.EventHandler(this.textBox6_Leave);
            this.textBox6.Enter += new System.EventHandler(this.textBox6_Enter);

            textBox7.Text = "Vazamento em Reservatórios";
            textBox7.ReadOnly = false;
            textBox7.Font = new Font(textBox7.Font.FontFamily, 8);
            textBox7.ForeColor = SystemColors.ActiveBorder;
            this.textBox7.Leave += new System.EventHandler(this.textBox7_Leave);
            this.textBox7.Enter += new System.EventHandler(this.textBox7_Enter);

            textBox8.Text = "Vazamento em Redes";
            textBox8.ReadOnly = false;
            textBox8.Font = new Font(textBox8.Font.FontFamily, 8);
            textBox8.ForeColor = SystemColors.ActiveBorder;
            this.textBox8.Leave += new System.EventHandler(this.textBox8_Leave);
            this.textBox8.Enter += new System.EventHandler(this.textBox8_Enter);
        }

        private void BalancoHidrico_Load(object sender, EventArgs e)
        {
            Contador = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            Contador = false;
            listView.Items.Clear();

            int index = comboBoxCliente.SelectedIndex;

            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar o tipo do ponto.",
                "Selecione o tipo do ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            initilizetextbox();

            label4.Text = "";
            label6.Text = "";
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
            label10.Text = "";
            label11.Text = "";
            label12.Text = "";

            if (textBox1.Text == "0") txtb1[0] = false;
            if (textBox2.Text == "0") txtb1[1] = false;
            if (textBox3.Text == "0") txtb1[2] = false;
            if (textBox4.Text == "0") txtb1[3] = false;
            if (textBox5.Text == "0") txtb1[4] = false;
            if (textBox6.Text == "0") txtb1[5] = false;
            if (textBox7.Text == "0") txtb1[6] = false;
            if (textBox8.Text == "0") txtb1[7] = false;

            StoreSearchData();
            Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTela));
            novaThread.Start(index);
        }

        private void CarregaTela(object parametro)
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    int index = Convert.ToInt32(parametro);
                    index2 = index;
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    List<string[]> result2 = dbManager.Select("balanco_h", "*", "ponto_tipo='" + indexPonto[index2].ToString() + "' and data_ini='" + dateTimeInicial.Value.ToString("s") + "' and data_fim='" + dateTimeFinal.Value.Date.ToString("s") + "'", "id_balanco", "1");

                    List<string[]> result = dbManager.obterconsumo(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), indexPonto[index].ToString());
                    if (result.Count <= 0)
                    {
                        Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum resultado para o ponto selecionado.",
                            "Aviso: Nenhum resultado encontrado.",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                        Application.UseWaitCursor = false;

                        Application.DoEvents();
                        return;
                    }

                    Invoke((MethodInvoker)(() => FillListView(result)));
                    Invoke((MethodInvoker)(() => GetSubItems()));
                    Invoke((MethodInvoker)(() => FilltextBox(result2)));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
                dbManager.CloseConnection();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void FilltextBox(List<string[]> result)
        {
            try
            {
                textBox1.Text = result[0][3].ToString();
                textBox1.ForeColor = SystemColors.WindowText;
                textBox1.Font = new Font(textBox1.Font.FontFamily, 11);
                textBox2.Text = result[0][4].ToString();
                textBox2.ForeColor = SystemColors.WindowText;
                textBox2.Font = new Font(textBox1.Font.FontFamily, 11);
                textBox3.Text = result[0][5].ToString();
                textBox3.ForeColor = SystemColors.WindowText;
                textBox3.Font = new Font(textBox1.Font.FontFamily, 11);
                textBox4.Text = result[0][6].ToString();
                textBox4.ForeColor = SystemColors.WindowText;
                textBox4.Font = new Font(textBox1.Font.FontFamily, 11);
                textBox5.Text = result[0][7].ToString();
                textBox5.ForeColor = SystemColors.WindowText;
                textBox5.Font = new Font(textBox1.Font.FontFamily, 11);
                textBox6.Text = result[0][8].ToString();
                textBox6.ForeColor = SystemColors.WindowText;
                textBox6.Font = new Font(textBox1.Font.FontFamily, 11);
                textBox7.Text = result[0][9].ToString();
                textBox7.ForeColor = SystemColors.WindowText;
                textBox7.Font = new Font(textBox1.Font.FontFamily, 11);
                textBox8.Text = result[0][10].ToString();
                textBox8.ForeColor = SystemColors.WindowText;
                textBox8.Font = new Font(textBox1.Font.FontFamily, 11);
                txtb1[0] = true;
                txtb1[1] = true;
                txtb1[2] = true;
                txtb1[3] = true;
                txtb1[4] = true;
                txtb1[5] = true;
                txtb1[6] = true;
                txtb1[7] = true;
                Invoke((MethodInvoker)(() => label12.Text = string.Format("{0:n2}", (((Convert.ToDecimal(result[0][10]) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %"));
                Invoke((MethodInvoker)(() => label11.Text = string.Format("{0:n2}", (((Convert.ToDecimal(result[0][9]) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %"));
                Invoke((MethodInvoker)(() => label10.Text = string.Format("{0:n2}", (((Convert.ToDecimal(result[0][8]) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %"));
                Invoke((MethodInvoker)(() => label9.Text = string.Format("{0:n2}", (((Convert.ToDecimal(result[0][7]) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %"));
                Invoke((MethodInvoker)(() => label8.Text = string.Format("{0:n2}", (((Convert.ToDecimal(result[0][6]) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %"));
                Invoke((MethodInvoker)(() => label7.Text = string.Format("{0:n2}", (((Convert.ToDecimal(result[0][5]) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %"));
                Invoke((MethodInvoker)(() => label6.Text = string.Format("{0:n2}", (((Convert.ToDecimal(result[0][4]) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %"));
                Invoke((MethodInvoker)(() => label4.Text = string.Format("{0:n2}", (((Convert.ToDecimal(result[0][3]) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %"));
            }
            catch (Exception)
            {

            }
        }
        protected void FillListView(List<string[]> resultList)
        {
            ListViewItem listViewItem;
            listView.Items.Clear();
            foreach (string[] item in resultList)
            {
                listViewItem = new ListViewItem(item);
                listView.Items.Add(listViewItem);
            }
        }

        private void GetSubItems()
        {
            Entrada = 0;
            int total = 0;
            foreach (ListViewItem item in listView.Items)
            {
                total += Convert.ToInt32(item.SubItems[1].Text);
            }
            Entrada = total;
            textBEntrada.Text = Entrada.ToString();
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "Consumo Medido Faturado")
            {
                textBox1.Font = new Font(textBox1.Font.FontFamily, 8);
                textBox1.Text = "";
                textBox1.ForeColor = SystemColors.WindowText;
                textBox1.ReadOnly = true;
            }
            else textBox1.Font = new Font(textBox1.Font.FontFamily, 11);
            textBox1.Font = new Font(textBox1.Font.FontFamily, 11);
            string value = textBox1.Text;
            if (InputPai.show("Consumo Medido Faturado (m3):", ref value) == DialogResult.OK)
            {
                textBox1.Text = value.ToString();
                label4.Text = string.Format("{0:n2}", (((Convert.ToDecimal(value) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                if (textBox1.Text != "") txtb1[0] = true;
                else textBox1.Text = "0";
            }
            else
                if (textBox1.Text == "")
            {
                // textBox1.ForeColor = SystemColors.GrayText;
                textBox1.Text = "0";
                /// textBox1.Font = new Font(textBox1.Font.FontFamily, 8);
                // textBox1.ReadOnly = false;
            }

        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "Consumo Medido Faturado")
            {
                textBox1.Font = new Font(textBox1.Font.FontFamily, 8);
                textBox1.Text = "";
                textBox1.ForeColor = SystemColors.WindowText;
                textBox1.ReadOnly = true;
            }
            else textBox1.Font = new Font(textBox1.Font.FontFamily, 11);
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "Consumo Medido Faturado")
            {
                textBox1.Font = new Font(textBox1.Font.FontFamily, 8);
                textBox1.Text = "";
                textBox1.ForeColor = SystemColors.WindowText;
                textBox1.ReadOnly = true;
            }
            else textBox1.Font = new Font(textBox1.Font.FontFamily, 11);
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "Consumo Estimado Faturado")
            {
                textBox2.Font = new Font(textBox2.Font.FontFamily, 8);
                textBox2.Text = "";
                textBox2.ForeColor = SystemColors.WindowText;
                textBox2.ReadOnly = true;
            }
            else textBox2.Font = new Font(textBox2.Font.FontFamily, 11);
            textBox2.Font = new Font(textBox2.Font.FontFamily, 11);
            string value = textBox2.Text;
            if (InputPai.show("Consumo Estimado Faturado (m3):", ref value) == DialogResult.OK)
            {
                textBox2.Text = value.ToString();
                label6.Text = string.Format("{0:n2}", (((Convert.ToDecimal(value) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                if (textBox2.Text != "") txtb1[1] = true;
                else textBox2.Text = "0";
            }
            else
                if (textBox2.Text == "")
            {
                // textBox2.ForeColor = SystemColors.GrayText;
                textBox2.Text = "0";
                /// textBox2.Font = new Font(textBox2.Font.FontFamily, 8);
                // textBox2.ReadOnly = false;
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "Consumo Estimado Faturado")
            {
                textBox2.Font = new Font(textBox2.Font.FontFamily, 8);
                textBox2.Text = "";
                textBox2.ForeColor = SystemColors.WindowText;
                textBox2.ReadOnly = true;
            }
            else textBox2.Font = new Font(textBox2.Font.FontFamily, 11);
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "Consumo Estimado Faturado")
            {
                textBox2.Font = new Font(textBox2.Font.FontFamily, 8);
                textBox2.Text = "";
                textBox2.ForeColor = SystemColors.WindowText;
                textBox2.ReadOnly = true;
            }
            else textBox2.Font = new Font(textBox2.Font.FontFamily, 11);
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            if (textBox3.Text == "Consumo Medido não Faturado")
            {
                textBox3.Font = new Font(textBox3.Font.FontFamily, 8);
                textBox3.Text = "";
                textBox3.ForeColor = SystemColors.WindowText;
                textBox3.ReadOnly = true;
            }
            else textBox3.Font = new Font(textBox3.Font.FontFamily, 11);
            textBox3.Font = new Font(textBox3.Font.FontFamily, 11);
            string value = textBox3.Text;
            if (InputPai.show("Consumo Medido não Faturado (m3):", ref value) == DialogResult.OK)
            {
                textBox3.Text = value.ToString();
                label7.Text = string.Format("{0:n2}", (((Convert.ToDecimal(value) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                if (textBox3.Text != "") txtb1[2] = true;
                else textBox3.Text = "0";
            }
            else
                if (textBox3.Text == "")
            {
                // textBox3.ForeColor = SystemColors.GrayText;
                textBox3.Text = "0";
                /// textBox3.Font = new Font(textBox3.Font.FontFamily, 8);
                // textBox3.ReadOnly = false;
            }
        }

        private void textBox3_Leave(object sender, EventArgs e)
        {
            if (textBox3.Text == "Consumo Medido não Faturado")
            {
                textBox3.Font = new Font(textBox3.Font.FontFamily, 8);
                textBox3.Text = "";
                textBox3.ForeColor = SystemColors.WindowText;
                textBox3.ReadOnly = true;
            }
            else textBox3.Font = new Font(textBox3.Font.FontFamily, 11);
        }

        private void textBox3_Enter(object sender, EventArgs e)
        {
            if (textBox3.Text == "Consumo Medido não Faturado")
            {
                textBox3.Font = new Font(textBox3.Font.FontFamily, 8);
                textBox3.Text = "";
                textBox3.ForeColor = SystemColors.WindowText;
                textBox3.ReadOnly = true;
            }
            else textBox3.Font = new Font(textBox3.Font.FontFamily, 11);
        }

        private void textBox4_Click(object sender, EventArgs e)
        {
            if (textBox4.Text == "Consumo Estimado não Faturado")
            {
                textBox4.Font = new Font(textBox4.Font.FontFamily, 8);
                textBox4.Text = "";
                textBox4.ForeColor = SystemColors.WindowText;
                textBox4.ReadOnly = true;
            }
            else textBox4.Font = new Font(textBox4.Font.FontFamily, 11);
            textBox4.Font = new Font(textBox4.Font.FontFamily, 11);
            string value = textBox4.Text;
            if (InputPai.show("Consumo Estimado não Faturado (m3):", ref value) == DialogResult.OK)
            {
                textBox4.Text = value.ToString();
                label8.Text = string.Format("{0:n2}", (((Convert.ToDecimal(value) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                if (textBox4.Text != "") txtb1[3] = true;
                else textBox4.Text = "0";
            }
            else
                if (textBox4.Text == "")
            {
                // textBox4.ForeColor = SystemColors.GrayText;
                textBox4.Text = "0";
                /// textBox4.Font = new Font(textBox4.Font.FontFamily, 8);
                // textBox4.ReadOnly = false;
            }
        }

        private void textBox4_Leave(object sender, EventArgs e)
        {
            if (textBox4.Text == "Consumo Estimado não Faturado")
            {
                textBox4.Font = new Font(textBox4.Font.FontFamily, 8);
                textBox4.Text = "";
                textBox4.ForeColor = SystemColors.WindowText;
                textBox4.ReadOnly = true;
            }
            else textBox4.Font = new Font(textBox4.Font.FontFamily, 11);
        }

        private void textBox4_Enter(object sender, EventArgs e)
        {
            if (textBox4.Text == "Consumo Estimado não Faturado")
            {
                textBox4.Font = new Font(textBox4.Font.FontFamily, 8);
                textBox4.Text = "";
                textBox4.ForeColor = SystemColors.WindowText;
                textBox4.ReadOnly = true;
            }
            else textBox4.Font = new Font(textBox4.Font.FontFamily, 11);
        }

        private void textBox5_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == "Consumo não Autorizado")
            {
                textBox5.Font = new Font(textBox5.Font.FontFamily, 8);
                textBox5.Text = "";
                textBox5.ForeColor = SystemColors.WindowText;
                textBox5.ReadOnly = true;
            }
            else textBox5.Font = new Font(textBox5.Font.FontFamily, 11);
            textBox5.Font = new Font(textBox5.Font.FontFamily, 11);
            string value = textBox5.Text;
            if (InputPai.show("Consumo não Autorizado (m3):", ref value) == DialogResult.OK)
            {
                textBox5.Text = value.ToString();
                label9.Text = string.Format("{0:n2}", (((Convert.ToDecimal(value) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                if (textBox5.Text != "") txtb1[4] = true;
                else textBox5.Text = "0";
            }
            else
                if (textBox5.Text == "")
            {
                // textBox5.ForeColor = SystemColors.GrayText;
                textBox5.Text = "0";
                /// textBox5.Font = new Font(textBox5.Font.FontFamily, 8);
                // textBox5.ReadOnly = false;
            }
        }

        private void textBox5_Leave(object sender, EventArgs e)
        {
            if (textBox5.Text == "Consumo não Autorizado")
            {
                textBox5.Font = new Font(textBox5.Font.FontFamily, 8);
                textBox5.Text = "";
                textBox5.ForeColor = SystemColors.WindowText;
                textBox5.ReadOnly = true;
            }
            else textBox5.Font = new Font(textBox5.Font.FontFamily, 11);
        }

        private void textBox5_Enter(object sender, EventArgs e)
        {
            if (textBox5.Text == "Consumo não Autorizado")
            {
                textBox5.Font = new Font(textBox5.Font.FontFamily, 8);
                textBox5.Text = "";
                textBox5.ForeColor = SystemColors.WindowText;
                textBox5.ReadOnly = true;
            }
            else textBox5.Font = new Font(textBox5.Font.FontFamily, 11);
        }


        private void textBox6_Click(object sender, EventArgs e)
        {
            if (textBox6.Text == "Erro de Medição")
            {
                textBox6.Font = new Font(textBox6.Font.FontFamily, 8);
                textBox6.Text = "";
                textBox6.ForeColor = SystemColors.WindowText;
                textBox6.ReadOnly = true;
            }
            else textBox6.Font = new Font(textBox6.Font.FontFamily, 11);
            textBox6.Font = new Font(textBox6.Font.FontFamily, 11);
            string value = textBox6.Text;
            if (InputPai.show("Erro de Medição (m3):", ref value) == DialogResult.OK)
            {
                textBox6.Text = value.ToString();
                label10.Text = string.Format("{0:n2}", (((Convert.ToDecimal(value) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %";
                if (textBox6.Text != "") txtb1[5] = true;
                else textBox6.Text = "0";
            }
            else
                if (textBox6.Text == "")
            {
                // textBox6.ForeColor = SystemColors.GrayText;
                textBox6.Text = "0";
                /// textBox6.Font = new Font(textBox6.Font.FontFamily, 8);
                // textBox6.ReadOnly = false;
            }
        }

        private void textBox6_Leave(object sender, EventArgs e)
        {
            if (textBox6.Text == "Erro de Medição")
            {
                textBox6.Font = new Font(textBox6.Font.FontFamily, 8);
                textBox6.Text = "";
                textBox6.ForeColor = SystemColors.WindowText;
                textBox6.ReadOnly = true;
            }
            else textBox6.Font = new Font(textBox6.Font.FontFamily, 11);
        }

        private void textBox6_Enter(object sender, EventArgs e)
        {
            if (textBox6.Text == "Erro de Medição")
            {
                textBox6.Font = new Font(textBox6.Font.FontFamily, 8);
                textBox6.Text = "";
                textBox6.ForeColor = SystemColors.WindowText;
                textBox6.ReadOnly = true;
            }
            else textBox6.Font = new Font(textBox6.Font.FontFamily, 11);
        }

        private void textBox7_Click(object sender, EventArgs e)
        {
            if (textBox7.Text == "Vazamento em Reservatórios")
            {
                textBox7.Font = new Font(textBox7.Font.FontFamily, 8);
                textBox7.Text = "";
                textBox7.ForeColor = SystemColors.WindowText;
                textBox7.ReadOnly = true;
            }
            else textBox7.Font = new Font(textBox7.Font.FontFamily, 11);
            textBox7.Font = new Font(textBox7.Font.FontFamily, 11);
            string value = textBox7.Text;
            if (InputPai.show("Vazamento em Reservatórios (m3):", ref value) == DialogResult.OK)
            {
                textBox7.Text = value.ToString();
                label11.Text = string.Format("{0:n2}", (((Convert.ToDecimal(value) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %";
                if (textBox7.Text != "")
                {
                    txtb1[6] = true;
                }
                else textBox7.Text = "0";
            }
            else
                if (textBox7.Text == "")
            {
                // textBox7.ForeColor = SystemColors.GrayText;
                textBox7.Text = "0";
                /// textBox7.Font = new Font(textBox7.Font.FontFamily, 8);
                // textBox7.ReadOnly = false;
            }
        }

        private void textBox7_Leave(object sender, EventArgs e)
        {
            if (textBox7.Text == "Vazamento em Reservatórios")
            {
                textBox7.Font = new Font(textBox7.Font.FontFamily, 8);
                textBox7.Text = "";
                textBox7.ForeColor = SystemColors.WindowText;
                textBox7.ReadOnly = true;
            }
            else textBox7.Font = new Font(textBox7.Font.FontFamily, 11);
        }

        private void textBox7_Enter(object sender, EventArgs e)
        {
            if (textBox7.Text == "Vazamento em Reservatórios")
            {
                textBox7.Font = new Font(textBox7.Font.FontFamily, 8);
                textBox7.Text = "";
                textBox7.ForeColor = SystemColors.WindowText;
                textBox7.ReadOnly = true;
            }
            else textBox7.Font = new Font(textBox7.Font.FontFamily, 11);
        }

        private void textBox8_Click(object sender, EventArgs e)
        {
            if (textBox8.Text == "Vazamento em Redes")
            {
                textBox8.Font = new Font(textBox8.Font.FontFamily, 8);
                textBox8.Text = "";
                textBox8.ForeColor = SystemColors.WindowText;
                textBox8.ReadOnly = true;
            }
            else textBox8.Font = new Font(textBox8.Font.FontFamily, 11);
            textBox8.Font = new Font(textBox8.Font.FontFamily, 11);
            string value = textBox8.Text;
            if (InputPai.show("Vazamento em Redes (m3):", ref value) == DialogResult.OK)
            {
                textBox8.Text = value.ToString();
                label12.Text = string.Format("{0:n2}", (((Convert.ToDecimal(value) * 100) / Convert.ToDecimal(textBEntrada.Text)))) + " %";
                if (textBox8.Text != "")
                {
                    txtb1[7] = true;
                }
                else
                {
                    textBox8.Text = "0";
                }
            }
            else
                if (textBox8.Text == "")
            {
                // textBox8.ForeColor = SystemColors.GrayText;
                textBox8.Text = "0";
                /// textBox8.Font = new Font(textBox8.Font.FontFamily, 8);
                // textBox8.ReadOnly = false;
            }
        }

        private void textBox8_Leave(object sender, EventArgs e)
        {
            if (textBox8.Text == "Vazamento em Redes")
            {
                textBox8.Font = new Font(textBox8.Font.FontFamily, 8);
                textBox8.Text = "";
                textBox8.ForeColor = SystemColors.WindowText;
                textBox8.ReadOnly = true;
            }
            else textBox8.Font = new Font(textBox8.Font.FontFamily, 11);
        }

        private void textBox8_Enter(object sender, EventArgs e)
        {
            if (textBox8.Text == "Vazamento em Redes")
            {
                textBox8.Font = new Font(textBox8.Font.FontFamily, 8);
                textBox8.Text = "";
                textBox8.ForeColor = SystemColors.WindowText;
                textBox8.ReadOnly = true;
            }
            else textBox8.Font = new Font(textBox8.Font.FontFamily, 11);
        }

        private void buttonLimpar_Click(object sender, EventArgs e)
        {
            int contador = 0;
            if (textBox1.Text == "0") txtb1[0] = false;
            if (textBox2.Text == "0") txtb1[1] = false;
            if (textBox3.Text == "0") txtb1[2] = false;
            if (textBox4.Text == "0") txtb1[3] = false;
            if (textBox5.Text == "0") txtb1[4] = false;
            if (textBox6.Text == "0") txtb1[5] = false;
            if (textBox7.Text == "0") txtb1[6] = false;
            if (textBox8.Text == "0") txtb1[7] = false;

            for (int i = 0; i < 8; i++)
            {
                if (txtb1[i] == false) contador++;
            }
            if (textBEntrada.Text.Length == 0)
            {
                MessageBox.Show("Deve ser feita a busca antes de calcular", "Alerta!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (contador > 1)
            {
                MessageBox.Show("Deve ser preenchido mais " + (contador - 1) + " campos para o cálculo", "Alerta!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (contador <= 1)
            {
                int acumulador = 0;

                int ptry = 0;
                if (txtb1[0] == false)
                {
                    textBox1.Text = "0";
                    textBox1.Text = Convert.ToString(Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text)));
                    textBox1.Font = new Font(textBox8.Font.FontFamily, 11);
                    label4.Text = string.Format("{0:n2}", (((Convert.ToDecimal(textBox1.Text) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                    textBox1.ForeColor = SystemColors.WindowText;
                    textBox1.ReadOnly = true;
                    acumulador = Convert.ToInt32(textBox1.Text);
                    ptry = 1;
                }
                else if (txtb1[1] == false)
                {
                    textBox2.Text = "0";
                    textBox2.Text = Convert.ToString(Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text)));
                    textBox2.Font = new Font(textBox8.Font.FontFamily, 11);
                    label6.Text = string.Format("{0:n2}", (((Convert.ToDecimal(textBox2.Text) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                    textBox2.ForeColor = SystemColors.WindowText;
                    textBox2.ReadOnly = true;
                    acumulador = Convert.ToInt32(textBox2.Text);
                    ptry = 2;
                }
                else if (txtb1[2] == false)
                {
                    textBox3.Text = "0";
                    textBox3.Text = Convert.ToString(Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text)));
                    textBox3.Font = new Font(textBox8.Font.FontFamily, 11);
                    label7.Text = string.Format("{0:n2}", (((Convert.ToDecimal(textBox3.Text) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                    textBox3.ForeColor = SystemColors.WindowText;
                    textBox3.ReadOnly = true;
                    acumulador = Convert.ToInt32(textBox3.Text);
                    ptry = 3;
                }
                else if (txtb1[3] == false)
                {
                    textBox4.Text = "0";
                    textBox4.Text = Convert.ToString(Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text)));
                    textBox4.Font = new Font(textBox8.Font.FontFamily, 11);
                    label8.Text = string.Format("{0:n2}", (((Convert.ToDecimal(textBox4.Text) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                    textBox4.ForeColor = SystemColors.WindowText;
                    textBox4.ReadOnly = true;
                    acumulador = Convert.ToInt32(textBox4.Text);
                    ptry = 4;
                }
                else if (txtb1[4] == false)
                {
                    textBox5.Text = "0";
                    textBox5.Text = Convert.ToString(Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text)));
                    textBox5.Font = new Font(textBox8.Font.FontFamily, 11);
                    label9.Text = string.Format("{0:n2}", (((Convert.ToDecimal(textBox5.Text) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                    textBox5.ForeColor = SystemColors.WindowText;
                    textBox5.ReadOnly = true;
                    acumulador = Convert.ToInt32(textBox5.Text);
                    ptry = 5;
                }
                else if (txtb1[5] == false)
                {
                    textBox6.Text = "0";
                    textBox6.Text = Convert.ToString(Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text)));
                    textBox6.Font = new Font(textBox8.Font.FontFamily, 11);
                    label10.Text = string.Format("{0:n2}", (((Convert.ToDecimal(textBox6.Text) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                    textBox6.ForeColor = SystemColors.WindowText;
                    textBox6.ReadOnly = true;
                    acumulador = Convert.ToInt32(textBox6.Text);
                    ptry = 6;
                }
                else if (txtb1[6] == false)
                {
                    textBox7.Text = "0";
                    textBox7.Text = Convert.ToString(Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text)));
                    textBox7.Font = new Font(textBox8.Font.FontFamily, 11);
                    label11.Text = string.Format("{0:n2}", (((Convert.ToDecimal(textBox7.Text) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                    textBox7.ForeColor = SystemColors.WindowText;
                    textBox7.ReadOnly = true;
                    acumulador = Convert.ToInt32(textBox7.Text);
                    ptry = 7;
                }
                else if (txtb1[7] == false)
                {
                    textBox8.Text = "0";
                    textBox8.Text = Convert.ToString(Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text)));
                    textBox8.Font = new Font(textBox8.Font.FontFamily, 11);
                    label12.Text = string.Format("{0:n2}", (((Convert.ToDecimal(textBox8.Text) * 100)) / Convert.ToDecimal(textBEntrada.Text))) + " %";
                    textBox8.ForeColor = SystemColors.WindowText;
                    textBox8.ReadOnly = true;
                    acumulador = Convert.ToInt32(textBox8.Text);
                    ptry = 8;
                }
                else
                {
                    acumulador = Convert.ToInt32(textBEntrada.Text) - (Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox2.Text) + Convert.ToInt32(textBox3.Text) + Convert.ToInt32(textBox4.Text) + Convert.ToInt32(textBox5.Text) + Convert.ToInt32(textBox6.Text) + Convert.ToInt32(textBox7.Text) + Convert.ToInt32(textBox8.Text));
                    ptry = 0;
                }
                txtb1 = new bool[] { true, true, true, true, true, true, true, true, true };
                if (acumulador < 0)
                {
                    initilizetextbox();
                    limpalabel();
                    txtb1 = new bool[] { false, false, false, false, false, false, false, false };
                    MessageBox.Show("O valor Informado é maior que o valor de entrada no sistema.", "Alerta!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                if (acumulador > 0 && ptry == 0)
                {
                    //initilizetextbox();
                    limpalabel();
                    //txtb1 = new bool[] { false, false, false, false, false, false, false, false };
                    MessageBox.Show("O valor Informado é: " + acumulador + " m3 menor que o valor de entrada no sistema.", "Alerta!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }
                Contador = true;
            }
        }

        private void textBox1_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void buttonXls_Click(object sender, EventArgs e)
        {
            if (Contador == false)
            {
                MessageBox.Show("Para gerar a planilha é necessário calcular o balanço hídrico!",
                "Atenção!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            SpreadsheetCreator4 creator = new SpreadsheetCreator4(textBEntrada.Text, textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, textBox6.Text, textBox7.Text, textBox8.Text);
            creator.CreateFile("Balanço Hidrico", ultDateIn, ultDateFi);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Contador == false)
            {
                MessageBox.Show("Para salvar é necessário calcular o balanço hídrico!",
                "Atenção!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Thread novaThread = new Thread(new ParameterizedThreadStart(inputCarregaTela));
            novaThread.Start(index2);
        }
        private void inputCarregaTela(object parametro)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            try
            {
                int index = Convert.ToInt32(parametro);
                dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                dbManager.Insert("balanco_h", "ponto_tipo, volume_total, consumo_mf, consumo_ef, consumo_mnf, consumo_enf, consumo_na, erro_medicao, vazamento_redes, vazmento_reserv, data_ini, data_fim", "'" + indexPonto[index2].ToString() + "', '" + textBEntrada.Text + "', '" + textBox1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + textBox4.Text + "', '" + textBox5.Text + "', '" + textBox6.Text + "', '" + textBox7.Text + "', '" + textBox8.Text + "', '" + dateTimeInicial.Value.ToString("s") + "', '" + dateTimeFinal.Value.ToString("s") + "'");
            }
            catch (Exception)
            {
            }
            Application.UseWaitCursor = false;
            Application.DoEvents();
            dbManager.CloseConnection();
        }

        private void textBEntrada_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<string[]> Lista = new List<string[]>();
            if (SearchBalaco.show(ref Lista) == DialogResult.OK)
            {
                if (Lista.Count() > 0)
                {
                    listView.Items.Clear();
                    textBEntrada.Text = Lista[0][3];
                    dateTimeInicial.Value = Convert.ToDateTime(Lista[0][0]);
                    dateTimeFinal.Value = Convert.ToDateTime(Lista[0][1]);
                    if (Lista[0][2] == "producao")
                    {
                        comboBoxCliente.SelectedIndex = 0;
                        comboBoxCliente.Text = "Pontos de Produção";
                    }
                    else
                    {
                        comboBoxCliente.SelectedIndex = 1;
                        comboBoxCliente.Text = "Pontos de Processo";
                    }
                    Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTela));
                    novaThread.Start(comboBoxCliente.SelectedIndex);
                    List<string[]> ListaB = new List<string[]>();
                    string[] bRetorno = new string[Lista[0].Length - 1];
                    for (int i = 0; i < Lista[0].Length - 1; i++)
                    {
                        bRetorno[i] = Lista[0][1 + i];
                    }
                    ListaB.Add(bRetorno);
                    FilltextBox(ListaB);
                    Contador = true;
                    ultDateIn = Lista[0][0];
                    ultDateFi = Lista[0][1];
                }
            }
        }

        private void limpalabel()
        {
            label4.Text = "";
            label6.Text = "";
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
            label10.Text = "";
            label11.Text = "";
            label12.Text = "";
        }
    }
}
