﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class RelatoriodeGastos : Office2007Form
    {

        protected DBManager dbManager;

        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        public string valorunitario = "";
        private int index = -1;

        public string data = "";
        public RelatoriodeGastos()
        {
            InitializeComponent();
            dateYear.Format = DateTimePickerFormat.Custom;
            dateYear.CustomFormat = "yyyy";
            dateYear.ShowUpDown = true;
            dateYear.Value = DateTime.Today;

            dateMonth.Format = DateTimePickerFormat.Custom;
            dateMonth.CustomFormat = "MMM";
            dateMonth.ShowUpDown = true;
            dateMonth.Value = DateTime.Today;
            dateYear.Value = dateMonth.Value;
            dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
        }

        private void RelatoriodeGastos_Load(object sender, EventArgs e)
        {
            Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTabela));
            novaThread.Start(index);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dateMonth_ValueChanged(object sender, EventArgs e)
        {

        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            listView.Items.Clear();

            int index = comboBoxGasto.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar o tipo de gasto e a data de referência.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }



            //Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTabela));
            //novaThread.Start(index);


            //labelListView.Text = "Resultados para o Ponto:" + ultCliente + " de " + ultDateIn +
            //  " a " + ultDateFi;
        }


        private void CarregaTabela(object parametro)
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    data = dateMonth.Value.Month + "/" + dateYear.Value.Year;
                    index = Convert.ToInt32(parametro);
                    string datareferencia = dateYear.Value.Year + "-" + dateMonth.Value.Month + "-01";
                    List<string[]> result = dbManager.Select("gastos inner join ponto on ponto.id_ponto = gastos.id_ponto", "ponto.nome_ponto, data_inicial, data_final, gasto, valor", "tipo_gasto='" + (index + 1).ToString() + "' and data_referencia='" + datareferencia + "' group by gastos.id_ponto");
                    if (result.Count <= 0)
                    {
                        Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum resultado para o tipo de gasto e a data de referência selecionados.",
                            "Aviso: Nenhum resultado encontrado.",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                        Application.UseWaitCursor = false;
                        Application.DoEvents();
                        return;
                    }
                    List<string[]> result2 = ResultList(result);
                    Invoke((MethodInvoker)(() => FillListView(result2)));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }

            catch
            {
                result = false;
            }
            return result;
        }

        private List<string[]> ResultList(List<string[]> resultList)
        {
            List<string[]> resultList2 = new List<string[]>();
            int i = 0;
            foreach (string[] item in resultList)
            {
                resultList2.Add(new string[4]);
                resultList2[i][0] = item[0];
                resultList2[i][1] = item[1] + " - " + item[2];
                resultList2[i][2] = item[3];
                resultList2[i][3] = (Math.Round(Convert.ToDecimal(item[3].ToString()) * Convert.ToDecimal(item[4].ToString()), 2)).ToString();
                valorunitario = item[4];
                i++;
            }
            return resultList2;
        }

        protected void FillListView(List<string[]> resultList)
        {
            ListViewItem listViewItem;
            listView.Items.Clear();
            foreach (string[] item in resultList)
            {
                listViewItem = new ListViewItem(item);
                listView.Items.Add(listViewItem);
            }
        }

        private void buttonLimpar_Click(object sender, EventArgs e)
        {
            listView.Items.Clear();
        }

        private void buttonXls_Click(object sender, EventArgs e)
        {
            comboBoxGasto.SelectedIndex = index;
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar planilha.",
                    "Aviso: Lista de Resultados Vazia",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            SpreadsheetCreator5 creator = new SpreadsheetCreator5(listView);
            creator.CreateFile("Relatório de gastos", comboBoxGasto.Text, data, valorunitario);
        }

        private void comboBoxGasto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxGasto.SelectedIndex == 0)
            {
                listView.Columns[2].Text = "kWh Consumido";
            }
            else
            {
                listView.Columns[2].Text = "P. químico Consumido (Kg)";
            }
        }
    }
}
