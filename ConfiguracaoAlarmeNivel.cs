﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;
using System.Data;

namespace SATS
{
    //Alarme Nível
    public partial class ConfiguracaoAlarmeNivel : Office2007Form
    {
        protected DBManager dbManager;
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        public List<string> indexClientes { get; set; }
        private Thread novaThread;
        private int opcao = 0;
        List<string[]> res;
        List<string[]> res1;
        List<string> listBomba;
        private DataTable dt = new DataTable();

        public ConfiguracaoAlarmeNivel()
        {
            InitializeComponent();
        }

        private void programaligamento_Load(object sender, EventArgs e)
        {
            novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
                    Invoke((MethodInvoker)(() => PassListToComboBox(listBomba)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(listBomba)));
                }
                catch (Exception)
                {
                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);

            cBPonto.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            cBPonto.Items.Clear();
            foreach (string concat in list)
                cBPonto.Items.Add(concat);
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            listBomba = new List<string>();
            res = dbManager.Select2("ponto", "id_ponto, nome_ponto", "ponto_tipo='0'", "id_ponto");
            res1 = dbManager.Select("ponto p inner join ponto_aciona pa on pa.id_ponto= p.id_ponto order by id_ponto asc", "p.id_ponto, nome_ponto", "");
            foreach (string[] item in res1)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                listConcat.Add(concat);
            }

            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                listBomba.Add(concat);
            }
            return listConcat;
        }

        private void bttnEditar_Click(object sender, EventArgs e)
        {
            int index = cBPonto.SelectedIndex;
            if (cBPonto.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione o Ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            enableComponets(true);
            bttnNovo.Enabled = false;
            bttnEditar.Enabled = false;
            btAcionamento.Enabled = false;
            cBPonto.Enabled = false;
            opcao = 2;
            Thread novaThread = new Thread(new ParameterizedThreadStart(carregar));
            novaThread.Start(index);
        }


        private void enableComponets(bool choice)
        {
            checkBox.Enabled = choice;
            txtNivelMin.Enabled = choice;
            txtNivelMax.Enabled = choice;
            bttnSalvar.Enabled = choice;
            bttnCancelar.Enabled = choice;
        }

        private void carregar(object parametro)
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                int index = Convert.ToInt32(parametro);
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    List<string[]> result = dbManager.Select("alarme_nivel", "*", "id_ponto='" + res[index][0] + "'");
                    if (result.Count == 0)
                    {
                        MessageBox.Show("Dados não cadastrados!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Invoke((MethodInvoker)(() => limpa()));
                        Application.UseWaitCursor = false;
                        Application.DoEvents();
                        return;
                    }

                    Invoke((MethodInvoker)(() => checkBox.Checked = Convert.ToBoolean(result[0][1])));
                    Invoke((MethodInvoker)(() => txtNivelMin.Text = result[0][2].ToString()));
                    Invoke((MethodInvoker)(() => txtNivelMax.Text = result[0][3].ToString()));

                }
              
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void limpa()
        {
            cBPonto.SelectedIndex = -1;
            cBPonto.SelectedIndex = -1;
            cBPonto.Text = "";
            txtNivelMin.Text = "";
            txtNivelMax.Text = "";
            checkBox.Checked = false;
            enableComponets(false);
            bttnNovo.Enabled = true;
            bttnEditar.Enabled = true;
            btAcionamento.Enabled = true;
            cBPonto.Enabled = true;
            opcao = 0;

            dt = new DataTable();
            dt.Columns.Add("horaLiga", typeof(string)); //Hora de Ligar a Bomba
            dt.Columns.Add("horaDesliga", typeof(string)); //Hora de Desligar a Bomba
            dt.Columns.Add("idHora", typeof(int));
        }

        private void bttnCancelar_Click(object sender, EventArgs e)
        {
            limpa();
        }

        private void bttnNovo_Click(object sender, EventArgs e)
        {
            enableComponets(true);
            bttnNovo.Enabled = false;
            bttnEditar.Enabled = false;
            btAcionamento.Enabled = false;
            opcao = 1;
        }

        private void bttnSalvar_Click(object sender, EventArgs e)
        {
            int index = cBPonto.SelectedIndex;
            if (cBPonto.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione o Ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (opcao == 1)
            {
                Thread novaThread = new Thread(new ParameterizedThreadStart(inserir));
                novaThread.Start(index);
            }
            if (opcao == 2)
            {
                Thread novaThread = new Thread(new ParameterizedThreadStart(update));
                novaThread.Start(index);
            }
        }

        private void inserir(object parametro)
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                int indexbomba = 0;
                Invoke((MethodInvoker)(() => indexbomba = cBPonto.SelectedIndex));
                int index = Convert.ToInt32(parametro);

                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    bool resultbol = dbManager.Insert2("alarme_nivel", "id_ponto, habilitado, valor_min, valor_max",
                        "'" + res[index][0].ToString() + "', '" + Convert.ToInt32(checkBox.Checked) + "', '" + txtNivelMin.Text.ToString() + "', '" + txtNivelMax.Text.ToString() + "'");

                    if (resultbol != true)
                    {
                        MessageBox.Show("Dados Não cadastrados, verifique os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        MessageBox.Show("Dados cadastrados com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    Invoke((MethodInvoker)(() => limpa()));
                }
                catch (Exception)
                {
                    MessageBox.Show("Dados já cadastrados no Banco de Dados!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void update(object parametro)
        {
            if (IsConnected() == true)
            {
                int index = Convert.ToInt32(parametro);
                int indexbomba = 0;

                Invoke((MethodInvoker)(() => indexbomba = cBPonto.SelectedIndex));

                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                { 
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    bool resultbol = dbManager.Update("alarme_nivel", "habilitado= '" + Convert.ToInt32(checkBox.Checked) + "', valor_min='" + txtNivelMin.Text.Replace(",", ".") + "', valor_max='" + txtNivelMax.Text.Replace(",", ".") + "'", "id_ponto='" + res[indexbomba][0].ToString() + "'");
                    if (resultbol == false)
                    {
                        MessageBox.Show("Erro ao editar dados!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Invoke((MethodInvoker)(() => limpa()));
                    }
                    else
                    {
                        MessageBox.Show("Dados alterados com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Invoke((MethodInvoker)(() => limpa()));
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show( e.ToString(),"errro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void txtVazao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

        }

        private void txtminFponta_TextChanged(object sender, EventArgs e)
        {

        }

        private void ckTemporizacao_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btAcionamento_Click(object sender, EventArgs e)
        {
            string[] searcharray = new string[10];
            int index = 0;
            FormPesquisa newForm = new FormPesquisa(ref searcharray);
            newForm.ShowDialog();
            try
            {
                List<string[]> result1 = dbManager.Select("ponto", "id_ponto", "ponto_tipo='" + 0 + "' order by id_ponto asc");
                for (int i = 0; i < result1.Count(); i++)
                {
                    if (result1[i][0] == searcharray[0].ToString())
                    {
                        index = i;
                        break;
                    }
                }
                // index = Convert.ToInt32(searcharray[0]);
                Invoke((MethodInvoker)(() => cBPonto.SelectedIndex = index));
                enableComponets(true);
                btAcionamento.Enabled = false;
                bttnNovo.Enabled = false;
                bttnEditar.Enabled = false;
                btAcionamento.Enabled = false;
                cBPonto.Enabled = false;
                opcao = 2;
                Thread novaThread = new Thread(new ParameterizedThreadStart(carregar));
                novaThread.Start(index);
            }
            catch
            {

            }

        }
    }
}