﻿namespace SATS
{
    partial class ImportGastos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportGastos));
            this.cBPonto = new System.Windows.Forms.ComboBox();
            this.cBTipo = new System.Windows.Forms.ComboBox();
            this.dateYear = new System.Windows.Forms.DateTimePicker();
            this.dateMonth = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTInicial = new System.Windows.Forms.DateTimePicker();
            this.dateTFinal = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQuantidade = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bTsave = new System.Windows.Forms.Button();
            this.bTcancel = new System.Windows.Forms.Button();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cBPonto
            // 
            this.cBPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBPonto.FormattingEnabled = true;
            this.cBPonto.Location = new System.Drawing.Point(146, 21);
            this.cBPonto.Name = "cBPonto";
            this.cBPonto.Size = new System.Drawing.Size(264, 24);
            this.cBPonto.TabIndex = 0;
            this.cBPonto.SelectedIndexChanged += new System.EventHandler(this.cBPonto_SelectedIndexChanged);
            // 
            // cBTipo
            // 
            this.cBTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBTipo.FormattingEnabled = true;
            this.cBTipo.Items.AddRange(new object[] {
            "Energia (kWh) nos poços",
            "Produtos Químicos (Kg) nos poços",
            "Produtos Químicos (Kg) na ETA"});
            this.cBTipo.Location = new System.Drawing.Point(146, 58);
            this.cBTipo.Name = "cBTipo";
            this.cBTipo.Size = new System.Drawing.Size(264, 24);
            this.cBTipo.TabIndex = 1;
            this.cBTipo.SelectedIndexChanged += new System.EventHandler(this.cBTipo_SelectedIndexChanged);
            // 
            // dateYear
            // 
            this.dateYear.CustomFormat = "";
            this.dateYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateYear.Location = new System.Drawing.Point(278, 95);
            this.dateYear.Name = "dateYear";
            this.dateYear.Size = new System.Drawing.Size(132, 22);
            this.dateYear.TabIndex = 3;
            this.dateYear.Value = new System.DateTime(2016, 8, 17, 0, 0, 0, 0);
            // 
            // dateMonth
            // 
            this.dateMonth.CustomFormat = "";
            this.dateMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMonth.Location = new System.Drawing.Point(146, 95);
            this.dateMonth.Name = "dateMonth";
            this.dateMonth.Size = new System.Drawing.Size(126, 22);
            this.dateMonth.TabIndex = 2;
            this.dateMonth.Value = new System.DateTime(2016, 8, 17, 15, 51, 57, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(72, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ponto:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Data de Referência:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tipo de Gasto:";
            // 
            // dateTInicial
            // 
            this.dateTInicial.CustomFormat = "";
            this.dateTInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTInicial.Location = new System.Drawing.Point(109, 19);
            this.dateTInicial.Name = "dateTInicial";
            this.dateTInicial.Size = new System.Drawing.Size(108, 22);
            this.dateTInicial.TabIndex = 1;
            this.dateTInicial.ValueChanged += new System.EventHandler(this.dateTInicial_ValueChanged);
            // 
            // dateTFinal
            // 
            this.dateTFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTFinal.Location = new System.Drawing.Point(301, 19);
            this.dateTFinal.Name = "dateTFinal";
            this.dateTFinal.Size = new System.Drawing.Size(106, 22);
            this.dateTFinal.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dateTInicial);
            this.groupBox1.Controls.Add(this.dateTFinal);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 127);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 53);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Período";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(223, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Data Final:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Data Incial:";
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantidade.Location = new System.Drawing.Point(146, 197);
            this.txtQuantidade.MaxLength = 12;
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.Size = new System.Drawing.Size(264, 22);
            this.txtQuantidade.TabIndex = 5;
            this.txtQuantidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantidade_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Quantidade (kWh):";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // bTsave
            // 
            this.bTsave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bTsave.Image = global::SATS.Properties.Resources.Crystal_Clear_action_apply1;
            this.bTsave.Location = new System.Drawing.Point(146, 275);
            this.bTsave.Name = "bTsave";
            this.bTsave.Size = new System.Drawing.Size(96, 38);
            this.bTsave.TabIndex = 7;
            this.bTsave.Text = "Salvar";
            this.bTsave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bTsave.UseVisualStyleBackColor = true;
            this.bTsave.Click += new System.EventHandler(this.bTsave_Click);
            // 
            // bTcancel
            // 
            this.bTcancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bTcancel.Image = global::SATS.Properties.Resources.Crystal_Clear_action_button_cancel;
            this.bTcancel.Location = new System.Drawing.Point(263, 275);
            this.bTcancel.Name = "bTcancel";
            this.bTcancel.Size = new System.Drawing.Size(96, 38);
            this.bTcancel.TabIndex = 8;
            this.bTcancel.Text = "Cancelar";
            this.bTcancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bTcancel.UseVisualStyleBackColor = true;
            this.bTcancel.Click += new System.EventHandler(this.bTcancel_Click);
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerColorTint = System.Drawing.Color.CornflowerBlue;
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 16);
            this.label7.TabIndex = 15;
            this.label7.Text = "Valor unitário do kWh:";
            // 
            // txtValor
            // 
            this.txtValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValor.Location = new System.Drawing.Point(146, 233);
            this.txtValor.MaxLength = 12;
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(264, 22);
            this.txtValor.TabIndex = 6;
            this.txtValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValor_KeyPress);
            // 
            // ImportGastos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(464, 325);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.bTcancel);
            this.Controls.Add(this.bTsave);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtQuantidade);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateMonth);
            this.Controls.Add(this.dateYear);
            this.Controls.Add(this.cBTipo);
            this.Controls.Add(this.cBPonto);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(480, 364);
            this.MinimumSize = new System.Drawing.Size(480, 364);
            this.Name = "ImportGastos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Importar Gastos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ImportGastos_FormClosing);
            this.Load += new System.EventHandler(this.ImportGastos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cBPonto;
        private System.Windows.Forms.ComboBox cBTipo;
        private System.Windows.Forms.DateTimePicker dateYear;
        private System.Windows.Forms.DateTimePicker dateMonth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTInicial;
        private System.Windows.Forms.DateTimePicker dateTFinal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtQuantidade;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bTsave;
        private System.Windows.Forms.Button bTcancel;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtValor;
    }
}