﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class CurvaAbc : Office2007Form
    {
        List<string[]> paraTabela;
        List<string> rotulos;
        List<string> rotulos2;

        public CurvaAbc()
        {
            InitializeComponent();
        }

        public CurvaAbc(List<string[]> lista)
        {

            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();

            this.paraTabela = lista;
        }

        private void curvaAbc_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            int somaT = 0;
            float somaP = 0;
            this.rotulos2 = new List<string>();
            this.rotulos = new List<string>();

            chart1.Series.Clear();
            //chart1.Palette = ChartColorPalette.EarthTones;

            Legend I = new Legend()
            {
                BackColor = Color.Transparent,
                ForeColor = Color.Black,
                Title = "Legenda"
            };

            chart1.Legends.Add(I);

            Series s1 = new Series()
            {
                Name = "s1",
                IsVisibleInLegend = true,
                Color = Color.Transparent,
                ChartType = SeriesChartType.Pie,
                IsValueShownAsLabel = true
            };

            chart1.Series.Add(s1);

            for (int contItens = 0; contItens < paraTabela.Count; contItens++)
            {
                if (!rotulos.Contains(paraTabela[contItens][0]))
                {
                    Console.WriteLine(paraTabela[contItens][0]);
                    // Joga na tabela:
                    somaP = somaVolumePonto(paraTabela[contItens][0]);
                    somaT += Convert.ToInt32(somaP);
                    // Joga no gráfico
                    DataPoint p = new DataPoint(0, somaP);
                    s1.Points.Add(p);
                    p.AxisLabel = paraTabela[contItens][0];
                    p.LegendText = paraTabela[contItens][0];
                    rotulos.Add(paraTabela[contItens][0]);
                }
            }

            for (int contItens = 0; contItens < paraTabela.Count; contItens++)
            {
                if (!rotulos2.Contains(paraTabela[contItens][0]))
                {
                    somaP = somaVolumePonto(paraTabela[contItens][0]);
                    this.rotulos2.Add(paraTabela[contItens][0]);
                    tabelaCurvaAbc.Rows.Add(new object[] { paraTabela[contItens][0], somaP, Convert.ToInt32(((somaP / somaT) * 100)) });
                }
            }
            tabelaCurvaAbc.Sort(tabelaCurvaAbc.Columns[1], ListSortDirection.Descending);
            tabelaCurvaAbc.Rows.Add(new object[] { "Total", somaT, "100 %" });
            this.Controls.Add(chart1);
        }

        private float somaVolumePonto(String rotulo)
        {
            float soma = 0;
            int k = 0;
            int k2 = 0;
            for (int contItens = 1; contItens < paraTabela.Count; contItens++)
            {
                if (paraTabela[contItens][0] == rotulo)
                {
                    k2 = contItens;
                    k++;
                }
            }
            soma = Convert.ToInt32(paraTabela[k2][1]) - Convert.ToInt32(paraTabela[k2 + 1 - k][1]);
            return soma;
        }

        private int somaVolumes()
        {
            int soma = 0;
            for (int contItens = 1; contItens < paraTabela.Count; contItens++)
            {
                soma += Convert.ToInt32(paraTabela[contItens][1]);
            }
            return soma;
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void tabelaCurvaAbc_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
