﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using DevComponents.DotNetBar;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;

namespace SATS
{
    public partial class ConsultarPressaoFrequencia : Office2007Form
    {
        protected DBManager dbManager;
        protected List<string> listaClientes;
        protected List<string> indexClientes;
        List<string[]> consultalistPressao;
        List<string[]> result2 = new List<string[]>();
        List<string[]> consultalistFrequencia;
        private int grafico = 1;

        public ConsultarPressaoFrequencia()
        {
            
            InitializeComponent();
        }

        private void ConsultarPressaoFrequencia_Load(object sender, EventArgs e)
        {
            cartesianChart1.Visible = false;
            Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();

        }
        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select2("ponto", "id_ponto, nome_ponto", "ponto_tipo='7'", "id_ponto");

            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];

                listConcat.Add(concat);
            }
            return listConcat;
        }
        public void CarregaTela()
        {
            if (comboBoxPonto.Items.Count <= 1)
            {
                
                    Application.UseWaitCursor = true;
                    Application.DoEvents();
                    try
                    {
                        dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                        List<string> listaClientes = new List<string>();
                        listaClientes = GetClientes();
                        Invoke((MethodInvoker)(() => PassListToComboBox(listaClientes)));
                        Invoke((MethodInvoker)(() => PassListToAutoComplete(listaClientes)));
                    }
                    catch (Exception)
                    {

                    }
                    Application.UseWaitCursor = false;
                    Application.DoEvents();
                }
                
            
        }
        protected void PassListToComboBox(List<string> list)
        {
            foreach (string concat in list)
                comboBoxPonto.Items.Add(concat);
        }
        protected void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
                comboBoxPonto.AutoCompleteCustomSource = stringCollection;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            cartesianChart1.Visible = true;

            int index = comboBoxPonto.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar-se um ponto.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            String cliente = comboBoxPonto.Text;
            Thread novaThread = new Thread(new ParameterizedThreadStart(PesquisaDados));
            novaThread.Start(cliente);
          
        }

        private void PesquisaDados (object parametro)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            try
            {
                string cliente = parametro.ToString();
                //Pega o valor selecionado no combobox do ponto       
                string[] words = cliente.Split('-'); //Separa o id do nome do ponto        
                DateTime result = dateTimePicker1.Value;
                string data = result.ToString("yyyy-MM-dd "); //pega a data escolhida pelo usuário

                dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                ArrayList arr = new ArrayList();
                consultalistPressao = dbManager.Select("ponto_pressao", "pressao, data_hora", "id_ponto=" + words[0] + " and data_hora between '" + data + " 00:00:00' and '" + data + " 23:59:59' order by data_hora");
                consultalistFrequencia = dbManager.Select("ponto_inversor_dados", "frequency, data_hora", "id_ponto=" + words[0] + " and data_hora between '" + data + " 00:00:00' and '" + data + " 23:59:59' order by data_hora");
                Invoke((MethodInvoker)(() => gerarGrafico()));
                //Thread t = new Thread(() => gerarGrafico());
                //t.Start();

            }
            catch (Exception)
            {

            }
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        //private Thread t;
        public void gerarGrafico()
        {
            try
            {
                 cartesianChart1.Series.Clear();
                 cartesianChart1.AxisX.Clear();
                 cartesianChart1.AxisY.Clear();

                var datas = new List<string>();

                var chart = new ChartValues<double>();
                var chart1 = new ChartValues<double>();

                foreach (var l in consultalistPressao)
                {

                    chart.Add(Convert.ToDouble(l[0]));
                    datas.Add(l[1]);

                }
                foreach (var j in consultalistFrequencia)
                    {
                        chart1.Add(Convert.ToDouble(j[0]));
                        datas.Add(j[1]);
                    }

                if (grafico == 1)
                {
                    cartesianChart1.Series = new LiveCharts.SeriesCollection
                                {
                                    new LineSeries
                                    {
                                        Title = "Leitura de pressão",
                                        Values = chart,
                                        PointGeometry = DefaultGeometries.Circle,
                                        PointGeometrySize = 10,
                                        LineSmoothness = 0.2
                                    },
                                    new LineSeries
                                    {
                                        Title = "Leitura de frequência",
                                        Values = chart1,
                                        PointGeometry = DefaultGeometries.Circle,
                                        PointGeometrySize = 10,
                                        LineSmoothness = 0.2
                                    }
                                };
                }
                else if (grafico == 2)
                {
                    cartesianChart1.Series = new LiveCharts.SeriesCollection
                                {
                                    new ColumnSeries
                                    {
                                        Title = "Leitura de pressão",
                                        Values = chart,
                                    },
                                    new ColumnSeries
                                    {
                                        Title = "Leitura de frequência",
                                        Values = chart1,
                                    }
                                };
                }

                 cartesianChart1.AxisX.Add(new LiveCharts.Wpf.Axis
                {
                    Title = "Data e Hora",
                    Labels = datas
                });

                cartesianChart1.AxisY.Clear();
                cartesianChart1.AxisY.Add(new LiveCharts.Wpf.Axis
                {
                    Title = "Hz / m.c.a ",
                    FlowDirection = System.Windows.FlowDirection.LeftToRight,
                    Position = AxisPosition.LeftBottom
            
                });

                 cartesianChart1.LegendLocation = LegendLocation.Right;
                 this.Cursor = Cursors.Default;
                 cartesianChart1.Visible = true;
            }
            catch
            {

            }
        }
    }
}
