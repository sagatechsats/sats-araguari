﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class CadastroHorarioSetPoint : Office2007Form
    {
        protected DBManager dbManager;

        public string idBomba { get; private set; }

        public CadastroHorarioSetPoint(String idBomba)
        {
            InitializeComponent();
            this.idBomba = idBomba;
        }

        private void CadastroHorarioSetPoint_Load(object sender, EventArgs e)
        {
            bttnSalvar.Enabled = true;
        }

        private void bttnSalvar_Click(object sender, EventArgs e)
        {
            bttnSalvar.Enabled = false;
            Salvar();
        }
        private void Salvar()
        {

            dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            bool resultbol = dbManager.Insert("setpoint_horario", "id_bomba, set_point, hora_inicio, hora_fim",
                        "'" + idBomba.ToString() + "', '" + txtSetpoint.Text.ToString() + "','" + dateTimeInicial.Text.ToString() + "' ,'" + dateTimeFinal.Text.ToString() + "'");


            bttnSalvar.Enabled = true;
            this.Close();
        }
    }
}
