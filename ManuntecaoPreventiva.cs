﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class manunteção : Office2007Form
    {
        protected DBManager dbManager;
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        string selecteitem = "";
        string selectponto = "";

        public manunteção()
        {
            InitializeComponent();
        }

        private void manunteção_Load(object sender, EventArgs e)
        {
            Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();
        }

        private void CarregaTela()
        {

            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();

                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
                    Invoke((MethodInvoker)(() => PassListToComboBox(consulta)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(consulta)));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
            comboPonto.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            comboPonto.Items.Clear();
            foreach (string concat in list)
                comboPonto.Items.Add(concat);
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select("ponto order by id_ponto asc", "id_ponto, nome_ponto", "");
            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];

                listConcat.Add(concat);
            }
            return listConcat;
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public List<string> indexClientes { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = comboPonto.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a ação é necessário selecionar um ponto.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int indextipo = comboTipo.SelectedIndex;
            if (indextipo == -1)
            {
                MessageBox.Show("Para realizar a ação é necessário selecionar o tipo.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Thread insere = new Thread(new ThreadStart(this.Inserir));
            selecteitem = comboTipo.SelectedItem.ToString();
            selectponto = (Convert.ToInt32(comboPonto.SelectedIndex) + 1).ToString();
            //dbManager = new DBManager(System.IO.File.ReadAllText(@"C:\Supervisorio\stconnector"));
            // String query = 1+", '" +dateTimeDate.Value.ToString("s")+"', '"+comboTipo.SelectedItem+"', '"+txtDescri.Text+"'";
            //dbManager.Insert("spr_manutencao", "id_spr, data_hora, tipo_manutencao, descricao", query);
            insere.Start();
        }

        private void Inserir()
        {
            dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            String query = selectponto + ", '" + dateTimeDate.Value.ToString("s") + "', '" + selecteitem + "', '" + txtDescri.Text + "'";
            dbManager.Insert("ponto_manutencao", "id_ponto, data_hora, tipo_manutencao, descricao", query);
            Invoke((MethodInvoker)(() => this.Close()));
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
