﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class CadastroDados : Office2007Form
    {

        protected DBManager dbManager;

        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        public List<string> indexClientes { get; set; }

        Thread novaThread;
        private int opcao = 0;

        public CadastroDados()
        {
            InitializeComponent();
        }

        private void cadastroDados_Load(object sender, EventArgs e)
        {
            txtBitola.Enabled = false;
            txtMbomba.Enabled = false;
            txtNd.Enabled = false;
            txtNe.Enabled = false;
            txtPbomba.Enabled = false;
            txtPpoco.Enabled = false;
            cBtipoBomba.Enabled = false;
            cBtsBomba.Enabled = false;

            bttnSalvar.Enabled = false;
            cBPonto.Enabled = true;
            bttnEditar.Enabled = true;
            bttnNovo.Enabled = true;

            novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPpoco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
      (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtNe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
      (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtNd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
      (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtBitola_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
      (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void bttnNovo_Click(object sender, EventArgs e)
        {
            opcao = 1;
            txtBitola.Text = "";
            txtMbomba.Text = "";
            txtNd.Text = "";
            txtNe.Text = "";
            txtPbomba.Text = "";
            txtPpoco.Text = "";
            cBtipoBomba.Text = "";
            cBtsBomba.Text = "";
            dateTimePicker.Value = DateTime.Today;
            txtBiltolaTubo.Text = "";


            txtBitola.Enabled = true;
            txtMbomba.Enabled = true;
            txtNd.Enabled = true;
            txtNe.Enabled = true;
            txtPbomba.Enabled = true;
            txtPpoco.Enabled = true;
            dateTimePicker.Enabled = true;
            txtBiltolaTubo.Enabled = true;


            cBtipoBomba.Enabled = true;
            cBtsBomba.Enabled = true;
            //bttnDeletar.Enabled = true;
            bttnSalvar.Enabled = true;

            bttnEditar.Enabled = false;
            bttnNovo.Enabled = false;
        }

        private void CarregaTela()
        {

            if (IsConnected() == true)
            {

                Application.UseWaitCursor = true;
                Application.DoEvents();

                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
                    Invoke((MethodInvoker)(() => PassListToComboBox(consulta)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(consulta)));
                }
                catch (Exception)
                {
                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }



        }

        private void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);

            cBPonto.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            cBPonto.Items.Clear();
            foreach (string concat in list)
                cBPonto.Items.Add(concat);
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select("ponto order by id_ponto asc", "id_ponto, nome_ponto", "");
            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];

                listConcat.Add(concat);
            }
            return listConcat;
        }

        public static Boolean IsConnected()
        {

            bool result;

            try
            {

                int Description;

                result = InternetGetConnectedState(out Description, 0);

            }

            catch
            {


                result = false;

            }

            return result;

        }

        private void bttnEditar_Click(object sender, EventArgs e)
        {
            int index = cBPonto.SelectedIndex;
            if (cBPonto.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione o Ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            opcao = 2;
            txtBitola.Enabled = true;
            txtMbomba.Enabled = true;
            txtNd.Enabled = true;
            txtNe.Enabled = true;
            txtPbomba.Enabled = true;
            txtPpoco.Enabled = true;
            cBtipoBomba.Enabled = true;
            cBtsBomba.Enabled = true;
            dateTimePicker.Enabled = true;
            txtBiltolaTubo.Enabled = true;

            bttnSalvar.Enabled = true;
            cBPonto.Enabled = false;
            bttnEditar.Enabled = false;
            bttnNovo.Enabled = false;
            Thread novaThread = new Thread(new ParameterizedThreadStart(carregar));
            novaThread.Start(index);

        }

        private void bttnCancelar_Click(object sender, EventArgs e)
        {
            limpa();
        }

        private void bttnSalvar_Click(object sender, EventArgs e)
        {
            int index = cBPonto.SelectedIndex;
            if (cBPonto.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione o Ponto!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (txtMbomba.Text == "")
            {
                MessageBox.Show("Informe o modelo da bomba!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (cBtipoBomba.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione o tipo da bomba!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (cBtsBomba.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione a tensão da bomba!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (txtPpoco.Text == "")
            {
                MessageBox.Show("Informe a profundidade do poço!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (opcao == 1)
            {
                Thread novaThread = new Thread(new ParameterizedThreadStart(inserir));
                novaThread.Start(index);
            }
            if (opcao == 2)
            {
                Thread novaThread = new Thread(new ParameterizedThreadStart(update));
                novaThread.Start(index);
            }
        }


        private void inserir(object parametro)
        {
            if (IsConnected() == true)
            {

                Application.UseWaitCursor = true;
                Application.DoEvents();

                int index = Convert.ToInt32(parametro) + 1;
                int indexTBomba = -1;
                Invoke((MethodInvoker)(() => indexTBomba = cBtipoBomba.SelectedIndex + 1));
                int indextsBomba = -1;
                Invoke((MethodInvoker)(() => indextsBomba = cBtsBomba.SelectedIndex));
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    bool resultbol = dbManager.Insert("ponto_dados", "id_ponto, modelo_bomba, potencia_bomba, tipo_bomba, tensao, profundidade_poco, nivel_estatico, nivel_dinamico, bitola_hidrometro, bitola_tubo, data_instalacao ", "'" + (index).ToString() + "', '" + txtMbomba.Text.ToString() + "', '" + txtPbomba.Text.ToString() + "', '" + (indexTBomba).ToString() + "', '" + tensao(indextsBomba).ToString() + "', '" + txtPpoco.Text.ToString() + "', '" + txtNe.Text.ToString() + "', '" + txtNd.Text.ToString() + "', '" + txtBitola.Text.ToString() + "', '" + txtBiltolaTubo.Text.ToString() + "', '" + dateTimePicker.Value.ToString("s") + "'");
                    if (resultbol == false) Invoke((MethodInvoker)(() => limpa()));
                }
                catch (Exception)
                {

                }
                Invoke((MethodInvoker)(() => limpa()));
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void update(object parametro)
        {
            if (IsConnected() == true)
            {

                Application.UseWaitCursor = true;
                Application.DoEvents();

                int index = Convert.ToInt32(parametro) + 1;
                int indexTBomba = -1;
                Invoke((MethodInvoker)(() => indexTBomba = cBtipoBomba.SelectedIndex + 1));
                int indextsBomba = -1;
                Invoke((MethodInvoker)(() => indextsBomba = cBtsBomba.SelectedIndex));
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    bool resultbol = dbManager.Update("ponto_dados", "modelo_bomba= '" + txtMbomba.Text.ToString() + "', potencia_bomba='" + txtPbomba.Text.ToString() +
                        "', tipo_bomba='" + (indexTBomba).ToString() + "', tensao='" + tensao(indextsBomba).ToString() + "', profundidade_poco='" + txtPpoco.Text.ToString() +
                        "', nivel_estatico='" + txtNe.Text.ToString() + "', nivel_dinamico='" + txtNd.Text.ToString() + "', bitola_hidrometro='" + txtBitola.Text.ToString() +
                        "', bitola_tubo='" + txtBiltolaTubo.Text.ToString() + "', data_instalacao='" + dateTimePicker.Value.ToString("s") + "'", "id_ponto='" + index.ToString() + "'");
                    if (resultbol == false)
                    {
                        MessageBox.Show("Erro ao editar dados!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Invoke((MethodInvoker)(() => limpa()));
                    }
                    else
                    {
                        MessageBox.Show("Dados alterados com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Invoke((MethodInvoker)(() => limpa()));
                    }
                }
                catch (Exception)
                {

                }

                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void carregar(object parametro)
        {

            if (IsConnected() == true)
            {

                Application.UseWaitCursor = true;
                Application.DoEvents();
                int index = Convert.ToInt32(parametro) + 1;

                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    List<string[]> result = dbManager.Select("ponto_dados", "*", "id_ponto='" + index + "'");
                    if (result.Count == 0)
                    {
                        MessageBox.Show("Dados não cadastrados!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Invoke((MethodInvoker)(() => limpa()));
                        Application.UseWaitCursor = false;
                        Application.DoEvents();
                        return;
                    }

                    Invoke((MethodInvoker)(() => txtMbomba.Text = result[0][1].ToString()));
                    Invoke((MethodInvoker)(() => txtPbomba.Text = result[0][2].ToString()));
                    Invoke((MethodInvoker)(() => cBtipoBomba.SelectedIndex = Convert.ToInt32(result[0][3]) - 1));
                    if (result[0][4] == "110") Invoke((MethodInvoker)(() => cBtsBomba.SelectedIndex = 0));
                    else if (result[0][4] == "220") Invoke((MethodInvoker)(() => cBtsBomba.SelectedIndex = 1));
                    else if (result[0][4] == "380") Invoke((MethodInvoker)(() => cBtsBomba.SelectedIndex = 2));
                    Invoke((MethodInvoker)(() => txtPpoco.Text = result[0][5].ToString().Replace(",", ".")));
                    Invoke((MethodInvoker)(() => txtNe.Text = result[0][6].ToString().Replace(",", ".")));
                    Invoke((MethodInvoker)(() => txtNd.Text = result[0][7].ToString().Replace(",", ".")));
                    Invoke((MethodInvoker)(() => txtBitola.Text = result[0][8].ToString().Replace(",", ".")));
                    Invoke((MethodInvoker)(() => txtBiltolaTubo.Text = result[0][9].ToString().Replace(",", ".")));
                    Invoke((MethodInvoker)(() => dateTimePicker.Value = Convert.ToDateTime(result[0][10])));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        private int tensao(int index)
        {
            if (index == 0) return 110;
            else if (index == 1) return 220;
            else if (index == 2) return 380;
            else return 0;
        }

        private void limpa()
        {
            opcao = 0;
            txtBitola.Enabled = false;
            txtMbomba.Enabled = false;
            txtNd.Enabled = false;
            txtNe.Enabled = false;
            txtPbomba.Enabled = false;
            txtPpoco.Enabled = false;
            cBtipoBomba.Enabled = false;
            cBtsBomba.Enabled = false;
            dateTimePicker.Enabled = false;
            txtBiltolaTubo.Enabled = false;


            bttnSalvar.Enabled = false;
            cBPonto.Enabled = true;
            bttnEditar.Enabled = true;
            bttnNovo.Enabled = true;

            dateTimePicker.Value = DateTime.Today;
            txtBiltolaTubo.Text = "";
            txtBitola.Text = "";
            txtMbomba.Text = "";
            txtNd.Text = "";
            txtNe.Text = "";
            txtPbomba.Text = "";
            txtPpoco.Text = "";
            cBtipoBomba.Text = "";
            cBtsBomba.Text = "";
            cBPonto.Text = "";
            cBPonto.SelectedIndex = -1;
            cBtipoBomba.SelectedIndex = -1;
            cBtsBomba.SelectedIndex = -1;
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }
    }
}
