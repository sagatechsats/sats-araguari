﻿namespace SATS
{
    partial class RelatorioDescritivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RelatorioDescritivo));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gerarGrafico = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.geraCurvaAbc = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Size = new System.Drawing.Size(46, 16);
            // 
            // comboBoxCliente
            // 
            this.comboBoxCliente.AutoCompleteCustomSource.AddRange(new string[] {
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTAS CACHOEIRA",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. BEIJA FLOR",
            "000018 - P. LUIZINHO",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA"});
            this.comboBoxCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCliente.Items.AddRange(new object[] {
            "",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTA CACHOEIRAS",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. ADUTORA ONCA",
            "000018 - P. 01 BR-135",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTAS CACHOEIRA",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. BEIJA FLOR",
            "000018 - P. LUIZINHO",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA",
            "000001 - P. NOVO HORIZONTE",
            "000002 - P. SAO GERALDO",
            "000003 - P. ANTONIO TIBURCIO",
            "000004 - P. CARIUNA",
            "000005 - P. ETA ONCA",
            "000006 - P. ETA 1",
            "000007 - P. ETA 2",
            "000008 - P. ETA 3",
            "000009 - P. ETA 4",
            "000010 - P. QUINTAS CACHOEIRA",
            "000011 - P. MANGA DO NECO",
            "000012 - P. BONFIM 1",
            "000013 - P. BONFIM 2",
            "000014 - P. MERCADO OFICINA",
            "000015 - P. MERCADO BORRACHARIA",
            "000016 - P. DERIVA GUARACIAMA",
            "000017 - P. BEIJA FLOR",
            "000018 - P. LUIZINHO",
            "000019 - P. 02 BR-135",
            "000020 - P. 03 BR-135",
            "000021 - P. AZULAO",
            "000022 - P. MATADOURO 1",
            "000023 - P. MATADOURO 2",
            "000024 - P. PARQUE MUNICIPAL",
            "000025 - P. MATADOURO 3",
            "000026 - P. L. N. S. APARECIDA",
            "000027 - P. J. N. S. APARECIDA"});
            this.comboBoxCliente.Size = new System.Drawing.Size(271, 24);
            // 
            // dateTimeInicial
            // 
            this.dateTimeInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeInicial.Location = new System.Drawing.Point(386, 12);
            this.dateTimeInicial.Size = new System.Drawing.Size(123, 22);
            // 
            // dateTimeFinal
            // 
            this.dateTimeFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeFinal.Location = new System.Drawing.Point(562, 12);
            this.dateTimeFinal.Size = new System.Drawing.Size(114, 22);
            // 
            // buttonBuscar
            // 
            this.buttonBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBuscar.Image = global::SATS.Properties.Resources.Crystal_Clear_app_xmag__1_;
            this.buttonBuscar.Location = new System.Drawing.Point(682, 12);
            this.buttonBuscar.Size = new System.Drawing.Size(153, 32);
            this.buttonBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonBuscar.Click += new System.EventHandler(this.buttonBuscar_Click);
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4});
            this.listView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView.Location = new System.Drawing.Point(15, 95);
            this.listView.Size = new System.Drawing.Size(661, 300);
            // 
            // buttonLimpar
            // 
            this.buttonLimpar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLimpar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_editpaste;
            this.buttonLimpar.Location = new System.Drawing.Point(682, 247);
            this.buttonLimpar.Size = new System.Drawing.Size(153, 28);
            this.buttonLimpar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonLimpar.Click += new System.EventHandler(this.buttonLimpar_Click_1);
            // 
            // buttonXls
            // 
            this.buttonXls.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonXls.Image = global::SATS.Properties.Resources._1387910006_Arzo_Icons_Icon_96_2;
            this.buttonXls.Location = new System.Drawing.Point(682, 349);
            this.buttonXls.Size = new System.Drawing.Size(153, 46);
            this.buttonXls.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonXls.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonXls.Click += new System.EventHandler(this.buttonXls_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(335, 11);
            this.label2.Size = new System.Drawing.Size(45, 32);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(515, 11);
            this.label3.Size = new System.Drawing.Size(40, 32);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Ponto";
            this.columnHeader1.Width = 202;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Volume (m3)";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Vazão (m³/H)";
            // 
            // labelListView
            // 
            this.labelListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelListView.Size = new System.Drawing.Size(164, 16);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Data e Hora";
            this.columnHeader4.Width = 170;
            // 
            // gerarGrafico
            // 
            this.gerarGrafico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gerarGrafico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gerarGrafico.Image = global::SATS.Properties.Resources.Crystal_Clear_app_no3D__1_;
            this.gerarGrafico.Location = new System.Drawing.Point(682, 315);
            this.gerarGrafico.Name = "gerarGrafico";
            this.gerarGrafico.Size = new System.Drawing.Size(153, 28);
            this.gerarGrafico.TabIndex = 13;
            this.gerarGrafico.Text = "Gerar Gráfico";
            this.gerarGrafico.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.gerarGrafico.UseVisualStyleBackColor = true;
            this.gerarGrafico.Click += new System.EventHandler(this.gerarGrafico_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(60, 42);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(163, 20);
            this.checkBox1.TabIndex = 14;
            this.checkBox1.Text = "Vazão Média por Hora";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // geraCurvaAbc
            // 
            this.geraCurvaAbc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.geraCurvaAbc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.geraCurvaAbc.Image = global::SATS.Properties.Resources.Crystal_Clear_app_Volume_Manager__1_;
            this.geraCurvaAbc.Location = new System.Drawing.Point(682, 281);
            this.geraCurvaAbc.Name = "geraCurvaAbc";
            this.geraCurvaAbc.Size = new System.Drawing.Size(153, 28);
            this.geraCurvaAbc.TabIndex = 15;
            this.geraCurvaAbc.Text = "Gerar curva ABC";
            this.geraCurvaAbc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.geraCurvaAbc.UseVisualStyleBackColor = true;
            this.geraCurvaAbc.Click += new System.EventHandler(this.geraCurvaAbc_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::SATS.Properties.Resources.rsz_crystal_clear_app_error;
            this.button2.Location = new System.Drawing.Point(682, 158);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(153, 33);
            this.button2.TabIndex = 17;
            this.button2.Text = "Relátorio de Perda";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::SATS.Properties.Resources.rsz_crystal_clear_app_kodo;
            this.button1.Location = new System.Drawing.Point(682, 197);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(153, 44);
            this.button1.TabIndex = 16;
            this.button1.Text = "Relátorio de Gastos";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Relatorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 407);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.geraCurvaAbc);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.gerarGrafico);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(863, 446);
            this.Name = "Relatorio";
            this.Text = "Relatório Descritivo";
            this.Load += new System.EventHandler(this.Relatorio_Load);
            this.Controls.SetChildIndex(this.gerarGrafico, 0);
            this.Controls.SetChildIndex(this.checkBox1, 0);
            this.Controls.SetChildIndex(this.geraCurvaAbc, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.comboBoxCliente, 0);
            this.Controls.SetChildIndex(this.dateTimeInicial, 0);
            this.Controls.SetChildIndex(this.dateTimeFinal, 0);
            this.Controls.SetChildIndex(this.buttonBuscar, 0);
            this.Controls.SetChildIndex(this.listView, 0);
            this.Controls.SetChildIndex(this.buttonLimpar, 0);
            this.Controls.SetChildIndex(this.buttonXls, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.labelListView, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button gerarGrafico;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button geraCurvaAbc;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}