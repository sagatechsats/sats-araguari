﻿using ExcelLibrary.SpreadSheet;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Para XLSX utilizamos a biblioteca EPPlus(using OfficeOpenXml). Para XLS, 
// Excel Library.(Lembrar de adicionar refencias)

namespace SATS
{
    class SpreadsheetCreator3
    {
        private List<string[]> listView;
        public SpreadsheetCreator3(List<string[]> resultSetTotal)
        {
            listView = resultSetTotal;
        }

        // Estou retornando o tipo SaveFileDialog porque eu preciso do path e do
        // filterIndex escolhido.
        private SaveFileDialog SaveDialog()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Excel 2007/2010/2013 (*.xlsx)|*.xlsx";
                                     //"Todos os arquivos (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                return saveFileDialog1;
            else
                return null;
        }

        public void CreateFile(string nameSpread, string cliente, string dataInicial,
            string dataFinal, List<string[]> resultSet)
        {
            SaveFileDialog saveFileStruct = SaveDialog();
            if (saveFileStruct == null)
                return;


            CreateXLSFile(nameSpread, cliente, dataInicial, dataFinal, saveFileStruct.FileName, resultSet);
            
        }
     
        
            private void CreateXLSFile(string nameSpread, string cliente, string dataInicial,
            string dataFinal, string path, List<string[]> resultSet)
        {
            FileInfo file = new FileInfo(path);
            //FileInfo file = new FileInfo(@"C:\Users\amador\Desktop\test.xlsx");
            using (ExcelPackage pck = new ExcelPackage())
            {
                var ws = pck.Workbook.Worksheets.Add(nameSpread);
                ws.Name = nameSpread;
                ws.Cells.Style.Font.Size = 11;

                
                ws.Cells[1, 1].Value = "Início: " + dataInicial;
                ws.Cells[1, 2].Value = "Término: " + dataFinal;

                

                ws.Cells["A1:B1"].Style.Font.Italic = true;
                ws.Cells["A1:B1"].Style.Font.Bold = true;
                
                ws.Column(1).Width = 50;
                ws.Column(2).Width = 30;
                ws.Column(3).Width = 30;

                ws.Cells[2, 1].Value = "Valor da Micromedição:";
                ws.Cells[2, 2].Value = Convert.ToInt32(resultSet[0][3]);

                ws.Cells["A2:A2"].Style.Font.Bold = true;
                

                int sumVol = 0;

                for (int i = 0; i < listView.Count; i++)
                {
                    sumVol += Convert.ToInt32(listView[i][1]);
                }

                ws.Cells[3, 1].Value = "Valor da Macromedição:";

                ws.Cells[3, 2].Value = sumVol;
                ws.Cells["A3:A3"].Style.Font.Bold = true;

                ws.Cells[4, 1].Value = "Diferença (Macromedição - Micromedição):";

               ws.Cells[4, 2].Value = sumVol - Convert.ToInt32(resultSet[0][3]);

               ws.Cells["A4:B4"].Style.Font.Italic = true;
               ws.Cells["A4:B4"].Style.Font.Bold = true;
                // Salva o arquivo.
                Byte[] bin = pck.GetAsByteArray();
                File.WriteAllBytes(path, bin);
                try
                {
                    File.WriteAllBytes(path, bin);
                    MessageBox.Show("Arquivo salvo com sucesso.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Aviso: O arquivo esta aberto.", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
