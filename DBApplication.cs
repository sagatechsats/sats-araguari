﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SATS
{
    public class DBApplication
    {
        public DBManager DatabaseManager;

        //static string CONNECTION_STRING = "Server=localhost;port=3306;" +
        //"Database=sagamedi_sats;Uid=sagamedi_users1;Pwd=542*sag@;" +
        //"default command timeout=60; connect timeout=60";

        public DBApplication()
        {
            string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
            DatabaseManager = new DBManager(stcon);
        }


        string EncryptOrDecrypt(string text, string key)
        {
            var result = new StringBuilder();

            for (int c = 0; c < text.Length; c++)
                result.Append((char)((uint)text[c] ^ (uint)key[c % key.Length]));

            return result.ToString();
        }

        //-----------------Metodos usados pela classe Form1-----------------------------

        // Para lidar com os altos tempos de resposta do BD(por volta de 5, 6 segundos),
        // foi necessario gerar uma query que buscasse por todos os pontos. So que nao
        // se pode buscar pelo id, ja que o BD pode ser alterado e o id e gerado automaticamente.
        // Entao busca-se a informacao por nome.

        public bool LookForExistingConnections() { return DatabaseManager.LookForExistingConnections(); }

        public string GetServerTime()
        {
            string query = "SELECT CURTIME();";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            if (rs.Count > 0)
                return rs[0][0];
            else
                return "Sem dados.";
        }


        public void SetAllIDs(List<Ponto> listPontos)
        {
            string query = "SELECT id_ponto, nome_ponto FROM ponto";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            foreach (var result in rs)
            {
                for (var i = 0; i < listPontos.Count; ++i)
                {
                    if (result[1] == listPontos[i]._nameBD)
                        listPontos[i]._idBD = result[0];
                }
            }
        }


        //inner join com tabela ponto posicao para pegar as informações do eiXo 
        public List<string[]> AllPontos()
        {
                string query = "select ponto.id_ponto , ponto.nome_ponto, ponto.ponto_tipo, ponto_posicao.eixoX, ponto_posicao.eixoY, ponto.poco, ponto.poco_solteiro from ponto inner join ponto_posicao on  ponto.id_ponto = ponto_posicao.id_ponto";
                List<string[]> rs = DatabaseManager.DeFactoSelect(query);
                return rs;           
        }


        //public void SetAllBombaIDs(List<Ponto> listPontos)
        //{
        //    string query = "SELECT id_ponto, nome_ponto FROM ponto where ponto.ponto_tipo = 1 ";
        //    List<string[]> rs = DatabaseManager.DeFactoSelect(query);
        //    foreach (var result in rs)
        //    {
        //        for (var i = 0; i < listPontos.Count; ++i)
        //        {
        //            if (result[1] == listPontos[i]._nameBD)
        //                listPontos[i]._idBD = result[0];
        //        }
        //    }
        //}

        private string GenerateSQLQueryRetrevial(List<string> listNamesDB)
        {
            string query = "Select l.vazao/3600, nome_ponto, l.data_hora, l.id_ponto " +
                           "from ponto, ponto_leitura l inner join (select id_ponto, max(data_hora) as maxid " +
                           "from ponto_leitura group by id_ponto asc) l2 on l.data_hora = l2.maxid and l.id_ponto = l2.id_ponto " +
                           "where l.id_ponto = ponto.id_ponto group by l.id_ponto asc;";
            /*string str = "(SELECT vazao, nome_ponto, data_hora FROM ponto_leitura INNER JOIN ponto " +
                "ON ponto_leitura.id_ponto = ponto.id_ponto WHERE ponto.nome_ponto = '";
            string st2 = "' ORDER BY data_hora DESC LIMIT 1)";
            string st3 = " UNION ";

            for (var i = 0; i < listNamesDB.Count; ++i)
            {
                query += str + listNamesDB[i] + st2;
                // se existirem mais nomes depois desta iteracao, poe o union
                if (i + 1 <= listNamesDB.Count - 1)
                    query += st3;
            }*/
            return query;
        }


        // rs[i][0] = Vazao mais recente do ponto i
        // rs[i][1] = Nome associado a vazao

        public List<string[]> SelectAllBomba()
        {
            string query = "select l.status_acionar, nome_ponto, l.data_hora, l.id_ponto, l.status_chave, l.data_chave " +
                            "from ponto_aciona l inner join ponto l2 on l.id_ponto= l2.id_ponto " +
                            "group by l.id_ponto asc;";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }

        public List<string[]> manutencao()
        {

            List<string[]> result = DatabaseManager.Select("ponto_manutencao INNER JOIN ponto ON ponto.id_ponto = ponto_manutencao.id_ponto", "nome_ponto,tipo_manutencao, descricao, data_hora", "data_hora>= curdate() and data_hora<= curdate()+ interval 1 day;");
            return result;
        }

        public List<string[]> UpdateVazaoAll(List<Ponto> listPontos, List<string> listNamesDB)
        {

            // Depois de fazer a busca da vazao mais recente de cada ponto, eu ainda
            // preciso voltar verificando resultado por resultado, ja que se nao tiver
            // leitura pra um ponto, o BD nao retorna nada. Eu tive o cuidado de por o
            // listNamesBD e listPontos na ordem, mas o rs nao fica com as posicoes
            // correspondente a esses por essas ausencias de resultados. Entao para atribuir
            // a vazao eu tenho que  verificar o nome_ponto de cada resultado, e associar esse
            // ao nome ja cadastrado antes em _nameBD.

            string query = GenerateSQLQueryRetrevial(listNamesDB);
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }


        public List<string[]> UpdateNivelAll(List<Ponto> listPontos, List<string> listNamesDB)
        {
            string query = "Select l.nivel, nome_ponto, l.data_hora, l.id_ponto from ponto, ponto_nivel l inner join (select id_ponto, max(data_hora) as maxid from ponto_nivel group by id_ponto asc) l2 on l.data_hora = l2.maxid and l.id_ponto = l2.id_ponto where l.id_ponto = ponto.id_ponto group by l.id_ponto asc;";
            return DatabaseManager.DeFactoSelect(query);
        }

        public List<string[]> UpdateBomba(List<Ponto> listPontos, List<string> listNamesDB)
        {
            string query = "Select id_ponto, status_acionar from ponto_aciona order by id_ponto asc;";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }
        public List<string[]> UpdatePhAll(List<Ponto> listPontos, List<string> listNamesDB)
        {
            string query = "Select l.ph, nome_ponto, l.data_hora, l.id_ponto "
                            + "from ponto, ponto_ph l inner join (select id_ponto, max(data_hora) as maxid "
                            + "from ponto_ph group by id_ponto asc) l2 on l.data_hora = l2.maxid and l.id_ponto = l2.id_ponto "
                            + "where l.id_ponto = ponto.id_ponto group by l.id_ponto asc;";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }

        public List<string[]> UpdateEnergiaAll(List<Ponto> listPontos, List<string> listNamesDB)
        {
            string query = "Select l.corrente, nome_ponto, l.data_hora, l.id_ponto "
                            + "from ponto, ponto_energia l inner join (select id_ponto, max(data_hora) as maxid "
                            + "from ponto_energia group by id_ponto asc) l2 on l.data_hora = l2.maxid and l.id_ponto = l2.id_ponto "
                            + "where l.id_ponto = ponto.id_ponto group by l.id_ponto asc;";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }

        public List<string[]> UpdateTurbidezAll(List<Ponto> listPontos, List<string> listNamesDB)
        {
            string query = "Select l.turbidez, nome_ponto, l.data_hora, l.id_turbidez "
                            + "from ponto, ponto_turbidez l inner join (select id_ponto, max(data_hora) as maxid "
                            + "from ponto_turbidez group by id_ponto asc) l2 on l.data_hora = l2.maxid and l.id_ponto = l2.id_ponto "
                            + "where l.id_ponto = ponto.id_ponto group by l.id_ponto asc;";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }

        public List<string[]> UpdatePressaoAll(List<Ponto> listPontos, List<string> listNamesDB)
        {
            string query = "Select l.pressao, nome_ponto, l.data_hora, l.id_ponto "
                            + "from ponto, ponto_pressao l inner join (select id_ponto, max(data_hora) as maxid "
                            + "from ponto_pressao group by id_ponto asc) l2 on l.data_hora = l2.maxid and l.id_ponto = l2.id_ponto "
                            + "where l.id_ponto = ponto.id_ponto group by l.id_ponto asc;";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }

        public List<string[]> UpdateInversorAll(List<Ponto> listPontos, List<string> listNamesDB)
        {
            string query = "Select l.inversor, nome_ponto, l.data_hora, l.id_ponto "
                            + "from ponto, ponto_inversor l inner join (select id_ponto, max(data_hora) as maxid "
                            + "from ponto_inversor group by id_ponto asc) l2 on l.data_hora = l2.maxid and l.id_ponto = l2.id_ponto "
                            + "where l.id_ponto = ponto.id_ponto group by l.id_ponto asc;";

            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }

        public List<string[]> UpdateControleAll(List<Ponto> listPontos, List<string> listNamesDB)
        {
            string query = "Select valor_sanalogica, nome_ponto, p.id_ponto "
                            + "from ponto_aciona pa inner join ponto p "
                            + "on pa.id_ponto= p.id_ponto where controle_analogico =1 order by pa.id_ponto asc;";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            return rs;
        }

        public bool insertvolume(long volume)
        {
            bool inserido = false;
            inserido = DatabaseManager.Insert2("ponto_volume", "volumetotal, data_hora", volume.ToString() + ", Now()");
            return inserido;
        }

        public bool returnquant()
        {
            string query = "select data_hora from ponto_volume where data_hora>CURDATE() and data_hora<CURDATE()+interval 1 day;";
            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            if (rs.Count > 0) return false;
            else return true;
        }

        public long Volumeproduzido()
        {
            long soma = 0;
            string query = "select Max(volume)- Min(volume) as Valor " +
            "from ponto_leitura INNER JOIN ponto ON ponto.id_ponto = ponto_leitura.id_ponto  " +
            "where data_hora> CURDATE()-interval 1 day and  data_hora<=CURDATE() and ponto.ponto_tipo='producao' group by ponto_leitura.id_ponto;";

            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            for (int i = 0; i < rs.Count; i++)
            {
                soma += Convert.ToInt32(rs[i][0].ToString());
            }

            return soma;
        }

        public string[] SelectMostRecentAlarmeEvento()
        {
            string query = "(SELECT nome_ponto, tipo_evento, data_hora FROM ponto_eventos " +
            "INNER JOIN ponto ON ponto.id_ponto = ponto_eventos.id_ponto) UNION (SELECT " +
            "nome_ponto, tipo_alarme, data_hora FROM ponto_alarme INNER JOIN ponto " +
            "ON ponto.id_ponto = ponto_alarme.id_ponto) ORDER BY data_hora DESC LIMIT 1;";

            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            if (rs.Count > 0)
                return rs[0];
            else
                return null;
        }

        public Boolean CheckAlarmsEvents()
        {
            string query = "SELECT id_alarme FROM ponto_alarme WHERE status_alarme = 0 ORDER BY data_hora DESC";

            List<string[]> rs = DatabaseManager.DeFactoSelect(query);
            if (rs.Count() > 0)
                return true;
            else
                return false;
        }

        //-----------------Metodos usados pela classe Ponto-----------------------------

        public string[] SelectPontosMostRecentAlarmeEvento(string nameBD)
        {
            //string query = "(SELECT tipo_alarme, data_hora, nome_ponto, descricao FROM ponto_alarme " +
            //"INNER JOIN ponto ON ponto.id_ponto = ponto_alarme.id_ponto WHERE nome_ponto = '" + nameBD + "') UNION " +
            //"(SELECT tipo_evento, data_hora, nome_ponto, descricao FROM ponto_eventos INNER JOIN  ponto " +
            //"ON ponto.id_ponto = ponto_eventos.id_ponto WHERE nome_ponto = '" + nameBD + "')" +
            //"ORDER BY data_hora DESC LIMIT 1;";

            //string query = "select  tipo_alarme, data_hora, nome_ponto, id_alarme, descricao from ponto_alarme pa inner " +
            //    "join ponto p on pa.id_alarme = p.alarme_id WHERE nome_ponto = '" + nameBD +
            //    "'UNION select tipo_evento, data_hora, nome_ponto, id_evento, descricao " +
            //    "from ponto_eventos pe inner join ponto p on pe.id_ponto = p.evento_id WHERE nome_ponto = '" + nameBD + "';";
         string query = " SELECT tipo_alarme, data_hora, nome_ponto, uae.id_alarme, descricao from ponto_alarme pa " +
                " INNER JOIN ponto p on pa.id_ponto = p.id_ponto INNER JOIN ultimo_alarme_evento uae ON pa.id_alarme = uae.id_alarme WHERE p.nome_ponto = '" + nameBD +"'" +
                " UNION " +
                " SELECT tipo_evento, data_hora, nome_ponto, uae.id_evento, descricao from ponto_eventos pe " +
                " INNER JOIN ponto p ON pe.id_ponto = p.id_ponto " +
                " INNER JOIN ultimo_alarme_evento uae on pe.id_evento = uae.id_evento WHERE p.nome_ponto  = '" + nameBD +"'";



            Console.WriteLine("Alarme "+ query);

            List<string[]> rs = DatabaseManager.DeFactoSelect(query);

            if (rs.Count == 0)
                return null;
            else
                return rs[0];
        }

        public string[] SelectMostRecentLeitura(string idBD, string table, string columns)
        {
            List<string[]> rs = DatabaseManager.Select(table, columns, idBD,
                "data_hora", "1");
            if (rs.Count == 0)
                return null;
            else
                return rs[0];
        }

        public string[] SelectMostRecentLeituraPressao(string idBD, string table, string columns)
        {
            List<string[]> rs = DatabaseManager.Select(table, columns, idBD,
                "data_hora", "1");
            if (rs.Count == 0)
                return null;
            else
                return rs[0];
        }



        public string[] Selectvazaototal()
        {
            string query = "select sum(t.vazao) as vazao " +
                            "from (select ponto.id_ponto, vazao, data_hora from ponto_leitura inner join ponto " +
                             "on ponto.id_ponto = ponto_leitura.id_ponto " +
                             "where data_hora>=now()-interval 3 minute and ponto_tipo = 'producao' and ponto_leitura.id_ponto!=5 " +
                            "group by ponto.id_ponto union " +
                            "select id_ponto, vazao, data_hora from ponto_leitura " +
                            "where id_ponto = 5 and data_hora>=now()-interval 6 minute " +
                            "group by id_ponto order by data_hora desc) as t;";

            //string query = "select sum(t.vazao/1000) as vazao " +
            //                "from (select ponto.id_ponto, vazao/1000, data_hora from ponto_leitura inner join ponto " +
            //                 "on ponto.id_ponto = ponto_leitura.id_ponto " +
            //                 "where data_hora>=now()-interval 3 minute and ponto_tipo = 'producao' and ponto_leitura.id_ponto!=5 " +
            //                "group by ponto.id_ponto union " +
            //                "select id_ponto, vazao/1000, data_hora from ponto_leitura " +
            //                "where id_ponto = 5 and data_hora>=now()-interval 6 minute " +
            //                "group by id_ponto order by data_hora desc) as t;";

            List<string[]> rs = DatabaseManager.DeFactoSelect(query);

            if (rs.Count == 0)
                return null;
            else
                return rs[0];
        }


    }
}
