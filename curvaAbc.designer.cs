﻿namespace SATS
{
    partial class CurvaAbc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CurvaAbc));
            this.tabelaCurvaAbc = new System.Windows.Forms.DataGridView();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pontoCaptacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumePeriodo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaCurvaAbc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabelaCurvaAbc
            // 
            this.tabelaCurvaAbc.AllowUserToAddRows = false;
            this.tabelaCurvaAbc.AllowUserToDeleteRows = false;
            this.tabelaCurvaAbc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.tabelaCurvaAbc.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.tabelaCurvaAbc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tabelaCurvaAbc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pontoCaptacao,
            this.volumePeriodo,
            this.porcentagem});
            this.tabelaCurvaAbc.Location = new System.Drawing.Point(13, 13);
            this.tabelaCurvaAbc.Name = "tabelaCurvaAbc";
            this.tabelaCurvaAbc.ReadOnly = true;
            this.tabelaCurvaAbc.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.tabelaCurvaAbc.Size = new System.Drawing.Size(398, 403);
            this.tabelaCurvaAbc.TabIndex = 0;
            this.tabelaCurvaAbc.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tabelaCurvaAbc_CellContentClick);
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.chart1.BackImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.chart1.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            chartArea1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            chartArea1.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            legend1.BackSecondaryColor = System.Drawing.SystemColors.GradientActiveCaption;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(418, 13);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(341, 403);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "Porcentagem de gasto por região";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // pontoCaptacao
            // 
            this.pontoCaptacao.FillWeight = 61.8792F;
            this.pontoCaptacao.HeaderText = "Pontos de captação";
            this.pontoCaptacao.Name = "pontoCaptacao";
            this.pontoCaptacao.ReadOnly = true;
            this.pontoCaptacao.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.pontoCaptacao.Width = 150;
            // 
            // volumePeriodo
            // 
            this.volumePeriodo.FillWeight = 228.4264F;
            this.volumePeriodo.HeaderText = "Volume total no período (m3)";
            this.volumePeriodo.Name = "volumePeriodo";
            this.volumePeriodo.ReadOnly = true;
            this.volumePeriodo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.volumePeriodo.Width = 170;
            // 
            // porcentagem
            // 
            this.porcentagem.FillWeight = 9.694408F;
            this.porcentagem.HeaderText = "%";
            this.porcentagem.Name = "porcentagem";
            this.porcentagem.ReadOnly = true;
            this.porcentagem.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.porcentagem.Width = 50;
            // 
            // curvaAbc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(771, 428);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.tabelaCurvaAbc);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(787, 467);
            this.Name = "curvaAbc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Curva ABC";
            this.Load += new System.EventHandler(this.curvaAbc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabelaCurvaAbc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView tabelaCurvaAbc;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pontoCaptacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumePeriodo;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentagem;
    }
}