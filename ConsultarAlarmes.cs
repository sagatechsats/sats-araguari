﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class ConsultarAlarmes : Office2007Form
    {
        protected DBManager dbManager;
        [DllImport("wininet.dll")]

        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private List<string> indexClientes;
        public ConsultarAlarmes()
        {
            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
        }

        private void consultaralarmes_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Thread novaThread = new Thread(new ThreadStart(CarregaTela));
            novaThread.Start();
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;

        }
        public class drop
        {
            public drop(string nome, int valor)
            {
                Nome = nome;
                Valor = valor;
            }
            string _nome;
            int _valor;
            public int Valor
            {
                get
                {
                    return _valor;
                }
                set
                {
                    _valor = value;
                }
            }
            public string Nome
            {
                get
                {
                    return _nome;
                }
                set
                {
                    _nome = value;
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar-se um ponto.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            String cliente = comboBox1.Text;
            Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTabela));
            novaThread.Start(cliente);
        }

        private void CarregaTabela(object parametro)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            try
            {
                string cliente = parametro.ToString();
                //Pega o valor selecionado no combobox de cliente         
                string[] words = cliente.Split('-'); //Separa o id do nome do cliente        
                DateTime result = dateTimePicker1.Value;
                string data = result.ToString("yyyy-MM-dd "); //pega a data escolhida pelo usuário

                dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                ArrayList arr = new ArrayList();
                List<string[]> consultalist = dbManager.Select("ponto_alarme", "tipo_alarme, descricao, data_hora", "id_ponto=" + words[0] + " and data_hora between '" + data + " 00:00:00' and '" + data + " 23:59:59' order by data_hora");
                Invoke((MethodInvoker)(() => preencheList(consultalist)));
            }
            catch (Exception)
            {

            }
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void preencheList(List<string[]> consultalist)
        {
            listView1.Items.Clear();
            foreach (string[] linha in consultalist)
            {
                string[] subitems = new string[3];
                object[] o = linha;
                for (int i = 0; i < 3; i++)
                {
                    subitems[i] = o[i].ToString();
                }
                ListViewItem item = new ListViewItem(subitems);
                listView1.Items.Add(item); //carrega os itens no list view
            }
            label3.Text = "Quantidade de alarmes: " + consultalist.Count;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CarregaTela()
        {
            if (IsConnected() == true)
            {
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    ArrayList arr = new ArrayList();
                    List<string> consulta = GetClientes();
                    Invoke((MethodInvoker)(() => PassListToComboBox(consulta)));
                    Invoke((MethodInvoker)(() => PassListToAutoComplete(consulta)));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            //Thread.Sleep(1000 * 60);
        }

        protected List<string> GetClientes()
        {
            indexClientes = new List<string>();
            List<string> listConcat = new List<string>();
            List<string[]> res = dbManager.Select("ponto order by id_ponto ASC", "id_ponto, nome_ponto", "");

            foreach (string[] item in res)
            {
                string concat = String.Empty;
                indexClientes.Add(item[0]);//posicao 0 deve ser id_cliente.
                concat += Int32.Parse(item[0]).ToString("D6");
                concat += " - ";
                concat += item[1];
                listConcat.Add(concat);
            }
            return listConcat;
        }

        protected void PassListToAutoComplete(List<string> list)
        {
            AutoCompleteStringCollection stringCollection = new AutoCompleteStringCollection();
            foreach (string concat in list)
                stringCollection.Add(concat);
            comboBox1.AutoCompleteCustomSource = stringCollection;
        }

        private void PassListToComboBox(List<string> list)
        {
            comboBox1.Items.Clear();
            foreach (string concat in list)
                comboBox1.Items.Add(concat);
        }
    }
}
