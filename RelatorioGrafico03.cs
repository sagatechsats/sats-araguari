﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.DataVisualization.Charting.Borders3D;
using System.Windows.Forms.DataVisualization.Charting.ChartTypes;
using System.Windows.Forms.DataVisualization.Charting.Data;
using System.Windows.Forms.DataVisualization.Charting.Formulas;
using System.Windows.Forms.DataVisualization.Charting.Utilities;
using DevComponents.DotNetBar;


namespace SATS
{
    public partial class RelatorioGrafico03 : Office2007Form
    {
        public int volumeOuVazao;
        int count = 0;
        string diat = "";

        public RelatorioGrafico03(int optionVolumeOuVazao, int count1, string diat1)
        {
            
            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;

            volumeOuVazao = optionVolumeOuVazao;
            count = count1;
            diat = diat1;


            chart1.Series[0].Enabled = false;
            
                for (int i = 0; i < count; i++)
                {
                    chart1.Series.Add("Vazão(m³/H): "+ diat.ToString() +" "+ (i+1).ToString());
                    
                    //chart1.Series[i].Name = "Vazão";
                }

                for (int i = 0; i <= count; i++)
                {
                    chart1.Series[i].ChartType = SeriesChartType.Line;
                    chart1.Series[i].IsValueShownAsLabel = false;
                }
                
        }

        private string ReturnDecimalWithDot(string commaDecimal)
        {
            char[] newDecimal = commaDecimal.ToCharArray();
            for (int i = 0; i < newDecimal.Length; ++i)
            {
                if (newDecimal[i] == ',')
                    newDecimal[i] = '.';
            }
            return new string(newDecimal);
        }

        private float StringToFloat(string value)
        {
            float temp = (float)Convert.ToDouble(value);
            return temp;
        }

        private decimal StringToDecimal(string value)
        {
            try
            {
                decimal temp = Convert.ToDecimal(value);
                return Decimal.Round(temp, 2);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void ReceiveFromListView(List<string[]> items)
        {
            /*Colunas da listView
            0 e volume
            1 e vazao
            2 e data hora*/

            //if (listView.Items.Count > 20)
                chart1.Series[0].IsValueShownAsLabel = true;
            /*else
                chart1.Series[0].IsValueShownAsLabel = true;*/
            // pesquisar depois pra inserir mais grades se tiver 
            // mais de 20 items pra facilitar a visualizacao,
            // ja que nao teremos rotulos para esse caso
                int j = 0;
                int series=0;
                //foreach (ListViewItem item in listView.Items)
            for(int i=0; i<items.Count();i++)
                {
                    if (j%24==0)
                    {
                        //j = 0;
                        series++;

                    }

                    int temp = volumeOuVazao;
                    chart1.Series[series].Points.AddXY(items[i][2].ToString(),
                        StringToDecimal(items[i][temp].ToString()));

                    if (items[i][temp].ToString() == "")
                    {
                        chart1.Series[series].Points[j % 24 ].IsEmpty = true;
                    }

                    j++;
                }
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void Grafico_Load(object sender, EventArgs e)
        {
        }
    }
}
