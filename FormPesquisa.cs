﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SATS
{
    public partial class FormPesquisa : Form
    {
        protected DBManager dbManager;
        private DataTable dt = new DataTable();
        private string[] returnArray;

        public FormPesquisa(ref string[] returnArray)
        {
            InitializeComponent();
            this.returnArray = returnArray;
            //dt.Columns.Add("idAcionamento", typeof(int));
          
            dt.Columns.Add("IdBomba", typeof(int));
            dt.Columns.Add("Bomba", typeof(string));
            dt.Columns.Add("IdReservatorio", typeof(int));
            dt.Columns.Add("Habilitado", typeof(string));
            dt.Columns.Add("Nivel Mínimo", typeof(string));
            dt.Columns.Add("Nivel Máximo", typeof(string));
            dt.Columns.Add("NivelMin ForaPonta", typeof(string));
            dt.Columns.Add("NivelMax ForaPonta", typeof(string));
            //dt.Columns.Add("HoraLiga", typeof(string));
            //dt.Columns.Add("HoraDesliga", typeof(string));

            dgvAcionamentos.DataSource = dt;
            //dgvAcionamentos.Columns["idAcionamento"].Visible = false;

            populateInitialGridView();

            
        }
        private void populateInitialGridView()
        {
            dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
            List<string[]> result = dbManager.Select("ponto as pp, ponto_automatico as p", "p.id_bomba, pp.nome_ponto, p.id_ponto,  p.habilitado, p.nivel_liga, p.nivel_desliga, p.nivel_liga_fponta, p.nivel_desliga_fponta, p.hora_liga, p.hora_desliga", "p.id_bomba = pp.id_ponto  order by id_bomba asc");

            try
            {
                for (int i = 0; i < result.Count(); i++)
                {
                    DataRow dr = dt.NewRow();
                    //dr["idAcionamento"] = i+1;
                    
                    dr["IdBomba"] = Convert.ToInt32(result[i][0]);
                    dr["Bomba"] = result[i][1].ToString();
                    dr["IdReservatorio"] = Convert.ToInt32(result[i][2]);
                    dr["Habilitado"] =result[i][3].ToString();
                    dr["Nivel Mínimo"] = result[i][4].ToString();
                    dr["Nivel Máximo"] = result[i][5].ToString();
                    dr["NivelMin ForaPonta"] = result[i][6].ToString();
                    dr["NivelMax ForaPonta"] = result[i][7].ToString();
                    //dr["HoraLiga"] = result[i][8].ToString();
                    //dr["HoraDesliga"] = result[i][9].ToString();
                    
                    //dr["idHora"] = Convert.ToInt32(result[i][0]);

                    //dgvAcionamentos.Columns["idAcionamento"].Visible = false;
                    dgvAcionamentos.DataSource = dt;
                    dt.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Error,
                   MessageBoxDefaultButton.Button1);
            }
        }

        private void dgvAcionamento_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                for (int i = 0; i < dgvAcionamentos.ColumnCount; i++)
                {
                    if (e.RowIndex != -1)
                    {
                        if (dgvAcionamentos[i, e.RowIndex].Value == null)
                            returnArray[i] = "";
                        else
                            returnArray[i] = dgvAcionamentos[i, e.RowIndex].Value.ToString();
                    }
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error,
                MessageBoxDefaultButton.Button1);
            }
        }
    }
}
