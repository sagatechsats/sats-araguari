﻿# Host: 127.0.0.1  (Version 5.7.14)
# Date: 2018-01-19 14:22:42
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "acao"
#

CREATE TABLE `acao` (
  `id_acao` int(11) NOT NULL AUTO_INCREMENT,
  `nome_acao` varchar(30) NOT NULL DEFAULT '',
  `data_acao` datetime NOT NULL,
  `descricao` varchar(200) DEFAULT '',
  PRIMARY KEY (`id_acao`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "acao"
#


#
# Structure for table "gastos"
#

CREATE TABLE `gastos` (
  `id_gasto` int(11) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) NOT NULL,
  `tipo_gasto` int(11) NOT NULL,
  `data_referencia` date NOT NULL,
  `data_inicial` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_final` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gasto` int(11) NOT NULL DEFAULT '0',
  `valor` decimal(11,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`id_gasto`),
  UNIQUE KEY `identificadores` (`id_ponto`,`tipo_gasto`,`data_referencia`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "gastos"
#


#
# Structure for table "ponto"
#

CREATE TABLE `ponto` (
  `id_ponto` int(11) NOT NULL AUTO_INCREMENT,
  `nome_ponto` varchar(30) NOT NULL,
  `ddns` varchar(30) DEFAULT NULL,
  `ponto_tipo` int(10) NOT NULL DEFAULT '0',
  `bomba_ponto` int(11) NOT NULL DEFAULT '0',
  `porta_nivel` int(11) NOT NULL DEFAULT '0',
  `nivel_min` decimal(10,2) NOT NULL DEFAULT '0.00',
  `nivel_max` decimal(10,2) NOT NULL DEFAULT '0.00',
  `porta_rele` int(11) NOT NULL DEFAULT '0',
  `max_reservatorio` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` varchar(50) NOT NULL DEFAULT '',
  `pulsar` int(11) NOT NULL DEFAULT '0',
  `ordem` int(11) NOT NULL DEFAULT '0',
  `adress` int(11) NOT NULL DEFAULT '1',
  `porta_energia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ponto`),
  UNIQUE KEY `nome_ponto` (`nome_ponto`),
  KEY `id_ponto` (`id_ponto`),
  KEY `nome_ponto_2` (`nome_ponto`),
  KEY `nivel_max` (`nivel_max`),
  KEY `nivel_min` (`nivel_min`),
  KEY `max_reservatorio` (`max_reservatorio`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

#
# Data for table "ponto"
#


#
# Structure for table "ponto_aciona"
#

CREATE TABLE `ponto_aciona` (
  `id_ponto` int(11) DEFAULT '0',
  `acionar` int(1) NOT NULL DEFAULT '0',
  `status_acionar` int(11) NOT NULL DEFAULT '0',
  `porta` int(11) NOT NULL DEFAULT '6',
  `data_hora` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id_ponto` (`id_ponto`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "ponto_aciona"
#


#
# Structure for table "ponto_alarme"
#

CREATE TABLE `ponto_alarme` (
  `id_alarme` int(11) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) DEFAULT '0',
  `tipo_alarme` varchar(50) NOT NULL DEFAULT '',
  `descricao` varchar(100) DEFAULT NULL,
  `data_hora` datetime NOT NULL,
  `status_alarme` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_alarme`),
  KEY `id_ponto` (`id_ponto`),
  KEY `id_alarme` (`id_alarme`),
  KEY `id_ponto_2` (`id_ponto`),
  KEY `data_hora` (`data_hora`),
  KEY `status_alarme` (`status_alarme`)
) ENGINE=MyISAM AUTO_INCREMENT=90541 DEFAULT CHARSET=utf8;

#
# Data for table "ponto_alarme"
#


#
# Structure for table "ponto_automatico"
#

CREATE TABLE `ponto_automatico` (
  `id_ponto` int(11) NOT NULL DEFAULT '0',
  `id_bomba` int(11) NOT NULL DEFAULT '0',
  `habilitado` tinyint(1) NOT NULL DEFAULT '0',
  `hora_liga` time NOT NULL DEFAULT '00:00:00',
  `hora_desliga` time NOT NULL DEFAULT '00:00:00',
  `nivel_liga` decimal(10,2) NOT NULL DEFAULT '0.00',
  `nivel_desliga` decimal(10,2) NOT NULL DEFAULT '0.00',
  `nivel_liga_fponta` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_bomba2` int(11) NOT NULL DEFAULT '0',
  `id_reservatorio2` int(11) NOT NULL DEFAULT '0',
  `nivel_min_res2` decimal(10,2) NOT NULL DEFAULT '0.00',
  UNIQUE KEY `id_ponto` (`id_ponto`),
  UNIQUE KEY `id_bomba` (`id_bomba`),
  KEY `hora_liga` (`hora_liga`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "ponto_automatico"
#


#
# Structure for table "ponto_eventos"
#

CREATE TABLE `ponto_eventos` (
  `id_evento` int(11) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) DEFAULT '0',
  `tipo_evento` varchar(50) NOT NULL DEFAULT '',
  `descricao` varchar(100) DEFAULT NULL,
  `data_hora` datetime NOT NULL,
  PRIMARY KEY (`id_evento`),
  KEY `id_ponto` (`id_ponto`),
  KEY `id_evento` (`id_evento`),
  KEY `id_ponto_2` (`id_ponto`),
  KEY `data_hora` (`data_hora`)
) ENGINE=MyISAM AUTO_INCREMENT=1215597 DEFAULT CHARSET=utf8;

#
# Data for table "ponto_eventos"
#


#
# Structure for table "ponto_horario"
#

CREATE TABLE `ponto_horario` (
  `Id_hora` int(11) NOT NULL AUTO_INCREMENT,
  `id_bomba` int(11) DEFAULT NULL,
  `hora_liga` time NOT NULL DEFAULT '00:00:00',
  `hora_desliga` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`Id_hora`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "ponto_horario"
#


#
# Structure for table "ponto_inversor"
#

CREATE TABLE `ponto_inversor` (
  `id_leitura` int(11) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) DEFAULT '0',
  `frequencia` decimal(10,2) DEFAULT '0.00',
  `date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_leitura`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "ponto_inversor"
#


#
# Structure for table "ponto_leitura"
#

CREATE TABLE `ponto_leitura` (
  `id_leitura` bigint(15) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) DEFAULT '0',
  `volume` int(11) NOT NULL DEFAULT '0',
  `vazao` decimal(10,2) NOT NULL DEFAULT '0.00',
  `data_hora` datetime NOT NULL,
  PRIMARY KEY (`id_leitura`),
  KEY `id_ponto` (`id_ponto`),
  KEY `data_hora` (`data_hora`),
  KEY `id_ponto_2` (`id_ponto`),
  KEY `id_leitura` (`id_leitura`)
) ENGINE=MyISAM AUTO_INCREMENT=170777 DEFAULT CHARSET=utf8;

#
# Data for table "ponto_leitura"
#


#
# Structure for table "ponto_manutencao"
#

CREATE TABLE `ponto_manutencao` (
  `id_manutencao` int(11) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) NOT NULL,
  `data_hora` datetime NOT NULL,
  `tipo_manutencao` varchar(50) NOT NULL,
  `descricao` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id_manutencao`),
  KEY `id_ponto` (`id_ponto`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "ponto_manutencao"
#


#
# Structure for table "ponto_nivel"
#

CREATE TABLE `ponto_nivel` (
  `id_leitura` bigint(15) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) DEFAULT '0',
  `nivel` decimal(10,2) NOT NULL DEFAULT '0.00',
  `data_hora` datetime NOT NULL,
  PRIMARY KEY (`id_leitura`),
  KEY `id_ponto` (`id_ponto`),
  KEY `id_leitura` (`id_leitura`),
  KEY `id_ponto_2` (`id_ponto`),
  KEY `data_hora` (`data_hora`)
) ENGINE=MyISAM AUTO_INCREMENT=4798082 DEFAULT CHARSET=utf8;

#
# Data for table "ponto_nivel"
#


#
# Structure for table "ponto_ph"
#

CREATE TABLE `ponto_ph` (
  `id_leitura` bigint(15) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) DEFAULT '0',
  `ph` decimal(10,2) NOT NULL DEFAULT '0.00',
  `data_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_leitura`),
  KEY `data_hora` (`data_hora`),
  KEY `data_hora_2` (`data_hora`),
  KEY `data_hora_3` (`data_hora`),
  KEY `id_leitura` (`id_leitura`),
  KEY `id_ponto` (`id_ponto`),
  KEY `id_ponto_2` (`id_ponto`)
) ENGINE=MyISAM AUTO_INCREMENT=32183 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

#
# Data for table "ponto_ph"
#


#
# Structure for table "ponto_pid"
#

CREATE TABLE `ponto_pid` (
  `id_inversor` int(11) NOT NULL DEFAULT '0',
  `id_pressao` int(11) DEFAULT '0',
  `habilitado` tinyint(1) NOT NULL DEFAULT '0',
  `pressao_set_point` decimal(10,2) DEFAULT '0.00',
  `frequencia_max` decimal(10,2) DEFAULT '60.00',
  PRIMARY KEY (`id_inversor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "ponto_pid"
#


#
# Structure for table "ponto_pressao"
#

CREATE TABLE `ponto_pressao` (
  `id_pressao` bigint(15) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) DEFAULT '0',
  `pressao` decimal(10,2) NOT NULL DEFAULT '0.00',
  `data_hora` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pressao`),
  KEY `id_ponto` (`id_ponto`)
) ENGINE=MyISAM AUTO_INCREMENT=6971967 DEFAULT CHARSET=utf8;

#
# Data for table "ponto_pressao"
#


#
# Structure for table "ponto_turbidez"
#

CREATE TABLE `ponto_turbidez` (
  `id_turbidez` bigint(15) NOT NULL AUTO_INCREMENT,
  `id_ponto` int(11) DEFAULT '0',
  `turbidez` decimal(10,2) NOT NULL DEFAULT '0.00',
  `data_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_turbidez`),
  KEY `data_hora` (`data_hora`),
  KEY `id_ponto` (`id_ponto`),
  KEY `id_turbidez` (`id_turbidez`)
) ENGINE=MyISAM AUTO_INCREMENT=64376 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

#
# Data for table "ponto_turbidez"
#


#
# Structure for table "sats_user"
#

CREATE TABLE `sats_user` (
  `nome_user` varchar(30) NOT NULL,
  `senha_user` varchar(512) NOT NULL DEFAULT '',
  `permissao_user` varchar(20) NOT NULL,
  `email_user` varchar(30) DEFAULT NULL,
  `celular_user` varchar(11) DEFAULT NULL,
  `send_to_user` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`nome_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "sats_user"
#

INSERT INTO `sats_user` VALUES ('DARLAN','d37b8aecd85d96f8977fb643083b604e15472b2efc6da3941092653d3f07a916','Supervisor',NULL,NULL,NULL),('DIEGO','bbdd643aa434b46f36a3cea2f2add3bf083da142cfeedab04f90ca1eec72db9d','Supervisor',NULL,NULL,0),('GUSTAVOC','a8301d61bd82f51e35f8ef5fe43b397db863894163b9794a7418184f50d4350d','Supervisor',NULL,NULL,0),('LUIS.DUARTE','eb24db2c81bea435ce414f3ae0d83122719c17ff3b1836354aa9624cd5e80639','Supervisor','almox@sagamedicao.com.br','3898395688',1),('MARCIAD','b6f2bcf51058fca47522e8bae1b1ac30d4aed84d0d830b38b16c7c85d7f55b4a','Supervisor',NULL,NULL,0),('MARCILIO','18ce57bf0355f516f924d90bbaaf93ab7358538932f1a7756e657e2c30661d6f','Supervisor',NULL,NULL,0),('RODRIGOP','205618d3e5e5e7cc870ab83f775c589a1581ff9082c6dea0138cefc258bf8f1e','Supervisor',NULL,NULL,0),('SAGATECH','0c141b9024c9847943a59cea91a8de526fb84b96459a9f6478ef2199f4d0774d','Supervisor','no-reply@sagamedicao.com.br',NULL,0);
