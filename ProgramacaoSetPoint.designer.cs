﻿namespace SATS
{
    //SetPoint
    partial class ProgramacaoSetPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgramacaoSetPoint));
            this.label2 = new System.Windows.Forms.Label();
            this.txtSetPoint = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cBbomba = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFreqMin = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cBpressao = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtFreqMax = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvTemporizacao = new System.Windows.Forms.DataGridView();
            this.txtGanhoProp = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textSun = new System.Windows.Forms.TextBox();
            this.textMon = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textTue = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textFri = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textThu = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textWed = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textSat = new System.Windows.Forms.TextBox();
            this.groupBoxSemana = new System.Windows.Forms.GroupBox();
            this.groupBoxHorario = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCadHSetPoint = new System.Windows.Forms.Button();
            this.btnAtualizarH = new System.Windows.Forms.Button();
            this.bttnCancelar = new System.Windows.Forms.Button();
            this.bttnSalvar = new System.Windows.Forms.Button();
            this.bttnNovo = new System.Windows.Forms.Button();
            this.bttnEditar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemporizacao)).BeginInit();
            this.groupBoxSemana.SuspendLayout();
            this.groupBoxHorario.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(194, 137);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 69;
            this.label2.Text = "m.c.a";
            // 
            // txtSetPoint
            // 
            this.txtSetPoint.Enabled = false;
            this.txtSetPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSetPoint.Location = new System.Drawing.Point(109, 131);
            this.txtSetPoint.MaxLength = 5;
            this.txtSetPoint.Name = "txtSetPoint";
            this.txtSetPoint.Size = new System.Drawing.Size(84, 22);
            this.txtSetPoint.TabIndex = 5;
            this.txtSetPoint.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSetPoint.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(106, 112);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 16);
            this.label6.TabIndex = 68;
            this.label6.Text = "SetPoint Pressão";
            // 
            // checkBox
            // 
            this.checkBox.AutoSize = true;
            this.checkBox.Enabled = false;
            this.checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox.Location = new System.Drawing.Point(108, 76);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(94, 22);
            this.checkBox.TabIndex = 4;
            this.checkBox.Text = "Habilitar ";
            this.checkBox.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 19);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 16);
            this.label8.TabIndex = 59;
            this.label8.Text = "Ponto Bomba:";
            // 
            // cBbomba
            // 
            this.cBbomba.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBbomba.FormattingEnabled = true;
            this.cBbomba.Location = new System.Drawing.Point(108, 16);
            this.cBbomba.Name = "cBbomba";
            this.cBbomba.Size = new System.Drawing.Size(308, 24);
            this.cBbomba.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(194, 192);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 16);
            this.label3.TabIndex = 72;
            this.label3.Text = "Hz";
            // 
            // txtFreqMin
            // 
            this.txtFreqMin.Enabled = false;
            this.txtFreqMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFreqMin.Location = new System.Drawing.Point(109, 186);
            this.txtFreqMin.MaxLength = 5;
            this.txtFreqMin.Name = "txtFreqMin";
            this.txtFreqMin.Size = new System.Drawing.Size(84, 22);
            this.txtFreqMin.TabIndex = 7;
            this.txtFreqMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFreqMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(106, 167);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 16);
            this.label4.TabIndex = 71;
            this.label4.Text = "Frequência Mínima";
            // 
            // cBpressao
            // 
            this.cBpressao.Enabled = false;
            this.cBpressao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBpressao.FormattingEnabled = true;
            this.cBpressao.Location = new System.Drawing.Point(108, 46);
            this.cBpressao.Name = "cBpressao";
            this.cBpressao.Size = new System.Drawing.Size(308, 24);
            this.cBpressao.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 74;
            this.label1.Text = "Ponto Pressão:";
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerColorTint = System.Drawing.Color.CornflowerBlue;
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(329, 192);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 16);
            this.label9.TabIndex = 81;
            this.label9.Text = "Hz";
            // 
            // txtFreqMax
            // 
            this.txtFreqMax.Enabled = false;
            this.txtFreqMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFreqMax.Location = new System.Drawing.Point(245, 187);
            this.txtFreqMax.MaxLength = 5;
            this.txtFreqMax.Name = "txtFreqMax";
            this.txtFreqMax.Size = new System.Drawing.Size(84, 22);
            this.txtFreqMax.TabIndex = 8;
            this.txtFreqMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFreqMax.TextChanged += new System.EventHandler(this.txtminFponta_TextChanged);
            this.txtFreqMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(242, 168);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 16);
            this.label10.TabIndex = 80;
            this.label10.Text = "Frequência Máxima";
            // 
            // dgvTemporizacao
            // 
            this.dgvTemporizacao.AllowUserToAddRows = false;
            this.dgvTemporizacao.AllowUserToResizeColumns = false;
            this.dgvTemporizacao.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvTemporizacao.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTemporizacao.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTemporizacao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvTemporizacao.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvTemporizacao.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTemporizacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTemporizacao.Location = new System.Drawing.Point(12, 23);
            this.dgvTemporizacao.Name = "dgvTemporizacao";
            this.dgvTemporizacao.RowHeadersVisible = false;
            this.dgvTemporizacao.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvTemporizacao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTemporizacao.Size = new System.Drawing.Size(329, 148);
            this.dgvTemporizacao.TabIndex = 91;
            // 
            // txtGanhoProp
            // 
            this.txtGanhoProp.Enabled = false;
            this.txtGanhoProp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGanhoProp.Location = new System.Drawing.Point(245, 131);
            this.txtGanhoProp.MaxLength = 4;
            this.txtGanhoProp.Name = "txtGanhoProp";
            this.txtGanhoProp.Size = new System.Drawing.Size(84, 22);
            this.txtGanhoProp.TabIndex = 6;
            this.txtGanhoProp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtGanhoProp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(242, 112);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 16);
            this.label14.TabIndex = 94;
            this.label14.Text = "Ganho Proporcional";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(14, 61);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 16);
            this.label11.TabIndex = 96;
            this.label11.Text = "m.c.a";
            // 
            // textSun
            // 
            this.textSun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSun.Location = new System.Drawing.Point(17, 36);
            this.textSun.MaxLength = 5;
            this.textSun.Name = "textSun";
            this.textSun.Size = new System.Drawing.Size(37, 22);
            this.textSun.TabIndex = 95;
            this.textSun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textSun.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // textMon
            // 
            this.textMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMon.Location = new System.Drawing.Point(76, 36);
            this.textMon.MaxLength = 5;
            this.textMon.Name = "textMon";
            this.textMon.Size = new System.Drawing.Size(37, 22);
            this.textMon.TabIndex = 97;
            this.textMon.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textMon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(75, 61);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 16);
            this.label12.TabIndex = 98;
            this.label12.Text = "m.c.a";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(4, 17);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 16);
            this.label13.TabIndex = 99;
            this.label13.Text = "Domingo";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(68, 17);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 16);
            this.label15.TabIndex = 100;
            this.label15.Text = "Segunda";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(129, 17);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 16);
            this.label16.TabIndex = 103;
            this.label16.Text = "Terça";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(132, 61);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 16);
            this.label17.TabIndex = 102;
            this.label17.Text = "m.c.a";
            // 
            // textTue
            // 
            this.textTue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTue.Location = new System.Drawing.Point(133, 36);
            this.textTue.MaxLength = 5;
            this.textTue.Name = "textTue";
            this.textTue.Size = new System.Drawing.Size(37, 22);
            this.textTue.TabIndex = 101;
            this.textTue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textTue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(286, 17);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 16);
            this.label18.TabIndex = 112;
            this.label18.Text = "Sexta";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(289, 61);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 16);
            this.label19.TabIndex = 111;
            this.label19.Text = "m.c.a";
            // 
            // textFri
            // 
            this.textFri.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFri.Location = new System.Drawing.Point(290, 36);
            this.textFri.MaxLength = 5;
            this.textFri.Name = "textFri";
            this.textFri.Size = new System.Drawing.Size(37, 22);
            this.textFri.TabIndex = 110;
            this.textFri.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textFri.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(236, 17);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 16);
            this.label20.TabIndex = 109;
            this.label20.Text = "Quinta";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(181, 17);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 16);
            this.label21.TabIndex = 108;
            this.label21.Text = "Quarta";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(239, 61);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 16);
            this.label22.TabIndex = 107;
            this.label22.Text = "m.c.a";
            // 
            // textThu
            // 
            this.textThu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textThu.Location = new System.Drawing.Point(242, 36);
            this.textThu.MaxLength = 5;
            this.textThu.Name = "textThu";
            this.textThu.Size = new System.Drawing.Size(37, 22);
            this.textThu.TabIndex = 106;
            this.textThu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textThu.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(185, 61);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(40, 16);
            this.label23.TabIndex = 105;
            this.label23.Text = "m.c.a";
            // 
            // textWed
            // 
            this.textWed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textWed.Location = new System.Drawing.Point(188, 36);
            this.textWed.MaxLength = 5;
            this.textWed.Name = "textWed";
            this.textWed.Size = new System.Drawing.Size(37, 22);
            this.textWed.TabIndex = 104;
            this.textWed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textWed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(334, 17);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(57, 16);
            this.label24.TabIndex = 115;
            this.label24.Text = "Sábado";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(344, 61);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 16);
            this.label25.TabIndex = 114;
            this.label25.Text = "m.c.a";
            // 
            // textSat
            // 
            this.textSat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSat.Location = new System.Drawing.Point(345, 36);
            this.textSat.MaxLength = 5;
            this.textSat.Name = "textSat";
            this.textSat.Size = new System.Drawing.Size(37, 22);
            this.textSat.TabIndex = 113;
            this.textSat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textSat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVazao_KeyPress);
            // 
            // groupBoxSemana
            // 
            this.groupBoxSemana.Controls.Add(this.label24);
            this.groupBoxSemana.Controls.Add(this.textSun);
            this.groupBoxSemana.Controls.Add(this.label25);
            this.groupBoxSemana.Controls.Add(this.label11);
            this.groupBoxSemana.Controls.Add(this.textSat);
            this.groupBoxSemana.Controls.Add(this.textMon);
            this.groupBoxSemana.Controls.Add(this.label18);
            this.groupBoxSemana.Controls.Add(this.label12);
            this.groupBoxSemana.Controls.Add(this.label19);
            this.groupBoxSemana.Controls.Add(this.label13);
            this.groupBoxSemana.Controls.Add(this.textFri);
            this.groupBoxSemana.Controls.Add(this.label15);
            this.groupBoxSemana.Controls.Add(this.label20);
            this.groupBoxSemana.Controls.Add(this.textTue);
            this.groupBoxSemana.Controls.Add(this.label21);
            this.groupBoxSemana.Controls.Add(this.label17);
            this.groupBoxSemana.Controls.Add(this.label22);
            this.groupBoxSemana.Controls.Add(this.label16);
            this.groupBoxSemana.Controls.Add(this.textThu);
            this.groupBoxSemana.Controls.Add(this.textWed);
            this.groupBoxSemana.Controls.Add(this.label23);
            this.groupBoxSemana.Enabled = false;
            this.groupBoxSemana.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxSemana.Location = new System.Drawing.Point(109, 407);
            this.groupBoxSemana.Name = "groupBoxSemana";
            this.groupBoxSemana.Size = new System.Drawing.Size(475, 108);
            this.groupBoxSemana.TabIndex = 116;
            this.groupBoxSemana.TabStop = false;
            this.groupBoxSemana.Text = "Setpoint por dias da semana";
            // 
            // groupBoxHorario
            // 
            this.groupBoxHorario.Controls.Add(this.btnDelete);
            this.groupBoxHorario.Controls.Add(this.btnCadHSetPoint);
            this.groupBoxHorario.Controls.Add(this.btnAtualizarH);
            this.groupBoxHorario.Controls.Add(this.dgvTemporizacao);
            this.groupBoxHorario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxHorario.Location = new System.Drawing.Point(109, 224);
            this.groupBoxHorario.Name = "groupBoxHorario";
            this.groupBoxHorario.Size = new System.Drawing.Size(475, 177);
            this.groupBoxHorario.TabIndex = 117;
            this.groupBoxHorario.TabStop = false;
            this.groupBoxHorario.Text = "Setpoint por Horário";
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = global::SATS.Properties.Resources.Action_delete_icon;
            this.btnDelete.Location = new System.Drawing.Point(348, 92);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(117, 33);
            this.btnDelete.TabIndex = 120;
            this.btnDelete.Text = "Deletar";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCadHSetPoint
            // 
            this.btnCadHSetPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadHSetPoint.Image = global::SATS.Properties.Resources.Crystal_Clear_action_edit_add;
            this.btnCadHSetPoint.Location = new System.Drawing.Point(347, 51);
            this.btnCadHSetPoint.Name = "btnCadHSetPoint";
            this.btnCadHSetPoint.Size = new System.Drawing.Size(118, 35);
            this.btnCadHSetPoint.TabIndex = 118;
            this.btnCadHSetPoint.Text = "Novo Horário";
            this.btnCadHSetPoint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCadHSetPoint.UseVisualStyleBackColor = true;
            this.btnCadHSetPoint.Click += new System.EventHandler(this.btnCadHSetPoint_Click);
            // 
            // btnAtualizarH
            // 
            this.btnAtualizarH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizarH.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizarH.Image")));
            this.btnAtualizarH.Location = new System.Drawing.Point(348, 136);
            this.btnAtualizarH.Name = "btnAtualizarH";
            this.btnAtualizarH.Size = new System.Drawing.Size(118, 35);
            this.btnAtualizarH.TabIndex = 119;
            this.btnAtualizarH.Text = "Atualizar";
            this.btnAtualizarH.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAtualizarH.UseVisualStyleBackColor = true;
            this.btnAtualizarH.Click += new System.EventHandler(this.btnAtualizarH_Click_1);
            // 
            // bttnCancelar
            // 
            this.bttnCancelar.Enabled = false;
            this.bttnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("bttnCancelar.Image")));
            this.bttnCancelar.Location = new System.Drawing.Point(370, 545);
            this.bttnCancelar.Name = "bttnCancelar";
            this.bttnCancelar.Size = new System.Drawing.Size(97, 35);
            this.bttnCancelar.TabIndex = 11;
            this.bttnCancelar.Text = "Cancelar";
            this.bttnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnCancelar.UseVisualStyleBackColor = true;
            this.bttnCancelar.Click += new System.EventHandler(this.bttnCancelar_Click);
            // 
            // bttnSalvar
            // 
            this.bttnSalvar.Enabled = false;
            this.bttnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnSalvar.Image = ((System.Drawing.Image)(resources.GetObject("bttnSalvar.Image")));
            this.bttnSalvar.Location = new System.Drawing.Point(256, 545);
            this.bttnSalvar.Name = "bttnSalvar";
            this.bttnSalvar.Size = new System.Drawing.Size(97, 35);
            this.bttnSalvar.TabIndex = 10;
            this.bttnSalvar.Text = "Salvar";
            this.bttnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnSalvar.UseVisualStyleBackColor = true;
            this.bttnSalvar.Click += new System.EventHandler(this.bttnSalvar_Click);
            // 
            // bttnNovo
            // 
            this.bttnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnNovo.Image = global::SATS.Properties.Resources.Crystal_Clear_action_edit_add;
            this.bttnNovo.Location = new System.Drawing.Point(143, 545);
            this.bttnNovo.Name = "bttnNovo";
            this.bttnNovo.Size = new System.Drawing.Size(97, 35);
            this.bttnNovo.TabIndex = 13;
            this.bttnNovo.Text = "Novo";
            this.bttnNovo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnNovo.UseVisualStyleBackColor = true;
            this.bttnNovo.Click += new System.EventHandler(this.bttnNovo_Click);
            // 
            // bttnEditar
            // 
            this.bttnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnEditar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_editpaste;
            this.bttnEditar.Location = new System.Drawing.Point(453, 19);
            this.bttnEditar.Name = "bttnEditar";
            this.bttnEditar.Size = new System.Drawing.Size(116, 39);
            this.bttnEditar.TabIndex = 2;
            this.bttnEditar.Text = "Editar";
            this.bttnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnEditar.UseVisualStyleBackColor = true;
            this.bttnEditar.Click += new System.EventHandler(this.bttnEditar_Click);
            // 
            // ProgramacaoSetPoint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(625, 590);
            this.Controls.Add(this.txtGanhoProp);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtFreqMax);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.groupBoxSemana);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBpressao);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFreqMin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSetPoint);
            this.Controls.Add(this.groupBoxHorario);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bttnCancelar);
            this.Controls.Add(this.bttnSalvar);
            this.Controls.Add(this.bttnNovo);
            this.Controls.Add(this.checkBox);
            this.Controls.Add(this.bttnEditar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cBbomba);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "ProgramacaoSetPoint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Programação SetPoint de Funcionamento";
            this.Load += new System.EventHandler(this.programaligamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemporizacao)).EndInit();
            this.groupBoxSemana.ResumeLayout(false);
            this.groupBoxSemana.PerformLayout();
            this.groupBoxHorario.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSetPoint;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bttnCancelar;
        private System.Windows.Forms.Button bttnSalvar;
        private System.Windows.Forms.Button bttnNovo;
        private System.Windows.Forms.CheckBox checkBox;
        private System.Windows.Forms.Button bttnEditar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cBbomba;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFreqMin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cBpressao;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFreqMax;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvTemporizacao;
        private System.Windows.Forms.TextBox txtGanhoProp;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textSun;
        private System.Windows.Forms.TextBox textMon;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textTue;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textFri;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textThu;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textWed;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textSat;
        private System.Windows.Forms.GroupBox groupBoxSemana;
        private System.Windows.Forms.GroupBox groupBoxHorario;
        private System.Windows.Forms.Button btnCadHSetPoint;
        private System.Windows.Forms.Button btnAtualizarH;
        private System.Windows.Forms.Button btnDelete;
    }
}