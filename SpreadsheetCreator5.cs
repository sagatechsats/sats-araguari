﻿using ExcelLibrary.SpreadSheet;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Para XLSX utilizamos a biblioteca EPPlus(using OfficeOpenXml). Para XLS, 
// Excel Library.(Lembrar de adicionar refencias)

namespace SATS
{
    class SpreadsheetCreator5
    {
        private ListView listView;
        public SpreadsheetCreator5(ListView listViewfromRelatorio)
        {
            listView = listViewfromRelatorio;
        }

        // Estou retornando o tipo SaveFileDialog porque eu preciso do path e do
        // filterIndex escolhido.
        private SaveFileDialog SaveDialog()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Excel 2007/2010/2013 (*.xlsx)|*.xlsx";
                                     //"Todos os arquivos (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                return saveFileDialog1;
            else
                return null;
        }

        public void CreateFile(string nameSpread, string tipoGasto, string dataReferencia, string valor)
        {
            SaveFileDialog saveFileStruct = SaveDialog();
            if (saveFileStruct == null)
                return;
            
            if(saveFileStruct.FilterIndex == 1)
                CreateXLSLFile(nameSpread, tipoGasto, dataReferencia, valor, saveFileStruct.FileName);
            //else if(saveFileStruct.FilterIndex == 2)
               // CreateXLSFile (nameSpread, cliente, dataInicial, dataFinal, saveFileStruct.FileName);
        }

        private void CreateXLSFile(string nameSpread, string cliente, string dataInicial,
            string dataFinal, string path)
        {
            Workbook workbook = new Workbook();
            Worksheet ws = new Worksheet(nameSpread);
            ws.Name = nameSpread;
            ws.Cells[0, 0] = new Cell(cliente);
            ws.Cells[0, 1] = new Cell("Início: " + dataInicial);
            ws.Cells[0, 2] = new Cell("Término: " + dataFinal);

            for (var i = 0; i < listView.Columns.Count; ++i)
            {
                ws.Cells[2, i] = new Cell(listView.Columns[i].Text);
                ws.Cells[2, i].Style = new CellStyle();
                // Não funciona, essa biblioteca e limitada em recursos de formatacao.
                ws.Cells[2, i].Style.BackColor = System.Drawing.Color.FromArgb(128, 128, 128);
            }

            ws.Cells.ColumnWidth[0] = 7000;
            ws.Cells.ColumnWidth[1] = 11000;
            ws.Cells.ColumnWidth[2] = 7000;

            for (var i = 0; i < listView.Items.Count; ++i)
            {
                for (var j = 0; j < listView.Items[i].SubItems.Count; ++j)
                {
                    ws.Cells[i + 3, j] = new Cell(listView.Items[i].SubItems[j].Text);
                }
            }

            workbook.Worksheets.Add(ws);
            workbook.Save(path);
        }

        private void CreateXLSLFile(string nameSpread, string gastos, string dataReferencia, string valor, string path)
        {
            FileInfo file = new FileInfo(path);
            //FileInfo file = new FileInfo(@"C:\Users\amador\Desktop\test.xlsx");
            using (ExcelPackage pck = new ExcelPackage())
            {
                var ws = pck.Workbook.Worksheets.Add(nameSpread);
                ws.Name = nameSpread;
                ws.Cells.Style.Font.Size = 11;

                ws.Cells[1, 1].Value = "Tipo de Gasto: "+gastos.ToString();
                ws.Cells[1, 2].Value = "Data de referência: " + dataReferencia;
                ws.Cells[1, 3].Value = "Preço unitário: ";
                 ws.Cells[1, 4].Value =Convert.ToDecimal( valor);

                for (var i = 0; i < listView.Columns.Count; ++i)
                    ws.Cells[2, i + 1].Value = listView.Columns[i].Text;

                ws.Cells["A1:D1"].Style.Font.Italic = true;
                ws.Cells["A1:D1"].Style.Font.Bold = true;
                ws.Cells["A2:G2"].Style.Font.Bold = true;
                ws.Column(1).Width = 38;
                ws.Column(2).Width = 39;
                ws.Column(3).Width = 20;
                ws.Column(4).Width = 28;
                ws.Column(5).Width = 20;
                ws.Column(6).Width = 23;
                ws.Column(7).Width = 26;

                decimal soma=0;
                int contador = 3;
                for ( var i= 0; i < listView.Items.Count; ++i)
                {
                    contador++;
                    for (var j = 0; j < listView.Items[i].SubItems.Count; ++j)
                    {
                        

                        if (j > 1)
                        {
                            ws.Cells[i + 3, j + 1].Value = Convert.ToDecimal(listView.Items[i].SubItems[j].Text);
                            if(j==3) soma+=Convert.ToDecimal(listView.Items[i].SubItems[j].Text);
                        }
                        else ws.Cells[i + 3, j + 1].Value = listView.Items[i].SubItems[j].Text;
                    }
                }
                ws.Cells[contador, 3].Value ="Total Gasto (R$):";
                ws.Cells[contador, 4].Value =Convert.ToDecimal( soma);
                ws.Cells["C"+contador+":D"+contador].Style.Font.Bold = true;
                // Salva o arquivo.
                Byte[] bin = pck.GetAsByteArray();
                try
                {
                    File.WriteAllBytes(path, bin);
                }
                catch (Exception)
                {
                    MessageBox.Show("Não é possível salvar com o nome selecionado, existe uma planilha de mesmo nome aberta.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
