﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public partial class FrmInformacao: Office2007Form
    {

        protected DBManager dbManager;
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private int index = 0;

        public FrmInformacao(int index,string ponto)
        {
            InitializeComponent();
            this.index = index;
            //lblPonto.Text = ponto;
        }

        public static Boolean IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void carregar()
        {
            if (IsConnected() == true)
            {
                
                try
                {
                    dbManager = new DBManager(System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector"));
                    //List<string[]> result = dbManager.Select("ponto", "*", "id_ponto='" + index + "'");
                    List<string[]> result = dbManager.SelectInnerJoin2("ponto_informacao", "*","ponto" , "ponto.id_ponto = ponto_informacao.id_ponto","ponto_informacao.id_ponto = '" + index + "'");

                    if (result.Count == 0)
                    {
                        MessageBox.Show("Dados não cadastrados! Favor inserir as informações do ponto, em cadastro de ponto.", "Informação!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Visible = false;
                        this.Hide();

                        this.Close();
                        return;
                    }
                    else
                    {
                        Invoke((MethodInvoker)(() => lblPonto.Text = result[0][5]));
                        Invoke((MethodInvoker)(() => textBox1.Text = result[0][2]));                                                                     
                    }


                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();

            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

       

        private void FrmInformacao_Load(object sender, EventArgs e)
        {
            carregar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
    }
}
