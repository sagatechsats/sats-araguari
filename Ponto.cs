﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;


namespace SATS
{

    public partial class Ponto : UserControl
    {
        int X, Y;
        public string _nameBD { get; set; } 
        public string _idBD { get; set; }
        public string idPonto;
        public string name;
        private string label2StartText;
        public bool isLastAlarmeEvento { get; set; }
        public string bomba_ligada { get; set; }
        public string tipo_ponto { get; set; }
        protected DBManager DatabaseManager;
        protected DBApplication DatabaseAppManager;

        public static string permi = "";
        // O DBManager que vem do form deve estar devidamente iniciado;
        public Ponto()
        {
            if (Screen.AllScreens.Length > 1)
            {
                Rectangle workingArea = Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();


            //permi = "Supervisor";
            if (permi.CompareTo("Supervisor") == 0)
            {
                this.MouseDown += new MouseEventHandler(Ponto_MouseDown);
                this.MouseMove += new MouseEventHandler(Ponto_MouseMove);
            }
            //GetIDFromDB();
        }

        public void Start(string nameBD, DBApplication AppDBManager)
        {
            DatabaseAppManager = AppDBManager;
            _nameBD = nameBD;
            label2StartText = label2.Text;
            SetValor("Sem dados", "");
            isLastAlarmeEvento = false;
        }

        public void SetName(string name2)
        {
            this.label1.Text = name2;
            name = name2;
        }

        public void SetValor(string valorRef, string datahora)
        {
            decimal stempo = 0;

            DateTime datanow = DateTime.Now;
            DateTime data = DateTime.Now;
            try
            {
                data = Convert.ToDateTime(datahora);
            }
            catch (Exception)
            {
            }

            TimeSpan ts = datanow.Subtract(data);
            stempo = Convert.ToDecimal(ts.TotalMinutes);

            try
            {
                label2.Text = valorRef;
            }
            catch (Exception)
            {

            }
            if (stempo > 60)
            {
                ChangeStatusImage(YELLOW_CIRCLE);
            }
            else
            {
                if (valorRef == "Nível: 0.00 m.c.a" ||
                    valorRef == "Nível: 0,00 m.c.a" ||
                    valorRef == "Nível: 0.00 m.ca - NÍVEL ALTO" ||
                    valorRef == "Nível: 0.00 m.ca - NÍVEL BAIXO" ||
                    valorRef == "Nível: 0,00" ||
                    valorRef == "Nível: 0.00" ||
                    valorRef == "Bomba: Desligada" ||
                    valorRef == "Bomba: Desligada/Automático" ||
                    valorRef == "Bomba: Desligada/Manual" ||
                    valorRef == "Velocidade (RPM): 0" ||
                    valorRef == "Pressão (Bar): 0,00" ||
                    valorRef == "Pressão (Bar): 0.00" ||
                    valorRef == "Pressão (bar): 0,00" ||
                    valorRef == "Pressão (bar): 0.00" ||
                    valorRef == "Pressão: 0.00 (Bar)" ||
                    valorRef == "Pressão: 0.00 (bar)" ||
                    valorRef == "Vazão (m³/H): 0,00" ||
                    valorRef == "Vazão (m³/h): 0,00" ||
                    valorRef == "Vazão (m3/h): 0,00" ||
                    valorRef == "Vazão (m3/H): 0.00" ||
                    valorRef == "Ph: 0.00" ||
                    valorRef == "Ph: 0,00" ||
                    valorRef == "PH: 0.00" ||
                    valorRef == "pH: 0,00" ||
                    valorRef == "pH: 0.00" ||
                    valorRef == "ph: 0,00" ||
                    valorRef == "ph: 0.00" ||
                    valorRef == "PH: 0,00" ||
                    valorRef == "Corrente: 0.00" ||
                    valorRef == "Corrente: 0,00" ||
                    valorRef == "Turbidez: 0.00 (NTU)" ||
                    valorRef == "Turbidez: 0,00" ||
                    valorRef == "turbidez: 0.00" ||
                    valorRef == "turbidez: 0,00" ||
                    valorRef == "Inversor: 0.00 (KHz)" ||
                    valorRef == "Inversor: 0,00 (KHz)" ||
                    valorRef == "inversor: 0.00 (KHz)" ||
                    valorRef == "inversor: 0,00 (KHz)")
                {
                    ChangeStatusImage(RED_CIRCLE);
                }
                else if (valorRef == "Sem dados")
                    ChangeStatusImage(GREY_CIRCLE);
                else
                {
                    ChangeStatusImage(BLUE_CIRCLE);
                }
            }
        }

        public void ChangeStatusImage(Image image)
        {
            this.pictureBox1.Image = image;
        }

        public void NivelCritico(int status)
        {
            if (status == 0)
                this.pictureBox1.BackColor = Color.Red;
            else if (status == 1)
                this.pictureBox1.BackColor = Color.Yellow;
            else
                this.pictureBox1.BackColor = Color.Transparent;
        }
        public void AlertaAr(int status)
        {
            if (status == 1)
                this.pictureBox1.BackColor = Color.Orange;
            else
                this.pictureBox1.BackColor = Color.Transparent;
        }

        public void Carrega()
        {
            //Invoke((MethodInvoker)(() => Application.UseWaitCursor = true));  
            switch (tipo_ponto)
            {

                case "0":
                          Invoke((MethodInvoker)(() => CarregaNivel()));
                break;

                case "1":
                //case "9":
                         Invoke((MethodInvoker)(() => CarregaBombas()));                    
                break;

                case "2":
                case "5":
                    Invoke((MethodInvoker)(() => CarregaVazao()));
                    break;

                case "7":
                    Invoke((MethodInvoker)(() => CarregaPressao()));
                    break;

                case "8":
                    Invoke((MethodInvoker)(() => CarregaInversor()));
                    break;


            }

            

            #region
            //string[] rsLeituraVazao = DatabaseAppManager.SelectMostRecentLeitura("ponto_leitura.id_ponto" + " = " + _idBD,
            //    "ponto_leitura INNER JOIN ponto ON ponto.id_ponto = ponto_leitura.id_ponto", "*");

            //string[] rsLeituraPh = DatabaseAppManager.SelectMostRecentLeitura("ponto_ph.id_ponto" + " = " + _idBD,
            //    "ponto_ph INNER JOIN ponto ON ponto.id_ponto = ponto_ph.id_ponto", "*");

            //string[] rsLeituraEnergia = DatabaseAppManager.SelectMostRecentLeitura("ponto_energia.id_ponto" + " = " + _idBD,
            //    "ponto_energia INNER JOIN ponto ON ponto.id_ponto = ponto_energia.id_ponto", "*");

            //string[] rsLeituraTurbidez = DatabaseAppManager.SelectMostRecentLeitura("ponto_turbidez.id_ponto" + " = " + _idBD,
            //    "ponto_turbidez INNER JOIN ponto ON ponto.id_ponto = ponto_turbidez.id_ponto", "*");



            //string[] rsLeituraInversor = DatabaseAppManager.SelectMostRecentLeitura("ponto_inversor.id_ponto" + " = " + _idBD,
            //     "ponto_inversor INNER JOIN ponto ON ponto.id_ponto = ponto_inversor.id_ponto", "*");

            //if (rsLeituraVazao != null)
            //{
            //    try
            //    {
            //        //SetValor(rsLeituraVazao[3],rsLeituraVazao[4]);
            //        string mensagem = "Informações última leitura (" + rsLeituraVazao[4] + "):\n\nVolume (m³): " +
            //        rsLeituraVazao[2] + "\nVazão (m³/H): " + rsLeituraVazao[3] + "\n\nÚltimo Alarme ou Evento: " +
            //        rsAlarmeEvento[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
            //        //(      string ponto,                          int bomba,    string alarmeE,  string descricao,     string horaAE,      int identificador,                      int tipodados,   string nivel_max,      string status)
            //        Invoke((MethodInvoker)(() => TelaVazao.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraVazao[9]), rsAlarmeEvento[0], rsAlarmeEvento[3], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraVazao[8]), rsLeituraVazao[14], rsLeituraVazao[15])));
            //    }
            //    catch (Exception)
            //    {

            //    }
            //    return;
            //}

            //if (rsLeituraPh != null)
            //{
            //    try
            //    {
            //        //SetValor(rsLeituraNivel[3],rsLeituraNivel[4]);
            //        string mensagem = "Informações última leitura (" + rsLeituraPh[4] + "):\n\nPh: " +
            //        rsLeituraPh[2] + "\nPh: " + rsLeituraPh[3] + "\n\nÚltimo Alarme ou Evento: " +
            //        rsAlarmeEvento[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
            //        //(      string ponto,                      int bomba,    string alarmeE,  string descricao,     string horaAE,      int identificador,                   int tipodados,string nivel_max,    string rede)
            //        Invoke((MethodInvoker)(() => TelaPh.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraPh[9]), rsAlarmeEvento[0], rsAlarmeEvento[3], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraPh[8]), rsLeituraPh[14], rsLeituraPh[4])));
            //    }

            //    catch (Exception)
            //    {

            //    }
            //    return;
            //}

            //if (rsLeituraEnergia != null)
            //{
            //    try
            //    {
            //        //SetValor(rsLeituraNivel[3],rsLeituraNivel[4]);
            //        string mensagem = "Informações última leitura (" + rsLeituraEnergia[4] + "):\n\nCorrente: " +
            //        rsLeituraEnergia[2] + "\nCorrente: " + rsLeituraEnergia[3] + "\n\nÚltimo Alarme ou Evento: " +
            //        rsAlarmeEvento[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
            //        //(      string ponto,                            int bomba,    string alarmeE,  string descricao,     string horaAE,      int identificador,                      int tipodados,   string nivel_max,      string status)
            //        Invoke((MethodInvoker)(() => TelaEnergia.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraEnergia[13]), rsAlarmeEvento[0], rsAlarmeEvento[3], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraEnergia[12]), rsLeituraEnergia[16], rsLeituraEnergia[19])));
            //    }

            //    catch (Exception)
            //    {

            //    }
            //    return;
            //}

            //if (rsLeituraTurbidez != null)
            //{
            //    try
            //    {
            //        string mensagem = "Informações última leitura (" + rsLeituraTurbidez[4] + "):\n\nTurbidez: " +
            //        rsLeituraTurbidez[2] + "\nTurbidez: " + rsLeituraTurbidez[3] + "\n\nÚltimo Alarme ou Evento: " +
            //        rsLeituraTurbidez[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
            //        //                                  (      string ponto,                        int bomba,        string alarmeE,     string descricao,     string horaAE,      int identificador,                   int tipodados,           string nivel_max,          string status)
            //        Invoke((MethodInvoker)(() => TelaTurbidez.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraTurbidez[8]), rsAlarmeEvento[0], rsAlarmeEvento[3], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraTurbidez[7]), rsLeituraTurbidez[13], rsLeituraTurbidez[14])));
            //    }

            //    catch (Exception)
            //    {

            //    }
            //    return;
            //}

            //if (rsLeituraPressao != null)
            //{
            //    try
            //    {
            //        string mensagem = "Informações última leitura (" + rsLeituraPressao[4] + "):\n\nPressão: " +
            //        rsLeituraPressao[2] + "\nPressão: " + rsLeituraPressao[3] + "\n\nÚltimo Alarme ou Evento: " +
            //        rsLeituraPressao[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
            //        //                                  (      string ponto,                        int bomba,        string alarmeE,     string descricao,     string horaAE,      int identificador,                   int tipodados,           string nivel_max,          string status)
            //        Invoke((MethodInvoker)(() => TelaPressao.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraPressao[8]), rsAlarmeEvento[0], rsAlarmeEvento[3], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraPressao[7]), rsLeituraPressao[13], rsLeituraPressao[14])));
            //    }

            //    catch (Exception)
            //    {

            //    }
            //    return;
            //}

            //if (rsLeituraInversor != null)
            //{
            //    try
            //    {
            //        string mensagem = "Informações última leitura (" + rsLeituraInversor[4] + "):\n\nPressão: " +
            //        rsLeituraInversor[2] + "\nInversor: " + rsLeituraInversor[3] + "\n\nÚltimo Alarme ou Evento: " +
            //        rsLeituraInversor[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
            //        //                                  (      string ponto,                        int bomba,        string alarmeE,     string descricao,     string horaAE,      int identificador,                   int tipodados,           string nivel_max,          string status)
            //        Invoke((MethodInvoker)(() => TelaInversor.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraInversor[8]), rsAlarmeEvento[0], rsAlarmeEvento[3], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraInversor[7]), rsLeituraInversor[13], rsLeituraInversor[14])));
            //    }

            //    catch (Exception)
            //    {

            //    }
            //    return;
            //}

            #endregion


        }

        private void CarregaVazao()
        {
            Application.UseWaitCursor = true;

            string[] rsAlarmeEvento = { "Sem Eventos", "s/evento", "Nome Ponto", "Não houve nenhum evento para este ponto" };

            if (DatabaseAppManager.SelectPontosMostRecentAlarmeEvento(_nameBD) != null)
            {
                rsAlarmeEvento = DatabaseAppManager.SelectPontosMostRecentAlarmeEvento(_nameBD);
            }

            string[] rsLeituraVazao = DatabaseAppManager.SelectMostRecentLeitura("ponto_leitura.id_ponto" + " = " + _idBD,
                "ponto_leitura INNER JOIN ponto ON ponto.id_ponto = ponto_leitura.id_ponto", "*");

            if (rsLeituraVazao != null)
            {
                try
                {
                    //SetValor(rsLeituraVazao[3],rsLeituraVazao[4]);
                    string mensagem = "Informações última leitura (" + rsLeituraVazao[4] + "):\n\nVolume (m³): " +
                    rsLeituraVazao[2] + "\nVazão (m³/H): " + rsLeituraVazao[3] + "\n\nÚltimo Alarme ou Evento: " +
                    rsAlarmeEvento[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
                    //(      string ponto,                          int bomba,    string alarmeE,  string descricao,     string horaAE,      int identificador,                      int tipodados,   string nivel_max,      string status)
                    Application.UseWaitCursor = false;
                    Invoke((MethodInvoker)(() => TelaVazao.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraVazao[9]), rsAlarmeEvento[0], rsAlarmeEvento[4], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraVazao[8]), rsLeituraVazao[14], rsLeituraVazao[15])));
                }
                catch (Exception)
                {

                }
                return;
            }
        }

        private void CarregaNivel()
        {
            Application.UseWaitCursor = true;

            string[] rsAlarmeEvento = { "Sem Eventos", "s/evento", "Nome Ponto", "Não houve nenhum evento para este ponto" };

            if (DatabaseAppManager.SelectPontosMostRecentAlarmeEvento(_nameBD) != null)
            {
                rsAlarmeEvento = DatabaseAppManager.SelectPontosMostRecentAlarmeEvento(_nameBD);
            }

            string[] rsLeituraNivel = DatabaseAppManager.SelectMostRecentLeitura("ponto_nivel.id_ponto" + " = " + _idBD,
                                "ponto_nivel INNER JOIN ponto ON ponto.id_ponto = ponto_nivel.id_ponto", "*");

            if (rsLeituraNivel != null)
            {
                Application.UseWaitCursor = true;

                try
                {
                    //SetValor(rsLeituraNivel[3],rsLeituraNivel[4]);
                    string mensagem = "Informações última leitura (" + rsLeituraNivel[4] + "):\n\nNível (m.c.a): " +
                    rsLeituraNivel[2] + "\nNível (m.c.a): " + rsLeituraNivel[3] + "\n\nÚltimo Alarme ou Evento: " +
                    rsAlarmeEvento[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
                    //(      string ponto,                          int bomba,    string alarmeE,  string descricao,     string horaAE,      int identificador,                      int tipodados,   string nivel_max,      string status)
                    Application.UseWaitCursor = false;
                    Invoke((MethodInvoker)(() => TelaNivel.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraNivel[8]), rsAlarmeEvento[0], rsAlarmeEvento[4], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraNivel[7]), rsLeituraNivel[13], rsLeituraNivel[14])));
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return;
            }

        }

        private void CarregaBombas()
        {
            Application.UseWaitCursor = true;

            string[] rsAlarmeEvento = { "Sem Eventos", "s/evento", "Nome Ponto", "Não houve nenhum evento para este ponto" };
            string[] rsAlarmeEventoAux = DatabaseAppManager.SelectPontosMostRecentAlarmeEvento(_nameBD);


            if (rsAlarmeEventoAux != null)
            {
                rsAlarmeEvento = rsAlarmeEventoAux;
            }

            string[] rsLeitura2 = DatabaseAppManager.SelectMostRecentLeitura("ponto_aciona.id_ponto" + " = " + _idBD,
                                   "ponto_aciona INNER JOIN ponto ON ponto.id_ponto = ponto_aciona.id_ponto", "*");


            if (rsLeitura2 != null)
            {
                try
                {
                    Application.UseWaitCursor = false;
                    //SetValor("Pressão (bar):" + " " + rsLeitura2[2], rsLeitura2[3]);
                    Invoke((MethodInvoker)(() => TelaBomba.show(_nameBD.ToString(), Convert.ToInt32(rsLeitura2[9 + 2]), rsAlarmeEvento[0], rsAlarmeEvento[4], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeitura2[8 + 2]), rsLeitura2[14 + 2], rsLeitura2[15 + 2])));
                }

                catch (Exception)
                {
                    MessageBox.Show("Não há informações para este ponto.", "Sem informações para o ponto",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return;
            }
            else
            {
                MessageBox.Show("Não há leituras para este ponto.", "Sem leituras para o ponto",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void CarregaPressao()
        {
            Application.UseWaitCursor = true;
            string[] rsAlarmeEvento = { "Sem Eventos", "s/evento", "Nome Ponto", "Não houve nenhum evento para este ponto" };

            if (DatabaseAppManager.SelectPontosMostRecentAlarmeEvento(_nameBD) != null)
            {
                rsAlarmeEvento = DatabaseAppManager.SelectPontosMostRecentAlarmeEvento(_nameBD);
            }

            string[] rsLeituraPressao = DatabaseAppManager.SelectMostRecentLeituraPressao("ponto_pressao.id_ponto" + " = " + _idBD,
                    "ponto_pressao INNER JOIN ponto ON ponto.id_ponto = ponto_pressao.id_ponto", "*");


            if (rsLeituraPressao != null)
            {
                try
                {
                    string mensagem = "Informações última leitura (" + rsLeituraPressao[4] + "):\n\nPressão: " +
                    rsLeituraPressao[2] + "\nPressão: " + rsLeituraPressao[3] + "\n\nÚltimo Alarme ou Evento: " +
                    rsLeituraPressao[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
                    //                                  (      string ponto,                        int bomba,        string alarmeE,     string descricao,     string horaAE,      int identificador,                   int tipodados,           string nivel_max,          string status)
                    Application.UseWaitCursor = false;
                    Invoke((MethodInvoker)(() => TelaPressao.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraPressao[8]), rsAlarmeEvento[0], rsAlarmeEvento[4], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraPressao[7]), rsLeituraPressao[13], rsLeituraPressao[14])));
                }

                catch (Exception)
                {

                }
                return;
            }
        }


        private void CarregaInversor()
        {
            Application.UseWaitCursor = true;
            string[] rsAlarmeEvento = { "Sem Eventos", "s/evento", "Nome Ponto", "Não houve nenhum evento para este ponto" };
            string[] rsAlarmeEventoAux = DatabaseAppManager.SelectPontosMostRecentAlarmeEvento(_nameBD);

            if (rsAlarmeEventoAux != null)
            {
                rsAlarmeEvento = rsAlarmeEventoAux;
            }

            string[] rsLeituraInversor = DatabaseAppManager.SelectMostRecentLeitura("ponto_inversor.id_ponto" + " = " + _idBD,
                 "ponto_inversor INNER JOIN ponto ON ponto.id_ponto = ponto_inversor.id_ponto", "*");


            if (rsLeituraInversor != null)
            {
                try
                {
                    string mensagem = "Informações última leitura (" + rsLeituraInversor[4] + "):\n\nPressão: " +
                    rsLeituraInversor[2] + "\nInversor: " + rsLeituraInversor[3] + "\n\nÚltimo Alarme ou Evento: " +
                    rsLeituraInversor[0] + "\nDescrição: " + rsAlarmeEvento[3] + "\nHora: " + rsAlarmeEvento[1];
                    //                                  (      string ponto,                        int bomba,        string alarmeE,     string descricao,     string horaAE,      int identificador,                   int tipodados,           string nivel_max,          string status)
                    Invoke((MethodInvoker)(() => TelaInversor.show(_nameBD.ToString(), Convert.ToInt32(rsLeituraInversor[8]), rsAlarmeEvento[0], rsAlarmeEvento[4], rsAlarmeEvento[1], Convert.ToInt32(_idBD), Convert.ToInt32(rsLeituraInversor[7]), rsLeituraInversor[13], rsLeituraInversor[14])));
                }

                catch (Exception)
                {

                }
                return;
            }
        }


        private void PictureClickActions()
        // Primeiro verifica se tem acesso a internet. Depois verifica se tem acesso ao BD. 
        // Depois verifica se tem Dados no BD pro ponto. Depois busca as informacoes.
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();
            if (_idBD == null)
            {
                MessageBox.Show("Não há informações no banco de dados para este ponto. " +
                    "Entre em contato com a SAGA Medição.", "Sem informações no Banco de Dados",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Application.UseWaitCursor = false;
                Application.DoEvents();
                return;
            }
            Application.UseWaitCursor = true;
            Thread novaThread = new Thread(new ThreadStart(Carrega));
            novaThread.Start();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //PictureClickActions();
        }

        private void Ponto_Load(object sender, EventArgs e)
        {
            //this.Parent = pictureBox1;
            //this.BackColor = Color.Transparent;
        }

        private void Ponto_Click(object sender, EventArgs e)
        {
            //PictureClickActions();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //PictureClickActions();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            //PictureClickActions();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            pictureBox1.Size = new Size(26, 26);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            pictureBox1.Size = new Size(24, 24);
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            pictureBox1.Size = new Size(26, 26);
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            pictureBox1.Size = new Size(24, 24);
        }

        private void label2_MouseEnter(object sender, EventArgs e)
        {
            // pictureBox1.Size = new Size(26, 26);
        }

        private void label2_MouseLeave(object sender, EventArgs e)
        {
            // pictureBox1.Size = new Size(24, 24);
        }

        private void Ponto_MouseEnter(object sender, EventArgs e)
        {
            //pictureBox1.Size = new Size(24, 24);
        }

        private void Ponto_MouseLeave(object sender, EventArgs e)
        {
            //pictureBox1.Size = new Size(21, 21);
        }

        private void Ponto_MouseDown(object sender, MouseEventArgs e)
        {
            if (permi.CompareTo("Supervisor") == 0)
            {
                if (e.Button != MouseButtons.Left) return;
                X = this.Left - MousePosition.X;
                Y = this.Top - MousePosition.Y;
            }
        }

        private void Ponto_MouseMove(object sender, MouseEventArgs e)
        {
            if (permi.CompareTo("Supervisor") == 0)
            {
                if (e.Button != MouseButtons.Left) return;
                this.Left = X + MousePosition.X;
                this.Top = Y + MousePosition.Y;
            }
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            if (permi.CompareTo("Supervisor") == 0)
            {
                if (e.Button != MouseButtons.Left) return;
                X = this.Left - MousePosition.X;
                Y = this.Top - MousePosition.Y;
            }
        }

        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            if (permi.CompareTo("Supervisor") == 0)
            {
                if (e.Button != MouseButtons.Left) return;
                this.Left = X + MousePosition.X;
                this.Top = Y + MousePosition.Y;
            }
        }

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            if (permi.CompareTo("Supervisor") == 0)
            {
                if (e.Button != MouseButtons.Left) return;
                X = this.Left - MousePosition.X;
                Y = this.Top - MousePosition.Y;
            }
        }

        private void label2_MouseMove(object sender, MouseEventArgs e)
        {
            if (permi.CompareTo("Supervisor") == 0)
            {
                if (e.Button != MouseButtons.Left) return;
                this.Left = X + MousePosition.X;
                this.Top = Y + MousePosition.Y;
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (permi.CompareTo("Supervisor") == 0)
            {
                if (e.Button != MouseButtons.Left) return;
                X = this.Left - MousePosition.X;
                Y = this.Top - MousePosition.Y;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (permi.CompareTo("Supervisor") == 0)
            {
                if (e.Button != MouseButtons.Left) return;
                this.Left = X + MousePosition.X;
                this.Top = Y + MousePosition.Y;
            }
        }

        private void Ponto_DoubleClick(object sender, EventArgs e)
        {

        }

        private void label1_DoubleClick(object sender, EventArgs e)
        {
            PictureClickActions();
        }

        private void label2_DoubleClick(object sender, EventArgs e)
        {
            PictureClickActions();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            PictureClickActions();
        }
    }
}
