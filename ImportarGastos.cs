﻿using System;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class ImportarGastos : Office2007Form
    {
        public ImportarGastos()
        {
            InitializeComponent();
        }

        private void importarGastos_Load(object sender, EventArgs e)
        {
            dataGasto.Format = DateTimePickerFormat.Custom;
            dataGasto.CustomFormat = "MM/yyyy";
            dataGasto.Value = DateTime.Now;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (descricaoGasto.Text == "" || valorGasto.Text == "")
            {
                MessageBox.Show("Favor preencher todos os campos.");
            }
            else
            {
                String aux = rEnergia.Text;
                if (rEnergia.Checked)
                {
                    aux = rEnergia.Text;
                }
                else if (rPQuimicos.Checked)
                {
                    aux = rPQuimicos.Text;
                }
                else if (rOutros.Checked)
                {
                    aux = rOutros.Text;
                }
                decimal valor = StringToDecimal(ReturnDecimalWithDot(valorGasto.Text.ToString()));
                gasto Gasto = new gasto(aux, descricaoGasto.Text, dataGasto.Value, valor);
                bdGasto trabGasto = new bdGasto();

                trabGasto.setGasto(Gasto);
                trabGasto.inserir();
                this.Close();
            }
        }

        public string ReturnDecimalWithDot(string commaDecimal)
        {
            string newDecimal = commaDecimal.ToString();
            newDecimal = newDecimal.Replace(".", ",");
            return newDecimal.ToString();
        }

        private float StringToFloat(string value)
        {
            float temp = (float)Convert.ToDouble(value);
            return temp;
        }

        private decimal StringToDecimal(string value)
        {
            try
            {
                decimal temp = Convert.ToDecimal(value);
                return Decimal.Round(temp, 2);
            }
            catch (Exception)
            {
                return 0;
            }
        }

    }
}
