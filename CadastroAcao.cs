﻿using System;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace SATS
{
    public partial class CadastroAcao : Office2007Form
    {
        public CadastroAcao()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void cadastroAcao_Load(object sender, EventArgs e)
        {
            dataAcao.Value = DateTime.Now;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (nomeAcao.Text == "" || descricaoAcao.Text == "")
            {
                MessageBox.Show("Favor preencher todos os campos.");
            }
            else
            {
                acao Acao = new acao(nomeAcao.Text, dataAcao.Value, descricaoAcao.Text);
                bdAcao trabAcao = new bdAcao();

                trabAcao.setAcao(Acao);
                trabAcao.inserir();
                this.Close();
            }
        }
    }
}
