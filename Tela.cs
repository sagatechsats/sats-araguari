﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;

namespace SATS
{
    public partial class Tela : Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private int X = 0;
        private int Y = 0;
        private int ID = 0;
        private string nome = "";
        private bool acionado;
        private bool acionadoAutomatico;
        private int acionamento = 2;
        private string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
        private Thread Threadupdate;
        private int TipoDados;
        private float NivelMax;
        private String Status;

        //private ReservatorioLiquidos.TanqueEscala tanque2;

        public bool verifica(int id)
        {
            ID = id;
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> Acionamento = DatabaseManager.Select("ponto_aciona", "acionar, porta, data_hora, status_acionar", "id_ponto= '" + id + "'");
                List<string[]> Automatico = DatabaseManager.Select("ponto_automatico", "habilitado ", "id_bomba= '" + id + "'");

                if(Automatico[0][0] != null)
                    Invoke((MethodInvoker)(() => AUTOMATICO(Convert.ToBoolean(Automatico[0][0]))));

                if (Acionamento.Count > 0 && (acionamento == Convert.ToInt32(Acionamento[0][3]) || acionamento == 2))
                {
                    Invoke((MethodInvoker)(() => LEDS(Convert.ToInt32(Acionamento[0][3]))));
                  //  Invoke((MethodInvoker)(() => AUTOMATICO(Convert.ToInt32(Automatico[0][0]))));
                    acionamento = 2;
                    SetLed(float.Parse(Acionamento[0][3]), Acionamento[0][2]);
                    this.UseWaitCursor = false; //Cursor de espera do mouse false
                    Application.DoEvents();
                }

                if (TipoDados == 1)
                {    
                    if (Status.Length == 0)
                        labelHora.Text = "Última atulização: " + Acionamento[0][2];
                    else
                        labelHora.Text = "Última atulização: " + Acionamento[0][2] + "\r\n" + Status;

                    buttonAciona.Enabled = true;
                    btnAcionamentoAutomatico.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        public void AUTOMATICO(bool automatico)
        {

            if (automatico == false)
            {

                btnAcionamentoAutomatico.BackgroundImage = ((Image)(Properties.Resources.switchoff));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                Invoke((MethodInvoker)(() => btnAcionamentoAutomatico.Enabled = true));
                acionadoAutomatico = false;

            }
            else if ( automatico == true)
            {

                btnAcionamentoAutomatico.BackgroundImage = ((Image)(Properties.Resources.switchon));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                Invoke((MethodInvoker)(() => btnAcionamentoAutomatico.Enabled = true));
                acionadoAutomatico = true;

            }
            else
            {
                pictureBox2.BackgroundImage = ((Image)(Properties.Resources.ledgray));
                Invoke((MethodInvoker)(() => btnAcionamentoAutomatico.Enabled = false));
            }

        }
        

        public void LEDS(int led)
        {
            //button2.Visible = true;
            if (led == 0)
            {
                buttonAciona.BackgroundImage = ((Image)(Properties.Resources.switchoff));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                Invoke((MethodInvoker)(() => buttonAciona.Enabled = true));


                acionado = false;
            }
            else if (led == 1)
            {
                buttonAciona.BackgroundImage = ((Image)(Properties.Resources.switchon));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                Invoke((MethodInvoker)(() => buttonAciona.Enabled = true));
                acionado = true;
            }
            else
            {
                pictureBox2.BackgroundImage = ((Image)(Properties.Resources.ledgray));
                Invoke((MethodInvoker)(() => buttonAciona.Enabled = false));
            }
        }
        public void ligaAutomatico()
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                if (acionadoAutomatico == false)
                {
                    DatabaseManager.Update("ponto_automatico", "habilitado='1'", "id_bomba='" + ID + "'");
                    acionadoAutomatico = true;
                  //  acionamento = 1;
                }
                else
                {
                    DatabaseManager.Update("ponto_automatico", "habilitado='0'", "id_bomba='" + ID + "'");
                    acionadoAutomatico = false;
                //    acionamento = 0;
                }
            }
            catch (Exception)
            {
                try
                {
                    if (acionado == false)
                    {
                        btnAcionamentoAutomatico.BackgroundImage = Properties.Resources.switchoff;

                        Invoke((MethodInvoker)(() => btnAcionamentoAutomatico.Enabled = true));
                    }
                    else
                    {
                        btnAcionamentoAutomatico.BackgroundImage = ((Image)(Properties.Resources.switchon));
                        Invoke((MethodInvoker)(() => btnAcionamentoAutomatico.Enabled = true));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }
        public void ligabomba()
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                if (acionado == false)
                {
                    DatabaseManager.Update("ponto_aciona", "acionar='1'", "id_ponto='" + ID + "'");
                    acionado = true;
                    acionamento = 1;
                }
                else
                {
                    DatabaseManager.Update("ponto_aciona", "acionar='0'", "id_ponto='" + ID + "'");
                    acionado = false;
                    acionamento = 0;
                }
            }
            catch (Exception)
            {
                try
                {
                    if (acionado == false)
                    {
                        buttonAciona.BackgroundImage = Properties.Resources.switchoff;

                        Invoke((MethodInvoker)(() => buttonAciona.Enabled = true));
                    }
                    else
                    {
                        buttonAciona.BackgroundImage = ((Image)(Properties.Resources.switchon));
                        Invoke((MethodInvoker)(() => buttonAciona.Enabled = true));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public static bool IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public byte CHKSUM(byte[] receive)
        {
            byte CHk = 0;
            for (int i = 0; i < receive.Length; i++)
            {

                CHk = (byte)(receive[i] ^ CHk);

            }

            return CHk;

        }

        public void SetLed(float vazao, string datahora)
        {
            decimal stempo = 0;

            DateTime datanow = DateTime.Now;
            DateTime data = DateTime.Now;
            try
            {
                data = Convert.ToDateTime(datahora);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            TimeSpan ts = datanow.Subtract(data);
            stempo = Convert.ToDecimal(ts.TotalMinutes);


            if (vazao == 0)
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                Invoke((MethodInvoker)(() => pictureBox3.Visible = false));
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    Invoke((MethodInvoker)(() => pictureBox3.Visible = true));
                }
            }
            else
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                Invoke((MethodInvoker)(() => pictureBox3.Visible = false));
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    Invoke((MethodInvoker)(() => pictureBox3.Visible = true));
                }
            }
        }

        public Tela(int idp, string nomep, int bomba, int tipodados, float nivel_max, string status)
        {
            InitializeComponent();
            Status = status;
            NivelMax = nivel_max;

            DBManager DatabaseManager = new DBManager(stcon);

            Application.UseWaitCursor = true;
            Application.DoEvents();
            nome = nomep;
            ID = idp;
            TipoDados = tipodados;
            acionamento = 2;

            if (tipodados == 1)
            {
                pictureBox1.Location = new Point(245, -6);
                pictureBox1.Size = new Size(370, 334);
            }

            //if (tipodados == 4)
            //{
            //    pictureBox1.Location = new Point(245, -6);
            //    pictureBox1.Size = new Size(370, 334);
            //}

            //devia estar no desiner, mas n dá certo la
            this.tanque1.Tanque1.CorFluido = System.Drawing.Color.FromArgb(0, 111, 223);
            this.tanque1.Tanque1.CorVidro = System.Drawing.Color.Gray;
            this.tanque1.Tanque1.Value = 0;
            this.tanque1.Tanque1.MinVidro = 25;

            var pos = this.PointToScreen(labelPonto.Location);
            pos = pictureBox1.PointToClient(pos);
            labelPonto.Parent = pictureBox1;
            labelPonto.Location = pos;
            labelPonto.BackColor = Color.Transparent;

            var pos4 = this.PointToScreen(labelHora.Location);
            pos4 = pictureBox1.PointToClient(pos4);
            labelHora.Parent = pictureBox1;
            labelHora.Location = pos4;
            labelHora.BackColor = Color.Transparent;

            var pos5 = this.PointToScreen(labelAhora.Location);
            pos5 = pictureBox1.PointToClient(pos5);
            labelAhora.Parent = pictureBox1;
            labelAhora.Location = pos5;
            labelAhora.BackColor = Color.Transparent;

            var pos6 = this.PointToScreen(labelAevento.Location);
            pos6 = pictureBox1.PointToClient(pos6);
            labelAevento.Parent = pictureBox1;
            labelAevento.Location = pos6;
            labelAevento.BackColor = Color.Transparent;

            var pos7 = this.PointToScreen(labelAdescricao.Location);
            pos7 = pictureBox1.PointToClient(pos7);
            labelAdescricao.Parent = pictureBox1;
            labelAdescricao.Location = pos7;
            labelAdescricao.BackColor = Color.Transparent;

            var pos8 = this.PointToScreen(buttonAciona.Location);
            pos8 = pictureBox1.PointToClient(pos8);
            buttonAciona.Parent = pictureBox1;
            buttonAciona.Location = pos8;
            buttonAciona.BackColor = Color.Transparent;

            var pos9 = this.PointToScreen(labelAciona.Location);
            pos9 = pictureBox1.PointToClient(pos9);
            labelAciona.Parent = pictureBox1;
            labelAciona.Location = pos9;
            labelAciona.BackColor = Color.Transparent;

            var pos3 = this.PointToScreen(pictureBox2.Location);
            pos3 = pictureBox1.PointToClient(pos3);
            pictureBox2.Parent = pictureBox1;
            pictureBox2.Location = pos3;
            pictureBox2.BackColor = Color.Transparent;

            var pos12 = this.PointToScreen(button4.Location);
            pos12 = pictureBox1.PointToClient(pos12);
            button4.Parent = pictureBox1;
            button4.Location = pos12;
            button4.BackColor = Color.Transparent;

            var pos111 = this.PointToScreen(tanque1.Location);
            pos111 = pictureBox1.PointToClient(pos111);
            tanque1.Parent = pictureBox1;
            tanque1.Location = pos111;
            tanque1.BackColor = Color.Transparent;
            //this.aquaGaugeVazao.BackColor = Color.Transparent;

            if (tipodados == 0) //Nível
            {
                verifica(idp);
                enableBomba(bomba);
                tanque1.Visible = true;
                aquaGaugeVazao.Visible = false;
                aquaGaugePh.Visible = false;
                gBxEnergia.Visible = false;
                aquaGaugePressao.Visible = false;
                nivel(idp);
            }
            else if (tipodados == 1) //Bomba
            {
                verifica(idp);
                enableBomba(bomba);
                tanque1.Visible = false;
                aquaGaugeVazao.Visible = false;
                gBxEnergia.Visible = false;
                aquaGaugePh.Visible = false;
                aquaGaugePressao.Visible = false;
            }

            else if (tipodados == 2 || tipodados == 5) //Vazão
            {
                verifica(idp);
                enableBomba(bomba);
                tanque1.Visible = false;
                aquaGaugeVazao.Visible = true;
                aquaGaugePh.Visible = false;
                gBxEnergia.Visible = false;
                aquaGaugePressao.Visible = false;
                vazao(idp);
            }

            else if (tipodados == 3)//Ph
            {
                verifica(idp);
                enableBomba(bomba);
                tanque1.Visible = false;
                aquaGaugeVazao.Visible = false;
                aquaGaugePh.Visible = true;
                gBxEnergia.Visible = false;
                aquaGaugePressao.Visible = false;
                ph(idp);
            }

            else if (tipodados == 4)//Energia
            {
                verifica(idp);
                enableBomba(bomba);
                tanque1.Visible = false;
                aquaGaugeVazao.Visible = false;
                aquaGaugePh.Visible = false;
                gBxEnergia.Visible = true;
                aquaGaugePressao.Visible = false;
                energia(idp);
            }
            else if (tipodados == 6)//Turbidez
            {
                verifica(idp);
                enableBomba(bomba);
                tanque1.Visible = false;
                aquaGaugeVazao.Visible = false;
                aquaGaugePh.Visible = false;
                gBxEnergia.Visible = true;
                gBxEnergia.Text = "";
                lblTurbidezLbl.Visible = true;
                lblTurbidez.Visible = true;
                lblTurbidez.BringToFront();
                lblTurbidezLbl.BringToFront();
                aquaGaugePressao.Visible = false;
                turbidez(idp);
            }
            else if (tipodados == 7)//Pressão
            {
                verifica(idp);
                enableBomba(bomba);
                tanque1.Visible = false;
                aquaGaugeVazao.Visible = false;
                aquaGaugePh.Visible = false;
                gBxEnergia.Visible = false;
                aquaGaugePressao.BringToFront();
                aquaGaugePressao.Visible = true;
                pressao(idp);
            }

            else if (tipodados == 8)//Inversor
            {
                verifica(idp);
                enableBomba(bomba);
                tanque1.Visible = false;
                aquaGaugeVazao.Visible = false;
                aquaGaugePh.Visible = false;
                gBxEnergia.Visible = false;
                aquaGaugePressao.Visible = false;
                aquaGaugeInversor.BringToFront();
                aquaGaugeInversor.Visible = true;
                pressao(idp);
            }

            var pos19 = this.PointToScreen(pictureBox3.Location);
            pos19 = pictureBox1.PointToClient(pos19);
            pictureBox3.Parent = pictureBox1;
            pictureBox3.Location = pos19;
            pictureBox3.BackColor = Color.Transparent;
            pictureBox3.BringToFront();
            tanque1.BringToFront();
            aquaGaugeVazao.BringToFront();
            aquaGaugePh.BringToFront();
            lblTurbidez.BringToFront();
            lblTurbidezLbl.BringToFront();
            Application.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void enableBomba(int bomba)
        {
            if (bomba == 1)
            {
                buttonAciona.Enabled = true;
                buttonAciona.Visible = true;
                labelAciona.Enabled = true;
                labelAciona.Visible = true;
                lblAcionamentoAutomatico.Visible = true;
                btnAcionamentoAutomatico.Visible = true;
                lblAcionamentoAutomatico.Enabled = true;
                btnAcionamentoAutomatico.Enabled = true;
                //SetLed(vazao, datahoraleitura);
            }
            else if (bomba == 0)
            {
                buttonAciona.Enabled = false;
                buttonAciona.Visible = false;
                labelAciona.Enabled = false;
                labelAciona.Visible = false;
                buttonAciona.Text = "";
                lblAcionamentoAutomatico.Visible = false;
                btnAcionamentoAutomatico.Visible = false;
                lblAcionamentoAutomatico.Enabled = false;
                btnAcionamentoAutomatico.Enabled = false;
                //SetLed(vazao, datahoraleitura);
            }
        }

        private void Leitura(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituravazao = DatabaseManager.Select("ponto_nivel", "nivel, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                float vazao = float.Parse(leituravazao[0][0]);
                SetLed(float.Parse(leituravazao[0][0]), leituravazao[0][1]);
                AutoValidate = AutoValidate.EnablePreventFocusChange;
                tanque1.Tanque1.Value = ((100 * vazao) / NivelMax);
                tanque1.Tanque1.LabelValue.Text = vazao.ToString() + " m.c.a";
                //aquaGaugeVazao.DigitalValue = vazao / 1000;
                //aquaGaugeVazao.Value = vazao / 1000;
                if (Status.Length == 0)
                    labelHora.Text = "Última atualização: " + leituravazao[0][1];
                else
                    labelHora.Text = "Última atualização: " + leituravazao[0][1] + "\r\n" + Status;
            }
            catch (Exception)
            {

            }
        }
        private void vazao(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituravazao = DatabaseManager.Select("ponto_leitura", "vazao/3600, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                List<string[]> leituramaxvazao = DatabaseManager.Select("ponto", "nivel_max/1000", "id_ponto=" + id);
                float vazao = float.Parse(leituravazao[0][0]);
                float maxVazao = float.Parse(leituramaxvazao[0][0]);
                SetLed(float.Parse(leituravazao[0][0]), leituravazao[0][1]);
                AutoValidate = AutoValidate.EnablePreventFocusChange;
                aquaGaugeVazao.MaxValue = maxVazao;
                aquaGaugeVazao.Value = vazao;
                aquaGaugeVazao.DigitalValue = vazao;
                //aquaGaugeVazao.Tanque1.LabelValue.Text = vazao.ToString() + " m³/H";
                //aquaGaugeVazao.Value = vazao;
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atulização: " + leituravazao[0][1];
                }
                else
                {
                    labelHora.Text = "Última atulização: " + leituravazao[0][1] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }

        private void nivel(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituravazao = DatabaseManager.Select("ponto_nivel", "nivel, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                float vazao = float.Parse(leituravazao[0][0]);
                SetLed(float.Parse(leituravazao[0][0]), leituravazao[0][1]);
                AutoValidate = AutoValidate.EnablePreventFocusChange;
                tanque1.Tanque1.Value = ((100 * vazao) / NivelMax);
                tanque1.Tanque1.LabelValue.Text = vazao.ToString() + " m.c.a";
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atualização: " + leituravazao[0][1];
                }
                else
                {
                    labelHora.Text = "Última atualização: " + leituravazao[0][1] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }

        private void ph(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituraPh = DatabaseManager.Select("ponto_ph", "ph, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                //List<string[]> leituramaxPh = DatabaseManager.Select("ponto", "ponto", "id_ponto=" + id);
                float Ph = float.Parse(leituraPh[0][0]);
                //float maxPh = float.Parse(leituramaxPh[0][0]);
                float maxPh = 14;
                SetLed(float.Parse(leituraPh[0][0]), leituraPh[0][1]);
                AutoValidate = AutoValidate.EnablePreventFocusChange;
                aquaGaugePh.MaxValue = maxPh;
                aquaGaugePh.Value = Ph;
                aquaGaugePh.DigitalValue = Ph;
                //aquaGaugeVazao.Tanque1.LabelValue.Text = vazao.ToString() + " m³/H";
                //aquaGaugeVazao.Value = vazao;
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atulização: " + leituraPh[0][1];
                }
                else
                {
                    labelHora.Text = "Última atulização: " + leituraPh[0][1] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }

        private void energia(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituraEner = DatabaseManager.Select("ponto_energia", "corrente, potencia/1000, consumo/1000, tensao_fase1, tensao_fase2, tensao_fase3, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                //List<string[]> leituramaxEner = DatabaseManager.Select("ponto", "ponto", "id_ponto=" + id);
                float corrente = float.Parse(leituraEner[0][0]);
                double potencia = Math.Round(double.Parse(leituraEner[0][1]), 2);
                double consumo = Math.Round(double.Parse(leituraEner[0][2]), 2);
                float fase1 = float.Parse(leituraEner[0][3]);
                float fase2 = float.Parse(leituraEner[0][4]);
                float fase3 = float.Parse(leituraEner[0][5]);

                //float maxPh = float.Parse(leituramaxEner[0][0]);
                SetLed(float.Parse(leituraEner[0][0]), leituraEner[0][6]);
                //AutoValidate = AutoValidate.EnablePreventFocusChange;
                //aquaGaugePh.MaxValue = maxPh;
                lbCorrente.Text = "Corrente: " + corrente.ToString() + " A";
                lbPotencia.Text = "Potência: " + potencia.ToString() + " kW";
                lbConsumo.Text = "Consumo: " + consumo.ToString() + " kW/h";
                lbFase1.Text = "Tensão Fase1: " + fase1.ToString() + " V";
                lbFase2.Text = "Tensão Fase2: " + fase2.ToString() + " V";
                lbFase3.Text = "Tensão Fase3: " + fase3.ToString() + " V";
                //aquaGaugeVazao.Tanque1.LabelValue.Text = vazao.ToString() + " m³/H";
                //aquaGaugeVazao.Value = vazao;
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atulização: " + leituraEner[0][6];
                }
                else
                {
                    labelHora.Text = "Última atulização: " + leituraEner[0][6] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }

        private void turbidez(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituraTurbidez = DatabaseManager.Select("ponto_turbidez", "turbidez, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                //List<string[]> leituramaxTurbidez = DatabaseManager.Select("ponto", "ponto", "id_ponto=" + id);
                double turbidez = Math.Round(float.Parse(leituraTurbidez[0][0]), 3);
                //float maxTurbidez = float.Parse(leituramaxTurbidez[0][0]);
                SetLed(float.Parse(leituraTurbidez[0][0]), leituraTurbidez[0][1]);
                //AutoValidate = AutoValidate.EnablePreventFocusChange;

                lblTurbidez.Visible = true;
                lblTurbidezLbl.Visible = true;
                lblTurbidez.Text = turbidez.ToString();
                lblTurbidez.BringToFront();

                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atulização: " + leituraTurbidez[0][1];
                }
                else
                {
                    labelHora.Text = "Última atulização: " + leituraTurbidez[0][1] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }
        private void pressao(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituraPressao = DatabaseManager.Select("ponto_pressao", "pressao, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                List<string[]> leituramaxPressao = DatabaseManager.Select("ponto", "nivel_max", "id_ponto=" + id);
                float Pressao = float.Parse(leituraPressao[0][0]);
                float maxPressao = float.Parse(leituramaxPressao[0][0]);
                //float maxPressao = 10;
                SetLed(float.Parse(leituraPressao[0][0]), leituraPressao[0][1]);
                AutoValidate = AutoValidate.EnablePreventFocusChange;
                aquaGaugePressao.MaxValue = maxPressao/2;
                aquaGaugePressao.Value = Pressao;
                aquaGaugePressao.DigitalValue = Pressao;
                //aquaGaugeVazao.Tanque1.LabelValue.Text = vazao.ToString() + " m³/H";
                //aquaGaugeVazao.Value = vazao;
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atulização: " + leituraPressao[0][1];
                }
                else
                {
                    labelHora.Text = "Última atulização: " + leituraPressao[0][1] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }

        private void inversor(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituraInversor = DatabaseManager.Select("ponto_inversor", "inversor, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                //List<string[]> leituramaxPressao = DatabaseManager.Select("ponto", "ponto", "id_ponto=" + id);
                float Inversor = float.Parse(leituraInversor[0][0]);
                //float maxPressao = float.Parse(leituramaxPressao[0][0]);
                float maxFrequencia = 60;
                SetLed(float.Parse(leituraInversor[0][0]), leituraInversor[0][1]);
                AutoValidate = AutoValidate.EnablePreventFocusChange;
                aquaGaugeInversor.MaxValue = maxFrequencia;
                aquaGaugeInversor.Value = Inversor;
                aquaGaugeInversor.DigitalValue = Inversor;
                //aquaGaugeVazao.Tanque1.LabelValue.Text = vazao.ToString() + " m³/H";
                //aquaGaugeVazao.Value = vazao;
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atulização: " + leituraInversor[0][1];
                }
                else
                {
                    labelHora.Text = "Última atulização: " + leituraInversor[0][1] + "\r\n" + Status;
                }
            }
            catch (Exception)
            {

            }
        }

        //static Tela MsgBox; static DialogResult result;
        public static DialogResult show(string ponto, int bomba, string alarmeE, string descricao, string horaAE, int identificador, int tipodados, string nivel_max, string status)
        {
            if (IsConnected() == true)
            {
                Tela MsgBox = new Tela(identificador, ponto, bomba, tipodados, float.Parse(nivel_max), status);
                //MsgBox.AutoValidate = AutoValidate.EnablePreventFocusChange;
                //MsgBox.aquaGaugeVazao.DigitalValue = vazao / 1000;
                //MsgBox.aquaGaugeVazao.Value = vazao / 1000;
                //MsgBox.sevenSvolume.Value = (volume / 1000).ToString().PadLeft(7, '0');
                //MsgBox.label2.Text = "Última atulização: " + datahoraleitura;
                MsgBox.labelPonto.Text = ponto;
                MsgBox.labelAevento.Text = "Último Alarme ou Evento: " + alarmeE;
                MsgBox.labelAhora.Text = "Hora: " + horaAE;
                MsgBox.labelAdescricao.Text = "Descrição: " + descricao;
                DialogResult result = MsgBox.ShowDialog();
                return result;
            }
            else
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return DialogResult.Cancel;
            }
        }

        private void Tela_Load(object sender, EventArgs e)
        {
            //this.tanque1.Tanque1.CorFluido = System.Drawing.Color.DodgerBlue;
            //this.tanque1.Tanque1.CorFluido = System.Drawing.Color.FromArgb(0,111,223);
            //this.tanque1.Tanque1.CorVidro = System.Drawing.Color.Gray;
            //this.tanque1.Tanque1.Value = 0;
            //this.tanque1.Tanque1.MinVidro = 25;
            //this.tanque1.Tanque1.LabelValue.Text = "--%";
            Threadupdate = new Thread(new ThreadStart(UpdateAll));
            Threadupdate.Start();
        }

        private void UpdateAll()
        {
            while (true)
            {
                Thread.Sleep(45 * 1000);
                if (IsConnected() == true)
                {
                    try
                    {
                        verifica(ID);
                        if (TipoDados == 0)
                        {
                            nivel(ID);
                        }
                        else if (TipoDados == 1)
                        {
                            Leitura(ID);
                        }
                        else if (TipoDados == 2)
                        {
                            vazao(ID);
                        }
                        else if (TipoDados == 3)
                        {
                            ph(ID);
                        }
                        else if (TipoDados == 4)
                        {
                            energia(ID);
                        }
                        else if (TipoDados == 5)
                        {
                            vazao(ID);
                        }
                        else if (TipoDados == 6)
                        {
                            turbidez(ID);
                        }
                        else if (TipoDados == 7)
                        {
                            pressao(ID);
                        }
                        else if (TipoDados == 8)
                        {
                            inversor(ID);
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Threadupdate.Abort();
            this.Close();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            this.UseWaitCursor = true; //curso de espera do mouse true
            Application.DoEvents();

            btnAcionamentoAutomatico.Enabled = false;

            if (acionadoAutomatico == false)
            {
                btnAcionamentoAutomatico.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.switchon));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                Thread novaThread1 = new Thread(new ThreadStart(ligaAutomatico));
                novaThread1.Start();
            }
            else
            {
                btnAcionamentoAutomatico.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.switchoff));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledgray));
                Thread novaThread1 = new Thread(new ThreadStart(ligaAutomatico));
                novaThread1.Start();
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            // Set cursor as hourglass
            //Cursor.Current = Cursors.WaitCursor;
            this.UseWaitCursor = true; //curso de espera do mouse true
            Application.DoEvents();

            buttonAciona.Enabled = false;

            if (acionado == false)
            {
                buttonAciona.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.switchon));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                Thread novaThread1 = new Thread(new ThreadStart(ligabomba));
                novaThread1.Start();
            }
            else
            {
                buttonAciona.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.switchoff));
                //pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledgray));
                Thread novaThread1 = new Thread(new ThreadStart(ligabomba));
                novaThread1.Start();
            }
        }

        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        //COnverte Byte Array to Hex String
        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0').PadRight(3, ' '));
            return sb.ToString().ToUpper();
        }

        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            buttonFecha.Size = new Size(15, 15);
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            buttonFecha.Size = new Size(15, 15);
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            buttonFecha.Size = new Size(15, 15);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            buttonFecha.Size = new Size(17, 17);
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            X = this.Left - MousePosition.X;
            Y = this.Top - MousePosition.Y;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            this.Left = X + MousePosition.X;
            this.Top = Y + MousePosition.Y;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmInformacao newfomr = new FrmInformacao(ID,nome);
            newfomr.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        //private void pictureBoxNivel_Click(object sender, EventArgs e)
        //{
        //    pictureBoxNivel.Image = TanqueDeFluidos.desenha(ref pictureBoxNivel, 50, System.Drawing.Brushes.Navy);
        //}

    }
}
