﻿using System.Windows.Forms;
namespace SATS
{
    partial class RelatorioPai
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RelatorioPai));
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxCliente = new System.Windows.Forms.ComboBox();
            this.dateTimeInicial = new System.Windows.Forms.DateTimePicker();
            this.dateTimeFinal = new System.Windows.Forms.DateTimePicker();
            this.buttonBuscar = new System.Windows.Forms.Button();
            this.listView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonLimpar = new System.Windows.Forms.Button();
            this.buttonXls = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelListView = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ponto:";
            // 
            // comboBoxCliente
            // 
            this.comboBoxCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboBoxCliente.FormattingEnabled = true;
            this.comboBoxCliente.Location = new System.Drawing.Point(60, 12);
            this.comboBoxCliente.Name = "comboBoxCliente";
            this.comboBoxCliente.Size = new System.Drawing.Size(166, 21);
            this.comboBoxCliente.TabIndex = 1;
            // 
            // dateTimeInicial
            // 
            this.dateTimeInicial.CustomFormat = "dd/MM/yyyy";
            this.dateTimeInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeInicial.Location = new System.Drawing.Point(275, 12);
            this.dateTimeInicial.MaxDate = new System.DateTime(2200, 12, 31, 0, 0, 0, 0);
            this.dateTimeInicial.MinDate = new System.DateTime(2007, 1, 1, 0, 0, 0, 0);
            this.dateTimeInicial.Name = "dateTimeInicial";
            this.dateTimeInicial.Size = new System.Drawing.Size(119, 20);
            this.dateTimeInicial.TabIndex = 2;
            this.dateTimeInicial.Value = new System.DateTime(2015, 2, 24, 0, 0, 0, 0);
            // 
            // dateTimeFinal
            // 
            this.dateTimeFinal.CustomFormat = "dd/MM/yyyy";
            this.dateTimeFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeFinal.Location = new System.Drawing.Point(444, 12);
            this.dateTimeFinal.MaxDate = new System.DateTime(2200, 12, 31, 0, 0, 0, 0);
            this.dateTimeFinal.MinDate = new System.DateTime(2007, 1, 1, 0, 0, 0, 0);
            this.dateTimeFinal.Name = "dateTimeFinal";
            this.dateTimeFinal.Size = new System.Drawing.Size(119, 20);
            this.dateTimeFinal.TabIndex = 3;
            // 
            // buttonBuscar
            // 
            this.buttonBuscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBuscar.Location = new System.Drawing.Point(569, 12);
            this.buttonBuscar.Name = "buttonBuscar";
            this.buttonBuscar.Size = new System.Drawing.Size(114, 23);
            this.buttonBuscar.TabIndex = 4;
            this.buttonBuscar.Text = "Buscar";
            this.buttonBuscar.UseVisualStyleBackColor = true;
            this.buttonBuscar.Click += new System.EventHandler(this.buttonBuscar_Click);
            // 
            // listView
            // 
            this.listView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView.Location = new System.Drawing.Point(32, 95);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(435, 300);
            this.listView.TabIndex = 5;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "columnHeader1";
            this.columnHeader1.Width = 84;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "columnHeader2";
            this.columnHeader2.Width = 139;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Data e Hora";
            this.columnHeader3.Width = 139;
            // 
            // buttonLimpar
            // 
            this.buttonLimpar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLimpar.Location = new System.Drawing.Point(569, 202);
            this.buttonLimpar.Name = "buttonLimpar";
            this.buttonLimpar.Size = new System.Drawing.Size(114, 23);
            this.buttonLimpar.TabIndex = 6;
            this.buttonLimpar.Text = "Limpar";
            this.buttonLimpar.UseVisualStyleBackColor = true;
            this.buttonLimpar.Click += new System.EventHandler(this.buttonLimpar_Click);
            // 
            // buttonXls
            // 
            this.buttonXls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonXls.Location = new System.Drawing.Point(569, 361);
            this.buttonXls.Name = "buttonXls";
            this.buttonXls.Size = new System.Drawing.Size(114, 23);
            this.buttonXls.TabIndex = 9;
            this.buttonXls.Text = "Gerar Planilha Excel";
            this.buttonXls.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 26);
            this.label2.TabIndex = 10;
            this.label2.Text = "Data\r\nInicial:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(400, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 26);
            this.label3.TabIndex = 11;
            this.label3.Text = "Data\r\nFinal:";
            // 
            // labelListView
            // 
            this.labelListView.AutoSize = true;
            this.labelListView.Location = new System.Drawing.Point(32, 76);
            this.labelListView.Name = "labelListView";
            this.labelListView.Size = new System.Drawing.Size(129, 13);
            this.labelListView.TabIndex = 12;
            this.labelListView.Text = "Lista de Resultados Vazia";
            // 
            // RelatorioPai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(695, 407);
            this.Controls.Add(this.labelListView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonXls);
            this.Controls.Add(this.buttonLimpar);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.buttonBuscar);
            this.Controls.Add(this.dateTimeFinal);
            this.Controls.Add(this.dateTimeInicial);
            this.Controls.Add(this.comboBoxCliente);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(711, 446);
            this.Name = "RelatorioPai";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.RelatorioPai_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.ComboBox comboBoxCliente;
        protected System.Windows.Forms.DateTimePicker dateTimeInicial;
        protected System.Windows.Forms.DateTimePicker dateTimeFinal;
        protected System.Windows.Forms.Button buttonBuscar;
        protected System.Windows.Forms.ListView listView;
        protected System.Windows.Forms.Button buttonLimpar;
        protected System.Windows.Forms.Button buttonXls;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label label3;
        protected ColumnHeader columnHeader1;
        protected ColumnHeader columnHeader2;
        protected ColumnHeader columnHeader3;
        protected Label labelListView;
    }
}

