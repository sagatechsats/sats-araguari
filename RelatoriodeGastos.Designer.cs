﻿namespace SATS
{
    partial class RelatoriodeGastos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RelatoriodeGastos));
            this.labelListView = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonXls = new System.Windows.Forms.Button();
            this.buttonLimpar = new System.Windows.Forms.Button();
            this.listView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonBuscar = new System.Windows.Forms.Button();
            this.dateYear = new System.Windows.Forms.DateTimePicker();
            this.dateMonth = new System.Windows.Forms.DateTimePicker();
            this.comboBoxGasto = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.SuspendLayout();
            // 
            // labelListView
            // 
            this.labelListView.AutoSize = true;
            this.labelListView.Location = new System.Drawing.Point(13, 81);
            this.labelListView.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelListView.Name = "labelListView";
            this.labelListView.Size = new System.Drawing.Size(164, 16);
            this.labelListView.TabIndex = 23;
            this.labelListView.Text = "Lista de Resultados Vazia";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(325, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 32);
            this.label2.TabIndex = 21;
            this.label2.Text = "Data \r\nde Referência:";
            // 
            // buttonXls
            // 
            this.buttonXls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonXls.Image = global::SATS.Properties.Resources._1387910006_Arzo_Icons_Icon_96_2;
            this.buttonXls.Location = new System.Drawing.Point(649, 333);
            this.buttonXls.Margin = new System.Windows.Forms.Padding(4);
            this.buttonXls.Name = "buttonXls";
            this.buttonXls.Size = new System.Drawing.Size(143, 49);
            this.buttonXls.TabIndex = 20;
            this.buttonXls.Text = "Gerar Planilha Excel";
            this.buttonXls.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonXls.UseVisualStyleBackColor = true;
            this.buttonXls.Click += new System.EventHandler(this.buttonXls_Click);
            // 
            // buttonLimpar
            // 
            this.buttonLimpar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLimpar.Image = global::SATS.Properties.Resources.Crystal_Clear_action_editpaste;
            this.buttonLimpar.Location = new System.Drawing.Point(649, 297);
            this.buttonLimpar.Margin = new System.Windows.Forms.Padding(4);
            this.buttonLimpar.Name = "buttonLimpar";
            this.buttonLimpar.Size = new System.Drawing.Size(143, 28);
            this.buttonLimpar.TabIndex = 19;
            this.buttonLimpar.Text = "Limpar";
            this.buttonLimpar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonLimpar.UseVisualStyleBackColor = true;
            this.buttonLimpar.Click += new System.EventHandler(this.buttonLimpar_Click);
            // 
            // listView
            // 
            this.listView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader7});
            this.listView.Location = new System.Drawing.Point(13, 101);
            this.listView.Margin = new System.Windows.Forms.Padding(4);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(628, 279);
            this.listView.TabIndex = 18;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Ponto";
            this.columnHeader1.Width = 205;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Período";
            this.columnHeader2.Width = 258;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "kWh Consumido";
            this.columnHeader4.Width = 197;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Valor Gasto por Ponto (R$)";
            this.columnHeader7.Width = 176;
            // 
            // buttonBuscar
            // 
            this.buttonBuscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBuscar.Image = global::SATS.Properties.Resources.Crystal_Clear_app_xmag__1_;
            this.buttonBuscar.Location = new System.Drawing.Point(649, 23);
            this.buttonBuscar.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBuscar.Name = "buttonBuscar";
            this.buttonBuscar.Size = new System.Drawing.Size(143, 28);
            this.buttonBuscar.TabIndex = 17;
            this.buttonBuscar.Text = "Buscar";
            this.buttonBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonBuscar.UseVisualStyleBackColor = true;
            this.buttonBuscar.Click += new System.EventHandler(this.buttonBuscar_Click);
            // 
            // dateYear
            // 
            this.dateYear.CustomFormat = "dd/MM/yyyy";
            this.dateYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateYear.Location = new System.Drawing.Point(538, 24);
            this.dateYear.Margin = new System.Windows.Forms.Padding(4);
            this.dateYear.MaxDate = new System.DateTime(2200, 12, 31, 0, 0, 0, 0);
            this.dateYear.MinDate = new System.DateTime(2007, 1, 1, 0, 0, 0, 0);
            this.dateYear.Name = "dateYear";
            this.dateYear.Size = new System.Drawing.Size(103, 22);
            this.dateYear.TabIndex = 16;
            // 
            // dateMonth
            // 
            this.dateMonth.CustomFormat = "dd/MM/yyyy";
            this.dateMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMonth.Location = new System.Drawing.Point(429, 24);
            this.dateMonth.Margin = new System.Windows.Forms.Padding(4);
            this.dateMonth.MaxDate = new System.DateTime(2200, 12, 31, 0, 0, 0, 0);
            this.dateMonth.MinDate = new System.DateTime(2007, 1, 1, 0, 0, 0, 0);
            this.dateMonth.Name = "dateMonth";
            this.dateMonth.Size = new System.Drawing.Size(101, 22);
            this.dateMonth.TabIndex = 15;
            this.dateMonth.Value = new System.DateTime(2015, 2, 24, 0, 0, 0, 0);
            this.dateMonth.ValueChanged += new System.EventHandler(this.dateMonth_ValueChanged);
            // 
            // comboBoxGasto
            // 
            this.comboBoxGasto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxGasto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboBoxGasto.FormattingEnabled = true;
            this.comboBoxGasto.Items.AddRange(new object[] {
            "Energia (kWh) nos poços",
            "Produtos Químicos (Kg) nos poços",
            "Produtos Químicos (Kg) na ETA"});
            this.comboBoxGasto.Location = new System.Drawing.Point(90, 24);
            this.comboBoxGasto.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxGasto.Name = "comboBoxGasto";
            this.comboBoxGasto.Size = new System.Drawing.Size(227, 24);
            this.comboBoxGasto.TabIndex = 14;
            this.comboBoxGasto.SelectedIndexChanged += new System.EventHandler(this.comboBoxGasto_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 32);
            this.label1.TabIndex = 13;
            this.label1.Text = "Tipo \r\nde Gasto:";
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerColorTint = System.Drawing.Color.CornflowerBlue;
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            // 
            // RelatoriodeGastos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(215)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(796, 407);
            this.Controls.Add(this.labelListView);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonXls);
            this.Controls.Add(this.buttonLimpar);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.buttonBuscar);
            this.Controls.Add(this.dateYear);
            this.Controls.Add(this.dateMonth);
            this.Controls.Add(this.comboBoxGasto);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(812, 446);
            this.Name = "RelatoriodeGastos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Relatorio de Gastos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RelatoriodeGastos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Label labelListView;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Button buttonXls;
        protected System.Windows.Forms.Button buttonLimpar;
        protected System.Windows.Forms.ListView listView;
        protected System.Windows.Forms.ColumnHeader columnHeader1;
        protected System.Windows.Forms.ColumnHeader columnHeader2;
        protected System.Windows.Forms.Button buttonBuscar;
        protected System.Windows.Forms.DateTimePicker dateYear;
        protected System.Windows.Forms.DateTimePicker dateMonth;
        protected System.Windows.Forms.ComboBox comboBoxGasto;
        protected System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader7;
    }
}