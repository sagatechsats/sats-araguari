﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SATS
{
    public partial class TelaEnergia : Office2007Form
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private int id = 0;
        private int X = 0;
        private int Y = 0;
        private string nome = "";
        private string stcon = System.IO.File.ReadAllText(@"" + System.AppDomain.CurrentDomain.BaseDirectory + "stconnector");
        List<string[]> Ponto;
        private String Status;

        public TelaEnergia(int id, string nomep, int bomba, int tipodados, float nivel_max, string status)
        {
            InitializeComponent();
            this.id = id;
            nome = nomep;
            Status = status;
            energia(id);
            verifica();
            gBxEnergia.Visible = true;
        }


        public static bool IsConnected()
        {
            bool result;
            try
            {
                int Description;
                result = InternetGetConnectedState(out Description, 0);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public void verifica()
        {

            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                Ponto = DatabaseManager.Select("ponto", "nome_ponto", "id_ponto= '" + id + "'");
                //Evento = DatabaseManager.Select("ponto_eventos", "tipo_evento, descricao, max(data_hora)", "id_ponto= '" + id + "'");
                passaResultadoTela();

            }
            catch (Exception)
            {

            }

        }

        public void passaResultadoTela()
        {
            labelPonto.Text = "Ponto " + Ponto[0][0];
        }


        private void energia(int id)
        {
            try
            {
                DBManager DatabaseManager = new DBManager(stcon);
                List<string[]> leituraEner = DatabaseManager.Select("ponto_energia", "corrente, potencia/1000, consumo/1000, tensao_fase1, tensao_fase2, tensao_fase3, max(data_hora)", "id_ponto='" + id + "'group by data_hora desc limit 1");
                //List<string[]> leituramaxEner = DatabaseManager.Select("ponto", "ponto", "id_ponto=" + id);
                float corrente = float.Parse(leituraEner[0][0]);
                double potencia = Math.Round(double.Parse(leituraEner[0][1]), 2);
                double consumo = Math.Round(double.Parse(leituraEner[0][2]), 2);
                float fase1 = float.Parse(leituraEner[0][3]);
                float fase2 = float.Parse(leituraEner[0][4]);
                float fase3 = float.Parse(leituraEner[0][5]);

                //float maxPh = float.Parse(leituramaxEner[0][0]);
                SetLed(float.Parse(leituraEner[0][0]), leituraEner[0][6]);
                //AutoValidate = AutoValidate.EnablePreventFocusChange;
                //aquaGaugePh.MaxValue = maxPh;
                lbCorrente.Text = "Corrente: " + corrente.ToString() + " A";
                lbPotencia.Text = "Potência: " + potencia.ToString() + " kW";
                lbConsumo.Text = "Consumo: " + consumo.ToString() + " kW/h";
                lbFase1.Text = "Tensão Fase1: " + fase1.ToString() + " V";
                lbFase2.Text = "Tensão Fase2: " + fase2.ToString() + " V";
                lbFase3.Text = "Tensão Fase3: " + fase3.ToString() + " V";
                //aquaGaugeVazao.Tanque1.LabelValue.Text = vazao.ToString() + " m³/H";
                //aquaGaugeVazao.Value = vazao;
                if (Status.Length == 0)
                {
                    labelHora.Text = "Última atulização: " + leituraEner[0][6];
                }
                else
                {
                    labelHora.Text = "Última atulização: " + leituraEner[0][6] + "\r\n" + Status;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void SetLed(float vazao, string datahora)
        {
            decimal stempo = 0;

            DateTime datanow = DateTime.Now;
            DateTime data = DateTime.Now;
            try
            {
                data = Convert.ToDateTime(datahora);
            }
            catch (Exception)
            {
            }

            TimeSpan ts = datanow.Subtract(data);
            stempo = Convert.ToDecimal(ts.TotalMinutes);


            if (vazao == 0)
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledred));
                Invoke((MethodInvoker)(() => pictureBox3.Visible = false));
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    Invoke((MethodInvoker)(() => pictureBox3.Visible = true));
                }
            }
            else
            {
                pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.led));
                Invoke((MethodInvoker)(() => pictureBox3.Visible = false));
                if (stempo > 30)
                {
                    pictureBox2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.ledY));
                    Invoke((MethodInvoker)(() => pictureBox3.Visible = true));
                }
            }
        }




        public static DialogResult show(string ponto, int bomba, string alarmeE, string descricao, string horaAE, int identificador, int tipodados, string nivel_max, string status)
        {
            if (IsConnected() == true)
            {
                TelaEnergia MsgBox = new TelaEnergia(identificador, ponto, bomba, tipodados, float.Parse(nivel_max), status);
                //MsgBox.AutoValidate = AutoValidate.EnablePreventFocusChange;
                //MsgBox.aquaGaugeVazao.DigitalValue = vazao / 1000;
                //MsgBox.aquaGaugeVazao.Value = vazao / 1000;
                //MsgBox.sevenSvolume.Value = (volume / 1000).ToString().PadLeft(7, '0');
                //MsgBox.label2.Text = "Última atulização: " + datahoraleitura;
                MsgBox.labelPonto.Text = ponto;
                MsgBox.labelAevento.Text = "Último Alarme ou Evento: " + alarmeE;
                MsgBox.labelAhora.Text = "Hora: " + horaAE;
                MsgBox.labelAdescricao.Text = "Descrição: " + descricao;
                DialogResult result = MsgBox.ShowDialog();
                return result;
            }
            else
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return DialogResult.Cancel;
            }
        }

        private void buttonFecha_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }
    }
}
