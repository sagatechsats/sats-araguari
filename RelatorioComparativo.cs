﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;
using System.Threading;

namespace SATS
{
    public partial class RelatorioComparativo : RelatorioPai
    {
        List<string[]> resultgrafico = new List<string[]>();
        int count = 0;
        public RelatorioComparativo()
        {
            if (Screen.AllScreens.Length > 1)
            {
                System.Drawing.Rectangle workingArea = Screen.AllScreens[1].WorkingArea;
            }
            InitializeComponent();
            comboBoxCliente.Items.Clear();
            comboBoxCliente.AutoCompleteCustomSource.Clear();
            this.WindowState = FormWindowState.Maximized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ArrayList evento = new ArrayList();
            //evento.Add(new drop("", 0));
            evento.Add(new drop("Domingo", 1));
            evento.Add(new drop("Segunda", 2));
            evento.Add(new drop("Terça", 3));
            evento.Add(new drop("Quarta", 4));
            evento.Add(new drop("Quinta", 5));
            evento.Add(new drop("Sexta", 6));
            evento.Add(new drop("Sábado", 7));
            comboBoxdia.DataSource = evento;
            comboBoxdia.DisplayMember = "nome";
            comboBoxdia.ValueMember = "valor";
        }

        public class drop
        {
            public drop(string nome, int valor)
            {
                Nome = nome;
                Valor = valor;
            }
            string _nome;
            int _valor;
            public int Valor
            {
                get
                {
                    return _valor;
                }
                set
                {
                    _valor = value;
                }
            }
            public string Nome
            {
                get
                {
                    return _nome;
                }
                set
                {
                    _nome = value;
                }
            }
        }

        private void GenerateChart(int volumeOuVazao)
        {
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar gráfico.",
                "Aviso: Lista de Resultados Vazia",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            RelatorioGrafico03 chart = new RelatorioGrafico03(volumeOuVazao, count, comboBoxdia.Text);
            chart.ReceiveFromListView(resultgrafico);
            chart.ShowDialog();
        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                resultgrafico.Clear();
            }
            catch (Exception)
            {
            }
            if (CheckDateValidity() == false)
                return;

            int index = comboBoxCliente.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Para realizar a busca é necessário selecionar-se um ponto.",
                "Selecione um ponto",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            StoreSearchData();
            Thread novaThread = new Thread(new ParameterizedThreadStart(CarregaTela));
            novaThread.Start(index);
            labelListView.Text = "Resultados para o Ponto:" + ultCliente + " de " + ultDateIn +
                " a " + ultDateFi;
        }
        private void CarregaTela(object parametro)
        {
            if (IsConnected() == true)
            {
                int indexdia = 0;
                Invoke((MethodInvoker)(() => indexdia = comboBoxdia.SelectedIndex + 1));

                if (indexdia == 0)
                {
                    MessageBox.Show("Para realizar a busca é necessário selecionar um dia.",
                    "Selecione um da semana",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                Application.UseWaitCursor = true;
                Application.DoEvents();
                try
                {
                    int index = Convert.ToInt32(parametro);

                    count = 0;
                    List<string[]> result = dbManager.Obterporhora2(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), indexdia, indexClientes[index].ToString());
                    if (result.Count <= 0)
                    {
                        Invoke((MethodInvoker)(() => MessageBox.Show("Não foi encontrado nenhum resultado para o ponto selecionado.",
                            "Aviso: Nenhum resultado encontrado.",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning)));
                        Application.UseWaitCursor = false;

                        Application.DoEvents();
                        return;
                    }
                    resultgrafico = result;
                    count = dbManager.Obterporquatidade2(dateTimeInicial.Value.ToString("s"), dateTimeFinal.Value.ToString("s"), indexdia, indexClientes[index].ToString());
                    Invoke((MethodInvoker)(() => FillListView(result)));
                }
                catch (Exception)
                {

                }
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else if (IsConnected() == false)
            {
                MessageBox.Show("Erro de Conexão com o Banco de dados, verifique se há conexão com internet.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void preencheList(List<string[]> consultalist)
        {
            listView.Items.Clear();
            int j = 0;
            for (int i = 0; i < 24 * (count); i++)
            {
                string[] subitems = new string[4];
                {
                    subitems[0] = "";
                    subitems[1] = "";
                    subitems[2] = j.ToString();
                    subitems[3] = "";
                }
                //ListViewItem item = new ListViewItem(subitems);
                resultgrafico.Add(subitems); //carrega os itens no list view
                //j=i;
                j++;
                j = j % 24;
            }
            // label1.Text = "Quantidade de dados: " + consultalist.Count; //escreve na label a quantidade de dados exibidos no listview
            this.Controls.Add(listView);
            int i4 = 0;
            for (int i3 = 0; i3 < consultalist.Count; i3++)
            {
                for (int i2 = i4; i2 < 24 * count; i2++)
                {
                    if (resultgrafico[i2][4].ToString().CompareTo(consultalist[i3][4].ToString()) == 0)
                    {
                        resultgrafico[i2][0] = consultalist[i3][0].ToString();
                        resultgrafico[i2][1] = consultalist[i3][1].ToString();
                        resultgrafico[i2][2] = consultalist[i3][2].ToString();
                        resultgrafico[i2][3] = consultalist[i3][3].ToString();
                        //listView.Items[i2].SubItems[4].Text = consultalist[i3][4].ToString();
                        i4 = i2;
                        break;
                    }
                }
            }
        }

        private void buttonXls_Click(object sender, EventArgs e)
        {
            if (listView.Items.Count == 0)
            {
                MessageBox.Show("Sem resultados para gerar planilha.",
                    "Aviso: Lista de Resultados Vazia",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            SpreadsheetCreator2 creator = new SpreadsheetCreator2(listView);
            creator.CreateFile("Descritivo", ultCliente, ultDateIn, ultDateFi);
        }

        private void buttonGrafico_Click(object sender, EventArgs e)
        {
            GenerateChart(1);
        }

        private void RelatorioComparativo_Load(object sender, EventArgs e)
        {
            comboBoxCliente.Items.Clear();
        }
    }
}
